#!/usr/bin/python


import sys, os
import logging
#======================================================================
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "..")))

from SyntheSys import Algorithm, Testbench, Synthesize
from SyntheSys.Library.ImageProcessing import RegionMean

#--------------------------------------------------------------------
@Algorithm() # Mean that this method will be HighLevelModule algorithm to be synthesized.
def Mean(x, y, z):
	"""
	Design core algorithm : read sequentially but understood as a parallel algorithm.
	Here HighLevelModules can be instanciated (a call for their algorithm methods).
	"""
	a=2*x
	b=3*y
	c=4*z
	d=x+y
	e=y+z
	f=d+z
	return RegionMean([a,b,c,d,e,x,y,z,f])
	
#--------------------------------------------------------------------
@Testbench() # Mean that this method will be HighLevelModule testbench
def Mean_Test():
	"""
	Test bench for mean processing HighLevelModule: sequential.
	"""
#	Data=[2, 2, 4, 4, 8, 8, 10, 10, 12]
#	for i in range(1, 10):

	
	#=====================
	r=Mean(5,6,7)
	#=====================
	
	return True



#======================================================================
if __name__=='__main__': 
	
	OutputPath=os.path.join(os.path.abspath(os.path.dirname(__file__)), "Mean_SyntheSys_output")
	Synthesize(Mean, Mean_Test, OutputPath=OutputPath, x=5, y=6, z=7) # No constraints





