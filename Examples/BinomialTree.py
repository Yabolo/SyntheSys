#!/usr/bin/python


import sys, os
import logging
#======================================================================
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "..")))

from SyntheSys import Algorithm, Testbench, Synthesize
from SyntheSys.Library.SW.Processing.Finance import BOPM_Unit_Add

TABSIZE=2
#--------------------------------------------------------------------
@Algorithm() # Mean that this method will be HighLevelModule algorithm to be synthesized.
def BOPM(Tab):
	"""
	Design core algorithm : read sequentially but understood as a parallel algorithm.
	Here HighLevelModules can be instanciated (a call for their algorithm methods).
	"""		
	for Step in range(len(Tab)):
		for i in range(len(Tab)-Step-1):
			Tab[i]=BOPM_Unit_Add(Stim1=Tab[i], Stim2=Tab[i+1])
	return Tab[0]

#--------------------------------------------------------------------
@Testbench() # Mean that this method will be HighLevelModule testbench
def BOPM_Test():
	"""
	Test bench for ALU HighLevelModule: sequential.
	"""
	for T in range(1):
		InitValues = [7 for i in range(TABSIZE)]
		#=====================
		r=BOPM(InitValues)
		#=====================
	return True



#======================================================================
if __name__=='__main__': 
	
	for Item in sys.argv:
		if Item.lower().startswith("--tabsize="):
			TABSIZE=int(Item.lower().split("--tabsize=")[1])
			sys.argv.remove(Item)
	
	Arguments=[3 for i in range(TABSIZE)]
#	OutputPath=os.path.join(os.path.abspath(os.path.dirname(__file__)), "BinomialTree_SyntheSys_output")
	Synthesize(BOPM, BOPM_Test, OutputPath="./BinomialTree_SyntheSys_output", Tab=Arguments) # No constraints





