#!/usr/bin/python


import sys, os
import logging
#======================================================================
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "..")))

from SyntheSys import Algorithm, Testbench, Synthesize
from SyntheSys.Library.ImageProcessing import CIE_a

#--------------------------------------------------------------------
@Algorithm() # Mean that this method will be HighLevelModule algorithm to be synthesized.
def CIA(x,y):
	"""
	Design core algorithm : read sequentially but understood as a parallel algorithm.
	Here HighLevelModules can be instanciated (a call for their algorithm methods).
	"""		
	return CIE_a(x,y)
	
#--------------------------------------------------------------------
@Testbench() # Mean that this method will be HighLevelModule testbench
def CIA_Test():
	"""
	Test bench for mean processing HighLevelModule: sequential.
	"""
	for x,y in [(1.13,1.13), (0.478,1.24), (2.45,5.86)]:
		r=CIA(x,y)
	
	return True



#======================================================================
if __name__=='__main__': 
	
	OutputPath=os.path.join(os.path.abspath(os.path.dirname(__file__)), "CIA_SyntheSys_output")
	Synthesize(CIA, CIA_Test, OutputPath=OutputPath, x=1.13,y=1.13) # No constraints





