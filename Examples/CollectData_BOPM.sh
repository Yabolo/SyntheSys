#! /bin/bash

# Cette fonction permet de quitter après l'échec d'une commande
function test {
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
        echo "error with $1" >&2
        exit
    fi
    return $status
}

test python3 BinomialTree.py --tabsize=3 synthesize -a VC707 --host=CIME_TIMA
test python3 BinomialTree.py --tabsize=4 synthesize -a VC707 --host=CIME_TIMA
test python3 BinomialTree.py --tabsize=5 synthesize -a VC707 --host=CIME_TIMA
test python3 BinomialTree.py --tabsize=6 synthesize -a VC707 --host=CIME_TIMA
test python3 BinomialTree.py --tabsize=7 synthesize -a VC707 --host=CIME_TIMA
test python3 BinomialTree.py --tabsize=8 synthesize -a VC707 --host=CIME_TIMA
test python3 BinomialTree.py --tabsize=9 synthesize -a VC707 --host=CIME_TIMA
test python3 BinomialTree.py --tabsize=10 synthesize -a VC707 --host=CIME_TIMA
test python3 BinomialTree.py --tabsize=11 synthesize -a VC707 --host=CIME_TIMA
test python3 BinomialTree.py --tabsize=12 synthesize -a VC707 --host=CIME_TIMA
test python3 BinomialTree.py --tabsize=13 synthesize -a VC707 --host=CIME_TIMA
test python3 BinomialTree.py --tabsize=14 synthesize -a VC707 --host=CIME_TIMA
test python3 BinomialTree.py --tabsize=15 synthesize -a VC707 --host=CIME_TIMA
