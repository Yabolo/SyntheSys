#! /bin/bash

if [ -z "$1" ]; then
	echo "No .bit file supplied."
	exit
fi
CurDir=`pwd`
BitDirname=`dirname $1`
BitBasename=`basename $1`
DesignName="${BitBasename%.*}"
TarPath=$CurDir/$DesignName.tar
echo BitDirname: $BitDirname
cd $BitDirname
tar -v -cvf $DesignName.tar $BitBasename
mv -v $DesignName.tar ../../../
cd ../../../
echo "moved to" `pwd`
tar -v --append --file=$DesignName.tar *.bin
mv -v $DesignName.tar $CurDir
cd $CurDir
echo TarPath: $TarPath
scp $TarPath HUBERTCURIEN:/home_nfs/pam19445/
rm $TarPath



