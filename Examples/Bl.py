#!/usr/bin/python


import sys, os
import logging
#======================================================================
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "..")))

from SyntheSys import Algorithm, Testbench, Synthesize
from SyntheSys.Library.ImageProcessing import Blue

#--------------------------------------------------------------------
@Algorithm() # Mean that this method will be HighLevelModule algorithm to be synthesized.
def Bl(x,y,z):
	"""
	Design core algorithm : read sequentially but understood as a parallel algorithm.
	Here HighLevelModules can be instanciated (a call for their algorithm methods).
	"""		
	return Blue(x,y,z)
	
#--------------------------------------------------------------------
@Testbench() # Mean that this method will be HighLevelModule testbench
def Bl_Test():
	"""
	Test bench for mean processing HighLevelModule: sequential.
	"""
	for x,y,z in [(1,2,3), (4,5,6), (7,8,9)]:
		r=Bl(x,y,z)
	
	return True



#======================================================================
if __name__=='__main__': 
	
	OutputPath=os.path.join(os.path.abspath(os.path.dirname(__file__)), "Bl_SyntheSys_output")
	Synthesize(Bl, Bl_Test, OutputPath=OutputPath, x=1,y=2,z=3) # No constraints





