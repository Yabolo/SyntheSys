#!/usr/bin/python


import sys, os
import logging
#======================================================================
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "..")))

from SyntheSys import Algorithm, Testbench, Synthesize
from SyntheSys.Library.ImageProcessing import RegionMean

#--------------------------------------------------------------------
@Algorithm() # Mean that this method will be HighLevelModule algorithm to be synthesized.
def MultVectors(DataList):
	"""
	Design core algorithm : read sequentially but understood as a parallel algorithm.
	Here HighLevelModules can be instanciated (a call for their algorithm methods).
	"""		
	return RegionMean(DataList)
	
#--------------------------------------------------------------------
@Testbench() # Mean that this method will be HighLevelModule testbench
def MultVectors_Test():
	"""
	Test bench for mean processing HighLevelModule: sequential.
	"""
	Data=[2, 2, 4, 4, 8, 8, 10, 10, 12]
	for i in range(1, 10):
		#=====================
		r=Mean([D for D in Data])
		#=====================
	
	return True



#======================================================================
if __name__=='__main__': 
	
	OutputPath=os.path.join(os.path.abspath(os.path.dirname(__file__)), "Mean_SyntheSys_output")
	Synthesize(Mean, Mean_Test, OutputPath=OutputPath, DataList=range(9)) # No constraints





