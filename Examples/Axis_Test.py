#!/usr/bin/python


import sys, os
import logging
#======================================================================
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "..")))

from SyntheSys import Algorithm, Testbench, Synthesize
from SyntheSys.Library.ImageProcessing import AxisXYZ

#--------------------------------------------------------------------
@Algorithm() # Mean that this method will be HighLevelModule algorithm to be synthesized.
def Axis(x, y, z):
	"""
	Design core algorithm : read sequentially but understood as a parallel algorithm.
	Here call for library functions.
	"""
	a=2*x
	b=3*y
	c=4*z
	d=x+y
	e=y+z
	f=d+z
#	print("ARG1:", [a,b,c,d,e,x,y,z,f])
#	print("ARG2:", [a,b,c,d,e,x,y,z,f])
#	input()
	return AxisXYZ([a,b,c,d,e,f,x,y,z],[a,b,c,d,e,f,x,y,z])
	
#--------------------------------------------------------------------
@Testbench() # Mean that this method will be HighLevelModule testbench
def Axis_Test():
	"""
	Test bench for mean processing HighLevelModule: sequential.
	"""
#	Data=[2, 2, 4, 4, 8, 8, 10, 10, 12]
#	for i in range(1, 10):

	#=====================
	for T in [(1,2,3), (4,5,6), (7,8,9)]:
		r=Axis(*T)
	#=====================
	
	return True



#======================================================================
if __name__=='__main__': 
	
	if len(sys.argv)<2:
		Axis_Test()
		sys.exit(0)
	OutputPath=os.path.join(os.path.abspath(os.path.dirname(__file__)), "Axis_SyntheSys_output")
	Synthesize(Axis, Axis_Test, OutputPath=OutputPath, x=1, y=2, z=3) # No constraints





