#!/usr/bin/python

import sys, os
import logging
#======================================================================
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "../..")))

from SyntheSys import Algorithm, Testbench, Synthesize
#sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "..", "Library", "SW")))


#--------------------------------------------------------------------
@Algorithm() # Mean that this method will be HighLevelModule algorithm to be synthesized.
def CondBranch(a, b, Op=0):
	"""
	Design core algorithm : read sequentially but interpreted as a parallel algorithm.
	Here HighLevelModules can be instanciated (a call for their algorithm methods).
	"""	
	if Op==7:
		c=a+b
#			d=c+1
#			e=d-b		
#			return a+b#c
	elif Op==12:
		c=a-b
#			e=a-6
#			return a-b#c
	else:
		c=a*b
#			d=c-1
#			e=d+b
#			while(c+10):
#				c-=1
#				print("c:",c)
#			return a*b#c
#		c=c-1
	return c
#--------------------------------------------------------------------
@Testbench() # Mean that this method will be HighLevelModule testbench
def TestBench_CondBranch():
	"""
	Test bench for ALU HighLevelModule: sequential.
	"""
#		InitValues = [0 for i in range(self.__TABSIZE__)]
#		InitValues[0]=1
	#=====================
	r=CondBranch(a=10, b=5, Op=7)
#	r=CondBranch(a=9, b=4, Op=12)
#	r=CondBranch(a=8, b=3, Op=0)
	#=====================
#			LastVal=InitValues.pop(-1)
#			InitValues.insert(0, LastVal)
#		raw_input("[TESTBENCH] Result='{0}'({1})".format(r, r._PythonVar))
	
	return True


#======================================================================
if __name__=='__main__': 

	Synthesize(CondBranch, TestBench_CondBranch, OutputPath="./CondBranch_SyntheSys_output", a=1, b=1, Op=7) # No constraints




