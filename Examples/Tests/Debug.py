#!/usr/bin/python



import sys, os
import logging
#======================================================================
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "..", "..")))

from SyntheSys import Algorithm, Testbench, Synthesize
from SyntheSys.Library.SW.Processing import TestNode

#--------------------------------------------------------------------
@Algorithm() # Mean that this method will be HighLevelModule algorithm to be synthesized.
def Debug(I):
	"""
	Design core algorithm : read sequentially but understood as a parallel algorithm.
	Here HighLevelModules can be instanciated (a call for their algorithm methods).
	"""		
	return TestNode(I)
	
#--------------------------------------------------------------------
@Testbench() # Mean that this method will be HighLevelModule testbench
def Debug_Stimuli():
	"""
	Stimuli for TestNode.
	"""
	for i in range(1, 11):
		#=====================
		r=Debug(i)
		#=====================
	
	return True



sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "..", "..")))
from Utilities.Timer import Timer

#======================================================================
if __name__=='__main__': 
	
	Synthesize(Debug, Debug_Stimuli, OutputPath="./TestNode_SyntheSys_output", I=7) # No constraints





