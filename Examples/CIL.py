#!/usr/bin/python


import sys, os
import logging
#======================================================================
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "..")))

from SyntheSys import Algorithm, Testbench, Synthesize
from SyntheSys.Library.ImageProcessing import CIE_L

#--------------------------------------------------------------------
@Algorithm() # Mean that this method will be HighLevelModule algorithm to be synthesized.
def CIL(y):
	"""
	Design core algorithm : read sequentially but understood as a parallel algorithm.
	Here HighLevelModules can be instanciated (a call for their algorithm methods).
	"""		
	return CIE_L(y)
	
#--------------------------------------------------------------------
@Testbench() # Mean that this method will be HighLevelModule testbench
def CIL_Test():
	"""
	Test bench for mean processing HighLevelModule: sequential.
	"""
	#for x,y in [(1.13,1.13), (0.478,1.24), (2.45,5.86)]:
		#r=CIA(x,y)
	r=CIL(1.13)
	#for y in [1.13, 0.478, 2.45]:
		#r=CIL(y)
	return True



#======================================================================
if __name__=='__main__': 
	
	OutputPath=os.path.join(os.path.abspath(os.path.dirname(__file__)), "CIL_SyntheSys_output")
	Synthesize(CIL, CIL_Test, OutputPath=OutputPath, y=1.13) # No constraints





