#include "stdio.h"
#include "math.h"
#include "time.h"
#include "pthread.h"
#define longueur 980
#define region 1
#define data 4096
#define NUM_THREADS_AXIS 6
#define NUM_THREADS_RGB 6
#define NUM_THREADS_LAB 6
#define NUM_THREADS_DISTANCE 4

struct axis_param {
	float* tab;
	float* tab2;
	float res;
};

struct rgb_param {
	float X;
	float Y;
	float Z;
	float color;
};

struct cie_param {
	float X;
	float Y;
	float res;
};

/*lecture des donnees originaux*/
void lecture(float tabdata[], const char* filename)
{
	long int i;
	
	FILE *fp;
	if((fp=fopen(filename, "rt"))==NULL)
	{
		printf("File %s cann't be opened\n", filename);
	}
    else
		for(i=0;i<data;i++)
        {
			fscanf(fp, "%f", &tabdata[i]);
			//tabdata[i]=n;
        }
    fclose(fp);
}

/*Enregistre les moyennes dans fichier tab1  */
void ecrituredata(float result[], const char* filename)
{
	int i;
	
	FILE *fp;
	
	fp=fopen(filename,"wr");
	for(i=0;i<longueur;i++)
		fprintf(fp,"%f\n", result[i]);
	
	fclose(fp);
	
}

/*calculer le moyenne1*/
void cal_moyenne(float tabdata[], const char* filename)
{
	float sum, moyenne[longueur];
	
	//moyenne par ligne de tabdata
	for (int i=0; i< longueur; i++)
	{
        sum=0;
        for(int j=0;j<data;j++)
		{
			sum=sum+tabdata[j];
		}
		moyenne[i]=sum/data;
	}
	ecrituredata(moyenne, filename);
}

/* -----------  PROJECTION COULEUR ------------------- */
/*--CIE X Y Z--*/
void* axisXYZ(void* param)
{
	struct axis_param* px;
	px = (struct axis_param*) param;
	int i;
	float sum_axis=0;
	for(i=0;i<longueur;i++)
	{
		sum_axis=px->tab[i]*px->tab2[i]+sum_axis;
	}
	px->res = sum_axis;
	//return sum_axis;
	pthread_exit((void*) param);
}

/*-- CIE XYZ--> RGB --*/
void* rouge(void* param)
{
	
	struct rgb_param* cp;
	cp = (struct rgb_param*) param;
	int i;
	float red=0;
	for(i=0; i<longueur; i++)
	{
		red = 2.37067*cp->X - 0.513885*cp->Y + 0.005298*cp->Z;
	}
	cp->color = red ;
	pthread_exit((void*) param);
}

void* vert(void* param)
{
	
	struct rgb_param* cp;
	cp = (struct rgb_param*) param;
	int i;
	float green = 0;
	for(i=0; i<longueur; i++)
	{
		green = 1.42530*cp->Y - 0.900040*cp->X - 0.014695*cp->Z;
	}
	cp->color = green ;
	pthread_exit((void*) param);
}

void* bleu(void* param)
{
	
	struct rgb_param* cp;
	cp = (struct rgb_param*) param;
	int i;
	float blue = 0;
	for(i=0; i<longueur; i++)
	{
		blue = 0.088581*cp->Y - 0.470634*cp->X - 1.00940*cp->Z;
	}
	cp->color = blue ;
	pthread_exit((void*) param);
}


/*-- CIE XYZ--> Lab --*/
void* CIE_L( void* param)
{
	struct cie_param* cie;
	cie = (struct cie_param*) param;
    float val_l,  var_Y ;
    var_Y = cie->Y/100.000;
    if (var_Y>0.008856)
        var_Y = pow(var_Y,1/3);
    else
        var_Y = ( 7.787*var_Y ) + ( 16 / 116 );
    val_l=( 116 * var_Y ) - 16;
	
	cie->Y = val_l;
	pthread_exit((void*) param);
}


float CIE_a (void* param)
{
	struct cie_param* cie;
	cie = (struct cie_param*) param;

    float val_a, var_X, var_Y;
    var_X=cie->X/95.047;
    var_Y=cie->Y/100.000;
	if (var_X > 0.008856)
        var_X = pow(var_X,1/3);
	else
        var_X = (903.3*var_X + 16)/116;
	if (var_Y > 0.008856)
		var_Y = pow(var_Y,1/3);
	else
		var_Y = (903.3*var_Y + 16)/116;
	
    val_a=500*(var_X-var_Y);
	cie->res = val_a;
	pthread_exit((void*) param);

   // return val_a;
}
/*
float CIE_L( float Y)
{
    float val_l,  var_Y ;
    var_Y = Y/100.000;
    if (var_Y>0.008856)
        var_Y = pow(var_Y,1/3);
    else
        var_Y = ( 7.787*var_Y ) + ( 16 / 116 );
    val_l=( 116 * var_Y ) - 16;
    return val_l;
}
 
float CIE_a (float X, float Y)
{
    float val_a, var_X, var_Y;
    var_X=X/95.047;
    var_Y=Y/100.000;
	if (var_X > 0.008856)
        var_X = pow(var_X,1/3);
	else
        var_X = (903.3*var_X + 16)/116;
	if (var_Y > 0.008856)
		var_Y = pow(var_Y,1/3);
	else
		var_Y = (903.3*var_Y + 16)/116;
	
    val_a=500*(var_X-var_Y);
    return val_a;
}
 */


float CIE_b (float Y, float Z)
{
	float val_b, var_Y, var_Z;
	var_Y=Y/100.000;
	var_Z=Z/108.883;
	if (var_Y > 0.008856)
		var_Y = pow(var_Y,1/3);
	else
		var_Y = (903.3 * var_Y + 16)/116;
	if (var_Z>0.008856)
        var_Z= pow(var_Z,1/3);
	else
        var_Z = (903.3 * var_Z + 16)/116;
	val_b=200*(var_Y-var_Z);
	return val_b;
}

/* -----------  Calcule de distance ------------------- */
/*-- Distance RMS -- */
float distance_RMS (float tab1[],float tab2[])
{
	float sum=0;
	float dis_rms;
	for (int i=0; i<longueur; i++)
		sum = pow (tab1[i]-tab2[i],2)+sum;
	dis_rms = sqrt(sum)/longueur;
	return dis_rms;
}

/*-- Distance WRMS --*/
float distance_WRMS(float tab1[],float tab2[])
{
	float sum=0;
	float dis_wrms;
	for(int i=0;i<longueur;i++)
	{
		sum= fabs(tab1[i]-tab2[i])*fabs(tab1[i]-tab2[i])/(sqrt(tab1[i])*sqrt(tab2[i]))+sum;
	}
	dis_wrms=sqrt(sum/longueur);
	return dis_wrms;
}

/*-- Distance #E (R,G,B) --*/
float distance_RGB(float R1, float G1,float B1,float R2,float G2,float B2)
{
	float deta_E;
	deta_E= sqrt((R1-R2)*(R1-R2)+(G1-G2)*(G1-G2)+(B1-B2)*(B1-B2));
	return deta_E;
}

/*-- Distance GFC --*/
float distance_GFC(float tab_Es[], float tab_Me[])
{
	float sum_Es=0;
	float sum_Me=0;
	float sum_EM=0;
	float GFC;
	for (int i=0;i<longueur;i++)
	{
		sum_Es=tab_Es[i]*tab_Es[i]+sum_Es;
		sum_Me=tab_Me[i]*tab_Me[i]+sum_Me;
		sum_EM=tab_Es[i]*tab_Me[i]+sum_EM;
	}
	GFC=fabs(sum_EM)/(sqrt(sum_Es)*sqrt(sum_Me));
	return GFC;
}

void launch_thread_axisXYZ(float tab[], float tabref[], float X, pthread_t* t_thread_axis, pthread_attr_t* attr) 
{

	struct axis_param px1 = {tab, tabref, X};
	int rc = pthread_create(t_thread_axis, attr, axisXYZ, (void *) &px1); 
	if (rc) {
		printf("ERROR; return code from pthread_create() is %d\n", rc);
	}
}

//int color : 0 for red, 1 for green, 2 for blue
float launch_thread_rgb(float X, float Y, float Z, pthread_t* t_thread_rgb, pthread_attr_t* attr, int color) 
{
	if (color > 2) {
		printf("ERROR, no matching for color = %i (color is 0, 1 or 2)", color);
	}
	struct rgb_param cp = {X, Y, Z, 0};
	int rc = 0;
	switch (color) {
		//red
		case 0:
			rc = pthread_create(t_thread_rgb, attr, rouge, (void *) &cp); 
			break;
		// green
		case 1:
			rc = pthread_create(t_thread_rgb, attr, vert, (void *) &cp); 
			break;
		//blue
		case 2:
			rc = pthread_create(t_thread_rgb, attr, bleu, (void *) &cp); 
			break;
	}
	
	if (rc) {
		printf("ERROR; return code from pthread_create() is %d\n", rc);
	}
	
	return cp.color;
}

//int mode : 0 for L, 1 for a, 2 for b
float launch_thread_Lab(float X, float Y, pthread_t* t_thread_Lab, pthread_attr_t* attr, int mode) 
{
	if (mode > 2) {
		printf("ERROR, no matching for mode = %i (mode is 0, 1 or 2)", mode);
	}
	struct cie_param cie = {X, Y, 0};
	int rc = 0;
	switch (mode) {
			//red
		case 0:
			rc = pthread_create(t_thread_Lab, attr, CIE_L, (void *) &cie); 
			break;
			// green
		case 1:
			rc = pthread_create(t_thread_Lab, attr, CIE_a, (void *) &cie); 
			break;
			//blue
		case 2:
			rc = pthread_create(t_thread_Lab, attr, CIE_b, (void *) &cie); 
			break;
	}
	
	if (rc) {
		printf("ERROR; return code from pthread_create() is %d\n", rc);
	}
	
	return cie.res;
}


int main()
{
	clock_t start, finish;
	double duration;
	float tab1[longueur];
	float tab2[longueur];
	float tabrefX[longueur];
	float tabrefY[longueur];
	float tabrefZ[longueur];
	float X1,X2,Y1,Y2,Z1,Z2;
	float r1,g1,b1,r2,g2,b2;
	float Lab1[3],Lab2[3];
	float dis_RMS;
	float dis_WRMS;
	float dis_detaE;
	float dis_GFC;
	//float moyenne1[longueur];
	float tabdata[data],tabdata2[data];
	void *status;
	
	// variable pour rendre les thread joignable
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    for (int i=0; i<region; i++)
	{

		
/////////////////////////
// lecture des données
/////////////////////////
		
		lecture(tabdata, "tabdata.dat");
		lecture(tabdata2, "tabdata2.dat");
		lecture(tab1, "tab1.dat");
		lecture(tab2, "tab2.dat");
		lecture(tabrefX, "tabrefX.dat");
		lecture(tabrefY, "tabrefY.dat");
		lecture(tabrefZ, "tabrefZ.dat");
		
/////////////////////////
// calcul des moyennes
/////////////////////////
		
		cal_moyenne(tabdata, "tab1.dat");
		cal_moyenne(tabdata2, "tab2.dat");
		
		// ouverture du fichier ne dépend pas du temps de l'algo
		start = clock();
		
		pthread_t t_thread_axis[NUM_THREADS_AXIS];
		pthread_t t_thread_rgb[NUM_THREADS_RGB];
		pthread_t t_thread_lab[NUM_THREADS_LAB];
		pthread_t t_thread_distance[NUM_THREADS_DISTANCE];
		
/////////////////////////
// calcul des valeurs CIE
/////////////////////////
		
		/*------Calculer la valeur CIE XYZ-----*/
		launch_thread_axisXYZ(tab1, tabrefX, X1, &t_thread_axis[0], &attr);
		launch_thread_axisXYZ(tab2, tabrefX, X2, &t_thread_axis[1], &attr);
		launch_thread_axisXYZ(tab1, tabrefY, Y1, &t_thread_axis[2], &attr);
		launch_thread_axisXYZ(tab2, tabrefY, Y2, &t_thread_axis[3], &attr);
		launch_thread_axisXYZ(tab1, tabrefZ, Z1, &t_thread_axis[4], &attr);
		launch_thread_axisXYZ(tab2, tabrefZ, Z2, &t_thread_axis[5], &attr);
		
		// join the thread for axis calcul
		for(int i=0; i<NUM_THREADS_AXIS; i++) 
		{
			int rc = pthread_join(t_thread_axis[i], &status);
			if (rc) {
				printf("ERROR; return code from pthread_join() is %d\n", rc);
			}
			printf("Main: completed join with thread %i having a status of %ld\n", i, (long)status);
		}		
		
/////////////////////////
// calcul des valeurs RGB
/////////////////////////
		
		r1 = launch_thread_rgb(X1, Y1, Z1, &t_thread_rgb[0], &attr, 0);
		r2 = launch_thread_rgb(X2, Y2, Z2, &t_thread_rgb[1], &attr, 0);
		
		g1 = launch_thread_rgb(X1, Y1, Z1, &t_thread_rgb[2], &attr, 1);
		g2 = launch_thread_rgb(X2, Y2, Z2, &t_thread_rgb[3], &attr, 1);
		
		b1 = launch_thread_rgb(X1, Y1, Z1, &t_thread_rgb[4], &attr, 2);
		b2 = launch_thread_rgb(X2, Y2, Z2, &t_thread_rgb[5], &attr, 2);

		// join the thread for axis calcul
		for (int i=0; i<NUM_THREADS_RGB; i++) 
		{
			int rc = pthread_join(t_thread_rgb[i], &status);
			if (rc) {
				printf("ERROR; return code from pthread_join() is %d\n", rc);
			}
			printf("Main: completed join with thread %i having a status of %ld\n", i, (long)status);
		}
		// print results
		printf("RGB is:\nr1=%f\nr2=%f\ng1=%f\ng2=%f\nb1=%f\nb2=%f\n",r1,r2,g1,g2,b1,b2);    
		
		
/////////////////////////
// calcul des valeurs L*ab
/////////////////////////
				
		/*------Calculer la valeur L*ab-----*/
		
		
		/*
		Lab1[0]= CIE_L(Y1);
		Lab2[0]= CIE_L(Y2);
		Lab1[1]= CIE_a (X1, Y1);
		Lab2[1]= CIE_a (X2, Y2);
		Lab1[2]= CIE_b (Y1, Z1);
		Lab2[2]= CIE_b (Y2, Z2);
		 */
		printf ("L*ab is:\nLab1=%f %f %f\nLab2=%f %f %f\n", Lab1, Lab2);
		
		
		// faire un join ici
		
		
/////////////////////////
// calcul des valeurs RMS
/////////////////////////

		/*-------Calculer distance RMS-------*/
		dis_RMS=distance_RMS(tab1,tab2);
		/*  printf ("Distance RMS is: %f\n",dis_RMS);   */
		
		/*-------Calculer distance WRMS------*/
		dis_WRMS=distance_WRMS(tab1,tab2);
		/* printf ("Distance WRMS is: %f\n",dis_WRMS);   */
				
		/*-------Calculer distance deaERGB------*/
		dis_detaE=distance_RGB(r1,r2,g1,g2,b1,b2);
		/* printf ("Distance detaE is: %f\n",dis_detaE);  */
		
		/*-------Calculer distance GFC------*/
		dis_GFC=distance_GFC(tab1,tab2);
		/*  printf ("Distance GFC is: %f\n",dis_GFC);   */
		
		// faire un join ici

		
		finish=clock();
	}
	
	
	//duration = (double)(finish - start)/CLK_TCK ;
	duration = (double)(finish - start)/1000;
	printf("Calculer distance GFC:%f\n",dis_GFC);
	printf ("Programme est fini!\n");
	printf("Time to do Programme is: %f seconds\n",duration);
	//getch();
	return 0;
}










