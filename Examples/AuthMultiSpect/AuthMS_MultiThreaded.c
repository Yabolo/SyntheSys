#include "stdio.h"
#include "math.h"
#include "time.h"
#include "pthread.h"
#define longueur 980
#define region 1
#define data 4096
#define NUM_THREADS_AXIS 6
#define NUM_THREADS_RGB 6
#define NUM_THREADS_LAB 6
#define NUM_THREADS_DISTANCE 4

struct axis_param {
	double* tab;
	double* tab2;
	double res;
};

struct rgb_param {
	double X;
	double Y;
	double Z;
	double color;
};

struct cie_param {
	double X;
	double Y;
	double res;
};

double MoyenneOR[longueur];
double MoyenneCR[longueur];

struct rgb_param ParamR;
struct rgb_param ParamG;
struct rgb_param ParamB;

/*lecture des donnees originaux*/
void lecture(double tabdata[], const char* filename)
{
	long int i;
	
	FILE *fp;
	if((fp=fopen(filename, "rt"))==NULL)
	{
		printf("File %s cann't be opened\n", filename);
	}
    else
		for(i=0;i<data;i++)
        {
			fscanf(fp, "%lf", &tabdata[i]);
			//tabdata[i]=n;
        }
    fclose(fp);
}

/*Enregistre les moyennes dans fichier tab1  */
void ecrituredata(double result[], const char* filename)
{
	int i;
	
	FILE *fp;
	
	fp=fopen(filename,"wr");
	for(i=0;i<longueur;i++)
		fprintf(fp,"%lf\n", result[i]);
	
	fclose(fp);
	
}


/*calculer le moyenne1*/
void cal_moyenne_OR(double tabdata[], const char* filename)
{
	double sum;/*, moyenne[longueur];*/
	
	//moyenne par ligne de tabdata
	for (int i=0; i< longueur; i++)
	{
        sum=0;
        for(int j=0;j<data;j++)
		{
			sum=sum+tabdata[j];
		}
		MoyenneOR[i]=sum/data;
	}
/*	ecrituredata(moyenne, filename);*/
}
/*calculer le moyenne1*/
void cal_moyenne_CR(double tabdata[], const char* filename)
{
	double sum;/*, moyenne[longueur];*/
	
	//moyenne par ligne de tabdata
	for (int i=0; i< longueur; i++)
	{
        sum=0;
        for(int j=0;j<data;j++)
		{
			sum=sum+tabdata[j];
		}
		MoyenneCR[i]=sum/data;
	}
/*	ecrituredata(moyenne, filename);*/
}

/* -----------  PROJECTION COULEUR ------------------- */
/*--CIE X Y Z--*/
void* axisXYZ(void* param)
{
	struct axis_param* px;
	px = (struct axis_param*) param;
	int i;
	double sum_axis=0;
	for(i=0;i<longueur;i++)
	{
		sum_axis=px->tab[i]*px->tab2[i]+sum_axis;
	}
	px->res = sum_axis;
	//return sum_axis;
	pthread_exit((void*) param);
}

/*-- CIE XYZ--> RGB --*/
void* rouge()
{
	
/*	struct rgb_param* cp;*/
/*	cp = (struct rgb_param*) param;*/
	int i;
	float red=0;
	for(i=0; i<longueur; i++)
	{
		red = 2.37067*ParamR.X - 0.513885*ParamR.Y + 0.005298*ParamR.Z;
	}
	ParamR.color = red ;
	pthread_exit(NULL);
}

void* vert()
{
	
/*	struct rgb_param* cp;*/
/*	cp = (struct rgb_param*) param;*/
	int i;
	float green = 0;
	for(i=0; i<longueur; i++)
	{
		green = 1.42530*ParamG.Y - 0.900040*ParamG.X - 0.014695*ParamG.Z;
	}
	ParamG.color = green ;
	pthread_exit(NULL);
}

void* bleu()
{
	
/*	struct rgb_param* cp;*/
/*	cp = (struct rgb_param*) param;*/
	int i;
	float blue = 0;
	for(i=0; i<longueur; i++)
	{
		blue = 0.088581*ParamB.Y - 0.470634*ParamB.X - 1.00940*ParamB.Z;
	}
	ParamB.color = blue ;
	pthread_exit(NULL);
}


/*-- CIE XYZ--> Lab --*/
void* CIE_L( void* param)
{
	struct cie_param* cie;
	cie = (struct cie_param*) param;
    double val_l,  var_Y ;
    var_Y = cie->Y/100.000;
    if (var_Y>0.008856)
        var_Y = pow(var_Y,1/3);
    else
        var_Y = ( 7.787*var_Y ) + ( 16 / 116 );
    val_l=( 116 * var_Y ) - 16;
	
	cie->Y = val_l;
	pthread_exit((void*) param);
}


double CIE_a (void* param)
{
	struct cie_param* cie;
	cie = (struct cie_param*) param;

    double val_a, var_X, var_Y;
    var_X=cie->X/95.047;
    var_Y=cie->Y/100.000;
	if (var_X > 0.008856)
        var_X = pow(var_X,1/3);
	else
        var_X = (903.3*var_X + 16)/116;
	if (var_Y > 0.008856)
		var_Y = pow(var_Y,1/3);
	else
		var_Y = (903.3*var_Y + 16)/116;
	
    val_a=500*(var_X-var_Y);
	cie->res = val_a;
	pthread_exit((void*) param);

   // return val_a;
}
/*
double CIE_L( double Y)
{
    double val_l,  var_Y ;
    var_Y = Y/100.000;
    if (var_Y>0.008856)
        var_Y = pow(var_Y,1/3);
    else
        var_Y = ( 7.787*var_Y ) + ( 16 / 116 );
    val_l=( 116 * var_Y ) - 16;
    return val_l;
}
 
double CIE_a (double X, double Y)
{
    double val_a, var_X, var_Y;
    var_X=X/95.047;
    var_Y=Y/100.000;
	if (var_X > 0.008856)
        var_X = pow(var_X,1/3);
	else
        var_X = (903.3*var_X + 16)/116;
	if (var_Y > 0.008856)
		var_Y = pow(var_Y,1/3);
	else
		var_Y = (903.3*var_Y + 16)/116;
	
    val_a=500*(var_X-var_Y);
    return val_a;
}
 */


double CIE_b (double Y, double Z)
{
	double val_b, var_Y, var_Z;
	var_Y=Y/100.000;
	var_Z=Z/108.883;
	if (var_Y > 0.008856)
		var_Y = pow(var_Y,1/3);
	else
		var_Y = (903.3 * var_Y + 16)/116;
	if (var_Z>0.008856)
        var_Z= pow(var_Z,1/3);
	else
        var_Z = (903.3 * var_Z + 16)/116;
	val_b=200*(var_Y-var_Z);
	return val_b;
}

/* -----------  Calcule de distance ------------------- */
/*-- Distance RMS -- */
double distance_RMS (double tab1[],double tab2[])
{
	double sum=0;
	double dis_rms;
	for (int i=0; i<longueur; i++)
		sum = pow (tab1[i]-tab2[i],2)+sum;
	dis_rms = sqrt(sum)/longueur;
	return dis_rms;
}

/*-- Distance WRMS --*/
double distance_WRMS(double tab1[],double tab2[])
{
	double sum=0;
	double dis_wrms;
	for(int i=0;i<longueur;i++)
	{
		sum= fabs(tab1[i]-tab2[i])*fabs(tab1[i]-tab2[i])/(sqrt(tab1[i])*sqrt(tab2[i]))+sum;
	}
	dis_wrms=sqrt(sum/longueur);
	return dis_wrms;
}

/*-- Distance #E (R,G,B) --*/
double distance_RGB(double R1, double G1,double B1,double R2,double G2,double B2)
{
	double deta_E;
	deta_E= sqrt((R1-R2)*(R1-R2)+(G1-G2)*(G1-G2)+(B1-B2)*(B1-B2));
	return deta_E;
}

/*-- Distance GFC --*/
double distance_GFC(double tab_Es[], double tab_Me[])
{
	double sum_Es=0;
	double sum_Me=0;
	double sum_EM=0;
	double GFC;
	for (int i=0;i<longueur;i++)
	{
		sum_Es=tab_Es[i]*tab_Es[i]+sum_Es;
		sum_Me=tab_Me[i]*tab_Me[i]+sum_Me;
		sum_EM=tab_Es[i]*tab_Me[i]+sum_EM;
	}
	GFC=fabs(sum_EM)/(sqrt(sum_Es)*sqrt(sum_Me));
	return GFC;
}

void launch_thread_axisXYZ(double tab[], double tabref[], double X, pthread_t* t_thread_axis, pthread_attr_t* attr) 
{

	struct axis_param px1 = {tab, tabref, X};
	int rc = pthread_create(t_thread_axis, attr, axisXYZ, (void *) &px1); 
	if (rc) {
		printf("ERROR; return code from pthread_create() is %d\n", rc);
	}
}

//int color : 0 for red, 1 for green, 2 for blue
double launch_thread_rgb(double X, double Y, double Z, pthread_t* t_thread_rgb, pthread_attr_t* attr, int color) 
{
	if (color > 2) {
		printf("ERROR, no matching for color = %i (color is 0, 1 or 2)", color);
	}
/*	struct rgb_param cp = {X, Y, Z, 0};*/
	int rc = 0;
	switch (color) {
		//red
		case 0:
			ParamR.X=X;
			ParamR.Y=Y;
			ParamR.Z=Z;
			ParamR.color=0;
			rc = pthread_create(t_thread_rgb, attr, rouge, NULL); 
			if (rc) {
				printf("ERROR; return code from pthread_create() is %d\n", rc);}
			return ParamR.color;
			break;
		// green
		case 1:
			ParamG.X=X;
			ParamG.Y=Y;
			ParamG.Z=Z;
			ParamG.color=0;
			rc = pthread_create(t_thread_rgb, attr, vert, NULL); 
			if (rc) {
				printf("ERROR; return code from pthread_create() is %d\n", rc);}
			return ParamG.color;
			break;
		//blue
		case 2:
			ParamB.X=X;
			ParamB.Y=Y;
			ParamB.Z=Z;
			ParamB.color=0;
			rc = pthread_create(t_thread_rgb, attr, bleu, NULL); 
			if (rc) {
				printf("ERROR; return code from pthread_create() is %d\n", rc);}
			return ParamB.color;
			break;
		}
	
	
/*	return cp.color;*/
}

//int mode : 0 for L, 1 for a, 2 for b
/*double launch_thread_Lab(double X, double Y, pthread_t* t_thread_Lab, pthread_attr_t* attr, int mode) */
/*{*/
/*	if (mode > 2) {*/
/*		printf("ERROR, no matching for mode = %i (mode is 0, 1 or 2)", mode);*/
/*	}*/
/*	struct cie_param cie = {X, Y, 0};*/
/*	int rc = 0;*/
/*	switch (mode) {*/
/*			//red*/
/*		case 0:*/
/*			rc = pthread_create(t_thread_Lab, attr, CIE_L, (void *) &cie); */
/*			break;*/
/*			// green*/
/*		case 1:*/
/*			rc = pthread_create(t_thread_Lab, attr, CIE_a, (void *) &cie); */
/*			break;*/
/*			//blue*/
/*		case 2:*/
/*			rc = pthread_create(t_thread_Lab, attr, CIE_b, (void *) &cie); */
/*			break;*/
/*	}*/
/*	*/
/*	if (rc) {*/
/*		printf("ERROR; return code from pthread_create() is %d\n", rc);*/
/*	}*/
/*	*/
/*	return cie.res;*/
/*}*/


int main()
{
	clock_t start, finish;
	double Duration, TotalDuration=0;
	double tab1[longueur];
	double tab2[longueur];
	double tabrefX[longueur];
	double tabrefY[longueur];
	double tabrefZ[longueur];
	double X1,X2,Y1,Y2,Z1,Z2;
	double r1,g1,b1,r2,g2,b2;
	double Lab1[3],Lab2[3];
	double dis_RMS;
	double dis_WRMS;
	double dis_detaE;
	double dis_GFC;
	//double moyenne1[longueur];
	double tabdata[data],tabdata2[data];
	void *status;
	
	// variable pour rendre les thread joignable
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	for (int R=0; R<region; R++)
	{
/////////////////////////
// lecture des données
/////////////////////////
		
		printf("Lecture fichiers...\n");
		lecture(tabdata, "tabdata.dat");
		lecture(tabdata2, "tabdata2.dat");
/*		lecture(tab1, "tab1.dat");*/
/*		lecture(tab2, "tab2.dat");*/
		lecture(tabrefX, "tabrefX.dat");
		lecture(tabrefY, "tabrefY.dat");
		lecture(tabrefZ, "tabrefZ.dat");
		
/////////////////////////
// calcul des moyennes
/////////////////////////
		
		// ouverture du fichier ne dépend pas du temps de l'algo
		start = clock();
		
		printf("Moyenne...\n");
		cal_moyenne_OR(tabdata, "tab1.dat");
		cal_moyenne_CR(tabdata2, "tab2.dat");
		printf("Moyenne OK\n");
		
		pthread_t t_thread_axis[NUM_THREADS_AXIS];
		pthread_t t_thread_rgb[NUM_THREADS_RGB];
/*		pthread_t t_thread_lab[NUM_THREADS_LAB];*/
		pthread_t t_thread_distance[NUM_THREADS_DISTANCE];
		
/////////////////////////
// calcul des valeurs CIE
/////////////////////////
		
		/*------Calculer la valeur CIE XYZ-----*/
		printf("axisXYZ...\n");
		launch_thread_axisXYZ(MoyenneOR, tabrefX, X1, &t_thread_axis[0], &attr);
		launch_thread_axisXYZ(MoyenneCR, tabrefX, X2, &t_thread_axis[1], &attr);
		launch_thread_axisXYZ(MoyenneOR, tabrefY, Y1, &t_thread_axis[2], &attr);
		launch_thread_axisXYZ(MoyenneCR, tabrefY, Y2, &t_thread_axis[3], &attr);
		launch_thread_axisXYZ(MoyenneOR, tabrefZ, Z1, &t_thread_axis[4], &attr);
		launch_thread_axisXYZ(MoyenneCR, tabrefZ, Z2, &t_thread_axis[5], &attr);
		printf("axisXYZ launched\n");
		
		// join the thread for axis calcul
		for(int i=0; i<NUM_THREADS_AXIS; i++) 
		{
			printf("join the thread for axis calcul...\n");
			int rc = pthread_join(t_thread_axis[i], &status);
			if (rc) {
				printf("ERROR; return code from pthread_join() is %d\n", rc);
			}
			printf("%i: joined\n", i);
/*			printf("Main: completed join with thread %i having a status of %ld\n", i, (long)status);*/
		}		
		
/////////////////////////
// calcul des valeurs RGB
/////////////////////////
		
		printf("RGB...\n");
		r1 = launch_thread_rgb(X1, Y1, Z1, &t_thread_rgb[0], &attr, 0);
		r2 = launch_thread_rgb(X2, Y2, Z2, &t_thread_rgb[1], &attr, 0);
		
		g1 = launch_thread_rgb(X1, Y1, Z1, &t_thread_rgb[2], &attr, 1);
		g2 = launch_thread_rgb(X2, Y2, Z2, &t_thread_rgb[3], &attr, 1);
		
		b1 = launch_thread_rgb(X1, Y1, Z1, &t_thread_rgb[4], &attr, 2);
		b2 = launch_thread_rgb(X2, Y2, Z2, &t_thread_rgb[5], &attr, 2);
		printf("RGB: OK\n");

		// join the thread for axis calcul
		for (int i=0; i<NUM_THREADS_RGB; i++) 
		{
			int rc = pthread_join(t_thread_rgb[i], &status);
			printf("rc:%i\n", rc);
			if (rc) {
				printf("ERROR; return code from pthread_join() is %d\n", rc);
			}
/*			printf("Main: completed join with thread %i having a status of %ld\n", i, (long)status);*/
		}
		// print results
/*		printf("RGB is:\nr1=%lf\nr2=%lf\ng1=%lf\ng2=%lf\nb1=%lf\nb2=%lf\n",r1,r2,g1,g2,b1,b2);    */
		
		
/////////////////////////
// calcul des valeurs L*ab
/////////////////////////
		
		/*------Calculer la valeur L*ab-----*/
		
		
		/*
		Lab1[0]= CIE_L(Y1);
		Lab2[0]= CIE_L(Y2);
		Lab1[1]= CIE_a (X1, Y1);
		Lab2[1]= CIE_a (X2, Y2);
		Lab1[2]= CIE_b (Y1, Z1);
		Lab2[2]= CIE_b (Y2, Z2);
		 */
/*		printf ("L*ab is:\nLab1=%lf %lf %lf\nLab2=%lf %lf %lf\n", Lab1, Lab2);*/
		
		
		// faire un join ici
		
		
/////////////////////////
// calcul des valeurs RMS
/////////////////////////

		/*-------Calculer distance RMS-------*/
/*		dis_RMS=distance_RMS(MoyenneOR, MoyenneCR);*/
		/*  printf ("Distance RMS is: %lf\n",dis_RMS);   */
		
		/*-------Calculer distance WRMS------*/
		dis_WRMS=distance_WRMS(MoyenneOR, MoyenneCR);
		/* printf ("Distance WRMS is: %lf\n",dis_WRMS);   */
				
		/*-------Calculer distance deaERGB------*/
		dis_detaE=distance_RGB(r1,r2,g1,g2,b1,b2);
		/* printf ("Distance detaE is: %lf\n",dis_detaE);  */
		
		/*-------Calculer distance GFC------*/
/*		dis_GFC=distance_GFC(MoyenneOR, MoyenneCR);*/
		/*  printf ("Distance GFC is: %lf\n",dis_GFC);   */
		
		// faire un join ici

		
		finish=clock();
		printf("start=%f \n", start);
		printf("finish=%f \n", finish);
		Duration = (double)(finish - start)/1000;
		printf("* MS Region %i : %f seconds\n", R, Duration);
		
		TotalDuration+=Duration;
	}
	
	
	//Duration = (double)(finish - start)/CLK_TCK ;
/*	Duration = (double)(finish - start)/1000;*/
/*	printf("Calculer distance GFC:%lf\n",dis_GFC);*/
/*	printf ("Programme est fini!\n");*/
	printf("Time to do Programme is: %lf seconds\n",TotalDuration);
	//getch();
	return 0;
}










