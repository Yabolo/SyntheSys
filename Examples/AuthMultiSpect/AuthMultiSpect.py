#! /usr/bin/python3
import sys, os
import math

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))

from SyntheSys import Algorithm, Testbench, Synthesize
from SyntheSys.Library.SW.Processing.ImageProcessing import RegionMean, AxisXYZ, Red, Green, Blue, CIE_L, CIE_a, CIE_b, DistanceGFC, DistanceRGB, DistanceRMS, DistanceWRMS

NbMultSpecRegions=1604
SubRegionsSize=9
NbRegions=100


#lecture des donnees originaux---------------------
def Lecture(FilePath):
	Data=[]
	if not os.path.isfile(FilePath):
		print("File '{0}' cann't be opened".format(FilePath))
		sys.exit(1)
	with open(FilePath, "r") as DataFile:
		for Line in DataFile.readlines():
			F=float(Line.split()[0])
			Data.append(F)
	return Data

TabRefX=[] # 1604 values
TabRefY=[] # 1604 values
TabRefZ=[] # 1604 values

# Enregistre les moyennes dans fichier Tab1-------
def EcritureData(Results, FilePath):
	if not os.path.isfile(FilePath):
		print("File '{0}' cann't be opened".format(FilePath))
		sys.exit(1)
	with open(FilePath, "w+") as DataFile:
		for Result in Results:
			DataFile.write(str(Result)+"\n")

## Calcul des moyennes-----------------------------
#def RegionMean(DataList):
#	global Longueur
#	Moyenne=[]
#	for i in range(Longueur):
#		Sum=sum(DataList[:Longueur])
#		Moyenne.append(Sum/Longueur)
##	EcritureData(Results=Moyenne, FilePath="tab1.dat")
##	EcritureData(Results=Moyenne2, FilePath="tab2.dat")
#	return Moyenne

## ----------- PROJECTION COULEUR ------------------- 
##--CIE X Y Z--
#def AxisXYZ(TabSp, TabRef):
#	sum_axis=0
#	if len(TabSp)>len(TabRef):
#		print("Error: Reference values '{0}' < processing values '{1}'. Aborted.".format(len(TabRef), len(TabSp) ))
#		sys.exit(1)
#	for i in range(len(TabSp)):
#		sum_axis+=TabSp[i]*TabRef[i]
#	return sum_axis

##-- CIE XYZ--> RGB ---------------------------------
#def Red(X, Y, Z):
#	r=2.37067*X-0.513885*Y+0.005298*Z
#	return r

#def Green(X, Y, Z):
#	g=1.42530*Y-0.900040*X-0.014695*Z
#	return g

#def Blue(X, Y, Z):
#	b=0.088581*Y-0.470634*X+1.00940*Z
#	return b



##-- CIE XYZ--> Lab ---------------------------------
#def CIE_L(Y):
#	var_Y = Y/100.000
#	if var_Y>0.008856:
#		var_Y = pow(var_Y,1/3)
#	else:
#		var_Y = (7.787*var_Y) + (16/116)
#	val_l = (116*var_Y) - 16
#	return val_l

#def CIE_a(X, Y):
#	var_X=X/95.047
#	var_Y=Y/100.000
#	if var_X > 0.008856:
#		var_X = pow(var_X,1/3)
#	else:
#		var_X = (903.3*var_X + 16)/116
#	if var_Y > 0.008856:
#		var_Y = pow(var_Y,1/3)
#	else:
#		var_Y = (903.3*var_Y + 16)/116

#	val_a=500*(var_X-var_Y)
#	return val_a

#def CIE_b (Y, Z):
#	var_Y=Y/100.000
#	var_Z=Z/108.883
#	if var_Y > 0.008856:
#		var_Y = pow(var_Y,1/3)
#	else:
#		var_Y = (903.3 * var_Y + 16)/116
#	if var_Z>0.008856:
#		var_Z= pow(var_Z,1/3)
#	else:
#		var_Z = (903.3 * var_Z + 16)/116
#	val_b=200*(var_Y-var_Z)
#	return val_b

## -----------	Calcule de distance ------------------- 
##-- Distance RMS -- 
#def DistanceRMS(Tab1, Tab2):
#	global Longueur
#	Sum=0
#	for i in range(len(Tab1)):
#		Sum+=pow(Tab1[i]-Tab2[i], 2)
#	dis_rms=math.sqrt(Sum)/Longueur
#	return dis_rms


##-- Distance WRMS --
#def DistanceWRMS(Tab1, Tab2):
#	global Longueur
#	Sum=0
#	for i in range(len(Tab1)):
#		Sum=math.fabs(Tab1[i]-Tab2[i])*math.fabs(Tab1[i]-Tab2[i])/(math.sqrt(Tab1[i])*math.sqrt(Tab2[i]))+Sum
#	dis_wrms=math.sqrt(Sum/Longueur)
#	return dis_wrms


##-- Distance #E (R,G,B) --
#def DistanceRGB(R1, G1, B1, R2, G2, B2):
#	deta_E= math.sqrt((R1-R2)*(R1-R2)+(G1-G2)*(G1-G2)+(B1-B2)*(B1-B2))
#	return deta_E

##-- Distance GFC --
#def DistanceGFC(TabEs, TabMe):
#	SumEs=0
#	SumMe=0
#	SumEM=0
#	for i in range(len(TabEs)):
#		SumEs+=TabEs[i]*TabEs[i]
#		SumMe+=TabMe[i]*TabMe[i]
#		SumEM+=TabEs[i]*TabMe[i]
#	GFC=math.fabs(SumEM)/(math.sqrt(SumEs)*math.sqrt(SumMe))
#	return GFC


#==================================================================
@Algorithm() # Mean that this method will be HighLevelModule algorithm to be synthesized.
def AuthentifyMS(OriginalRegions, TestedRegions):
	global TabRefX, TabRefY, TabRefZ
	OriRegionMeans=[]
	TestedRegionMeans=[]
		
	for OriginalRegion, TestedRegion in zip(OriginalRegions, TestedRegions):
		OriRegionMeans.append(RegionMean(OriginalRegion))
		TestedRegionMeans.append(RegionMean(TestedRegion))

	#------Calculer la valeur CIE XYZ-----
	X1=AxisXYZ(OriRegionMeans,TabRefX)
	Y1=AxisXYZ(OriRegionMeans,TabRefY)
	Z1=AxisXYZ(OriRegionMeans,TabRefZ)
	
	X2=AxisXYZ(TestedRegionMeans,TabRefX)
	Y2=AxisXYZ(TestedRegionMeans,TabRefY)
	Z2=AxisXYZ(TestedRegionMeans,TabRefZ)
	#printf ("XYZ is:\nX1=%f\nX2=%f\nY1=%f\nY2=%f\nZ1=%f\nZ2=%f\n", X1,X2,Y1,Y2,Z1,Z2)

	#------Calculer la valeur RGB-----
	R1=Red(X1,Y1,Z1)
	R2=Red(X2,Y2,Z2)
	G1=Green(X1,Y1,Z1)
	G2=Green(X2,Y2,Z2)
	B1=Blue(X1,Y1,Z1)
	B2=Blue(X2,Y2,Z2)
	#printf("RGB is:\nr1=%f\nr2=%f\ng1=%f\ng2=%f\nb1=%f\nb2=%f\n",r1,r2,g1,g2,b1,b2)		


	#------Calculer la valeur L*ab-----
#	Lab1=[]
#	Lab2=[]
#	Lab1.append( CIE_L(Y1) )
#	Lab2.append( CIE_L(Y2) )
#	Lab1.append( CIE_a(X1, Y1) )
#	Lab2.append( CIE_a(X2, Y2) )
#	Lab1.append( CIE_b(Y1, Z1) )
#	Lab2.append( CIE_b(Y2, Z2) )
	#printf ("L*ab is:\nLab1=%f %f %f\nLab2=%f %f %f\n", Lab1,Lab2)

	# DISTANCE COULEUR
	#-------Calculer distance deaERGB------
	dis_RGB=DistanceRGB(R1,R2,G1,G2,B1,B2)
	# printf ("Distance detaE is: %f\n",dis_detaE)
#	if dis_RGB > 1.0:
#		Authentified=False
##		return False
#	# DISTANCE MULTISPECTRALE
#	#-------Calculer distance RMS-------
##	dis_RMS=DistanceRMS(OriRegionMeans,TestedRegionMeans)
#	# printf ("Distance RMS is: %f\n",dis_RMS)
#	else:
	#-------Calculer distance WRMS------
	dis_WRMS=DistanceWRMS(OriRegionMeans,TestedRegionMeans)
#		Authentified=dis_WRMS < 1.0
	# printf ("Distance WRMS is: %f\n",dis_WRMS)

	#-------Calculer distance GFC------
#	dis_GFC=DistanceGFC(OriRegionMeans,TestedRegionMeans)
	# printf ("Distance GFC is: %f\n",dis_GFC)
#	return Authentified
#	return dis_RMS, dis_WRMS, dis_RGB, dis_GFC
	return dis_RGB, dis_WRMS

#==================================================================
import time
class Timer():
 	#----------------------------------------------------------
	def __init__(self, verbose=False):
		self.StartTime = 0
		self.EndTime   = 0
 	#----------------------------------------------------------
	def __enter__(self):
		self.StartTime = time.process_time()
		return self
 	#----------------------------------------------------------
	def __exit__(self, *args):
		self.EndTime = time.process_time()
 	#----------------------------------------------------------
	def AsString(self):
		Delta = self.EndTime - self.StartTime
		h         = int(Delta/3600)
		m         = int((Delta-h*3600)/60)
		s         = int((Delta-h*3600-m*60))
		millisecs = int((Delta-h*3600-m*60-s)*1000)
		microsec  = int((Delta-h*3600-m*60-s-millisecs/1000)*1000000)
		return '{0}h {1}min {2}s {3}ms {4}µs'.format(h,m,s, millisecs, microsec)

#==================================================================
@Testbench() # Mean that this method will be HighLevelModule testbench
def AuthentifyMS_Test():
	"""
	Test bench for the authentication.
	"""
	global SubRegionsSize
	global NbMultSpecRegions
	global TabRefX, TabRefY, TabRefZ

	OriginalMsImage=Lecture(FilePath="tabdata.dat")
	TestedMsImage=Lecture(FilePath="tabdata2.dat")
	


	print("OriginalMsImage     :", len(OriginalMsImage))
	print("TestedMsImage       :", len(TestedMsImage))
	print("TabRefXMulti        :", len(TabRefX))
	print("TabRefYMulti        :", len(TabRefY))
	print("TabRefZMulti        :", len(TabRefZ))
	
	RMS_Distances=[]
	WRMS_Distances=[]
	GFC_Distances=[]
	RGB_Distances=[]
	with Timer(verbose=True) as T:
		for i in range(NbRegions): # 13 multispectral regions for one Ms-Image
			OriginalRegions=[OriginalMsImage[x:x+SubRegionsSize] for x in range(0, NbMultSpecRegions*SubRegionsSize, SubRegionsSize)]
			TestedRegions=[OriginalMsImage[x:x+SubRegionsSize] for x in range(0, NbMultSpecRegions*SubRegionsSize, SubRegionsSize)]
#			print("TestedRegions   :", TestedRegions)
#			print("OriginalRegions :", OriginalRegions)
#			dis_RMS, dis_WRMS, dis_RGB, dis_GFC = AuthentifyMS(OriginalRegions, TestedRegions, TabRefX[:NbMultSpecRegions], TabRefY[:NbMultSpecRegions], TabRefZ[:NbMultSpecRegions])
			Authentified = AuthentifyMS(OriginalRegions, TestedRegions)
			
#			print("*"*30)
#			print("*  Multispectral Region {0}".format(i))
#			print("*"*30)
#			print("Distance RMS   :", dis_RMS)
#			print("Distance WRMS  :", dis_WRMS)
#			print("Distance RGB   :", dis_RGB)
#			print("Distance GFC   :", dis_GFC)

			#-------comparer couleur et multispectral--------
#			if (dis_RGB<1.0):
#				print("\n:) Couleur ok => Distance Multispectral")
#				if (dis_WRMS<1.0):
#					print("\n:) => Authentified")
#					print("*"*30+'\n')
##					return
#				else:
#					print("\n:( => not authentified")
#					print("*"*30+'\n')
#			else:
#				print("\n:( => not authentified")
#				print("*"*30+'\n')

	print("Time to authentify is: {0} seconds".format(T.AsString()))
	print("End of testbench.")
	return

#==================================================================
if __name__=="__main__":
	if len(sys.argv)>1:
		if sys.argv[1].lower() == "test":
			AuthentifyMS_Test()
			sys.exit(0)

	NbMultSpecRegions = 5
	SubRegionsSize    = 9
	NbRegions         = 10

	TabRefX=Lecture(FilePath="tabrefX.dat")[:NbMultSpecRegions] # 1604 values
	TabRefY=Lecture(FilePath="tabrefY.dat")[:NbMultSpecRegions] # 1604 values
	TabRefZ=Lecture(FilePath="tabrefZ.dat")[:NbMultSpecRegions] # 1604 values

#	print("TabRefX:", TabRefX)
#	print("TabRefY:", TabRefY)
#	print("TabRefZ:", TabRefZ)

	Synthesize(
		AuthentifyMS, 
		AuthentifyMS_Test, 
		OutputPath="./AuthMultiSpect_SyntheSys_Output",
		OriginalRegions=[[float(1) for i in range(SubRegionsSize)] for i in range(NbMultSpecRegions)], # 1604 spectrums : only one multispectral region
		TestedRegions=[[float(1) for i in range(SubRegionsSize)] for i in range(NbMultSpecRegions)] # 1604 spectrums : only one multispectral region
#			TabRefX=list([float(1) for i in range(NbMultSpecRegions)]), 
#			TabRefY=list([float(1) for i in range(NbMultSpecRegions)]), 
#			TabRefZ=list([float(1) for i in range(NbMultSpecRegions)]) 
		)











