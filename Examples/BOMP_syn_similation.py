#!/usr/bin/python

import math
import copy
import sys, os
import logging
#======================================================================
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "..")))

from SyntheSys import Algorithm, Testbench, Synthesize
from SyntheSys.Analysis.Module import HighLevelModule, Synthesizable
from SyntheSys.Analysis.SyntheSys_Algorithm import SyntheSys_Algorithm as Algorithm
from SyntheSys.Library import BOPM_Unit

@Algorithm()
#vmax
def vmax(x, y):
	if (x > y): 
		return x
	return y
#------------------------------------------------------------------------------------------------------------------------------------------
@Algorithm()
def calcule_values(d,spots_p,rinv,values_call_obtenu,values_call_obtenu_pre,values_put_obtenu,values_put_obtenu_pre,strike,p,q):
	spots_p = d * spots_p
	a= rinv * (p * values_call_obtenu + q * values_call_obtenu_pre)
	b= spots_p-strike
	c=max(b,0.0)
	temp_call_new = max(a,c)
	temp_call_new = max(rinv * (p * values_call_obtenu + q * values_call_obtenu_pre), max(spots_p-strike,0.0))
	d= rinv * (p * values_put_obtenu + q * values_put_obtenu_pre)
	e= strike-spots_p
	f=max(e,0.0)
	temp_put_new = max(d,f)
	temp_put_new = max(rinv * (p * values_put_obtenu + q * values_put_obtenu_pre), max(strike-spots_p,0.0))
	values_call_new = max(temp_call_new,0.0)
	values_call_new = max(temp_call_new, 0.0)
	temp_put_new = max(temp_put_new, 0.0)
	values_put_new = max(temp_put_new, 0.0)

	return values_call_new, values_put_new, spots_p

#--------------------------------------------------------------------
@Algorithm() # Mean that this method will be HighLevelModule algorithm to be synthesized.
def algorithme(temp_call_tab,values_call_tab,temp_put_tab,values_put_tab,spots_tab,strike,rinv,d,p,q,m):
	"""
	Design core algorithm : read sequentially but understood as a parallel algorithm.
	Here HighLevelModules can be instanciated (a call for their algorithm methods).
	"""
	#for Step in range(len(values_call_tab)):
	#	for k in range(len(values_call_tab)-Step-1):
	for j in range(1,len(values_call_tab)+1,1):
	#for j in range(1,m+1,1):
		for k in range(len(values_call_tab),j-1,-1):
		#for k in range(m,j-1,-1):
			calcule_values_resultat = calcule_values(d,spots_tab[k],rinv,values_call_tab[k],values_call_tab[k-1],values_put_tab[k],values_put_tab[k-1],strike,p,q)
			values_call_tab[k]= calcule_values_resultat[0]
			values_put_tab[k] = calcule_values_resultat[1]
			spots_tab[k] = calcule_values_resultat[2]
			
	return values_call_tab[m],values_put_tab[m]
#----------------------------------------------------------------------------------------------------------------
@Testbench() # Mean that this method will be HighLevelModule testbench
def BOPM_Test(temp_call_tab,values_call_tab,temp_put_tab,values_put_tab,spots_tab,strike,rinv,d,p,q,m):
	"""
	Test bench for ALU HighLevelModule: sequential.
	"""
	for T in range(1):
		#=====================
		r=algorithme(temp_call_tab,values_call_tab,temp_put_tab,values_put_tab,spots_tab,strike,rinv,d,p,q,m)
		
		#=====================
	
	return True
#----------------------------------------------------------------------------------------------------------------
if __name__=='__main__': 

	settlementdate = 400
	expirydate = 460.0
	spot = 100.0
	rate = 0
	strike = 100.0
	volatility = 0.250
	m = 800 #steps

	maturity = (expirydate - settlementdate) / 365.0
	dt = maturity / m
	r = math.exp(rate * dt)
	rinv = 1.0 / r
	u = math.exp(volatility * math.sqrt(dt))
	d = math.exp((-1)*volatility * math.sqrt(dt));#1.0 / u
	#print('d={}'.format(d))
	p = (r - d) / (u - d)
	q = 1.0 - p
	j = 0
	k = 0
	spots = [0.0 for i in range(m+1)]
	values_call = [0.0 for i in range(m+1)]
	values_put = [0.0 for i in range(m+1)]
	temp_call = [0.0 for i in range(m+1)]
	temp_put = [0.0 for i in range(m+1)]

	for k in range(m,-1,-1):
		spots[k] = spot * math.pow(u,(2*k-m))
		values_call[k] = vmax(spots[k]-strike, 0.0)
		temp_call[k] = values_put[k]
		values_put[k] = vmax(strike-spots[k], 0.0)
		temp_put[k] = values_put[k]
	
	#resultat=algorithme(temp_call,values_call,temp_put,values_put,spots,strike,rinv,d,p,q,m)
	#final_value_call = resultat[0]
	#final_value_put = resultat[1]	
	#print('Resultat_call:{} et Resultat_put:{} '.format(final_value_call,final_value_put))
	
	OutputPath=os.path.join(os.path.abspath(os.path.dirname(__file__)), "lina_output")
	
	Synthesize(algorithme, BOPM_Test, OutputPath="./lina_output", temp_call_tab=values_call, values_call_tab=values_call,temp_put_tab=temp_put,values_put_tab=values_put,spots_tab=spots,strike=strike,rinv=rinv,d=d,p=p,q=q,m=m) # No constraints



