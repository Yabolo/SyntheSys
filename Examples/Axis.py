#!/usr/bin/python


import sys, os
import logging
#======================================================================
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "..")))

from SyntheSys import Algorithm, Testbench, Synthesize
from SyntheSys.Library.ImageProcessing import AxisXYZ

#--------------------------------------------------------------------
@Algorithm() # Mean that this method will be HighLevelModule algorithm to be synthesized.
def Axis(Region1, Region2):
	"""
	Design core algorithm : read sequentially but understood as a parallel algorithm.
	Here HighLevelModules can be instanciated (a call for their algorithm methods).
	"""
#	a=2*x
#	b=3*y
#	c=4*z
#	d=x+y
#	e=y+z
#	f=d+z
	return AxisXYZ(Region1, Region2)
	
#--------------------------------------------------------------------
@Testbench() # Mean that this method will be HighLevelModule testbench
def Axis_Test():
	"""
	Test bench for mean processing HighLevelModule: sequential.
	"""
#	Data=[2, 2, 4, 4, 8, 8, 10, 10, 12]
#	for i in range(1, 10):

	for a,b,c,d,e,f,g,h,i in [(1,2,3,4,5,6,7,8,9),]:
		r=Axis([a,b,c,d,e,f,g,h,i],[a+1,b+1,c+1,d+1,e+1,f+1,g+1,h+1,i+1])
	#=====================
	#r=Axis(1,2,3)
	#=====================
	
	return True



#======================================================================
if __name__=='__main__': 
	
	OutputPath=os.path.join(os.path.abspath(os.path.dirname(__file__)), "Axis_SyntheSys_output")
	Synthesize(Axis, Axis_Test, OutputPath=OutputPath, Region1=[1,2,3,4,5,6,7,8,9], Region2=[1,2,3,4,5,6,7,8,9]) # No constraints





