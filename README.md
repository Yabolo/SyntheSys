
SyntheSys  
=========

SyntheSys is a Python module for generating network on chip (NoC) based systems from Python3 programs. It is specially design for reconfigurable computing, i.e. using FPGA to accelerate parts of programs. It has a extensible library for any type of FPGA and uses NoC properties to be scalable.

This is the README file for the project.

----
SyntheSys is distributed with a GPLv3 license.
See LICENSE.txt for details.
----

Matthieu PAYET <matthieu.payet@free.fr>
More on Matthieu's website : mpayet.net


