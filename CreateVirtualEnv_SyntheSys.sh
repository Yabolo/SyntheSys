#! /bin/bash

EnvName="VEnv_SyntheSys"
PythonVersion="3"
Location=${HOME}

#------------------------------------------------------
TargetPath=$Location/$EnvName
if [ -d "$TargetPath" ]; then
	# Control will enter here if $TargetPath exists.
	Answer="x"
	while [ 1 ]
	do
		echo -n "$TargetPath already exists. Remove it ? Y/n > "
		read Answer
		#echo "Given '$Answer'"
		if [ $Answer=="Y" ]; then
			echo "Removing previous virtual environment..."
			rm -rf $TargetPath
			echo "Done."
			break
		else
			if [ $Answer=="n" ]; then
				echo "Do nothing."
				#exit
				break
			fi
		fi
	done
fi

#------------------------------------------------------
CurrentDir=`pwd`
echo "CurrentDir: $CurrentDir"
echo "Location: $Location"
cd $Location
virtualenv $EnvName -p /usr/bin/python$PythonVersion
source ./$EnvName/bin/activate

#pip install git+ssh://git@gitlab.com/Yabolo/HdlLib.git
#if [ $? -ne 0 ]; then echo "HdlLib installation failure. Exiting."; exit; fi
#pip install git+ssh://git@gitlab.com/Yabolo/SyntheSys.git
#if [ $? -ne 0 ]; then echo "SyntheSys installation failure. Exiting."; exit; fi

pip install HdlLib
pip install SyntheSys

deactivate
cd $CurrentDir


echo "Virtual environment created successfully."
echo "Execute 'source ./$EnvName/bin/activate' to enter the new created virtual environment then 'deactivate' to exit."










