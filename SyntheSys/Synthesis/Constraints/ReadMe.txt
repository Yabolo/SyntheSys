
- Interval d'initialisation : nombre de cycle d'horloge à attendre entre deux cycle d'ordonnancement (1 pour pipeline maximum). C'est la contrainte de pipelining !

- Latence : nombre de cycles d'horloges entre la première entrée et la première sortie de données.

- Débit : 1 opération toute les D cycles d'horloge.
