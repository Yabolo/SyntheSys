MODULO SCHEDULING:
	une approche dite "ordonancer puis déplacer"
	

--------------------------------------------------------------------
Data introduction interval (interval d'injection de données): 
	Nombre de cycle d'horloge entre deux productions de données consécutives.
	
--------------------------------------------------------------------
Latence :
	Nombre de cycle d'horloge entre la consommation et la production de données

--------------------------------------------------------------------
Module non pipeliné : IID = Latence
Module entièrement pipeliné : IID = 1, Latence > 1
Module partiellement pipeliné : 1 < IID < Latence

--------------------------------------------------------------------
Exploration :
	Itérer sur les paires Fréquence/IID 
	
--------------------------------------------------------------------
Cycles dans un graphe de dépendences :
	Autorisé par l'utilisation d'opérateur retard
	Programmé comme registres à décalage
	
--------------------------------------------------------------------
Ordonnançabilité :
	DD : Date de début (start time)
	LM : Marge libre (slack value)
	Fenêtre d'ordonancement = temps entre DD et DD+LM
	
	Deux DD de modules partagés doivent être séparé de n*IID (n>=1).
		Sinon => pas ordonnançable
--------------------------------------------------------------------
MII (minimum initiation interval) : 
	Un II plus petit => temps d'execution plus court
	MII est la plus petite valeur de II pour laquelle un ordonnancement modulo existe.
	le candidat II est d'abord = à MII plus augmenté jusqu'à ce qu'un ordonancement modulo est obtenu.
	MII=max(ResMII, RecMII)
	
	
	ResMII :
		MII calculé à partir des contraintes d'utilisation en ressources.
	
	RecMII : 
		MII calculé à partir des latences autour des circuits élémentaires.
--------------------------------------------------------------------


Dans notre architecture, l'impact du partage d'un opérateur sur la consommation en ressources est nul. 

Nous préferons réaliser le partage des ressources avant l'ordonancement afin d'estimer la consommation en ressources et les latences plus précisément.

Notre architecture fonctionne à la manière d'un modèle SDC (synchronous data flow) ; par jetons de données.

Pré-requis :
	- les modules consomment et produisent toujours le même nombre de données.
	

On peut pas utiliser de matrice minDist (matrice d'adjacence) car elle est N²... 





=> Citation 43

--------------------------------------------------------------------
--------------------------------------------------------------------
EXPLORATION DU PARTAGE DES RESSOURCES :
--------------------------------------------------------------------
--------------------------------------------------------------------
Result: t s , m, cOps
def DynamicAllocate(op, t_min , t_max):
	mAlloc=allocatedComp(op) # Récupérer la liste des composants compatibles avec l'operation.
	if notWorthShare(op) or (mAlloc is None): # Liste vide ou pas interet de partager (partager coute plus que pas partager).
		t=t_min
		m=create a new component for op
		cOps=None
		return t, m, cOps # Retourne date d'ordonancement minimale et nouveau composant (pas de conflit d'operations)
	maxSimCompC=None
	maxSimCompNC=None

	for t in range(t_min, t_max+1): # Pour chaque cycle d'horloge possible
		# récupérer le composant de la liste de similarité maximale s'il y a des composants dont les opérations ne commencent pas dans a ce cycle
		(conflict, maxSimComp)=findMaxSimComp(t, op, mAlloc) # calcul le partitionnement en cliques de coût minimal sur le graphe de compatibilité pondéré poru trouver le meilleur partage de ressources pour chaque composant. Dans le cas où plus d'un composant sans conflit de ressources, la similarité moyenne de chaque composant est calculée. Ce calcul résulte de la somme des poids entre l'opération courante et toutes les opérations affectées à ce composant. Cette somme est divisée par le nombre d'opération actuellement affectées. Le composant avec la meilleure similarité moyenne est retournée. Si aucun composant non conflictuel ne peut être trouvé, la différence de similarité moyenne est calculée (différence de similarité moyenne entre les opérations conflictuelles et l'opération courante. Le composant avec la meileure différence de similarité moyenne est retournée. 
		if (conflict): then # Composant avec conflit
			if maxSimCompC < maxSimComp: # Comparaison avec le meilleur composant choisi jusqu'alors
				maxSimCompC=maxSimComp # Met à jour le composant si besoin
		else: # composant sans conflit
			if maxSimCompNC < maxSimComp: then
				maxSimCompNC=maxSimComp;

	if not (maxSimCompNC is None): then
		# S'il existe un meilleur composant non conflictuel : le retourner
		return maxSimCompNC
	if (maxSimCompC ≥ 0) then
		# Si désordonnancer l'opération conflictuel du composant conflictuel vaut le coup (associer l'opération courante économise des ressources matérielles par rapport à l'opération conflictuelle)
		if worthUnschedule():
			return maxSimCompC
return new component for op

--------------------------------------------------------------------
--------------------------------------------------------------------
MODULE SELECTION :
--------------------------------------------------------------------
--------------------------------------------------------------------
HOIMS(DG, T, LIB ) begin
	CandidateModules(T, LIB )
	δ min ← MinII()
	δ max ← MaxII()
	S best ← ø
	for δ ← δ min to δ max do
		f δ ← δ·T 
		RemoveDominated(δ, f δ )
		InitialMS(δ,f δ )
		CorrectMS(DG) # Vérifie l'ordonnançabilité du module selectionné et le modifie jusqu'à ce que le graphe soit ordonnançable ou qu'il n'y est plus de modification possible.
		CreateWCG()
		repeat
			Initialize all operations to be never scheduled;
			HeightR(δ);
			Insert all operations into Q;
			Schedule(START, 0 );
			while Q = ø do
				v ← HighestPriorityOperation(Q)
				t s min ← CalculateEarlyStart(v)
				t s max ← t s min + δ − 1
				(t s , m) ← DynamicAllocate(v,T s min ,T s min )
				Schedule(v, t s , M )
				Q ← Q + UnscheduleConflicts(v, t s , M );
			if Cost(S current ) < Cost(S best ) then
				S best ← S current ;
		until (not MS Improvable()) ;
end


--------------------------------------------------------------------
--------------------------------------------------------------------
CORRECTION DE LA SELECTION INITIALE DE MODULES
--------------------------------------------------------------------
--------------------------------------------------------------------
def correctMS(DG):
	foreach scc in DG:
		setup smaller latency modules for each v ∈ scc
		validMS=false
		while not validMS:
			# SCC (Strongly Connected Component) decomposition of a dependence graph
			# Évalué grace à l'algorithme de détection de SCC de Kosaraju (citation 35)
			(slack, posSlackOps)=minDist(scc) # Vérifie l'ordonnançabilité du SCC courant
			if slack ≤ 0:
				validMS=True
			else:
				sort posSlackOps
				update=false
				foreach v ∈ posSlackOps do
					if setSmallerLatencyModule(v):
						update ← true
						break
				if not update then
					break
		if not validMS then
			return false
	return true


--------------------------------------------------------------------
--------------------------------------------------------------------
RAFINEMENT DE LA SELECTION GLOBALE DE MODULE
--------------------------------------------------------------------
--------------------------------------------------------------------
def globalMSRefine():
	initialize mi2f ree[]
	foreach op ∈ DG do
		if notWorthShare(op) 
			continue
		foreach mi ∈ mi2f ree[] do
			if moduleType(mi)=MS(op) or fullOrEmpty(mi):
				continue
			if compatible(op, mi) and changeable(op, mi):
			setMS(op, mi)
			update mi2f ree[] for current and new module of op
			break


--------------------------------------------------------------------
--------------------------------------------------------------------
DIT WIDTH MORPHING
--------------------------------------------------------------------
--------------------------------------------------------------------
def bitWidthMorph(s):
	foreach ms ∈ s.sharedModule():
		sort(ms .mi )
		initialize mi2f ree[]
		foreach mi1 ∈ ms.mi excluding the last one:
			if mi2f ree[mi1].full():
				continue
			mi2 ← mi1.next()
			while not mi2f ree[mi2].empty():
				if mi2.width() == mi1.width():
					mi2=mi2.next()
					continue
				if mi2.width() < mi1.width() - w compatible:
					break
				bOps=mi2.boundOps()
				sort(bOps)
				foreach op ∈ bOps do
					if op.width() < mi1.width() - w compatible then
						break
					bit width morph op with mi 1 .width()
					update mi2f ree[mi 1 ] and mi2f ree[mi 2 ]
				mi2=mi2.next()
				
				
				
