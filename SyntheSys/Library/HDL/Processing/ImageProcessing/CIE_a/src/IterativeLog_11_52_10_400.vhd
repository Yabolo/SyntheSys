--------------------------------------------------------------------------------
--                             LZOC_52_F400_uid3
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Bogdan Pasca (2007)
--------------------------------------------------------------------------------
-- Pipeline depth: 3 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LZOC_52_F400_uid3 is
   port ( clk, rst : in std_logic;
          I : in  std_logic_vector(51 downto 0);
          OZB : in  std_logic;
          O : out  std_logic_vector(5 downto 0)   );
end entity;

architecture arch of LZOC_52_F400_uid3 is
signal sozb, sozb_d1, sozb_d2, sozb_d3 :  std_logic;
signal level6, level6_d1 :  std_logic_vector(63 downto 0);
signal digit6, digit6_d1, digit6_d2, digit6_d3 :  std_logic;
signal level5 :  std_logic_vector(31 downto 0);
signal digit5, digit5_d1, digit5_d2 :  std_logic;
signal level4, level4_d1 :  std_logic_vector(15 downto 0);
signal digit4, digit4_d1 :  std_logic;
signal level3 :  std_logic_vector(7 downto 0);
signal digit3, digit3_d1 :  std_logic;
signal level2, level2_d1 :  std_logic_vector(3 downto 0);
signal digit2 :  std_logic;
signal level1 :  std_logic_vector(1 downto 0);
signal digit1 :  std_logic;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            sozb_d1 <=  sozb;
            sozb_d2 <=  sozb_d1;
            sozb_d3 <=  sozb_d2;
            level6_d1 <=  level6;
            digit6_d1 <=  digit6;
            digit6_d2 <=  digit6_d1;
            digit6_d3 <=  digit6_d2;
            digit5_d1 <=  digit5;
            digit5_d2 <=  digit5_d1;
            level4_d1 <=  level4;
            digit4_d1 <=  digit4;
            digit3_d1 <=  digit3;
            level2_d1 <=  level2;
         end if;
      end process;
   sozb <= OZB;
   level6<= I& (11 downto 0 => not(sozb));
   digit6<= '1' when level6(63 downto 32) = (63 downto 32 => sozb) else '0';
   ----------------Synchro barrier, entering cycle 1----------------
   level5<= level6_d1(31 downto 0) when digit6_d1='1' else level6_d1(63 downto 32);
   digit5<= '1' when level5(31 downto 16) = (31 downto 16 => sozb_d1) else '0';
   level4<= level5(15 downto 0) when digit5='1' else level5(31 downto 16);
   ----------------Synchro barrier, entering cycle 2----------------
   digit4<= '1' when level4_d1(15 downto 8) = (15 downto 8 => sozb_d2) else '0';
   level3<= level4_d1(7 downto 0) when digit4='1' else level4_d1(15 downto 8);
   digit3<= '1' when level3(7 downto 4) = (7 downto 4 => sozb_d2) else '0';
   level2<= level3(3 downto 0) when digit3='1' else level3(7 downto 4);
   ----------------Synchro barrier, entering cycle 3----------------
   digit2<= '1' when level2_d1(3 downto 2) = (3 downto 2 => sozb_d3) else '0';
   level1<= level2_d1(1 downto 0) when digit2='1' else level2_d1(3 downto 2);
   digit1<= '1' when level1(1 downto 1) = (1 downto 1 => sozb_d3) else '0';
   O <= digit6_d3 & digit5_d2 & digit4_d1 & digit3_d1 & digit2 & digit1;
end architecture;

--------------------------------------------------------------------------------
--                     LeftShifter_27_by_max_27_F400_uid6
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LeftShifter_27_by_max_27_F400_uid6 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(26 downto 0);
          S : in  std_logic_vector(4 downto 0);
          R : out  std_logic_vector(53 downto 0)   );
end entity;

architecture arch of LeftShifter_27_by_max_27_F400_uid6 is
signal level0 :  std_logic_vector(26 downto 0);
signal ps, ps_d1 :  std_logic_vector(4 downto 0);
signal level1 :  std_logic_vector(27 downto 0);
signal level2, level2_d1 :  std_logic_vector(29 downto 0);
signal level3 :  std_logic_vector(33 downto 0);
signal level4 :  std_logic_vector(41 downto 0);
signal level5 :  std_logic_vector(57 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            ps_d1 <=  ps;
            level2_d1 <=  level2;
         end if;
      end process;
   level0<= X;
   ps<= S;
   level1<= level0 & (0 downto 0 => '0') when ps(0)= '1' else     (0 downto 0 => '0') & level0;
   level2<= level1 & (1 downto 0 => '0') when ps(1)= '1' else     (1 downto 0 => '0') & level1;
   ----------------Synchro barrier, entering cycle 1----------------
   level3<= level2_d1 & (3 downto 0 => '0') when ps_d1(2)= '1' else     (3 downto 0 => '0') & level2_d1;
   level4<= level3 & (7 downto 0 => '0') when ps_d1(3)= '1' else     (7 downto 0 => '0') & level3;
   level5<= level4 & (15 downto 0 => '0') when ps_d1(4)= '1' else     (15 downto 0 => '0') & level4;
   R <= level5(53 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                              InvTable_0_9_10
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity InvTable_0_9_10 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(8 downto 0);
          Y : out  std_logic_vector(9 downto 0)   );
end entity;

architecture arch of InvTable_0_9_10 is
begin
  with X select  Y <= 
   "1000000000" when "000000000",
   "1000000000" when "000000001",
   "0111111111" when "000000010",
   "0111111110" when "000000011",
   "0111111101" when "000000100",
   "0111111100" when "000000101",
   "0111111011" when "000000110",
   "0111111010" when "000000111",
   "0111111001" when "000001000",
   "0111111000" when "000001001",
   "0111110111" when "000001010",
   "0111110110" when "000001011",
   "0111110101" when "000001100",
   "0111110100" when "000001101",
   "0111110011" when "000001110",
   "0111110010" when "000001111",
   "0111110001" when "000010000",
   "0111110000" when "000010001",
   "0111101111" when "000010010",
   "0111101110" when "000010011",
   "0111101101" when "000010100",
   "0111101100" when "000010101",
   "0111101011" when "000010110",
   "0111101010" when "000010111",
   "0111101010" when "000011000",
   "0111101001" when "000011001",
   "0111101000" when "000011010",
   "0111100111" when "000011011",
   "0111100110" when "000011100",
   "0111100101" when "000011101",
   "0111100100" when "000011110",
   "0111100011" when "000011111",
   "0111100010" when "000100000",
   "0111100001" when "000100001",
   "0111100001" when "000100010",
   "0111100000" when "000100011",
   "0111011111" when "000100100",
   "0111011110" when "000100101",
   "0111011101" when "000100110",
   "0111011100" when "000100111",
   "0111011011" when "000101000",
   "0111011011" when "000101001",
   "0111011010" when "000101010",
   "0111011001" when "000101011",
   "0111011000" when "000101100",
   "0111010111" when "000101101",
   "0111010110" when "000101110",
   "0111010101" when "000101111",
   "0111010101" when "000110000",
   "0111010100" when "000110001",
   "0111010011" when "000110010",
   "0111010010" when "000110011",
   "0111010001" when "000110100",
   "0111010000" when "000110101",
   "0111010000" when "000110110",
   "0111001111" when "000110111",
   "0111001110" when "000111000",
   "0111001101" when "000111001",
   "0111001100" when "000111010",
   "0111001100" when "000111011",
   "0111001011" when "000111100",
   "0111001010" when "000111101",
   "0111001001" when "000111110",
   "0111001000" when "000111111",
   "0111001000" when "001000000",
   "0111000111" when "001000001",
   "0111000110" when "001000010",
   "0111000101" when "001000011",
   "0111000100" when "001000100",
   "0111000100" when "001000101",
   "0111000011" when "001000110",
   "0111000010" when "001000111",
   "0111000001" when "001001000",
   "0111000001" when "001001001",
   "0111000000" when "001001010",
   "0110111111" when "001001011",
   "0110111110" when "001001100",
   "0110111110" when "001001101",
   "0110111101" when "001001110",
   "0110111100" when "001001111",
   "0110111011" when "001010000",
   "0110111011" when "001010001",
   "0110111010" when "001010010",
   "0110111001" when "001010011",
   "0110111000" when "001010100",
   "0110111000" when "001010101",
   "0110110111" when "001010110",
   "0110110110" when "001010111",
   "0110110101" when "001011000",
   "0110110101" when "001011001",
   "0110110100" when "001011010",
   "0110110011" when "001011011",
   "0110110011" when "001011100",
   "0110110010" when "001011101",
   "0110110001" when "001011110",
   "0110110000" when "001011111",
   "0110110000" when "001100000",
   "0110101111" when "001100001",
   "0110101110" when "001100010",
   "0110101110" when "001100011",
   "0110101101" when "001100100",
   "0110101100" when "001100101",
   "0110101011" when "001100110",
   "0110101011" when "001100111",
   "0110101010" when "001101000",
   "0110101001" when "001101001",
   "0110101001" when "001101010",
   "0110101000" when "001101011",
   "0110100111" when "001101100",
   "0110100111" when "001101101",
   "0110100110" when "001101110",
   "0110100101" when "001101111",
   "0110100101" when "001110000",
   "0110100100" when "001110001",
   "0110100011" when "001110010",
   "0110100011" when "001110011",
   "0110100010" when "001110100",
   "0110100001" when "001110101",
   "0110100001" when "001110110",
   "0110100000" when "001110111",
   "0110011111" when "001111000",
   "0110011111" when "001111001",
   "0110011110" when "001111010",
   "0110011101" when "001111011",
   "0110011101" when "001111100",
   "0110011100" when "001111101",
   "0110011011" when "001111110",
   "0110011011" when "001111111",
   "0110011010" when "010000000",
   "0110011001" when "010000001",
   "0110011001" when "010000010",
   "0110011000" when "010000011",
   "0110011000" when "010000100",
   "0110010111" when "010000101",
   "0110010110" when "010000110",
   "0110010110" when "010000111",
   "0110010101" when "010001000",
   "0110010100" when "010001001",
   "0110010100" when "010001010",
   "0110010011" when "010001011",
   "0110010011" when "010001100",
   "0110010010" when "010001101",
   "0110010001" when "010001110",
   "0110010001" when "010001111",
   "0110010000" when "010010000",
   "0110010000" when "010010001",
   "0110001111" when "010010010",
   "0110001110" when "010010011",
   "0110001110" when "010010100",
   "0110001101" when "010010101",
   "0110001100" when "010010110",
   "0110001100" when "010010111",
   "0110001011" when "010011000",
   "0110001011" when "010011001",
   "0110001010" when "010011010",
   "0110001010" when "010011011",
   "0110001001" when "010011100",
   "0110001000" when "010011101",
   "0110001000" when "010011110",
   "0110000111" when "010011111",
   "0110000111" when "010100000",
   "0110000110" when "010100001",
   "0110000101" when "010100010",
   "0110000101" when "010100011",
   "0110000100" when "010100100",
   "0110000100" when "010100101",
   "0110000011" when "010100110",
   "0110000011" when "010100111",
   "0110000010" when "010101000",
   "0110000001" when "010101001",
   "0110000001" when "010101010",
   "0110000000" when "010101011",
   "0110000000" when "010101100",
   "0101111111" when "010101101",
   "0101111111" when "010101110",
   "0101111110" when "010101111",
   "0101111110" when "010110000",
   "0101111101" when "010110001",
   "0101111100" when "010110010",
   "0101111100" when "010110011",
   "0101111011" when "010110100",
   "0101111011" when "010110101",
   "0101111010" when "010110110",
   "0101111010" when "010110111",
   "0101111001" when "010111000",
   "0101111001" when "010111001",
   "0101111000" when "010111010",
   "0101111000" when "010111011",
   "0101110111" when "010111100",
   "0101110110" when "010111101",
   "0101110110" when "010111110",
   "0101110101" when "010111111",
   "0101110101" when "011000000",
   "0101110100" when "011000001",
   "0101110100" when "011000010",
   "0101110011" when "011000011",
   "0101110011" when "011000100",
   "0101110010" when "011000101",
   "0101110010" when "011000110",
   "0101110001" when "011000111",
   "0101110001" when "011001000",
   "0101110000" when "011001001",
   "0101110000" when "011001010",
   "0101101111" when "011001011",
   "0101101111" when "011001100",
   "0101101110" when "011001101",
   "0101101110" when "011001110",
   "0101101101" when "011001111",
   "0101101101" when "011010000",
   "0101101100" when "011010001",
   "0101101100" when "011010010",
   "0101101011" when "011010011",
   "0101101011" when "011010100",
   "0101101010" when "011010101",
   "0101101010" when "011010110",
   "0101101001" when "011010111",
   "0101101001" when "011011000",
   "0101101000" when "011011001",
   "0101101000" when "011011010",
   "0101100111" when "011011011",
   "0101100111" when "011011100",
   "0101100110" when "011011101",
   "0101100110" when "011011110",
   "0101100101" when "011011111",
   "0101100101" when "011100000",
   "0101100100" when "011100001",
   "0101100100" when "011100010",
   "0101100011" when "011100011",
   "0101100011" when "011100100",
   "0101100010" when "011100101",
   "0101100010" when "011100110",
   "0101100001" when "011100111",
   "0101100001" when "011101000",
   "0101100000" when "011101001",
   "0101100000" when "011101010",
   "0101011111" when "011101011",
   "0101011111" when "011101100",
   "0101011110" when "011101101",
   "0101011110" when "011101110",
   "0101011110" when "011101111",
   "0101011101" when "011110000",
   "0101011101" when "011110001",
   "0101011100" when "011110010",
   "0101011100" when "011110011",
   "0101011011" when "011110100",
   "0101011011" when "011110101",
   "0101011010" when "011110110",
   "0101011010" when "011110111",
   "0101011001" when "011111000",
   "0101011001" when "011111001",
   "0101011001" when "011111010",
   "0101011000" when "011111011",
   "0101011000" when "011111100",
   "0101010111" when "011111101",
   "0101010111" when "011111110",
   "0101010110" when "011111111",
   "1010101011" when "100000000",
   "1010101010" when "100000001",
   "1010101001" when "100000010",
   "1010101001" when "100000011",
   "1010101000" when "100000100",
   "1010100111" when "100000101",
   "1010100110" when "100000110",
   "1010100101" when "100000111",
   "1010100100" when "100001000",
   "1010100011" when "100001001",
   "1010100010" when "100001010",
   "1010100010" when "100001011",
   "1010100001" when "100001100",
   "1010100000" when "100001101",
   "1010011111" when "100001110",
   "1010011110" when "100001111",
   "1010011101" when "100010000",
   "1010011100" when "100010001",
   "1010011100" when "100010010",
   "1010011011" when "100010011",
   "1010011010" when "100010100",
   "1010011001" when "100010101",
   "1010011000" when "100010110",
   "1010010111" when "100010111",
   "1010010110" when "100011000",
   "1010010110" when "100011001",
   "1010010101" when "100011010",
   "1010010100" when "100011011",
   "1010010011" when "100011100",
   "1010010010" when "100011101",
   "1010010010" when "100011110",
   "1010010001" when "100011111",
   "1010010000" when "100100000",
   "1010001111" when "100100001",
   "1010001110" when "100100010",
   "1010001101" when "100100011",
   "1010001101" when "100100100",
   "1010001100" when "100100101",
   "1010001011" when "100100110",
   "1010001010" when "100100111",
   "1010001001" when "100101000",
   "1010001001" when "100101001",
   "1010001000" when "100101010",
   "1010000111" when "100101011",
   "1010000110" when "100101100",
   "1010000101" when "100101101",
   "1010000101" when "100101110",
   "1010000100" when "100101111",
   "1010000011" when "100110000",
   "1010000010" when "100110001",
   "1010000001" when "100110010",
   "1010000001" when "100110011",
   "1010000000" when "100110100",
   "1001111111" when "100110101",
   "1001111110" when "100110110",
   "1001111110" when "100110111",
   "1001111101" when "100111000",
   "1001111100" when "100111001",
   "1001111011" when "100111010",
   "1001111010" when "100111011",
   "1001111010" when "100111100",
   "1001111001" when "100111101",
   "1001111000" when "100111110",
   "1001110111" when "100111111",
   "1001110111" when "101000000",
   "1001110110" when "101000001",
   "1001110101" when "101000010",
   "1001110100" when "101000011",
   "1001110100" when "101000100",
   "1001110011" when "101000101",
   "1001110010" when "101000110",
   "1001110001" when "101000111",
   "1001110001" when "101001000",
   "1001110000" when "101001001",
   "1001101111" when "101001010",
   "1001101110" when "101001011",
   "1001101110" when "101001100",
   "1001101101" when "101001101",
   "1001101100" when "101001110",
   "1001101011" when "101001111",
   "1001101011" when "101010000",
   "1001101010" when "101010001",
   "1001101001" when "101010010",
   "1001101001" when "101010011",
   "1001101000" when "101010100",
   "1001100111" when "101010101",
   "1001100110" when "101010110",
   "1001100110" when "101010111",
   "1001100101" when "101011000",
   "1001100100" when "101011001",
   "1001100100" when "101011010",
   "1001100011" when "101011011",
   "1001100010" when "101011100",
   "1001100001" when "101011101",
   "1001100001" when "101011110",
   "1001100000" when "101011111",
   "1001011111" when "101100000",
   "1001011111" when "101100001",
   "1001011110" when "101100010",
   "1001011101" when "101100011",
   "1001011101" when "101100100",
   "1001011100" when "101100101",
   "1001011011" when "101100110",
   "1001011010" when "101100111",
   "1001011010" when "101101000",
   "1001011001" when "101101001",
   "1001011000" when "101101010",
   "1001011000" when "101101011",
   "1001010111" when "101101100",
   "1001010110" when "101101101",
   "1001010110" when "101101110",
   "1001010101" when "101101111",
   "1001010100" when "101110000",
   "1001010100" when "101110001",
   "1001010011" when "101110010",
   "1001010010" when "101110011",
   "1001010010" when "101110100",
   "1001010001" when "101110101",
   "1001010000" when "101110110",
   "1001010000" when "101110111",
   "1001001111" when "101111000",
   "1001001110" when "101111001",
   "1001001110" when "101111010",
   "1001001101" when "101111011",
   "1001001100" when "101111100",
   "1001001100" when "101111101",
   "1001001011" when "101111110",
   "1001001010" when "101111111",
   "1001001010" when "110000000",
   "1001001001" when "110000001",
   "1001001000" when "110000010",
   "1001001000" when "110000011",
   "1001000111" when "110000100",
   "1001000110" when "110000101",
   "1001000110" when "110000110",
   "1001000101" when "110000111",
   "1001000100" when "110001000",
   "1001000100" when "110001001",
   "1001000011" when "110001010",
   "1001000011" when "110001011",
   "1001000010" when "110001100",
   "1001000001" when "110001101",
   "1001000001" when "110001110",
   "1001000000" when "110001111",
   "1000111111" when "110010000",
   "1000111111" when "110010001",
   "1000111110" when "110010010",
   "1000111101" when "110010011",
   "1000111101" when "110010100",
   "1000111100" when "110010101",
   "1000111100" when "110010110",
   "1000111011" when "110010111",
   "1000111010" when "110011000",
   "1000111010" when "110011001",
   "1000111001" when "110011010",
   "1000111001" when "110011011",
   "1000111000" when "110011100",
   "1000110111" when "110011101",
   "1000110111" when "110011110",
   "1000110110" when "110011111",
   "1000110101" when "110100000",
   "1000110101" when "110100001",
   "1000110100" when "110100010",
   "1000110100" when "110100011",
   "1000110011" when "110100100",
   "1000110010" when "110100101",
   "1000110010" when "110100110",
   "1000110001" when "110100111",
   "1000110001" when "110101000",
   "1000110000" when "110101001",
   "1000101111" when "110101010",
   "1000101111" when "110101011",
   "1000101110" when "110101100",
   "1000101110" when "110101101",
   "1000101101" when "110101110",
   "1000101100" when "110101111",
   "1000101100" when "110110000",
   "1000101011" when "110110001",
   "1000101011" when "110110010",
   "1000101010" when "110110011",
   "1000101010" when "110110100",
   "1000101001" when "110110101",
   "1000101000" when "110110110",
   "1000101000" when "110110111",
   "1000100111" when "110111000",
   "1000100111" when "110111001",
   "1000100110" when "110111010",
   "1000100101" when "110111011",
   "1000100101" when "110111100",
   "1000100100" when "110111101",
   "1000100100" when "110111110",
   "1000100011" when "110111111",
   "1000100011" when "111000000",
   "1000100010" when "111000001",
   "1000100001" when "111000010",
   "1000100001" when "111000011",
   "1000100000" when "111000100",
   "1000100000" when "111000101",
   "1000011111" when "111000110",
   "1000011111" when "111000111",
   "1000011110" when "111001000",
   "1000011110" when "111001001",
   "1000011101" when "111001010",
   "1000011100" when "111001011",
   "1000011100" when "111001100",
   "1000011011" when "111001101",
   "1000011011" when "111001110",
   "1000011010" when "111001111",
   "1000011010" when "111010000",
   "1000011001" when "111010001",
   "1000011001" when "111010010",
   "1000011000" when "111010011",
   "1000010111" when "111010100",
   "1000010111" when "111010101",
   "1000010110" when "111010110",
   "1000010110" when "111010111",
   "1000010101" when "111011000",
   "1000010101" when "111011001",
   "1000010100" when "111011010",
   "1000010100" when "111011011",
   "1000010011" when "111011100",
   "1000010011" when "111011101",
   "1000010010" when "111011110",
   "1000010010" when "111011111",
   "1000010001" when "111100000",
   "1000010000" when "111100001",
   "1000010000" when "111100010",
   "1000001111" when "111100011",
   "1000001111" when "111100100",
   "1000001110" when "111100101",
   "1000001110" when "111100110",
   "1000001101" when "111100111",
   "1000001101" when "111101000",
   "1000001100" when "111101001",
   "1000001100" when "111101010",
   "1000001011" when "111101011",
   "1000001011" when "111101100",
   "1000001010" when "111101101",
   "1000001010" when "111101110",
   "1000001001" when "111101111",
   "1000001001" when "111110000",
   "1000001000" when "111110001",
   "1000001000" when "111110010",
   "1000000111" when "111110011",
   "1000000111" when "111110100",
   "1000000110" when "111110101",
   "1000000110" when "111110110",
   "1000000101" when "111110111",
   "1000000101" when "111111000",
   "1000000100" when "111111001",
   "1000000100" when "111111010",
   "1000000011" when "111111011",
   "1000000011" when "111111100",
   "1000000010" when "111111101",
   "1000000010" when "111111110",
   "1000000001" when "111111111",
   "----------" when others;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_64_f400_uid11
--                    (IntAdderAlternative_64_f400_uid15)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_64_f400_uid11 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(63 downto 0);
          Y : in  std_logic_vector(63 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(63 downto 0)   );
end entity;

architecture arch of IntAdder_64_f400_uid11 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(22 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(21 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(22 downto 0);
signal sum_l1_idx1 :  std_logic_vector(21 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(63 downto 42)) + ( "0" & Y(63 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(21 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(22 downto 22);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(21 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(22 downto 22);
   R <= sum_l1_idx1(21 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_64_f400_uid18
--                     (IntAdderClassical_64_f400_uid20)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_64_f400_uid18 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(63 downto 0);
          Y : in  std_logic_vector(63 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(63 downto 0)   );
end entity;

architecture arch of IntAdder_64_f400_uid18 is
signal x0 :  std_logic_vector(27 downto 0);
signal y0 :  std_logic_vector(27 downto 0);
signal x1, x1_d1 :  std_logic_vector(35 downto 0);
signal y1, y1_d1 :  std_logic_vector(35 downto 0);
signal sum0, sum0_d1 :  std_logic_vector(28 downto 0);
signal sum1 :  std_logic_vector(36 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            x1_d1 <=  x1;
            y1_d1 <=  y1;
            sum0_d1 <=  sum0;
         end if;
      end process;
   --Classical
   x0 <= X(27 downto 0);
   y0 <= Y(27 downto 0);
   x1 <= X(63 downto 28);
   y1 <= Y(63 downto 28);
   sum0 <= ( "0" & x0) + ( "0" & y0)  + Cin;
   ----------------Synchro barrier, entering cycle 1----------------
   sum1 <= ( "0" & x1_d1) + ( "0" & y1_d1)  + sum0_d1(28);
   R <= sum1(35 downto 0) & sum0_d1(27 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_63_f400_uid25
--                     (IntAdderClassical_63_f400_uid27)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 2 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_63_f400_uid25 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(62 downto 0);
          Y : in  std_logic_vector(62 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(62 downto 0)   );
end entity;

architecture arch of IntAdder_63_f400_uid25 is
signal x0 :  std_logic_vector(13 downto 0);
signal y0 :  std_logic_vector(13 downto 0);
signal x1, x1_d1 :  std_logic_vector(41 downto 0);
signal y1, y1_d1 :  std_logic_vector(41 downto 0);
signal x2, x2_d1, x2_d2 :  std_logic_vector(6 downto 0);
signal y2, y2_d1, y2_d2 :  std_logic_vector(6 downto 0);
signal sum0, sum0_d1, sum0_d2 :  std_logic_vector(14 downto 0);
signal sum1, sum1_d1 :  std_logic_vector(42 downto 0);
signal sum2 :  std_logic_vector(7 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            x1_d1 <=  x1;
            y1_d1 <=  y1;
            x2_d1 <=  x2;
            x2_d2 <=  x2_d1;
            y2_d1 <=  y2;
            y2_d2 <=  y2_d1;
            sum0_d1 <=  sum0;
            sum0_d2 <=  sum0_d1;
            sum1_d1 <=  sum1;
         end if;
      end process;
   --Classical
   x0 <= X(13 downto 0);
   y0 <= Y(13 downto 0);
   x1 <= X(55 downto 14);
   y1 <= Y(55 downto 14);
   x2 <= X(62 downto 56);
   y2 <= Y(62 downto 56);
   sum0 <= ( "0" & x0) + ( "0" & y0)  + Cin;
   ----------------Synchro barrier, entering cycle 1----------------
   sum1 <= ( "0" & x1_d1) + ( "0" & y1_d1)  + sum0_d1(14);
   ----------------Synchro barrier, entering cycle 2----------------
   sum2 <= ( "0" & x2_d2) + ( "0" & y2_d2)  + sum1_d1(42);
   R <= sum2(6 downto 0) & sum1_d1(41 downto 0) & sum0_d2(13 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_63_f400_uid32
--                    (IntAdderAlternative_63_f400_uid36)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_63_f400_uid32 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(62 downto 0);
          Y : in  std_logic_vector(62 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(62 downto 0)   );
end entity;

architecture arch of IntAdder_63_f400_uid32 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(21 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(20 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(21 downto 0);
signal sum_l1_idx1 :  std_logic_vector(20 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(62 downto 42)) + ( "0" & Y(62 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(20 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(21 downto 21);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(20 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(21 downto 21);
   R <= sum_l1_idx1(20 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_56_f400_uid39
--                    (IntAdderAlternative_56_f400_uid43)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_56_f400_uid39 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(55 downto 0);
          Y : in  std_logic_vector(55 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(55 downto 0)   );
end entity;

architecture arch of IntAdder_56_f400_uid39 is
signal s_sum_l0_idx0 :  std_logic_vector(29 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(27 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(28 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(26 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(27 downto 0);
signal sum_l1_idx1 :  std_logic_vector(26 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(28 downto 0)) + ( "0" & Y(28 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(55 downto 29)) + ( "0" & Y(55 downto 29));
   sum_l0_idx0 <= s_sum_l0_idx0(28 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(29 downto 29);
   sum_l0_idx1 <= s_sum_l0_idx1(26 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(27 downto 27);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(26 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(27 downto 27);
   R <= sum_l1_idx1(26 downto 0) & sum_l0_idx0_d1(28 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_56_f400_uid46
--                     (IntAdderClassical_56_f400_uid48)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_56_f400_uid46 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(55 downto 0);
          Y : in  std_logic_vector(55 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(55 downto 0)   );
end entity;

architecture arch of IntAdder_56_f400_uid46 is
signal x0 :  std_logic_vector(22 downto 0);
signal y0 :  std_logic_vector(22 downto 0);
signal x1, x1_d1 :  std_logic_vector(32 downto 0);
signal y1, y1_d1 :  std_logic_vector(32 downto 0);
signal sum0, sum0_d1 :  std_logic_vector(23 downto 0);
signal sum1 :  std_logic_vector(33 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            x1_d1 <=  x1;
            y1_d1 <=  y1;
            sum0_d1 <=  sum0;
         end if;
      end process;
   --Classical
   x0 <= X(22 downto 0);
   y0 <= Y(22 downto 0);
   x1 <= X(55 downto 23);
   y1 <= Y(55 downto 23);
   sum0 <= ( "0" & x0) + ( "0" & y0)  + Cin;
   ----------------Synchro barrier, entering cycle 1----------------
   sum1 <= ( "0" & x1_d1) + ( "0" & y1_d1)  + sum0_d1(23);
   R <= sum1(32 downto 0) & sum0_d1(22 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                            IntSquarer_31_uid53
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca (2009)
--------------------------------------------------------------------------------
-- Pipeline depth: 4 cycles

library ieee; 
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;
library work;
entity IntSquarer_31_uid53 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(30 downto 0);
          R : out  std_logic_vector(61 downto 0)   );
end entity;

architecture arch of IntSquarer_31_uid53 is
signal x0_16, x0_16_d1, x0_16_d2 :  std_logic_vector(17 downto 0);
signal x17_32, x17_32_d1, x17_32_d2, x17_32_d3 :  std_logic_vector(17 downto 0);
signal x17_32_shr, x17_32_shr_d1, x17_32_shr_d2 :  std_logic_vector(17 downto 0);
signal p0, p0_d1, p0_d2 :  std_logic_vector(35 downto 0);
signal p1_x2, p1_x2_d1 :  std_logic_vector(35 downto 0);
signal s1, s1_d1 :  std_logic_vector(35 downto 0);
signal p2, p2_d1 :  std_logic_vector(35 downto 0);
signal s2 :  std_logic_vector(35 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            x0_16_d1 <=  x0_16;
            x0_16_d2 <=  x0_16_d1;
            x17_32_d1 <=  x17_32;
            x17_32_d2 <=  x17_32_d1;
            x17_32_d3 <=  x17_32_d2;
            x17_32_shr_d1 <=  x17_32_shr;
            x17_32_shr_d2 <=  x17_32_shr_d1;
            p0_d1 <=  p0;
            p0_d2 <=  p0_d1;
            p1_x2_d1 <=  p1_x2;
            s1_d1 <=  s1;
            p2_d1 <=  p2;
         end if;
      end process;
   x0_16 <= "0" & X(16 downto 0);
   x17_32 <= "00" & "00" & X(30 downto 17);
   x17_32_shr <= "0" & "00" & X(30 downto 17) & "0";
   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   p0 <= x0_16_d2 * x0_16_d2;
   p1_x2 <= x17_32_shr_d2 * x0_16_d2;
   ----------------Synchro barrier, entering cycle 3----------------
   s1 <= p1_x2_d1 + ( "00000000000000000" & p0_d1(35 downto 17));
   p2 <= x17_32_d3 * x17_32_d3;
   ----------------Synchro barrier, entering cycle 4----------------
   s2 <= p2_d1 + ( "00000000000000000" & s1_d1(35 downto 17));
   R <= s2(27 downto 0) & s1_d1(16 downto 0) & p0_d2(16 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_56_f400_uid56
--                     (IntAdderClassical_56_f400_uid58)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_56_f400_uid56 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(55 downto 0);
          Y : in  std_logic_vector(55 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(55 downto 0)   );
end entity;

architecture arch of IntAdder_56_f400_uid56 is
signal x0 :  std_logic_vector(21 downto 0);
signal y0 :  std_logic_vector(21 downto 0);
signal x1, x1_d1 :  std_logic_vector(33 downto 0);
signal y1, y1_d1 :  std_logic_vector(33 downto 0);
signal sum0, sum0_d1 :  std_logic_vector(22 downto 0);
signal sum1 :  std_logic_vector(34 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            x1_d1 <=  x1;
            y1_d1 <=  y1;
            sum0_d1 <=  sum0;
         end if;
      end process;
   --Classical
   x0 <= X(21 downto 0);
   y0 <= Y(21 downto 0);
   x1 <= X(55 downto 22);
   y1 <= Y(55 downto 22);
   sum0 <= ( "0" & x0) + ( "0" & y0)  + Cin;
   ----------------Synchro barrier, entering cycle 1----------------
   sum1 <= ( "0" & x1_d1) + ( "0" & y1_d1)  + sum0_d1(22);
   R <= sum1(33 downto 0) & sum0_d1(21 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                              LogTable_0_9_83
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity LogTable_0_9_83 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(8 downto 0);
          Y : out  std_logic_vector(82 downto 0)   );
end entity;

architecture arch of LogTable_0_9_83 is
   -- Build a 2-D array type for the RoM
   subtype word_t is std_logic_vector(82 downto 0);
   type memory_t is array(0 to 511) of word_t;
   function init_rom
      return memory_t is 
      variable tmp : memory_t := (
   "11111111111110111111111110111111111111110000000000000000000000000000000000000000000",
   "11111111111110111111111110111111111111110000000000000000000000000000000000000000000",
   "00000000011111000001111111001010101011011010110001000100111011110011100000110011100",
   "00000000111111001000000000010101100101001000100010110011010101111110010110011000111",
   "00000001011111010010000011100001010001001000011010110101010000001110000010100101110",
   "00000001111111100000001001101110101100000001101111001110001001010001100110011000110",
   "00000010011111110010010011111111001011000000011001011101111100011101010101110100000",
   "00000011000000001000100011010100011100000001010001010101010001000001001011000101100",
   "00000011100000100010111000110000100101111110101111100000110011000000001100010000010",
   "00000100000001000001010101010110001000111101011000010001110100100111110010001110100",
   "00000100100001100011111010000111111110011000101110001001111100011001010000100100110",
   "00000101000010001010101000001001011001010000001100110000001001110011001001010000110",
   "00000101100010110101100000011110000110010100001011110101011111100111000100101011001",
   "00000110000011100100100100001010001100010011001010101111101000101110011011010011000",
   "00000110100100010111110100010010001100000111000100001111110001111011000001101001100",
   "00000111000101001111010001111011000001000010101010111100011000110001111111001111011",
   "00000111100110001010111110001010000000111111001110010100010101110101101101000100011",
   "00001000000111001010111010000100111100101010001000100010001110000000101110011110010",
   "00001000101000001111000110110001111111110010110101000110011101010110010110111110101",
   "00001001001001010111100101010111110001011000110000011111010011010111000000010000111",
   "00001001101010100100010110111101010011111001100000110101011111010101111011111001111",
   "00001010001011110101011100101010000101011111000111110100110001100100010100100001000",
   "00001010101101001010110111100110000000001110011101110111011100100010000000111011100",
   "00001011001110100100101000111001011010010101110110101100001000001000001001111100110",
   "00001011001110100100101000111001011010010101110110101100001000001000001001111100110",
   "00001011110000000010110001101101000110011011101111011101001111001011110111110001010",
   "00001100010001100101010011001010010011101101100110100001100110101000101111101001100",
   "00001100110011001100001110011010101110001110111100111101110100011011000100111011001",
   "00001101010100110111100100101000011111001000100001111110000011010001101010011101111",
   "00001101110110100111010110111110001100110111101000010000000111100101110011110011101",
   "00001110011000011011100110100110111011011101100101100101110000110110111000001010000",
   "00001110111010010100010100101110001100101111011100100111001010011100110110010011000",
   "00001111011100010001100010100000000000100101110000111101110110000011011000000110111",
   "00001111111110010011010001001000110101001100100110000000010001100100110000011100000",
   "00001111111110010011010001001000110101001100100110000000010001100100110000011100000",
   "00010000100000011001100001110101100111010011101000000110100010001010001111111101101",
   "00010001000010100100010101110011110010011110100000110000100101101101011110110101110",
   "00010001100100110011101110010001010001010101010101100110110100010101010011111001010",
   "00010010000111000111101100011100011101110101010010011101100011001111110000010000110",
   "00010010101001100000010001100100010001100001011110100100100010111010100010110001110",
   "00010011001011111101011110111000000101110011111101001011011010100100101010111111001",
   "00010011001011111101011110111000000101110011111101001011011010100100101010111111001",
   "00010011101110011111010101100111110100001110111001100100001011110101001011101101101",
   "00010100010001000101110111000011110110101101111110101101010001101110100011010010011",
   "00010100110011110001000100011101000111110111111010101100010111011010011111010000110",
   "00010101010110100000111111000101000011010000001110000011101011101000000110001010100",
   "00010101111001010101101000001101100101101001000111001011011111001110000111010001100",
   "00010110011100001111000001001001001101010101101001111001100110010100110010011101010",
   "00010110011100001111000001001001001101010101101001111001100110010100110010011101010",
   "00010110111111001101001011001010111010011100000011100000111101000110110101001101000",
   "00010111100010010000000111100110001111001000001011010011011010101111001010010110111",
   "00011000000101010111110111101111001111111110001111110000000110111001111100101110000",
   "00011000101000100100011100111010100100001101110000101000110000001010111110011011111",
   "00011001001011110101111000011101010110000100100110001000101011100001110101011110101",
   "00011001001011110101111000011101010110000100100110001000101011100001110101011110101",
   "00011001101111001100001011101101010011000010010101001000001111110110000101111100100",
   "00011010010010100111011000000000101100001011110000110111101010010010100011010111100",
   "00011010110110000111011110101110010110011110101010001100010011011111011000111100111",
   "00011011011001101100100001001101101011000101101100011011110111111111010100000010001",
   "00011011011001101100100001001101101011000101101100011011110111111111010100000010001",
   "00011011111101010110100000110110100111101100101000010000110001100100011011000001001",
   "00011100100001000101011111000001101110110100101100100011011010001110001100101101011",
   "00011101000100111001011101001000001000001001001101100100001100111011010111101111110",
   "00011101101000110010011100100011100000110100011010100010010100000000011111101001110",
   "00011101101000110010011100100011100000110100011010100010010100000000011111101001110",
   "00011110001100110000011110101110001011110100100001111011010000100111001100111011010",
   "00011110110000110011100101000011000010010001000100011111110010111010100000001100111",
   "00011111010100111011110000111101100011110000010111011010100110110010011110101010001",
   "00011111111001001001000011111001110110101101010101100101100101001101100000101111100",
   "00011111111001001001000011111001110110101101010101100101100101001101100000101111100",
   "00100000011101011011011111010100101000101101100000011010100111001111000101101010000",
   "00100001000001110011000100101011001110110111010000001101000100010100011111111011010",
   "00100001100110001111110101011011100110001000010100011001010010110110101011111111111",
   "00100001100110001111110101011011100110001000010100011001010010110110101011111111111",
   "00100010001010110001110011000100010011101100100011110111101111000110011100010110100",
   "00100010101111011000111111000100100101010100111101100001011010010101010010000000010",
   "00100011010100000101011010111100010001101110111001010011110001110010001000011100010",
   "00100011010100000101011010111100010001101110111001010011110001110010001000011100010",
   "00100011111000110111001000001011111000111011101010000010001011000101100100111110000",
   "00100100011101101110001000010100100100101000010000000011010010001110001111110101000",
   "00100101000010101010011100111000001000100101011101001001010011100011010110010011000",
   "00100101000010101010011100111000001000100101011101001001010011100011010110010011000",
   "00100101100111101100000111011001000011000000001001110011101011011001100101110000010",
   "00100110001100110011001001011010011100111001111100001001100011101110001110100101000",
   "00100110110001111111100100100000001010100010000000101100010111110101000001000110101",
   "00100110110001111111100100100000001010100010000000101100010111110101000001000110101",
   "00100111010111010001011010001110101011101110010101010010000101110000110101101100111",
   "00100111111100101000101100001011001100010101000110010111000100111100000011001010111",
   "00101000100010000101011011111011100100100110011110110111101001110101100011101110010",
   "00101000100010000101011011111011100100100110011110110111101001110101100011101110010",
   "00101001000111100111101011000110011001100110101011000001101111001010101001111110111",
   "00101001101101001111011011010010111101101000001110001111001101101100000101011100010",
   "00101001101101001111011011010010111101101000001110001111001101101100000101011100010",
   "00101010010010111100101110001001010000100110101100011001111101000111001010110100110",
   "00101010111000101111100101010010000000100001100110111010101001111010011010111000100",
   "00101011011110101000000010010110101001110111101101100011111101110000010100100101111",
   "00101011011110101000000010010110101001110111101101100011111101110000010100100101111",
   "00101100000100100110000111000001011000000010100011101011101010010011011001110111000",
   "00101100101010101001110100111101000101110010011001110011111000101100101100001111000",
   "00101100101010101001110100111101000101110010011001110011111000101100101100001111000",
   "00101101010000110011001101110101011101101010011100000110110010110001100110010101111",
   "00101101110111000010010011010110111010011101010101110111001110010000110100110010000",
   "00101110011101010111000111001110100111101010001010011001010101101111001100000000000",
   "00101110011101010111000111001110100111101010001010011001010101101111001100000000000",
   "00101111000011110001101011001010100001111001100011100110011110111110001111011011110",
   "00101111101010010010000000111001010111011011010110011111110010100111010000011110100",
   "00101111101010010010000000111001010111011011010110011111110010100111010000011110100",
   "00110000010000111000001010001010101000100100011110000011011101101110011010011000110",
   "00110000110111100100001000101110101000001101001100101000111010110100001011111001110",
   "00110000110111100100001000101110101000001101001100101000111010110100001011111001110",
   "00110001011110010101111110010110011100001111110100011000010101010110101010100010111",
   "00110010000101001101101100110011111110000111100110110010100000101010001000111001110",
   "00110010000101001101101100110011111110000111100110110010100000101010001000111001110",
   "00110010101100001011010101111001111011010000001011111110010001010000101100010100001",
   "00110011010011001110111011011011110101100101010001110000111110100011111011111110110",
   "00110011010011001110111011011011110101100101010001110000111110100011111011111110110",
   "00110011111010011000011111001110000100000010110011001000001001101011010000110010110",
   "00110100100001101000000011000101110011000101011000001010011101111100100000011111100",
   "00110100100001101000000011000101110011000101011000001010011101111100100000011111100",
   "00110101001000111101101000111001000101001011001111000110110111100101011111010101000",
   "00110101110000011001010010011110110011010101011110101000110101011010111001010101000",
   "00110101110000011001010010011110110011010101011110101000110101011010111001010101000",
   "00110110010111111011000001101110101101101001110001111001010011100101011000101010011",
   "00110110111111100010111000100001011011110100011110100100000110100000111011100111010",
   "00110110111111100010111000100001011011110100011110100100000110100000111011100111010",
   "00110111100111010000111000110000011101101011000101011010000111011101001101010110000",
   "00111000001111000101000100010110001011101111001101101000111110001000110101010001110",
   "00111000001111000101000100010110001011101111001101101000111110001000110101010001110",
   "00111000110110111111011101001101110111110001111011100001001110001101001110100110101",
   "00111001011111000000000101010011101101010111100010100100101010011110100010101010001",
   "00111001011111000000000101010011101101010111100010100100101010011110100010101010001",
   "00111010000111000110111110100100110010011011110011110110101100000011000101001001001",
   "00111010000111000110111110100100110010011011110011110110101100000011000101001001001",
   "00111010101111010100001010111111000111110110101000101001000111111001000000111110000",
   "00111011010111100111101100100001101010000001001010000000011010101000010001110101001",
   "00111011010111100111101100100001101010000001001010000000011010101000010001110101001",
   "00111100000000000001100101001100010001011011010101101010011111111010100100100011100",
   "00111100101000100001110110111111110011010010000000100100001001000100111011000000100",
   "00111100101000100001110110111111110011010010000000100100001001000100111011000000100",
   "00111101010001001000100011111110000010000101010111101001000101100110001101110011001",
   "00111101010001001000100011111110000010000101010111101001000101100110001101110011001",
   "00111101111001110101101110001001101110001111111111001011101111011101010100110100100",
   "00111110100010101001010111100110100110101110010001010001101101100000111000100000100",
   "00111110100010101001010111100110100110101110010001010001101101100000111000100000100",
   "00111111001011100011100010011001011001100110011011110010111110110011001010000011010",
   "00111111001011100011100010011001011001100110011011110010111110110011001010000011010",
   "00111111110100100100010000100111110100110000111110011010000011000110111100000000010",
   "01000000011101101011100100011000100110100001101001000011110111001111101011011110100",
   "01000000011101101011100100011000100110100001101001000011110111001111101011011110100",
   "01000001000110111001011111110011011110010000111011011110111110001000011010011100110",
   "01000001110000001110000101000001001101000110000110001001110011011011000101101110110",
   "01000001110000001110000101000001001101000110000110001001110011011011000101101110110",
   "01000010011001101001010110001011100110100001101101010000101000011110000101110011010",
   "01000010011001101001010110001011100110100001101101010000101000011110000101110011010",
   "01000011000011001011010101011101100001001000101110001100010001011000101100011110100",
   "01000011000011001011010101011101100001001000101110001100010001011000101100011110100",
   "01000011101100110100000101000010110111010000001000000011001101101010010110110000100",
   "01000100010110100011100111001000100111101001000111101111011110001100111000101101000",
   "01000100010110100011100111001000100111101001000111101111011110001100111000101101000",
   "01000101000000011001111101111100110110001101111000001100000001111000001001001101110",
   "01000101000000011001111101111100110110001101111000001100000001111000001001001101110",
   "01000101101010010111001011101110101100101110110111001101011001101111101111010010100",
   "01000110010100011011010010101110011011100000101111101001011110111001110101101000110",
   "01000110010100011011010010101110011011100000101111101001011110111001110101101000110",
   "01000110111110100110010101001101011010001010111001010011011101101010100010100011111",
   "01000110111110100110010101001101011010001010111001010011011101101010100010100011111",
   "01000111101000111000010101011110001000010110011111010001010100010010110000110111010",
   "01000111101000111000010101011110001000010110011111010001010100010010110000110111010",
   "01001000010011010001010101110100001110011110001101010000111110111101011101001000110",
   "01001000111101110001011000100100011110011110100100100100000110111011011101011101001",
   "01001000111101110001011000100100011110011110100100100100000110111011011101011101001",
   "01001001101000011000100000000100110100100110111001001001111000001110111110101101111",
   "01001001101000011000100000000100110100100110111001001001111000001110111110101101111",
   "01001010010011000110101110101100011000001010110111101111001111010100100011001111001",
   "01001010010011000110101110101100011000001010110111101111001111010100100011001111001",
   "01001010111101111100000110110011011100010100110101001110011111010010010100101000000",
   "01001010111101111100000110110011011100010100110101001110011111010010010100101000000",
   "01001011101000111000101010110011100000111000101000011000000001100000100101000110100",
   "01001100010011111100011101000111010011000111001110001110110000101001011001101111011",
   "01001100010011111100011101000111010011000111001110001110110000101001011001101111011",
   "01001100111111000111100000001010101110100010111010000011100011001010101010010001110",
   "01001100111111000111100000001010101110100010111010000011100011001010101010010001110",
   "01001101101010011001110110011010111101110100010001011011101100110110110000111100011",
   "01001101101010011001110110011010111101110100010001011011101100110110110000111100011",
   "01001110010101110011100010010110011011011111110101001111100011000111010100110011100",
   "01001110010101110011100010010110011011011111110101001111100011000111010100110011100",
   "01001111000001010100100110011100110010111100011000001110110101010010110110000001110",
   "01001111000001010100100110011100110010111100011000001110110101010010110110000001110",
   "01001111101100111101000101001111000001001010000011111001100000111101011010000000110",
   "01010000011000101101000001001111010101101010001100011100100001101101110110010010001",
   "01010000011000101101000001001111010101101010001100011100100001101101110110010010001",
   "01010001000100100100011101000001010011010111110100100010110101001110110110101101110",
   "01010001000100100100011101000001010011010111110100100010110101001110110110101101110",
   "01010001110000100011011011001001110001100001000001101100000110001111101101001100000",
   "01010001110000100011011011001001110001100001000001101100000110001111101101001100000",
   "01010010011100101001111110001110111100100001000001111011001100110100110110101111010",
   "01010010011100101001111110001110111100100001000001111011001100110100110110101111010",
   "01010011001000111000001000111000010110111011000011101111101110110111001001111010100",
   "01010011001000111000001000111000010110111011000011101111101110110111001001111010100",
   "01010011110101001101111101101110111010010110000000111110101001110011010011001110111",
   "01010011110101001101111101101110111010010110000000111110101001110011010011001110111",
   "01010100100001101011011111011100111000011000111101011111001110000100000010110010011",
   "01010100100001101011011111011100111000011000111101011111001110000100000010110010011",
   "01010101001110010000110000101101111011101000011010100010010101010011000000100100010",
   "01010101001110010000110000101101111011101000011010100010010101010011000000100100010",
   "01010101111010111101110100001111001000100100011111101011011110110111101110010111010",
   "01010101111010111101110100001111001000100100011111101011011110110111101110010111010",
   "01010110100111110010101100101110111110100111111010000011100001100100111101010000100",
   "01010110100111110010101100101110111110100111111010000011100001100100111101010000100",
   "01010111010100101111011100111101011001000111110010111110100110101011111011101011100",
   "01010111010100101111011100111101011001000111110010111110100110101011111011101011100",
   "01011000000001110100000111101011110000010100011110101111101001000101110110110111110",
   "01011000000001110100000111101011110000010100011110101111101001000101110110110111110",
   "01011000101111000000101111101100111010011011000100100100111011100000110001100111010",
   "01011000101111000000101111101100111010011011000100100100111011100000110001100111010",
   "01011001011100010101010111110101001100101000000000101010100010011111101001011011100",
   "01011001011100010101010111110101001100101000000000101010100010011111101001011011100",
   "01011010001001110010000010111010011100001010100001010000011010011001001110101011100",
   "01011010001001110010000010111010011100001010100001010000011010011001001110101011100",
   "01011010110111010110110011110011111111011000111111110011001110101000011010011011100",
   "01011010110111010110110011110011111111011000111111110011001110101000011010011011100",
   "01011011100101000011101101011010101110110110010111001000010110010101111100001000100",
   "01011011100101000011101101011010101110110110010111001000010110010101111100001000100",
   "01011100010010111000110010101001000110011000010111101110011011001001100001011111101",
   "01011100010010111000110010101001000110011000010111101110011011001001100001011111101",
   "01011101000000110110000110011011000110001110111011000101100001001010010011101110010",
   "01011101000000110110000110011011000110001110111011000101100001001010010011101110010",
   "01011101101110111011101011101110010100001100010111010010111011100111001001100001010",
   "01011101101110111011101011101110010100001100010111010010111011100111001001100001010",
   "01011110011101001001100101100001111100101110110011110110001111101001011101111110100",
   "01011110011101001001100101100001111100101110110011110110001111101001011101111110100",
   "01011111001011011111110110110110110100001010100000110110011110111100011111111101110",
   "01011111001011011111110110110110110100001010100000110110011110111100011111111101110",
   "01011111111001111110100010101111010111110101010001101111101101111001101111110110110",
   "01011111111001111110100010101111010111110101010001101111101101111001101111110110110",
   "01100000101000100101101100001111101111010010111100101010110101010101110011111010110",
   "01100000101000100101101100001111101111010010111100101010110101010101110011111010110",
   "01100001010111010101010110011101101101100010111111101010101101111001100110011001110",
   "01100001010111010101010110011101101101100010111111101010101101111001100110011001110",
   "01100001010111010101010110011101101101100010111111101010101101111001100110011001110",
   "01100010000110001101100100100000110010001111001100111011100011101111000001000000001",
   "01100010000110001101100100100000110010001111001100111011100011101111000001000000001",
   "01100010110101001110011001100010001010111011011111010010100011110000110100001100100",
   "01100010110101001110011001100010001010111011011111010010100011110000110100001100100",
   "01100011100100010111111000101100110100010110111000001101111000100111011000110111011",
   "01100011100100010111111000101100110100010110111000001101111000100111011000110111011",
   "01100100010011101010000101001101011011101101101000100110010100101011010001000110100",
   "01100100010011101010000101001101011011101101101000100110010100101011010001000110100",
   "01100101000011000101000010010010011111111100100101100101110000001110010010110000000",
   "01100101000011000101000010010010011111111100100101100101110000001110010010110000000",
   "01100101000011000101000010010010011111111100100101100101110000001110010010110000000",
   "01100101110010101000110011001100010011000101101010110111001110110001011010111111010",
   "01100101110010101000110011001100010011000101101010110111001110110001011010111111010",
   "01100110100010010101011011001100111011100101101011100111001101010011010101000011100",
   "01100110100010010101011011001100111011100101101011100111001101010011010101000011100",
   "01100111010010001010111101101000010101101011010011101100001100001111011110010111101",
   "10110110001101100111100101111011000000001110101100001011000011100000100111101010101",
   "10110110100101100111111101111100100000010010011100011000010000111101110110110001011",
   "10110110111101101010100110010000000010101010111111110000000000001100110111010111000",
   "10110110111101101010100110010000000010101010111111110000000000001100110111010111000",
   "10110111010101101111011111010000110000010001101001101001011101101111010111101011000",
   "10110111101101110110101001011001111001101010011111010001100001110100010111010110110",
   "10111000000110000000000101000110110111000111010000110101110010011111110111111001101",
   "10111000011110001011110010110011001000101010010100000111000011010101011001001111010",
   "10111000110110011001110010111010010110001001100000010100000100001110010111000110010",
   "10111001001110101010000101111000001111010001001011011101010001100110110111000101011",
   "10111001100110111100101100001000101011100111001001000010011000101100001011100101000",
   "10111001100110111100101100001000101011100111001001000010011000101100001011100101000",
   "10111001111111010001100110000111101010101101101010001010100010111010011101001010001",
   "10111010010111101000110100010001010100000110011111000111111100011000110011100000110",
   "10111010110000000010010111000001110111010101111010010111101001100101110111111100100",
   "10111011001000011110001110110101101100000101110100111110100001001001100111010110100",
   "10111011100000111100011100001001010010001000110100100011111111000100011111100110100",
   "10111011111001011100111111011001010001011101010010101011100111011000000101111101010",
   "10111011111001011100111111011001010001011101010010101011100111011000000101111101010",
   "10111100010001111111111001000010011010010000100101101110001110101001010000101101010",
   "10111100101010100101001001100001100101000010001011010011011111100000100110111111001",
   "10111101000011001100110001010011110010100110110100001100110100110011001110100010010",
   "10111101011011110110110000110110001100001011110001110010100000100011000000100001100",
   "10111101110100100011001000100110000011011010000101000011111000101011111100111010011",
   "10111110001101010001111001000000110010011001101111001011100010111010001111100000001",
   "10111110001101010001111001000000110010011001101111001011100010111010001111100000001",
   "10111110100110000011000010100011111011110101000011101000011001101011110011010010010",
   "10111110111110110110100101101101001010111011111011111100100101000111011000010000110",
   "10111111010111101100100010111010010011100111001101000011000010111011000110000011111",
   "10111111110000100100111010101001010010011011111110001100111001011100101010110100100",
   "10111111110000100100111010101001010010011011111110001100111001011100101010110100100",
   "11000000001001011111101101011000001100101111000001100111010010001110011010010101000",
   "11000000100010011100111011100101010000101000001110101010111001010101010101110011001",
   "11000000111011011100100101101110110101000101111101110101111111010110101001010010100",
   "11000001010100011110101100010011011010000000100110010001111100011101000001001001000",
   "11000001101101100011001111110001101000001101111101000101010011110001010000010111111",
   "11000001101101100011001111110001101000001101111101000101010011110001010000010111111",
   "11000010000110101010010000101000010001100100110110010011010110111100111001000011011",
   "11000010011111110011101111010110010001000000100111101010001010011001100001111110111",
   "11000010111000111111101100011010101010100100101101000000001011011100000001110100100",
   "11000011010010001110001000010100101011100000001110100010011010010111100111100011110",
   "11000011010010001110001000010100101011100000001110100010011010010111100111100011110",
   "11000011101011011111000011100011101010010001101000110100001011000010101011110000000",
   "11000100000100110010011110100111000110101010010110100001011111010100111101001000100",
   "11000100011110001000011001111110101001110010011100000101001111100101100111010111010",
   "11000100110111100000110110001010000110001100010101000100000101111111000011011001010",
   "11000100110111100000110110001010000110001100010101000100000101111111000011011001010",
   "11000101010000111011110011101001010111111000100011011101010010001101110111100101010",
   "11000101101010011001010010111100100100011001100000110010011100000001001010001010101",
   "11000110000011111001010100100011111010110111010001000111011011100011001011111100001",
   "11000110011101011011111000111111110100000011010111111011011111100011001011001101011",
   "11000110011101011011111000111111110100000011010111111011011111100011001011001101011",
   "11000110110111000001000000110000110010011100101110111100101001111011010001001111110",
   "11000111010000101000101100010111100010010011011110110110101100010000100110100101111",
   "11000111101010010010111100010100111001101100111001111110101110011110111101110001110",
   "11000111101010010010111100010100111001101100111001111110101110011110111101110001110",
   "11001000000011111111110001001001111000100111011000111100101010110101110000011110010",
   "11001000011101101111001011010111101000111110011001010011101011000000110001100011000",
   "11001000110111100001001011011111011110101110011110001010110011001100110111101000100",
   "11001001010001010101110010000010110111111001010010110111001000101110110000000001001",
   "11001001010001010101110010000010110111111001010010110111001000101110110000000001001",
   "11001001101011001100111111100011011100101001101111101000100010101001000001101101000",
   "11001010000101000110110100100010111111011000000000011010010011100110001100111111110",
   "11001010011111000011010001100011011100101101101101101000111101010011100101011111000",
   "11001010011111000011010001100011011100101101101101101000111101010011100101011111000",
   "11001010111001000010010111000110111011101010000111001110011110100011000011011110010",
   "11001011010011000100000101101111101101100110010001100110001101110011010111100010011",
   "11001011101101001000011110000000001110011001010100111001110011011001000111011100101",
   "11001011101101001000011110000000001110011001010100111001110011011001000111011100101",
   "11001100000111001111100000011011000100011100101110011000010110111101100111111100111",
   "11001100100001011001001101100011000000110000100011111001010101000101000011100111000",
   "11001100111011100101100101111010111110111111111001101100010010101001101100100010110",
   "11001100111011100101100101111010111110111111111001101100010010101001101100100010110",
   "11001101010101110100101010000110000101100101001010010111000100110111110110011111001",
   "11001101110000000110011010100111100101101110100001000011100101010100001000100001000",
   "11001110001010011010111000000010111011100010010101111110101010110100111011000010100",
   "11001110001010011010111000000010111011100010010101111110101010110100111011000010100",
   "11001110100100110010000010111011101110000011101101001001100000110111101111110100100",
   "11001110111111001011111011110101101111010110110111011110110111111011111111101110000",
   "11001111011001101000100011010100111100100101110110001101101010101101111101001110001",
   "11001111011001101000100011010100111100100101110110001101101010101101111101001110001",
   "11001111110100000111111001111101011110000101000000101010010100101111100100100000001",
   "11010000001110101010000000010011100111010111101100011000011000001111100010011100010",
   "11010000001110101010000000010011100111010111101100011000011000001111100010011100010",
   "11010000101001001110110110111011110111010100110111101101110010000011100100101001101",
   "11010001000011110110011110011010111000001011110110110001011011011111100101110000110",
   "11010001011110100000110111010101011111101001000010110110011011001001100100000001100",
   "11010001011110100000110111010101011111101001000010110110011011001001100100000001100",
   "11010001111001001110000010010000101110111010101100010101100110110000100101010110101",
   "11010010010011111101111111110001110010110101101111000110111001010101011100010011010",
   "11010010010011111101111111110001110010110101101111000110111001010101011100010011010",
   "11010010101110110000110000011110000011111010101001011100000001111011101101110010111",
   "11010011001001100110010100111011000110011010010101011110010000100100000000111001010",
   "11010011100100011110101101101110101010011011000101010000100111111010101000111111010",
   "11010011100100011110101101101110101010011011000101010000100111111010101000111111010",
   "11010011111111011001111011011110101011111101100001011000011011110001100000111011111",
   "11010100011010010111111110110001010011000001101010001101100101001000110011010010010",
   "11010100011010010111111110110001010011000001101010001101100101001000110011010010010",
   "11010100110101011000111000001100110011101011111011110100010110010111010010001010101",
   "11010101010000011100101000010111101110001010010100100010011010110010001001011001110",
   "11010101010000011100101000010111101110001010010100100010011010110010001001011001110",
   "11010101101011100011001111111000101110111001011110010000101110100011011111111100010",
   "11010110000110101100101111010110101110101001111010011011111100101111101011111110110",
   "11010110100001111001000111011000110010100101010000110101010010111011001111110010001",
   "11010110100001111001000111011000110010100101010000110101010010111011001111110010001",
   "11010110111101001000011000100110001100010011100001000101011010110110001101110000001",
   "11010111011000011010100011100110011010000000010111000011001100000101100001001101011",
   "11010111011000011010100011100110011010000000010111000011001100000101100001001101011",
   "11010111110011101111101001000001000110100000100010000000001000111000010000001111110",
   "11011000001111000111101001011110001001010111001110101100011010101100111010100011101",
   "11011000001111000111101001011110001001010111001110101100011010101100111010100011101",
   "11011000101010100010100101100101100110111011100100010100000100100010000010101000101",
   "11011001000110000000011101111111110000011110000100010111100010000110010011010010100",
   "11011001000110000000011101111111110000011110000100010111100010000110010011010010100",
   "11011001100001100001010011010101000100001110001101100001001100110101110100000001100",
   "11011001111101000101000110001110001101100000000001011010000000101101011100111001000",
   "11011001111101000101000110001110001101100000000001011010000000101101011100111001000",
   "11011010011000101011110111010100000100110001101101011110111100011001000110110000000",
   "11011010110100010101100111001111101111110001010110111001011001111111010001000111001",
   "11011010110100010101100111001111101111110001010110111001011001111111010001000111001",
   "11011011010000000010010110101010100001100010101001011100011110101011000011101000101",
   "11011011101011110010000110001101111010100100101001101001000001010101110000100000100",
   "11011011101011110010000110001101111010100100101001101001000001010101110000100000100",
   "11011100000111100100110110100011101000110111101001111010101001110010001111011010001",
   "11011100100011011010101000010101101000000011000010111111101011011111001111110111010",
   "11011100100011011010101000010101101000000011000010111111101011011111001111110111010",
   "11011100111111010011011100001110000001011011001111011110000000101001011110110110001",
   "11011101011011001111010010110111001100000111101010100111001011101011101111110110000",
   "11011101011011001111010010110111001100000111101010100111001011101011101111110110000",
   "11011101110111001110001100111011101101001000110010011101100111000010000110011100110",
   "11011110010011010000001011000110010111011110001101001101001100101101000100011101000",
   "11011110010011010000001011000110010111011110001101001101001100101101000100011101000",
   "11011110101111010101001110000010001100001100110001111001100000101011100010101011111",
   "11011111001011011101010110011010011010100100110100100011101010111100111001101110000",
   "11011111001011011101010110011010011010100100110100100011101010111100111001101110000",
   "11011111100111101000100100111010100000001000010101101010001111101001011100111000000",
   "11100000000011110110111010001110001000110001010101000101010101011101000110101110011",
   "11100000000011110110111010001110001000110001010101000101010101011101000110101110011",
   "11100000100000001000010111000001001110111000001000100001001100001111110100010011000",
   "11100000100000001000010111000001001110111000001000100001001100001111110100010011000",
   "11100000111100011100111011111111111011011001110101011001100111101000001100110110100",
   "11100001011000110100101001110110100101111110101110011000100010110111011000111011000",
   "11100001011000110100101001110110100101111110101110011000100010110111011000111011000",
   "11100001110101001111100001010001110101000000110100011010000101100001010101111000000",
   "11100010010001101101100010111110011101110010011011011000100001111010100101010001111",
   "11100010010001101101100010111110011101110010011011011000100001111010100101010001111",
   "11100010101110001110101111101001100100100100110010100010101000011011110010001001101",
   "11100011001010110011001000000000011100101110110000011110101100100100101011110010001",
   "11100011001010110011001000000000011100101110110000011110101100100100101011110010001",
   "11100011100111011010101100110000101000110011100010111100111010100010100111101000000",
   "11100011100111011010101100110000101000110011100010111100111010100010100111101000000",
   "11100100000100000101011110100111111010101001100010011011011110001011011011011111001",
   "11100100100000110011011110010100010011100001001001011110111101111011110000111001100",
   "11100100100000110011011110010100010011100001001001011110111101111011110000111001100",
   "11100100111101100100101100100100000100001011110000000001101110100111110011100001000",
   "11100100111101100100101100100100000100001011110000000001101110100111110011100001000",
   "11100101011010011001001010000101101101000010101010011100100110101111010000101101111",
   "11100101110111010000110111100111111110001110001100101011111010001001001000000110100",
   "11100101110111010000110111100111111110001110001100101011111010001001001000000110100",
   "11100110010100001011110101111001110111101100110001010011001001000001001100111100010",
   "11100110110001001010000101101010101001011010000100100010001111001000110001111111010",
   "11100110110001001010000101101010101001011010000100100010001111001000110001111111010",
   "11100111001110001011100111101001110011010110010011011111000010100101001101001011111",
   "11100111001110001011100111101001110011010110010011011111000010100101001101001011111",
   "11100111101011010000011100100111000101101101011111010101110011010010010001101001100",
   "11101000001000011000100101010010100000111110110100110011011110111011101101111111010",
   "11101000001000011000100101010010100000111110110100110011011110111011101101111111010",
   "11101000100101100100000010011100010110000100000111101100101111000000001101011100010",
   "11101000100101100100000010011100010110000100000111101100101111000000001101011100010",
   "11101001000010110010110100110101000110011001010010110100011001000001101101100110010",
   "11101001100000000100111101001101100100000011111100000100011011011010010010110000100",
   "11101001100000000100111101001101100100000011111100000100011011011010010010110000100",
   "11101001111101011010011100010110110001111010111100111100010011100010000100010110010",
   "11101001111101011010011100010110110001111010111100111100010011100010000100010110010",
   "11101010011010110011010011000010000011101110001111010111101100001010100011011101011",
   "11101010111000001111100010000000111110001110011111000000100101101101100001110001101",
   "11101010111000001111100010000000111110001110011111000000100101101101100001110001101",
   "11101011010101101111001010000101010111010100111110111111111100001001100001000100110",
   "11101011010101101111001010000101010111010100111110111111111100001001100001000100110",
   "11101011110011010010001100000001010110001011100100001111110001000100010101100000111",
   "11101011110011010010001100000001010110001011100100001111110001000100010101100000111",
   "11101100010000111000101000100111010011010100100100010010000010101100101001010110010",
   "11101100101110100010100000101001111000110010111000101111011011010110011001111100100",
   "11101100101110100010100000101001111000110010111000101111011011010110011001111100100",
   "11101101001100001111110100111100000010010010000111100001000111010011010110011010100",
   "11101101001100001111110100111100000010010010000111100001000111010011010110011010100",
   "11101101101010000000100110010000111101001110101111101001000001110000010010010110011",
   "11101110000111110100110101011100001000111110011010111011101100001010000101100001100",
   "11101110000111110100110101011100001000111110011010111011101100001010000101100001100",
   "11101110100101101100100011010001010110111000010100011111000101111001110110100011111",
   "11101110100101101100100011010001010110111000010100011111000101111001110110100011111",
   "11101111000011100111110000100100101010011101100100000001111101000110110101001000101",
   "11101111000011100111110000100100101010011101100100000001111101000110110101001000101",
   "11101111100001100110011110001010011001100001101110001110110011111110010010110101101",
   "11101111111111101000101100110111001100010011011001111110011001000101111011001110101",
   "11101111111111101000101100110111001100010011011001111110011001000101111011001110101",
   "11110000011101101110011101011111111101100100111010101100110011110100000110011011010",
   "11110000011101101110011101011111111101100100111010101100110011110100000110011011010",
   "11110000111011110111110000111001111010110100111111110101001000110011001000110001010",
   "11110000111011110111110000111001111010110100111111110101001000110011001000110001010",
   "11110001011010000100100111111010100100010111101001010110111101110000111000011101011",
   "11110001011010000100100111111010100100010111101001010110111101110000111000011101011",
   "11110001111000010101000011010111101101011111000001101001100110011011001010100000010",
   "11110010010110101001000100000111011100100100011100100000100111101011100101110101101",
   "11110010010110101001000100000111011100100100011100100000100111101011100101110101101",
   "11110010110101000000101011000000001011010001011011100101100001001010000001000000001",
   "11110010110101000000101011000000001011010001011011100101100001001010000001000000001",
   "11110011010011011011111000111000100110101000111000001010010000010100101111100011000",
   "11110011010011011011111000111000100110101000111000001010010000010100101111100011000",
   "11110011110001111010101110100111101111010000010010011000100011101000010011010010110",
   "11110011110001111010101110100111101111010000010010011000100011101000010011010010110",
   "11110100010000011101001101000100111001011001000110000001110111001110010110000110110",
   "11110100101111000011010101000111101101001010000100110011111000001100000110000001110",
   "11110100101111000011010101000111101101001010000100110011111000001100000110000001110",
   "11110101001101101101000111101000000110101000110110010101101110011100101010111000011",
   "11110101001101101101000111101000000110101000110110010101101110011100101010111000011",
   "11110101101100011010100101011110010110000011011101110001110000111010110101001000001",
   "11110101101100011010100101011110010110000011011101110001110000111010110101001000001",
   "11110110001011001011101111100010111111111010000101010000001010110111111101010010100",
   "11110110001011001011101111100010111111111010000101010000001010110111111101010010100",
   "11110110101010000000100110101110111101001000101111000110011100111011110101001000110",
   "11110110101010000000100110101110111101001000101111000110011100111011110101001000110",
   "11110111001000111001001011111011011011010001001101000000000111101001101011101000100",
   "11110111001000111001001011111011011011010001001101000000000111101001101011101000100",
   "11110111100111110101100000000001111100100100111101000100110001001011011110000001001",
   "11111000000110110101100011111100011000001111001100111111111011001100000111000001000",
   "11111000000110110101100011111100011000001111001100111111111011001100000111000001000",
   "11111000100101111001011000100100111010011111000011001111000001110100110100111001100",
   "11111000100101111001011000100100111010011111000011001111000001110100110100111001100",
   "11111001000101000000111110110110000100110001101110011010000000010000101011100110101",
   "11111001000101000000111110110110000100110001101110011010000000010000101011100110101",
   "11111001100100001100010111101010101101111100111010111010111011001011111101100011111",
   "11111001100100001100010111101010101101111100111010111010111011001011111101100011111",
   "11111010000011011011100011111110000010011001001110111001010101011011011000000010101",
   "11111010000011011011100011111110000010011001001110111001010101011011011000000010101",
   "11111010100010101110100100101011100100001100101100011101110110110001000010110110110",
   "11111010100010101110100100101011100100001100101100011101110110110001000010110110110",
   "11111011000010000101011010101111001011010101011010100010110001000011001011010110101",
   "11111011000010000101011010101111001011010101011010100010110001000011001011010110101",
   "11111011100001100000000111000101000101110100010100000110010011101010000011111111010",
   "11111011100001100000000111000101000101110100010100000110010011101010000011111111010",
   "11111100000000111110101010101001110111110111111110000011100001100000110000100100110",
   "11111100000000111110101010101001110111110111111110000011100001100000110000100100110",
   "11111100100000100001000110011010011100000111100011110110100001111101101000010000101",
   "11111100100000100001000110011010011100000111100011110110100001111101101000010000101",
   "11111101000000000111011011010100000011101101111010110001001001000101101101011101101",
   "11111101000000000111011011010100000011101101111010110001001001000101101101011101101",
   "11111101011111110001101010010100010110100100101100000100111000001111111110010100111",
   "11111101011111110001101010010100010110100100101100000100111000001111111110010100111",
   "11111101111111011111110100011001010011011111100110000111011100000011110010001001100",
   "11111101111111011111110100011001010011011111100110000111011100000011110010001001100",
   "11111110011111010001111010100001010000010111110100010110110001011000011110100101110",
   "11111110011111010001111010100001010000010111110100010110110001011000011110100101110",
   "11111110111111000111111101101010111010010111011110100001111111011010111110111100101",
   "11111110111111000111111101101010111010010111011110100001111111011010111110111100101",
   "11111111011111000001111110110101010110000101001110111100011001100001110101100001110",
      others => (others => '0'));
      	begin 
      return tmp;
      end init_rom;
	signal rom : memory_t := init_rom;
   signal TableOut :  std_logic_vector(82 downto 0);
   signal Y0 :  std_logic_vector(82 downto 0);
begin
	process(clk)
   begin
   if(rising_edge(clk)) then
   	Y0 <= rom(  TO_INTEGER(unsigned(X))  );
   end if;
   end process;
    Y <= Y0;
end architecture;

--------------------------------------------------------------------------------
--                              LogTable_1_7_76
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity LogTable_1_7_76 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(6 downto 0);
          Y : out  std_logic_vector(75 downto 0)   );
end entity;

architecture arch of LogTable_1_7_76 is
begin
  with X select  Y <= 
   "0000000100000000000000001111111111111110101010101010110010101010101001110111" when "0000000",
   "0000001100000000000000010000000000000001010101010101011101010101010110001001" when "0000001",
   "0000010100000000000010010000000000100100000000001010001000000011000010011011" when "0000010",
   "0000011100000000000110010000000010100110101011111000110011010001101110111111" when "0000011",
   "0000100100000000001100010000000111001001011010000001100000100111011101010101" when "0000100",
   "0000101100000000010100010000001111001100001100110100010011100010010010000001" when "0000101",
   "0000110100000000011110010000011011101111000111010001010010001000010111100001" when "0000110",
   "0000111100000000101010010000101101110010001101001000100101111000000001111110" when "0000111",
   "0001000100000000111000010001000110010101100010111010011100010111110011110101" when "0001000",
   "0001001100000001001000010001100110011001001101110111001000000110100011100100" when "0001001",
   "0001010100000001011010010010001110111101010011111111000001001011100010001010" when "0001010",
   "0001011100000001101110010011000001000001111100000010100110000110100010101011" when "0001011",
   "0001100100000010000100010011111101100111001101100010011100100000000010101011" when "0001100",
   "0001101100000010011100010101000101101101010000101111010001111001010011101001" when "0001101",
   "0001110100000010110110010110011010010100001110101001111100011100100101010001" when "0001110",
   "0001111100000011010010010111111100011100010001000011011011101101010000110001" when "0001111",
   "0010000100000011110000011001101101000101100010011100111001011000000101000110" when "0010000",
   "0010001100000100010000011011101101010000001110000111101010000011010100000101" when "0010001",
   "0010010100000100110010011101111101111100100000000101001101111111000000100011" when "0010010",
   "0010011100000101010110100000100000001010100101000111010001110101001101010110" when "0010011",
   "0010100100000101111100100011010100111010101010101111101111011010001101010101" when "0010100",
   "0010101100000110100100100110011101001100111111010000101110011100110100010110" when "0010101",
   "0010110100000111001110101001111010000001110001101100100101010110101001000001" when "0010110",
   "0010111100000111111010101101101100011001010001110101111001111100010111101001" when "0010111",
   "0011000100001000101000110001110101010011110000001111100010001110000101111011" when "0011000",
   "0011001100001001011000110110010101110001011110001100100101000111100111101010" when "0011001",
   "0011010100001010001010111011001110110010101101110000011011010000110100011100" when "0011010",
   "0011011100001010111111000000100001010111110001101110101111101101111110001100" when "0011011",
   "0011100100001011110101000110001110100000111101101011100000110000001000110100" when "0011100",
   "0011101100001100101101001100010111001110100101111011000000100101100010100100" when "0011101",
   "0011110100001101100111010010111100100000111111100001110110001001111101100010" when "0011110",
   "0011111100001110100011011001111111011000100000010100111101110111001010000101" when "0011111",
   "0100000100001111100001100001100000110101011110111001101010010101010010000111" when "0100000",
   "0100001100010000100001101001100001111000010010100101100101001011010101010110" when "0100001",
   "0100010100010001100011110010000011100001010011011110101111101111100110101000" when "0100010",
   "0100011100010010100111111011000110110000111010011011100011111000001010000000" when "0100011",
   "0100100100010011101110000100101100100111100001000010110100101011010011111001" when "0100100",
   "0100101100010100110110001110110110000101100001101011101111010000001001001100" when "0100101",
   "0100110100010110000000011001100100001011010111011101111011011111000000001110" when "0100110",
   "0100111100010111001100100100110111111001011110010001011100110010000010110100" when "0100111",
   "0101000100011000011010110000110010010000010010101110110010110101110001000110" when "0101000",
   "0101001100011001101010111101010100010000010010001110111010011001100101100000" when "0101001",
   "0101010100011010111101001010011110111001111010111011001110000000011001011101" when "0101010",
   "0101011100011100010001011000010011001101101011101101100110110001001011010011" when "0101011",
   "0101100100011101100111100110110010001100000100010000011101000111100100111010" when "0101100",
   "0101101100011110111111110101111100110101100100111110101001100100100011011010" when "0101101",
   "0101110100100000011010000101110100001010101111000011100101011110111111110110" when "0101110",
   "0101111100100001110110010110011001001100000100011011001011110100011000101100" when "0101111",
   "0110000100100011010100100111101100111010000111110001111001111001011100011010" when "0110000",
   "0110001100100100110100111001110000010101011100100100110000001010110101000000" when "0110001",
   "0110010100100110010111001100100100011110100111000001010010111101110100011010" when "0110010",
   "0110011100100111111011100000001010010110001100000101101011010001000001111001" when "0110011",
   "0110100100101001100001110100100010111100110001100000100111011101001000011000" when "0110100",
   "0110101100101011001010001001101111010010111101110001011100000101100101110110" when "0110101",
   "0110110100101100110100011111110000011001011000001000000100101001011011100000" when "0110110",
   "0110111100101110100000110110100111010000101000100101000100010011111111000001" when "0110111",
   "0111000100110000001111001110010100111001010111111001100110101101101100110000" when "0111000",
   "0111001100110001111111100110111010010100001111100111100000101100111010110110" when "0111001",
   "0111010100110011110010000000011000100001111010000001010001000110101101010000" when "0111010",
   "0111011100110101100110011010110000100011000010001010000001011111101010111000" when "0111011",
   "0111100100110111011100110110000011011000010011110101100110111100110011100100" when "0111100",
   "0111101100111001010101010010010010000010011011101000100010110100010110111110" when "0111101",
   "0111110100111011001111101111011101100010000110111000000011011110101100100010" when "0111110",
   "0111111100111101001100001101100110111000000011101010000101000111001100011010" when "0111111",
   "1000000000111110001011001101000011000011100111001111001101110011110010100111" when "1000000",
   "1000001001000000001010101100101011000100010110011010101111110010110011000111" when "1000001",
   "1000010001000010001100001101010011011101001101101100110110100100100010111100" when "1000010",
   "1000011001000100001111101110111101001110111101110110000100101000001010001101" when "1000011",
   "1000100001000110010101010001101001011010011000010111101101101011111101100001" when "1000100",
   "1000101001001000011100110101011001000000001111100011110111011110011100000011" when "1000101",
   "1000110001001010100110011010001101000001010110011101011010011111001110101100" when "1000110",
   "1000111001001100110010000000000110011110100000111000000010110000001000000111" when "1000111",
   "1001000001001110111111100111000110011000100011011000010000100110000101110110" when "1001000",
   "1001001001010001001111001111001101110000010011010011011001011010010010001010" when "1001001",
   "1001010001010011100000111000011101100110100110101111101000011011000111001001" when "1001010",
   "1001011001010101110100100010110110111100010100100011111111011101010010100010" when "1001011",
   "1001100001011000001010001110011010110010010100011000010111101100111010101011" when "1001100",
   "1001101001011010100001111011001010001001011110100101100010011110100100010001" when "1001101",
   "1001110001011100111011101001000110000010101100010101001010000000011001010001" when "1001110",
   "1001111001011111010111011000001111011110110111100001110010001011010000100110" when "1001111",
   "1010000001100001110101001000100111011110111010110110111001010011110110111001" when "1010000",
   "1010001001100100010100111010001111000011110001110000111000111011111000001110" when "1010001",
   "1010010001100110110110101101000111001110011000011101000110100011001010101110" when "1010010",
   "1010011001101001011010100001010000111111101011111001110100011000111010001111" when "1010011",
   "1010100001101100000000010110101101011000101001110110010010001100110100111011" when "1010100",
   "1010101001101110101000001101011101011010010000110010101110000000011000110100" when "1010101",
   "1010110001110001010010000101100010000101100000000000010100111000000010010101" when "1010110",
   "1010111001110011111101111110111100011011010111100001010011101100011011101111" when "1010111",
   "1011000001110110101011111001101101011100111000001000110111111011101101101011" when "1011000",
   "1011001001111001011011110101110110001011000011011011010000011010110000100100" when "1011001",
   "1011010001111100001101110011010111100110111011101101101110000110011110111110" when "1011010",
   "1011011001111111000001110010010010110001100100000110100100110101001000111110" when "1011011",
   "1011100010000001110111110010101000101100000000011101001100000111101000100001" when "1011100",
   "1011101010000100101111110100011010010111010101011001111111111010110110101110" when "1011101",
   "1011110010000111101001110111101000110100101000010110100001011001000010000111" when "1011110",
   "1011111010001010100101111100010101000100111111011101010111101011000101110111" when "1011111",
   "1100000010001101100100000010100000001001100001101010010000101010000010000010" when "1100000",
   "1100001010010000100100001010001011000011010110101010000001110000010100101110" when "1100001",
   "1100010010010011100110010011010110110011100110111010101000101011010100001110" when "1100010",
   "1100011010010110101010011110000100011011011011101011001100001100101010001001" when "1100011",
   "1100100010011001110000101010010100111011111110111011111100111011101111011101" when "1100100",
   "1100101010011100111000111000001001010110011011011110010110000111001001101010" when "1100101",
   "1100110010100000000011000111100010101011111100110100111110010110001000101100" when "1100110",
   "1100111010100011001111011000100001111101101111010011101000011010000110000001" when "1100111",
   "1101000010100110011101101011001000001100111111111111010100000000000100100100" when "1101000",
   "1101001010101001101101111111010110011010111100101110001110100010010001101110" when "1101001",
   "1101010010101101000000010101001101101000110100000111110011111001100111001011" when "1101010",
   "1101011010110000010100101100101110110111110101100100101111001111001101111001" when "1101011",
   "1101100010110011101011000101111011001001010001001110111011101110000001111111" when "1101100",
   "1101101010110111000011100000110011011110011000000001100101010100010111100000" when "1101101",
   "1101110010111010011101111101011000111000011011101001001001100101100000010001" when "1101110",
   "1101111010111101111010011011101100011000101110100011011000011011010010101110" when "1101111",
   "1110000011000001011000111011101111000000100011111111010100110111110001101010" when "1110000",
   "1110001011000100111001011101100001110001001111111101010101110110110100111100" when "1110001",
   "1110010011001000011100000001000101101100000111001111000110111111110011010011" when "1110010",
   "1110011011001100000000100110011011110010011111010111101001010111001100111101" when "1110011",
   "1110100011001111100111001101100101000101101110101011010100010000010111010111" when "1110100",
   "1110101011010011001111110110100010100111001100001111110101111111001001110100" when "1110101",
   "1110110011010110111010100001010101011000001111111100010100101001101011000110" when "1110110",
   "1110111011011010100111001101111110011010010010011001001110111010000000000110" when "1110111",
   "1111000011011110010101111100011110101110101101000000011100101111111011010111" when "1111000",
   "1111001011100010000110101100110111010110111001111101010000010010101101101111" when "1111001",
   "1111010011100101111001011111001001010100010100001100010110100010110111110100" when "1111010",
   "1111011011101001101110010011010101101000010111011011111000001011111100100011" when "1111011",
   "1111100011101101100101001001011101010100100000001011011010010110010100101110" when "1111100",
   "1111101011110001011110000001100001011010001011101011111111011001000011011000" when "1111101",
   "1111110011110101011000111011100010111010111000000000000111101011101011011001" when "1111110",
   "1111111011111001010101110111100010111000000011111011110010011000000101110111" when "1111111",
   "----------------------------------------------------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_83_f400_uid85
--                    (IntAdderAlternative_83_f400_uid89)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_83_f400_uid85 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(82 downto 0);
          Y : in  std_logic_vector(82 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(82 downto 0)   );
end entity;

architecture arch of IntAdder_83_f400_uid85 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(41 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(40 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(41 downto 0);
signal sum_l1_idx1 :  std_logic_vector(40 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(82 downto 42)) + ( "0" & Y(82 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(40 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(41 downto 41);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(40 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(41 downto 41);
   R <= sum_l1_idx1(40 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                              LogTable_2_8_70
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity LogTable_2_8_70 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(7 downto 0);
          Y : out  std_logic_vector(69 downto 0)   );
end entity;

architecture arch of LogTable_2_8_70 is
begin
  with X select  Y <= 
   "0000000000000000000000000000000000000000111111111111111111111111110101" when "00000000",
   "0000000100000000000000000000001111000001000000000001001101100101001011" when "00000001",
   "0000001000000000000000000000111110000001000000001010001011001010100010" when "00000010",
   "0000001100000000000000000010001101000001000000100010111000101111111111" when "00000011",
   "0000010000000000000000000011111100000001000001010011010110010101101010" when "00000100",
   "0000010100000000000000000110001011000001000010100011100011111011101100" when "00000101",
   "0000011000000000000000001000111010000001000100011011100001100010010100" when "00000110",
   "0000011100000000000000001100001001000001000111000011001111001001110001" when "00000111",
   "0000100000000000000000001111111000000001001010100010101100110010011000" when "00001000",
   "0000100100000000000000010100000111000001001111000001111010011100011110" when "00001001",
   "0000101000000000000000011000110110000001010100101000111000001000011101" when "00001010",
   "0000101100000000000000011110000101000001011011011111100101110110110001" when "00001011",
   "0000110000000000000000100011110100000001100011101110000011100111111010" when "00001100",
   "0000110100000000000000101010000011000001101101011100010001011100011011" when "00001101",
   "0000111000000000000000110000110010000001111000110010001111010100110111" when "00001110",
   "0000111100000000000000111000000001000010000101110111111101010001111001" when "00001111",
   "0001000000000000000000111111110000000010010100110101011011010100001011" when "00010000",
   "0001000100000000000001000111111111000010100101110010101001011100011100" when "00010001",
   "0001001000000000000001010000101110000010111000110111100111101011011100" when "00010010",
   "0001001100000000000001011001111101000011001110001100010110000010000010" when "00010011",
   "0001010000000000000001100011101100000011100101111000110100100001000100" when "00010100",
   "0001010100000000000001101101111011000100000000000101000011001001011011" when "00010101",
   "0001011000000000000001111000101010000100011100111001000001111100000111" when "00010110",
   "0001011100000000000010000011111001000100111100011100110000111010000111" when "00010111",
   "0001100000000000000010001111101000000101011110111000010000000100011110" when "00011000",
   "0001100100000000000010011011110111000110000100010011011111011100010100" when "00011001",
   "0001101000000000000010101000100110000110101100110110011111000010110000" when "00011010",
   "0001101100000000000010110101110101000111011000101001001110111001000001" when "00011011",
   "0001110000000000000011000011100100001000000111110011101111000000010101" when "00011100",
   "0001110100000000000011010001110011001000111010011101111111011001111111" when "00011101",
   "0001111000000000000011100000100010001001110000110000000000000111010011" when "00011110",
   "0001111100000000000011101111110001001010101010110001110001001001101011" when "00011111",
   "0010000000000000000011111111100000001011101000101011010010100010100010" when "00100000",
   "0010000100000000000100001111101111001100101010100100100100010011010110" when "00100001",
   "0010001000000000000100100000011110001101110000100101100110011101101010" when "00100010",
   "0010001100000000000100110001101101001110111010110110011001000011000000" when "00100011",
   "0010010000000000000101000011011100010000001001011110111100000101000000" when "00100100",
   "0010010100000000000101010101101011010001011100100111001111100101010110" when "00100101",
   "0010011000000000000101101000011010010010110100010111010011100101101110" when "00100110",
   "0010011100000000000101111011101001010100010000110111001000000111111000" when "00100111",
   "0010100000000000000110001111011000010101110010001110101101001101101001" when "00101000",
   "0010100100000000000110100011100111010111011000100110000010111000110110" when "00101001",
   "0010101000000000000110111000010110011001000100000101001001001011011001" when "00101010",
   "0010101100000000000111001101100101011010110100110100000000000111001111" when "00101011",
   "0010110000000000000111100011010100011100101010111010100111101110010110" when "00101100",
   "0010110100000000000111111001100011011110100110100001000000000010110010" when "00101101",
   "0010111000000000001000010000010010100000100111101111001001000110100111" when "00101110",
   "0010111100000000001000100111100001100010101110101101000010111011111110" when "00101111",
   "0011000000000000001000111111010000100100111011100010101101100101000011" when "00110000",
   "0011000100000000001001010111011111100111001110011000001001000100000100" when "00110001",
   "0011001000000000001001110000001110101001100111010101010101011011010010" when "00110010",
   "0011001100000000001010001001011101101100000110100010010010101101000001" when "00110011",
   "0011010000000000001010100011001100101110101100000111000000111011101010" when "00110100",
   "0011010100000000001010111101011011110001011000001011100000001001100110" when "00110101",
   "0011011000000000001011011000001010110100001010110111110000011001010011" when "00110110",
   "0011011100000000001011110011011001110111000100010011110001101101010001" when "00110111",
   "0011100000000000001100001111001000111010000100100111100100001000000100" when "00111000",
   "0011100100000000001100101011010111111101001011111011000111101100010011" when "00111001",
   "0011101000000000001101001000000111000000011010010110011100011100100110" when "00111010",
   "0011101100000000001101100101010110000011110000000001100010011011101010" when "00111011",
   "0011110000000000001110000011000101000111001101000100011001101100001110" when "00111100",
   "0011110100000000001110100001010100001010110001100111000010010001000101" when "00111101",
   "0011111000000000001111000000000011001110011101110001011100001101000100" when "00111110",
   "0011111100000000001111011111010010010010010001101011100111100011000100" when "00111111",
   "0100000000000000001111111111000001010110001101011101100100010110000001" when "01000000",
   "0100000100000000010000011111010000011010010001001111010010101000111000" when "01000001",
   "0100001000000000010000111111111111011110011101001000110010011110101010" when "01000010",
   "0100001100000000010001100001001110100010110001010010000011111010011101" when "01000011",
   "0100010000000000010010000010111101100111001101110011000110111111011000" when "01000100",
   "0100010100000000010010100101001100101011110010110011111011110000100100" when "01000101",
   "0100011000000000010011000111111011110000100000011100100010010001010001" when "01000110",
   "0100011100000000010011101011001010110101010110110100111010100100101110" when "01000111",
   "0100100000000000010100001110111001111010010110000101000100101110001101" when "01001000",
   "0100100100000000010100110011001000111111011110010101000000110001000111" when "01001001",
   "0100101000000000010101010111111000000100101111101100101110110000110100" when "01001010",
   "0100101100000000010101111101000111001010001010010100001110110000110000" when "01001011",
   "0100110000000000010110100010110110001111101110010011100000110100011100" when "01001100",
   "0100110100000000010111001001000101010101011011110010100100111111011010" when "01001101",
   "0100111000000000010111101111110100011011010010111001011011010101001110" when "01001110",
   "0100111100000000011000010111000011100001010011110000000011111001100010" when "01001111",
   "0101000000000000011000111110110010100111011110011110011110110000000000" when "01010000",
   "0101000100000000011001100111000001101101110011001100101011111100011000" when "01010001",
   "0101001000000000011010001111110000110100010010000010101011100010011011" when "01010010",
   "0101001100000000011010111000111111111010111011001000011101100101111100" when "01010011",
   "0101010000000000011011100010101111000001101110100110000010001010110100" when "01010100",
   "0101010100000000011100001100111110001000101100100011011001010100111110" when "01010101",
   "0101011000000000011100110111101101001111110101001000100011001000010101" when "01010110",
   "0101011100000000011101100010111100010111001000011101011111101000111011" when "01010111",
   "0101100000000000011110001110101011011110100110101010001110111010110100" when "01011000",
   "0101100100000000011110111010111010100110001111110110110001000010000100" when "01011001",
   "0101101000000000011111100111101001101110000100001011000110000010111000" when "01011010",
   "0101101100000000100000010100111000110110000011101111001110000001011010" when "01011011",
   "0101110000000000100001000010100111111110001110101011001001000001111001" when "01011100",
   "0101110100000000100001110000110111000110100101000110110111001000101001" when "01011101",
   "0101111000000000100010011111100110001111000111001010011000011001111110" when "01011110",
   "0101111100000000100011001110110101010111110100111101101100111010010010" when "01011111",
   "0110000000000000100011111110100100100000101110101000110100101110000000" when "01100000",
   "0110000100000000100100101110110011101001110100010011101111111001100110" when "01100001",
   "0110001000000000100101011111100010110011000110000110011110100001100110" when "01100010",
   "0110001100000000100110010000110001111100100100001001000000101010100010" when "01100011",
   "0110010000000000100111000010100001000110001110100011010110011001000101" when "01100100",
   "0110010100000000100111110100110000010000000101011101011111110001111000" when "01100101",
   "0110011000000000101000100111011111011010001000111111011100111001101000" when "01100110",
   "0110011100000000101001011010101110100100011001010001001101110101000100" when "01100111",
   "0110100000000000101010001110011101101110110110011010110010101001000011" when "01101000",
   "0110100100000000101011000010101100111001100000100100001011011010011010" when "01101001",
   "0110101000000000101011110111011100000100010111110101011000001110000000" when "01101010",
   "0110101100000000101100101100101011001111011100010110011001001000110101" when "01101011",
   "0110110000000000101101100010011010011010101110001111001110001111110110" when "01101100",
   "0110110100000000101110011000101001100110001101100111110111101000001000" when "01101101",
   "0110111000000000101111001111011000110001111010101000010101010110101101" when "01101110",
   "0110111100000000110000000110100111111101110101011000100111100000110000" when "01101111",
   "0111000000000000110000111110010111001001111110000000101110001011011011" when "01110000",
   "0111000100000000110001110110100110010110010100101000101001011011111110" when "01110001",
   "0111001000000000110010101111010101100010111001011000011001010111101000" when "01110010",
   "0111001100000000110011101000100100101111101100010111111110000011110000" when "01110011",
   "0111010000000000110100100010010011111100101101101111010111100101101101" when "01110100",
   "0111010100000000110101011100100011001001111101100110100110000010111000" when "01110101",
   "0111011000000000110110010111010010010111011100000101101001100000110000" when "01110110",
   "0111011100000000110111010010100001100101001001010100100010000100110100" when "01110111",
   "0111100000000000111000001110010000110011000101011011001111110100101001" when "01111000",
   "0111100100000000111001001010100000000001010000100001110010110101110100" when "01111001",
   "0111101000000000111010000111001111001111101010110000001011001110000000" when "01111010",
   "0111101100000000111011000100011110011110010100001110011001000010111000" when "01111011",
   "0111110000000000111100000010001101101101001101000100011100011010001011" when "01111100",
   "0111110100000000111101000000011100111100010101011010010101011001101110" when "01111101",
   "0111111000000000111101111111001100001011101101011000000100000111010100" when "01111110",
   "0111111100000000111110111110011011011011010101000101101000101000110110" when "01111111",
   "1000000000000000111111111110001010101011001100101011000011000100010000" when "10000000",
   "1000000100000001000000111110011001111011010100010000010011011111100001" when "10000001",
   "1000001000000001000001111111001001001011101011111101011010000000101001" when "10000010",
   "1000001100000001000011000000011000011100010011111010010110101101101110" when "10000011",
   "1000010000000001000100000010000111101101001100001111001001101100110110" when "10000100",
   "1000010100000001000101000100010110111110010101000011110011000100001100" when "10000101",
   "1000011000000001000110000111000110001111101110100000010010111001111110" when "10000110",
   "1000011100000001000111001010010101100001011000101100101001010100011011" when "10000111",
   "1000100000000001001000001110000100110011010011110000110110011001111000" when "10001000",
   "1000100100000001001001010010010100000101011111110100111010010000101011" when "10001001",
   "1000101000000001001010010111000011010111111101000000110100111111001110" when "10001010",
   "1000101100000001001011011100010010101010101011011100100110101011111100" when "10001011",
   "1000110000000001001100100010000001111101101011010000001111011101010101" when "10001100",
   "1000110100000001001101101000010001010000111100100011101111011001111100" when "10001101",
   "1000111000000001001110101111000000100100011111011111000110101000010111" when "10001110",
   "1000111100000001001111110110001111111000010100001010010101001111001101" when "10001111",
   "1001000000000001010000111101111111001100011010101101011011010101001010" when "10010000",
   "1001000100000001010010000110001110100000110011010000011001000000111101" when "10010001",
   "1001001000000001010011001110111101110101011101111011001110011001010111" when "10010010",
   "1001001100000001010100011000001101001010011010110101111011100101001100" when "10010011",
   "1001010000000001010101100001111100011111101010001000100000101011010100" when "10010100",
   "1001010100000001010110101100001011110101001011111010111101110010101010" when "10010101",
   "1001011000000001010111110110111011001011000000010101010011000010001011" when "10010110",
   "1001011100000001011001000010001010100001000111011111100000100000110111" when "10010111",
   "1001100000000001011010001101111001110111100001100001100110010101110010" when "10011000",
   "1001100100000001011011011010001001001110001110100011100100101000000010" when "10011001",
   "1001101000000001011100100110111000100101001110101101011011011110110010" when "10011010",
   "1001101100000001011101110100000111111100100010000111001011000001001100" when "10011011",
   "1001110000000001011111000001110111010100001000111000110011010110100001" when "10011100",
   "1001110100000001100000010000000110101100000011001010010100100110000100" when "10011101",
   "1001111000000001100001011110110110000100010001000011101110110111001001" when "10011110",
   "1001111100000001100010101110000101011100110010101101000010010001001010" when "10011111",
   "1010000000000001100011111101110100110101101000001110001110111011100001" when "10100000",
   "1010000100000001100101001110000100001110110001101111010100111101101101" when "10100001",
   "1010001000000001100110011110110011101000001111011000010100011111010000" when "10100010",
   "1010001100000001100111110000000011000010000001010001001101100111101110" when "10100011",
   "1010010000000001101001000001110010011100000111100010000000011110101111" when "10100100",
   "1010010100000001101010010100000001110110100010010010101101001011111100" when "10100101",
   "1010011000000001101011100110110001010001010001101011010011110111000101" when "10100110",
   "1010011100000001101100111010000000101100010101110011110100100111111000" when "10100111",
   "1010100000000001101110001101110000000111101110110100001111100110001010" when "10101000",
   "1010100100000001101111100001111111100011011100110100100100111001110001" when "10101001",
   "1010101000000001110000110110101110111111011111111100110100101010100110" when "10101010",
   "1010101100000001110010001011111110011011111000010100111111000000100111" when "10101011",
   "1010110000000001110011100001101101111000100110000101000100000011110001" when "10101100",
   "1010110100000001110100110111111101010101101001010101000011111100001001" when "10101101",
   "1010111000000001110110001110101100110011000010001100111110110001110100" when "10101110",
   "1010111100000001110111100101111100010000110000110100110100101100111001" when "10101111",
   "1011000000000001111000111101101011101110110101010100100101110101100101" when "10110000",
   "1011000100000001111010010101111011001101001111110100010010010100000101" when "10110001",
   "1011001000000001111011101110101010101100000000011011111010010000101100" when "10110010",
   "1011001100000001111101000111111010001011000111010011011101110011101111" when "10110011",
   "1011010000000001111110100001101001101010100100100010111101000101100011" when "10110100",
   "1011010100000001111111111011111001001010011000010010011000001110100100" when "10110101",
   "1011011000000010000001010110101000101010100010101001101111010111010000" when "10110110",
   "1011011100000010000010110001111000001011000011110001000010101000000111" when "10110111",
   "1011100000000010000100001101100111101011111011110000010010001001101100" when "10111000",
   "1011100100000010000101101001110111001101001010101111011110000100100111" when "10111001",
   "1011101000000010000111000110100110101110110000110110100110100001011111" when "10111010",
   "1011101100000010001000100011110110010000101110001101101011101001000011" when "10111011",
   "1011110000000010001010000001100101110011000010111100101101100100000001" when "10111100",
   "1011110100000010001011011111110101010101101111001011101100011011001100" when "10111101",
   "1011111000000010001100111110100100111000110011000010101000010111011001" when "10111110",
   "1011111100000010001110011101110100011100001110101001100001100001100010" when "10111111",
   "1100000000000010001111111101100100000000000010001000011000000010100001" when "11000000",
   "1100000100000010010001011101110011100100001101100111001100000011010101" when "11000001",
   "1100001000000010010010111110100011001000110001001101111101101100111111" when "11000010",
   "1100001100000010010100011111110010101101101101000100101101001000100101" when "11000011",
   "1100010000000010010110000001100010010011000001010011011010011111001101" when "11000100",
   "1100010100000010010111100011110001111000101110000010000101111010000010" when "11000101",
   "1100011000000010011001000110100001011110110011011000101111100010010001" when "11000110",
   "1100011100000010011010101001110001000101010001011111010111100001001100" when "11000111",
   "1100100000000010011100001101100000101100001000011101111110000000000101" when "11001000",
   "1100100100000010011101110001110000010011011000011100100011001000010100" when "11001001",
   "1100101000000010011111010110011111111011000001100011000111000011010001" when "11001010",
   "1100101100000010100000111011101111100011000011111001101001111010011001" when "11001011",
   "1100110000000010100010100001011111001011011111101000001011110111001100" when "11001100",
   "1100110100000010100100000111101110110100010100110110101101000011001100" when "11001101",
   "1100111000000010100101101110011110011101100011101101001101100111111111" when "11001110",
   "1100111100000010100111010101101110000111001100010011101101101111001110" when "11001111",
   "1101000000000010101000111101011101110001001110110010001101100010100011" when "11010000",
   "1101000100000010101010100101101101011011101011010000101101001011101101" when "11010001",
   "1101001000000010101100001110011101000110100001110111001100110100011110" when "11010010",
   "1101001100000010101101110111101100110001110010101101101100100110101011" when "11010011",
   "1101010000000010101111100001011100011101011101111100001100101100001010" when "11010100",
   "1101010100000010110001001011101100001001100011101010101101001110110111" when "11010101",
   "1101011000000010110010110110011011110110000100000001001110011000101111" when "11010110",
   "1101011100000010110100100001101011100010111111000111110000010011110011" when "11010111",
   "1101100000000010110110001101011011010000010101000110010011001010000101" when "11011000",
   "1101100100000010110111111001101010111110000110000100110111000101101110" when "11011001",
   "1101101000000010111001100110011010101100010010001011011100010000110101" when "11011010",
   "1101101100000010111011010011101010011010111001100010000010110101101000" when "11011011",
   "1101110000000010111101000001011010001001111100010000101010111110010110" when "11011100",
   "1101110100000010111110101111101001111001011010011111010100110101010010" when "11011101",
   "1101111000000011000000011110011001101001010100010110000000100100110010" when "11011110",
   "1101111100000011000010001101101001011001101001111100101110010111001101" when "11011111",
   "1110000000000011000011111101011001001010011011011011011110010111000000" when "11100000",
   "1110000100000011000101101101101000111011101000111010010000101110101000" when "11100001",
   "1110001000000011000111011110011000101101010010100001000101101000101000" when "11100010",
   "1110001100000011001001001111101000011111011000010111111101001111100101" when "11100011",
   "1110010000000011001011000001011000010001111010100110110111101110000100" when "11100100",
   "1110010100000011001100110011101000000100111001010101110101001110110010" when "11100101",
   "1110011000000011001110100110010111111000010100101100110101111100011100" when "11100110",
   "1110011100000011010000011001100111101100001100110011111010000001110010" when "11100111",
   "1110100000000011010010001101010111100000100001110011000001101001101000" when "11101000",
   "1110100100000011010100000001100111010101010011110010001100111110110100" when "11101001",
   "1110101000000011010101110110010111001010100010111001011100001100010001" when "11101010",
   "1110101100000011010111101011100111000000001111010000101111011100111001" when "11101011",
   "1110110000000011011001100001010110110110011001000000000110111011101110" when "11101100",
   "1110110100000011011011010111100110101101000000001111100010110011110010" when "11101101",
   "1110111000000011011101001110010110100100000101000111000011010000001010" when "11101110",
   "1110111100000011011111000101100110011011100111101110101000011011111110" when "11101111",
   "1111000000000011100000111101010110010011101000001110010010100010011011" when "11110000",
   "1111000100000011100010110101100110001100000110101110000001101110101111" when "11110001",
   "1111001000000011100100101110010110000101000011010101110110001100001011" when "11110010",
   "1111001100000011100110100111100101111110011110001101110000000110000101" when "11110011",
   "1111010000000011101000100001010101111000010111011101101111100111110011" when "11110100",
   "1111010100000011101010011011100101110010101111001101110100111100110000" when "11110101",
   "1111011000000011101100010110010101101101100101100110000000010000011010" when "11110110",
   "1111011100000011101110010001100101101000111010101110010001101110010010" when "11110111",
   "1111100000000011110000001101010101100100101110101110101001100001111010" when "11111000",
   "1111100100000011110010001001100101100001000001101111000111110110111010" when "11111001",
   "1111101000000011110100000110010101011101110011110111101100111000111011" when "11111010",
   "1111101100000011110110000011100101011011000101010000011000110011101010" when "11111011",
   "1111110000000011111000000001010101011000110110000001001011110010110110" when "11111100",
   "1111110100000011111001111111100101010111000110010010000110000010010010" when "11111101",
   "1111111000000011111011111110010101010101110110001011000111101101110100" when "11111110",
   "1111111100000011111101111101100101010101000101110100010001000001010100" when "11111111",
   "----------------------------------------------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_83_f400_uid94
--                    (IntAdderAlternative_83_f400_uid98)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_83_f400_uid94 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(82 downto 0);
          Y : in  std_logic_vector(82 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(82 downto 0)   );
end entity;

architecture arch of IntAdder_83_f400_uid94 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(41 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(40 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(41 downto 0);
signal sum_l1_idx1 :  std_logic_vector(40 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(82 downto 42)) + ( "0" & Y(82 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(40 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(41 downto 41);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(40 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(41 downto 41);
   R <= sum_l1_idx1(40 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                              LogTable_3_8_63
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity LogTable_3_8_63 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(7 downto 0);
          Y : out  std_logic_vector(62 downto 0)   );
end entity;

architecture arch of LogTable_3_8_63 is
begin
  with X select  Y <= 
   "000000000000000000000000000000000000000000000000000000000000100" when "00000000",
   "000000010000000000000000000000000000011111111111000000000000100" when "00000001",
   "000000100000000000000000000000000001111111111110000000000000101" when "00000010",
   "000000110000000000000000000000000100011111111101000000000001000" when "00000011",
   "000001000000000000000000000000000111111111111100000000000001111" when "00000100",
   "000001010000000000000000000000001100011111111011000000000011001" when "00000101",
   "000001100000000000000000000000010001111111111010000000000101000" when "00000110",
   "000001110000000000000000000000011000011111111001000000000111101" when "00000111",
   "000010000000000000000000000000011111111111111000000000001011001" when "00001000",
   "000010010000000000000000000000101000011111110111000000001111110" when "00001001",
   "000010100000000000000000000000110001111111110110000000010101011" when "00001010",
   "000010110000000000000000000000111100011111110101000000011100010" when "00001011",
   "000011000000000000000000000001000111111111110100000000100100100" when "00001100",
   "000011010000000000000000000001010100011111110011000000101110010" when "00001101",
   "000011100000000000000000000001100001111111110010000000111001101" when "00001110",
   "000011110000000000000000000001110000011111110001000001000110110" when "00001111",
   "000100000000000000000000000001111111111111110000000001010101111" when "00010000",
   "000100010000000000000000000010010000011111101111000001100110111" when "00010001",
   "000100100000000000000000000010100001111111101110000001111010000" when "00010010",
   "000100110000000000000000000010110100011111101101000010001111011" when "00010011",
   "000101000000000000000000000011000111111111101100000010100111001" when "00010100",
   "000101010000000000000000000011011100011111101011000011000001100" when "00010101",
   "000101100000000000000000000011110001111111101010000011011110011" when "00010110",
   "000101110000000000000000000100001000011111101001000011111110000" when "00010111",
   "000110000000000000000000000100011111111111101000000100100000100" when "00011000",
   "000110010000000000000000000100111000011111100111000101000110000" when "00011001",
   "000110100000000000000000000101010001111111100110000101101110101" when "00011010",
   "000110110000000000000000000101101100011111100101000110011010100" when "00011011",
   "000111000000000000000000000110000111111111100100000111001001111" when "00011100",
   "000111010000000000000000000110100100011111100011000111111100101" when "00011101",
   "000111100000000000000000000111000001111111100010001000110011000" when "00011110",
   "000111110000000000000000000111100000011111100001001001101101001" when "00011111",
   "001000000000000000000000000111111111111111100000001010101011001" when "00100000",
   "001000010000000000000000001000100000011111011111001011101101001" when "00100001",
   "001000100000000000000000001001000001111111011110001100110011010" when "00100010",
   "001000110000000000000000001001100100011111011101001101111101110" when "00100011",
   "001001000000000000000000001010000111111111011100001111001100100" when "00100100",
   "001001010000000000000000001010101100011111011011010000011111110" when "00100101",
   "001001100000000000000000001011010001111111011010010001110111101" when "00100110",
   "001001110000000000000000001011111000011111011001010011010100010" when "00100111",
   "001010000000000000000000001100011111111111011000010100110101110" when "00101000",
   "001010010000000000000000001101001000011111010111010110011100011" when "00101001",
   "001010100000000000000000001101110001111111010110011000001000000" when "00101010",
   "001010110000000000000000001110011100011111010101011001111000111" when "00101011",
   "001011000000000000000000001111000111111111010100011011101111001" when "00101100",
   "001011010000000000000000001111110100011111010011011101101010111" when "00101101",
   "001011100000000000000000010000100001111111010010011111101100010" when "00101110",
   "001011110000000000000000010001010000011111010001100001110011100" when "00101111",
   "001100000000000000000000010001111111111111010000100100000000100" when "00110000",
   "001100010000000000000000010010110000011111001111100110010011100" when "00110001",
   "001100100000000000000000010011100001111111001110101000101100101" when "00110010",
   "001100110000000000000000010100010100011111001101101011001100000" when "00110011",
   "001101000000000000000000010101000111111111001100101101110001110" when "00110100",
   "001101010000000000000000010101111100011111001011110000011110000" when "00110101",
   "001101100000000000000000010110110001111111001010110011010001000" when "00110110",
   "001101110000000000000000010111101000011111001001110110001010101" when "00110111",
   "001110000000000000000000011000011111111111001000111001001011001" when "00111000",
   "001110010000000000000000011001011000011111000111111100010010101" when "00111001",
   "001110100000000000000000011010010001111111000110111111100001010" when "00111010",
   "001110110000000000000000011011001100011111000110000010110111010" when "00111011",
   "001111000000000000000000011100000111111111000101000110010100100" when "00111100",
   "001111010000000000000000011101000100011111000100001001111001010" when "00111101",
   "001111100000000000000000011110000001111111000011001101100101101" when "00111110",
   "001111110000000000000000011111000000011111000010010001011001110" when "00111111",
   "010000000000000000000000011111111111111111000001010101010101110" when "01000000",
   "010000010000000000000000100001000000011111000000011001011001110" when "01000001",
   "010000100000000000000000100010000001111110111111011101100110000" when "01000010",
   "010000110000000000000000100011000100011110111110100001111010010" when "01000011",
   "010001000000000000000000100100000111111110111101100110010111001" when "01000100",
   "010001010000000000000000100101001100011110111100101010111100011" when "01000101",
   "010001100000000000000000100110010001111110111011101111101010010" when "01000110",
   "010001110000000000000000100111011000011110111010110100100000111" when "01000111",
   "010010000000000000000000101000011111111110111001111001100000100" when "01001000",
   "010010010000000000000000101001101000011110111000111110101001000" when "01001001",
   "010010100000000000000000101010110001111110111000000011111010100" when "01001010",
   "010010110000000000000000101011111100011110110111001001010101100" when "01001011",
   "010011000000000000000000101101000111111110110110001110111001110" when "01001100",
   "010011010000000000000000101110010100011110110101010100100111100" when "01001101",
   "010011100000000000000000101111100001111110110100011010011111000" when "01001110",
   "010011110000000000000000110000110000011110110011100000100000000" when "01001111",
   "010100000000000000000000110001111111111110110010100110101011000" when "01010000",
   "010100010000000000000000110011010000011110110001101101000000000" when "01010001",
   "010100100000000000000000110100100001111110110000110011011111010" when "01010010",
   "010100110000000000000000110101110100011110101111111010001000101" when "01010011",
   "010101000000000000000000110111000111111110101111000000111100011" when "01010100",
   "010101010000000000000000111000011100011110101110000111111010110" when "01010101",
   "010101100000000000000000111001110001111110101101001111000011100" when "01010110",
   "010101110000000000000000111011001000011110101100010110010111010" when "01010111",
   "010110000000000000000000111100011111111110101011011101110101110" when "01011000",
   "010110010000000000000000111101111000011110101010100101011111010" when "01011001",
   "010110100000000000000000111111010001111110101001101101010011111" when "01011010",
   "010110110000000000000001000000101100011110101000110101010011110" when "01011011",
   "010111000000000000000001000010000111111110100111111101011111000" when "01011100",
   "010111010000000000000001000011100100011110100111000101110101110" when "01011101",
   "010111100000000000000001000101000001111110100110001110011000010" when "01011110",
   "010111110000000000000001000110100000011110100101010111000110011" when "01011111",
   "011000000000000000000001000111111111111110100100100000000000011" when "01100000",
   "011000010000000000000001001001100000011110100011101001000110011" when "01100001",
   "011000100000000000000001001011000001111110100010110010011000100" when "01100010",
   "011000110000000000000001001100100100011110100001111011110111000" when "01100011",
   "011001000000000000000001001110000111111110100001000101100001110" when "01100100",
   "011001010000000000000001001111101100011110100000001111011001000" when "01100101",
   "011001100000000000000001010001010001111110011111011001011100111" when "01100110",
   "011001110000000000000001010010111000011110011110100011101101100" when "01100111",
   "011010000000000000000001010100011111111110011101101110001011000" when "01101000",
   "011010010000000000000001010110001000011110011100111000110101100" when "01101001",
   "011010100000000000000001010111110001111110011100000011101101010" when "01101010",
   "011010110000000000000001011001011100011110011011001110110010000" when "01101011",
   "011011000000000000000001011011000111111110011010011010000100010" when "01101100",
   "011011010000000000000001011100110100011110011001100101100100001" when "01101101",
   "011011100000000000000001011110100001111110011000110001010001100" when "01101110",
   "011011110000000000000001100000010000011110010111111101001100101" when "01101111",
   "011100000000000000000001100001111111111110010111001001010101101" when "01110000",
   "011100010000000000000001100011110000011110010110010101101100110" when "01110001",
   "011100100000000000000001100101100001111110010101100010010001110" when "01110010",
   "011100110000000000000001100111010100011110010100101111000101010" when "01110011",
   "011101000000000000000001101001000111111110010011111100000111000" when "01110100",
   "011101010000000000000001101010111100011110010011001001010111010" when "01110101",
   "011101100000000000000001101100110001111110010010010110110110001" when "01110110",
   "011101110000000000000001101110101000011110010001100100100011110" when "01110111",
   "011110000000000000000001110000011111111110010000110010100000010" when "01111000",
   "011110010000000000000001110010011000011110010000000000101011110" when "01111001",
   "011110100000000000000001110100010001111110001111001111000110100" when "01111010",
   "011110110000000000000001110110001100011110001110011101110000011" when "01111011",
   "011111000000000000000001111000000111111110001101101100101001101" when "01111100",
   "011111010000000000000001111010000100011110001100111011110010011" when "01111101",
   "011111100000000000000001111100000001111110001100001011001010110" when "01111110",
   "011111110000000000000001111110000000011110001011011010110011000" when "01111111",
   "100000000000000000000001111111111111111110001010101010101011000" when "10000000",
   "100000010000000000000010000010000000011110001001111010110011000" when "10000001",
   "100000100000000000000010000100000001111110001001001011001011001" when "10000010",
   "100000110000000000000010000110000100011110001000011011110011100" when "10000011",
   "100001000000000000000010001000000111111110000111101100101100010" when "10000100",
   "100001010000000000000010001010001100011110000110111101110101100" when "10000101",
   "100001100000000000000010001100010001111110000110001111001111011" when "10000110",
   "100001110000000000000010001110011000011110000101100000111010000" when "10000111",
   "100010000000000000000010010000011111111110000100110010110101101" when "10001000",
   "100010010000000000000010010010101000011110000100000101000010001" when "10001001",
   "100010100000000000000010010100110001111110000011010111011111110" when "10001010",
   "100010110000000000000010010110111100011110000010101010001110101" when "10001011",
   "100011000000000000000010011001000111111110000001111101001110111" when "10001100",
   "100011010000000000000010011011010100011110000001010000100000101" when "10001101",
   "100011100000000000000010011101100001111110000000100100000100000" when "10001110",
   "100011110000000000000010011111110000011101111111110111111001010" when "10001111",
   "100100000000000000000010100001111111111101111111001100000000010" when "10010000",
   "100100010000000000000010100100010000011101111110100000011001010" when "10010001",
   "100100100000000000000010100110100001111101111101110101000100011" when "10010010",
   "100100110000000000000010101000110100011101111101001010000001110" when "10010011",
   "100101000000000000000010101011000111111101111100011111010001100" when "10010100",
   "100101010000000000000010101101011100011101111011110100110011110" when "10010101",
   "100101100000000000000010101111110001111101111011001010101000101" when "10010110",
   "100101110000000000000010110010001000011101111010100000110000011" when "10010111",
   "100110000000000000000010110100011111111101111001110111001010111" when "10011000",
   "100110010000000000000010110110111000011101111001001101111000011" when "10011001",
   "100110100000000000000010111001010001111101111000100100111001000" when "10011010",
   "100110110000000000000010111011101100011101110111111100001100111" when "10011011",
   "100111000000000000000010111110000111111101110111010011110100001" when "10011100",
   "100111010000000000000011000000100100011101110110101011101110111" when "10011101",
   "100111100000000000000011000011000001111101110110000011111101011" when "10011110",
   "100111110000000000000011000101100000011101110101011100011111100" when "10011111",
   "101000000000000000000011000111111111111101110100110101010101100" when "10100000",
   "101000010000000000000011001010100000011101110100001110011111100" when "10100001",
   "101000100000000000000011001101000001111101110011100111111101101" when "10100010",
   "101000110000000000000011001111100100011101110011000001110000000" when "10100011",
   "101001000000000000000011010010000111111101110010011011110110110" when "10100100",
   "101001010000000000000011010100101100011101110001110110010010001" when "10100101",
   "101001100000000000000011010111010001111101110001010001000010000" when "10100110",
   "101001110000000000000011011001111000011101110000101100000110101" when "10100111",
   "101010000000000000000011011100011111111101110000000111100000001" when "10101000",
   "101010010000000000000011011111001000011101101111100011001110101" when "10101001",
   "101010100000000000000011100001110001111101101110111111010010010" when "10101010",
   "101010110000000000000011100100011100011101101110011011101011001" when "10101011",
   "101011000000000000000011100111000111111101101101111000011001011" when "10101100",
   "101011010000000000000011101001110100011101101101010101011101010" when "10101101",
   "101011100000000000000011101100100001111101101100110010110110101" when "10101110",
   "101011110000000000000011101111010000011101101100010000100101110" when "10101111",
   "101100000000000000000011110001111111111101101011101110101010110" when "10110000",
   "101100010000000000000011110100110000011101101011001101000101110" when "10110001",
   "101100100000000000000011110111100001111101101010101011110110111" when "10110010",
   "101100110000000000000011111010010100011101101010001010111110010" when "10110011",
   "101101000000000000000011111101000111111101101001101010011100001" when "10110100",
   "101101010000000000000011111111111100011101101001001010010000011" when "10110101",
   "101101100000000000000100000010110001111101101000101010011011010" when "10110110",
   "101101110000000000000100000101101000011101101000001010111100111" when "10110111",
   "101110000000000000000100001000011111111101100111101011110101011" when "10111000",
   "101110010000000000000100001011011000011101100111001101000100111" when "10111001",
   "101110100000000000000100001110010001111101100110101110101011100" when "10111010",
   "101110110000000000000100010001001100011101100110010000101001011" when "10111011",
   "101111000000000000000100010100000111111101100101110010111110110" when "10111100",
   "101111010000000000000100010111000100011101100101010101101011100" when "10111101",
   "101111100000000000000100011010000001111101100100111000101111111" when "10111110",
   "101111110000000000000100011101000000011101100100011100001100000" when "10111111",
   "110000000000000000000100011111111111111101100100000000000000000" when "11000000",
   "110000010000000000000100100011000000011101100011100100001100000" when "11000001",
   "110000100000000000000100100110000001111101100011001000110000001" when "11000010",
   "110000110000000000000100101001000100011101100010101101101100101" when "11000011",
   "110001000000000000000100101100000111111101100010010011000001011" when "11000100",
   "110001010000000000000100101111001100011101100001111000101110101" when "11000101",
   "110001100000000000000100110010010001111101100001011110110100100" when "11000110",
   "110001110000000000000100110101011000011101100001000101010011001" when "11000111",
   "110010000000000000000100111000011111111101100000101100001010101" when "11001000",
   "110010010000000000000100111011101000011101100000010011011011001" when "11001001",
   "110010100000000000000100111110110001111101011111111011000100110" when "11001010",
   "110010110000000000000101000001111100011101011111100011000111110" when "11001011",
   "110011000000000000000101000101000111111101011111001011100100000" when "11001100",
   "110011010000000000000101001000010100011101011110110100011001110" when "11001101",
   "110011100000000000000101001011100001111101011110011101101001001" when "11001110",
   "110011110000000000000101001110110000011101011110000111010010010" when "11001111",
   "110100000000000000000101010001111111111101011101110001010101010" when "11010000",
   "110100010000000000000101010101010000011101011101011011110010010" when "11010001",
   "110100100000000000000101011000100001111101011101000110101001100" when "11010010",
   "110100110000000000000101011011110100011101011100110001111010111" when "11010011",
   "110101000000000000000101011111000111111101011100011101100110101" when "11010100",
   "110101010000000000000101100010011100011101011100001001101100111" when "11010101",
   "110101100000000000000101100101110001111101011011110110001101110" when "11010110",
   "110101110000000000000101101001001000011101011011100011001001011" when "11010111",
   "110110000000000000000101101100011111111101011011010000011111111" when "11011000",
   "110110010000000000000101101111111000011101011010111110010001011" when "11011001",
   "110110100000000000000101110011010001111101011010101100011110001" when "11011010",
   "110110110000000000000101110110101100011101011010011011000110000" when "11011011",
   "110111000000000000000101111010000111111101011010001010001001010" when "11011100",
   "110111010000000000000101111101100100011101011001111001101000000" when "11011101",
   "110111100000000000000110000001000001111101011001101001100010011" when "11011110",
   "110111110000000000000110000100100000011101011001011001111000100" when "11011111",
   "111000000000000000000110000111111111111101011001001010101010100" when "11100000",
   "111000010000000000000110001011100000011101011000111011111000101" when "11100001",
   "111000100000000000000110001111000001111101011000101101100010110" when "11100010",
   "111000110000000000000110010010100100011101011000011111101001001" when "11100011",
   "111001000000000000000110010110000111111101011000010010001011111" when "11100100",
   "111001010000000000000110011001101100011101011000000101001011001" when "11100101",
   "111001100000000000000110011101010001111101010111111000100111000" when "11100110",
   "111001110000000000000110100000111000011101010111101100011111101" when "11100111",
   "111010000000000000000110100100011111111101010111100000110101001" when "11101000",
   "111010010000000000000110101000001000011101010111010101100111110" when "11101001",
   "111010100000000000000110101011110001111101010111001010110111011" when "11101010",
   "111010110000000000000110101111011100011101010111000000100100010" when "11101011",
   "111011000000000000000110110011000111111101010110110110101110100" when "11101100",
   "111011010000000000000110110110110100011101010110101101010110010" when "11101101",
   "111011100000000000000110111010100001111101010110100100011011101" when "11101110",
   "111011110000000000000110111110010000011101010110011011111110110" when "11101111",
   "111100000000000000000111000001111111111101010110010011111111111" when "11110000",
   "111100010000000000000111000101110000011101010110001100011110111" when "11110001",
   "111100100000000000000111001001100001111101010110000101011100000" when "11110010",
   "111100110000000000000111001101010100011101010101111110110111011" when "11110011",
   "111101000000000000000111010001000111111101010101111000110001001" when "11110100",
   "111101010000000000000111010100111100011101010101110011001001011" when "11110101",
   "111101100000000000000111011000110001111101010101101110000000010" when "11110110",
   "111101110000000000000111011100101000011101010101101001010101111" when "11110111",
   "111110000000000000000111100000011111111101010101100101001010100" when "11111000",
   "111110010000000000000111100100011000011101010101100001011110000" when "11111001",
   "111110100000000000000111101000010001111101010101011110010000101" when "11111010",
   "111110110000000000000111101100001100011101010101011011100010100" when "11111011",
   "111111000000000000000111110000000111111101010101011001010011110" when "11111100",
   "111111010000000000000111110100000100011101010101010111100100100" when "11111101",
   "111111100000000000000111111000000001111101010101010110010100111" when "11111110",
   "111111110000000000000111111100000000011101010101010101100101001" when "11111111",
   "---------------------------------------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_83_f400_uid103
--                    (IntAdderAlternative_83_f400_uid107)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_83_f400_uid103 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(82 downto 0);
          Y : in  std_logic_vector(82 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(82 downto 0)   );
end entity;

architecture arch of IntAdder_83_f400_uid103 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(41 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(40 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(41 downto 0);
signal sum_l1_idx1 :  std_logic_vector(40 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(82 downto 42)) + ( "0" & Y(82 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(40 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(41 downto 41);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(40 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(41 downto 41);
   R <= sum_l1_idx1(40 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_83_f400_uid110
--                    (IntAdderAlternative_83_f400_uid114)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_83_f400_uid110 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(82 downto 0);
          Y : in  std_logic_vector(82 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(82 downto 0)   );
end entity;

architecture arch of IntAdder_83_f400_uid110 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(41 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(40 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(41 downto 0);
signal sum_l1_idx1 :  std_logic_vector(40 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(82 downto 42)) + ( "0" & Y(82 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(40 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(41 downto 41);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(40 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(41 downto 41);
   R <= sum_l1_idx1(40 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                   KCMTable_6_49946518145322874_unsigned
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity KCMTable_6_49946518145322874_unsigned is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(61 downto 0)   );
end entity;

architecture arch of KCMTable_6_49946518145322874_unsigned is
begin
  with X select  Y <= 
   "00000000000000000000000000000000000000000000000000000000000000" when "000000",
   "00000010110001011100100001011111110111110100011100111101111010" when "000001",
   "00000101100010111001000010111111101111101000111001111011110100" when "000010",
   "00001000010100010101100100011111100111011101010110111001101110" when "000011",
   "00001011000101110010000101111111011111010001110011110111101000" when "000100",
   "00001101110111001110100111011111010111000110010000110101100010" when "000101",
   "00010000101000101011001000111111001110111010101101110011011100" when "000110",
   "00010011011010000111101010011111000110101111001010110001010110" when "000111",
   "00010110001011100100001011111110111110100011100111101111010000" when "001000",
   "00011000111101000000101101011110110110011000000100101101001010" when "001001",
   "00011011101110011101001110111110101110001100100001101011000100" when "001010",
   "00011110011111111001110000011110100110000000111110101000111110" when "001011",
   "00100001010001010110010001111110011101110101011011100110111000" when "001100",
   "00100100000010110010110011011110010101101001111000100100110010" when "001101",
   "00100110110100001111010100111110001101011110010101100010101100" when "001110",
   "00101001100101101011110110011110000101010010110010100000100110" when "001111",
   "00101100010111001000010111111101111101000111001111011110100000" when "010000",
   "00101111001000100100111001011101110100111011101100011100011010" when "010001",
   "00110001111010000001011010111101101100110000001001011010010100" when "010010",
   "00110100101011011101111100011101100100100100100110011000001110" when "010011",
   "00110111011100111010011101111101011100011001000011010110001000" when "010100",
   "00111010001110010110111111011101010100001101100000010100000010" when "010101",
   "00111100111111110011100000111101001100000001111101010001111100" when "010110",
   "00111111110001010000000010011101000011110110011010001111110110" when "010111",
   "01000010100010101100100011111100111011101010110111001101110000" when "011000",
   "01000101010100001001000101011100110011011111010100001011101010" when "011001",
   "01001000000101100101100110111100101011010011110001001001100100" when "011010",
   "01001010110111000010001000011100100011001000001110000111011110" when "011011",
   "01001101101000011110101001111100011010111100101011000101011000" when "011100",
   "01010000011001111011001011011100010010110001001000000011010010" when "011101",
   "01010011001011010111101100111100001010100101100101000001001100" when "011110",
   "01010101111100110100001110011100000010011010000001111111000110" when "011111",
   "01011000101110010000101111111011111010001110011110111101000000" when "100000",
   "01011011011111101101010001011011110010000010111011111010111010" when "100001",
   "01011110010001001001110010111011101001110111011000111000110100" when "100010",
   "01100001000010100110010100011011100001101011110101110110101110" when "100011",
   "01100011110100000010110101111011011001100000010010110100101000" when "100100",
   "01100110100101011111010111011011010001010100101111110010100010" when "100101",
   "01101001010110111011111000111011001001001001001100110000011100" when "100110",
   "01101100001000011000011010011011000000111101101001101110010110" when "100111",
   "01101110111001110100111011111010111000110010000110101100010000" when "101000",
   "01110001101011010001011101011010110000100110100011101010001010" when "101001",
   "01110100011100101101111110111010101000011011000000101000000100" when "101010",
   "01110111001110001010100000011010100000001111011101100101111110" when "101011",
   "01111001111111100111000001111010011000000011111010100011111000" when "101100",
   "01111100110001000011100011011010001111111000010111100001110010" when "101101",
   "01111111100010100000000100111010000111101100110100011111101100" when "101110",
   "10000010010011111100100110011001111111100001010001011101100110" when "101111",
   "10000101000101011001000111111001110111010101101110011011100000" when "110000",
   "10000111110110110101101001011001101111001010001011011001011010" when "110001",
   "10001010101000010010001010111001100110111110101000010111010100" when "110010",
   "10001101011001101110101100011001011110110011000101010101001110" when "110011",
   "10010000001011001011001101111001010110100111100010010011001000" when "110100",
   "10010010111100100111101111011001001110011011111111010001000010" when "110101",
   "10010101101110000100010000111001000110010000011100001110111100" when "110110",
   "10011000011111100000110010011000111110000100111001001100110110" when "110111",
   "10011011010000111101010011111000110101111001010110001010110000" when "111000",
   "10011110000010011001110101011000101101101101110011001000101010" when "111001",
   "10100000110011110110010110111000100101100010010000000110100100" when "111010",
   "10100011100101010010111000011000011101010110101101000100011110" when "111011",
   "10100110010110101111011001111000010101001011001010000010011000" when "111100",
   "10101001001000001011111011011000001100111111100111000000010010" when "111101",
   "10101011111001101000011100111000000100110100000011111110001100" when "111110",
   "10101110101011000100111110010111111100101000100000111100000110" when "111111",
   "--------------------------------------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                   KCMTable_5_49946518145322874_unsigned
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity KCMTable_5_49946518145322874_unsigned is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(4 downto 0);
          Y : out  std_logic_vector(60 downto 0)   );
end entity;

architecture arch of KCMTable_5_49946518145322874_unsigned is
begin
  with X select  Y <= 
   "0000000000000000000000000000000000000000000000000000000000000" when "00000",
   "0000010110001011100100001011111110111110100011100111101111010" when "00001",
   "0000101100010111001000010111111101111101000111001111011110100" when "00010",
   "0001000010100010101100100011111100111011101010110111001101110" when "00011",
   "0001011000101110010000101111111011111010001110011110111101000" when "00100",
   "0001101110111001110100111011111010111000110010000110101100010" when "00101",
   "0010000101000101011001000111111001110111010101101110011011100" when "00110",
   "0010011011010000111101010011111000110101111001010110001010110" when "00111",
   "0010110001011100100001011111110111110100011100111101111010000" when "01000",
   "0011000111101000000101101011110110110011000000100101101001010" when "01001",
   "0011011101110011101001110111110101110001100100001101011000100" when "01010",
   "0011110011111111001110000011110100110000000111110101000111110" when "01011",
   "0100001010001010110010001111110011101110101011011100110111000" when "01100",
   "0100100000010110010110011011110010101101001111000100100110010" when "01101",
   "0100110110100001111010100111110001101011110010101100010101100" when "01110",
   "0101001100101101011110110011110000101010010110010100000100110" when "01111",
   "0101100010111001000010111111101111101000111001111011110100000" when "10000",
   "0101111001000100100111001011101110100111011101100011100011010" when "10001",
   "0110001111010000001011010111101101100110000001001011010010100" when "10010",
   "0110100101011011101111100011101100100100100100110011000001110" when "10011",
   "0110111011100111010011101111101011100011001000011010110001000" when "10100",
   "0111010001110010110111111011101010100001101100000010100000010" when "10101",
   "0111100111111110011100000111101001100000001111101010001111100" when "10110",
   "0111111110001010000000010011101000011110110011010001111110110" when "10111",
   "1000010100010101100100011111100111011101010110111001101110000" when "11000",
   "1000101010100001001000101011100110011011111010100001011101010" when "11001",
   "1001000000101100101100110111100101011010011110001001001100100" when "11010",
   "1001010110111000010001000011100100011001000001110000111011110" when "11011",
   "1001101101000011110101001111100011010111100101011000101011000" when "11100",
   "1010000011001111011001011011100010010110001001000000011010010" when "11101",
   "1010011001011010111101100111100001010100101100101000001001100" when "11110",
   "1010101111100110100001110011100000010011010000001111111000110" when "11111",
   "-------------------------------------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_61_f400_uid122
--                     (IntAdderClassical_61_f400_uid124)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 2 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_61_f400_uid122 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(60 downto 0);
          Y : in  std_logic_vector(60 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(60 downto 0)   );
end entity;

architecture arch of IntAdder_61_f400_uid122 is
signal x0 :  std_logic_vector(13 downto 0);
signal y0 :  std_logic_vector(13 downto 0);
signal x1, x1_d1 :  std_logic_vector(41 downto 0);
signal y1, y1_d1 :  std_logic_vector(41 downto 0);
signal x2, x2_d1, x2_d2 :  std_logic_vector(4 downto 0);
signal y2, y2_d1, y2_d2 :  std_logic_vector(4 downto 0);
signal sum0, sum0_d1, sum0_d2 :  std_logic_vector(14 downto 0);
signal sum1, sum1_d1 :  std_logic_vector(42 downto 0);
signal sum2 :  std_logic_vector(5 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            x1_d1 <=  x1;
            y1_d1 <=  y1;
            x2_d1 <=  x2;
            x2_d2 <=  x2_d1;
            y2_d1 <=  y2;
            y2_d2 <=  y2_d1;
            sum0_d1 <=  sum0;
            sum0_d2 <=  sum0_d1;
            sum1_d1 <=  sum1;
         end if;
      end process;
   --Classical
   x0 <= X(13 downto 0);
   y0 <= Y(13 downto 0);
   x1 <= X(55 downto 14);
   y1 <= Y(55 downto 14);
   x2 <= X(60 downto 56);
   y2 <= Y(60 downto 56);
   sum0 <= ( "0" & x0) + ( "0" & y0)  + Cin;
   ----------------Synchro barrier, entering cycle 1----------------
   sum1 <= ( "0" & x1_d1) + ( "0" & y1_d1)  + sum0_d1(14);
   ----------------Synchro barrier, entering cycle 2----------------
   sum2 <= ( "0" & x2_d2) + ( "0" & y2_d2)  + sum1_d1(42);
   R <= sum2(4 downto 0) & sum1_d1(41 downto 0) & sum0_d2(13 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                  IntIntKCM_11_49946518145322874_unsigned
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2009,2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 2 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntIntKCM_11_49946518145322874_unsigned is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(10 downto 0);
          R : out  std_logic_vector(66 downto 0)   );
end entity;

architecture arch of IntIntKCM_11_49946518145322874_unsigned is
   component IntAdder_61_f400_uid122 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(60 downto 0);
             Y : in  std_logic_vector(60 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(60 downto 0)   );
   end component;

   component KCMTable_5_49946518145322874_unsigned is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(4 downto 0);
             Y : out  std_logic_vector(60 downto 0)   );
   end component;

   component KCMTable_6_49946518145322874_unsigned is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(61 downto 0)   );
   end component;

signal d0 :  std_logic_vector(5 downto 0);
signal pp0, pp0_d1, pp0_d2 :  std_logic_vector(61 downto 0);
signal d1 :  std_logic_vector(4 downto 0);
signal pp1 :  std_logic_vector(60 downto 0);
signal addOp0 :  std_logic_vector(60 downto 0);
signal addOp1 :  std_logic_vector(60 downto 0);
signal OutRes :  std_logic_vector(60 downto 0);
attribute rom_extract: string;
attribute rom_style: string;
attribute rom_extract of KCMTable_5_49946518145322874_unsigned: component is "yes";
attribute rom_extract of KCMTable_6_49946518145322874_unsigned: component is "yes";
attribute rom_style of KCMTable_5_49946518145322874_unsigned: component is "distributed";
attribute rom_style of KCMTable_6_49946518145322874_unsigned: component is "distributed";
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            pp0_d1 <=  pp0;
            pp0_d2 <=  pp0_d1;
         end if;
      end process;
   d0 <= X(5 downto 0);
   KCMTable_0: KCMTable_6_49946518145322874_unsigned  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => d0,
                 Y => pp0);
   d1 <= X(10 downto 6);
   KCMTable_1: KCMTable_5_49946518145322874_unsigned  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => d1,
                 Y => pp1);
   addOp0 <= (60 downto 56 => '0') & pp0(61 downto 6) & "";
   addOp1 <= pp1(60 downto 0) & "";
   Result_Adder: IntAdder_61_f400_uid122  -- pipelineDepth=2 maxInDelay=1.49864e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => OutRes,
                 X => addOp0,
                 Y => addOp1);
   ----------------Synchro barrier, entering cycle 2----------------
   R <= OutRes & pp0_d2(5 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_94_f400_uid130
--                     (IntAdderClassical_94_f400_uid132)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 3 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_94_f400_uid130 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(93 downto 0);
          Y : in  std_logic_vector(93 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(93 downto 0)   );
end entity;

architecture arch of IntAdder_94_f400_uid130 is
signal x0 :  std_logic_vector(4 downto 0);
signal y0 :  std_logic_vector(4 downto 0);
signal x1, x1_d1 :  std_logic_vector(41 downto 0);
signal y1, y1_d1 :  std_logic_vector(41 downto 0);
signal x2, x2_d1, x2_d2 :  std_logic_vector(41 downto 0);
signal y2, y2_d1, y2_d2 :  std_logic_vector(41 downto 0);
signal x3, x3_d1, x3_d2, x3_d3 :  std_logic_vector(4 downto 0);
signal y3, y3_d1, y3_d2, y3_d3 :  std_logic_vector(4 downto 0);
signal sum0, sum0_d1, sum0_d2, sum0_d3 :  std_logic_vector(5 downto 0);
signal sum1, sum1_d1, sum1_d2 :  std_logic_vector(42 downto 0);
signal sum2, sum2_d1 :  std_logic_vector(42 downto 0);
signal sum3 :  std_logic_vector(5 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            x1_d1 <=  x1;
            y1_d1 <=  y1;
            x2_d1 <=  x2;
            x2_d2 <=  x2_d1;
            y2_d1 <=  y2;
            y2_d2 <=  y2_d1;
            x3_d1 <=  x3;
            x3_d2 <=  x3_d1;
            x3_d3 <=  x3_d2;
            y3_d1 <=  y3;
            y3_d2 <=  y3_d1;
            y3_d3 <=  y3_d2;
            sum0_d1 <=  sum0;
            sum0_d2 <=  sum0_d1;
            sum0_d3 <=  sum0_d2;
            sum1_d1 <=  sum1;
            sum1_d2 <=  sum1_d1;
            sum2_d1 <=  sum2;
         end if;
      end process;
   --Classical
   x0 <= X(4 downto 0);
   y0 <= Y(4 downto 0);
   x1 <= X(46 downto 5);
   y1 <= Y(46 downto 5);
   x2 <= X(88 downto 47);
   y2 <= Y(88 downto 47);
   x3 <= X(93 downto 89);
   y3 <= Y(93 downto 89);
   sum0 <= ( "0" & x0) + ( "0" & y0)  + Cin;
   ----------------Synchro barrier, entering cycle 1----------------
   sum1 <= ( "0" & x1_d1) + ( "0" & y1_d1)  + sum0_d1(5);
   ----------------Synchro barrier, entering cycle 2----------------
   sum2 <= ( "0" & x2_d2) + ( "0" & y2_d2)  + sum1_d1(42);
   ----------------Synchro barrier, entering cycle 3----------------
   sum3 <= ( "0" & x3_d3) + ( "0" & y3_d3)  + sum2_d1(42);
   R <= sum3(4 downto 0) & sum2_d1(41 downto 0) & sum1_d2(41 downto 0) & sum0_d3(4 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                LZCShifter_94_to_83_counting_128_F400_uid137
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Bogdan Pasca (2007)
--------------------------------------------------------------------------------
-- Pipeline depth: 4 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LZCShifter_94_to_83_counting_128_F400_uid137 is
   port ( clk, rst : in std_logic;
          I : in  std_logic_vector(93 downto 0);
          Count : out  std_logic_vector(6 downto 0);
          O : out  std_logic_vector(82 downto 0)   );
end entity;

architecture arch of LZCShifter_94_to_83_counting_128_F400_uid137 is
signal level7, level7_d1 :  std_logic_vector(93 downto 0);
signal count6, count6_d1, count6_d2, count6_d3 :  std_logic;
signal level6 :  std_logic_vector(93 downto 0);
signal count5, count5_d1, count5_d2, count5_d3 :  std_logic;
signal level5, level5_d1 :  std_logic_vector(93 downto 0);
signal count4, count4_d1, count4_d2 :  std_logic;
signal level4 :  std_logic_vector(93 downto 0);
signal count3, count3_d1, count3_d2 :  std_logic;
signal level3, level3_d1 :  std_logic_vector(89 downto 0);
signal count2, count2_d1 :  std_logic;
signal level2 :  std_logic_vector(85 downto 0);
signal count1, count1_d1 :  std_logic;
signal level1, level1_d1 :  std_logic_vector(83 downto 0);
signal count0 :  std_logic;
signal level0 :  std_logic_vector(82 downto 0);
signal sCount :  std_logic_vector(6 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            level7_d1 <=  level7;
            count6_d1 <=  count6;
            count6_d2 <=  count6_d1;
            count6_d3 <=  count6_d2;
            count5_d1 <=  count5;
            count5_d2 <=  count5_d1;
            count5_d3 <=  count5_d2;
            level5_d1 <=  level5;
            count4_d1 <=  count4;
            count4_d2 <=  count4_d1;
            count3_d1 <=  count3;
            count3_d2 <=  count3_d1;
            level3_d1 <=  level3;
            count2_d1 <=  count2;
            count1_d1 <=  count1;
            level1_d1 <=  level1;
         end if;
      end process;
   level7 <= I ;
   ----------------Synchro barrier, entering cycle 1----------------
   count6<= '1' when level7_d1(93 downto 30) = (93 downto 30=>'0') else '0';
   level6<= level7_d1(93 downto 0) when count6='0' else level7_d1(29 downto 0) & (63 downto 0 => '0');

   count5<= '1' when level6(93 downto 62) = (93 downto 62=>'0') else '0';
   level5<= level6(93 downto 0) when count5='0' else level6(61 downto 0) & (31 downto 0 => '0');

   ----------------Synchro barrier, entering cycle 2----------------
   count4<= '1' when level5_d1(93 downto 78) = (93 downto 78=>'0') else '0';
   level4<= level5_d1(93 downto 0) when count4='0' else level5_d1(77 downto 0) & (15 downto 0 => '0');

   count3<= '1' when level4(93 downto 86) = (93 downto 86=>'0') else '0';
   level3<= level4(93 downto 4) when count3='0' else level4(85 downto 0) & (3 downto 0 => '0');

   ----------------Synchro barrier, entering cycle 3----------------
   count2<= '1' when level3_d1(89 downto 86) = (89 downto 86=>'0') else '0';
   level2<= level3_d1(89 downto 4) when count2='0' else level3_d1(85 downto 0);

   count1<= '1' when level2(85 downto 84) = (85 downto 84=>'0') else '0';
   level1<= level2(85 downto 2) when count1='0' else level2(83 downto 0);

   ----------------Synchro barrier, entering cycle 4----------------
   count0<= '1' when level1_d1(83 downto 83) = (83 downto 83=>'0') else '0';
   level0<= level1_d1(83 downto 1) when count0='0' else level1_d1(82 downto 0);

   O <= level0;
   sCount <= count6_d3 & count5_d3 & count4_d2 & count3_d2 & count2_d1 & count1_d1 & count0;
   Count <= sCount;
end architecture;

--------------------------------------------------------------------------------
--                   RightShifter_31_by_max_30_F400_uid140
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity RightShifter_31_by_max_30_F400_uid140 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(30 downto 0);
          S : in  std_logic_vector(4 downto 0);
          R : out  std_logic_vector(60 downto 0)   );
end entity;

architecture arch of RightShifter_31_by_max_30_F400_uid140 is
signal level0 :  std_logic_vector(30 downto 0);
signal ps, ps_d1 :  std_logic_vector(4 downto 0);
signal level1 :  std_logic_vector(31 downto 0);
signal level2, level2_d1 :  std_logic_vector(33 downto 0);
signal level3 :  std_logic_vector(37 downto 0);
signal level4 :  std_logic_vector(45 downto 0);
signal level5 :  std_logic_vector(61 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            ps_d1 <=  ps;
            level2_d1 <=  level2;
         end if;
      end process;
   level0<= X;
   ps<= S;
   level1<=  (0 downto 0 => '0') & level0 when ps(0) = '1' else    level0 & (0 downto 0 => '0');
   level2<=  (1 downto 0 => '0') & level1 when ps(1) = '1' else    level1 & (1 downto 0 => '0');
   ----------------Synchro barrier, entering cycle 1----------------
   level3<=  (3 downto 0 => '0') & level2_d1 when ps_d1(2) = '1' else    level2_d1 & (3 downto 0 => '0');
   level4<=  (7 downto 0 => '0') & level3 when ps_d1(3) = '1' else    level3 & (7 downto 0 => '0');
   level5<=  (15 downto 0 => '0') & level4 when ps_d1(4) = '1' else    level4 & (15 downto 0 => '0');
   R <= level5(61 downto 1);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_58_f400_uid143
--                    (IntAdderAlternative_58_f400_uid147)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_58_f400_uid143 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(57 downto 0);
          Y : in  std_logic_vector(57 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(57 downto 0)   );
end entity;

architecture arch of IntAdder_58_f400_uid143 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(16 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(15 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(16 downto 0);
signal sum_l1_idx1 :  std_logic_vector(15 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(57 downto 42)) + ( "0" & Y(57 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(15 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(16 downto 16);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(15 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(16 downto 16);
   R <= sum_l1_idx1(15 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_63_f400_uid150
--                    (IntAdderAlternative_63_f400_uid154)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_63_f400_uid150 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(62 downto 0);
          Y : in  std_logic_vector(62 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(62 downto 0)   );
end entity;

architecture arch of IntAdder_63_f400_uid150 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(21 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(20 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(21 downto 0);
signal sum_l1_idx1 :  std_logic_vector(20 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(62 downto 42)) + ( "0" & Y(62 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(20 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(21 downto 21);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(20 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(21 downto 21);
   R <= sum_l1_idx1(20 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                         IterativeLog_11_52_10_400
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: F. de Dinechin, C. Klein  (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 25 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IterativeLog_11_52_10_400 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(11+52+2 downto 0);
          R : out  std_logic_vector(11+52+2 downto 0)   );
end entity;

architecture arch of IterativeLog_11_52_10_400 is
   component IntAdder_56_f400_uid39 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(55 downto 0);
             Y : in  std_logic_vector(55 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(55 downto 0)   );
   end component;

   component IntAdder_56_f400_uid46 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(55 downto 0);
             Y : in  std_logic_vector(55 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(55 downto 0)   );
   end component;

   component IntAdder_56_f400_uid56 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(55 downto 0);
             Y : in  std_logic_vector(55 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(55 downto 0)   );
   end component;

   component IntAdder_58_f400_uid143 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(57 downto 0);
             Y : in  std_logic_vector(57 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(57 downto 0)   );
   end component;

   component IntAdder_63_f400_uid150 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(62 downto 0);
             Y : in  std_logic_vector(62 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(62 downto 0)   );
   end component;

   component IntAdder_63_f400_uid25 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(62 downto 0);
             Y : in  std_logic_vector(62 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(62 downto 0)   );
   end component;

   component IntAdder_63_f400_uid32 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(62 downto 0);
             Y : in  std_logic_vector(62 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(62 downto 0)   );
   end component;

   component IntAdder_64_f400_uid11 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(63 downto 0);
             Y : in  std_logic_vector(63 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(63 downto 0)   );
   end component;

   component IntAdder_64_f400_uid18 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(63 downto 0);
             Y : in  std_logic_vector(63 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(63 downto 0)   );
   end component;

   component IntAdder_83_f400_uid103 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(82 downto 0);
             Y : in  std_logic_vector(82 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(82 downto 0)   );
   end component;

   component IntAdder_83_f400_uid110 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(82 downto 0);
             Y : in  std_logic_vector(82 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(82 downto 0)   );
   end component;

   component IntAdder_83_f400_uid85 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(82 downto 0);
             Y : in  std_logic_vector(82 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(82 downto 0)   );
   end component;

   component IntAdder_83_f400_uid94 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(82 downto 0);
             Y : in  std_logic_vector(82 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(82 downto 0)   );
   end component;

   component IntAdder_94_f400_uid130 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(93 downto 0);
             Y : in  std_logic_vector(93 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(93 downto 0)   );
   end component;

   component IntIntKCM_11_49946518145322874_unsigned is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(10 downto 0);
             R : out  std_logic_vector(66 downto 0)   );
   end component;

   component IntSquarer_31_uid53 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(30 downto 0);
             R : out  std_logic_vector(61 downto 0)   );
   end component;

   component InvTable_0_9_10 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(8 downto 0);
             Y : out  std_logic_vector(9 downto 0)   );
   end component;

   component LZCShifter_94_to_83_counting_128_F400_uid137 is
      port ( clk, rst : in std_logic;
             I : in  std_logic_vector(93 downto 0);
             Count : out  std_logic_vector(6 downto 0);
             O : out  std_logic_vector(82 downto 0)   );
   end component;

   component LZOC_52_F400_uid3 is
      port ( clk, rst : in std_logic;
             I : in  std_logic_vector(51 downto 0);
             OZB : in  std_logic;
             O : out  std_logic_vector(5 downto 0)   );
   end component;

   component LeftShifter_27_by_max_27_F400_uid6 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(26 downto 0);
             S : in  std_logic_vector(4 downto 0);
             R : out  std_logic_vector(53 downto 0)   );
   end component;

   component LogTable_0_9_83 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(8 downto 0);
             Y : out  std_logic_vector(82 downto 0)   );
   end component;

   component LogTable_1_7_76 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(6 downto 0);
             Y : out  std_logic_vector(75 downto 0)   );
   end component;

   component LogTable_2_8_70 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(7 downto 0);
             Y : out  std_logic_vector(69 downto 0)   );
   end component;

   component LogTable_3_8_63 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(7 downto 0);
             Y : out  std_logic_vector(62 downto 0)   );
   end component;

   component RightShifter_31_by_max_30_F400_uid140 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(30 downto 0);
             S : in  std_logic_vector(4 downto 0);
             R : out  std_logic_vector(60 downto 0)   );
   end component;

signal XExnSgn, XExnSgn_d1, XExnSgn_d2, XExnSgn_d3, XExnSgn_d4, XExnSgn_d5, XExnSgn_d6, XExnSgn_d7, XExnSgn_d8, XExnSgn_d9, XExnSgn_d10, XExnSgn_d11, XExnSgn_d12, XExnSgn_d13, XExnSgn_d14, XExnSgn_d15, XExnSgn_d16, XExnSgn_d17, XExnSgn_d18, XExnSgn_d19, XExnSgn_d20, XExnSgn_d21, XExnSgn_d22, XExnSgn_d23, XExnSgn_d24, XExnSgn_d25 :  std_logic_vector(2 downto 0);
signal FirstBit :  std_logic;
signal Y0, Y0_d1, Y0_d2 :  std_logic_vector(53 downto 0);
signal Y0h :  std_logic_vector(51 downto 0);
signal sR, sR_d1, sR_d2, sR_d3, sR_d4, sR_d5, sR_d6, sR_d7, sR_d8, sR_d9, sR_d10, sR_d11, sR_d12, sR_d13, sR_d14, sR_d15, sR_d16, sR_d17, sR_d18, sR_d19, sR_d20, sR_d21, sR_d22, sR_d23, sR_d24, sR_d25 :  std_logic;
signal absZ0, absZ0_d1, absZ0_d2, absZ0_d3, absZ0_d4 :  std_logic_vector(26 downto 0);
signal E, E_d1 :  std_logic_vector(10 downto 0);
signal absE, absE_d1, absE_d2, absE_d3, absE_d4, absE_d5, absE_d6, absE_d7, absE_d8, absE_d9, absE_d10, absE_d11, absE_d12, absE_d13, absE_d14 :  std_logic_vector(10 downto 0);
signal EeqZero, EeqZero_d1, EeqZero_d2, EeqZero_d3 :  std_logic;
signal lzo, lzo_d1, lzo_d2, lzo_d3, lzo_d4, lzo_d5, lzo_d6, lzo_d7, lzo_d8, lzo_d9, lzo_d10, lzo_d11, lzo_d12, lzo_d13, lzo_d14, lzo_d15, lzo_d16, lzo_d17, lzo_d18, lzo_d19, lzo_d20, lzo_d21 :  std_logic_vector(5 downto 0);
signal pfinal_s :  std_logic_vector(5 downto 0);
signal shiftval :  std_logic_vector(6 downto 0);
signal shiftvalinL :  std_logic_vector(4 downto 0);
signal shiftvalinR, shiftvalinR_d1, shiftvalinR_d2, shiftvalinR_d3, shiftvalinR_d4, shiftvalinR_d5, shiftvalinR_d6, shiftvalinR_d7, shiftvalinR_d8, shiftvalinR_d9, shiftvalinR_d10, shiftvalinR_d11, shiftvalinR_d12, shiftvalinR_d13, shiftvalinR_d14, shiftvalinR_d15, shiftvalinR_d16 :  std_logic_vector(4 downto 0);
signal doRR, doRR_d1, doRR_d2, doRR_d3, doRR_d4, doRR_d5, doRR_d6 :  std_logic;
signal small, small_d1, small_d2, small_d3, small_d4, small_d5, small_d6, small_d7, small_d8, small_d9, small_d10, small_d11, small_d12, small_d13, small_d14, small_d15, small_d16, small_d17, small_d18, small_d19, small_d20, small_d21 :  std_logic;
signal small_absZ0_normd_full :  std_logic_vector(53 downto 0);
signal small_absZ0_normd, small_absZ0_normd_d1, small_absZ0_normd_d2, small_absZ0_normd_d3, small_absZ0_normd_d4, small_absZ0_normd_d5, small_absZ0_normd_d6, small_absZ0_normd_d7, small_absZ0_normd_d8, small_absZ0_normd_d9, small_absZ0_normd_d10, small_absZ0_normd_d11, small_absZ0_normd_d12, small_absZ0_normd_d13, small_absZ0_normd_d14, small_absZ0_normd_d15, small_absZ0_normd_d16 :  std_logic_vector(26 downto 0);
signal A0, A0_d1, A0_d2, A0_d3 :  std_logic_vector(8 downto 0);
signal InvA0, InvA0_d1 :  std_logic_vector(9 downto 0);
signal P0, P0_d1 :  std_logic_vector(63 downto 0);
signal Z1 :  std_logic_vector(54 downto 0);
signal A1, A1_d1 :  std_logic_vector(6 downto 0);
signal B1 :  std_logic_vector(47 downto 0);
signal ZM1, ZM1_d1 :  std_logic_vector(54 downto 0);
signal P1 :  std_logic_vector(61 downto 0);
signal Y1 :  std_logic_vector(62 downto 0);
signal EiY1 :  std_logic_vector(63 downto 0);
signal addXIter1 :  std_logic_vector(63 downto 0);
signal EiYPB1 :  std_logic_vector(63 downto 0);
signal Pp1 :  std_logic_vector(63 downto 0);
signal Z2 :  std_logic_vector(63 downto 0);
signal A2, A2_d1, A2_d2, A2_d3 :  std_logic_vector(7 downto 0);
signal B2 :  std_logic_vector(55 downto 0);
signal ZM2, ZM2_d1 :  std_logic_vector(56 downto 0);
signal P2, P2_d1 :  std_logic_vector(64 downto 0);
signal Y2 :  std_logic_vector(77 downto 0);
signal EiY2 :  std_logic_vector(62 downto 0);
signal addXIter2 :  std_logic_vector(62 downto 0);
signal EiYPB2 :  std_logic_vector(62 downto 0);
signal Pp2 :  std_logic_vector(62 downto 0);
signal Z3 :  std_logic_vector(62 downto 0);
signal A3, A3_d1, A3_d2, A3_d3, A3_d4 :  std_logic_vector(7 downto 0);
signal B3 :  std_logic_vector(54 downto 0);
signal ZM3, ZM3_d1 :  std_logic_vector(42 downto 0);
signal P3 :  std_logic_vector(50 downto 0);
signal Y3 :  std_logic_vector(83 downto 0);
signal EiY3 :  std_logic_vector(55 downto 0);
signal addXIter3 :  std_logic_vector(55 downto 0);
signal EiYPB3 :  std_logic_vector(55 downto 0);
signal Pp3 :  std_logic_vector(55 downto 0);
signal Z4 :  std_logic_vector(55 downto 0);
signal Zfinal, Zfinal_d1, Zfinal_d2, Zfinal_d3, Zfinal_d4 :  std_logic_vector(55 downto 0);
signal squarerIn :  std_logic_vector(30 downto 0);
signal Z2o2_full :  std_logic_vector(61 downto 0);
signal Z2o2_full_dummy, Z2o2_full_dummy_d1, Z2o2_full_dummy_d2, Z2o2_full_dummy_d3, Z2o2_full_dummy_d4, Z2o2_full_dummy_d5, Z2o2_full_dummy_d6 :  std_logic_vector(61 downto 0);
signal Z2o2_normal :  std_logic_vector(27 downto 0);
signal addFinalLog1pY :  std_logic_vector(55 downto 0);
signal Log1p_normal, Log1p_normal_d1 :  std_logic_vector(55 downto 0);
signal L0 :  std_logic_vector(82 downto 0);
signal S1, S1_d1, S1_d2 :  std_logic_vector(82 downto 0);
signal L1 :  std_logic_vector(75 downto 0);
signal sopX1, sopX1_d1, sopX1_d2 :  std_logic_vector(82 downto 0);
signal S2, S2_d1, S2_d2, S2_d3 :  std_logic_vector(82 downto 0);
signal L2 :  std_logic_vector(69 downto 0);
signal sopX2, sopX2_d1, sopX2_d2 :  std_logic_vector(82 downto 0);
signal S3, S3_d1, S3_d2, S3_d3 :  std_logic_vector(82 downto 0);
signal L3 :  std_logic_vector(62 downto 0);
signal sopX3, sopX3_d1, sopX3_d2 :  std_logic_vector(82 downto 0);
signal S4, S4_d1 :  std_logic_vector(82 downto 0);
signal almostLog :  std_logic_vector(82 downto 0);
signal adderLogF_normalY :  std_logic_vector(82 downto 0);
signal LogF_normal :  std_logic_vector(82 downto 0);
signal absELog2 :  std_logic_vector(66 downto 0);
signal absELog2_pad :  std_logic_vector(93 downto 0);
signal LogF_normal_pad :  std_logic_vector(93 downto 0);
signal lnaddX :  std_logic_vector(93 downto 0);
signal lnaddY :  std_logic_vector(93 downto 0);
signal Log_normal :  std_logic_vector(93 downto 0);
signal E_normal :  std_logic_vector(6 downto 0);
signal Log_normal_normd, Log_normal_normd_d1 :  std_logic_vector(82 downto 0);
signal Z2o2_small_bs :  std_logic_vector(30 downto 0);
signal Z2o2_small_s :  std_logic_vector(60 downto 0);
signal Z2o2_small, Z2o2_small_d1 :  std_logic_vector(57 downto 0);
signal Z_small, Z_small_d1 :  std_logic_vector(57 downto 0);
signal Log_smallY :  std_logic_vector(57 downto 0);
signal nsRCin :  std_logic;
signal Log_small, Log_small_d1 :  std_logic_vector(57 downto 0);
signal E0_sub, E0_sub_d1 :  std_logic_vector(1 downto 0);
signal ufl, ufl_d1, ufl_d2 :  std_logic;
signal E_small :  std_logic_vector(10 downto 0);
signal Log_small_normd, Log_small_normd_d1 :  std_logic_vector(55 downto 0);
signal E0offset :  std_logic_vector(10 downto 0);
signal ER :  std_logic_vector(10 downto 0);
signal Log_g :  std_logic_vector(55 downto 0);
signal round :  std_logic;
signal fraX :  std_logic_vector(62 downto 0);
signal fraY :  std_logic_vector(62 downto 0);
signal EFR :  std_logic_vector(62 downto 0);
constant g: positive := 4;
constant log2wF: positive := 6;
constant pfinal: positive := 27;
constant sfinal: positive := 56;
constant targetprec: positive := 83;
constant wE: positive := 11;
constant wF: positive := 52;
attribute rom_extract: string;
attribute rom_style: string;
attribute rom_extract of InvTable_0_9_10: component is "yes";
attribute rom_style of InvTable_0_9_10: component is "block";
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            XExnSgn_d1 <=  XExnSgn;
            XExnSgn_d2 <=  XExnSgn_d1;
            XExnSgn_d3 <=  XExnSgn_d2;
            XExnSgn_d4 <=  XExnSgn_d3;
            XExnSgn_d5 <=  XExnSgn_d4;
            XExnSgn_d6 <=  XExnSgn_d5;
            XExnSgn_d7 <=  XExnSgn_d6;
            XExnSgn_d8 <=  XExnSgn_d7;
            XExnSgn_d9 <=  XExnSgn_d8;
            XExnSgn_d10 <=  XExnSgn_d9;
            XExnSgn_d11 <=  XExnSgn_d10;
            XExnSgn_d12 <=  XExnSgn_d11;
            XExnSgn_d13 <=  XExnSgn_d12;
            XExnSgn_d14 <=  XExnSgn_d13;
            XExnSgn_d15 <=  XExnSgn_d14;
            XExnSgn_d16 <=  XExnSgn_d15;
            XExnSgn_d17 <=  XExnSgn_d16;
            XExnSgn_d18 <=  XExnSgn_d17;
            XExnSgn_d19 <=  XExnSgn_d18;
            XExnSgn_d20 <=  XExnSgn_d19;
            XExnSgn_d21 <=  XExnSgn_d20;
            XExnSgn_d22 <=  XExnSgn_d21;
            XExnSgn_d23 <=  XExnSgn_d22;
            XExnSgn_d24 <=  XExnSgn_d23;
            XExnSgn_d25 <=  XExnSgn_d24;
            Y0_d1 <=  Y0;
            Y0_d2 <=  Y0_d1;
            sR_d1 <=  sR;
            sR_d2 <=  sR_d1;
            sR_d3 <=  sR_d2;
            sR_d4 <=  sR_d3;
            sR_d5 <=  sR_d4;
            sR_d6 <=  sR_d5;
            sR_d7 <=  sR_d6;
            sR_d8 <=  sR_d7;
            sR_d9 <=  sR_d8;
            sR_d10 <=  sR_d9;
            sR_d11 <=  sR_d10;
            sR_d12 <=  sR_d11;
            sR_d13 <=  sR_d12;
            sR_d14 <=  sR_d13;
            sR_d15 <=  sR_d14;
            sR_d16 <=  sR_d15;
            sR_d17 <=  sR_d16;
            sR_d18 <=  sR_d17;
            sR_d19 <=  sR_d18;
            sR_d20 <=  sR_d19;
            sR_d21 <=  sR_d20;
            sR_d22 <=  sR_d21;
            sR_d23 <=  sR_d22;
            sR_d24 <=  sR_d23;
            sR_d25 <=  sR_d24;
            absZ0_d1 <=  absZ0;
            absZ0_d2 <=  absZ0_d1;
            absZ0_d3 <=  absZ0_d2;
            absZ0_d4 <=  absZ0_d3;
            E_d1 <=  E;
            absE_d1 <=  absE;
            absE_d2 <=  absE_d1;
            absE_d3 <=  absE_d2;
            absE_d4 <=  absE_d3;
            absE_d5 <=  absE_d4;
            absE_d6 <=  absE_d5;
            absE_d7 <=  absE_d6;
            absE_d8 <=  absE_d7;
            absE_d9 <=  absE_d8;
            absE_d10 <=  absE_d9;
            absE_d11 <=  absE_d10;
            absE_d12 <=  absE_d11;
            absE_d13 <=  absE_d12;
            absE_d14 <=  absE_d13;
            EeqZero_d1 <=  EeqZero;
            EeqZero_d2 <=  EeqZero_d1;
            EeqZero_d3 <=  EeqZero_d2;
            lzo_d1 <=  lzo;
            lzo_d2 <=  lzo_d1;
            lzo_d3 <=  lzo_d2;
            lzo_d4 <=  lzo_d3;
            lzo_d5 <=  lzo_d4;
            lzo_d6 <=  lzo_d5;
            lzo_d7 <=  lzo_d6;
            lzo_d8 <=  lzo_d7;
            lzo_d9 <=  lzo_d8;
            lzo_d10 <=  lzo_d9;
            lzo_d11 <=  lzo_d10;
            lzo_d12 <=  lzo_d11;
            lzo_d13 <=  lzo_d12;
            lzo_d14 <=  lzo_d13;
            lzo_d15 <=  lzo_d14;
            lzo_d16 <=  lzo_d15;
            lzo_d17 <=  lzo_d16;
            lzo_d18 <=  lzo_d17;
            lzo_d19 <=  lzo_d18;
            lzo_d20 <=  lzo_d19;
            lzo_d21 <=  lzo_d20;
            shiftvalinR_d1 <=  shiftvalinR;
            shiftvalinR_d2 <=  shiftvalinR_d1;
            shiftvalinR_d3 <=  shiftvalinR_d2;
            shiftvalinR_d4 <=  shiftvalinR_d3;
            shiftvalinR_d5 <=  shiftvalinR_d4;
            shiftvalinR_d6 <=  shiftvalinR_d5;
            shiftvalinR_d7 <=  shiftvalinR_d6;
            shiftvalinR_d8 <=  shiftvalinR_d7;
            shiftvalinR_d9 <=  shiftvalinR_d8;
            shiftvalinR_d10 <=  shiftvalinR_d9;
            shiftvalinR_d11 <=  shiftvalinR_d10;
            shiftvalinR_d12 <=  shiftvalinR_d11;
            shiftvalinR_d13 <=  shiftvalinR_d12;
            shiftvalinR_d14 <=  shiftvalinR_d13;
            shiftvalinR_d15 <=  shiftvalinR_d14;
            shiftvalinR_d16 <=  shiftvalinR_d15;
            doRR_d1 <=  doRR;
            doRR_d2 <=  doRR_d1;
            doRR_d3 <=  doRR_d2;
            doRR_d4 <=  doRR_d3;
            doRR_d5 <=  doRR_d4;
            doRR_d6 <=  doRR_d5;
            small_d1 <=  small;
            small_d2 <=  small_d1;
            small_d3 <=  small_d2;
            small_d4 <=  small_d3;
            small_d5 <=  small_d4;
            small_d6 <=  small_d5;
            small_d7 <=  small_d6;
            small_d8 <=  small_d7;
            small_d9 <=  small_d8;
            small_d10 <=  small_d9;
            small_d11 <=  small_d10;
            small_d12 <=  small_d11;
            small_d13 <=  small_d12;
            small_d14 <=  small_d13;
            small_d15 <=  small_d14;
            small_d16 <=  small_d15;
            small_d17 <=  small_d16;
            small_d18 <=  small_d17;
            small_d19 <=  small_d18;
            small_d20 <=  small_d19;
            small_d21 <=  small_d20;
            small_absZ0_normd_d1 <=  small_absZ0_normd;
            small_absZ0_normd_d2 <=  small_absZ0_normd_d1;
            small_absZ0_normd_d3 <=  small_absZ0_normd_d2;
            small_absZ0_normd_d4 <=  small_absZ0_normd_d3;
            small_absZ0_normd_d5 <=  small_absZ0_normd_d4;
            small_absZ0_normd_d6 <=  small_absZ0_normd_d5;
            small_absZ0_normd_d7 <=  small_absZ0_normd_d6;
            small_absZ0_normd_d8 <=  small_absZ0_normd_d7;
            small_absZ0_normd_d9 <=  small_absZ0_normd_d8;
            small_absZ0_normd_d10 <=  small_absZ0_normd_d9;
            small_absZ0_normd_d11 <=  small_absZ0_normd_d10;
            small_absZ0_normd_d12 <=  small_absZ0_normd_d11;
            small_absZ0_normd_d13 <=  small_absZ0_normd_d12;
            small_absZ0_normd_d14 <=  small_absZ0_normd_d13;
            small_absZ0_normd_d15 <=  small_absZ0_normd_d14;
            small_absZ0_normd_d16 <=  small_absZ0_normd_d15;
            A0_d1 <=  A0;
            A0_d2 <=  A0_d1;
            A0_d3 <=  A0_d2;
            InvA0_d1 <=  InvA0;
            P0_d1 <=  P0;
            A1_d1 <=  A1;
            ZM1_d1 <=  ZM1;
            A2_d1 <=  A2;
            A2_d2 <=  A2_d1;
            A2_d3 <=  A2_d2;
            ZM2_d1 <=  ZM2;
            P2_d1 <=  P2;
            A3_d1 <=  A3;
            A3_d2 <=  A3_d1;
            A3_d3 <=  A3_d2;
            A3_d4 <=  A3_d3;
            ZM3_d1 <=  ZM3;
            Zfinal_d1 <=  Zfinal;
            Zfinal_d2 <=  Zfinal_d1;
            Zfinal_d3 <=  Zfinal_d2;
            Zfinal_d4 <=  Zfinal_d3;
            Z2o2_full_dummy_d1 <=  Z2o2_full_dummy;
            Z2o2_full_dummy_d2 <=  Z2o2_full_dummy_d1;
            Z2o2_full_dummy_d3 <=  Z2o2_full_dummy_d2;
            Z2o2_full_dummy_d4 <=  Z2o2_full_dummy_d3;
            Z2o2_full_dummy_d5 <=  Z2o2_full_dummy_d4;
            Z2o2_full_dummy_d6 <=  Z2o2_full_dummy_d5;
            Log1p_normal_d1 <=  Log1p_normal;
            S1_d1 <=  S1;
            S1_d2 <=  S1_d1;
            sopX1_d1 <=  sopX1;
            sopX1_d2 <=  sopX1_d1;
            S2_d1 <=  S2;
            S2_d2 <=  S2_d1;
            S2_d3 <=  S2_d2;
            sopX2_d1 <=  sopX2;
            sopX2_d2 <=  sopX2_d1;
            S3_d1 <=  S3;
            S3_d2 <=  S3_d1;
            S3_d3 <=  S3_d2;
            sopX3_d1 <=  sopX3;
            sopX3_d2 <=  sopX3_d1;
            S4_d1 <=  S4;
            Log_normal_normd_d1 <=  Log_normal_normd;
            Z2o2_small_d1 <=  Z2o2_small;
            Z_small_d1 <=  Z_small;
            Log_small_d1 <=  Log_small;
            E0_sub_d1 <=  E0_sub;
            ufl_d1 <=  ufl;
            ufl_d2 <=  ufl_d1;
            Log_small_normd_d1 <=  Log_small_normd;
         end if;
      end process;
   XExnSgn <=  X(wE+wF+2 downto wE+wF);
   FirstBit <=  X(wF-1);
   Y0 <= "1" & X(wF-1 downto 0) & "0" when FirstBit = '0' else "01" & X(wF-1 downto 0);
   Y0h <= Y0(wF downto 1);
   -- Sign of the result;
   sR <= '0'   when  (X(wE+wF-1 downto wF) = ('0' & (wE-2 downto 0 => '1')))  -- binade [1..2)
     else not X(wE+wF-1);                -- MSB of exponent
   absZ0 <=   Y0(wF-pfinal+1 downto 0)          when (sR='0') else
             ((wF-pfinal+1 downto 0 => '0') - Y0(wF-pfinal+1 downto 0));
   E <= (X(wE+wF-1 downto wF)) - ("0" & (wE-2 downto 1 => '1') & (not FirstBit));
   ----------------Synchro barrier, entering cycle 1----------------
   absE <= ((wE-1 downto 0 => '0') - E_d1)   when sR_d1 = '1' else E_d1;
   EeqZero <= '1' when E_d1=(wE-1 downto 0 => '0') else '0';
   ---------------- cycle 0----------------
   lzoc1: LZOC_52_F400_uid3  -- pipelineDepth=3 maxInDelay=5.92e-10
      port map ( clk  => clk,
                 rst  => rst,
                 I => Y0h,
                 O => lzo,
                 OZB => FirstBit);
   ---------------- cycle 3----------------
   ----------------Synchro barrier, entering cycle 4----------------
   pfinal_s <= "011011";
   shiftval <= ('0' & lzo_d1) - ('0' & pfinal_s); 
   shiftvalinL <= shiftval(4 downto 0);
   shiftvalinR <= shiftval(4 downto 0);
   doRR <= shiftval(log2wF); -- sign of the result
   small <= EeqZero_d3 and not(doRR);
   ---------------- cycle 4----------------
   -- The left shifter for the 'small' case
   small_lshift: LeftShifter_27_by_max_27_F400_uid6  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => small_absZ0_normd_full,
                 S => shiftvalinL,
                 X => absZ0_d4);
   ----------------Synchro barrier, entering cycle 5----------------
   small_absZ0_normd <= small_absZ0_normd_full(26 downto 0); -- get rid of leading zeroes
   ----------------Synchro barrier, entering cycle 0----------------
   ---------------- The range reduction box ---------------
   A0 <= X(51 downto 43);
   ----------------Synchro barrier, entering cycle 1----------------
   -- First inv table
   itO: InvTable_0_9_10  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => A0_d1,
                 Y => InvA0);
   ----------------Synchro barrier, entering cycle 2----------------
   P0 <= InvA0_d1 * Y0_d2;

   ----------------Synchro barrier, entering cycle 3----------------
   Z1 <= P0_d1(54 downto 0);

   A1 <= Z1(54 downto 48);
   B1 <= Z1(47 downto 0);
   ZM1 <= Z1;
   ----------------Synchro barrier, entering cycle 4----------------
   P1 <= A1_d1*ZM1_d1;
   ----------------Synchro barrier, entering cycle 5----------------
    -- delay at multiplier output is 0
   ---------------- cycle 3----------------
   Y1 <= "1" & (6 downto 0 => '0') & Z1;
   EiY1 <= Y1 & (0 downto 0 => '0')  when A1(6) = '1'
     else  "0" & Y1;
   addXIter1 <= "0" & B1 & (14 downto 0 => '0');
   addIter1_1: IntAdder_64_f400_uid11  -- pipelineDepth=1 maxInDelay=8.6e-11
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0',
                 R => EiYPB1,
                 X => addXIter1,
                 Y => EiY1);

   ----------------Synchro barrier, entering cycle 4----------------
   Pp1 <= (0 downto 0 => '1') & not(P1 & (0 downto 0 => '0'));
   addIter2_1: IntAdder_64_f400_uid18  -- pipelineDepth=1 maxInDelay=1.174e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '1',
                 R => Z2,
                 X => EiYPB1,
                 Y => Pp1);

   ----------------Synchro barrier, entering cycle 5----------------
 -- the critical path at the adder output = 1.496e-09

   A2 <= Z2(63 downto 56);
   B2 <= Z2(55 downto 0);
   ZM2 <= Z2(63 downto 7);
   ----------------Synchro barrier, entering cycle 6----------------
   P2 <= A2_d1*ZM2_d1;
   ----------------Synchro barrier, entering cycle 7----------------
    -- delay at multiplier output is 0
   ---------------- cycle 5----------------
   Y2 <= "1" & (12 downto 0 => '0') & Z2;
   EiY2 <= (4 downto 0 => '0') & Y2(77 downto 20);
   addXIter2 <= "0" & B2 & (5 downto 0 => '0');
   addIter1_2: IntAdder_63_f400_uid25  -- pipelineDepth=2 maxInDelay=1.496e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0',
                 R => EiYPB2,
                 X => addXIter2,
                 Y => EiY2);

   ----------------Synchro barrier, entering cycle 7----------------
   Pp2 <= (5 downto 0 => '1') & not(P2_d1(64 downto 8));
   addIter2_2: IntAdder_63_f400_uid32  -- pipelineDepth=1 maxInDelay=8.29e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '1',
                 R => Z3,
                 X => EiYPB2,
                 Y => Pp2);

   ----------------Synchro barrier, entering cycle 8----------------
 -- the critical path at the adder output = 1.151e-09

   A3 <= Z3(62 downto 55);
   B3 <= Z3(54 downto 0);
   ZM3 <= Z3(62 downto 20);
   ----------------Synchro barrier, entering cycle 9----------------
   P3 <= A3_d1*ZM3_d1;
   ----------------Synchro barrier, entering cycle 10----------------
    -- delay at multiplier output is 0
   ---------------- cycle 8----------------
   Y3 <= "1" & (19 downto 0 => '0') & Z3;
   EiY3 <= (11 downto 0 => '0') & Y3(83 downto 40);
   addXIter3 <= "0" & B3;
   addIter1_3: IntAdder_56_f400_uid39  -- pipelineDepth=1 maxInDelay=1.151e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0',
                 R => EiYPB3,
                 X => addXIter3,
                 Y => EiY3);

   ----------------Synchro barrier, entering cycle 9----------------
   Pp3 <= (12 downto 0 => '1') & not(P3(50 downto 8));
   addIter2_3: IntAdder_56_f400_uid46  -- pipelineDepth=1 maxInDelay=1.289e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '1',
                 R => Z4,
                 X => EiYPB3,
                 Y => Pp3);

   ----------------Synchro barrier, entering cycle 10----------------
 -- the critical path at the adder output = 1.427e-09
   Zfinal <= Z4;
   --  Synchro between RR box and case almost 1
   squarerIn <= Zfinal(sfinal-1 downto sfinal-31) when doRR_d6='1'
                    else (small_absZ0_normd_d5 & (3 downto 0 => '0'));  
   squarer: IntSquarer_31_uid53  -- pipelineDepth=4 maxInDelay=2.39372e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => Z2o2_full,
                 X => squarerIn);
   ----------------Synchro barrier, entering cycle 14----------------
   Z2o2_full_dummy <= Z2o2_full;
   Z2o2_normal <= Z2o2_full_dummy (61  downto 34);
   addFinalLog1pY <= (pfinal downto 0  => '1') & not(Z2o2_normal);
   addFinalLog1p_normalAdder: IntAdder_56_f400_uid56  -- pipelineDepth=1 maxInDelay=1.31672e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '1',
                 R => Log1p_normal,
                 X => Zfinal_d4,
                 Y => addFinalLog1pY);
   ----------------Synchro barrier, entering cycle 15----------------

   -- Now the log tables, as late as possible
   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   ----------------Synchro barrier, entering cycle 3----------------
   ----------------Synchro barrier, entering cycle 4----------------
   ----------------Synchro barrier, entering cycle 5----------------
   ----------------Synchro barrier, entering cycle 6----------------
   ----------------Synchro barrier, entering cycle 7----------------
   ----------------Synchro barrier, entering cycle 8----------------
   ----------------Synchro barrier, entering cycle 9----------------
   ----------------Synchro barrier, entering cycle 10----------------
   ----------------Synchro barrier, entering cycle 11----------------
   ----------------Synchro barrier, entering cycle 12----------------
   ----------------Synchro barrier, entering cycle 3----------------
   -- First log table
   ltO: LogTable_0_9_83  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => A0_d3,
                 Y => L0);
   ----------------Synchro barrier, entering cycle 4----------------
   S1 <= L0;
   lt1: LogTable_1_7_76  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => A1_d1,
                 Y => L1);
   sopX1 <= ((82 downto 76 => '0') & L1);
   ----------------Synchro barrier, entering cycle 5----------------
   ----------------Synchro barrier, entering cycle 6----------------
   adderS1: IntAdder_83_f400_uid85  -- pipelineDepth=1 maxInDelay=4.36e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0' ,
                 R => S2,
                 X => S1_d2,
                 Y => sopX1_d2);

   ----------------Synchro barrier, entering cycle 7----------------
   ----------------Synchro barrier, entering cycle 8----------------
   lt2: LogTable_2_8_70  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => A2_d3,
                 Y => L2);
   sopX2 <= ((82 downto 70 => '0') & L2);
   ----------------Synchro barrier, entering cycle 9----------------
   ----------------Synchro barrier, entering cycle 10----------------
   adderS2: IntAdder_83_f400_uid94  -- pipelineDepth=1 maxInDelay=4.36e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0' ,
                 R => S3,
                 X => S2_d3,
                 Y => sopX2_d2);

   ----------------Synchro barrier, entering cycle 11----------------
   ----------------Synchro barrier, entering cycle 12----------------
   lt3: LogTable_3_8_63  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => A3_d4,
                 Y => L3);
   sopX3 <= ((82 downto 63 => '0') & L3);
   ----------------Synchro barrier, entering cycle 13----------------
   ----------------Synchro barrier, entering cycle 14----------------
   adderS3: IntAdder_83_f400_uid103  -- pipelineDepth=1 maxInDelay=4.36e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0' ,
                 R => S4,
                 X => S3_d3,
                 Y => sopX3_d2);

   ----------------Synchro barrier, entering cycle 15----------------
   ----------------Synchro barrier, entering cycle 16----------------
   almostLog <= S4_d1;
   adderLogF_normalY <= ((targetprec-1 downto sfinal => '0') & Log1p_normal_d1);
   adderLogF_normal: IntAdder_83_f400_uid110  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => LogF_normal,
                 X => almostLog,
                 Y => adderLogF_normalY);
   ----------------Synchro barrier, entering cycle 17----------------
   ----------------Synchro barrier, entering cycle 15----------------
   Log2KCM: IntIntKCM_11_49946518145322874_unsigned  -- pipelineDepth=2 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => absELog2,
                 X => absE_d14);
   ----------------Synchro barrier, entering cycle 17----------------
   absELog2_pad <=   absELog2 & (targetprec-wF-g-1 downto 0 => '0');       
   LogF_normal_pad <= (wE-1  downto 0 => LogF_normal(targetprec-1))  & LogF_normal;
   lnaddX <= absELog2_pad;
   lnaddY <= LogF_normal_pad when sR_d17='0' else not(LogF_normal_pad); 
   lnadder: IntAdder_94_f400_uid130  -- pipelineDepth=3 maxInDelay=1.697e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => sR_d17,
                 R => Log_normal,
                 X => lnaddX,
                 Y => lnaddY);

   ----------------Synchro barrier, entering cycle 20----------------
   final_norm: LZCShifter_94_to_83_counting_128_F400_uid137  -- pipelineDepth=4 maxInDelay=1.22772e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Count => E_normal,
                 I => Log_normal,
                 O => Log_normal_normd);
   Z2o2_small_bs <= Z2o2_full_dummy_d6(61 downto 31);
   ao_rshift: RightShifter_31_by_max_30_F400_uid140  -- pipelineDepth=1 maxInDelay=5.3072e-10
      port map ( clk  => clk,
                 rst  => rst,
                 R => Z2o2_small_s,
                 S => shiftvalinR_d16,
                 X => Z2o2_small_bs);
   ---------------- cycle 21----------------
   -- output delay at shifter output is 1.98576e-09
     -- send the MSB to position pfinal
   Z2o2_small <=  (pfinal-1 downto 0  => '0') & Z2o2_small_s(60 downto 30);
   -- mantissa will be either Y0-z^2/2  or  -Y0+z^2/2,  depending on sR  
   Z_small <= small_absZ0_normd_d16 & (30 downto 0 => '0');
   ----------------Synchro barrier, entering cycle 22----------------
   Log_smallY <= Z2o2_small_d1 when sR_d22='1' else not(Z2o2_small_d1);
   nsRCin <= not ( sR_d22 );
   log_small_adder: IntAdder_58_f400_uid143  -- pipelineDepth=1 maxInDelay=4.4472e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => nsRCin,
                 R => Log_small,
                 X => Z_small_d1,
                 Y => Log_smallY);

   ----------------Synchro barrier, entering cycle 23----------------
 -- critical path here is 1.036e-09
   -- Possibly subtract 1 or 2 to the exponent, depending on the LZC of Log_small
   E0_sub <=   "11" when Log_small(wF+g+1) = '1'
          else "10" when Log_small(wF+g+1 downto wF+g) = "01"
          else "01" ;
   -- The smallest log will be log(1+2^{-wF}) \approx 2^{-wF}  = 2^-52
   -- The smallest representable number is 2^{1-2^(wE-1)} = 2^-1023
   -- No underflow possible
   ufl <= '0';
   ----------------Synchro barrier, entering cycle 24----------------
   E_small <=  ("0" & (wE-2 downto 2 => '1') & E0_sub_d1)  -  ((wE-1 downto 6 => '0') & lzo_d21) ;
   Log_small_normd <= Log_small_d1(wF+g+1 downto 2) when Log_small_d1(wF+g+1)='1'
           else Log_small_d1(wF+g downto 1)  when Log_small_d1(wF+g)='1'  -- remove the first zero
           else Log_small_d1(wF+g-1 downto 0)  ; -- remove two zeroes (extremely rare, 001000000 only)
   E0offset <= "10000001001"; -- E0 + wE 
   ER <= E_small(10 downto 0) when small_d20='1'
      else E0offset - ((10 downto 7 => '0') & E_normal);
   ---------------- cycle 24----------------
   Log_g <=  Log_small_normd(wF+g-2 downto 0) & "0" when small_d20='1'           -- remove implicit 1
      else Log_normal_normd(targetprec-2 downto targetprec-wF-g-1 );  -- remove implicit 1
   round <= Log_g(g-1) ; -- sticky is always 1 for a transcendental function 
   -- if round leads to a change of binade, the carry propagation magically updates both mantissa and exponent
   fraX <= (ER & Log_g(wF+g-1 downto g)) ; 
   fraY <= ((wE+wF-1 downto 1 => '0') & round); 
   finalRoundAdder: IntAdder_63_f400_uid150  -- pipelineDepth=1 maxInDelay=6.1672e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => EFR,
                 X => fraX,
                 Y => fraY);
   ----------------Synchro barrier, entering cycle 25----------------
   R(wE+wF+2 downto wE+wF) <= "110" when ((XExnSgn_d25(2) and (XExnSgn_d25(1) or XExnSgn_d25(0))) or (XExnSgn_d25(1) and XExnSgn_d25(0))) = '1' else
                              "101" when XExnSgn_d25(2 downto 1) = "00"  else
                              "100" when XExnSgn_d25(2 downto 1) = "10"  else
                              "00" & sR_d25 when (((Log_normal_normd_d1(targetprec-1)='0') and (small_d21='0')) or ( (Log_small_normd_d1 (wF+g-1)='0') and (small_d21='1'))) or (ufl_d2 = '1') else
                               "01" & sR_d25;
   R(wE+wF-1 downto 0) <=  EFR;
end architecture;

