--------------------------------------------------------------------------------
--                              Compressor_23_3
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Popa, Illyes Kinga, 2012
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity Compressor_23_3 is
   port ( X0 : in  std_logic_vector(2 downto 0);
          X1 : in  std_logic_vector(1 downto 0);
          R : out  std_logic_vector(2 downto 0)   );
end entity;

architecture arch of Compressor_23_3 is
signal X :  std_logic_vector(4 downto 0);
begin
   X <=X1 & X0 ;
   with X select R <= 
      "000" when "00000", 
      "001" when "00001", 
      "001" when "00010", 
      "010" when "00011", 
      "001" when "00100", 
      "010" when "00101", 
      "010" when "00110", 
      "011" when "00111", 
      "010" when "01000", 
      "011" when "01001", 
      "011" when "01010", 
      "100" when "01011", 
      "011" when "01100", 
      "100" when "01101", 
      "100" when "01110", 
      "101" when "01111", 
      "010" when "10000", 
      "011" when "10001", 
      "011" when "10010", 
      "100" when "10011", 
      "011" when "10100", 
      "100" when "10101", 
      "100" when "10110", 
      "101" when "10111", 
      "100" when "11000", 
      "101" when "11001", 
      "101" when "11010", 
      "110" when "11011", 
      "101" when "11100", 
      "110" when "11101", 
      "110" when "11110", 
      "111" when "11111", 
      "---" when others;

end architecture;

--------------------------------------------------------------------------------
--                              Compressor_13_3
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Popa, Illyes Kinga, 2012
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity Compressor_13_3 is
   port ( X0 : in  std_logic_vector(2 downto 0);
          X1 : in  std_logic_vector(0 downto 0);
          R : out  std_logic_vector(2 downto 0)   );
end entity;

architecture arch of Compressor_13_3 is
signal X :  std_logic_vector(3 downto 0);
begin
   X <=X1 & X0 ;
   with X select R <= 
      "000" when "0000", 
      "001" when "0001", 
      "001" when "0010", 
      "010" when "0011", 
      "001" when "0100", 
      "010" when "0101", 
      "010" when "0110", 
      "011" when "0111", 
      "010" when "1000", 
      "011" when "1001", 
      "011" when "1010", 
      "100" when "1011", 
      "011" when "1100", 
      "100" when "1101", 
      "100" when "1110", 
      "101" when "1111", 
      "---" when others;

end architecture;

--------------------------------------------------------------------------------
--                               Compressor_3_2
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Popa, Illyes Kinga, 2012
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity Compressor_3_2 is
   port ( X0 : in  std_logic_vector(2 downto 0);
          R : out  std_logic_vector(1 downto 0)   );
end entity;

architecture arch of Compressor_3_2 is
signal X :  std_logic_vector(2 downto 0);
begin
   X <=X0 ;
   with X select R <= 
      "00" when "000", 
      "01" when "001", 
      "01" when "010", 
      "10" when "011", 
      "01" when "100", 
      "10" when "101", 
      "10" when "110", 
      "11" when "111", 
      "--" when others;

end architecture;

--------------------------------------------------------------------------------
--                              Compressor_14_3
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Popa, Illyes Kinga, 2012
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity Compressor_14_3 is
   port ( X0 : in  std_logic_vector(3 downto 0);
          X1 : in  std_logic_vector(0 downto 0);
          R : out  std_logic_vector(2 downto 0)   );
end entity;

architecture arch of Compressor_14_3 is
signal X :  std_logic_vector(4 downto 0);
begin
   X <=X1 & X0 ;
   with X select R <= 
      "000" when "00000", 
      "001" when "00001", 
      "001" when "00010", 
      "010" when "00011", 
      "001" when "00100", 
      "010" when "00101", 
      "010" when "00110", 
      "011" when "00111", 
      "001" when "01000", 
      "010" when "01001", 
      "010" when "01010", 
      "011" when "01011", 
      "010" when "01100", 
      "011" when "01101", 
      "011" when "01110", 
      "100" when "01111", 
      "010" when "10000", 
      "011" when "10001", 
      "011" when "10010", 
      "100" when "10011", 
      "011" when "10100", 
      "100" when "10101", 
      "100" when "10110", 
      "101" when "10111", 
      "011" when "11000", 
      "100" when "11001", 
      "100" when "11010", 
      "101" when "11011", 
      "100" when "11100", 
      "101" when "11101", 
      "101" when "11110", 
      "110" when "11111", 
      "---" when others;

end architecture;

--------------------------------------------------------------------------------
--                     LeftShifter_53_by_max_66_F400_uid3
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 3 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LeftShifter_53_by_max_66_F400_uid3 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(52 downto 0);
          S : in  std_logic_vector(6 downto 0);
          R : out  std_logic_vector(118 downto 0)   );
end entity;

architecture arch of LeftShifter_53_by_max_66_F400_uid3 is
signal level0 :  std_logic_vector(52 downto 0);
signal ps, ps_d1, ps_d2, ps_d3 :  std_logic_vector(6 downto 0);
signal level1 :  std_logic_vector(53 downto 0);
signal level2, level2_d1 :  std_logic_vector(55 downto 0);
signal level3 :  std_logic_vector(59 downto 0);
signal level4, level4_d1 :  std_logic_vector(67 downto 0);
signal level5 :  std_logic_vector(83 downto 0);
signal level6, level6_d1 :  std_logic_vector(115 downto 0);
signal level7 :  std_logic_vector(179 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            ps_d1 <=  ps;
            ps_d2 <=  ps_d1;
            ps_d3 <=  ps_d2;
            level2_d1 <=  level2;
            level4_d1 <=  level4;
            level6_d1 <=  level6;
         end if;
      end process;
   level0<= X;
   ps<= S;
   level1<= level0 & (0 downto 0 => '0') when ps(0)= '1' else     (0 downto 0 => '0') & level0;
   level2<= level1 & (1 downto 0 => '0') when ps(1)= '1' else     (1 downto 0 => '0') & level1;
   ----------------Synchro barrier, entering cycle 1----------------
   level3<= level2_d1 & (3 downto 0 => '0') when ps_d1(2)= '1' else     (3 downto 0 => '0') & level2_d1;
   level4<= level3 & (7 downto 0 => '0') when ps_d1(3)= '1' else     (7 downto 0 => '0') & level3;
   ----------------Synchro barrier, entering cycle 2----------------
   level5<= level4_d1 & (15 downto 0 => '0') when ps_d2(4)= '1' else     (15 downto 0 => '0') & level4_d1;
   level6<= level5 & (31 downto 0 => '0') when ps_d2(5)= '1' else     (31 downto 0 => '0') & level5;
   ----------------Synchro barrier, entering cycle 3----------------
   level7<= level6_d1 & (63 downto 0 => '0') when ps_d3(6)= '1' else     (63 downto 0 => '0') & level6_d1;
   R <= level7(118 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                 FixRealKCM_M3_9_0_1_log_2_unsigned_Table_1
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity FixRealKCM_M3_9_0_1_log_2_unsigned_Table_1 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(4 downto 0);
          Y : out  std_logic_vector(14 downto 0)   );
end entity;

architecture arch of FixRealKCM_M3_9_0_1_log_2_unsigned_Table_1 is
begin
  with X select  Y <= 
   "000000000000000" when "00000",
   "000001011100011" when "00001",
   "000010111000101" when "00010",
   "000100010101000" when "00011",
   "000101110001011" when "00100",
   "000111001101101" when "00101",
   "001000101010000" when "00110",
   "001010000110011" when "00111",
   "001011100010101" when "01000",
   "001100111111000" when "01001",
   "001110011011011" when "01010",
   "001111110111101" when "01011",
   "010001010100000" when "01100",
   "010010110000011" when "01101",
   "010100001100101" when "01110",
   "010101101001000" when "01111",
   "010111000101011" when "10000",
   "011000100001101" when "10001",
   "011001111110000" when "10010",
   "011011011010011" when "10011",
   "011100110110101" when "10100",
   "011110010011000" when "10101",
   "011111101111011" when "10110",
   "100001001011101" when "10111",
   "100010101000000" when "11000",
   "100100000100010" when "11001",
   "100101100000101" when "11010",
   "100110111101000" when "11011",
   "101000011001010" when "11100",
   "101001110101101" when "11101",
   "101011010010000" when "11110",
   "101100101110010" when "11111",
   "---------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                 FixRealKCM_M3_9_0_1_log_2_unsigned_Table_0
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity FixRealKCM_M3_9_0_1_log_2_unsigned_Table_0 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(7 downto 0);
          Y : out  std_logic_vector(9 downto 0)   );
end entity;

architecture arch of FixRealKCM_M3_9_0_1_log_2_unsigned_Table_0 is
begin
  with X select  Y <= 
   "0000000000" when "00000000",
   "0000000011" when "00000001",
   "0000000110" when "00000010",
   "0000001001" when "00000011",
   "0000001100" when "00000100",
   "0000001110" when "00000101",
   "0000010001" when "00000110",
   "0000010100" when "00000111",
   "0000010111" when "00001000",
   "0000011010" when "00001001",
   "0000011101" when "00001010",
   "0000100000" when "00001011",
   "0000100011" when "00001100",
   "0000100110" when "00001101",
   "0000101000" when "00001110",
   "0000101011" when "00001111",
   "0000101110" when "00010000",
   "0000110001" when "00010001",
   "0000110100" when "00010010",
   "0000110111" when "00010011",
   "0000111010" when "00010100",
   "0000111101" when "00010101",
   "0000111111" when "00010110",
   "0001000010" when "00010111",
   "0001000101" when "00011000",
   "0001001000" when "00011001",
   "0001001011" when "00011010",
   "0001001110" when "00011011",
   "0001010001" when "00011100",
   "0001010100" when "00011101",
   "0001010111" when "00011110",
   "0001011001" when "00011111",
   "0001011100" when "00100000",
   "0001011111" when "00100001",
   "0001100010" when "00100010",
   "0001100101" when "00100011",
   "0001101000" when "00100100",
   "0001101011" when "00100101",
   "0001101110" when "00100110",
   "0001110001" when "00100111",
   "0001110011" when "00101000",
   "0001110110" when "00101001",
   "0001111001" when "00101010",
   "0001111100" when "00101011",
   "0001111111" when "00101100",
   "0010000010" when "00101101",
   "0010000101" when "00101110",
   "0010001000" when "00101111",
   "0010001010" when "00110000",
   "0010001101" when "00110001",
   "0010010000" when "00110010",
   "0010010011" when "00110011",
   "0010010110" when "00110100",
   "0010011001" when "00110101",
   "0010011100" when "00110110",
   "0010011111" when "00110111",
   "0010100010" when "00111000",
   "0010100100" when "00111001",
   "0010100111" when "00111010",
   "0010101010" when "00111011",
   "0010101101" when "00111100",
   "0010110000" when "00111101",
   "0010110011" when "00111110",
   "0010110110" when "00111111",
   "0010111001" when "01000000",
   "0010111100" when "01000001",
   "0010111110" when "01000010",
   "0011000001" when "01000011",
   "0011000100" when "01000100",
   "0011000111" when "01000101",
   "0011001010" when "01000110",
   "0011001101" when "01000111",
   "0011010000" when "01001000",
   "0011010011" when "01001001",
   "0011010110" when "01001010",
   "0011011000" when "01001011",
   "0011011011" when "01001100",
   "0011011110" when "01001101",
   "0011100001" when "01001110",
   "0011100100" when "01001111",
   "0011100111" when "01010000",
   "0011101010" when "01010001",
   "0011101101" when "01010010",
   "0011101111" when "01010011",
   "0011110010" when "01010100",
   "0011110101" when "01010101",
   "0011111000" when "01010110",
   "0011111011" when "01010111",
   "0011111110" when "01011000",
   "0100000001" when "01011001",
   "0100000100" when "01011010",
   "0100000111" when "01011011",
   "0100001001" when "01011100",
   "0100001100" when "01011101",
   "0100001111" when "01011110",
   "0100010010" when "01011111",
   "0100010101" when "01100000",
   "0100011000" when "01100001",
   "0100011011" when "01100010",
   "0100011110" when "01100011",
   "0100100001" when "01100100",
   "0100100011" when "01100101",
   "0100100110" when "01100110",
   "0100101001" when "01100111",
   "0100101100" when "01101000",
   "0100101111" when "01101001",
   "0100110010" when "01101010",
   "0100110101" when "01101011",
   "0100111000" when "01101100",
   "0100111011" when "01101101",
   "0100111101" when "01101110",
   "0101000000" when "01101111",
   "0101000011" when "01110000",
   "0101000110" when "01110001",
   "0101001001" when "01110010",
   "0101001100" when "01110011",
   "0101001111" when "01110100",
   "0101010010" when "01110101",
   "0101010100" when "01110110",
   "0101010111" when "01110111",
   "0101011010" when "01111000",
   "0101011101" when "01111001",
   "0101100000" when "01111010",
   "0101100011" when "01111011",
   "0101100110" when "01111100",
   "0101101001" when "01111101",
   "0101101100" when "01111110",
   "0101101110" when "01111111",
   "0101110001" when "10000000",
   "0101110100" when "10000001",
   "0101110111" when "10000010",
   "0101111010" when "10000011",
   "0101111101" when "10000100",
   "0110000000" when "10000101",
   "0110000011" when "10000110",
   "0110000110" when "10000111",
   "0110001000" when "10001000",
   "0110001011" when "10001001",
   "0110001110" when "10001010",
   "0110010001" when "10001011",
   "0110010100" when "10001100",
   "0110010111" when "10001101",
   "0110011010" when "10001110",
   "0110011101" when "10001111",
   "0110011111" when "10010000",
   "0110100010" when "10010001",
   "0110100101" when "10010010",
   "0110101000" when "10010011",
   "0110101011" when "10010100",
   "0110101110" when "10010101",
   "0110110001" when "10010110",
   "0110110100" when "10010111",
   "0110110111" when "10011000",
   "0110111001" when "10011001",
   "0110111100" when "10011010",
   "0110111111" when "10011011",
   "0111000010" when "10011100",
   "0111000101" when "10011101",
   "0111001000" when "10011110",
   "0111001011" when "10011111",
   "0111001110" when "10100000",
   "0111010001" when "10100001",
   "0111010011" when "10100010",
   "0111010110" when "10100011",
   "0111011001" when "10100100",
   "0111011100" when "10100101",
   "0111011111" when "10100110",
   "0111100010" when "10100111",
   "0111100101" when "10101000",
   "0111101000" when "10101001",
   "0111101011" when "10101010",
   "0111101101" when "10101011",
   "0111110000" when "10101100",
   "0111110011" when "10101101",
   "0111110110" when "10101110",
   "0111111001" when "10101111",
   "0111111100" when "10110000",
   "0111111111" when "10110001",
   "1000000010" when "10110010",
   "1000000100" when "10110011",
   "1000000111" when "10110100",
   "1000001010" when "10110101",
   "1000001101" when "10110110",
   "1000010000" when "10110111",
   "1000010011" when "10111000",
   "1000010110" when "10111001",
   "1000011001" when "10111010",
   "1000011100" when "10111011",
   "1000011110" when "10111100",
   "1000100001" when "10111101",
   "1000100100" when "10111110",
   "1000100111" when "10111111",
   "1000101010" when "11000000",
   "1000101101" when "11000001",
   "1000110000" when "11000010",
   "1000110011" when "11000011",
   "1000110110" when "11000100",
   "1000111000" when "11000101",
   "1000111011" when "11000110",
   "1000111110" when "11000111",
   "1001000001" when "11001000",
   "1001000100" when "11001001",
   "1001000111" when "11001010",
   "1001001010" when "11001011",
   "1001001101" when "11001100",
   "1001010000" when "11001101",
   "1001010010" when "11001110",
   "1001010101" when "11001111",
   "1001011000" when "11010000",
   "1001011011" when "11010001",
   "1001011110" when "11010010",
   "1001100001" when "11010011",
   "1001100100" when "11010100",
   "1001100111" when "11010101",
   "1001101001" when "11010110",
   "1001101100" when "11010111",
   "1001101111" when "11011000",
   "1001110010" when "11011001",
   "1001110101" when "11011010",
   "1001111000" when "11011011",
   "1001111011" when "11011100",
   "1001111110" when "11011101",
   "1010000001" when "11011110",
   "1010000011" when "11011111",
   "1010000110" when "11100000",
   "1010001001" when "11100001",
   "1010001100" when "11100010",
   "1010001111" when "11100011",
   "1010010010" when "11100100",
   "1010010101" when "11100101",
   "1010011000" when "11100110",
   "1010011011" when "11100111",
   "1010011101" when "11101000",
   "1010100000" when "11101001",
   "1010100011" when "11101010",
   "1010100110" when "11101011",
   "1010101001" when "11101100",
   "1010101100" when "11101101",
   "1010101111" when "11101110",
   "1010110010" when "11101111",
   "1010110100" when "11110000",
   "1010110111" when "11110001",
   "1010111010" when "11110010",
   "1010111101" when "11110011",
   "1011000000" when "11110100",
   "1011000011" when "11110101",
   "1011000110" when "11110110",
   "1011001001" when "11110111",
   "1011001100" when "11111000",
   "1011001110" when "11111001",
   "1011010001" when "11111010",
   "1011010100" when "11111011",
   "1011010111" when "11111100",
   "1011011010" when "11111101",
   "1011011101" when "11111110",
   "1011100000" when "11111111",
   "----------" when others;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_16_f400_uid23
--                     (IntAdderClassical_16_f400_uid25)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_16_f400_uid23 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(15 downto 0);
          Y : in  std_logic_vector(15 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(15 downto 0)   );
end entity;

architecture arch of IntAdder_16_f400_uid23 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                     FixRealKCM_M3_9_0_1_log_2_unsigned
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team 2015
--------------------------------------------------------------------------------
-- Pipeline depth: 2 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library std;
use std.textio.all;
library work;

entity FixRealKCM_M3_9_0_1_log_2_unsigned is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(12 downto 0);
          R : out  std_logic_vector(10 downto 0)   );
end entity;

architecture arch of FixRealKCM_M3_9_0_1_log_2_unsigned is
   component Compressor_13_3 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             X1 : in  std_logic_vector(0 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component Compressor_23_3 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             X1 : in  std_logic_vector(1 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component FixRealKCM_M3_9_0_1_log_2_unsigned_Table_0 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(7 downto 0);
             Y : out  std_logic_vector(9 downto 0)   );
   end component;

   component FixRealKCM_M3_9_0_1_log_2_unsigned_Table_1 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(4 downto 0);
             Y : out  std_logic_vector(14 downto 0)   );
   end component;

   component IntAdder_16_f400_uid23 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(15 downto 0);
             Y : in  std_logic_vector(15 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(15 downto 0)   );
   end component;

signal d1_kcmMult_5 :  std_logic_vector(4 downto 0);
signal d0_kcmMult_5 :  std_logic_vector(7 downto 0);
signal pp0_kcmMult_5 :  std_logic_vector(9 downto 0);
signal heap_bh6_w0_0, heap_bh6_w0_0_d1 :  std_logic;
signal heap_bh6_w1_0, heap_bh6_w1_0_d1 :  std_logic;
signal heap_bh6_w2_0, heap_bh6_w2_0_d1 :  std_logic;
signal heap_bh6_w3_0 :  std_logic;
signal heap_bh6_w4_0 :  std_logic;
signal heap_bh6_w5_0 :  std_logic;
signal heap_bh6_w6_0 :  std_logic;
signal heap_bh6_w7_0, heap_bh6_w7_0_d1 :  std_logic;
signal heap_bh6_w8_0, heap_bh6_w8_0_d1 :  std_logic;
signal heap_bh6_w9_0, heap_bh6_w9_0_d1 :  std_logic;
signal pp1_kcmMult_5 :  std_logic_vector(14 downto 0);
signal heap_bh6_w0_1, heap_bh6_w0_1_d1 :  std_logic;
signal heap_bh6_w1_1, heap_bh6_w1_1_d1 :  std_logic;
signal heap_bh6_w2_1, heap_bh6_w2_1_d1 :  std_logic;
signal heap_bh6_w3_1 :  std_logic;
signal heap_bh6_w4_1 :  std_logic;
signal heap_bh6_w5_1 :  std_logic;
signal heap_bh6_w6_1 :  std_logic;
signal heap_bh6_w7_1, heap_bh6_w7_1_d1 :  std_logic;
signal heap_bh6_w8_1, heap_bh6_w8_1_d1 :  std_logic;
signal heap_bh6_w9_1, heap_bh6_w9_1_d1 :  std_logic;
signal heap_bh6_w10_0, heap_bh6_w10_0_d1 :  std_logic;
signal heap_bh6_w11_0, heap_bh6_w11_0_d1 :  std_logic;
signal heap_bh6_w12_0, heap_bh6_w12_0_d1 :  std_logic;
signal heap_bh6_w13_0, heap_bh6_w13_0_d1 :  std_logic;
signal heap_bh6_w14_0, heap_bh6_w14_0_d1 :  std_logic;
signal heap_bh6_w3_2, heap_bh6_w3_2_d1 :  std_logic;
signal CompressorIn_bh6_0_0 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh6_0_1 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh6_0_0 :  std_logic_vector(2 downto 0);
signal heap_bh6_w3_3, heap_bh6_w3_3_d1 :  std_logic;
signal heap_bh6_w4_2, heap_bh6_w4_2_d1 :  std_logic;
signal heap_bh6_w5_2 :  std_logic;
signal CompressorIn_bh6_1_2 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh6_1_3 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh6_1_1 :  std_logic_vector(2 downto 0);
signal heap_bh6_w5_3, heap_bh6_w5_3_d1 :  std_logic;
signal heap_bh6_w6_2, heap_bh6_w6_2_d1 :  std_logic;
signal heap_bh6_w7_2, heap_bh6_w7_2_d1 :  std_logic;
signal CompressorIn_bh6_2_4 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh6_2_5 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh6_2_2 :  std_logic_vector(2 downto 0);
signal heap_bh6_w7_3 :  std_logic;
signal heap_bh6_w8_2 :  std_logic;
signal heap_bh6_w9_2 :  std_logic;
signal CompressorIn_bh6_3_6 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh6_3_7 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh6_3_3 :  std_logic_vector(2 downto 0);
signal heap_bh6_w9_3 :  std_logic;
signal heap_bh6_w10_1 :  std_logic;
signal heap_bh6_w11_1 :  std_logic;
signal finalAdderIn0_bh6 :  std_logic_vector(15 downto 0);
signal finalAdderIn1_bh6 :  std_logic_vector(15 downto 0);
signal finalAdderCin_bh6 :  std_logic;
signal finalAdderOut_bh6 :  std_logic_vector(15 downto 0);
signal CompressionResult6 :  std_logic_vector(15 downto 0);
signal OutRes :  std_logic_vector(14 downto 0);
signal X_d1 :  std_logic_vector(12 downto 0);
attribute rom_extract: string;
attribute rom_style: string;
attribute rom_extract of FixRealKCM_M3_9_0_1_log_2_unsigned_Table_0: component is "yes";
attribute rom_extract of FixRealKCM_M3_9_0_1_log_2_unsigned_Table_1: component is "yes";
attribute rom_style of FixRealKCM_M3_9_0_1_log_2_unsigned_Table_0: component is "distributed";
attribute rom_style of FixRealKCM_M3_9_0_1_log_2_unsigned_Table_1: component is "distributed";
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            heap_bh6_w0_0_d1 <=  heap_bh6_w0_0;
            heap_bh6_w1_0_d1 <=  heap_bh6_w1_0;
            heap_bh6_w2_0_d1 <=  heap_bh6_w2_0;
            heap_bh6_w7_0_d1 <=  heap_bh6_w7_0;
            heap_bh6_w8_0_d1 <=  heap_bh6_w8_0;
            heap_bh6_w9_0_d1 <=  heap_bh6_w9_0;
            heap_bh6_w0_1_d1 <=  heap_bh6_w0_1;
            heap_bh6_w1_1_d1 <=  heap_bh6_w1_1;
            heap_bh6_w2_1_d1 <=  heap_bh6_w2_1;
            heap_bh6_w7_1_d1 <=  heap_bh6_w7_1;
            heap_bh6_w8_1_d1 <=  heap_bh6_w8_1;
            heap_bh6_w9_1_d1 <=  heap_bh6_w9_1;
            heap_bh6_w10_0_d1 <=  heap_bh6_w10_0;
            heap_bh6_w11_0_d1 <=  heap_bh6_w11_0;
            heap_bh6_w12_0_d1 <=  heap_bh6_w12_0;
            heap_bh6_w13_0_d1 <=  heap_bh6_w13_0;
            heap_bh6_w14_0_d1 <=  heap_bh6_w14_0;
            heap_bh6_w3_2_d1 <=  heap_bh6_w3_2;
            heap_bh6_w3_3_d1 <=  heap_bh6_w3_3;
            heap_bh6_w4_2_d1 <=  heap_bh6_w4_2;
            heap_bh6_w5_3_d1 <=  heap_bh6_w5_3;
            heap_bh6_w6_2_d1 <=  heap_bh6_w6_2;
            heap_bh6_w7_2_d1 <=  heap_bh6_w7_2;
            X_d1 <=  X;
         end if;
      end process;
   ----------------Synchro barrier, entering cycle 1----------------
   d1_kcmMult_5 <= X_d1(12 downto 8);
   d0_kcmMult_5 <= X_d1(7 downto 0);
   ---------------- cycle 1----------------
   KCMTable_0_kcmMult_5: FixRealKCM_M3_9_0_1_log_2_unsigned_Table_0  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => d0_kcmMult_5,
                 Y => pp0_kcmMult_5);
   heap_bh6_w0_0 <= pp0_kcmMult_5(0); -- cycle= 1 cp= 1.72e-10
   heap_bh6_w1_0 <= pp0_kcmMult_5(1); -- cycle= 1 cp= 2.58e-10
   heap_bh6_w2_0 <= pp0_kcmMult_5(2); -- cycle= 1 cp= 3.44e-10
   heap_bh6_w3_0 <= pp0_kcmMult_5(3); -- cycle= 1 cp= 4.3e-10
   heap_bh6_w4_0 <= pp0_kcmMult_5(4); -- cycle= 1 cp= 5.16e-10
   heap_bh6_w5_0 <= pp0_kcmMult_5(5); -- cycle= 1 cp= 6.02e-10
   heap_bh6_w6_0 <= pp0_kcmMult_5(6); -- cycle= 1 cp= 6.88e-10
   heap_bh6_w7_0 <= pp0_kcmMult_5(7); -- cycle= 1 cp= 7.74e-10
   heap_bh6_w8_0 <= pp0_kcmMult_5(8); -- cycle= 1 cp= 8.6e-10
   heap_bh6_w9_0 <= pp0_kcmMult_5(9); -- cycle= 1 cp= 9.46e-10
   ---------------- cycle 1----------------
   KCMTable_1_kcmMult_5: FixRealKCM_M3_9_0_1_log_2_unsigned_Table_1  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => d1_kcmMult_5,
                 Y => pp1_kcmMult_5);
   heap_bh6_w0_1 <= pp1_kcmMult_5(0); -- cycle= 1 cp= 1.72e-10
   heap_bh6_w1_1 <= pp1_kcmMult_5(1); -- cycle= 1 cp= 2.58e-10
   heap_bh6_w2_1 <= pp1_kcmMult_5(2); -- cycle= 1 cp= 3.44e-10
   heap_bh6_w3_1 <= pp1_kcmMult_5(3); -- cycle= 1 cp= 4.3e-10
   heap_bh6_w4_1 <= pp1_kcmMult_5(4); -- cycle= 1 cp= 5.16e-10
   heap_bh6_w5_1 <= pp1_kcmMult_5(5); -- cycle= 1 cp= 6.02e-10
   heap_bh6_w6_1 <= pp1_kcmMult_5(6); -- cycle= 1 cp= 6.88e-10
   heap_bh6_w7_1 <= pp1_kcmMult_5(7); -- cycle= 1 cp= 7.74e-10
   heap_bh6_w8_1 <= pp1_kcmMult_5(8); -- cycle= 1 cp= 8.6e-10
   heap_bh6_w9_1 <= pp1_kcmMult_5(9); -- cycle= 1 cp= 9.46e-10
   heap_bh6_w10_0 <= pp1_kcmMult_5(10); -- cycle= 1 cp= 1.032e-09
   heap_bh6_w11_0 <= pp1_kcmMult_5(11); -- cycle= 1 cp= 1.118e-09
   heap_bh6_w12_0 <= pp1_kcmMult_5(12); -- cycle= 1 cp= 1.204e-09
   heap_bh6_w13_0 <= pp1_kcmMult_5(13); -- cycle= 1 cp= 1.29e-09
   heap_bh6_w14_0 <= pp1_kcmMult_5(14); -- cycle= 1 cp= 1.376e-09
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
   heap_bh6_w3_2 <= '1'; -- cycle= 0 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh6_0_0 <= heap_bh6_w3_2_d1 & heap_bh6_w3_1 & heap_bh6_w3_0;
   CompressorIn_bh6_0_1 <= heap_bh6_w4_1 & heap_bh6_w4_0;
   Compressor_bh6_0: Compressor_23_3
      port map ( R => CompressorOut_bh6_0_0   ,
                 X0 => CompressorIn_bh6_0_0,
                 X1 => CompressorIn_bh6_0_1);
   heap_bh6_w3_3 <= CompressorOut_bh6_0_0(0); -- cycle= 1 cp= 1.04672e-09
   heap_bh6_w4_2 <= CompressorOut_bh6_0_0(1); -- cycle= 1 cp= 1.04672e-09
   heap_bh6_w5_2 <= CompressorOut_bh6_0_0(2); -- cycle= 1 cp= 1.04672e-09

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh6_1_2 <= heap_bh6_w5_1 & heap_bh6_w5_0 & heap_bh6_w5_2;
   CompressorIn_bh6_1_3 <= heap_bh6_w6_1 & heap_bh6_w6_0;
   Compressor_bh6_1: Compressor_23_3
      port map ( R => CompressorOut_bh6_1_1   ,
                 X0 => CompressorIn_bh6_1_2,
                 X1 => CompressorIn_bh6_1_3);
   heap_bh6_w5_3 <= CompressorOut_bh6_1_1(0); -- cycle= 1 cp= 1.57744e-09
   heap_bh6_w6_2 <= CompressorOut_bh6_1_1(1); -- cycle= 1 cp= 1.57744e-09
   heap_bh6_w7_2 <= CompressorOut_bh6_1_1(2); -- cycle= 1 cp= 1.57744e-09

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh6_2_4 <= heap_bh6_w7_1_d1 & heap_bh6_w7_0_d1 & heap_bh6_w7_2_d1;
   CompressorIn_bh6_2_5 <= heap_bh6_w8_1_d1 & heap_bh6_w8_0_d1;
   Compressor_bh6_2: Compressor_23_3
      port map ( R => CompressorOut_bh6_2_2   ,
                 X0 => CompressorIn_bh6_2_4,
                 X1 => CompressorIn_bh6_2_5);
   heap_bh6_w7_3 <= CompressorOut_bh6_2_2(0); -- cycle= 2 cp= 0
   heap_bh6_w8_2 <= CompressorOut_bh6_2_2(1); -- cycle= 2 cp= 0
   heap_bh6_w9_2 <= CompressorOut_bh6_2_2(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh6_3_6 <= heap_bh6_w9_1_d1 & heap_bh6_w9_0_d1 & heap_bh6_w9_2;
   CompressorIn_bh6_3_7(0) <= heap_bh6_w10_0_d1;
   Compressor_bh6_3: Compressor_13_3
      port map ( R => CompressorOut_bh6_3_3   ,
                 X0 => CompressorIn_bh6_3_6,
                 X1 => CompressorIn_bh6_3_7);
   heap_bh6_w9_3 <= CompressorOut_bh6_3_3(0); -- cycle= 2 cp= 5.3072e-10
   heap_bh6_w10_1 <= CompressorOut_bh6_3_3(1); -- cycle= 2 cp= 5.3072e-10
   heap_bh6_w11_1 <= CompressorOut_bh6_3_3(2); -- cycle= 2 cp= 5.3072e-10
   ----------------Synchro barrier, entering cycle 2----------------
   finalAdderIn0_bh6 <= "0" & heap_bh6_w14_0_d1 & heap_bh6_w13_0_d1 & heap_bh6_w12_0_d1 & heap_bh6_w11_0_d1 & heap_bh6_w10_1 & heap_bh6_w9_3 & heap_bh6_w8_2 & heap_bh6_w7_3 & heap_bh6_w6_2_d1 & heap_bh6_w5_3_d1 & heap_bh6_w4_2_d1 & heap_bh6_w3_3_d1 & heap_bh6_w2_1_d1 & heap_bh6_w1_1_d1 & heap_bh6_w0_1_d1;
   finalAdderIn1_bh6 <= "0" & '0' & '0' & '0' & heap_bh6_w11_1 & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh6_w2_0_d1 & heap_bh6_w1_0_d1 & heap_bh6_w0_0_d1;
   finalAdderCin_bh6 <= '0';
   Adder_final6_0: IntAdder_16_f400_uid23  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh6,
                 R => finalAdderOut_bh6   ,
                 X => finalAdderIn0_bh6,
                 Y => finalAdderIn1_bh6);
   -- concatenate all the compressed chunks
   CompressionResult6 <= finalAdderOut_bh6;
   -- End of code generated by BitHeap::generateCompressorVHDL
OutRes <= CompressionResult6(14 downto 0);
   R <= OutRes(14 downto 4);
end architecture;

--------------------------------------------------------------------------------
--                 FixRealKCM_0_10_M56_log_2_unsigned_Table_1
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity FixRealKCM_0_10_M56_log_2_unsigned_Table_1 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(4 downto 0);
          Y : out  std_logic_vector(66 downto 0)   );
end entity;

architecture arch of FixRealKCM_0_10_M56_log_2_unsigned_Table_1 is
begin
  with X select  Y <= 
   "0000000000000000000000000000000000000000000000000000000000000000000" when "00000",
   "0000010110001011100100001011111110111110100011100111101111001101011" when "00001",
   "0000101100010111001000010111111101111101000111001111011110011010110" when "00010",
   "0001000010100010101100100011111100111011101010110111001101101000001" when "00011",
   "0001011000101110010000101111111011111010001110011110111100110101100" when "00100",
   "0001101110111001110100111011111010111000110010000110101100000010111" when "00101",
   "0010000101000101011001000111111001110111010101101110011011010000010" when "00110",
   "0010011011010000111101010011111000110101111001010110001010011101101" when "00111",
   "0010110001011100100001011111110111110100011100111101111001101011000" when "01000",
   "0011000111101000000101101011110110110011000000100101101000111000011" when "01001",
   "0011011101110011101001110111110101110001100100001101011000000101101" when "01010",
   "0011110011111111001110000011110100110000000111110101000111010011000" when "01011",
   "0100001010001010110010001111110011101110101011011100110110100000011" when "01100",
   "0100100000010110010110011011110010101101001111000100100101101101110" when "01101",
   "0100110110100001111010100111110001101011110010101100010100111011001" when "01110",
   "0101001100101101011110110011110000101010010110010100000100001000100" when "01111",
   "0101100010111001000010111111101111101000111001111011110011010101111" when "10000",
   "0101111001000100100111001011101110100111011101100011100010100011010" when "10001",
   "0110001111010000001011010111101101100110000001001011010001110000101" when "10010",
   "0110100101011011101111100011101100100100100100110011000000111110000" when "10011",
   "0110111011100111010011101111101011100011001000011010110000001011011" when "10100",
   "0111010001110010110111111011101010100001101100000010011111011000110" when "10101",
   "0111100111111110011100000111101001100000001111101010001110100110001" when "10110",
   "0111111110001010000000010011101000011110110011010001111101110011100" when "10111",
   "1000010100010101100100011111100111011101010110111001101101000000111" when "11000",
   "1000101010100001001000101011100110011011111010100001011100001110010" when "11001",
   "1001000000101100101100110111100101011010011110001001001011011011101" when "11010",
   "1001010110111000010001000011100100011001000001110000111010101001000" when "11011",
   "1001101101000011110101001111100011010111100101011000101001110110011" when "11100",
   "1010000011001111011001011011100010010110001001000000011001000011101" when "11101",
   "1010011001011010111101100111100001010100101100101000001000010001000" when "11110",
   "1010101111100110100001110011100000010011010000001111110111011110011" when "11111",
   "-------------------------------------------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                 FixRealKCM_0_10_M56_log_2_unsigned_Table_0
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity FixRealKCM_0_10_M56_log_2_unsigned_Table_0 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(61 downto 0)   );
end entity;

architecture arch of FixRealKCM_0_10_M56_log_2_unsigned_Table_0 is
begin
  with X select  Y <= 
   "00000000000000000000000000000000000000000000000000000000000000" when "000000",
   "00000010110001011100100001011111110111110100011100111101111010" when "000001",
   "00000101100010111001000010111111101111101000111001111011110011" when "000010",
   "00001000010100010101100100011111100111011101010110111001101101" when "000011",
   "00001011000101110010000101111111011111010001110011110111100111" when "000100",
   "00001101110111001110100111011111010111000110010000110101100000" when "000101",
   "00010000101000101011001000111111001110111010101101110011011010" when "000110",
   "00010011011010000111101010011111000110101111001010110001010100" when "000111",
   "00010110001011100100001011111110111110100011100111101111001101" when "001000",
   "00011000111101000000101101011110110110011000000100101101000111" when "001001",
   "00011011101110011101001110111110101110001100100001101011000001" when "001010",
   "00011110011111111001110000011110100110000000111110101000111010" when "001011",
   "00100001010001010110010001111110011101110101011011100110110100" when "001100",
   "00100100000010110010110011011110010101101001111000100100101110" when "001101",
   "00100110110100001111010100111110001101011110010101100010100111" when "001110",
   "00101001100101101011110110011110000101010010110010100000100001" when "001111",
   "00101100010111001000010111111101111101000111001111011110011011" when "010000",
   "00101111001000100100111001011101110100111011101100011100010100" when "010001",
   "00110001111010000001011010111101101100110000001001011010001110" when "010010",
   "00110100101011011101111100011101100100100100100110011000001000" when "010011",
   "00110111011100111010011101111101011100011001000011010110000001" when "010100",
   "00111010001110010110111111011101010100001101100000010011111011" when "010101",
   "00111100111111110011100000111101001100000001111101010001110101" when "010110",
   "00111111110001010000000010011101000011110110011010001111101110" when "010111",
   "01000010100010101100100011111100111011101010110111001101101000" when "011000",
   "01000101010100001001000101011100110011011111010100001011100010" when "011001",
   "01001000000101100101100110111100101011010011110001001001011011" when "011010",
   "01001010110111000010001000011100100011001000001110000111010101" when "011011",
   "01001101101000011110101001111100011010111100101011000101001111" when "011100",
   "01010000011001111011001011011100010010110001001000000011001000" when "011101",
   "01010011001011010111101100111100001010100101100101000001000010" when "011110",
   "01010101111100110100001110011100000010011010000001111110111100" when "011111",
   "01011000101110010000101111111011111010001110011110111100110101" when "100000",
   "01011011011111101101010001011011110010000010111011111010101111" when "100001",
   "01011110010001001001110010111011101001110111011000111000101001" when "100010",
   "01100001000010100110010100011011100001101011110101110110100010" when "100011",
   "01100011110100000010110101111011011001100000010010110100011100" when "100100",
   "01100110100101011111010111011011010001010100101111110010010110" when "100101",
   "01101001010110111011111000111011001001001001001100110000001111" when "100110",
   "01101100001000011000011010011011000000111101101001101110001001" when "100111",
   "01101110111001110100111011111010111000110010000110101100000011" when "101000",
   "01110001101011010001011101011010110000100110100011101001111101" when "101001",
   "01110100011100101101111110111010101000011011000000100111110110" when "101010",
   "01110111001110001010100000011010100000001111011101100101110000" when "101011",
   "01111001111111100111000001111010011000000011111010100011101010" when "101100",
   "01111100110001000011100011011010001111111000010111100001100011" when "101101",
   "01111111100010100000000100111010000111101100110100011111011101" when "101110",
   "10000010010011111100100110011001111111100001010001011101010111" when "101111",
   "10000101000101011001000111111001110111010101101110011011010000" when "110000",
   "10000111110110110101101001011001101111001010001011011001001010" when "110001",
   "10001010101000010010001010111001100110111110101000010111000100" when "110010",
   "10001101011001101110101100011001011110110011000101010100111101" when "110011",
   "10010000001011001011001101111001010110100111100010010010110111" when "110100",
   "10010010111100100111101111011001001110011011111111010000110001" when "110101",
   "10010101101110000100010000111001000110010000011100001110101010" when "110110",
   "10011000011111100000110010011000111110000100111001001100100100" when "110111",
   "10011011010000111101010011111000110101111001010110001010011110" when "111000",
   "10011110000010011001110101011000101101101101110011001000010111" when "111001",
   "10100000110011110110010110111000100101100010010000000110010001" when "111010",
   "10100011100101010010111000011000011101010110101101000100001011" when "111011",
   "10100110010110101111011001111000010101001011001010000010000100" when "111100",
   "10101001001000001011111011011000001100111111100110111111111110" when "111101",
   "10101011111001101000011100111000000100110100000011111101111000" when "111110",
   "10101110101011000100111110010111111100101000100000111011110001" when "111111",
   "--------------------------------------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_68_f400_uid44
--                    (IntAdderAlternative_68_f400_uid48)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_68_f400_uid44 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(67 downto 0);
          Y : in  std_logic_vector(67 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(67 downto 0)   );
end entity;

architecture arch of IntAdder_68_f400_uid44 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(26 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(25 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(26 downto 0);
signal sum_l1_idx1 :  std_logic_vector(25 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(67 downto 42)) + ( "0" & Y(67 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(25 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(26 downto 26);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(25 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(26 downto 26);
   R <= sum_l1_idx1(25 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                     FixRealKCM_0_10_M56_log_2_unsigned
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team 2015
--------------------------------------------------------------------------------
-- Pipeline depth: 5 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library std;
use std.textio.all;
library work;

entity FixRealKCM_0_10_M56_log_2_unsigned is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(10 downto 0);
          R : out  std_logic_vector(66 downto 0)   );
end entity;

architecture arch of FixRealKCM_0_10_M56_log_2_unsigned is
   component FixRealKCM_0_10_M56_log_2_unsigned_Table_0 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(61 downto 0)   );
   end component;

   component FixRealKCM_0_10_M56_log_2_unsigned_Table_1 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(4 downto 0);
             Y : out  std_logic_vector(66 downto 0)   );
   end component;

   component IntAdder_68_f400_uid44 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(67 downto 0);
             Y : in  std_logic_vector(67 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(67 downto 0)   );
   end component;

signal d1_kcmMult_30 :  std_logic_vector(4 downto 0);
signal d0_kcmMult_30 :  std_logic_vector(5 downto 0);
signal pp0_kcmMult_30, pp0_kcmMult_30_d1, pp0_kcmMult_30_d2 :  std_logic_vector(61 downto 0);
signal heap_bh31_w0_0, heap_bh31_w0_0_d1, heap_bh31_w0_0_d2, heap_bh31_w0_0_d3 :  std_logic;
signal heap_bh31_w1_0, heap_bh31_w1_0_d1, heap_bh31_w1_0_d2, heap_bh31_w1_0_d3 :  std_logic;
signal heap_bh31_w2_0, heap_bh31_w2_0_d1, heap_bh31_w2_0_d2, heap_bh31_w2_0_d3 :  std_logic;
signal heap_bh31_w3_0, heap_bh31_w3_0_d1, heap_bh31_w3_0_d2, heap_bh31_w3_0_d3 :  std_logic;
signal heap_bh31_w4_0, heap_bh31_w4_0_d1, heap_bh31_w4_0_d2, heap_bh31_w4_0_d3 :  std_logic;
signal heap_bh31_w5_0, heap_bh31_w5_0_d1, heap_bh31_w5_0_d2, heap_bh31_w5_0_d3 :  std_logic;
signal heap_bh31_w6_0, heap_bh31_w6_0_d1, heap_bh31_w6_0_d2, heap_bh31_w6_0_d3 :  std_logic;
signal heap_bh31_w7_0, heap_bh31_w7_0_d1, heap_bh31_w7_0_d2, heap_bh31_w7_0_d3 :  std_logic;
signal heap_bh31_w8_0, heap_bh31_w8_0_d1, heap_bh31_w8_0_d2, heap_bh31_w8_0_d3 :  std_logic;
signal heap_bh31_w9_0, heap_bh31_w9_0_d1, heap_bh31_w9_0_d2, heap_bh31_w9_0_d3 :  std_logic;
signal heap_bh31_w10_0, heap_bh31_w10_0_d1, heap_bh31_w10_0_d2, heap_bh31_w10_0_d3 :  std_logic;
signal heap_bh31_w11_0, heap_bh31_w11_0_d1, heap_bh31_w11_0_d2, heap_bh31_w11_0_d3 :  std_logic;
signal heap_bh31_w12_0, heap_bh31_w12_0_d1, heap_bh31_w12_0_d2, heap_bh31_w12_0_d3 :  std_logic;
signal heap_bh31_w13_0, heap_bh31_w13_0_d1, heap_bh31_w13_0_d2, heap_bh31_w13_0_d3 :  std_logic;
signal heap_bh31_w14_0, heap_bh31_w14_0_d1, heap_bh31_w14_0_d2, heap_bh31_w14_0_d3 :  std_logic;
signal heap_bh31_w15_0, heap_bh31_w15_0_d1, heap_bh31_w15_0_d2, heap_bh31_w15_0_d3 :  std_logic;
signal heap_bh31_w16_0, heap_bh31_w16_0_d1, heap_bh31_w16_0_d2, heap_bh31_w16_0_d3 :  std_logic;
signal heap_bh31_w17_0, heap_bh31_w17_0_d1, heap_bh31_w17_0_d2, heap_bh31_w17_0_d3 :  std_logic;
signal heap_bh31_w18_0, heap_bh31_w18_0_d1, heap_bh31_w18_0_d2, heap_bh31_w18_0_d3 :  std_logic;
signal heap_bh31_w19_0, heap_bh31_w19_0_d1, heap_bh31_w19_0_d2, heap_bh31_w19_0_d3 :  std_logic;
signal heap_bh31_w20_0, heap_bh31_w20_0_d1, heap_bh31_w20_0_d2, heap_bh31_w20_0_d3 :  std_logic;
signal heap_bh31_w21_0, heap_bh31_w21_0_d1, heap_bh31_w21_0_d2, heap_bh31_w21_0_d3 :  std_logic;
signal heap_bh31_w22_0, heap_bh31_w22_0_d1, heap_bh31_w22_0_d2, heap_bh31_w22_0_d3 :  std_logic;
signal heap_bh31_w23_0, heap_bh31_w23_0_d1, heap_bh31_w23_0_d2 :  std_logic;
signal heap_bh31_w24_0, heap_bh31_w24_0_d1, heap_bh31_w24_0_d2 :  std_logic;
signal heap_bh31_w25_0, heap_bh31_w25_0_d1, heap_bh31_w25_0_d2 :  std_logic;
signal heap_bh31_w26_0, heap_bh31_w26_0_d1, heap_bh31_w26_0_d2 :  std_logic;
signal heap_bh31_w27_0, heap_bh31_w27_0_d1, heap_bh31_w27_0_d2 :  std_logic;
signal heap_bh31_w28_0, heap_bh31_w28_0_d1, heap_bh31_w28_0_d2 :  std_logic;
signal heap_bh31_w29_0, heap_bh31_w29_0_d1, heap_bh31_w29_0_d2 :  std_logic;
signal heap_bh31_w30_0, heap_bh31_w30_0_d1, heap_bh31_w30_0_d2 :  std_logic;
signal heap_bh31_w31_0, heap_bh31_w31_0_d1, heap_bh31_w31_0_d2 :  std_logic;
signal heap_bh31_w32_0, heap_bh31_w32_0_d1, heap_bh31_w32_0_d2 :  std_logic;
signal heap_bh31_w33_0, heap_bh31_w33_0_d1, heap_bh31_w33_0_d2 :  std_logic;
signal heap_bh31_w34_0, heap_bh31_w34_0_d1, heap_bh31_w34_0_d2 :  std_logic;
signal heap_bh31_w35_0, heap_bh31_w35_0_d1, heap_bh31_w35_0_d2 :  std_logic;
signal heap_bh31_w36_0, heap_bh31_w36_0_d1, heap_bh31_w36_0_d2 :  std_logic;
signal heap_bh31_w37_0, heap_bh31_w37_0_d1, heap_bh31_w37_0_d2 :  std_logic;
signal heap_bh31_w38_0, heap_bh31_w38_0_d1, heap_bh31_w38_0_d2 :  std_logic;
signal heap_bh31_w39_0, heap_bh31_w39_0_d1, heap_bh31_w39_0_d2 :  std_logic;
signal heap_bh31_w40_0, heap_bh31_w40_0_d1, heap_bh31_w40_0_d2 :  std_logic;
signal heap_bh31_w41_0, heap_bh31_w41_0_d1, heap_bh31_w41_0_d2 :  std_logic;
signal heap_bh31_w42_0, heap_bh31_w42_0_d1, heap_bh31_w42_0_d2 :  std_logic;
signal heap_bh31_w43_0, heap_bh31_w43_0_d1, heap_bh31_w43_0_d2 :  std_logic;
signal heap_bh31_w44_0, heap_bh31_w44_0_d1, heap_bh31_w44_0_d2 :  std_logic;
signal heap_bh31_w45_0, heap_bh31_w45_0_d1, heap_bh31_w45_0_d2 :  std_logic;
signal heap_bh31_w46_0, heap_bh31_w46_0_d1, heap_bh31_w46_0_d2 :  std_logic;
signal heap_bh31_w47_0, heap_bh31_w47_0_d1, heap_bh31_w47_0_d2 :  std_logic;
signal heap_bh31_w48_0, heap_bh31_w48_0_d1 :  std_logic;
signal heap_bh31_w49_0, heap_bh31_w49_0_d1 :  std_logic;
signal heap_bh31_w50_0, heap_bh31_w50_0_d1 :  std_logic;
signal heap_bh31_w51_0, heap_bh31_w51_0_d1 :  std_logic;
signal heap_bh31_w52_0, heap_bh31_w52_0_d1 :  std_logic;
signal heap_bh31_w53_0, heap_bh31_w53_0_d1 :  std_logic;
signal heap_bh31_w54_0, heap_bh31_w54_0_d1 :  std_logic;
signal heap_bh31_w55_0, heap_bh31_w55_0_d1 :  std_logic;
signal heap_bh31_w56_0, heap_bh31_w56_0_d1 :  std_logic;
signal heap_bh31_w57_0, heap_bh31_w57_0_d1 :  std_logic;
signal heap_bh31_w58_0, heap_bh31_w58_0_d1 :  std_logic;
signal heap_bh31_w59_0, heap_bh31_w59_0_d1 :  std_logic;
signal heap_bh31_w60_0, heap_bh31_w60_0_d1 :  std_logic;
signal heap_bh31_w61_0, heap_bh31_w61_0_d1 :  std_logic;
signal pp1_kcmMult_30, pp1_kcmMult_30_d1, pp1_kcmMult_30_d2 :  std_logic_vector(66 downto 0);
signal heap_bh31_w0_1, heap_bh31_w0_1_d1, heap_bh31_w0_1_d2, heap_bh31_w0_1_d3 :  std_logic;
signal heap_bh31_w1_1, heap_bh31_w1_1_d1, heap_bh31_w1_1_d2, heap_bh31_w1_1_d3 :  std_logic;
signal heap_bh31_w2_1, heap_bh31_w2_1_d1, heap_bh31_w2_1_d2, heap_bh31_w2_1_d3 :  std_logic;
signal heap_bh31_w3_1, heap_bh31_w3_1_d1, heap_bh31_w3_1_d2, heap_bh31_w3_1_d3 :  std_logic;
signal heap_bh31_w4_1, heap_bh31_w4_1_d1, heap_bh31_w4_1_d2, heap_bh31_w4_1_d3 :  std_logic;
signal heap_bh31_w5_1, heap_bh31_w5_1_d1, heap_bh31_w5_1_d2, heap_bh31_w5_1_d3 :  std_logic;
signal heap_bh31_w6_1, heap_bh31_w6_1_d1, heap_bh31_w6_1_d2, heap_bh31_w6_1_d3 :  std_logic;
signal heap_bh31_w7_1, heap_bh31_w7_1_d1, heap_bh31_w7_1_d2, heap_bh31_w7_1_d3 :  std_logic;
signal heap_bh31_w8_1, heap_bh31_w8_1_d1, heap_bh31_w8_1_d2, heap_bh31_w8_1_d3 :  std_logic;
signal heap_bh31_w9_1, heap_bh31_w9_1_d1, heap_bh31_w9_1_d2, heap_bh31_w9_1_d3 :  std_logic;
signal heap_bh31_w10_1, heap_bh31_w10_1_d1, heap_bh31_w10_1_d2, heap_bh31_w10_1_d3 :  std_logic;
signal heap_bh31_w11_1, heap_bh31_w11_1_d1, heap_bh31_w11_1_d2, heap_bh31_w11_1_d3 :  std_logic;
signal heap_bh31_w12_1, heap_bh31_w12_1_d1, heap_bh31_w12_1_d2, heap_bh31_w12_1_d3 :  std_logic;
signal heap_bh31_w13_1, heap_bh31_w13_1_d1, heap_bh31_w13_1_d2, heap_bh31_w13_1_d3 :  std_logic;
signal heap_bh31_w14_1, heap_bh31_w14_1_d1, heap_bh31_w14_1_d2, heap_bh31_w14_1_d3 :  std_logic;
signal heap_bh31_w15_1, heap_bh31_w15_1_d1, heap_bh31_w15_1_d2, heap_bh31_w15_1_d3 :  std_logic;
signal heap_bh31_w16_1, heap_bh31_w16_1_d1, heap_bh31_w16_1_d2, heap_bh31_w16_1_d3 :  std_logic;
signal heap_bh31_w17_1, heap_bh31_w17_1_d1, heap_bh31_w17_1_d2, heap_bh31_w17_1_d3 :  std_logic;
signal heap_bh31_w18_1, heap_bh31_w18_1_d1, heap_bh31_w18_1_d2, heap_bh31_w18_1_d3 :  std_logic;
signal heap_bh31_w19_1, heap_bh31_w19_1_d1, heap_bh31_w19_1_d2, heap_bh31_w19_1_d3 :  std_logic;
signal heap_bh31_w20_1, heap_bh31_w20_1_d1, heap_bh31_w20_1_d2, heap_bh31_w20_1_d3 :  std_logic;
signal heap_bh31_w21_1, heap_bh31_w21_1_d1, heap_bh31_w21_1_d2, heap_bh31_w21_1_d3 :  std_logic;
signal heap_bh31_w22_1, heap_bh31_w22_1_d1, heap_bh31_w22_1_d2, heap_bh31_w22_1_d3 :  std_logic;
signal heap_bh31_w23_1, heap_bh31_w23_1_d1, heap_bh31_w23_1_d2 :  std_logic;
signal heap_bh31_w24_1, heap_bh31_w24_1_d1, heap_bh31_w24_1_d2 :  std_logic;
signal heap_bh31_w25_1, heap_bh31_w25_1_d1, heap_bh31_w25_1_d2 :  std_logic;
signal heap_bh31_w26_1, heap_bh31_w26_1_d1, heap_bh31_w26_1_d2 :  std_logic;
signal heap_bh31_w27_1, heap_bh31_w27_1_d1, heap_bh31_w27_1_d2 :  std_logic;
signal heap_bh31_w28_1, heap_bh31_w28_1_d1, heap_bh31_w28_1_d2 :  std_logic;
signal heap_bh31_w29_1, heap_bh31_w29_1_d1, heap_bh31_w29_1_d2 :  std_logic;
signal heap_bh31_w30_1, heap_bh31_w30_1_d1, heap_bh31_w30_1_d2 :  std_logic;
signal heap_bh31_w31_1, heap_bh31_w31_1_d1, heap_bh31_w31_1_d2 :  std_logic;
signal heap_bh31_w32_1, heap_bh31_w32_1_d1, heap_bh31_w32_1_d2 :  std_logic;
signal heap_bh31_w33_1, heap_bh31_w33_1_d1, heap_bh31_w33_1_d2 :  std_logic;
signal heap_bh31_w34_1, heap_bh31_w34_1_d1, heap_bh31_w34_1_d2 :  std_logic;
signal heap_bh31_w35_1, heap_bh31_w35_1_d1, heap_bh31_w35_1_d2 :  std_logic;
signal heap_bh31_w36_1, heap_bh31_w36_1_d1, heap_bh31_w36_1_d2 :  std_logic;
signal heap_bh31_w37_1, heap_bh31_w37_1_d1, heap_bh31_w37_1_d2 :  std_logic;
signal heap_bh31_w38_1, heap_bh31_w38_1_d1, heap_bh31_w38_1_d2 :  std_logic;
signal heap_bh31_w39_1, heap_bh31_w39_1_d1, heap_bh31_w39_1_d2 :  std_logic;
signal heap_bh31_w40_1, heap_bh31_w40_1_d1, heap_bh31_w40_1_d2 :  std_logic;
signal heap_bh31_w41_1, heap_bh31_w41_1_d1, heap_bh31_w41_1_d2 :  std_logic;
signal heap_bh31_w42_1, heap_bh31_w42_1_d1, heap_bh31_w42_1_d2 :  std_logic;
signal heap_bh31_w43_1, heap_bh31_w43_1_d1, heap_bh31_w43_1_d2 :  std_logic;
signal heap_bh31_w44_1, heap_bh31_w44_1_d1, heap_bh31_w44_1_d2 :  std_logic;
signal heap_bh31_w45_1, heap_bh31_w45_1_d1, heap_bh31_w45_1_d2 :  std_logic;
signal heap_bh31_w46_1, heap_bh31_w46_1_d1, heap_bh31_w46_1_d2 :  std_logic;
signal heap_bh31_w47_1, heap_bh31_w47_1_d1, heap_bh31_w47_1_d2 :  std_logic;
signal heap_bh31_w48_1, heap_bh31_w48_1_d1 :  std_logic;
signal heap_bh31_w49_1, heap_bh31_w49_1_d1 :  std_logic;
signal heap_bh31_w50_1, heap_bh31_w50_1_d1 :  std_logic;
signal heap_bh31_w51_1, heap_bh31_w51_1_d1 :  std_logic;
signal heap_bh31_w52_1, heap_bh31_w52_1_d1 :  std_logic;
signal heap_bh31_w53_1, heap_bh31_w53_1_d1 :  std_logic;
signal heap_bh31_w54_1, heap_bh31_w54_1_d1 :  std_logic;
signal heap_bh31_w55_1, heap_bh31_w55_1_d1 :  std_logic;
signal heap_bh31_w56_1, heap_bh31_w56_1_d1 :  std_logic;
signal heap_bh31_w57_1, heap_bh31_w57_1_d1 :  std_logic;
signal heap_bh31_w58_1, heap_bh31_w58_1_d1 :  std_logic;
signal heap_bh31_w59_1, heap_bh31_w59_1_d1 :  std_logic;
signal heap_bh31_w60_1, heap_bh31_w60_1_d1 :  std_logic;
signal heap_bh31_w61_1, heap_bh31_w61_1_d1 :  std_logic;
signal heap_bh31_w62_0, heap_bh31_w62_0_d1 :  std_logic;
signal heap_bh31_w63_0, heap_bh31_w63_0_d1 :  std_logic;
signal heap_bh31_w64_0, heap_bh31_w64_0_d1 :  std_logic;
signal heap_bh31_w65_0, heap_bh31_w65_0_d1 :  std_logic;
signal heap_bh31_w66_0, heap_bh31_w66_0_d1 :  std_logic;
signal finalAdderIn0_bh31 :  std_logic_vector(67 downto 0);
signal finalAdderIn1_bh31 :  std_logic_vector(67 downto 0);
signal finalAdderCin_bh31 :  std_logic;
signal finalAdderOut_bh31 :  std_logic_vector(67 downto 0);
signal CompressionResult31 :  std_logic_vector(67 downto 0);
signal OutRes :  std_logic_vector(66 downto 0);
signal X_d1 :  std_logic_vector(10 downto 0);
attribute rom_extract: string;
attribute rom_style: string;
attribute rom_extract of FixRealKCM_0_10_M56_log_2_unsigned_Table_0: component is "yes";
attribute rom_extract of FixRealKCM_0_10_M56_log_2_unsigned_Table_1: component is "yes";
attribute rom_style of FixRealKCM_0_10_M56_log_2_unsigned_Table_0: component is "distributed";
attribute rom_style of FixRealKCM_0_10_M56_log_2_unsigned_Table_1: component is "distributed";
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            pp0_kcmMult_30_d1 <=  pp0_kcmMult_30;
            pp0_kcmMult_30_d2 <=  pp0_kcmMult_30_d1;
            heap_bh31_w0_0_d1 <=  heap_bh31_w0_0;
            heap_bh31_w0_0_d2 <=  heap_bh31_w0_0_d1;
            heap_bh31_w0_0_d3 <=  heap_bh31_w0_0_d2;
            heap_bh31_w1_0_d1 <=  heap_bh31_w1_0;
            heap_bh31_w1_0_d2 <=  heap_bh31_w1_0_d1;
            heap_bh31_w1_0_d3 <=  heap_bh31_w1_0_d2;
            heap_bh31_w2_0_d1 <=  heap_bh31_w2_0;
            heap_bh31_w2_0_d2 <=  heap_bh31_w2_0_d1;
            heap_bh31_w2_0_d3 <=  heap_bh31_w2_0_d2;
            heap_bh31_w3_0_d1 <=  heap_bh31_w3_0;
            heap_bh31_w3_0_d2 <=  heap_bh31_w3_0_d1;
            heap_bh31_w3_0_d3 <=  heap_bh31_w3_0_d2;
            heap_bh31_w4_0_d1 <=  heap_bh31_w4_0;
            heap_bh31_w4_0_d2 <=  heap_bh31_w4_0_d1;
            heap_bh31_w4_0_d3 <=  heap_bh31_w4_0_d2;
            heap_bh31_w5_0_d1 <=  heap_bh31_w5_0;
            heap_bh31_w5_0_d2 <=  heap_bh31_w5_0_d1;
            heap_bh31_w5_0_d3 <=  heap_bh31_w5_0_d2;
            heap_bh31_w6_0_d1 <=  heap_bh31_w6_0;
            heap_bh31_w6_0_d2 <=  heap_bh31_w6_0_d1;
            heap_bh31_w6_0_d3 <=  heap_bh31_w6_0_d2;
            heap_bh31_w7_0_d1 <=  heap_bh31_w7_0;
            heap_bh31_w7_0_d2 <=  heap_bh31_w7_0_d1;
            heap_bh31_w7_0_d3 <=  heap_bh31_w7_0_d2;
            heap_bh31_w8_0_d1 <=  heap_bh31_w8_0;
            heap_bh31_w8_0_d2 <=  heap_bh31_w8_0_d1;
            heap_bh31_w8_0_d3 <=  heap_bh31_w8_0_d2;
            heap_bh31_w9_0_d1 <=  heap_bh31_w9_0;
            heap_bh31_w9_0_d2 <=  heap_bh31_w9_0_d1;
            heap_bh31_w9_0_d3 <=  heap_bh31_w9_0_d2;
            heap_bh31_w10_0_d1 <=  heap_bh31_w10_0;
            heap_bh31_w10_0_d2 <=  heap_bh31_w10_0_d1;
            heap_bh31_w10_0_d3 <=  heap_bh31_w10_0_d2;
            heap_bh31_w11_0_d1 <=  heap_bh31_w11_0;
            heap_bh31_w11_0_d2 <=  heap_bh31_w11_0_d1;
            heap_bh31_w11_0_d3 <=  heap_bh31_w11_0_d2;
            heap_bh31_w12_0_d1 <=  heap_bh31_w12_0;
            heap_bh31_w12_0_d2 <=  heap_bh31_w12_0_d1;
            heap_bh31_w12_0_d3 <=  heap_bh31_w12_0_d2;
            heap_bh31_w13_0_d1 <=  heap_bh31_w13_0;
            heap_bh31_w13_0_d2 <=  heap_bh31_w13_0_d1;
            heap_bh31_w13_0_d3 <=  heap_bh31_w13_0_d2;
            heap_bh31_w14_0_d1 <=  heap_bh31_w14_0;
            heap_bh31_w14_0_d2 <=  heap_bh31_w14_0_d1;
            heap_bh31_w14_0_d3 <=  heap_bh31_w14_0_d2;
            heap_bh31_w15_0_d1 <=  heap_bh31_w15_0;
            heap_bh31_w15_0_d2 <=  heap_bh31_w15_0_d1;
            heap_bh31_w15_0_d3 <=  heap_bh31_w15_0_d2;
            heap_bh31_w16_0_d1 <=  heap_bh31_w16_0;
            heap_bh31_w16_0_d2 <=  heap_bh31_w16_0_d1;
            heap_bh31_w16_0_d3 <=  heap_bh31_w16_0_d2;
            heap_bh31_w17_0_d1 <=  heap_bh31_w17_0;
            heap_bh31_w17_0_d2 <=  heap_bh31_w17_0_d1;
            heap_bh31_w17_0_d3 <=  heap_bh31_w17_0_d2;
            heap_bh31_w18_0_d1 <=  heap_bh31_w18_0;
            heap_bh31_w18_0_d2 <=  heap_bh31_w18_0_d1;
            heap_bh31_w18_0_d3 <=  heap_bh31_w18_0_d2;
            heap_bh31_w19_0_d1 <=  heap_bh31_w19_0;
            heap_bh31_w19_0_d2 <=  heap_bh31_w19_0_d1;
            heap_bh31_w19_0_d3 <=  heap_bh31_w19_0_d2;
            heap_bh31_w20_0_d1 <=  heap_bh31_w20_0;
            heap_bh31_w20_0_d2 <=  heap_bh31_w20_0_d1;
            heap_bh31_w20_0_d3 <=  heap_bh31_w20_0_d2;
            heap_bh31_w21_0_d1 <=  heap_bh31_w21_0;
            heap_bh31_w21_0_d2 <=  heap_bh31_w21_0_d1;
            heap_bh31_w21_0_d3 <=  heap_bh31_w21_0_d2;
            heap_bh31_w22_0_d1 <=  heap_bh31_w22_0;
            heap_bh31_w22_0_d2 <=  heap_bh31_w22_0_d1;
            heap_bh31_w22_0_d3 <=  heap_bh31_w22_0_d2;
            heap_bh31_w23_0_d1 <=  heap_bh31_w23_0;
            heap_bh31_w23_0_d2 <=  heap_bh31_w23_0_d1;
            heap_bh31_w24_0_d1 <=  heap_bh31_w24_0;
            heap_bh31_w24_0_d2 <=  heap_bh31_w24_0_d1;
            heap_bh31_w25_0_d1 <=  heap_bh31_w25_0;
            heap_bh31_w25_0_d2 <=  heap_bh31_w25_0_d1;
            heap_bh31_w26_0_d1 <=  heap_bh31_w26_0;
            heap_bh31_w26_0_d2 <=  heap_bh31_w26_0_d1;
            heap_bh31_w27_0_d1 <=  heap_bh31_w27_0;
            heap_bh31_w27_0_d2 <=  heap_bh31_w27_0_d1;
            heap_bh31_w28_0_d1 <=  heap_bh31_w28_0;
            heap_bh31_w28_0_d2 <=  heap_bh31_w28_0_d1;
            heap_bh31_w29_0_d1 <=  heap_bh31_w29_0;
            heap_bh31_w29_0_d2 <=  heap_bh31_w29_0_d1;
            heap_bh31_w30_0_d1 <=  heap_bh31_w30_0;
            heap_bh31_w30_0_d2 <=  heap_bh31_w30_0_d1;
            heap_bh31_w31_0_d1 <=  heap_bh31_w31_0;
            heap_bh31_w31_0_d2 <=  heap_bh31_w31_0_d1;
            heap_bh31_w32_0_d1 <=  heap_bh31_w32_0;
            heap_bh31_w32_0_d2 <=  heap_bh31_w32_0_d1;
            heap_bh31_w33_0_d1 <=  heap_bh31_w33_0;
            heap_bh31_w33_0_d2 <=  heap_bh31_w33_0_d1;
            heap_bh31_w34_0_d1 <=  heap_bh31_w34_0;
            heap_bh31_w34_0_d2 <=  heap_bh31_w34_0_d1;
            heap_bh31_w35_0_d1 <=  heap_bh31_w35_0;
            heap_bh31_w35_0_d2 <=  heap_bh31_w35_0_d1;
            heap_bh31_w36_0_d1 <=  heap_bh31_w36_0;
            heap_bh31_w36_0_d2 <=  heap_bh31_w36_0_d1;
            heap_bh31_w37_0_d1 <=  heap_bh31_w37_0;
            heap_bh31_w37_0_d2 <=  heap_bh31_w37_0_d1;
            heap_bh31_w38_0_d1 <=  heap_bh31_w38_0;
            heap_bh31_w38_0_d2 <=  heap_bh31_w38_0_d1;
            heap_bh31_w39_0_d1 <=  heap_bh31_w39_0;
            heap_bh31_w39_0_d2 <=  heap_bh31_w39_0_d1;
            heap_bh31_w40_0_d1 <=  heap_bh31_w40_0;
            heap_bh31_w40_0_d2 <=  heap_bh31_w40_0_d1;
            heap_bh31_w41_0_d1 <=  heap_bh31_w41_0;
            heap_bh31_w41_0_d2 <=  heap_bh31_w41_0_d1;
            heap_bh31_w42_0_d1 <=  heap_bh31_w42_0;
            heap_bh31_w42_0_d2 <=  heap_bh31_w42_0_d1;
            heap_bh31_w43_0_d1 <=  heap_bh31_w43_0;
            heap_bh31_w43_0_d2 <=  heap_bh31_w43_0_d1;
            heap_bh31_w44_0_d1 <=  heap_bh31_w44_0;
            heap_bh31_w44_0_d2 <=  heap_bh31_w44_0_d1;
            heap_bh31_w45_0_d1 <=  heap_bh31_w45_0;
            heap_bh31_w45_0_d2 <=  heap_bh31_w45_0_d1;
            heap_bh31_w46_0_d1 <=  heap_bh31_w46_0;
            heap_bh31_w46_0_d2 <=  heap_bh31_w46_0_d1;
            heap_bh31_w47_0_d1 <=  heap_bh31_w47_0;
            heap_bh31_w47_0_d2 <=  heap_bh31_w47_0_d1;
            heap_bh31_w48_0_d1 <=  heap_bh31_w48_0;
            heap_bh31_w49_0_d1 <=  heap_bh31_w49_0;
            heap_bh31_w50_0_d1 <=  heap_bh31_w50_0;
            heap_bh31_w51_0_d1 <=  heap_bh31_w51_0;
            heap_bh31_w52_0_d1 <=  heap_bh31_w52_0;
            heap_bh31_w53_0_d1 <=  heap_bh31_w53_0;
            heap_bh31_w54_0_d1 <=  heap_bh31_w54_0;
            heap_bh31_w55_0_d1 <=  heap_bh31_w55_0;
            heap_bh31_w56_0_d1 <=  heap_bh31_w56_0;
            heap_bh31_w57_0_d1 <=  heap_bh31_w57_0;
            heap_bh31_w58_0_d1 <=  heap_bh31_w58_0;
            heap_bh31_w59_0_d1 <=  heap_bh31_w59_0;
            heap_bh31_w60_0_d1 <=  heap_bh31_w60_0;
            heap_bh31_w61_0_d1 <=  heap_bh31_w61_0;
            pp1_kcmMult_30_d1 <=  pp1_kcmMult_30;
            pp1_kcmMult_30_d2 <=  pp1_kcmMult_30_d1;
            heap_bh31_w0_1_d1 <=  heap_bh31_w0_1;
            heap_bh31_w0_1_d2 <=  heap_bh31_w0_1_d1;
            heap_bh31_w0_1_d3 <=  heap_bh31_w0_1_d2;
            heap_bh31_w1_1_d1 <=  heap_bh31_w1_1;
            heap_bh31_w1_1_d2 <=  heap_bh31_w1_1_d1;
            heap_bh31_w1_1_d3 <=  heap_bh31_w1_1_d2;
            heap_bh31_w2_1_d1 <=  heap_bh31_w2_1;
            heap_bh31_w2_1_d2 <=  heap_bh31_w2_1_d1;
            heap_bh31_w2_1_d3 <=  heap_bh31_w2_1_d2;
            heap_bh31_w3_1_d1 <=  heap_bh31_w3_1;
            heap_bh31_w3_1_d2 <=  heap_bh31_w3_1_d1;
            heap_bh31_w3_1_d3 <=  heap_bh31_w3_1_d2;
            heap_bh31_w4_1_d1 <=  heap_bh31_w4_1;
            heap_bh31_w4_1_d2 <=  heap_bh31_w4_1_d1;
            heap_bh31_w4_1_d3 <=  heap_bh31_w4_1_d2;
            heap_bh31_w5_1_d1 <=  heap_bh31_w5_1;
            heap_bh31_w5_1_d2 <=  heap_bh31_w5_1_d1;
            heap_bh31_w5_1_d3 <=  heap_bh31_w5_1_d2;
            heap_bh31_w6_1_d1 <=  heap_bh31_w6_1;
            heap_bh31_w6_1_d2 <=  heap_bh31_w6_1_d1;
            heap_bh31_w6_1_d3 <=  heap_bh31_w6_1_d2;
            heap_bh31_w7_1_d1 <=  heap_bh31_w7_1;
            heap_bh31_w7_1_d2 <=  heap_bh31_w7_1_d1;
            heap_bh31_w7_1_d3 <=  heap_bh31_w7_1_d2;
            heap_bh31_w8_1_d1 <=  heap_bh31_w8_1;
            heap_bh31_w8_1_d2 <=  heap_bh31_w8_1_d1;
            heap_bh31_w8_1_d3 <=  heap_bh31_w8_1_d2;
            heap_bh31_w9_1_d1 <=  heap_bh31_w9_1;
            heap_bh31_w9_1_d2 <=  heap_bh31_w9_1_d1;
            heap_bh31_w9_1_d3 <=  heap_bh31_w9_1_d2;
            heap_bh31_w10_1_d1 <=  heap_bh31_w10_1;
            heap_bh31_w10_1_d2 <=  heap_bh31_w10_1_d1;
            heap_bh31_w10_1_d3 <=  heap_bh31_w10_1_d2;
            heap_bh31_w11_1_d1 <=  heap_bh31_w11_1;
            heap_bh31_w11_1_d2 <=  heap_bh31_w11_1_d1;
            heap_bh31_w11_1_d3 <=  heap_bh31_w11_1_d2;
            heap_bh31_w12_1_d1 <=  heap_bh31_w12_1;
            heap_bh31_w12_1_d2 <=  heap_bh31_w12_1_d1;
            heap_bh31_w12_1_d3 <=  heap_bh31_w12_1_d2;
            heap_bh31_w13_1_d1 <=  heap_bh31_w13_1;
            heap_bh31_w13_1_d2 <=  heap_bh31_w13_1_d1;
            heap_bh31_w13_1_d3 <=  heap_bh31_w13_1_d2;
            heap_bh31_w14_1_d1 <=  heap_bh31_w14_1;
            heap_bh31_w14_1_d2 <=  heap_bh31_w14_1_d1;
            heap_bh31_w14_1_d3 <=  heap_bh31_w14_1_d2;
            heap_bh31_w15_1_d1 <=  heap_bh31_w15_1;
            heap_bh31_w15_1_d2 <=  heap_bh31_w15_1_d1;
            heap_bh31_w15_1_d3 <=  heap_bh31_w15_1_d2;
            heap_bh31_w16_1_d1 <=  heap_bh31_w16_1;
            heap_bh31_w16_1_d2 <=  heap_bh31_w16_1_d1;
            heap_bh31_w16_1_d3 <=  heap_bh31_w16_1_d2;
            heap_bh31_w17_1_d1 <=  heap_bh31_w17_1;
            heap_bh31_w17_1_d2 <=  heap_bh31_w17_1_d1;
            heap_bh31_w17_1_d3 <=  heap_bh31_w17_1_d2;
            heap_bh31_w18_1_d1 <=  heap_bh31_w18_1;
            heap_bh31_w18_1_d2 <=  heap_bh31_w18_1_d1;
            heap_bh31_w18_1_d3 <=  heap_bh31_w18_1_d2;
            heap_bh31_w19_1_d1 <=  heap_bh31_w19_1;
            heap_bh31_w19_1_d2 <=  heap_bh31_w19_1_d1;
            heap_bh31_w19_1_d3 <=  heap_bh31_w19_1_d2;
            heap_bh31_w20_1_d1 <=  heap_bh31_w20_1;
            heap_bh31_w20_1_d2 <=  heap_bh31_w20_1_d1;
            heap_bh31_w20_1_d3 <=  heap_bh31_w20_1_d2;
            heap_bh31_w21_1_d1 <=  heap_bh31_w21_1;
            heap_bh31_w21_1_d2 <=  heap_bh31_w21_1_d1;
            heap_bh31_w21_1_d3 <=  heap_bh31_w21_1_d2;
            heap_bh31_w22_1_d1 <=  heap_bh31_w22_1;
            heap_bh31_w22_1_d2 <=  heap_bh31_w22_1_d1;
            heap_bh31_w22_1_d3 <=  heap_bh31_w22_1_d2;
            heap_bh31_w23_1_d1 <=  heap_bh31_w23_1;
            heap_bh31_w23_1_d2 <=  heap_bh31_w23_1_d1;
            heap_bh31_w24_1_d1 <=  heap_bh31_w24_1;
            heap_bh31_w24_1_d2 <=  heap_bh31_w24_1_d1;
            heap_bh31_w25_1_d1 <=  heap_bh31_w25_1;
            heap_bh31_w25_1_d2 <=  heap_bh31_w25_1_d1;
            heap_bh31_w26_1_d1 <=  heap_bh31_w26_1;
            heap_bh31_w26_1_d2 <=  heap_bh31_w26_1_d1;
            heap_bh31_w27_1_d1 <=  heap_bh31_w27_1;
            heap_bh31_w27_1_d2 <=  heap_bh31_w27_1_d1;
            heap_bh31_w28_1_d1 <=  heap_bh31_w28_1;
            heap_bh31_w28_1_d2 <=  heap_bh31_w28_1_d1;
            heap_bh31_w29_1_d1 <=  heap_bh31_w29_1;
            heap_bh31_w29_1_d2 <=  heap_bh31_w29_1_d1;
            heap_bh31_w30_1_d1 <=  heap_bh31_w30_1;
            heap_bh31_w30_1_d2 <=  heap_bh31_w30_1_d1;
            heap_bh31_w31_1_d1 <=  heap_bh31_w31_1;
            heap_bh31_w31_1_d2 <=  heap_bh31_w31_1_d1;
            heap_bh31_w32_1_d1 <=  heap_bh31_w32_1;
            heap_bh31_w32_1_d2 <=  heap_bh31_w32_1_d1;
            heap_bh31_w33_1_d1 <=  heap_bh31_w33_1;
            heap_bh31_w33_1_d2 <=  heap_bh31_w33_1_d1;
            heap_bh31_w34_1_d1 <=  heap_bh31_w34_1;
            heap_bh31_w34_1_d2 <=  heap_bh31_w34_1_d1;
            heap_bh31_w35_1_d1 <=  heap_bh31_w35_1;
            heap_bh31_w35_1_d2 <=  heap_bh31_w35_1_d1;
            heap_bh31_w36_1_d1 <=  heap_bh31_w36_1;
            heap_bh31_w36_1_d2 <=  heap_bh31_w36_1_d1;
            heap_bh31_w37_1_d1 <=  heap_bh31_w37_1;
            heap_bh31_w37_1_d2 <=  heap_bh31_w37_1_d1;
            heap_bh31_w38_1_d1 <=  heap_bh31_w38_1;
            heap_bh31_w38_1_d2 <=  heap_bh31_w38_1_d1;
            heap_bh31_w39_1_d1 <=  heap_bh31_w39_1;
            heap_bh31_w39_1_d2 <=  heap_bh31_w39_1_d1;
            heap_bh31_w40_1_d1 <=  heap_bh31_w40_1;
            heap_bh31_w40_1_d2 <=  heap_bh31_w40_1_d1;
            heap_bh31_w41_1_d1 <=  heap_bh31_w41_1;
            heap_bh31_w41_1_d2 <=  heap_bh31_w41_1_d1;
            heap_bh31_w42_1_d1 <=  heap_bh31_w42_1;
            heap_bh31_w42_1_d2 <=  heap_bh31_w42_1_d1;
            heap_bh31_w43_1_d1 <=  heap_bh31_w43_1;
            heap_bh31_w43_1_d2 <=  heap_bh31_w43_1_d1;
            heap_bh31_w44_1_d1 <=  heap_bh31_w44_1;
            heap_bh31_w44_1_d2 <=  heap_bh31_w44_1_d1;
            heap_bh31_w45_1_d1 <=  heap_bh31_w45_1;
            heap_bh31_w45_1_d2 <=  heap_bh31_w45_1_d1;
            heap_bh31_w46_1_d1 <=  heap_bh31_w46_1;
            heap_bh31_w46_1_d2 <=  heap_bh31_w46_1_d1;
            heap_bh31_w47_1_d1 <=  heap_bh31_w47_1;
            heap_bh31_w47_1_d2 <=  heap_bh31_w47_1_d1;
            heap_bh31_w48_1_d1 <=  heap_bh31_w48_1;
            heap_bh31_w49_1_d1 <=  heap_bh31_w49_1;
            heap_bh31_w50_1_d1 <=  heap_bh31_w50_1;
            heap_bh31_w51_1_d1 <=  heap_bh31_w51_1;
            heap_bh31_w52_1_d1 <=  heap_bh31_w52_1;
            heap_bh31_w53_1_d1 <=  heap_bh31_w53_1;
            heap_bh31_w54_1_d1 <=  heap_bh31_w54_1;
            heap_bh31_w55_1_d1 <=  heap_bh31_w55_1;
            heap_bh31_w56_1_d1 <=  heap_bh31_w56_1;
            heap_bh31_w57_1_d1 <=  heap_bh31_w57_1;
            heap_bh31_w58_1_d1 <=  heap_bh31_w58_1;
            heap_bh31_w59_1_d1 <=  heap_bh31_w59_1;
            heap_bh31_w60_1_d1 <=  heap_bh31_w60_1;
            heap_bh31_w61_1_d1 <=  heap_bh31_w61_1;
            heap_bh31_w62_0_d1 <=  heap_bh31_w62_0;
            heap_bh31_w63_0_d1 <=  heap_bh31_w63_0;
            heap_bh31_w64_0_d1 <=  heap_bh31_w64_0;
            heap_bh31_w65_0_d1 <=  heap_bh31_w65_0;
            heap_bh31_w66_0_d1 <=  heap_bh31_w66_0;
            X_d1 <=  X;
         end if;
      end process;
   ----------------Synchro barrier, entering cycle 1----------------
   d1_kcmMult_30 <= X_d1(10 downto 6);
   d0_kcmMult_30 <= X_d1(5 downto 0);
   ---------------- cycle 1----------------
   KCMTable_0_kcmMult_30: FixRealKCM_0_10_M56_log_2_unsigned_Table_0  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => d0_kcmMult_30,
                 Y => pp0_kcmMult_30);
   heap_bh31_w0_0 <= pp0_kcmMult_30(0); -- cycle= 1 cp= 1.72e-10
   heap_bh31_w1_0 <= pp0_kcmMult_30(1); -- cycle= 1 cp= 2.58e-10
   heap_bh31_w2_0 <= pp0_kcmMult_30(2); -- cycle= 1 cp= 3.44e-10
   heap_bh31_w3_0 <= pp0_kcmMult_30(3); -- cycle= 1 cp= 4.3e-10
   heap_bh31_w4_0 <= pp0_kcmMult_30(4); -- cycle= 1 cp= 5.16e-10
   heap_bh31_w5_0 <= pp0_kcmMult_30(5); -- cycle= 1 cp= 6.02e-10
   heap_bh31_w6_0 <= pp0_kcmMult_30(6); -- cycle= 1 cp= 6.88e-10
   heap_bh31_w7_0 <= pp0_kcmMult_30(7); -- cycle= 1 cp= 7.74e-10
   heap_bh31_w8_0 <= pp0_kcmMult_30(8); -- cycle= 1 cp= 8.6e-10
   heap_bh31_w9_0 <= pp0_kcmMult_30(9); -- cycle= 1 cp= 9.46e-10
   heap_bh31_w10_0 <= pp0_kcmMult_30(10); -- cycle= 1 cp= 1.032e-09
   heap_bh31_w11_0 <= pp0_kcmMult_30(11); -- cycle= 1 cp= 1.118e-09
   heap_bh31_w12_0 <= pp0_kcmMult_30(12); -- cycle= 1 cp= 1.204e-09
   heap_bh31_w13_0 <= pp0_kcmMult_30(13); -- cycle= 1 cp= 1.29e-09
   heap_bh31_w14_0 <= pp0_kcmMult_30(14); -- cycle= 1 cp= 1.376e-09
   heap_bh31_w15_0 <= pp0_kcmMult_30(15); -- cycle= 1 cp= 1.462e-09
   heap_bh31_w16_0 <= pp0_kcmMult_30(16); -- cycle= 1 cp= 1.548e-09
   heap_bh31_w17_0 <= pp0_kcmMult_30(17); -- cycle= 1 cp= 1.634e-09
   heap_bh31_w18_0 <= pp0_kcmMult_30(18); -- cycle= 1 cp= 1.72e-09
   heap_bh31_w19_0 <= pp0_kcmMult_30(19); -- cycle= 1 cp= 1.806e-09
   heap_bh31_w20_0 <= pp0_kcmMult_30(20); -- cycle= 1 cp= 1.892e-09
   heap_bh31_w21_0 <= pp0_kcmMult_30(21); -- cycle= 1 cp= 1.978e-09
   heap_bh31_w22_0 <= pp0_kcmMult_30(22); -- cycle= 1 cp= 2.064e-09
   ----------------Synchro barrier, entering cycle 2----------------
   heap_bh31_w23_0 <= pp0_kcmMult_30_d1(23); -- cycle= 2 cp= 0
   heap_bh31_w24_0 <= pp0_kcmMult_30_d1(24); -- cycle= 2 cp= 8.6e-11
   heap_bh31_w25_0 <= pp0_kcmMult_30_d1(25); -- cycle= 2 cp= 1.72e-10
   heap_bh31_w26_0 <= pp0_kcmMult_30_d1(26); -- cycle= 2 cp= 2.58e-10
   heap_bh31_w27_0 <= pp0_kcmMult_30_d1(27); -- cycle= 2 cp= 3.44e-10
   heap_bh31_w28_0 <= pp0_kcmMult_30_d1(28); -- cycle= 2 cp= 4.3e-10
   heap_bh31_w29_0 <= pp0_kcmMult_30_d1(29); -- cycle= 2 cp= 5.16e-10
   heap_bh31_w30_0 <= pp0_kcmMult_30_d1(30); -- cycle= 2 cp= 6.02e-10
   heap_bh31_w31_0 <= pp0_kcmMult_30_d1(31); -- cycle= 2 cp= 6.88e-10
   heap_bh31_w32_0 <= pp0_kcmMult_30_d1(32); -- cycle= 2 cp= 7.74e-10
   heap_bh31_w33_0 <= pp0_kcmMult_30_d1(33); -- cycle= 2 cp= 8.6e-10
   heap_bh31_w34_0 <= pp0_kcmMult_30_d1(34); -- cycle= 2 cp= 9.46e-10
   heap_bh31_w35_0 <= pp0_kcmMult_30_d1(35); -- cycle= 2 cp= 1.032e-09
   heap_bh31_w36_0 <= pp0_kcmMult_30_d1(36); -- cycle= 2 cp= 1.118e-09
   heap_bh31_w37_0 <= pp0_kcmMult_30_d1(37); -- cycle= 2 cp= 1.204e-09
   heap_bh31_w38_0 <= pp0_kcmMult_30_d1(38); -- cycle= 2 cp= 1.29e-09
   heap_bh31_w39_0 <= pp0_kcmMult_30_d1(39); -- cycle= 2 cp= 1.376e-09
   heap_bh31_w40_0 <= pp0_kcmMult_30_d1(40); -- cycle= 2 cp= 1.462e-09
   heap_bh31_w41_0 <= pp0_kcmMult_30_d1(41); -- cycle= 2 cp= 1.548e-09
   heap_bh31_w42_0 <= pp0_kcmMult_30_d1(42); -- cycle= 2 cp= 1.634e-09
   heap_bh31_w43_0 <= pp0_kcmMult_30_d1(43); -- cycle= 2 cp= 1.72e-09
   heap_bh31_w44_0 <= pp0_kcmMult_30_d1(44); -- cycle= 2 cp= 1.806e-09
   heap_bh31_w45_0 <= pp0_kcmMult_30_d1(45); -- cycle= 2 cp= 1.892e-09
   heap_bh31_w46_0 <= pp0_kcmMult_30_d1(46); -- cycle= 2 cp= 1.978e-09
   heap_bh31_w47_0 <= pp0_kcmMult_30_d1(47); -- cycle= 2 cp= 2.064e-09
   ----------------Synchro barrier, entering cycle 3----------------
   heap_bh31_w48_0 <= pp0_kcmMult_30_d2(48); -- cycle= 3 cp= 0
   heap_bh31_w49_0 <= pp0_kcmMult_30_d2(49); -- cycle= 3 cp= 8.6e-11
   heap_bh31_w50_0 <= pp0_kcmMult_30_d2(50); -- cycle= 3 cp= 1.72e-10
   heap_bh31_w51_0 <= pp0_kcmMult_30_d2(51); -- cycle= 3 cp= 2.58e-10
   heap_bh31_w52_0 <= pp0_kcmMult_30_d2(52); -- cycle= 3 cp= 3.44e-10
   heap_bh31_w53_0 <= pp0_kcmMult_30_d2(53); -- cycle= 3 cp= 4.3e-10
   heap_bh31_w54_0 <= pp0_kcmMult_30_d2(54); -- cycle= 3 cp= 5.16e-10
   heap_bh31_w55_0 <= pp0_kcmMult_30_d2(55); -- cycle= 3 cp= 6.02e-10
   heap_bh31_w56_0 <= pp0_kcmMult_30_d2(56); -- cycle= 3 cp= 6.88e-10
   heap_bh31_w57_0 <= pp0_kcmMult_30_d2(57); -- cycle= 3 cp= 7.74e-10
   heap_bh31_w58_0 <= pp0_kcmMult_30_d2(58); -- cycle= 3 cp= 8.6e-10
   heap_bh31_w59_0 <= pp0_kcmMult_30_d2(59); -- cycle= 3 cp= 9.46e-10
   heap_bh31_w60_0 <= pp0_kcmMult_30_d2(60); -- cycle= 3 cp= 1.032e-09
   heap_bh31_w61_0 <= pp0_kcmMult_30_d2(61); -- cycle= 3 cp= 1.118e-09
   ---------------- cycle 1----------------
   KCMTable_1_kcmMult_30: FixRealKCM_0_10_M56_log_2_unsigned_Table_1  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => d1_kcmMult_30,
                 Y => pp1_kcmMult_30);
   heap_bh31_w0_1 <= pp1_kcmMult_30(0); -- cycle= 1 cp= 1.72e-10
   heap_bh31_w1_1 <= pp1_kcmMult_30(1); -- cycle= 1 cp= 2.58e-10
   heap_bh31_w2_1 <= pp1_kcmMult_30(2); -- cycle= 1 cp= 3.44e-10
   heap_bh31_w3_1 <= pp1_kcmMult_30(3); -- cycle= 1 cp= 4.3e-10
   heap_bh31_w4_1 <= pp1_kcmMult_30(4); -- cycle= 1 cp= 5.16e-10
   heap_bh31_w5_1 <= pp1_kcmMult_30(5); -- cycle= 1 cp= 6.02e-10
   heap_bh31_w6_1 <= pp1_kcmMult_30(6); -- cycle= 1 cp= 6.88e-10
   heap_bh31_w7_1 <= pp1_kcmMult_30(7); -- cycle= 1 cp= 7.74e-10
   heap_bh31_w8_1 <= pp1_kcmMult_30(8); -- cycle= 1 cp= 8.6e-10
   heap_bh31_w9_1 <= pp1_kcmMult_30(9); -- cycle= 1 cp= 9.46e-10
   heap_bh31_w10_1 <= pp1_kcmMult_30(10); -- cycle= 1 cp= 1.032e-09
   heap_bh31_w11_1 <= pp1_kcmMult_30(11); -- cycle= 1 cp= 1.118e-09
   heap_bh31_w12_1 <= pp1_kcmMult_30(12); -- cycle= 1 cp= 1.204e-09
   heap_bh31_w13_1 <= pp1_kcmMult_30(13); -- cycle= 1 cp= 1.29e-09
   heap_bh31_w14_1 <= pp1_kcmMult_30(14); -- cycle= 1 cp= 1.376e-09
   heap_bh31_w15_1 <= pp1_kcmMult_30(15); -- cycle= 1 cp= 1.462e-09
   heap_bh31_w16_1 <= pp1_kcmMult_30(16); -- cycle= 1 cp= 1.548e-09
   heap_bh31_w17_1 <= pp1_kcmMult_30(17); -- cycle= 1 cp= 1.634e-09
   heap_bh31_w18_1 <= pp1_kcmMult_30(18); -- cycle= 1 cp= 1.72e-09
   heap_bh31_w19_1 <= pp1_kcmMult_30(19); -- cycle= 1 cp= 1.806e-09
   heap_bh31_w20_1 <= pp1_kcmMult_30(20); -- cycle= 1 cp= 1.892e-09
   heap_bh31_w21_1 <= pp1_kcmMult_30(21); -- cycle= 1 cp= 1.978e-09
   heap_bh31_w22_1 <= pp1_kcmMult_30(22); -- cycle= 1 cp= 2.064e-09
   ----------------Synchro barrier, entering cycle 2----------------
   heap_bh31_w23_1 <= pp1_kcmMult_30_d1(23); -- cycle= 2 cp= 0
   heap_bh31_w24_1 <= pp1_kcmMult_30_d1(24); -- cycle= 2 cp= 8.6e-11
   heap_bh31_w25_1 <= pp1_kcmMult_30_d1(25); -- cycle= 2 cp= 1.72e-10
   heap_bh31_w26_1 <= pp1_kcmMult_30_d1(26); -- cycle= 2 cp= 2.58e-10
   heap_bh31_w27_1 <= pp1_kcmMult_30_d1(27); -- cycle= 2 cp= 3.44e-10
   heap_bh31_w28_1 <= pp1_kcmMult_30_d1(28); -- cycle= 2 cp= 4.3e-10
   heap_bh31_w29_1 <= pp1_kcmMult_30_d1(29); -- cycle= 2 cp= 5.16e-10
   heap_bh31_w30_1 <= pp1_kcmMult_30_d1(30); -- cycle= 2 cp= 6.02e-10
   heap_bh31_w31_1 <= pp1_kcmMult_30_d1(31); -- cycle= 2 cp= 6.88e-10
   heap_bh31_w32_1 <= pp1_kcmMult_30_d1(32); -- cycle= 2 cp= 7.74e-10
   heap_bh31_w33_1 <= pp1_kcmMult_30_d1(33); -- cycle= 2 cp= 8.6e-10
   heap_bh31_w34_1 <= pp1_kcmMult_30_d1(34); -- cycle= 2 cp= 9.46e-10
   heap_bh31_w35_1 <= pp1_kcmMult_30_d1(35); -- cycle= 2 cp= 1.032e-09
   heap_bh31_w36_1 <= pp1_kcmMult_30_d1(36); -- cycle= 2 cp= 1.118e-09
   heap_bh31_w37_1 <= pp1_kcmMult_30_d1(37); -- cycle= 2 cp= 1.204e-09
   heap_bh31_w38_1 <= pp1_kcmMult_30_d1(38); -- cycle= 2 cp= 1.29e-09
   heap_bh31_w39_1 <= pp1_kcmMult_30_d1(39); -- cycle= 2 cp= 1.376e-09
   heap_bh31_w40_1 <= pp1_kcmMult_30_d1(40); -- cycle= 2 cp= 1.462e-09
   heap_bh31_w41_1 <= pp1_kcmMult_30_d1(41); -- cycle= 2 cp= 1.548e-09
   heap_bh31_w42_1 <= pp1_kcmMult_30_d1(42); -- cycle= 2 cp= 1.634e-09
   heap_bh31_w43_1 <= pp1_kcmMult_30_d1(43); -- cycle= 2 cp= 1.72e-09
   heap_bh31_w44_1 <= pp1_kcmMult_30_d1(44); -- cycle= 2 cp= 1.806e-09
   heap_bh31_w45_1 <= pp1_kcmMult_30_d1(45); -- cycle= 2 cp= 1.892e-09
   heap_bh31_w46_1 <= pp1_kcmMult_30_d1(46); -- cycle= 2 cp= 1.978e-09
   heap_bh31_w47_1 <= pp1_kcmMult_30_d1(47); -- cycle= 2 cp= 2.064e-09
   ----------------Synchro barrier, entering cycle 3----------------
   heap_bh31_w48_1 <= pp1_kcmMult_30_d2(48); -- cycle= 3 cp= 0
   heap_bh31_w49_1 <= pp1_kcmMult_30_d2(49); -- cycle= 3 cp= 8.6e-11
   heap_bh31_w50_1 <= pp1_kcmMult_30_d2(50); -- cycle= 3 cp= 1.72e-10
   heap_bh31_w51_1 <= pp1_kcmMult_30_d2(51); -- cycle= 3 cp= 2.58e-10
   heap_bh31_w52_1 <= pp1_kcmMult_30_d2(52); -- cycle= 3 cp= 3.44e-10
   heap_bh31_w53_1 <= pp1_kcmMult_30_d2(53); -- cycle= 3 cp= 4.3e-10
   heap_bh31_w54_1 <= pp1_kcmMult_30_d2(54); -- cycle= 3 cp= 5.16e-10
   heap_bh31_w55_1 <= pp1_kcmMult_30_d2(55); -- cycle= 3 cp= 6.02e-10
   heap_bh31_w56_1 <= pp1_kcmMult_30_d2(56); -- cycle= 3 cp= 6.88e-10
   heap_bh31_w57_1 <= pp1_kcmMult_30_d2(57); -- cycle= 3 cp= 7.74e-10
   heap_bh31_w58_1 <= pp1_kcmMult_30_d2(58); -- cycle= 3 cp= 8.6e-10
   heap_bh31_w59_1 <= pp1_kcmMult_30_d2(59); -- cycle= 3 cp= 9.46e-10
   heap_bh31_w60_1 <= pp1_kcmMult_30_d2(60); -- cycle= 3 cp= 1.032e-09
   heap_bh31_w61_1 <= pp1_kcmMult_30_d2(61); -- cycle= 3 cp= 1.118e-09
   heap_bh31_w62_0 <= pp1_kcmMult_30_d2(62); -- cycle= 3 cp= 1.204e-09
   heap_bh31_w63_0 <= pp1_kcmMult_30_d2(63); -- cycle= 3 cp= 1.29e-09
   heap_bh31_w64_0 <= pp1_kcmMult_30_d2(64); -- cycle= 3 cp= 1.376e-09
   heap_bh31_w65_0 <= pp1_kcmMult_30_d2(65); -- cycle= 3 cp= 1.462e-09
   heap_bh31_w66_0 <= pp1_kcmMult_30_d2(66); -- cycle= 3 cp= 1.548e-09
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
      -- All the constant bits are zero, nothing to add

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 3----------------
   ----------------Synchro barrier, entering cycle 4----------------
   finalAdderIn0_bh31 <= "0" & heap_bh31_w66_0_d1 & heap_bh31_w65_0_d1 & heap_bh31_w64_0_d1 & heap_bh31_w63_0_d1 & heap_bh31_w62_0_d1 & heap_bh31_w61_1_d1 & heap_bh31_w60_1_d1 & heap_bh31_w59_1_d1 & heap_bh31_w58_1_d1 & heap_bh31_w57_1_d1 & heap_bh31_w56_1_d1 & heap_bh31_w55_1_d1 & heap_bh31_w54_1_d1 & heap_bh31_w53_1_d1 & heap_bh31_w52_1_d1 & heap_bh31_w51_1_d1 & heap_bh31_w50_1_d1 & heap_bh31_w49_1_d1 & heap_bh31_w48_1_d1 & heap_bh31_w47_1_d2 & heap_bh31_w46_1_d2 & heap_bh31_w45_1_d2 & heap_bh31_w44_1_d2 & heap_bh31_w43_1_d2 & heap_bh31_w42_1_d2 & heap_bh31_w41_1_d2 & heap_bh31_w40_1_d2 & heap_bh31_w39_1_d2 & heap_bh31_w38_1_d2 & heap_bh31_w37_1_d2 & heap_bh31_w36_1_d2 & heap_bh31_w35_1_d2 & heap_bh31_w34_1_d2 & heap_bh31_w33_1_d2 & heap_bh31_w32_1_d2 & heap_bh31_w31_1_d2 & heap_bh31_w30_1_d2 & heap_bh31_w29_1_d2 & heap_bh31_w28_1_d2 & heap_bh31_w27_1_d2 & heap_bh31_w26_1_d2 & heap_bh31_w25_1_d2 & heap_bh31_w24_1_d2 & heap_bh31_w23_1_d2 & heap_bh31_w22_1_d3 & heap_bh31_w21_1_d3 & heap_bh31_w20_1_d3 & heap_bh31_w19_1_d3 & heap_bh31_w18_1_d3 & heap_bh31_w17_1_d3 & heap_bh31_w16_1_d3 & heap_bh31_w15_1_d3 & heap_bh31_w14_1_d3 & heap_bh31_w13_1_d3 & heap_bh31_w12_1_d3 & heap_bh31_w11_1_d3 & heap_bh31_w10_1_d3 & heap_bh31_w9_1_d3 & heap_bh31_w8_1_d3 & heap_bh31_w7_1_d3 & heap_bh31_w6_1_d3 & heap_bh31_w5_1_d3 & heap_bh31_w4_1_d3 & heap_bh31_w3_1_d3 & heap_bh31_w2_1_d3 & heap_bh31_w1_1_d3 & heap_bh31_w0_1_d3;
   finalAdderIn1_bh31 <= "0" & '0' & '0' & '0' & '0' & '0' & heap_bh31_w61_0_d1 & heap_bh31_w60_0_d1 & heap_bh31_w59_0_d1 & heap_bh31_w58_0_d1 & heap_bh31_w57_0_d1 & heap_bh31_w56_0_d1 & heap_bh31_w55_0_d1 & heap_bh31_w54_0_d1 & heap_bh31_w53_0_d1 & heap_bh31_w52_0_d1 & heap_bh31_w51_0_d1 & heap_bh31_w50_0_d1 & heap_bh31_w49_0_d1 & heap_bh31_w48_0_d1 & heap_bh31_w47_0_d2 & heap_bh31_w46_0_d2 & heap_bh31_w45_0_d2 & heap_bh31_w44_0_d2 & heap_bh31_w43_0_d2 & heap_bh31_w42_0_d2 & heap_bh31_w41_0_d2 & heap_bh31_w40_0_d2 & heap_bh31_w39_0_d2 & heap_bh31_w38_0_d2 & heap_bh31_w37_0_d2 & heap_bh31_w36_0_d2 & heap_bh31_w35_0_d2 & heap_bh31_w34_0_d2 & heap_bh31_w33_0_d2 & heap_bh31_w32_0_d2 & heap_bh31_w31_0_d2 & heap_bh31_w30_0_d2 & heap_bh31_w29_0_d2 & heap_bh31_w28_0_d2 & heap_bh31_w27_0_d2 & heap_bh31_w26_0_d2 & heap_bh31_w25_0_d2 & heap_bh31_w24_0_d2 & heap_bh31_w23_0_d2 & heap_bh31_w22_0_d3 & heap_bh31_w21_0_d3 & heap_bh31_w20_0_d3 & heap_bh31_w19_0_d3 & heap_bh31_w18_0_d3 & heap_bh31_w17_0_d3 & heap_bh31_w16_0_d3 & heap_bh31_w15_0_d3 & heap_bh31_w14_0_d3 & heap_bh31_w13_0_d3 & heap_bh31_w12_0_d3 & heap_bh31_w11_0_d3 & heap_bh31_w10_0_d3 & heap_bh31_w9_0_d3 & heap_bh31_w8_0_d3 & heap_bh31_w7_0_d3 & heap_bh31_w6_0_d3 & heap_bh31_w5_0_d3 & heap_bh31_w4_0_d3 & heap_bh31_w3_0_d3 & heap_bh31_w2_0_d3 & heap_bh31_w1_0_d3 & heap_bh31_w0_0_d3;
   finalAdderCin_bh31 <= '0';
   Adder_final31_0: IntAdder_68_f400_uid44  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh31,
                 R => finalAdderOut_bh31   ,
                 X => finalAdderIn0_bh31,
                 Y => finalAdderIn1_bh31);
   ----------------Synchro barrier, entering cycle 5----------------
   -- concatenate all the compressed chunks
   CompressionResult31 <= finalAdderOut_bh31;
   -- End of code generated by BitHeap::generateCompressorVHDL
OutRes <= CompressionResult31(66 downto 0);
   R <= OutRes(66 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_56_f484_uid52
--                    (IntAdderAlternative_56_f484_uid56)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 3 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_56_f484_uid52 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(55 downto 0);
          Y : in  std_logic_vector(55 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(55 downto 0)   );
end entity;

architecture arch of IntAdder_56_f484_uid52 is
signal s_sum_l0_idx0 :  std_logic_vector(23 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(23 downto 0);
signal s_sum_l0_idx2, s_sum_l0_idx2_d1, s_sum_l0_idx2_d2 :  std_logic_vector(10 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1, sum_l0_idx0_d2 :  std_logic_vector(22 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(22 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx2 :  std_logic_vector(9 downto 0);
signal c_l0_idx2 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(23 downto 0);
signal sum_l1_idx1, sum_l1_idx1_d1 :  std_logic_vector(22 downto 0);
signal c_l1_idx1, c_l1_idx1_d1 :  std_logic_vector(0 downto 0);
signal s_sum_l2_idx2 :  std_logic_vector(10 downto 0);
signal sum_l2_idx2 :  std_logic_vector(9 downto 0);
signal c_l2_idx2 :  std_logic_vector(0 downto 0);
signal X_d1 :  std_logic_vector(55 downto 0);
signal Y_d1 :  std_logic_vector(55 downto 0);
signal Cin_d1 :  std_logic;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            s_sum_l0_idx2_d1 <=  s_sum_l0_idx2;
            s_sum_l0_idx2_d2 <=  s_sum_l0_idx2_d1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            sum_l0_idx0_d2 <=  sum_l0_idx0_d1;
            c_l0_idx0_d1 <=  c_l0_idx0;
            sum_l1_idx1_d1 <=  sum_l1_idx1;
            c_l1_idx1_d1 <=  c_l1_idx1;
            X_d1 <=  X;
            Y_d1 <=  Y;
            Cin_d1 <=  Cin;
         end if;
      end process;
   ----------------Synchro barrier, entering cycle 1----------------
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X_d1(22 downto 0)) + ( "0" & Y_d1(22 downto 0)) + Cin_d1;
   s_sum_l0_idx1 <= ( "0" & X_d1(45 downto 23)) + ( "0" & Y_d1(45 downto 23));
   s_sum_l0_idx2 <= ( "0" & X_d1(55 downto 46)) + ( "0" & Y_d1(55 downto 46));
   sum_l0_idx0 <= s_sum_l0_idx0(22 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(23 downto 23);
   sum_l0_idx1 <= s_sum_l0_idx1(22 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(23 downto 23);
   sum_l0_idx2 <= s_sum_l0_idx2(9 downto 0);
   c_l0_idx2 <= s_sum_l0_idx2(10 downto 10);
   ----------------Synchro barrier, entering cycle 2----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(22 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(23 downto 23);
   ----------------Synchro barrier, entering cycle 3----------------
   s_sum_l2_idx2 <=  s_sum_l0_idx2_d2 + c_l1_idx1_d1(0 downto 0);
   sum_l2_idx2 <= s_sum_l2_idx2(9 downto 0);
   c_l2_idx2 <= s_sum_l2_idx2(10 downto 10);
   R <= sum_l2_idx2(9 downto 0) & sum_l1_idx1_d1(22 downto 0) & sum_l0_idx0_d2(22 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                              ExpYTable_10_57
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity ExpYTable_10_57 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(9 downto 0);
          Y : out  std_logic_vector(56 downto 0)   );
end entity;

architecture arch of ExpYTable_10_57 is
   -- Build a 2-D array type for the RoM
   subtype word_t is std_logic_vector(56 downto 0);
   type memory_t is array(0 to 1023) of word_t;
   function init_rom
      return memory_t is 
      variable tmp : memory_t := (
   "100000000000000000000000000000000000000000000000000000000",
   "100000000010000000000100000000000101010101011010101010110",
   "100000000100000000010000000000101010101100000000000010001",
   "100000000110000000100100000010010000000110110000010000010",
   "100000001000000001000000000101010101101010101011101111000",
   "100000001010000001100100001010011011011110110011010000100",
   "100000001100000010010000010010000001101100001000000110111",
   "100000001110000011000100011100101000011101101100001100011",
   "100000010000000100000000101010110000000000100010001011011",
   "100000010010000101000100111100111000100011101101100110011",
   "100000010100000110010001010011100010011000010011000000010",
   "100000010110000111100101101111001101110001011000000100000",
   "100000011000001001000010010000011011000100000011101101010",
   "100000011010001010100110110111101010100111011110010000000",
   "100000011100001100010011100101011100110100110001100000110",
   "100000011110001110001000011010010010000111001000111100111",
   "100000100000010000000101010110101010111011110001110010001",
   "100000100010010010001010011011000111110001111011000111101",
   "100000100100010100010111101000001001001010110110000101001",
   "100000100110010110101100111110001111101001110101111011110",
   "100000101000011001001010011101111011110100010000001101110",
   "100000101010011011110000000111101110010001011100110110110",
   "100000101100011110011101111100000111101010110110010100001",
   "100000101110100001010011111011101000101011111001101101000",
   "100000110000100100010010000110110010000010000110111010001",
   "100000110010100111011000011110000100011101000000101110110",
   "100000110100101010100111000010000000101110001100111111111",
   "100000110110101101111101110011000111101001010100101101100",
   "100000111000110001011100110001111010000100000100001001111",
   "100000111010110101000011111110111000110110001011000010011",
   "100000111100111000110011011010100100111001011100100111011",
   "100000111110111100101011000101011111001001101111110100101",
   "100001000001000000101011000000001000100100111111011001010",
   "100001000011000100110011001011000010001011001010000000010",
   "100001000101001001000011100110101100111110010010011001000",
   "100001000111001101011100010011101010000010011111011110101",
   "100001001001010001111101010010011010011101111100100001001",
   "100001001011010110100110100011011111011000111001001101011",
   "100001001101011011011000000111011001111101101001110101010",
   "100001001111100000010001111110101011011000100111011000001",
   "100001010001100101010100001001110100111000001111101011000",
   "100001010011101010011110101001010111101101000101100001000",
   "100001010101101111110001011101110101001001110000110011011",
   "100001010111110101001100100111101110100010111110101010011",
   "100001011001111010110000000111100101001111100001100100111",
   "100001011100000000011011111101111010101000010001100001011",
   "100001011110000110010000001011010000001000001100000101111",
   "100001100000001100001100110000000111001100010100101000100",
   "100001100010010010010001101101000001010011110100010111100",
   "100001100100011000011111000010011111111111111010100010011",
   "100001100110011110110100110001000100110011111100100001011",
   "100001101000100101010010111001010001010101010101111110011",
   "100001101010101011111001011011100111001011101000111101001",
   "100001101100110010101000011000101000000000011110000100000",
   "100001101110111001011111110000110101011111100100100011111",
   "100001110001000000011111100100110001010110110010100000101",
   "100001110011000111100111110100111101010110000100111010001",
   "100001110101001110111000100001111011001111011111110100000",
   "100001110111010110010001101100001100110111001110011110100",
   "100001111001011101110011010100010100000011100011011110101",
   "100001111011100101011101011010110010101100111000110110111",
   "100001111101101101010000000000001010101101110000001111101",
   "100001111111110101001011000100111110000010110010111111101",
   "100010000001111101001110101001101110101010110010010100100",
   "100010000100000101011010101110111110100110100111011011000",
   "100010000110001101101111010101001111111001010011100111110",
   "100010001000010110001100011101000100101000000000100000000",
   "100010001010011110110010000110111110111010000000000001100",
   "100010001100100111100000010011100000111000101100101011100",
   "100010001110110000010111000011001100101111101001100111011",
   "100010010000111001010110010110100100101100100010110000100",
   "100010010011000010011110001110001010111111001100111101111",
   "100010010101001011101110101010100001111001100110001001110",
   "100010010111010101000111101100001011101111110101011010011",
   "100010011001011110101001010011101010111000001011001011001",
   "100010011011101000010011100001100001101011000001010100011",
   "100010011101110010000110010110010010100010111011010100101",
   "100010011111111100000001110010011111111100100110011000110",
   "100010100010000110000101110110101100010110111001100100110",
   "100010100100010000010010100011011010010010110101111100101",
   "100010100110011010100111111001001100010011100110101100100",
   "100010101000100101000101111000100100111110100001010001101",
   "100010101010101111101100100010000110111011000101100010110",
   "100010101100111010011011110110010100110010111101111001010",
   "100010101111000101010011110101110001010001111111011001010",
   "100010110001010000010100100000111111000110001001111010101",
   "100010110011011011011101111000100000111111101000010001100",
   "100010110101100110101111111100111001110000110000010111001",
   "100010110111110010001010101110101100001110000011010010011",
   "100010111001111101101110001110011011001110001101100000101",
   "100010111100001001011010011100101001101010000110111110010",
   "100010111110010101001111011001111010011100110011001111110",
   "100011000000100001001101000110110000100011100001101010000",
   "100011000010101101010011100011101110111101101101011011001",
   "100011000100111001100010110001011000101100111101110011101",
   "100011000111000101111010110000010000110101000110001110101",
   "100011001001010010011011100000111010011100000110011010111",
   "100011001011011111000101000011111000101010001010100011011",
   "100011001101101011110111011001101110101001101011011000101",
   "100011001111111000110010100010111111100111001110011000110",
   "100011010010000101110110100000001110110001100101111000110",
   "100011010100010011000011010001111111011001110001001101001",
   "100011010110100000011000111000110100110010111100110010111",
   "100011011000101101110111010101010010010010100010010111111",
   "100011011010111011011110100111111011010000001001000100011",
   "100011011101001001001110110001010011000101100101100011011",
   "100011011111010111000111110001111101001110111010001011100",
   "100011100001100101001001101010011101001010010111001000000",
   "100011100011110011010100011011010110011000011010100001110",
   "100011100110000001101000000101001100011011110000100111101",
   "100011101000010000000100101000100010111001010011111000000",
   "100011101010011110101010000101111101011000001101001001001",
   "100011101100101101011000011101111111100001110011110010100",
   "100011101110111100001111110001001101000001101101110101010",
   "100011110001001011010000000000001001100101110000000101101",
   "100011110011011010011001001011011000111101111110010011011",
   "100011110101101001101011010011011110111100101011010011010",
   "100011110111111001000110011000111111010110011001000111011",
   "100011111010001000101010011100011110000001111001001000110",
   "100011111100011000010111011110011110111000001100001111100",
   "100011111110101000001101011111100101110100100010111100111",
   "100100000000111000001100100000010110110100011101100011010",
   "100100000011001000010100100001010101110111101100001111100",
   "100100000101011000100101100011000111000000001111010010011",
   "100100000111101000111111100110001110010010010111001000110",
   "100100001001111001100010101011001111110100100100100101010",
   "100100001100001010001110110010101111101111101000111001001",
   "100100001110011011000011111101010010001110100101111100111",
   "100100010000101100000010001011011011011110101110011010000",
   "100100010010111101001001011101101111101111100101110011100",
   "100100010101001110011001110100110011010011000000101111011",
   "100100010111011111110011010001001010011101000100111111000",
   "100100011001110001010101110011011001100100001001101001001",
   "100100011100000011000001011100000101000000110111010010011",
   "100100011110010100110110001011110001001110001000000110000",
   "100100100000100110110100000011000010101001001000000000010",
   "100100100010111000111011000010011101110001010100110101111",
   "100100100101001011001011001010100111001000011110011110101",
   "100100100111011101100100011100000011010010100110111101101",
   "100100101001110000000110110111010110110110000010101010100",
   "100100101100000010110010011101000110011011011000011011000",
   "100100101110010101100111001101110110101101100001101011111",
   "100100110000101000100101001010001100011001101010101001110",
   "100100110010111011101100010010101100001111010010011011000",
   "100100110101001110111100100111111011000000001011001000011",
   "100100110111100010010110001010011101100000011010000110001",
   "100100111001110101111000111010111000100110010111111101110",
   "100100111100001001100100111001110001001010110000110110111",
   "100100111110011101011010000111101100001000100100100000011",
   "100101000000110001011000100101001110011101000110011001101",
   "100101000011000101100000010010111101000111111101111100001",
   "100101000101011001110001010001011101001011000110100100011",
   "100101000111101110001011100001010011101010101111111010111",
   "100101001010000010101111000011000101101101011101111110011",
   "100101001100010111011011110111011000011100001001001011111",
   "100101001110101100010001111110110001000001111110101000111",
   "100101010001000001010001011001110100101100100000001100100",
   "100101010011010110011010001001001000101011100100101000010",
   "100101010101101011101100001101010010010001010111110001111",
   "100101011000000001000111100110110110110010011010101100110",
   "100101011010010110101100010110011011100101100011110010101",
   "100101011100101100011010011100100110000011111110111101100",
   "100101011111000010010001111001111011101001001101110000110",
   "100101100001011000010010101111000001110011000111100010101",
   "100101100011101110011100111100011110000001111001100101100",
   "100101100110000100110000100010110101111000000111010001000",
   "100101101000011011001101100010101110111010101010001100000",
   "100101101010110001110011111100101110110000110010010101101",
   "100101101101001000100011110001011011000100000110001110100",
   "100101101111011111011101000001011001100000100011000010100",
   "100101110001110110011111101101001111110100011100110010000",
   "100101110100001101101011110101100011110000011110011011100",
   "100101110110100101000001011010111011000111101010000100101",
   "100101111000111100100000011101111011101111011001000100000",
   "100101111011010100001000111111001011011111011100001010110",
   "100101111101101011111010111111010000010001111011101101011",
   "100110000000000011110110011110110000000011010111101110000",
   "100110000010011011111011011110010000110010101000000101011",
   "100110000100110100001001111110011000100000111100101100011",
   "100110000111001100100001111111101101010001111101100101110",
   "100110001001100101000011100010110101001011101011000111100",
   "100110001011111101101110101000010110010110011110000100100",
   "100110001110010110100011010000110110111101000111110101101",
   "100110010000101111100001011100111101001100110010100100000",
   "100110010011001000101001001101001111010101000001010010000",
   "100110010101100001111010100010010011100111110000000100110",
   "100110010111111011010101011100110000011001010100001110010",
   "100110011010010100111001111101001100000000011100010110101",
   "100110011100101110101000000100001100110110010000100101011",
   "100110011111001000011111110010011001010110010010101011100",
   "100110100001100010100001001000010111111110011110001101001",
   "100110100011111100101100000110101111001111001000101010100",
   "100110100110010111000000101110000101101011000001101010011",
   "100110101000110001011110111111000001110111010011000011001",
   "100110101011001100000110111010001010011011100001000100100",
   "100110101101100110111000100000000110000001101010100001110",
   "100110110000000001110011110001011011010110001000111010010",
   "100110110010011100111000101110110001000111110000100100010",
   "100110110100111000000111011000101110000111110000110110000",
   "100110110111010011011111101111111001001001110100001111101",
   "100110111001101111000001110100111001000100000000100100101",
   "100110111100001010101101101000010100101110110111000101110",
   "100110111110100110100011001010110011000101010100101010111",
   "100111000001000010100010011100111011000100110001111100010",
   "100111000011011110101011011111010011101101000011011100110",
   "100111000101111010111110010010100100000000011001110011011",
   "100111001000010111011010110111010011000011100001110100111",
   "100111001010110100000001001110000111111101100100101110000",
   "100111001101010000110001010111101001111000001000001100110",
   "100111001111101101101011010100011111111111001110101010011",
   "100111010010001010101111000101010001100001010111010101001",
   "100111010100100111111100101010100101101111011110011010100",
   "100111010111000101010100000101000011111100111101010000000",
   "100111011001100010110101010101010011011111101010011110010",
   "100111011100000000100000011011111011101111111010001010000",
   "100111011110011110010101011001100100001000011101111101111",
   "100111100000111100010100001110110100000110100101010101000",
   "100111100011011010011100111100010011001001111101100100001",
   "100111100101111000101111100010101000110100110010000011111",
   "100111101000010111001100000010011100101011101100011010011",
   "100111101010110101110010011100010110010101110100100101101",
   "100111101101010100100010110000111101011100110001000100101",
   "100111101111110011011101000000111001101100100111000010001",
   "100111110010010010100001001100110010110011111010011110000",
   "100111110100110001101111010101010000100011101110010111011",
   "100111110111010001000111011010111010101111100100110110011",
   "100111111001110000101001011110011001001101011111010110101",
   "100111111100010000010101100000010011110101111110110000010",
   "100111111110110000001011100001010010100100000011100011001",
   "101000000001010000001011100001111101010101001101111111011",
   "101000000011110000010101100010111100001001011110010000111",
   "101000000110010000101001100100110111000011010100100111111",
   "101000001000110001000111101000010110000111110001100100001",
   "101000001011010001101111101110000001011110010101111110001",
   "101000001101110010100001110110100001010001000011010001101",
   "101000010000010011011110000010011101101100011011100111011",
   "101000010010110100100100010010011110111111100001111111010",
   "101000010101010101110100100111001101011011111010011010100",
   "101000010111110111001111000001010001010101101010000101101",
   "101000011010011000110011100001010011000011010111100010010",
   "101000011100111010100010000111111010111110001010110001111",
   "101000011111011100011010110101110001100001101101011111001",
   "101000100001111110011101101011011111001100001011001000101",
   "101000100100100000101010101001101100011110010001001010101",
   "101000100111000011000001110001000001111011001111001001010",
   "101000101001100101100011000010001000001000110110111010101",
   "101000101100001000001110011101100111101111011100110001010",
   "101000101110101011000100000100001001011001110111100101111",
   "101000110001001110000011110110010101110101100001000001110",
   "101000110011110001001101110100110101110010010101101000110",
   "101000110110010100100010000000010010000010110101000011111",
   "101000111000111000000000011001010011011100000010001010111",
   "101000111011011011101001000000100010110101100011001111001",
   "101000111101111111011011110110101001001001100010000101001",
   "101001000000100011011000111100001111010100101100001111100",
   "101001000011000111100000010001111110010110010011001000100",
   "101001000101101011110001111000011111010000001100001100110",
   "101001001000010000001101110000011011000110110001000101100",
   "101001001010110100110011111010011011000000111111110010011",
   "101001001101011001100100010111001000001000011010110100010",
   "101001001111111110011111000111001011101001001001010111011",
   "101001010010100011100100001011001110110001110111011101011",
   "101001010101001000110011100011111010110011110110000111111",
   "101001010111101110001101010001111001000010111011100010110",
   "101001011010010011110001010101110010110101100011001110100",
   "101001011100111001011111110000010001100100101110001010100",
   "101001011111011111011000100001111110101100000010111111001",
   "101001100010000101011011101011100011101001101110001000100",
   "101001100100101011101001001101101001111110100010000000111",
   "101001100111010010000001001000111011001101110111001010101",
   "101001101001111000100011011110000000111101101100011011000",
   "101001101100011111010000001101100100110110100111000100010",
   "101001101111000110000111011000010000100011110011000000001",
   "101001110001101101001000111110101101110011000010111010101",
   "101001110100010100010101000001100110010100110000011011101",
   "101001110110111011101011100001100011111011111100010010100",
   "101001111001100011001100011111010000011110001110011111010",
   "101001111100001010110111111011010101110011110110011110000",
   "101001111110110010101101110110011101110111101011010000111",
   "101010000001011010101110010001010010100111001011101010111",
   "101010000100000010111001001100011110000010011110011001111",
   "101010000110101011001110101000101010001100010010010001111",
   "101010001001010011101110100110100001001001111110010110101",
   "101010001011111100011001000110101101000011100010000110110",
   "101010001110100101001110001001111000000011100101100110001",
   "101010010001001110001101110000101100010111011001101000010",
   "101010010011110111010111111011110100001110110111111011011",
   "101010010110100000101100101011111001111100100011010010001",
   "101010011001001010001100000001100111110101100111101110111",
   "101010011011110011110101111101101000010001111010101101111",
   "101010011110011101101010100000100101101011111011010000011",
   "101010100001000111101001101011001010100000110010000110011",
   "101010100011110001110011011110000001010000010001111010000",
   "101010100110011100000111111001110100011100110111011001111",
   "101010101001000110100110111111001110101011101001100011110",
   "101010101011110001010000101110111010100100011001101111000",
   "101010101110011100000101001001100010110001100011110111011",
   "101010110001000111000100001111110010000000001110101000000",
   "101010110011110010001110000010010011000000001011100101100",
   "101010110110011101100010100001110000100011110111011001000",
   "101010111001001001000001101110110101100000011001111010110",
   "101010111011110100101011101010001100101101100110011101000",
   "101010111110100000100000010100100001000101111011110110010",
   "101011000001001100011111101110011101100110100100101100110",
   "101011000011111000101001111000101101001111010111100000010",
   "101011000110100100111110110011111011000010110110110101111",
   "101011001001010001011110100000110010000110010001100001101",
   "101011001011111110001000111111111101100001100010110010011",
   "101011001110101010111110010010001000011111010010011011110",
   "101011010001010111111110010111111110001100110101000001101",
   "101011010100000101001001010010001001111010001100000010001",
   "101011010110110010011111000001010110111010000110000001000",
   "101011011001011111111111100110010000100001111110110010101",
   "101011011100001101101011000001100010001001111111100110010",
   "101011011110111011100001010011110111001100111111010001001",
   "101011100001101001100010011101111011001000100010011001100",
   "101011100100010111101110100000011001011100111011100001001",
   "101011100111000110000101011011111101101101001011010000101",
   "101011101001110100100111010001010011011111000000100001111",
   "101011101100100011010100000001000110011010111000101011010",
   "101011101111010010001011101100000010001011111111101010100",
   "101011110010000001001110010010110010100000010000001111010",
   "101011110100110000011011110110000011001000010100000110110",
   "101011110111011111110100010110011111110111100100000110000",
   "101011111010001111010111110100110100100100001000010100111",
   "101011111100111111000110010001101101000110111000011001110",
   "101011111111101110111111101101110101011011011011100011101",
   "101100000010011111000100001001111001100000001000110101010",
   "101100000101001111010011100110100101010110000111010000111",
   "101100000111111111101110000100100101000001001110000010010",
   "101100001010110000010011100100100100101000000100101010010",
   "101100001101100001000100000111010000010100000011001001100",
   "101100010000010001111111101101010100010001010010001100001",
   "101100010011000011000110010111011100101110101011010011111",
   "101100010101110100011000000110010101111101111001000011111",
   "101100011000100101110100111010101100010011010111001011010",
   "101100011011010111011100110101001100000110010010110000101",
   "101100011110001001001111110110100001110000101010011100110",
   "101100100000111011001101111111011001101111001110100110000",
   "101100100011101101010111010000100000100001100001011011011",
   "101100100110011111101011101010100010101001110111001111100",
   "101100101001010010001011001110001100101101010110100011110",
   "101100101100000100110101111100001011010011111000010011101",
   "101100101110110111101011110101001011001000000111111111101",
   "101100110001101010101100111001111000110111100011111000110",
   "101100110100011101111001001011000001010010011101001011010",
   "101100110111010001010000101001010001001011111000001010010",
   "101100111010000100110011010101010101011001101100011010101",
   "101100111100111000100001001111111010110100100100111110100",
   "101100111111101100011010011001101110011000000000011111111",
   "101101000010100000011110110011011101000010010001011100111",
   "101101000101010100101110011101110011110100011110010001111",
   "101101001000001001001001011001011111110010100001100101101",
   "101101001010111101101111100111001110000011001010010100000",
   "101101001101110010100001000111101011101111111011111001101",
   "101101010000100111011101111011100110000101001110011110110",
   "101101010011011100100110000011101010010010001111000011000",
   "101101010110010001111001100000100101101000111111101000001",
   "101101011001000111011000010011000101011110010111011110010",
   "101101011011111101000010011011110111001010000011001110001",
   "101101011110110010110111111011101000000110100101000101000",
   "101101100001101000111000110011000101110001010101000000010",
   "101101100100011111000101000010111101101010100000111000001",
   "101101100111010101011100101011111101010101001100101011100",
   "101101101010001011111111101110110010010111010010101011011",
   "101101101101000010101110001100001010011001100011100110000",
   "101101101111111001101000000100110011000111100110110010001",
   "101101110010110000101101011001011010001111111010011011000",
   "101101110101100111111110001010101101100011110011101011011",
   "101101111000011111011010011001011010110111011110111001001",
   "101101111011010111000010000110010000000001111111110000010",
   "101101111110001110110101010001111010111101010001011111010",
   "101110000001000110110011111101001001100110000111000001110",
   "101110000011111110111110001000101001111100001011001100010",
   "101110000110110111010011110101001010000010000000111000000",
   "101110001001101111110101000011010111111101000011001101111",
   "101110001100101000100001110100000001110101100101110010011",
   "101110001111100001011010000111110101110110110100110000110",
   "101110010010011010011101111111100010001110110101000111001",
   "101110010101010011101101011011110101001110100100110001101",
   "101110011000001101001000011101011101001001111010110101110",
   "101110011011000110101111000101001000010111100111101110101",
   "101110011110000000100001010011100101010001010101010111111",
   "101110100000111010011111001001100010010011100111011001111",
   "101110100011110100101000100111101101111101111011010100110",
   "101110100110101110111101101110110110110010101000101100011",
   "101110101001101001011110011111101011010111000001010100000",
   "101110101100100100001010111010111010010011010001011001101",
   "101110101111011111000011000001010010010010011111110010000",
   "101110110010011010000110110011100010000010101110000100001",
   "101110110101010101010110010010011000010100111000110100111",
   "101110111000010000110001011110100011111100110111110010111",
   "101110111011001100011000011000110011110001011110000001111",
   "101110111110001000001011000001110110101100011010000110111",
   "101111000001000100001001011010011011101010010110010011101",
   "101111000100000000010011100011010001101010111000110010011",
   "101111000110111100101001011101000111110000100011110001100",
   "101111001001111001001011001000101101000000110101101111100",
   "101111001100110101111000100110110000100100001001100110110",
   "101111001111110010110001111000000001100101110110111001001",
   "101111010010101111110110111101001111010100010001111011110",
   "101111010101101101000111110111001001000000101100000011010",
   "101111011000101010100100100110011101111111010011101110110",
   "101111011011101000001101001011111101100111010100110100110",
   "101111011110100110000001101000010111010010111000101110000",
   "101111100001100100000001111100011010011111000110100001111",
   "101111100100100010001110001000110110101100000011010010010",
   "101111100111100000100110001110011011011100110010000111010",
   "101111101010011111001010001101111000010111010100011010111",
   "101111101101011101111010000111111101000100101010000101011",
   "101111110000011100110101111101011001010000110001101001000",
   "101111110011011011111101101110111100101010101000011101110",
   "101111110110011011010001011101010111000100001010111101100",
   "101111111001011010110001001001011000010010010100101111111",
   "101111111100011010011100110011110000001101000000110110000",
   "101111111111011010010100011101001110101111001001110111000",
   "110000000010011010011000000110100011110110101010001011101",
   "110000000101011010100111110000011111100100011100001001111",
   "110000001000011011000011011011110001111100011010010001111",
   "110000001011011011101011001001001011000101011111011001010",
   "110000001110011100011110111001011011001001100110110111001",
   "110000010001011101011110101101010010010101101100110000100",
   "110000010100011110101010100101100000111001101110000100011",
   "110000010111100000000010100010110111001000101000110111011",
   "110000011010100001100110100110000101011000011100011111111",
   "110000011101100011010110101111111100000010001001110010101",
   "110000100000100101010011000001001011100001110011001110010",
   "110000100011100111011011011010100100010110011101000111101",
   "110000100110101001101111111100110111000010001101110101111",
   "110000101001101100010000101000110100001010001101111110111",
   "110000101100101110111101011111001100010110101000100010110",
   "110000101111110001110110100000110000010010101011001000101",
   "110000110010110100111011101110010000101100100110001010100",
   "110000110101111000001101001000011110010101101101000001100",
   "110000111000111011101010110000001010000010010110010010000",
   "110000111011111111010100100110000100101001111011111000001",
   "110000111111000011001010101010111111000110111011010011010",
   "110001000010000111001100111111101010010110110101110011010",
   "110001000101001011011011100100110111011010010000100011111",
   "110001001000001111110110011011010111010100110100111001011",
   "110001001011010100011101100011111011001101010000011100111",
   "110001001110011001010000111111010100001101010101011000001",
   "110001010001011110010000101110010011100001111010100010110",
   "110001010100100011011100110001101010011010111011101101010",
   "110001010111101000110101001010001010001011011001101110101",
   "110001011010101110011001111000100100001001011010101111110",
   "110001011101110100001010111101101001101110001010011000001",
   "110001100000111010001000011010001100010101111001111010010",
   "110001100100000000010010001110111101100000000000011111110",
   "110001100111000110101000011100101110101110111011010101111",
   "110001101010001101001011000100010001101000001101111010001",
   "110001101101010011111010000110010111110100100010000110010",
   "110001110000011010110101100011110010111111101000011100101",
   "110001110011100001111101011101010100111000011000010101011",
   "110001110110101001010001110011101111010000110000001001110",
   "110001111001110000110010100111110011111101110101100001100",
   "110001111100111000011111111010010100110111110101011110111",
   "110010000000000000011001101100000011111010000100101011000",
   "110010000011001000011111111101110011000010111111100010111",
   "110010000110010000110010110000010100010100001010100011001",
   "110010001001011001010010000100011001110010010010010101010",
   "110010001100100001111101111010110101100101001011111011110",
   "110010001111101010110110010100011001110111110100111110010",
   "110010010010110011111011010001111000111000010011110111010",
   "110010010101111101001100110100000100110111110111111111011",
   "110010011001000110101010111011110000001010111001111010110",
   "110010011100010000010101101001101101001000111011100101010",
   "110010011111011010001100111110101110001100101000011111000",
   "110010100010100100010000111011100101110011110101111001100",
   "110010100101101110100001100001000110011111100011000011101",
   "110010101000111000111110110000000010110011111001010110100",
   "110010101100000011101000101001001101011000001100100010100",
   "110010101111001110011111001101011000110110111010111011010",
   "110010110010011001100010011101010111111101101101100100100",
   "110010110101100100110010011001111101011101011000011111010",
   "110010111000110000001111000011111100001001111010110101111",
   "110010111011111011111000011100000110111010011111001001010",
   "110010111111000111101110100011010000101001011011011100111",
   "110011000010010011110001011010001100010100010001100100011",
   "110011000101100000000001000001101100111011101111001111110",
   "110011001000101100011101011010100101100011101110011000001",
   "110011001011111001000110100101101001010011010101001100111",
   "110011001111000101111100100011101011010100110110011111111",
   "110011010010010010111111010101011110110101110001110010110",
   "110011010101100000001110111011110111000110110011100011011",
   "110011011000101101101011010111100111011011110101011001000",
   "110011011011111011010100101001100011001011111110010000110",
   "110011011111001001001010110010011101110001100010101010101",
   "110011100010010111001101110011001010101010000100110110010",
   "110011100101100101011101101100011101010110010101000000000",
   "110011101000110011111010011111001001011010010001011101101",
   "110011101100000010100100001100000010011101000110111011010",
   "110011101111010001011010110011111100001001010000101000001",
   "110011110010100000011110010111101010001100011000100011110",
   "110011110101101111101110111000000000010111010111101010111",
   "110011111000111111001100010101110010011110010110000100000",
   "110011111100001110110110110001110100011000101011001100101",
   "110011111111011110101110001100111010000000111110000110100",
   "110100000010101110110010100111110111010101000101100100000",
   "110100000101111111000100000011100000010110001000010101101",
   "110100001001001111100010100000101001001000011101010110101",
   "110100001100100000001110000000000101110011101011111010011",
   "110100001111110001000110100010101010100010101011111001000",
   "110100010011000010001100001001001011100011100101111100111",
   "110100010110010011011110110100011101000111110011101111011",
   "110100011001100100111110100101010011100100000000000101111",
   "110100011100110110101011011100100011010000000111001111001",
   "110100100000001000100101011011000000100111010111000000001",
   "110100100011011010101100100001100000001000001111000001011",
   "110100100110101101000000110000110110010100100000111011110",
   "110100101001111111100010001001110111110001010000100101111",
   "110100101101010010010000101101011001000110110100010001011",
   "010011011010001011001011111100011011111001011000001010000",
   "010011011011011000110111000100100000010011101100011001011",
   "010011011100100110100111000011011010111011111000001111011",
   "010011011101110100011011111001011111001101111011011010111",
   "010011011111000010010101100111000000100111000011011101100",
   "010011100000010000010100001100010010100101101011101111111",
   "010011100001011110010111101001101000101001011101100111001",
   "010011100010101100011111111111010110010011010000011000111",
   "010011100011111010101101001101101111000101001001100001010",
   "010011100101001000111111010101000110100010011100100110101",
   "010011100110010111010110010101110000001111101011011111010",
   "010011100111100101110010001111111111110010100110010110010",
   "010011101000110100010011000100001000110010001011101111101",
   "010011101010000010111000110010011110110110101000101110001",
   "010011101011010001100011011011010101101001011000110111110",
   "010011101100100000010010111111000000110101000110011010100",
   "010011101101101111000111011101110100000101101010010001101",
   "010011101110111110000000111000000011001000001100001010010",
   "010011110000001100111111001110000001101011000010101000110",
   "010011110001011100000010100000000011011101110011001101001",
   "010011110010101011001010101110011100010001010010011000001",
   "010011110011111010010111111001011111110111100011110000011",
   "010011110101001001101010000001100010000011111010000111010",
   "010011110110011001000001000110110110101010110111011101110",
   "010011110111101000011101001001110001100010001101001001011",
   "010011111000110111111110001010100110100000111011111001010",
   "010011111010000111100100001001101001011111010011111011000",
   "010011111011010111001111000111001110010110110100111111111",
   "010011111100100110111111000011101001000010001110100001011",
   "010011111101110110110011111111001101011101011111100110110",
   "010011111111000110101101111010001111100101110111001001011",
   "010100000000010110101100110101000011011001110011111010011",
   "010100000001100110110000101111111100111001000100100110111",
   "010100000010110110111001101011010000000100100111111101111",
   "010100000100000111000111100111010000111110101100110100101",
   "010100000101010111011010100100010011101010110010001011100",
   "010100000110100111110010100010101100001101100111010011110",
   "010100000111111000001111100010101110101101001011110100000",
   "010100001001001000110001100100101111010000101111101101010",
   "010100001010011001011000101001000010000000110011011111111",
   "010100001011101010000100101111111011000111001000010001010",
   "010100001100111010110101111001101110101110101111110000000",
   "010100001110001011101100000110110001000011111100011001010",
   "010100001111011100100111010111010110010100010001011110010",
   "010100010000101101100111101011110010101110100011001000101",
   "010100010001111110101101000100011010100010110110100000000",
   "010100010011001111110111100001100010000010100001101110101",
   "010100010100100001000111000011011101100000001100000110110",
   "010100010101110010011011101010100001001111101110000111111",
   "010100010111000011110101010111000001100110010001100011010",
   "010100011000010101010100001001010010111010010001100001010",
   "010100011001100110111000000001101001100011011010100110111",
   "010100011010111000100001000000011001111010101010111010000",
   "010100011100001010001111000101111000011010010010000111010",
   "010100011101011100000010010010011001011101110001100110110",
   "010100011110101101111010100110010001100001111100100001000",
   "010100011111111111111000000001110101000100110111110100101",
   "010100100001010001111010100101011000100101111010011010110",
   "010100100010100100000010010001010000100101101101001100111",
   "010100100011110110001111000101110001100110001011001001010",
   "010100100101001000100001000011010000001010100001011000101",
   "010100100110011010111000001010000000110111001111010011010",
   "010100100111101101010100011010011000010010000110100101011",
   "010100101000111111110101110100101011000010001011010101101",
   "010100101010010010011100011001001101101111110100001000111",
   "010100101011100101001000001000010101000100101010001000010",
   "010100101100110111111001000010010101101011101001000110010",
   "010100101110001010101111000111100100010000111111100011010",
   "010100101111011101101010011000010101100010001110110011010",
   "010100110000110000101010110100111110001110001011000011010",
   "010100110010000011110000011101110011000100111011011101110",
   "010100110011010110111011010011001000110111111010010000010",
   "010100110100101010001011010101010100011001110100110000111",
   "010100110101111101100000100100101010011110101011100010110",
   "010100110111010000111011000001011111111011110010011011111",
   "010100111000100100011010101100001001100111110000101010001",
   "010100111001110111111111100100111100011010100000111000010",
   "010100111011001011101001101100001101001101010001010011100",
   "010100111100011111011001000010010000111010100011110000100",
   "010100111101110011001101100111011100011110001101110000011",
   "010100111111000111000111011100000100110101011000100110110",
   "010101000000011011000110100000011110111110100001011110001",
   "010101000001101111001010110100111111111001011001011101010",
   "010101000011000011010100011001111100100111000101101101000",
   "010101000100010111100011001111101010001001111111011100111",
   "010101000101101011110111010110011101100101110100001000110",
   "010101000111000000010000101110101011111111100101011101111",
   "010101001000010100101111011000101010011101101001100000011",
   "010101001001101001010011010100101110000111101010110000010",
   "010101001010111101111100100011001100000110101000001111000",
   "010101001100010010101011000100011001100100110101100100010",
   "010101001101100111011110111000101011101101111011000100001",
   "010101001110111100011000000000010111101110110101110011011",
   "010101010000010001010110011011110010110101110111101101100",
   "010101010001100110011010001011010010010010100111101001111",
   "010101010010111011100011001111001011010110000001100000101",
   "010101010100010000110001100111110011010010010110010000101",
   "010101010101100110000101010101011111011011001100000100001",
   "010101010110111011011110011000100101000101011110010110101",
   "010101011000010000111100110001011001100111011101111010001",
   "010101011001100110100000100000010010011000110000111100000",
   "010101011010111100001001100101100100110010010011001011001",
   "010101011100010001111000000001100110001110010101111100011",
   "010101011101100111101011110100101100001000100000010000101",
   "010101011110111101100100111111001011111101101110111001110",
   "010101100000010011100011100001011011001100010100100000010",
   "010101100001101001100111011011101111010011111001101000010",
   "010101100010111111110000101110011101110101011100110111011",
   "010101100100010101111111011001111100010011010010111001100",
   "010101100101101100010011011110100000010001000110100111000",
   "010101100111000010101100111100011111010011111001001001000",
   "010101101000011001001011110100001111000010000001111111111",
   "010101101001101111110000000110000101000011001111000111111",
   "010101101011000110011001110010010111000000100100111111001",
   "010101101100011101001000111001011010100100011110101010100",
   "010101101101110011111101011011100101011010101101111011011",
   "010101101111001010110111011001001101010000011011010100111",
   "010101110000100001110110110010100111110100000110010001011",
   "010101110001111000111011101000001010110101100101001000000",
   "010101110011010000000101111010001100000110000101010010000",
   "010101110100100111010101101001000001011000001011010000000",
   "010101110101111110101010110101000000011111110010110000000",
   "010101110111010110000101011110011111010010001110110010001",
   "010101111000101101100101100101110011100110001001101110100",
   "010101111010000101001011001011010011010011100101011010101",
   "010101111011011100110110001111010100010011111011001111000",
   "010101111100110100100110110010001100100001111100001100001",
   "010101111110001100011100110100010001111001110001000000111",
   "010101111111100100011000010101111010011000111010001110101",
   "010110000000111100011001010111011011111110010000010000010",
   "010110000010010100011111111001001100101010000011011110101",
   "010110000011101100101011111011100010011101111100010110001",
   "010110000101000100111101011110110011011100111011011100111",
   "010110000110011101010100100011010101101011011001100111100",
   "010110000111110101110001001001011111001111000111111111000",
   "010110001001001110010011010001100110001111010000000110010",
   "010110001010100110111010111100000000110100010011111111100",
   "010110001011111111101000001001000101001000001110010001111",
   "010110001101011000011010111001001001010110010010001110110",
   "010110001110110001010011001100100011101011001011110111111",
   "010110010000001010010001000011101010010101000000000100001",
   "010110010001100011010100011110110011100011001100100101101",
   "010110010010111100011101011110010101100110101000001111010",
   "010110010100010101101100000010100110110001100010111001111",
   "010110010101101111000000001011111101010111100101101010011",
   "010110010111001000011001111010101111101101110010110110110",
   "010110011000100001111001001111010100001010100110001100001",
   "010110011001111011011110001010000001000101110100110100001",
   "010110011011010101001000101011001100111000101101011010010",
   "010110011100101110111000110011001101111101111000010010000",
   "010110011110001000101110100010011010110001010111011100010",
   "010110011111100010101001111001001001110000100110101100101",
   "010110100000111100101010110111110001011010011011101111010",
   "010110100010010110110001011110101000001111000110001110100",
   "010110100011110000111101101110000100110000001111111000100",
   "010110100101001011001111100110011101100000111100100101001",
   "010110100110100101100111001000001001000101101010011010110",
   "010110101000000000000100010011011110000100010001110100111",
   "010110101001011010100111001000110011000100000101101001001",
   "010110101010110101001111101000011110101101110011001101011",
   "010110101100001111111101110010110111101011100010011100111",
   "010110101101101010110001101000010100101000110101111110011",
   "010110101111000101101011001001001100010010101011001001101",
   "010110110000100000101010010101110101010111011010001100111",
   "010110110001111011101111001110100110100110110110010011001",
   "010110110011010110111001110011110110110010001101101000111",
   "010110110100110010001010000101111100101100001001100010111",
   "010110110110001101100000000101001111001000101110100010111",
   "010110110111101000111011110010000100111101011100011110001",
   "010110111001000100011101001100110101000001001110100010100",
   "010110111010100000000100010101110110001100011011011100011",
   "010110111011111011110001001101011111011000110101011100110",
   "010110111101010111100011110100000111100001101010011110010",
   "010110111110110011011100001010000101100011100100001011011",
   "010111000000001111011010001111110000011100101000000100010",
   "010111000001101011011110000101011111001100010111100100000",
   "010111000011000111100111101011101000110011110000000110101",
   "010111000100100011110111000010100100010101001011001111010",
   "010111000110000000001100001010101000110100011110101101001",
   "010111000111011100100111000100001101010110111100100010000",
   "010111001000111001000111101111101001000011010011000111010",
   "010111001010010101101110001101010011000001101101010100011",
   "010111001011110010011010011101100010011011110010100100011",
   "010111001101001111001100100000101110011100100110111011101",
   "010111001110101100000100010111001110010000101011001101100",
   "010111010000001001000010000001011001000101111101000010100",
   "010111010001100110000101011111100110001011110110111101111",
   "010111010011000011001110110010001100110011010000100011011",
   "010111010100100000011101111001100100001110011110011101000",
   "010111010101111101110010110110000011110001010010100001010",
   "010111010111011011001101101000000010110000111011111000011",
   "010111011000111000101110001111111000100100000111000010101",
   "010111011010010110010100101101111100100010111101111110001",
   "010111011011110100000001000010100110000111001000001100001",
   "010111011101010001110011001110001100101011101010110111100",
   "010111011110101111101011010001000111101101001000111010101",
   "010111100000001101101001001011101110101001100011000100100",
   "010111100001101011101100111110011001000000010111111111100",
   "010111100011001001110110101001011110010010100100010110101",
   "010111100100101000000110001101010110000010100010111011101",
   "010111100110000110011011101010010111110100001100101101001",
   "010111100111100100110111000000111011001100111000111011111",
   "010111101001000011011000010001010111110011011101010001011",
   "010111101010100001111111011100000101010000001101110101001",
   "010111101100000000101100100001011011001100111101010011000",
   "010111101101011111011111100001110001010100111101000001000",
   "010111101110111110011000011101011111010100111101000101010",
   "010111110000011101010111010100111100111011001100011011101",
   "010111110001111100011100001000100001110111011000111100000",
   "010111110011011011100110111000100101111010101111100000001",
   "010111110100111010110111100101100000110111111100001001101",
   "010111110110011010001110001111101010100011001010000111101",
   "010111110111111001101010110111011010110010000011111101001",
   "010111111001011001001101011101001001011011110011100110100",
   "010111111010111000110110000001001110011001000010100000001",
   "010111111100011000100100100100000001100011111001101011101",
   "010111111101111000011001000101111010111000000001110110010",
   "010111111111011000010011100111010010010010100011011111000",
   "011000000000111000010100001000011111110010000110111100000",
   "011000000010011000011010101001111011010110110100100001011",
   "011000000011111000100111001011111101000010010100100110010",
   "011000000101011000111001101110111100110111101111101011110",
   "011000000110111001010010010011010010111011101110100010010",
   "011000001000011001110000111001010111010100011010001111110",
   "011000001001111010010101100001100010001001011100010101111",
   "011000001011011011000000001100001011100011111110110111111",
   "011000001100111011110000111001101011101110101100100000101",
   "011000001110011100100111101010011010110101110000101000100",
   "011000001111111101100100011110110001000110110111011100000",
   "011000010001011110100111010111000110110001001110000001001",
   "011000010010111111110000010011110100000101100010011101101",
   "011000010100100000111111010101010001010110000011111101101",
   "011000010110000010010100011011110110110110100010111000101",
   "011000010111100011101111100111111100111100010000111000111",
   "011000011001000101010000111001111011111110000001000000001",
   "011000011010100110111000010010001100010100000111101110110",
   "011000011100001000100101110001000110011000011011001001010",
   "011000011101101010011001010111000010100110010010111110110",
   "011000011111001100010011000100011001011010101000101110111",
   "011000100000101110010010111001100011010011110111101111101",
   "011000100010010000011000110110111000110001111101010100010",
   "011000100011110010100100111100110010010110011000110010011",
   "011000100101010100110111001011101000100100001011101001001",
   "011000100110110111001111100011110011111111111001100110011",
   "011000101000011001101110000101101101001111101000101101101",
   "011000101001111100010010110001101100111011000001011101101",
   "011000101011011110111101101000001011101011001110110110110",
   "011000101101000001101110101001100010001010111110100001011",
   "011000101110100100100101110110001001000110100000110011101",
   "011000110000000111100011001110011001001011101000110111100",
   "011000110001101010100110110010101011001001101100110001111",
   "011000110011001101110000100011010111110001100101100111100",
   "011000110100110001000000100000110111110101101111100100010",
   "011000110110010100010110101011100100001010001010000000011",
   "011000110111110111110011000011110101100100010111100111110",
   "011000111001011011010101101010000100111011011110011111001",
   "011000111010111110111110011110101011001000001000001010111",
   "011000111100100010101101100010000001000100100001110101001",
   "011000111110000110100010110100011111101100011100010011110",
   "011000111111101010011110010110011111111101001100001111000",
   "011001000001001110100000001000011010110101101010000111100",
   "011001000010110010101000001010101001010110010010011100011",
   "011001000100010110110110011101100100100001000101110010000",
   "011001000101111011001011000001100101011001101000110111011",
   "011001000111011111100101110111000101000101000100101101100",
   "011001001001000100000110111110011100101010000110101100110",
   "011001001010101000101110011000000101010001000000101011100",
   "011001001100001101011100000100011000000011101001000100011",
   "011001001101110010010000000011101110001101011010111100110",
   "011001001111010111001010010110100000111011010110001010100",
   "011001010000111100001010111101001001011011111111011011000",
   "011001010010100001010001111000000000111111100000011000111",
   "011001010100000110011111000111100000110111100111110010100",
   "011001010101101011110010101100000010010111101001100000011",
   "011001010111010001001100100101111110110100011110101011011",
   "011001011000110110101100110101101111100100100101110011001",
   "011001011010011100010011011011101110000000000010110100100",
   "011001011100000010000000011000010011100000011111001111011",
   "011001011101100111110011101011111001100001001010001101111",
   "011001011111001101101101010110111001011110111000101010000",
   "011001100000110011101101011001101100111000000101010100010",
   "011001100010011001110011110100101101001100110000111001111",
   "011001100100000000000000101000010011111110100010001011110",
   "011001100101100110010011110100111010110000100110000011111",
   "011001100111001100101101011010111011000111101111101100100",
   "011001101000110011001101011010101110101010011000100110011",
   "011001101010011001110011110100101111000000100000101110111",
   "011001101100000000100000101001010101110011101110100110100",
   "011001101101100111010011111000111100101111001111010111101",
   "011001101111001110001101100011111101011111110110111100100",
   "011001110000110101001101101010110001110100000000000101111",
   "011001110010011100010100001101110011011011101100100001010",
   "011001110100000011100001001101011100001000100100111111101",
   "011001110101101010110100101010000101101101111001011011101",
   "011001110111010010001110100100001010000000100001000000011",
   "011001111000111001101110111100000010110110111010001111011",
   "011001111010100001010101110010001010001001001011000111100",
   "011001111100001001000011000110111001110001000001001010111",
   "011001111101110000110110111010101011101001110001100110001",
   "011001111111011000110001001101111001110000011001010110001",
   "011010000001000000110010000000111110000011011101001111001",
   "011010000010101000111001010100010010100011001010000010101",
   "011010000100010001000111001000010001010001010100100110010",
   "011010000101111001011011011101010100010001011001111010010",
   "011010000111100001110110010011110101101000011111010000000",
   "011010001001001010010111101100001111011101010010010000011",
   "011010001010110010111111100110111011111000001001000010011",
   "011010001100011011101110000100010101000011000010010001100",
   "011010001110000100100011000100110101001001100101010100110",
   "011010001111101101011110101000110110011001000010010100101",
   "011010010001010110100000110000110011000000010010010010000",
   "011010010010111111101001011101000101001111110111001100101",
   "011010010100101000111000101110000111011001111100001001100",
   "011010010110010010001110100100010011110010010101011001111",
   "011010010111111011101011000000000100101110100000100001001",
   "011010011001100101001110000001110100100101100100011100011",
   "011010011011001110110111101001111101110000010001100111111",
   "011010011100111000100111111000111010101001000010000110100",
   "011010011110100010011110101111000101101011111001101000010",
   "011010100000001100011100001100111001010110100101110000001",
   "011010100001110110100000010010110000001000011101111011110",
   "011010100011100000101011000001000100100010100011101001011",
   "011010100101001010111100011000010001000111100010011110101",
   "011010100110110101010100011000110000011011110000001111010",
   "011010101000011111110011000010111101000101001101000011101",
   "011010101010001010011000010111010001101011100011011111011",
   "011010101011110101000100010110001000111000001000101000010",
   "011010101101011111110110111111111101010101111100001100101",
   "011010101111001010110000010101001001110001101000101010010",
   "011010110000110101110000010110001000111001100011010100101",
   "011010110010100000110111000011010101011101101100011100010",
   "011010110100001100000100011101001010001111101111010100101",
   "011010110101110111011000100100000010000011000010011011110",
   "011010110111100010110011011000010111101100100111011111111",
   "011010111001001110010100111010100110000011001011100111010",
   "011010111010111001111101001011000111111111000111010101110",
   "011010111100100101101100001010011000011010011110110100101",
   "011010111110010001100001111000110010010001000001111000100",
   "011010111111111101011110010110110000100000001100001000010",
   "011011000001101001100001100100101110000111000101000100010",
   "011011000011010101101011100011000110000110100000001100010",
   "011011000101000001111100010010010011100000111101000110111",
   "011011000110101110010011110010110001011010100111101000001",
   "011011001000011010110010000100111010111001010111110111111",
   "011011001010000111010111001001001011000100110010011001010",
   "011011001011110100000010111111111101000110001000010000101",
   "011011001101100000110101101001101100001000010111001011011",
   "011011001111001101101111000110110011011000001001100101100",
   "011011010000111010101111010111101110000011110110110001110",
   "011011010010100111110110011100110111011011100010111111000",
   "011011010100010101000100010110101010110000111111100000011",
   "011011010110000010011001000101100011010111101010110011001",
   "011011010111101111110100101001111100100100110000100110001",
   "011011011001011101010111000100010001101111001010000000010",
   "011011011011001011000000010100111110001111011101100111010",
   "011011011100111000110000011100011101011111111111100110111",
   "011011011110100110100111011011001010111100110001110111011",
   "011011100000010100100101010001100010000011100100000101000",
   "011011100010000010101001111111111110010011110011110101110",
   "011011100011110000110101100110111011001110101100110001101",
   "011011100101011111001000000110110100010111001000101000000",
   "011011100111001101100001100000000101010001101111011000001",
   "011011101000111100000001110011001001100100110111010110101",
   "011011101010101010101001000000011100111000100101010101001",
   "011011101100011001010111001000011010110110101100101001000",
   "011011101110001000001100001011011111001010101111010010100",
   "011011101111110111001000001010000101100001111110000011001",
   "011011110001100110001011000100101001101011011000100101001",
   "011011110011010101010100111011100111010111101101100010000",
   "011011110101000100100101101111011010011001011010101001111",
   "011011110110110011111101100000011110100100101100111010000",
   "011011111000100011011100001111001111101111100000100100001",
   "011011111010010011000001111100001001110001100001010101000",
   "011011111100000010101110100111101000100100001010011011110",
   "011011111101110010100010010010001000000010100110110000101",
   "011011111111100010011100111100000100001001110000111100001",
   "011100000001010010011110100101111000111000010011011101110",
   "011100000011000010100111010000000010001110101000110011100",
   "011100000100110010110110111010111100001110111011100000001",
   "011100000110100011001101100111000010111101000110010010110",
   "011100001000010011101011010100110010011110110100001101100",
   "011100001010000100010000000100100110111011100000101101001",
   "011100001011110100111011110110111100011100010111101111001",
   "011100001101100101101110101100001111001100010101111001100",
   "011100001111010110101000100100111011011000001000100001100",
   "011100010001000111101001100001011101001110001101110010111",
   "011100010010111000110001100010010000111110110100110110100",
   "011100010100101010000000100111110010111011111101111010001",
   "011100010110011011010110110010011111011001011010010110101",
   "011100011000001100110100000010110010101100101100110111110",
   "011100011001111110011000011001001001001101001001100011000",
   "011100011011110000000011110101111111010011110101111110100",
   "011100011101100001110110011001110001011011101001011000100",
   "011100011111010011110000000100111100000001001100101110000",
   "011100100001000101110000110111111011100010111010110010010",
   "011100100010110111111000110011001100100001000000010101110",
   "011100100100101010000111110111001011011101011100001101011",
   "011100100110011100011110000100010100111011111111011001011",
   "011100101000001110111011011011000101100010001101001100110",
   "011100101010000001011111111011111001110111011011010100010",
   "011100101011110100001011100111001110100100110001111101101",
   "011100101101100110111110011101100000010101001011111110100",
   "011100101111011001111000011111001011110101010110111100000",
   "011100110001001100111001101100101101110011110011010001101",
   "011100110011000000000010000110100011000000110100011000100",
   "011100110100110011010001101101001000001110100000101110011",
   "011100110110100110101000100000111010010000110001111101010",
   "011100111000011010000110100010010101111101010101000010010",
   "011100111010001101101011110001111000001011101010010100111",
   "011100111100000001011000001111111101110101000101101110010",
   "011100111101110101001011111101000011110100101110110000011",
   "011100111111101001000110111001100111000111100000101101100",
   "011101000001011101001001000110000100101100001010101110111",
   "011101000011010001010010100010111001100011001111111100101",
   "011101000101000101100011010000100010101111000111100100100",
   "011101000110111001111011001111011101010011111101000001100",
   "011101001000101110011010100000000110010111110000000010110",
   "011101001010100011000001000010111011000010010100110011001",
   "011101001100010111101110111000011000011101010100000000011",
   "011101001110001100100100000000111011110100001011000010101",
   "011101010000000001100000011101000010010100001100000011010",
   "011101010001110110100100001101001001001100011110000100100",
   "011101010011101011101111010001101101101101111101001000100",
   "011101010101100001000001101011001101001011011010011001011",
   "011101010111010110011011011010000100111001011100001111010",
   "011101011001001011111100011110110010001110011110011000110",
   "011101011011000001100100111001110010100010110010000010000",
   "011101011100110111010100101011100011010000011101111011100",
   "011101011110101101001011110100100001110011011110100010000",
   "011101100000100011001010010101001011101001100110000110000",
   "011101100010011001010000001101111110010010011100110010011",
   "011101100100001111011101011111010111001111100000110100100",
   "011101100110000101110010001001110100000100000110100011001",
   "011101100111111100001110001101110010010101011000100110001",
   "011101101001110010110001101011101111101010010111111101110",
   "011101101011101001011100100100001001101011111100001001110",
   "011101101101100000001110110111011110000100110011010001100",
   "011101101111010111001000100110001010100001100010001010101",
   "011101110001001110001001110000101100110000100100100000110",
   "011101110011000101010010010111100010100010001100111101010",
   "011101110100111100100010011011001001101000100101001110000",
   "011101110110110011111001111011111111110111101110001101100",
   "011101111000101011011000111010100011000101100000001001111",
   "011101111010100010111111010111010001001001101010101100101",
   "011101111100011010101101010010100111111101110101000001110",
   "011101111110010010100010101101000101011101011101111111110",
   "011110000000001010011111100111000111100101111100001110100",
   "011110000010000010100100000001001100010110011110001111010",
   "011110000011111010101111111011110001110000001010100011110",
   "011110000101110011000011010111010101110101111111110110000",
   "011110000111101011011110010100010110101100110100111111110",
   "011110001001100100000000110011010010011011011001010001101",
   "011110001011011100101010110100100111001010010100011011010",
   "011110001101010101011100011000110011000100000110110010100",
   "011110001111001110010101100000010100010101001001011011000",
   "011110010001000111010110001011101001001011101110001101101",
   "011110010011000000011110011011001111111000000000000000011",
   "011110010100111001101110001111100110101100000010101101010",
   "011110010110110011000101101001001011111011110011011010111",
   "011110011000101100100100101000011101111101001000100011000",
   "011110011010100110001011001101111011000111110001111011000",
   "011110011100011111111001011010000001110101011000111010100",
   "011110011110011001101111001101010000100001100000100100000",
   "011110100000010011101100101000000101101001100101101011110",
   "011110100010001101110001101010111111101100111110111111110",
   "011110100100000111111110010110011101001100111101001111000",
   "011110100110000010010010101010111100101100101011010001101",
   "011110100111111100101110101000111100110001001110010000001",
   "011110101001110111010010010000111100000001100101101011000",
   "011110101011110001111101100011011001000110101011100011000",
   "011110101101101100110000100000110010101011010100011111110",
   "011110101111100111101011001001100111011100001111111000100",
   "011110110001100010101101011110010110001000000111111011000",
   "011110110011011101110111011111011101011111100001110011100",
   "011110110101011001001001001101011100010100111101110100100",
   "011110110111010100100010101000110001011100110111011110011",
   "011110111001010000000011110001111011101101100101100111001",
   "011110111011001011101100101001011001111111011010100001110",
   "011110111101000111011101001111101011001100100100000110110",
   "011110111111000011010101100101001110010001001011111010111",
   "011111000000111111010101101010100010001011010111010111101",
   "011111000010111011011101100000000101111011000111110010100",
   "011111000100110111101101000110011000100010011010100101001",
   "011111000110110100000100011101111001000101001001010101000",
   "011111001000110000100011100111000110101001001001111010101",
   "011111001010101101001010100010100000010110001110101010010",
   "011111001100101001111001010000100101010110000110011010110",
   "011111001110100110101111110001110100110100011100101110010",
   "011111010000100011101110000110101101111110111001111001001",
   "011111010010100000110100001111110000000101000011001010010",
   "011111010100011110000010001101011010011000011010110011000",
   "011111010110011011011000000000001100001100100000001110100",
   "011111011000011000110101101000100100110110110000001001111",
   "011111011010010110011011000111000011101110100100101011111",
   "011111011100010100001000011100001000001101010101011100111",
   "011111011110010001111101101000010001101110010111101110111",
   "011111100000001111111010101011111111101110111110100100101",
   "011111100010001101111111100111110001101110011010111010011",
   "011111100100001100001100011100000111001101111011101101010",
   "011111100110001010100001001001011111110000101110000011011",
   "011111101000001000111101110000011010111011111101010011100",
   "011111101010000111100010010001011000010110110011001101001",
   "011111101100000110001110101100110111101010011000000000001",
   "011111101110000101000011000011011000100001110010100101001",
   "011111110000000011111111010101011010101010001000100101000",
   "011111110010000011000011100011011101110010011110100000110",
   "011111110100000010001111101110000001101011110111111010001",
   "011111110110000001100011110101100110001001010111011010100",
   "011111111000000000111111111010101010111111111110111011110",
   "011111111010000000100011111101110000000110101111101111110",
   "011111111100000000001111111111010101010110101010101000100",
   "011111111110000000000011111111111010101010101111111111111",
      others => (others => '0'));
      	begin 
      return tmp;
      end init_rom;
	signal rom : memory_t := init_rom;
   signal TableOut :  std_logic_vector(56 downto 0);
   signal Y0 :  std_logic_vector(56 downto 0);
begin
	process(clk)
   begin
   if(rising_edge(clk)) then
   	Y0 <= rom(  TO_INTEGER(unsigned(X))  );
   end if;
   end process;
    Y <= Y0;
end architecture;

--------------------------------------------------------------------------------
--                             GenericTable_2_126
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity GenericTable_2_126 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(1 downto 0);
          Y : out  std_logic_vector(125 downto 0)   );
end entity;

architecture arch of GenericTable_2_126 is
begin
  with X select  Y <= 
   "000000100000000000000101010101010110011001000000000000010000000000000010110100000000000010000000000000100110101010101010111111" when "00",
   "000100100000000010010000000000110110011011000000000010010000000001001000000100000000000110000000000100100110101010101011101011" when "01",
   "001100100000001010011010110001001011110101000000000110010000000101001101100100000000001010000000001100100110101010101100010100" when "10",
   "011000100000011100100101101110010110110111000000001100010000001110010010111100000000001110000000011000100101101010101101000000" when "11",
   "------------------------------------------------------------------------------------------------------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_37_f400_uid87
--                     (IntAdderClassical_37_f400_uid89)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_37_f400_uid87 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(36 downto 0);
          Y : in  std_logic_vector(36 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(36 downto 0)   );
end entity;

architecture arch of IntAdder_37_f400_uid87 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                       FixMultAdd_19x19p34r35signed67
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Matei Istoan, 2012-2014
--------------------------------------------------------------------------------
-- Pipeline depth: 4 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library std;
use std.textio.all;
library work;

entity FixMultAdd_19x19p34r35signed67 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(18 downto 0);
          Y : in  std_logic_vector(18 downto 0);
          A : in  std_logic_vector(33 downto 0);
          R : out  std_logic_vector(34 downto 0)   );
end entity;

architecture arch of FixMultAdd_19x19p34r35signed67 is
   component Compressor_23_3 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             X1 : in  std_logic_vector(1 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component IntAdder_37_f400_uid87 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(36 downto 0);
             Y : in  std_logic_vector(36 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(36 downto 0)   );
   end component;

signal XX_m70 :  std_logic_vector(18 downto 0);
signal YY_m70 :  std_logic_vector(18 downto 0);
signal heap_bh68_w6_0, heap_bh68_w6_0_d1, heap_bh68_w6_0_d2, heap_bh68_w6_0_d3, heap_bh68_w6_0_d4 :  std_logic;
signal heap_bh68_w7_0, heap_bh68_w7_0_d1, heap_bh68_w7_0_d2, heap_bh68_w7_0_d3, heap_bh68_w7_0_d4 :  std_logic;
signal heap_bh68_w8_0, heap_bh68_w8_0_d1, heap_bh68_w8_0_d2, heap_bh68_w8_0_d3, heap_bh68_w8_0_d4 :  std_logic;
signal heap_bh68_w9_0, heap_bh68_w9_0_d1, heap_bh68_w9_0_d2, heap_bh68_w9_0_d3, heap_bh68_w9_0_d4 :  std_logic;
signal heap_bh68_w10_0, heap_bh68_w10_0_d1, heap_bh68_w10_0_d2, heap_bh68_w10_0_d3, heap_bh68_w10_0_d4 :  std_logic;
signal heap_bh68_w11_0, heap_bh68_w11_0_d1, heap_bh68_w11_0_d2, heap_bh68_w11_0_d3, heap_bh68_w11_0_d4 :  std_logic;
signal heap_bh68_w12_0, heap_bh68_w12_0_d1, heap_bh68_w12_0_d2, heap_bh68_w12_0_d3, heap_bh68_w12_0_d4 :  std_logic;
signal heap_bh68_w13_0, heap_bh68_w13_0_d1, heap_bh68_w13_0_d2, heap_bh68_w13_0_d3, heap_bh68_w13_0_d4 :  std_logic;
signal heap_bh68_w14_0, heap_bh68_w14_0_d1, heap_bh68_w14_0_d2, heap_bh68_w14_0_d3, heap_bh68_w14_0_d4 :  std_logic;
signal heap_bh68_w15_0, heap_bh68_w15_0_d1, heap_bh68_w15_0_d2, heap_bh68_w15_0_d3, heap_bh68_w15_0_d4 :  std_logic;
signal heap_bh68_w16_0, heap_bh68_w16_0_d1, heap_bh68_w16_0_d2, heap_bh68_w16_0_d3, heap_bh68_w16_0_d4 :  std_logic;
signal heap_bh68_w17_0, heap_bh68_w17_0_d1, heap_bh68_w17_0_d2, heap_bh68_w17_0_d3, heap_bh68_w17_0_d4 :  std_logic;
signal heap_bh68_w18_0, heap_bh68_w18_0_d1, heap_bh68_w18_0_d2, heap_bh68_w18_0_d3, heap_bh68_w18_0_d4 :  std_logic;
signal heap_bh68_w19_0, heap_bh68_w19_0_d1, heap_bh68_w19_0_d2, heap_bh68_w19_0_d3, heap_bh68_w19_0_d4 :  std_logic;
signal heap_bh68_w20_0, heap_bh68_w20_0_d1, heap_bh68_w20_0_d2, heap_bh68_w20_0_d3, heap_bh68_w20_0_d4 :  std_logic;
signal heap_bh68_w21_0, heap_bh68_w21_0_d1, heap_bh68_w21_0_d2, heap_bh68_w21_0_d3, heap_bh68_w21_0_d4 :  std_logic;
signal heap_bh68_w22_0, heap_bh68_w22_0_d1, heap_bh68_w22_0_d2, heap_bh68_w22_0_d3, heap_bh68_w22_0_d4 :  std_logic;
signal heap_bh68_w23_0, heap_bh68_w23_0_d1, heap_bh68_w23_0_d2, heap_bh68_w23_0_d3, heap_bh68_w23_0_d4 :  std_logic;
signal heap_bh68_w24_0, heap_bh68_w24_0_d1, heap_bh68_w24_0_d2, heap_bh68_w24_0_d3, heap_bh68_w24_0_d4 :  std_logic;
signal heap_bh68_w25_0, heap_bh68_w25_0_d1, heap_bh68_w25_0_d2, heap_bh68_w25_0_d3, heap_bh68_w25_0_d4 :  std_logic;
signal heap_bh68_w26_0, heap_bh68_w26_0_d1, heap_bh68_w26_0_d2 :  std_logic;
signal heap_bh68_w27_0, heap_bh68_w27_0_d1, heap_bh68_w27_0_d2 :  std_logic;
signal heap_bh68_w28_0, heap_bh68_w28_0_d1, heap_bh68_w28_0_d2 :  std_logic;
signal heap_bh68_w29_0, heap_bh68_w29_0_d1, heap_bh68_w29_0_d2 :  std_logic;
signal heap_bh68_w30_0, heap_bh68_w30_0_d1, heap_bh68_w30_0_d2 :  std_logic;
signal heap_bh68_w31_0, heap_bh68_w31_0_d1, heap_bh68_w31_0_d2 :  std_logic;
signal heap_bh68_w32_0, heap_bh68_w32_0_d1, heap_bh68_w32_0_d2 :  std_logic;
signal heap_bh68_w33_0, heap_bh68_w33_0_d1, heap_bh68_w33_0_d2 :  std_logic;
signal heap_bh68_w34_0, heap_bh68_w34_0_d1, heap_bh68_w34_0_d2, heap_bh68_w34_0_d3 :  std_logic;
signal heap_bh68_w35_0, heap_bh68_w35_0_d1, heap_bh68_w35_0_d2, heap_bh68_w35_0_d3 :  std_logic;
signal heap_bh68_w36_0, heap_bh68_w36_0_d1, heap_bh68_w36_0_d2, heap_bh68_w36_0_d3 :  std_logic;
signal heap_bh68_w37_0, heap_bh68_w37_0_d1, heap_bh68_w37_0_d2, heap_bh68_w37_0_d3 :  std_logic;
signal heap_bh68_w38_0, heap_bh68_w38_0_d1, heap_bh68_w38_0_d2, heap_bh68_w38_0_d3 :  std_logic;
signal heap_bh68_w39_0, heap_bh68_w39_0_d1, heap_bh68_w39_0_d2, heap_bh68_w39_0_d3 :  std_logic;
signal DSP_bh68_ch1_0, DSP_bh68_ch1_0_d1 :  std_logic_vector(42 downto 0);
signal DSP_bh68_root1_1, DSP_bh68_root1_1_d1 :  std_logic_vector(42 downto 0);
signal DSP_bh68_ch1_1 :  std_logic_vector(43 downto 0);
signal heap_bh68_w26_1, heap_bh68_w26_1_d1 :  std_logic;
signal heap_bh68_w25_1, heap_bh68_w25_1_d1, heap_bh68_w25_1_d2, heap_bh68_w25_1_d3 :  std_logic;
signal heap_bh68_w24_1, heap_bh68_w24_1_d1, heap_bh68_w24_1_d2, heap_bh68_w24_1_d3 :  std_logic;
signal heap_bh68_w23_1, heap_bh68_w23_1_d1, heap_bh68_w23_1_d2, heap_bh68_w23_1_d3 :  std_logic;
signal heap_bh68_w22_1, heap_bh68_w22_1_d1, heap_bh68_w22_1_d2, heap_bh68_w22_1_d3 :  std_logic;
signal heap_bh68_w21_1, heap_bh68_w21_1_d1, heap_bh68_w21_1_d2, heap_bh68_w21_1_d3 :  std_logic;
signal heap_bh68_w20_1, heap_bh68_w20_1_d1, heap_bh68_w20_1_d2, heap_bh68_w20_1_d3 :  std_logic;
signal heap_bh68_w19_1, heap_bh68_w19_1_d1, heap_bh68_w19_1_d2, heap_bh68_w19_1_d3 :  std_logic;
signal heap_bh68_w18_1, heap_bh68_w18_1_d1, heap_bh68_w18_1_d2, heap_bh68_w18_1_d3 :  std_logic;
signal heap_bh68_w17_1, heap_bh68_w17_1_d1, heap_bh68_w17_1_d2, heap_bh68_w17_1_d3 :  std_logic;
signal heap_bh68_w16_1, heap_bh68_w16_1_d1, heap_bh68_w16_1_d2, heap_bh68_w16_1_d3 :  std_logic;
signal heap_bh68_w15_1, heap_bh68_w15_1_d1, heap_bh68_w15_1_d2, heap_bh68_w15_1_d3 :  std_logic;
signal heap_bh68_w14_1, heap_bh68_w14_1_d1, heap_bh68_w14_1_d2, heap_bh68_w14_1_d3 :  std_logic;
signal heap_bh68_w13_1, heap_bh68_w13_1_d1, heap_bh68_w13_1_d2, heap_bh68_w13_1_d3 :  std_logic;
signal heap_bh68_w12_1, heap_bh68_w12_1_d1, heap_bh68_w12_1_d2, heap_bh68_w12_1_d3 :  std_logic;
signal heap_bh68_w11_1, heap_bh68_w11_1_d1, heap_bh68_w11_1_d2, heap_bh68_w11_1_d3 :  std_logic;
signal heap_bh68_w10_1, heap_bh68_w10_1_d1, heap_bh68_w10_1_d2, heap_bh68_w10_1_d3 :  std_logic;
signal heap_bh68_w9_1, heap_bh68_w9_1_d1, heap_bh68_w9_1_d2, heap_bh68_w9_1_d3 :  std_logic;
signal heap_bh68_w8_1, heap_bh68_w8_1_d1, heap_bh68_w8_1_d2, heap_bh68_w8_1_d3 :  std_logic;
signal heap_bh68_w7_1, heap_bh68_w7_1_d1, heap_bh68_w7_1_d2, heap_bh68_w7_1_d3 :  std_logic;
signal heap_bh68_w6_1, heap_bh68_w6_1_d1, heap_bh68_w6_1_d2, heap_bh68_w6_1_d3 :  std_logic;
signal heap_bh68_w5_0, heap_bh68_w5_0_d1, heap_bh68_w5_0_d2, heap_bh68_w5_0_d3 :  std_logic;
signal heap_bh68_w4_0, heap_bh68_w4_0_d1, heap_bh68_w4_0_d2, heap_bh68_w4_0_d3 :  std_logic;
signal heap_bh68_w3_0 :  std_logic;
signal heap_bh68_w2_0 :  std_logic;
signal heap_bh68_w1_0 :  std_logic;
signal heap_bh68_w0_0 :  std_logic;
signal heap_bh68_w4_1, heap_bh68_w4_1_d1, heap_bh68_w4_1_d2, heap_bh68_w4_1_d3, heap_bh68_w4_1_d4 :  std_logic;
signal heap_bh68_w26_2, heap_bh68_w26_2_d1, heap_bh68_w26_2_d2 :  std_logic;
signal heap_bh68_w27_1, heap_bh68_w27_1_d1, heap_bh68_w27_1_d2 :  std_logic;
signal heap_bh68_w28_1, heap_bh68_w28_1_d1, heap_bh68_w28_1_d2 :  std_logic;
signal heap_bh68_w29_1, heap_bh68_w29_1_d1, heap_bh68_w29_1_d2 :  std_logic;
signal heap_bh68_w30_1, heap_bh68_w30_1_d1, heap_bh68_w30_1_d2 :  std_logic;
signal heap_bh68_w31_1, heap_bh68_w31_1_d1, heap_bh68_w31_1_d2 :  std_logic;
signal heap_bh68_w32_1, heap_bh68_w32_1_d1, heap_bh68_w32_1_d2 :  std_logic;
signal heap_bh68_w33_1, heap_bh68_w33_1_d1, heap_bh68_w33_1_d2 :  std_logic;
signal heap_bh68_w34_1, heap_bh68_w34_1_d1, heap_bh68_w34_1_d2, heap_bh68_w34_1_d3 :  std_logic;
signal heap_bh68_w35_1, heap_bh68_w35_1_d1, heap_bh68_w35_1_d2, heap_bh68_w35_1_d3 :  std_logic;
signal heap_bh68_w36_1, heap_bh68_w36_1_d1, heap_bh68_w36_1_d2, heap_bh68_w36_1_d3 :  std_logic;
signal heap_bh68_w37_1, heap_bh68_w37_1_d1, heap_bh68_w37_1_d2, heap_bh68_w37_1_d3 :  std_logic;
signal heap_bh68_w38_1, heap_bh68_w38_1_d1, heap_bh68_w38_1_d2, heap_bh68_w38_1_d3 :  std_logic;
signal heap_bh68_w39_1, heap_bh68_w39_1_d1, heap_bh68_w39_1_d2, heap_bh68_w39_1_d3 :  std_logic;
signal tempR_bh68_0, tempR_bh68_0_d1, tempR_bh68_0_d2, tempR_bh68_0_d3 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh68_0_0 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh68_0_1 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh68_0_0 :  std_logic_vector(2 downto 0);
signal heap_bh68_w26_3, heap_bh68_w26_3_d1, heap_bh68_w26_3_d2 :  std_logic;
signal heap_bh68_w27_2, heap_bh68_w27_2_d1, heap_bh68_w27_2_d2 :  std_logic;
signal heap_bh68_w28_2 :  std_logic;
signal CompressorIn_bh68_1_2 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh68_1_3 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh68_1_1 :  std_logic_vector(2 downto 0);
signal heap_bh68_w28_3, heap_bh68_w28_3_d1, heap_bh68_w28_3_d2 :  std_logic;
signal heap_bh68_w29_2, heap_bh68_w29_2_d1, heap_bh68_w29_2_d2 :  std_logic;
signal heap_bh68_w30_2 :  std_logic;
signal CompressorIn_bh68_2_4 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh68_2_5 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh68_2_2 :  std_logic_vector(2 downto 0);
signal heap_bh68_w30_3, heap_bh68_w30_3_d1, heap_bh68_w30_3_d2 :  std_logic;
signal heap_bh68_w31_2, heap_bh68_w31_2_d1, heap_bh68_w31_2_d2 :  std_logic;
signal heap_bh68_w32_2 :  std_logic;
signal CompressorIn_bh68_3_6 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh68_3_7 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh68_3_3 :  std_logic_vector(2 downto 0);
signal heap_bh68_w32_3, heap_bh68_w32_3_d1, heap_bh68_w32_3_d2 :  std_logic;
signal heap_bh68_w33_2, heap_bh68_w33_2_d1, heap_bh68_w33_2_d2 :  std_logic;
signal heap_bh68_w34_2, heap_bh68_w34_2_d1 :  std_logic;
signal CompressorIn_bh68_4_8 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh68_4_9 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh68_4_4 :  std_logic_vector(2 downto 0);
signal heap_bh68_w34_3, heap_bh68_w34_3_d1 :  std_logic;
signal heap_bh68_w35_2, heap_bh68_w35_2_d1 :  std_logic;
signal heap_bh68_w36_2 :  std_logic;
signal CompressorIn_bh68_5_10 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh68_5_11 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh68_5_5 :  std_logic_vector(2 downto 0);
signal heap_bh68_w36_3, heap_bh68_w36_3_d1 :  std_logic;
signal heap_bh68_w37_2, heap_bh68_w37_2_d1 :  std_logic;
signal heap_bh68_w38_2 :  std_logic;
signal CompressorIn_bh68_6_12 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh68_6_13 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh68_6_6 :  std_logic_vector(2 downto 0);
signal heap_bh68_w38_3, heap_bh68_w38_3_d1 :  std_logic;
signal heap_bh68_w39_2, heap_bh68_w39_2_d1 :  std_logic;
signal finalAdderIn0_bh68 :  std_logic_vector(36 downto 0);
signal finalAdderIn1_bh68 :  std_logic_vector(36 downto 0);
signal finalAdderCin_bh68 :  std_logic;
signal finalAdderOut_bh68 :  std_logic_vector(36 downto 0);
signal CompressionResult68 :  std_logic_vector(40 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            heap_bh68_w6_0_d1 <=  heap_bh68_w6_0;
            heap_bh68_w6_0_d2 <=  heap_bh68_w6_0_d1;
            heap_bh68_w6_0_d3 <=  heap_bh68_w6_0_d2;
            heap_bh68_w6_0_d4 <=  heap_bh68_w6_0_d3;
            heap_bh68_w7_0_d1 <=  heap_bh68_w7_0;
            heap_bh68_w7_0_d2 <=  heap_bh68_w7_0_d1;
            heap_bh68_w7_0_d3 <=  heap_bh68_w7_0_d2;
            heap_bh68_w7_0_d4 <=  heap_bh68_w7_0_d3;
            heap_bh68_w8_0_d1 <=  heap_bh68_w8_0;
            heap_bh68_w8_0_d2 <=  heap_bh68_w8_0_d1;
            heap_bh68_w8_0_d3 <=  heap_bh68_w8_0_d2;
            heap_bh68_w8_0_d4 <=  heap_bh68_w8_0_d3;
            heap_bh68_w9_0_d1 <=  heap_bh68_w9_0;
            heap_bh68_w9_0_d2 <=  heap_bh68_w9_0_d1;
            heap_bh68_w9_0_d3 <=  heap_bh68_w9_0_d2;
            heap_bh68_w9_0_d4 <=  heap_bh68_w9_0_d3;
            heap_bh68_w10_0_d1 <=  heap_bh68_w10_0;
            heap_bh68_w10_0_d2 <=  heap_bh68_w10_0_d1;
            heap_bh68_w10_0_d3 <=  heap_bh68_w10_0_d2;
            heap_bh68_w10_0_d4 <=  heap_bh68_w10_0_d3;
            heap_bh68_w11_0_d1 <=  heap_bh68_w11_0;
            heap_bh68_w11_0_d2 <=  heap_bh68_w11_0_d1;
            heap_bh68_w11_0_d3 <=  heap_bh68_w11_0_d2;
            heap_bh68_w11_0_d4 <=  heap_bh68_w11_0_d3;
            heap_bh68_w12_0_d1 <=  heap_bh68_w12_0;
            heap_bh68_w12_0_d2 <=  heap_bh68_w12_0_d1;
            heap_bh68_w12_0_d3 <=  heap_bh68_w12_0_d2;
            heap_bh68_w12_0_d4 <=  heap_bh68_w12_0_d3;
            heap_bh68_w13_0_d1 <=  heap_bh68_w13_0;
            heap_bh68_w13_0_d2 <=  heap_bh68_w13_0_d1;
            heap_bh68_w13_0_d3 <=  heap_bh68_w13_0_d2;
            heap_bh68_w13_0_d4 <=  heap_bh68_w13_0_d3;
            heap_bh68_w14_0_d1 <=  heap_bh68_w14_0;
            heap_bh68_w14_0_d2 <=  heap_bh68_w14_0_d1;
            heap_bh68_w14_0_d3 <=  heap_bh68_w14_0_d2;
            heap_bh68_w14_0_d4 <=  heap_bh68_w14_0_d3;
            heap_bh68_w15_0_d1 <=  heap_bh68_w15_0;
            heap_bh68_w15_0_d2 <=  heap_bh68_w15_0_d1;
            heap_bh68_w15_0_d3 <=  heap_bh68_w15_0_d2;
            heap_bh68_w15_0_d4 <=  heap_bh68_w15_0_d3;
            heap_bh68_w16_0_d1 <=  heap_bh68_w16_0;
            heap_bh68_w16_0_d2 <=  heap_bh68_w16_0_d1;
            heap_bh68_w16_0_d3 <=  heap_bh68_w16_0_d2;
            heap_bh68_w16_0_d4 <=  heap_bh68_w16_0_d3;
            heap_bh68_w17_0_d1 <=  heap_bh68_w17_0;
            heap_bh68_w17_0_d2 <=  heap_bh68_w17_0_d1;
            heap_bh68_w17_0_d3 <=  heap_bh68_w17_0_d2;
            heap_bh68_w17_0_d4 <=  heap_bh68_w17_0_d3;
            heap_bh68_w18_0_d1 <=  heap_bh68_w18_0;
            heap_bh68_w18_0_d2 <=  heap_bh68_w18_0_d1;
            heap_bh68_w18_0_d3 <=  heap_bh68_w18_0_d2;
            heap_bh68_w18_0_d4 <=  heap_bh68_w18_0_d3;
            heap_bh68_w19_0_d1 <=  heap_bh68_w19_0;
            heap_bh68_w19_0_d2 <=  heap_bh68_w19_0_d1;
            heap_bh68_w19_0_d3 <=  heap_bh68_w19_0_d2;
            heap_bh68_w19_0_d4 <=  heap_bh68_w19_0_d3;
            heap_bh68_w20_0_d1 <=  heap_bh68_w20_0;
            heap_bh68_w20_0_d2 <=  heap_bh68_w20_0_d1;
            heap_bh68_w20_0_d3 <=  heap_bh68_w20_0_d2;
            heap_bh68_w20_0_d4 <=  heap_bh68_w20_0_d3;
            heap_bh68_w21_0_d1 <=  heap_bh68_w21_0;
            heap_bh68_w21_0_d2 <=  heap_bh68_w21_0_d1;
            heap_bh68_w21_0_d3 <=  heap_bh68_w21_0_d2;
            heap_bh68_w21_0_d4 <=  heap_bh68_w21_0_d3;
            heap_bh68_w22_0_d1 <=  heap_bh68_w22_0;
            heap_bh68_w22_0_d2 <=  heap_bh68_w22_0_d1;
            heap_bh68_w22_0_d3 <=  heap_bh68_w22_0_d2;
            heap_bh68_w22_0_d4 <=  heap_bh68_w22_0_d3;
            heap_bh68_w23_0_d1 <=  heap_bh68_w23_0;
            heap_bh68_w23_0_d2 <=  heap_bh68_w23_0_d1;
            heap_bh68_w23_0_d3 <=  heap_bh68_w23_0_d2;
            heap_bh68_w23_0_d4 <=  heap_bh68_w23_0_d3;
            heap_bh68_w24_0_d1 <=  heap_bh68_w24_0;
            heap_bh68_w24_0_d2 <=  heap_bh68_w24_0_d1;
            heap_bh68_w24_0_d3 <=  heap_bh68_w24_0_d2;
            heap_bh68_w24_0_d4 <=  heap_bh68_w24_0_d3;
            heap_bh68_w25_0_d1 <=  heap_bh68_w25_0;
            heap_bh68_w25_0_d2 <=  heap_bh68_w25_0_d1;
            heap_bh68_w25_0_d3 <=  heap_bh68_w25_0_d2;
            heap_bh68_w25_0_d4 <=  heap_bh68_w25_0_d3;
            heap_bh68_w26_0_d1 <=  heap_bh68_w26_0;
            heap_bh68_w26_0_d2 <=  heap_bh68_w26_0_d1;
            heap_bh68_w27_0_d1 <=  heap_bh68_w27_0;
            heap_bh68_w27_0_d2 <=  heap_bh68_w27_0_d1;
            heap_bh68_w28_0_d1 <=  heap_bh68_w28_0;
            heap_bh68_w28_0_d2 <=  heap_bh68_w28_0_d1;
            heap_bh68_w29_0_d1 <=  heap_bh68_w29_0;
            heap_bh68_w29_0_d2 <=  heap_bh68_w29_0_d1;
            heap_bh68_w30_0_d1 <=  heap_bh68_w30_0;
            heap_bh68_w30_0_d2 <=  heap_bh68_w30_0_d1;
            heap_bh68_w31_0_d1 <=  heap_bh68_w31_0;
            heap_bh68_w31_0_d2 <=  heap_bh68_w31_0_d1;
            heap_bh68_w32_0_d1 <=  heap_bh68_w32_0;
            heap_bh68_w32_0_d2 <=  heap_bh68_w32_0_d1;
            heap_bh68_w33_0_d1 <=  heap_bh68_w33_0;
            heap_bh68_w33_0_d2 <=  heap_bh68_w33_0_d1;
            heap_bh68_w34_0_d1 <=  heap_bh68_w34_0;
            heap_bh68_w34_0_d2 <=  heap_bh68_w34_0_d1;
            heap_bh68_w34_0_d3 <=  heap_bh68_w34_0_d2;
            heap_bh68_w35_0_d1 <=  heap_bh68_w35_0;
            heap_bh68_w35_0_d2 <=  heap_bh68_w35_0_d1;
            heap_bh68_w35_0_d3 <=  heap_bh68_w35_0_d2;
            heap_bh68_w36_0_d1 <=  heap_bh68_w36_0;
            heap_bh68_w36_0_d2 <=  heap_bh68_w36_0_d1;
            heap_bh68_w36_0_d3 <=  heap_bh68_w36_0_d2;
            heap_bh68_w37_0_d1 <=  heap_bh68_w37_0;
            heap_bh68_w37_0_d2 <=  heap_bh68_w37_0_d1;
            heap_bh68_w37_0_d3 <=  heap_bh68_w37_0_d2;
            heap_bh68_w38_0_d1 <=  heap_bh68_w38_0;
            heap_bh68_w38_0_d2 <=  heap_bh68_w38_0_d1;
            heap_bh68_w38_0_d3 <=  heap_bh68_w38_0_d2;
            heap_bh68_w39_0_d1 <=  heap_bh68_w39_0;
            heap_bh68_w39_0_d2 <=  heap_bh68_w39_0_d1;
            heap_bh68_w39_0_d3 <=  heap_bh68_w39_0_d2;
            DSP_bh68_ch1_0_d1 <=  DSP_bh68_ch1_0;
            DSP_bh68_root1_1_d1 <=  DSP_bh68_root1_1;
            heap_bh68_w26_1_d1 <=  heap_bh68_w26_1;
            heap_bh68_w25_1_d1 <=  heap_bh68_w25_1;
            heap_bh68_w25_1_d2 <=  heap_bh68_w25_1_d1;
            heap_bh68_w25_1_d3 <=  heap_bh68_w25_1_d2;
            heap_bh68_w24_1_d1 <=  heap_bh68_w24_1;
            heap_bh68_w24_1_d2 <=  heap_bh68_w24_1_d1;
            heap_bh68_w24_1_d3 <=  heap_bh68_w24_1_d2;
            heap_bh68_w23_1_d1 <=  heap_bh68_w23_1;
            heap_bh68_w23_1_d2 <=  heap_bh68_w23_1_d1;
            heap_bh68_w23_1_d3 <=  heap_bh68_w23_1_d2;
            heap_bh68_w22_1_d1 <=  heap_bh68_w22_1;
            heap_bh68_w22_1_d2 <=  heap_bh68_w22_1_d1;
            heap_bh68_w22_1_d3 <=  heap_bh68_w22_1_d2;
            heap_bh68_w21_1_d1 <=  heap_bh68_w21_1;
            heap_bh68_w21_1_d2 <=  heap_bh68_w21_1_d1;
            heap_bh68_w21_1_d3 <=  heap_bh68_w21_1_d2;
            heap_bh68_w20_1_d1 <=  heap_bh68_w20_1;
            heap_bh68_w20_1_d2 <=  heap_bh68_w20_1_d1;
            heap_bh68_w20_1_d3 <=  heap_bh68_w20_1_d2;
            heap_bh68_w19_1_d1 <=  heap_bh68_w19_1;
            heap_bh68_w19_1_d2 <=  heap_bh68_w19_1_d1;
            heap_bh68_w19_1_d3 <=  heap_bh68_w19_1_d2;
            heap_bh68_w18_1_d1 <=  heap_bh68_w18_1;
            heap_bh68_w18_1_d2 <=  heap_bh68_w18_1_d1;
            heap_bh68_w18_1_d3 <=  heap_bh68_w18_1_d2;
            heap_bh68_w17_1_d1 <=  heap_bh68_w17_1;
            heap_bh68_w17_1_d2 <=  heap_bh68_w17_1_d1;
            heap_bh68_w17_1_d3 <=  heap_bh68_w17_1_d2;
            heap_bh68_w16_1_d1 <=  heap_bh68_w16_1;
            heap_bh68_w16_1_d2 <=  heap_bh68_w16_1_d1;
            heap_bh68_w16_1_d3 <=  heap_bh68_w16_1_d2;
            heap_bh68_w15_1_d1 <=  heap_bh68_w15_1;
            heap_bh68_w15_1_d2 <=  heap_bh68_w15_1_d1;
            heap_bh68_w15_1_d3 <=  heap_bh68_w15_1_d2;
            heap_bh68_w14_1_d1 <=  heap_bh68_w14_1;
            heap_bh68_w14_1_d2 <=  heap_bh68_w14_1_d1;
            heap_bh68_w14_1_d3 <=  heap_bh68_w14_1_d2;
            heap_bh68_w13_1_d1 <=  heap_bh68_w13_1;
            heap_bh68_w13_1_d2 <=  heap_bh68_w13_1_d1;
            heap_bh68_w13_1_d3 <=  heap_bh68_w13_1_d2;
            heap_bh68_w12_1_d1 <=  heap_bh68_w12_1;
            heap_bh68_w12_1_d2 <=  heap_bh68_w12_1_d1;
            heap_bh68_w12_1_d3 <=  heap_bh68_w12_1_d2;
            heap_bh68_w11_1_d1 <=  heap_bh68_w11_1;
            heap_bh68_w11_1_d2 <=  heap_bh68_w11_1_d1;
            heap_bh68_w11_1_d3 <=  heap_bh68_w11_1_d2;
            heap_bh68_w10_1_d1 <=  heap_bh68_w10_1;
            heap_bh68_w10_1_d2 <=  heap_bh68_w10_1_d1;
            heap_bh68_w10_1_d3 <=  heap_bh68_w10_1_d2;
            heap_bh68_w9_1_d1 <=  heap_bh68_w9_1;
            heap_bh68_w9_1_d2 <=  heap_bh68_w9_1_d1;
            heap_bh68_w9_1_d3 <=  heap_bh68_w9_1_d2;
            heap_bh68_w8_1_d1 <=  heap_bh68_w8_1;
            heap_bh68_w8_1_d2 <=  heap_bh68_w8_1_d1;
            heap_bh68_w8_1_d3 <=  heap_bh68_w8_1_d2;
            heap_bh68_w7_1_d1 <=  heap_bh68_w7_1;
            heap_bh68_w7_1_d2 <=  heap_bh68_w7_1_d1;
            heap_bh68_w7_1_d3 <=  heap_bh68_w7_1_d2;
            heap_bh68_w6_1_d1 <=  heap_bh68_w6_1;
            heap_bh68_w6_1_d2 <=  heap_bh68_w6_1_d1;
            heap_bh68_w6_1_d3 <=  heap_bh68_w6_1_d2;
            heap_bh68_w5_0_d1 <=  heap_bh68_w5_0;
            heap_bh68_w5_0_d2 <=  heap_bh68_w5_0_d1;
            heap_bh68_w5_0_d3 <=  heap_bh68_w5_0_d2;
            heap_bh68_w4_0_d1 <=  heap_bh68_w4_0;
            heap_bh68_w4_0_d2 <=  heap_bh68_w4_0_d1;
            heap_bh68_w4_0_d3 <=  heap_bh68_w4_0_d2;
            heap_bh68_w4_1_d1 <=  heap_bh68_w4_1;
            heap_bh68_w4_1_d2 <=  heap_bh68_w4_1_d1;
            heap_bh68_w4_1_d3 <=  heap_bh68_w4_1_d2;
            heap_bh68_w4_1_d4 <=  heap_bh68_w4_1_d3;
            heap_bh68_w26_2_d1 <=  heap_bh68_w26_2;
            heap_bh68_w26_2_d2 <=  heap_bh68_w26_2_d1;
            heap_bh68_w27_1_d1 <=  heap_bh68_w27_1;
            heap_bh68_w27_1_d2 <=  heap_bh68_w27_1_d1;
            heap_bh68_w28_1_d1 <=  heap_bh68_w28_1;
            heap_bh68_w28_1_d2 <=  heap_bh68_w28_1_d1;
            heap_bh68_w29_1_d1 <=  heap_bh68_w29_1;
            heap_bh68_w29_1_d2 <=  heap_bh68_w29_1_d1;
            heap_bh68_w30_1_d1 <=  heap_bh68_w30_1;
            heap_bh68_w30_1_d2 <=  heap_bh68_w30_1_d1;
            heap_bh68_w31_1_d1 <=  heap_bh68_w31_1;
            heap_bh68_w31_1_d2 <=  heap_bh68_w31_1_d1;
            heap_bh68_w32_1_d1 <=  heap_bh68_w32_1;
            heap_bh68_w32_1_d2 <=  heap_bh68_w32_1_d1;
            heap_bh68_w33_1_d1 <=  heap_bh68_w33_1;
            heap_bh68_w33_1_d2 <=  heap_bh68_w33_1_d1;
            heap_bh68_w34_1_d1 <=  heap_bh68_w34_1;
            heap_bh68_w34_1_d2 <=  heap_bh68_w34_1_d1;
            heap_bh68_w34_1_d3 <=  heap_bh68_w34_1_d2;
            heap_bh68_w35_1_d1 <=  heap_bh68_w35_1;
            heap_bh68_w35_1_d2 <=  heap_bh68_w35_1_d1;
            heap_bh68_w35_1_d3 <=  heap_bh68_w35_1_d2;
            heap_bh68_w36_1_d1 <=  heap_bh68_w36_1;
            heap_bh68_w36_1_d2 <=  heap_bh68_w36_1_d1;
            heap_bh68_w36_1_d3 <=  heap_bh68_w36_1_d2;
            heap_bh68_w37_1_d1 <=  heap_bh68_w37_1;
            heap_bh68_w37_1_d2 <=  heap_bh68_w37_1_d1;
            heap_bh68_w37_1_d3 <=  heap_bh68_w37_1_d2;
            heap_bh68_w38_1_d1 <=  heap_bh68_w38_1;
            heap_bh68_w38_1_d2 <=  heap_bh68_w38_1_d1;
            heap_bh68_w38_1_d3 <=  heap_bh68_w38_1_d2;
            heap_bh68_w39_1_d1 <=  heap_bh68_w39_1;
            heap_bh68_w39_1_d2 <=  heap_bh68_w39_1_d1;
            heap_bh68_w39_1_d3 <=  heap_bh68_w39_1_d2;
            tempR_bh68_0_d1 <=  tempR_bh68_0;
            tempR_bh68_0_d2 <=  tempR_bh68_0_d1;
            tempR_bh68_0_d3 <=  tempR_bh68_0_d2;
            heap_bh68_w26_3_d1 <=  heap_bh68_w26_3;
            heap_bh68_w26_3_d2 <=  heap_bh68_w26_3_d1;
            heap_bh68_w27_2_d1 <=  heap_bh68_w27_2;
            heap_bh68_w27_2_d2 <=  heap_bh68_w27_2_d1;
            heap_bh68_w28_3_d1 <=  heap_bh68_w28_3;
            heap_bh68_w28_3_d2 <=  heap_bh68_w28_3_d1;
            heap_bh68_w29_2_d1 <=  heap_bh68_w29_2;
            heap_bh68_w29_2_d2 <=  heap_bh68_w29_2_d1;
            heap_bh68_w30_3_d1 <=  heap_bh68_w30_3;
            heap_bh68_w30_3_d2 <=  heap_bh68_w30_3_d1;
            heap_bh68_w31_2_d1 <=  heap_bh68_w31_2;
            heap_bh68_w31_2_d2 <=  heap_bh68_w31_2_d1;
            heap_bh68_w32_3_d1 <=  heap_bh68_w32_3;
            heap_bh68_w32_3_d2 <=  heap_bh68_w32_3_d1;
            heap_bh68_w33_2_d1 <=  heap_bh68_w33_2;
            heap_bh68_w33_2_d2 <=  heap_bh68_w33_2_d1;
            heap_bh68_w34_2_d1 <=  heap_bh68_w34_2;
            heap_bh68_w34_3_d1 <=  heap_bh68_w34_3;
            heap_bh68_w35_2_d1 <=  heap_bh68_w35_2;
            heap_bh68_w36_3_d1 <=  heap_bh68_w36_3;
            heap_bh68_w37_2_d1 <=  heap_bh68_w37_2;
            heap_bh68_w38_3_d1 <=  heap_bh68_w38_3;
            heap_bh68_w39_2_d1 <=  heap_bh68_w39_2;
         end if;
      end process;
   XX_m70 <= X ;
   YY_m70 <= Y ;
   heap_bh68_w6_0 <= A(0); -- cycle= 0 cp= 0
   heap_bh68_w7_0 <= A(1); -- cycle= 0 cp= 0
   heap_bh68_w8_0 <= A(2); -- cycle= 0 cp= 0
   heap_bh68_w9_0 <= A(3); -- cycle= 0 cp= 0
   heap_bh68_w10_0 <= A(4); -- cycle= 0 cp= 0
   heap_bh68_w11_0 <= A(5); -- cycle= 0 cp= 0
   heap_bh68_w12_0 <= A(6); -- cycle= 0 cp= 0
   heap_bh68_w13_0 <= A(7); -- cycle= 0 cp= 0
   heap_bh68_w14_0 <= A(8); -- cycle= 0 cp= 0
   heap_bh68_w15_0 <= A(9); -- cycle= 0 cp= 0
   heap_bh68_w16_0 <= A(10); -- cycle= 0 cp= 0
   heap_bh68_w17_0 <= A(11); -- cycle= 0 cp= 0
   heap_bh68_w18_0 <= A(12); -- cycle= 0 cp= 0
   heap_bh68_w19_0 <= A(13); -- cycle= 0 cp= 0
   heap_bh68_w20_0 <= A(14); -- cycle= 0 cp= 0
   heap_bh68_w21_0 <= A(15); -- cycle= 0 cp= 0
   heap_bh68_w22_0 <= A(16); -- cycle= 0 cp= 0
   heap_bh68_w23_0 <= A(17); -- cycle= 0 cp= 0
   heap_bh68_w24_0 <= A(18); -- cycle= 0 cp= 0
   heap_bh68_w25_0 <= A(19); -- cycle= 0 cp= 0
   heap_bh68_w26_0 <= A(20); -- cycle= 0 cp= 0
   heap_bh68_w27_0 <= A(21); -- cycle= 0 cp= 0
   heap_bh68_w28_0 <= A(22); -- cycle= 0 cp= 0
   heap_bh68_w29_0 <= A(23); -- cycle= 0 cp= 0
   heap_bh68_w30_0 <= A(24); -- cycle= 0 cp= 0
   heap_bh68_w31_0 <= A(25); -- cycle= 0 cp= 0
   heap_bh68_w32_0 <= A(26); -- cycle= 0 cp= 0
   heap_bh68_w33_0 <= A(27); -- cycle= 0 cp= 0
   heap_bh68_w34_0 <= A(28); -- cycle= 0 cp= 0
   heap_bh68_w35_0 <= A(29); -- cycle= 0 cp= 0
   heap_bh68_w36_0 <= A(30); -- cycle= 0 cp= 0
   heap_bh68_w37_0 <= A(31); -- cycle= 0 cp= 0
   heap_bh68_w38_0 <= A(32); -- cycle= 0 cp= 0
   heap_bh68_w39_0 <= A(33); -- cycle= 0 cp= 0
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh68_ch1_0 <= std_logic_vector(signed("" & XX_m70(18 downto 0) & "000000") * signed("0" & YY_m70(0 downto 0) & "0000000000000000"));
   DSP_bh68_root1_1 <= std_logic_vector(signed("" & XX_m70(18 downto 0) & "000000") * signed("" & YY_m70(18 downto 1) & ""));
   ----------------Synchro barrier, entering cycle 1----------------
   DSP_bh68_ch1_1<= std_logic_vector(signed(DSP_bh68_root1_1_d1) +  signed( DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) & DSP_bh68_ch1_0_d1(42) &   DSP_bh68_ch1_0_d1(42 downto 17) ));
   heap_bh68_w26_1 <= not( DSP_bh68_ch1_1(43) ); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w25_1 <= DSP_bh68_ch1_1(42); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w24_1 <= DSP_bh68_ch1_1(41); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w23_1 <= DSP_bh68_ch1_1(40); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w22_1 <= DSP_bh68_ch1_1(39); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w21_1 <= DSP_bh68_ch1_1(38); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w20_1 <= DSP_bh68_ch1_1(37); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w19_1 <= DSP_bh68_ch1_1(36); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w18_1 <= DSP_bh68_ch1_1(35); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w17_1 <= DSP_bh68_ch1_1(34); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w16_1 <= DSP_bh68_ch1_1(33); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w15_1 <= DSP_bh68_ch1_1(32); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w14_1 <= DSP_bh68_ch1_1(31); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w13_1 <= DSP_bh68_ch1_1(30); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w12_1 <= DSP_bh68_ch1_1(29); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w11_1 <= DSP_bh68_ch1_1(28); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w10_1 <= DSP_bh68_ch1_1(27); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w9_1 <= DSP_bh68_ch1_1(26); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w8_1 <= DSP_bh68_ch1_1(25); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w7_1 <= DSP_bh68_ch1_1(24); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w6_1 <= DSP_bh68_ch1_1(23); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w5_0 <= DSP_bh68_ch1_1(22); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w4_0 <= DSP_bh68_ch1_1(21); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w3_0 <= DSP_bh68_ch1_1(20); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w2_0 <= DSP_bh68_ch1_1(19); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w1_0 <= DSP_bh68_ch1_1(18); -- cycle= 1 cp= 2.274e-09
   heap_bh68_w0_0 <= DSP_bh68_ch1_1(17); -- cycle= 1 cp= 2.274e-09
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
   heap_bh68_w4_1 <= '1'; -- cycle= 0 cp= 0
   heap_bh68_w26_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh68_w27_1 <= '1'; -- cycle= 0 cp= 0
   heap_bh68_w28_1 <= '1'; -- cycle= 0 cp= 0
   heap_bh68_w29_1 <= '1'; -- cycle= 0 cp= 0
   heap_bh68_w30_1 <= '1'; -- cycle= 0 cp= 0
   heap_bh68_w31_1 <= '1'; -- cycle= 0 cp= 0
   heap_bh68_w32_1 <= '1'; -- cycle= 0 cp= 0
   heap_bh68_w33_1 <= '1'; -- cycle= 0 cp= 0
   heap_bh68_w34_1 <= '1'; -- cycle= 0 cp= 0
   heap_bh68_w35_1 <= '1'; -- cycle= 0 cp= 0
   heap_bh68_w36_1 <= '1'; -- cycle= 0 cp= 0
   heap_bh68_w37_1 <= '1'; -- cycle= 0 cp= 0
   heap_bh68_w38_1 <= '1'; -- cycle= 0 cp= 0
   heap_bh68_w39_1 <= '1'; -- cycle= 0 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   tempR_bh68_0 <= heap_bh68_w3_0 & heap_bh68_w2_0 & heap_bh68_w1_0 & heap_bh68_w0_0; -- already compressed

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh68_0_0 <= heap_bh68_w26_2_d2 & heap_bh68_w26_0_d2 & heap_bh68_w26_1_d1;
   CompressorIn_bh68_0_1 <= heap_bh68_w27_1_d2 & heap_bh68_w27_0_d2;
   Compressor_bh68_0: Compressor_23_3
      port map ( R => CompressorOut_bh68_0_0   ,
                 X0 => CompressorIn_bh68_0_0,
                 X1 => CompressorIn_bh68_0_1);
   heap_bh68_w26_3 <= CompressorOut_bh68_0_0(0); -- cycle= 2 cp= 0
   heap_bh68_w27_2 <= CompressorOut_bh68_0_0(1); -- cycle= 2 cp= 0
   heap_bh68_w28_2 <= CompressorOut_bh68_0_0(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh68_1_2 <= heap_bh68_w28_1_d2 & heap_bh68_w28_0_d2 & heap_bh68_w28_2;
   CompressorIn_bh68_1_3 <= heap_bh68_w29_1_d2 & heap_bh68_w29_0_d2;
   Compressor_bh68_1: Compressor_23_3
      port map ( R => CompressorOut_bh68_1_1   ,
                 X0 => CompressorIn_bh68_1_2,
                 X1 => CompressorIn_bh68_1_3);
   heap_bh68_w28_3 <= CompressorOut_bh68_1_1(0); -- cycle= 2 cp= 5.3072e-10
   heap_bh68_w29_2 <= CompressorOut_bh68_1_1(1); -- cycle= 2 cp= 5.3072e-10
   heap_bh68_w30_2 <= CompressorOut_bh68_1_1(2); -- cycle= 2 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh68_2_4 <= heap_bh68_w30_1_d2 & heap_bh68_w30_0_d2 & heap_bh68_w30_2;
   CompressorIn_bh68_2_5 <= heap_bh68_w31_1_d2 & heap_bh68_w31_0_d2;
   Compressor_bh68_2: Compressor_23_3
      port map ( R => CompressorOut_bh68_2_2   ,
                 X0 => CompressorIn_bh68_2_4,
                 X1 => CompressorIn_bh68_2_5);
   heap_bh68_w30_3 <= CompressorOut_bh68_2_2(0); -- cycle= 2 cp= 1.06144e-09
   heap_bh68_w31_2 <= CompressorOut_bh68_2_2(1); -- cycle= 2 cp= 1.06144e-09
   heap_bh68_w32_2 <= CompressorOut_bh68_2_2(2); -- cycle= 2 cp= 1.06144e-09

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh68_3_6 <= heap_bh68_w32_1_d2 & heap_bh68_w32_0_d2 & heap_bh68_w32_2;
   CompressorIn_bh68_3_7 <= heap_bh68_w33_1_d2 & heap_bh68_w33_0_d2;
   Compressor_bh68_3: Compressor_23_3
      port map ( R => CompressorOut_bh68_3_3   ,
                 X0 => CompressorIn_bh68_3_6,
                 X1 => CompressorIn_bh68_3_7);
   heap_bh68_w32_3 <= CompressorOut_bh68_3_3(0); -- cycle= 2 cp= 1.59216e-09
   heap_bh68_w33_2 <= CompressorOut_bh68_3_3(1); -- cycle= 2 cp= 1.59216e-09
   heap_bh68_w34_2 <= CompressorOut_bh68_3_3(2); -- cycle= 2 cp= 1.59216e-09

   ----------------Synchro barrier, entering cycle 2----------------
   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh68_4_8 <= heap_bh68_w34_1_d3 & heap_bh68_w34_0_d3 & heap_bh68_w34_2_d1;
   CompressorIn_bh68_4_9 <= heap_bh68_w35_1_d3 & heap_bh68_w35_0_d3;
   Compressor_bh68_4: Compressor_23_3
      port map ( R => CompressorOut_bh68_4_4   ,
                 X0 => CompressorIn_bh68_4_8,
                 X1 => CompressorIn_bh68_4_9);
   heap_bh68_w34_3 <= CompressorOut_bh68_4_4(0); -- cycle= 3 cp= 0
   heap_bh68_w35_2 <= CompressorOut_bh68_4_4(1); -- cycle= 3 cp= 0
   heap_bh68_w36_2 <= CompressorOut_bh68_4_4(2); -- cycle= 3 cp= 0

   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh68_5_10 <= heap_bh68_w36_1_d3 & heap_bh68_w36_0_d3 & heap_bh68_w36_2;
   CompressorIn_bh68_5_11 <= heap_bh68_w37_1_d3 & heap_bh68_w37_0_d3;
   Compressor_bh68_5: Compressor_23_3
      port map ( R => CompressorOut_bh68_5_5   ,
                 X0 => CompressorIn_bh68_5_10,
                 X1 => CompressorIn_bh68_5_11);
   heap_bh68_w36_3 <= CompressorOut_bh68_5_5(0); -- cycle= 3 cp= 5.3072e-10
   heap_bh68_w37_2 <= CompressorOut_bh68_5_5(1); -- cycle= 3 cp= 5.3072e-10
   heap_bh68_w38_2 <= CompressorOut_bh68_5_5(2); -- cycle= 3 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh68_6_12 <= heap_bh68_w38_1_d3 & heap_bh68_w38_0_d3 & heap_bh68_w38_2;
   CompressorIn_bh68_6_13 <= heap_bh68_w39_1_d3 & heap_bh68_w39_0_d3;
   Compressor_bh68_6: Compressor_23_3
      port map ( R => CompressorOut_bh68_6_6   ,
                 X0 => CompressorIn_bh68_6_12,
                 X1 => CompressorIn_bh68_6_13);
   heap_bh68_w38_3 <= CompressorOut_bh68_6_6(0); -- cycle= 3 cp= 1.06144e-09
   heap_bh68_w39_2 <= CompressorOut_bh68_6_6(1); -- cycle= 3 cp= 1.06144e-09
   ----------------Synchro barrier, entering cycle 3----------------
   ----------------Synchro barrier, entering cycle 4----------------
   finalAdderIn0_bh68 <= "0" & heap_bh68_w39_2_d1 & heap_bh68_w38_3_d1 & heap_bh68_w37_2_d1 & heap_bh68_w36_3_d1 & heap_bh68_w35_2_d1 & heap_bh68_w34_3_d1 & heap_bh68_w33_2_d2 & heap_bh68_w32_3_d2 & heap_bh68_w31_2_d2 & heap_bh68_w30_3_d2 & heap_bh68_w29_2_d2 & heap_bh68_w28_3_d2 & heap_bh68_w27_2_d2 & heap_bh68_w26_3_d2 & heap_bh68_w25_0_d4 & heap_bh68_w24_0_d4 & heap_bh68_w23_0_d4 & heap_bh68_w22_0_d4 & heap_bh68_w21_0_d4 & heap_bh68_w20_0_d4 & heap_bh68_w19_0_d4 & heap_bh68_w18_0_d4 & heap_bh68_w17_0_d4 & heap_bh68_w16_0_d4 & heap_bh68_w15_0_d4 & heap_bh68_w14_0_d4 & heap_bh68_w13_0_d4 & heap_bh68_w12_0_d4 & heap_bh68_w11_0_d4 & heap_bh68_w10_0_d4 & heap_bh68_w9_0_d4 & heap_bh68_w8_0_d4 & heap_bh68_w7_0_d4 & heap_bh68_w6_0_d4 & heap_bh68_w5_0_d3 & heap_bh68_w4_1_d4;
   finalAdderIn1_bh68 <= "0" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh68_w25_1_d3 & heap_bh68_w24_1_d3 & heap_bh68_w23_1_d3 & heap_bh68_w22_1_d3 & heap_bh68_w21_1_d3 & heap_bh68_w20_1_d3 & heap_bh68_w19_1_d3 & heap_bh68_w18_1_d3 & heap_bh68_w17_1_d3 & heap_bh68_w16_1_d3 & heap_bh68_w15_1_d3 & heap_bh68_w14_1_d3 & heap_bh68_w13_1_d3 & heap_bh68_w12_1_d3 & heap_bh68_w11_1_d3 & heap_bh68_w10_1_d3 & heap_bh68_w9_1_d3 & heap_bh68_w8_1_d3 & heap_bh68_w7_1_d3 & heap_bh68_w6_1_d3 & '0' & heap_bh68_w4_0_d3;
   finalAdderCin_bh68 <= '0';
   Adder_final68_0: IntAdder_37_f400_uid87  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh68,
                 R => finalAdderOut_bh68   ,
                 X => finalAdderIn0_bh68,
                 Y => finalAdderIn1_bh68);
   -- concatenate all the compressed chunks
   CompressionResult68 <= finalAdderOut_bh68 & tempR_bh68_0_d3;
   -- End of code generated by BitHeap::generateCompressorVHDL
   R <= CompressionResult68(39 downto 5);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_45_f400_uid140
--                    (IntAdderAlternative_45_f400_uid144)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_45_f400_uid140 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(44 downto 0);
          Y : in  std_logic_vector(44 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(44 downto 0)   );
end entity;

architecture arch of IntAdder_45_f400_uid140 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(3 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(2 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(3 downto 0);
signal sum_l1_idx1 :  std_logic_vector(2 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(44 downto 42)) + ( "0" & Y(44 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(2 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(3 downto 3);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(2 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(3 downto 3);
   R <= sum_l1_idx1(2 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                       FixMultAdd_34x35p37r38signed95
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Matei Istoan, 2012-2014
--------------------------------------------------------------------------------
-- Pipeline depth: 6 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library std;
use std.textio.all;
library work;

entity FixMultAdd_34x35p37r38signed95 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(33 downto 0);
          Y : in  std_logic_vector(34 downto 0);
          A : in  std_logic_vector(36 downto 0);
          R : out  std_logic_vector(37 downto 0)   );
end entity;

architecture arch of FixMultAdd_34x35p37r38signed95 is
   component Compressor_14_3 is
      port ( X0 : in  std_logic_vector(3 downto 0);
             X1 : in  std_logic_vector(0 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component Compressor_23_3 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             X1 : in  std_logic_vector(1 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component Compressor_3_2 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             R : out  std_logic_vector(1 downto 0)   );
   end component;

   component IntAdder_45_f400_uid140 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(44 downto 0);
             Y : in  std_logic_vector(44 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(44 downto 0)   );
   end component;

signal XX_m98 :  std_logic_vector(34 downto 0);
signal YY_m98 :  std_logic_vector(33 downto 0);
signal heap_bh96_w7_0, heap_bh96_w7_0_d1, heap_bh96_w7_0_d2 :  std_logic;
signal heap_bh96_w8_0, heap_bh96_w8_0_d1, heap_bh96_w8_0_d2 :  std_logic;
signal heap_bh96_w9_0, heap_bh96_w9_0_d1, heap_bh96_w9_0_d2 :  std_logic;
signal heap_bh96_w10_0, heap_bh96_w10_0_d1, heap_bh96_w10_0_d2 :  std_logic;
signal heap_bh96_w11_0, heap_bh96_w11_0_d1, heap_bh96_w11_0_d2 :  std_logic;
signal heap_bh96_w12_0, heap_bh96_w12_0_d1, heap_bh96_w12_0_d2 :  std_logic;
signal heap_bh96_w13_0, heap_bh96_w13_0_d1, heap_bh96_w13_0_d2 :  std_logic;
signal heap_bh96_w14_0, heap_bh96_w14_0_d1, heap_bh96_w14_0_d2 :  std_logic;
signal heap_bh96_w15_0, heap_bh96_w15_0_d1, heap_bh96_w15_0_d2 :  std_logic;
signal heap_bh96_w16_0, heap_bh96_w16_0_d1, heap_bh96_w16_0_d2 :  std_logic;
signal heap_bh96_w17_0, heap_bh96_w17_0_d1 :  std_logic;
signal heap_bh96_w18_0, heap_bh96_w18_0_d1 :  std_logic;
signal heap_bh96_w19_0, heap_bh96_w19_0_d1 :  std_logic;
signal heap_bh96_w20_0, heap_bh96_w20_0_d1 :  std_logic;
signal heap_bh96_w21_0, heap_bh96_w21_0_d1 :  std_logic;
signal heap_bh96_w22_0, heap_bh96_w22_0_d1 :  std_logic;
signal heap_bh96_w23_0, heap_bh96_w23_0_d1 :  std_logic;
signal heap_bh96_w24_0, heap_bh96_w24_0_d1 :  std_logic;
signal heap_bh96_w25_0, heap_bh96_w25_0_d1, heap_bh96_w25_0_d2 :  std_logic;
signal heap_bh96_w26_0, heap_bh96_w26_0_d1, heap_bh96_w26_0_d2 :  std_logic;
signal heap_bh96_w27_0, heap_bh96_w27_0_d1, heap_bh96_w27_0_d2 :  std_logic;
signal heap_bh96_w28_0, heap_bh96_w28_0_d1, heap_bh96_w28_0_d2 :  std_logic;
signal heap_bh96_w29_0, heap_bh96_w29_0_d1, heap_bh96_w29_0_d2 :  std_logic;
signal heap_bh96_w30_0, heap_bh96_w30_0_d1, heap_bh96_w30_0_d2 :  std_logic;
signal heap_bh96_w31_0, heap_bh96_w31_0_d1, heap_bh96_w31_0_d2 :  std_logic;
signal heap_bh96_w32_0, heap_bh96_w32_0_d1, heap_bh96_w32_0_d2 :  std_logic;
signal heap_bh96_w33_0, heap_bh96_w33_0_d1, heap_bh96_w33_0_d2 :  std_logic;
signal heap_bh96_w34_0, heap_bh96_w34_0_d1, heap_bh96_w34_0_d2 :  std_logic;
signal heap_bh96_w35_0, heap_bh96_w35_0_d1, heap_bh96_w35_0_d2 :  std_logic;
signal heap_bh96_w36_0, heap_bh96_w36_0_d1, heap_bh96_w36_0_d2 :  std_logic;
signal heap_bh96_w37_0, heap_bh96_w37_0_d1, heap_bh96_w37_0_d2 :  std_logic;
signal heap_bh96_w38_0, heap_bh96_w38_0_d1, heap_bh96_w38_0_d2 :  std_logic;
signal heap_bh96_w39_0, heap_bh96_w39_0_d1, heap_bh96_w39_0_d2 :  std_logic;
signal heap_bh96_w40_0, heap_bh96_w40_0_d1, heap_bh96_w40_0_d2 :  std_logic;
signal heap_bh96_w41_0, heap_bh96_w41_0_d1, heap_bh96_w41_0_d2 :  std_logic;
signal heap_bh96_w42_0, heap_bh96_w42_0_d1, heap_bh96_w42_0_d2 :  std_logic;
signal heap_bh96_w43_0, heap_bh96_w43_0_d1, heap_bh96_w43_0_d2 :  std_logic;
signal DSP_bh96_ch1_0 :  std_logic_vector(42 downto 0);
signal heap_bh96_w17_1, heap_bh96_w17_1_d1 :  std_logic;
signal heap_bh96_w16_1, heap_bh96_w16_1_d1, heap_bh96_w16_1_d2 :  std_logic;
signal heap_bh96_w15_1, heap_bh96_w15_1_d1, heap_bh96_w15_1_d2 :  std_logic;
signal heap_bh96_w14_1, heap_bh96_w14_1_d1, heap_bh96_w14_1_d2 :  std_logic;
signal heap_bh96_w13_1, heap_bh96_w13_1_d1, heap_bh96_w13_1_d2 :  std_logic;
signal heap_bh96_w12_1, heap_bh96_w12_1_d1, heap_bh96_w12_1_d2 :  std_logic;
signal heap_bh96_w11_1, heap_bh96_w11_1_d1, heap_bh96_w11_1_d2 :  std_logic;
signal heap_bh96_w10_1, heap_bh96_w10_1_d1, heap_bh96_w10_1_d2 :  std_logic;
signal heap_bh96_w9_1, heap_bh96_w9_1_d1, heap_bh96_w9_1_d2 :  std_logic;
signal heap_bh96_w8_1, heap_bh96_w8_1_d1, heap_bh96_w8_1_d2 :  std_logic;
signal heap_bh96_w7_1, heap_bh96_w7_1_d1, heap_bh96_w7_1_d2 :  std_logic;
signal heap_bh96_w6_0, heap_bh96_w6_0_d1, heap_bh96_w6_0_d2 :  std_logic;
signal heap_bh96_w5_0, heap_bh96_w5_0_d1, heap_bh96_w5_0_d2 :  std_logic;
signal heap_bh96_w4_0, heap_bh96_w4_0_d1, heap_bh96_w4_0_d2, heap_bh96_w4_0_d3, heap_bh96_w4_0_d4, heap_bh96_w4_0_d5 :  std_logic;
signal heap_bh96_w3_0, heap_bh96_w3_0_d1, heap_bh96_w3_0_d2, heap_bh96_w3_0_d3, heap_bh96_w3_0_d4, heap_bh96_w3_0_d5 :  std_logic;
signal heap_bh96_w2_0, heap_bh96_w2_0_d1, heap_bh96_w2_0_d2, heap_bh96_w2_0_d3, heap_bh96_w2_0_d4, heap_bh96_w2_0_d5 :  std_logic;
signal heap_bh96_w1_0, heap_bh96_w1_0_d1, heap_bh96_w1_0_d2, heap_bh96_w1_0_d3, heap_bh96_w1_0_d4, heap_bh96_w1_0_d5 :  std_logic;
signal heap_bh96_w0_0, heap_bh96_w0_0_d1, heap_bh96_w0_0_d2, heap_bh96_w0_0_d3, heap_bh96_w0_0_d4, heap_bh96_w0_0_d5 :  std_logic;
signal DSP_bh96_ch2_0, DSP_bh96_ch2_0_d1 :  std_logic_vector(42 downto 0);
signal DSP_bh96_root2_1, DSP_bh96_root2_1_d1 :  std_logic_vector(42 downto 0);
signal DSP_bh96_ch2_1 :  std_logic_vector(43 downto 0);
signal heap_bh96_w42_1, heap_bh96_w42_1_d1 :  std_logic;
signal heap_bh96_w41_1, heap_bh96_w41_1_d1 :  std_logic;
signal heap_bh96_w40_1, heap_bh96_w40_1_d1, heap_bh96_w40_1_d2, heap_bh96_w40_1_d3 :  std_logic;
signal heap_bh96_w39_1, heap_bh96_w39_1_d1 :  std_logic;
signal heap_bh96_w38_1, heap_bh96_w38_1_d1, heap_bh96_w38_1_d2 :  std_logic;
signal heap_bh96_w37_1, heap_bh96_w37_1_d1 :  std_logic;
signal heap_bh96_w36_1, heap_bh96_w36_1_d1, heap_bh96_w36_1_d2 :  std_logic;
signal heap_bh96_w35_1, heap_bh96_w35_1_d1 :  std_logic;
signal heap_bh96_w34_1, heap_bh96_w34_1_d1, heap_bh96_w34_1_d2 :  std_logic;
signal heap_bh96_w33_1, heap_bh96_w33_1_d1 :  std_logic;
signal heap_bh96_w32_1, heap_bh96_w32_1_d1, heap_bh96_w32_1_d2 :  std_logic;
signal heap_bh96_w31_1, heap_bh96_w31_1_d1 :  std_logic;
signal heap_bh96_w30_1, heap_bh96_w30_1_d1 :  std_logic;
signal heap_bh96_w29_1, heap_bh96_w29_1_d1 :  std_logic;
signal heap_bh96_w28_1, heap_bh96_w28_1_d1 :  std_logic;
signal heap_bh96_w27_1, heap_bh96_w27_1_d1 :  std_logic;
signal heap_bh96_w26_1, heap_bh96_w26_1_d1 :  std_logic;
signal heap_bh96_w25_1, heap_bh96_w25_1_d1 :  std_logic;
signal heap_bh96_w24_1, heap_bh96_w24_1_d1, heap_bh96_w24_1_d2 :  std_logic;
signal heap_bh96_w23_1, heap_bh96_w23_1_d1, heap_bh96_w23_1_d2 :  std_logic;
signal heap_bh96_w22_1, heap_bh96_w22_1_d1 :  std_logic;
signal heap_bh96_w21_1, heap_bh96_w21_1_d1 :  std_logic;
signal heap_bh96_w20_1, heap_bh96_w20_1_d1 :  std_logic;
signal heap_bh96_w19_1, heap_bh96_w19_1_d1 :  std_logic;
signal heap_bh96_w18_1, heap_bh96_w18_1_d1 :  std_logic;
signal heap_bh96_w17_2, heap_bh96_w17_2_d1 :  std_logic;
signal heap_bh96_w16_2, heap_bh96_w16_2_d1, heap_bh96_w16_2_d2, heap_bh96_w16_2_d3, heap_bh96_w16_2_d4 :  std_logic;
signal heap_bh96_w15_2, heap_bh96_w15_2_d1 :  std_logic;
signal heap_bh96_w14_2, heap_bh96_w14_2_d1, heap_bh96_w14_2_d2, heap_bh96_w14_2_d3, heap_bh96_w14_2_d4 :  std_logic;
signal heap_bh96_w13_2, heap_bh96_w13_2_d1 :  std_logic;
signal heap_bh96_w12_2, heap_bh96_w12_2_d1, heap_bh96_w12_2_d2, heap_bh96_w12_2_d3, heap_bh96_w12_2_d4 :  std_logic;
signal heap_bh96_w11_2, heap_bh96_w11_2_d1 :  std_logic;
signal heap_bh96_w10_2, heap_bh96_w10_2_d1, heap_bh96_w10_2_d2, heap_bh96_w10_2_d3, heap_bh96_w10_2_d4 :  std_logic;
signal heap_bh96_w9_2, heap_bh96_w9_2_d1 :  std_logic;
signal heap_bh96_w8_2, heap_bh96_w8_2_d1, heap_bh96_w8_2_d2, heap_bh96_w8_2_d3, heap_bh96_w8_2_d4 :  std_logic;
signal heap_bh96_w7_2, heap_bh96_w7_2_d1 :  std_logic;
signal heap_bh96_w6_1, heap_bh96_w6_1_d1 :  std_logic;
signal heap_bh96_w5_1, heap_bh96_w5_1_d1 :  std_logic;
signal heap_bh96_w4_1, heap_bh96_w4_1_d1, heap_bh96_w4_1_d2, heap_bh96_w4_1_d3, heap_bh96_w4_1_d4 :  std_logic;
signal heap_bh96_w3_1, heap_bh96_w3_1_d1, heap_bh96_w3_1_d2, heap_bh96_w3_1_d3, heap_bh96_w3_1_d4 :  std_logic;
signal heap_bh96_w2_1, heap_bh96_w2_1_d1, heap_bh96_w2_1_d2, heap_bh96_w2_1_d3, heap_bh96_w2_1_d4 :  std_logic;
signal heap_bh96_w1_1, heap_bh96_w1_1_d1, heap_bh96_w1_1_d2, heap_bh96_w1_1_d3, heap_bh96_w1_1_d4 :  std_logic;
signal heap_bh96_w0_1, heap_bh96_w0_1_d1, heap_bh96_w0_1_d2, heap_bh96_w0_1_d3, heap_bh96_w0_1_d4 :  std_logic;
signal heap_bh96_w5_2, heap_bh96_w5_2_d1, heap_bh96_w5_2_d2 :  std_logic;
signal heap_bh96_w17_3, heap_bh96_w17_3_d1 :  std_logic;
signal heap_bh96_w18_2, heap_bh96_w18_2_d1 :  std_logic;
signal heap_bh96_w19_2, heap_bh96_w19_2_d1 :  std_logic;
signal heap_bh96_w20_2, heap_bh96_w20_2_d1 :  std_logic;
signal heap_bh96_w21_2, heap_bh96_w21_2_d1 :  std_logic;
signal heap_bh96_w22_2, heap_bh96_w22_2_d1 :  std_logic;
signal heap_bh96_w23_2, heap_bh96_w23_2_d1 :  std_logic;
signal heap_bh96_w24_2, heap_bh96_w24_2_d1 :  std_logic;
signal heap_bh96_w25_2, heap_bh96_w25_2_d1, heap_bh96_w25_2_d2 :  std_logic;
signal heap_bh96_w26_2, heap_bh96_w26_2_d1, heap_bh96_w26_2_d2 :  std_logic;
signal heap_bh96_w27_2, heap_bh96_w27_2_d1, heap_bh96_w27_2_d2 :  std_logic;
signal heap_bh96_w28_2, heap_bh96_w28_2_d1, heap_bh96_w28_2_d2 :  std_logic;
signal heap_bh96_w29_2, heap_bh96_w29_2_d1, heap_bh96_w29_2_d2 :  std_logic;
signal heap_bh96_w30_2, heap_bh96_w30_2_d1, heap_bh96_w30_2_d2 :  std_logic;
signal heap_bh96_w31_2, heap_bh96_w31_2_d1, heap_bh96_w31_2_d2 :  std_logic;
signal heap_bh96_w32_2, heap_bh96_w32_2_d1, heap_bh96_w32_2_d2 :  std_logic;
signal heap_bh96_w33_2, heap_bh96_w33_2_d1, heap_bh96_w33_2_d2 :  std_logic;
signal heap_bh96_w34_2, heap_bh96_w34_2_d1, heap_bh96_w34_2_d2 :  std_logic;
signal heap_bh96_w35_2, heap_bh96_w35_2_d1, heap_bh96_w35_2_d2 :  std_logic;
signal heap_bh96_w36_2, heap_bh96_w36_2_d1, heap_bh96_w36_2_d2 :  std_logic;
signal heap_bh96_w37_2, heap_bh96_w37_2_d1, heap_bh96_w37_2_d2 :  std_logic;
signal heap_bh96_w38_2, heap_bh96_w38_2_d1, heap_bh96_w38_2_d2 :  std_logic;
signal heap_bh96_w39_2, heap_bh96_w39_2_d1, heap_bh96_w39_2_d2 :  std_logic;
signal heap_bh96_w40_2, heap_bh96_w40_2_d1, heap_bh96_w40_2_d2 :  std_logic;
signal heap_bh96_w41_2, heap_bh96_w41_2_d1, heap_bh96_w41_2_d2 :  std_logic;
signal heap_bh96_w43_1, heap_bh96_w43_1_d1, heap_bh96_w43_1_d2 :  std_logic;
signal CompressorIn_bh96_0_0 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_0_1 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_0_0 :  std_logic_vector(2 downto 0);
signal heap_bh96_w17_4, heap_bh96_w17_4_d1 :  std_logic;
signal heap_bh96_w18_3, heap_bh96_w18_3_d1 :  std_logic;
signal heap_bh96_w19_3 :  std_logic;
signal CompressorIn_bh96_1_2 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_1_3 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_1_1 :  std_logic_vector(2 downto 0);
signal heap_bh96_w19_4, heap_bh96_w19_4_d1 :  std_logic;
signal heap_bh96_w20_3, heap_bh96_w20_3_d1 :  std_logic;
signal heap_bh96_w21_3 :  std_logic;
signal CompressorIn_bh96_2_4 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_2_5 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_2_2 :  std_logic_vector(2 downto 0);
signal heap_bh96_w21_4, heap_bh96_w21_4_d1 :  std_logic;
signal heap_bh96_w22_3, heap_bh96_w22_3_d1 :  std_logic;
signal heap_bh96_w23_3 :  std_logic;
signal CompressorIn_bh96_3_6 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_3_7 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_3_3 :  std_logic_vector(2 downto 0);
signal heap_bh96_w23_4, heap_bh96_w23_4_d1, heap_bh96_w23_4_d2 :  std_logic;
signal heap_bh96_w24_3, heap_bh96_w24_3_d1, heap_bh96_w24_3_d2 :  std_logic;
signal heap_bh96_w25_3, heap_bh96_w25_3_d1 :  std_logic;
signal CompressorIn_bh96_4_8 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh96_4_9 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh96_4_4 :  std_logic_vector(2 downto 0);
signal heap_bh96_w25_4, heap_bh96_w25_4_d1, heap_bh96_w25_4_d2, heap_bh96_w25_4_d3 :  std_logic;
signal heap_bh96_w26_3 :  std_logic;
signal heap_bh96_w27_3 :  std_logic;
signal CompressorIn_bh96_5_10 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_5_11 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_5_5 :  std_logic_vector(2 downto 0);
signal heap_bh96_w5_3, heap_bh96_w5_3_d1, heap_bh96_w5_3_d2, heap_bh96_w5_3_d3 :  std_logic;
signal heap_bh96_w6_2, heap_bh96_w6_2_d1, heap_bh96_w6_2_d2, heap_bh96_w6_2_d3 :  std_logic;
signal heap_bh96_w7_3, heap_bh96_w7_3_d1, heap_bh96_w7_3_d2, heap_bh96_w7_3_d3 :  std_logic;
signal CompressorIn_bh96_6_12 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_6_13 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_6_6 :  std_logic_vector(2 downto 0);
signal heap_bh96_w7_4, heap_bh96_w7_4_d1, heap_bh96_w7_4_d2, heap_bh96_w7_4_d3 :  std_logic;
signal heap_bh96_w8_3, heap_bh96_w8_3_d1, heap_bh96_w8_3_d2, heap_bh96_w8_3_d3 :  std_logic;
signal heap_bh96_w9_3, heap_bh96_w9_3_d1, heap_bh96_w9_3_d2, heap_bh96_w9_3_d3 :  std_logic;
signal CompressorIn_bh96_7_14 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_7_15 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_7_7 :  std_logic_vector(2 downto 0);
signal heap_bh96_w9_4, heap_bh96_w9_4_d1, heap_bh96_w9_4_d2, heap_bh96_w9_4_d3 :  std_logic;
signal heap_bh96_w10_3, heap_bh96_w10_3_d1, heap_bh96_w10_3_d2, heap_bh96_w10_3_d3 :  std_logic;
signal heap_bh96_w11_3, heap_bh96_w11_3_d1, heap_bh96_w11_3_d2, heap_bh96_w11_3_d3 :  std_logic;
signal CompressorIn_bh96_8_16 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_8_17 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_8_8 :  std_logic_vector(2 downto 0);
signal heap_bh96_w11_4, heap_bh96_w11_4_d1, heap_bh96_w11_4_d2, heap_bh96_w11_4_d3 :  std_logic;
signal heap_bh96_w12_3, heap_bh96_w12_3_d1, heap_bh96_w12_3_d2, heap_bh96_w12_3_d3 :  std_logic;
signal heap_bh96_w13_3, heap_bh96_w13_3_d1, heap_bh96_w13_3_d2, heap_bh96_w13_3_d3 :  std_logic;
signal CompressorIn_bh96_9_18 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_9_19 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_9_9 :  std_logic_vector(2 downto 0);
signal heap_bh96_w13_4, heap_bh96_w13_4_d1, heap_bh96_w13_4_d2, heap_bh96_w13_4_d3 :  std_logic;
signal heap_bh96_w14_3, heap_bh96_w14_3_d1, heap_bh96_w14_3_d2, heap_bh96_w14_3_d3 :  std_logic;
signal heap_bh96_w15_3, heap_bh96_w15_3_d1, heap_bh96_w15_3_d2, heap_bh96_w15_3_d3 :  std_logic;
signal CompressorIn_bh96_10_20 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_10_21 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_10_10 :  std_logic_vector(2 downto 0);
signal heap_bh96_w15_4, heap_bh96_w15_4_d1, heap_bh96_w15_4_d2, heap_bh96_w15_4_d3 :  std_logic;
signal heap_bh96_w16_3, heap_bh96_w16_3_d1, heap_bh96_w16_3_d2, heap_bh96_w16_3_d3 :  std_logic;
signal heap_bh96_w17_5 :  std_logic;
signal CompressorIn_bh96_11_22 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_11_23 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_11_11 :  std_logic_vector(2 downto 0);
signal heap_bh96_w27_4 :  std_logic;
signal heap_bh96_w28_3 :  std_logic;
signal heap_bh96_w29_3 :  std_logic;
signal CompressorIn_bh96_12_24 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_12_25 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_12_12 :  std_logic_vector(2 downto 0);
signal heap_bh96_w29_4 :  std_logic;
signal heap_bh96_w30_3 :  std_logic;
signal heap_bh96_w31_3 :  std_logic;
signal CompressorIn_bh96_13_26 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_13_27 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_13_13 :  std_logic_vector(2 downto 0);
signal heap_bh96_w31_4 :  std_logic;
signal heap_bh96_w32_3, heap_bh96_w32_3_d1 :  std_logic;
signal heap_bh96_w33_3, heap_bh96_w33_3_d1 :  std_logic;
signal CompressorIn_bh96_14_28 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_14_29 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_14_14 :  std_logic_vector(2 downto 0);
signal heap_bh96_w33_4, heap_bh96_w33_4_d1 :  std_logic;
signal heap_bh96_w34_3, heap_bh96_w34_3_d1 :  std_logic;
signal heap_bh96_w35_3, heap_bh96_w35_3_d1 :  std_logic;
signal CompressorIn_bh96_15_30 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_15_31 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_15_15 :  std_logic_vector(2 downto 0);
signal heap_bh96_w35_4, heap_bh96_w35_4_d1 :  std_logic;
signal heap_bh96_w36_3, heap_bh96_w36_3_d1 :  std_logic;
signal heap_bh96_w37_3, heap_bh96_w37_3_d1 :  std_logic;
signal CompressorIn_bh96_16_32 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_16_33 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_16_16 :  std_logic_vector(2 downto 0);
signal heap_bh96_w37_4, heap_bh96_w37_4_d1 :  std_logic;
signal heap_bh96_w38_3, heap_bh96_w38_3_d1 :  std_logic;
signal heap_bh96_w39_3, heap_bh96_w39_3_d1 :  std_logic;
signal CompressorIn_bh96_17_34 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_17_35 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_17_17 :  std_logic_vector(2 downto 0);
signal heap_bh96_w39_4, heap_bh96_w39_4_d1 :  std_logic;
signal heap_bh96_w40_3, heap_bh96_w40_3_d1, heap_bh96_w40_3_d2 :  std_logic;
signal heap_bh96_w41_3, heap_bh96_w41_3_d1, heap_bh96_w41_3_d2 :  std_logic;
signal CompressorIn_bh96_18_36 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_18_37 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_18_18 :  std_logic_vector(2 downto 0);
signal heap_bh96_w41_4, heap_bh96_w41_4_d1, heap_bh96_w41_4_d2 :  std_logic;
signal heap_bh96_w42_2, heap_bh96_w42_2_d1, heap_bh96_w42_2_d2, heap_bh96_w42_2_d3 :  std_logic;
signal heap_bh96_w43_2 :  std_logic;
signal CompressorIn_bh96_19_38 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_19_39 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_19_19 :  std_logic_vector(2 downto 0);
signal heap_bh96_w17_6, heap_bh96_w17_6_d1, heap_bh96_w17_6_d2, heap_bh96_w17_6_d3 :  std_logic;
signal heap_bh96_w18_4, heap_bh96_w18_4_d1, heap_bh96_w18_4_d2, heap_bh96_w18_4_d3 :  std_logic;
signal heap_bh96_w19_5 :  std_logic;
signal CompressorIn_bh96_20_40 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_20_41 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_20_20 :  std_logic_vector(2 downto 0);
signal heap_bh96_w26_4, heap_bh96_w26_4_d1, heap_bh96_w26_4_d2, heap_bh96_w26_4_d3 :  std_logic;
signal heap_bh96_w27_5, heap_bh96_w27_5_d1, heap_bh96_w27_5_d2, heap_bh96_w27_5_d3 :  std_logic;
signal heap_bh96_w28_4 :  std_logic;
signal CompressorIn_bh96_21_42 :  std_logic_vector(2 downto 0);
signal CompressorOut_bh96_21_21 :  std_logic_vector(1 downto 0);
signal heap_bh96_w43_3, heap_bh96_w43_3_d1, heap_bh96_w43_3_d2, heap_bh96_w43_3_d3 :  std_logic;
signal CompressorIn_bh96_22_43 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_22_44 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_22_22 :  std_logic_vector(2 downto 0);
signal heap_bh96_w19_6, heap_bh96_w19_6_d1, heap_bh96_w19_6_d2, heap_bh96_w19_6_d3 :  std_logic;
signal heap_bh96_w20_4, heap_bh96_w20_4_d1, heap_bh96_w20_4_d2, heap_bh96_w20_4_d3 :  std_logic;
signal heap_bh96_w21_5 :  std_logic;
signal CompressorIn_bh96_23_45 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_23_46 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_23_23 :  std_logic_vector(2 downto 0);
signal heap_bh96_w28_5, heap_bh96_w28_5_d1, heap_bh96_w28_5_d2, heap_bh96_w28_5_d3 :  std_logic;
signal heap_bh96_w29_5, heap_bh96_w29_5_d1, heap_bh96_w29_5_d2, heap_bh96_w29_5_d3 :  std_logic;
signal heap_bh96_w30_4 :  std_logic;
signal CompressorIn_bh96_24_47 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_24_48 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_24_24 :  std_logic_vector(2 downto 0);
signal heap_bh96_w21_6, heap_bh96_w21_6_d1, heap_bh96_w21_6_d2, heap_bh96_w21_6_d3 :  std_logic;
signal heap_bh96_w22_4, heap_bh96_w22_4_d1, heap_bh96_w22_4_d2, heap_bh96_w22_4_d3 :  std_logic;
signal heap_bh96_w23_5, heap_bh96_w23_5_d1 :  std_logic;
signal CompressorIn_bh96_25_49 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_25_50 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_25_25 :  std_logic_vector(2 downto 0);
signal heap_bh96_w30_5, heap_bh96_w30_5_d1, heap_bh96_w30_5_d2, heap_bh96_w30_5_d3 :  std_logic;
signal heap_bh96_w31_5, heap_bh96_w31_5_d1, heap_bh96_w31_5_d2, heap_bh96_w31_5_d3 :  std_logic;
signal heap_bh96_w32_4, heap_bh96_w32_4_d1 :  std_logic;
signal CompressorIn_bh96_26_51 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_26_52 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_26_26 :  std_logic_vector(2 downto 0);
signal heap_bh96_w23_6, heap_bh96_w23_6_d1, heap_bh96_w23_6_d2 :  std_logic;
signal heap_bh96_w24_4, heap_bh96_w24_4_d1, heap_bh96_w24_4_d2 :  std_logic;
signal heap_bh96_w25_5, heap_bh96_w25_5_d1, heap_bh96_w25_5_d2 :  std_logic;
signal CompressorIn_bh96_27_53 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_27_54 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_27_27 :  std_logic_vector(2 downto 0);
signal heap_bh96_w32_5, heap_bh96_w32_5_d1, heap_bh96_w32_5_d2 :  std_logic;
signal heap_bh96_w33_5, heap_bh96_w33_5_d1, heap_bh96_w33_5_d2 :  std_logic;
signal heap_bh96_w34_4 :  std_logic;
signal CompressorIn_bh96_28_55 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_28_56 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_28_28 :  std_logic_vector(2 downto 0);
signal heap_bh96_w34_5, heap_bh96_w34_5_d1, heap_bh96_w34_5_d2 :  std_logic;
signal heap_bh96_w35_5, heap_bh96_w35_5_d1, heap_bh96_w35_5_d2 :  std_logic;
signal heap_bh96_w36_4 :  std_logic;
signal CompressorIn_bh96_29_57 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_29_58 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_29_29 :  std_logic_vector(2 downto 0);
signal heap_bh96_w36_5, heap_bh96_w36_5_d1, heap_bh96_w36_5_d2 :  std_logic;
signal heap_bh96_w37_5, heap_bh96_w37_5_d1, heap_bh96_w37_5_d2 :  std_logic;
signal heap_bh96_w38_4 :  std_logic;
signal CompressorIn_bh96_30_59 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_30_60 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_30_30 :  std_logic_vector(2 downto 0);
signal heap_bh96_w38_5, heap_bh96_w38_5_d1, heap_bh96_w38_5_d2 :  std_logic;
signal heap_bh96_w39_5, heap_bh96_w39_5_d1, heap_bh96_w39_5_d2 :  std_logic;
signal heap_bh96_w40_4, heap_bh96_w40_4_d1 :  std_logic;
signal CompressorIn_bh96_31_61 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh96_31_62 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh96_31_31 :  std_logic_vector(2 downto 0);
signal heap_bh96_w40_5, heap_bh96_w40_5_d1 :  std_logic;
signal heap_bh96_w41_5, heap_bh96_w41_5_d1 :  std_logic;
signal heap_bh96_w42_3, heap_bh96_w42_3_d1 :  std_logic;
signal finalAdderIn0_bh96 :  std_logic_vector(44 downto 0);
signal finalAdderIn1_bh96 :  std_logic_vector(44 downto 0);
signal finalAdderCin_bh96 :  std_logic;
signal finalAdderOut_bh96 :  std_logic_vector(44 downto 0);
signal CompressionResult96 :  std_logic_vector(44 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            heap_bh96_w7_0_d1 <=  heap_bh96_w7_0;
            heap_bh96_w7_0_d2 <=  heap_bh96_w7_0_d1;
            heap_bh96_w8_0_d1 <=  heap_bh96_w8_0;
            heap_bh96_w8_0_d2 <=  heap_bh96_w8_0_d1;
            heap_bh96_w9_0_d1 <=  heap_bh96_w9_0;
            heap_bh96_w9_0_d2 <=  heap_bh96_w9_0_d1;
            heap_bh96_w10_0_d1 <=  heap_bh96_w10_0;
            heap_bh96_w10_0_d2 <=  heap_bh96_w10_0_d1;
            heap_bh96_w11_0_d1 <=  heap_bh96_w11_0;
            heap_bh96_w11_0_d2 <=  heap_bh96_w11_0_d1;
            heap_bh96_w12_0_d1 <=  heap_bh96_w12_0;
            heap_bh96_w12_0_d2 <=  heap_bh96_w12_0_d1;
            heap_bh96_w13_0_d1 <=  heap_bh96_w13_0;
            heap_bh96_w13_0_d2 <=  heap_bh96_w13_0_d1;
            heap_bh96_w14_0_d1 <=  heap_bh96_w14_0;
            heap_bh96_w14_0_d2 <=  heap_bh96_w14_0_d1;
            heap_bh96_w15_0_d1 <=  heap_bh96_w15_0;
            heap_bh96_w15_0_d2 <=  heap_bh96_w15_0_d1;
            heap_bh96_w16_0_d1 <=  heap_bh96_w16_0;
            heap_bh96_w16_0_d2 <=  heap_bh96_w16_0_d1;
            heap_bh96_w17_0_d1 <=  heap_bh96_w17_0;
            heap_bh96_w18_0_d1 <=  heap_bh96_w18_0;
            heap_bh96_w19_0_d1 <=  heap_bh96_w19_0;
            heap_bh96_w20_0_d1 <=  heap_bh96_w20_0;
            heap_bh96_w21_0_d1 <=  heap_bh96_w21_0;
            heap_bh96_w22_0_d1 <=  heap_bh96_w22_0;
            heap_bh96_w23_0_d1 <=  heap_bh96_w23_0;
            heap_bh96_w24_0_d1 <=  heap_bh96_w24_0;
            heap_bh96_w25_0_d1 <=  heap_bh96_w25_0;
            heap_bh96_w25_0_d2 <=  heap_bh96_w25_0_d1;
            heap_bh96_w26_0_d1 <=  heap_bh96_w26_0;
            heap_bh96_w26_0_d2 <=  heap_bh96_w26_0_d1;
            heap_bh96_w27_0_d1 <=  heap_bh96_w27_0;
            heap_bh96_w27_0_d2 <=  heap_bh96_w27_0_d1;
            heap_bh96_w28_0_d1 <=  heap_bh96_w28_0;
            heap_bh96_w28_0_d2 <=  heap_bh96_w28_0_d1;
            heap_bh96_w29_0_d1 <=  heap_bh96_w29_0;
            heap_bh96_w29_0_d2 <=  heap_bh96_w29_0_d1;
            heap_bh96_w30_0_d1 <=  heap_bh96_w30_0;
            heap_bh96_w30_0_d2 <=  heap_bh96_w30_0_d1;
            heap_bh96_w31_0_d1 <=  heap_bh96_w31_0;
            heap_bh96_w31_0_d2 <=  heap_bh96_w31_0_d1;
            heap_bh96_w32_0_d1 <=  heap_bh96_w32_0;
            heap_bh96_w32_0_d2 <=  heap_bh96_w32_0_d1;
            heap_bh96_w33_0_d1 <=  heap_bh96_w33_0;
            heap_bh96_w33_0_d2 <=  heap_bh96_w33_0_d1;
            heap_bh96_w34_0_d1 <=  heap_bh96_w34_0;
            heap_bh96_w34_0_d2 <=  heap_bh96_w34_0_d1;
            heap_bh96_w35_0_d1 <=  heap_bh96_w35_0;
            heap_bh96_w35_0_d2 <=  heap_bh96_w35_0_d1;
            heap_bh96_w36_0_d1 <=  heap_bh96_w36_0;
            heap_bh96_w36_0_d2 <=  heap_bh96_w36_0_d1;
            heap_bh96_w37_0_d1 <=  heap_bh96_w37_0;
            heap_bh96_w37_0_d2 <=  heap_bh96_w37_0_d1;
            heap_bh96_w38_0_d1 <=  heap_bh96_w38_0;
            heap_bh96_w38_0_d2 <=  heap_bh96_w38_0_d1;
            heap_bh96_w39_0_d1 <=  heap_bh96_w39_0;
            heap_bh96_w39_0_d2 <=  heap_bh96_w39_0_d1;
            heap_bh96_w40_0_d1 <=  heap_bh96_w40_0;
            heap_bh96_w40_0_d2 <=  heap_bh96_w40_0_d1;
            heap_bh96_w41_0_d1 <=  heap_bh96_w41_0;
            heap_bh96_w41_0_d2 <=  heap_bh96_w41_0_d1;
            heap_bh96_w42_0_d1 <=  heap_bh96_w42_0;
            heap_bh96_w42_0_d2 <=  heap_bh96_w42_0_d1;
            heap_bh96_w43_0_d1 <=  heap_bh96_w43_0;
            heap_bh96_w43_0_d2 <=  heap_bh96_w43_0_d1;
            heap_bh96_w17_1_d1 <=  heap_bh96_w17_1;
            heap_bh96_w16_1_d1 <=  heap_bh96_w16_1;
            heap_bh96_w16_1_d2 <=  heap_bh96_w16_1_d1;
            heap_bh96_w15_1_d1 <=  heap_bh96_w15_1;
            heap_bh96_w15_1_d2 <=  heap_bh96_w15_1_d1;
            heap_bh96_w14_1_d1 <=  heap_bh96_w14_1;
            heap_bh96_w14_1_d2 <=  heap_bh96_w14_1_d1;
            heap_bh96_w13_1_d1 <=  heap_bh96_w13_1;
            heap_bh96_w13_1_d2 <=  heap_bh96_w13_1_d1;
            heap_bh96_w12_1_d1 <=  heap_bh96_w12_1;
            heap_bh96_w12_1_d2 <=  heap_bh96_w12_1_d1;
            heap_bh96_w11_1_d1 <=  heap_bh96_w11_1;
            heap_bh96_w11_1_d2 <=  heap_bh96_w11_1_d1;
            heap_bh96_w10_1_d1 <=  heap_bh96_w10_1;
            heap_bh96_w10_1_d2 <=  heap_bh96_w10_1_d1;
            heap_bh96_w9_1_d1 <=  heap_bh96_w9_1;
            heap_bh96_w9_1_d2 <=  heap_bh96_w9_1_d1;
            heap_bh96_w8_1_d1 <=  heap_bh96_w8_1;
            heap_bh96_w8_1_d2 <=  heap_bh96_w8_1_d1;
            heap_bh96_w7_1_d1 <=  heap_bh96_w7_1;
            heap_bh96_w7_1_d2 <=  heap_bh96_w7_1_d1;
            heap_bh96_w6_0_d1 <=  heap_bh96_w6_0;
            heap_bh96_w6_0_d2 <=  heap_bh96_w6_0_d1;
            heap_bh96_w5_0_d1 <=  heap_bh96_w5_0;
            heap_bh96_w5_0_d2 <=  heap_bh96_w5_0_d1;
            heap_bh96_w4_0_d1 <=  heap_bh96_w4_0;
            heap_bh96_w4_0_d2 <=  heap_bh96_w4_0_d1;
            heap_bh96_w4_0_d3 <=  heap_bh96_w4_0_d2;
            heap_bh96_w4_0_d4 <=  heap_bh96_w4_0_d3;
            heap_bh96_w4_0_d5 <=  heap_bh96_w4_0_d4;
            heap_bh96_w3_0_d1 <=  heap_bh96_w3_0;
            heap_bh96_w3_0_d2 <=  heap_bh96_w3_0_d1;
            heap_bh96_w3_0_d3 <=  heap_bh96_w3_0_d2;
            heap_bh96_w3_0_d4 <=  heap_bh96_w3_0_d3;
            heap_bh96_w3_0_d5 <=  heap_bh96_w3_0_d4;
            heap_bh96_w2_0_d1 <=  heap_bh96_w2_0;
            heap_bh96_w2_0_d2 <=  heap_bh96_w2_0_d1;
            heap_bh96_w2_0_d3 <=  heap_bh96_w2_0_d2;
            heap_bh96_w2_0_d4 <=  heap_bh96_w2_0_d3;
            heap_bh96_w2_0_d5 <=  heap_bh96_w2_0_d4;
            heap_bh96_w1_0_d1 <=  heap_bh96_w1_0;
            heap_bh96_w1_0_d2 <=  heap_bh96_w1_0_d1;
            heap_bh96_w1_0_d3 <=  heap_bh96_w1_0_d2;
            heap_bh96_w1_0_d4 <=  heap_bh96_w1_0_d3;
            heap_bh96_w1_0_d5 <=  heap_bh96_w1_0_d4;
            heap_bh96_w0_0_d1 <=  heap_bh96_w0_0;
            heap_bh96_w0_0_d2 <=  heap_bh96_w0_0_d1;
            heap_bh96_w0_0_d3 <=  heap_bh96_w0_0_d2;
            heap_bh96_w0_0_d4 <=  heap_bh96_w0_0_d3;
            heap_bh96_w0_0_d5 <=  heap_bh96_w0_0_d4;
            DSP_bh96_ch2_0_d1 <=  DSP_bh96_ch2_0;
            DSP_bh96_root2_1_d1 <=  DSP_bh96_root2_1;
            heap_bh96_w42_1_d1 <=  heap_bh96_w42_1;
            heap_bh96_w41_1_d1 <=  heap_bh96_w41_1;
            heap_bh96_w40_1_d1 <=  heap_bh96_w40_1;
            heap_bh96_w40_1_d2 <=  heap_bh96_w40_1_d1;
            heap_bh96_w40_1_d3 <=  heap_bh96_w40_1_d2;
            heap_bh96_w39_1_d1 <=  heap_bh96_w39_1;
            heap_bh96_w38_1_d1 <=  heap_bh96_w38_1;
            heap_bh96_w38_1_d2 <=  heap_bh96_w38_1_d1;
            heap_bh96_w37_1_d1 <=  heap_bh96_w37_1;
            heap_bh96_w36_1_d1 <=  heap_bh96_w36_1;
            heap_bh96_w36_1_d2 <=  heap_bh96_w36_1_d1;
            heap_bh96_w35_1_d1 <=  heap_bh96_w35_1;
            heap_bh96_w34_1_d1 <=  heap_bh96_w34_1;
            heap_bh96_w34_1_d2 <=  heap_bh96_w34_1_d1;
            heap_bh96_w33_1_d1 <=  heap_bh96_w33_1;
            heap_bh96_w32_1_d1 <=  heap_bh96_w32_1;
            heap_bh96_w32_1_d2 <=  heap_bh96_w32_1_d1;
            heap_bh96_w31_1_d1 <=  heap_bh96_w31_1;
            heap_bh96_w30_1_d1 <=  heap_bh96_w30_1;
            heap_bh96_w29_1_d1 <=  heap_bh96_w29_1;
            heap_bh96_w28_1_d1 <=  heap_bh96_w28_1;
            heap_bh96_w27_1_d1 <=  heap_bh96_w27_1;
            heap_bh96_w26_1_d1 <=  heap_bh96_w26_1;
            heap_bh96_w25_1_d1 <=  heap_bh96_w25_1;
            heap_bh96_w24_1_d1 <=  heap_bh96_w24_1;
            heap_bh96_w24_1_d2 <=  heap_bh96_w24_1_d1;
            heap_bh96_w23_1_d1 <=  heap_bh96_w23_1;
            heap_bh96_w23_1_d2 <=  heap_bh96_w23_1_d1;
            heap_bh96_w22_1_d1 <=  heap_bh96_w22_1;
            heap_bh96_w21_1_d1 <=  heap_bh96_w21_1;
            heap_bh96_w20_1_d1 <=  heap_bh96_w20_1;
            heap_bh96_w19_1_d1 <=  heap_bh96_w19_1;
            heap_bh96_w18_1_d1 <=  heap_bh96_w18_1;
            heap_bh96_w17_2_d1 <=  heap_bh96_w17_2;
            heap_bh96_w16_2_d1 <=  heap_bh96_w16_2;
            heap_bh96_w16_2_d2 <=  heap_bh96_w16_2_d1;
            heap_bh96_w16_2_d3 <=  heap_bh96_w16_2_d2;
            heap_bh96_w16_2_d4 <=  heap_bh96_w16_2_d3;
            heap_bh96_w15_2_d1 <=  heap_bh96_w15_2;
            heap_bh96_w14_2_d1 <=  heap_bh96_w14_2;
            heap_bh96_w14_2_d2 <=  heap_bh96_w14_2_d1;
            heap_bh96_w14_2_d3 <=  heap_bh96_w14_2_d2;
            heap_bh96_w14_2_d4 <=  heap_bh96_w14_2_d3;
            heap_bh96_w13_2_d1 <=  heap_bh96_w13_2;
            heap_bh96_w12_2_d1 <=  heap_bh96_w12_2;
            heap_bh96_w12_2_d2 <=  heap_bh96_w12_2_d1;
            heap_bh96_w12_2_d3 <=  heap_bh96_w12_2_d2;
            heap_bh96_w12_2_d4 <=  heap_bh96_w12_2_d3;
            heap_bh96_w11_2_d1 <=  heap_bh96_w11_2;
            heap_bh96_w10_2_d1 <=  heap_bh96_w10_2;
            heap_bh96_w10_2_d2 <=  heap_bh96_w10_2_d1;
            heap_bh96_w10_2_d3 <=  heap_bh96_w10_2_d2;
            heap_bh96_w10_2_d4 <=  heap_bh96_w10_2_d3;
            heap_bh96_w9_2_d1 <=  heap_bh96_w9_2;
            heap_bh96_w8_2_d1 <=  heap_bh96_w8_2;
            heap_bh96_w8_2_d2 <=  heap_bh96_w8_2_d1;
            heap_bh96_w8_2_d3 <=  heap_bh96_w8_2_d2;
            heap_bh96_w8_2_d4 <=  heap_bh96_w8_2_d3;
            heap_bh96_w7_2_d1 <=  heap_bh96_w7_2;
            heap_bh96_w6_1_d1 <=  heap_bh96_w6_1;
            heap_bh96_w5_1_d1 <=  heap_bh96_w5_1;
            heap_bh96_w4_1_d1 <=  heap_bh96_w4_1;
            heap_bh96_w4_1_d2 <=  heap_bh96_w4_1_d1;
            heap_bh96_w4_1_d3 <=  heap_bh96_w4_1_d2;
            heap_bh96_w4_1_d4 <=  heap_bh96_w4_1_d3;
            heap_bh96_w3_1_d1 <=  heap_bh96_w3_1;
            heap_bh96_w3_1_d2 <=  heap_bh96_w3_1_d1;
            heap_bh96_w3_1_d3 <=  heap_bh96_w3_1_d2;
            heap_bh96_w3_1_d4 <=  heap_bh96_w3_1_d3;
            heap_bh96_w2_1_d1 <=  heap_bh96_w2_1;
            heap_bh96_w2_1_d2 <=  heap_bh96_w2_1_d1;
            heap_bh96_w2_1_d3 <=  heap_bh96_w2_1_d2;
            heap_bh96_w2_1_d4 <=  heap_bh96_w2_1_d3;
            heap_bh96_w1_1_d1 <=  heap_bh96_w1_1;
            heap_bh96_w1_1_d2 <=  heap_bh96_w1_1_d1;
            heap_bh96_w1_1_d3 <=  heap_bh96_w1_1_d2;
            heap_bh96_w1_1_d4 <=  heap_bh96_w1_1_d3;
            heap_bh96_w0_1_d1 <=  heap_bh96_w0_1;
            heap_bh96_w0_1_d2 <=  heap_bh96_w0_1_d1;
            heap_bh96_w0_1_d3 <=  heap_bh96_w0_1_d2;
            heap_bh96_w0_1_d4 <=  heap_bh96_w0_1_d3;
            heap_bh96_w5_2_d1 <=  heap_bh96_w5_2;
            heap_bh96_w5_2_d2 <=  heap_bh96_w5_2_d1;
            heap_bh96_w17_3_d1 <=  heap_bh96_w17_3;
            heap_bh96_w18_2_d1 <=  heap_bh96_w18_2;
            heap_bh96_w19_2_d1 <=  heap_bh96_w19_2;
            heap_bh96_w20_2_d1 <=  heap_bh96_w20_2;
            heap_bh96_w21_2_d1 <=  heap_bh96_w21_2;
            heap_bh96_w22_2_d1 <=  heap_bh96_w22_2;
            heap_bh96_w23_2_d1 <=  heap_bh96_w23_2;
            heap_bh96_w24_2_d1 <=  heap_bh96_w24_2;
            heap_bh96_w25_2_d1 <=  heap_bh96_w25_2;
            heap_bh96_w25_2_d2 <=  heap_bh96_w25_2_d1;
            heap_bh96_w26_2_d1 <=  heap_bh96_w26_2;
            heap_bh96_w26_2_d2 <=  heap_bh96_w26_2_d1;
            heap_bh96_w27_2_d1 <=  heap_bh96_w27_2;
            heap_bh96_w27_2_d2 <=  heap_bh96_w27_2_d1;
            heap_bh96_w28_2_d1 <=  heap_bh96_w28_2;
            heap_bh96_w28_2_d2 <=  heap_bh96_w28_2_d1;
            heap_bh96_w29_2_d1 <=  heap_bh96_w29_2;
            heap_bh96_w29_2_d2 <=  heap_bh96_w29_2_d1;
            heap_bh96_w30_2_d1 <=  heap_bh96_w30_2;
            heap_bh96_w30_2_d2 <=  heap_bh96_w30_2_d1;
            heap_bh96_w31_2_d1 <=  heap_bh96_w31_2;
            heap_bh96_w31_2_d2 <=  heap_bh96_w31_2_d1;
            heap_bh96_w32_2_d1 <=  heap_bh96_w32_2;
            heap_bh96_w32_2_d2 <=  heap_bh96_w32_2_d1;
            heap_bh96_w33_2_d1 <=  heap_bh96_w33_2;
            heap_bh96_w33_2_d2 <=  heap_bh96_w33_2_d1;
            heap_bh96_w34_2_d1 <=  heap_bh96_w34_2;
            heap_bh96_w34_2_d2 <=  heap_bh96_w34_2_d1;
            heap_bh96_w35_2_d1 <=  heap_bh96_w35_2;
            heap_bh96_w35_2_d2 <=  heap_bh96_w35_2_d1;
            heap_bh96_w36_2_d1 <=  heap_bh96_w36_2;
            heap_bh96_w36_2_d2 <=  heap_bh96_w36_2_d1;
            heap_bh96_w37_2_d1 <=  heap_bh96_w37_2;
            heap_bh96_w37_2_d2 <=  heap_bh96_w37_2_d1;
            heap_bh96_w38_2_d1 <=  heap_bh96_w38_2;
            heap_bh96_w38_2_d2 <=  heap_bh96_w38_2_d1;
            heap_bh96_w39_2_d1 <=  heap_bh96_w39_2;
            heap_bh96_w39_2_d2 <=  heap_bh96_w39_2_d1;
            heap_bh96_w40_2_d1 <=  heap_bh96_w40_2;
            heap_bh96_w40_2_d2 <=  heap_bh96_w40_2_d1;
            heap_bh96_w41_2_d1 <=  heap_bh96_w41_2;
            heap_bh96_w41_2_d2 <=  heap_bh96_w41_2_d1;
            heap_bh96_w43_1_d1 <=  heap_bh96_w43_1;
            heap_bh96_w43_1_d2 <=  heap_bh96_w43_1_d1;
            heap_bh96_w17_4_d1 <=  heap_bh96_w17_4;
            heap_bh96_w18_3_d1 <=  heap_bh96_w18_3;
            heap_bh96_w19_4_d1 <=  heap_bh96_w19_4;
            heap_bh96_w20_3_d1 <=  heap_bh96_w20_3;
            heap_bh96_w21_4_d1 <=  heap_bh96_w21_4;
            heap_bh96_w22_3_d1 <=  heap_bh96_w22_3;
            heap_bh96_w23_4_d1 <=  heap_bh96_w23_4;
            heap_bh96_w23_4_d2 <=  heap_bh96_w23_4_d1;
            heap_bh96_w24_3_d1 <=  heap_bh96_w24_3;
            heap_bh96_w24_3_d2 <=  heap_bh96_w24_3_d1;
            heap_bh96_w25_3_d1 <=  heap_bh96_w25_3;
            heap_bh96_w25_4_d1 <=  heap_bh96_w25_4;
            heap_bh96_w25_4_d2 <=  heap_bh96_w25_4_d1;
            heap_bh96_w25_4_d3 <=  heap_bh96_w25_4_d2;
            heap_bh96_w5_3_d1 <=  heap_bh96_w5_3;
            heap_bh96_w5_3_d2 <=  heap_bh96_w5_3_d1;
            heap_bh96_w5_3_d3 <=  heap_bh96_w5_3_d2;
            heap_bh96_w6_2_d1 <=  heap_bh96_w6_2;
            heap_bh96_w6_2_d2 <=  heap_bh96_w6_2_d1;
            heap_bh96_w6_2_d3 <=  heap_bh96_w6_2_d2;
            heap_bh96_w7_3_d1 <=  heap_bh96_w7_3;
            heap_bh96_w7_3_d2 <=  heap_bh96_w7_3_d1;
            heap_bh96_w7_3_d3 <=  heap_bh96_w7_3_d2;
            heap_bh96_w7_4_d1 <=  heap_bh96_w7_4;
            heap_bh96_w7_4_d2 <=  heap_bh96_w7_4_d1;
            heap_bh96_w7_4_d3 <=  heap_bh96_w7_4_d2;
            heap_bh96_w8_3_d1 <=  heap_bh96_w8_3;
            heap_bh96_w8_3_d2 <=  heap_bh96_w8_3_d1;
            heap_bh96_w8_3_d3 <=  heap_bh96_w8_3_d2;
            heap_bh96_w9_3_d1 <=  heap_bh96_w9_3;
            heap_bh96_w9_3_d2 <=  heap_bh96_w9_3_d1;
            heap_bh96_w9_3_d3 <=  heap_bh96_w9_3_d2;
            heap_bh96_w9_4_d1 <=  heap_bh96_w9_4;
            heap_bh96_w9_4_d2 <=  heap_bh96_w9_4_d1;
            heap_bh96_w9_4_d3 <=  heap_bh96_w9_4_d2;
            heap_bh96_w10_3_d1 <=  heap_bh96_w10_3;
            heap_bh96_w10_3_d2 <=  heap_bh96_w10_3_d1;
            heap_bh96_w10_3_d3 <=  heap_bh96_w10_3_d2;
            heap_bh96_w11_3_d1 <=  heap_bh96_w11_3;
            heap_bh96_w11_3_d2 <=  heap_bh96_w11_3_d1;
            heap_bh96_w11_3_d3 <=  heap_bh96_w11_3_d2;
            heap_bh96_w11_4_d1 <=  heap_bh96_w11_4;
            heap_bh96_w11_4_d2 <=  heap_bh96_w11_4_d1;
            heap_bh96_w11_4_d3 <=  heap_bh96_w11_4_d2;
            heap_bh96_w12_3_d1 <=  heap_bh96_w12_3;
            heap_bh96_w12_3_d2 <=  heap_bh96_w12_3_d1;
            heap_bh96_w12_3_d3 <=  heap_bh96_w12_3_d2;
            heap_bh96_w13_3_d1 <=  heap_bh96_w13_3;
            heap_bh96_w13_3_d2 <=  heap_bh96_w13_3_d1;
            heap_bh96_w13_3_d3 <=  heap_bh96_w13_3_d2;
            heap_bh96_w13_4_d1 <=  heap_bh96_w13_4;
            heap_bh96_w13_4_d2 <=  heap_bh96_w13_4_d1;
            heap_bh96_w13_4_d3 <=  heap_bh96_w13_4_d2;
            heap_bh96_w14_3_d1 <=  heap_bh96_w14_3;
            heap_bh96_w14_3_d2 <=  heap_bh96_w14_3_d1;
            heap_bh96_w14_3_d3 <=  heap_bh96_w14_3_d2;
            heap_bh96_w15_3_d1 <=  heap_bh96_w15_3;
            heap_bh96_w15_3_d2 <=  heap_bh96_w15_3_d1;
            heap_bh96_w15_3_d3 <=  heap_bh96_w15_3_d2;
            heap_bh96_w15_4_d1 <=  heap_bh96_w15_4;
            heap_bh96_w15_4_d2 <=  heap_bh96_w15_4_d1;
            heap_bh96_w15_4_d3 <=  heap_bh96_w15_4_d2;
            heap_bh96_w16_3_d1 <=  heap_bh96_w16_3;
            heap_bh96_w16_3_d2 <=  heap_bh96_w16_3_d1;
            heap_bh96_w16_3_d3 <=  heap_bh96_w16_3_d2;
            heap_bh96_w32_3_d1 <=  heap_bh96_w32_3;
            heap_bh96_w33_3_d1 <=  heap_bh96_w33_3;
            heap_bh96_w33_4_d1 <=  heap_bh96_w33_4;
            heap_bh96_w34_3_d1 <=  heap_bh96_w34_3;
            heap_bh96_w35_3_d1 <=  heap_bh96_w35_3;
            heap_bh96_w35_4_d1 <=  heap_bh96_w35_4;
            heap_bh96_w36_3_d1 <=  heap_bh96_w36_3;
            heap_bh96_w37_3_d1 <=  heap_bh96_w37_3;
            heap_bh96_w37_4_d1 <=  heap_bh96_w37_4;
            heap_bh96_w38_3_d1 <=  heap_bh96_w38_3;
            heap_bh96_w39_3_d1 <=  heap_bh96_w39_3;
            heap_bh96_w39_4_d1 <=  heap_bh96_w39_4;
            heap_bh96_w40_3_d1 <=  heap_bh96_w40_3;
            heap_bh96_w40_3_d2 <=  heap_bh96_w40_3_d1;
            heap_bh96_w41_3_d1 <=  heap_bh96_w41_3;
            heap_bh96_w41_3_d2 <=  heap_bh96_w41_3_d1;
            heap_bh96_w41_4_d1 <=  heap_bh96_w41_4;
            heap_bh96_w41_4_d2 <=  heap_bh96_w41_4_d1;
            heap_bh96_w42_2_d1 <=  heap_bh96_w42_2;
            heap_bh96_w42_2_d2 <=  heap_bh96_w42_2_d1;
            heap_bh96_w42_2_d3 <=  heap_bh96_w42_2_d2;
            heap_bh96_w17_6_d1 <=  heap_bh96_w17_6;
            heap_bh96_w17_6_d2 <=  heap_bh96_w17_6_d1;
            heap_bh96_w17_6_d3 <=  heap_bh96_w17_6_d2;
            heap_bh96_w18_4_d1 <=  heap_bh96_w18_4;
            heap_bh96_w18_4_d2 <=  heap_bh96_w18_4_d1;
            heap_bh96_w18_4_d3 <=  heap_bh96_w18_4_d2;
            heap_bh96_w26_4_d1 <=  heap_bh96_w26_4;
            heap_bh96_w26_4_d2 <=  heap_bh96_w26_4_d1;
            heap_bh96_w26_4_d3 <=  heap_bh96_w26_4_d2;
            heap_bh96_w27_5_d1 <=  heap_bh96_w27_5;
            heap_bh96_w27_5_d2 <=  heap_bh96_w27_5_d1;
            heap_bh96_w27_5_d3 <=  heap_bh96_w27_5_d2;
            heap_bh96_w43_3_d1 <=  heap_bh96_w43_3;
            heap_bh96_w43_3_d2 <=  heap_bh96_w43_3_d1;
            heap_bh96_w43_3_d3 <=  heap_bh96_w43_3_d2;
            heap_bh96_w19_6_d1 <=  heap_bh96_w19_6;
            heap_bh96_w19_6_d2 <=  heap_bh96_w19_6_d1;
            heap_bh96_w19_6_d3 <=  heap_bh96_w19_6_d2;
            heap_bh96_w20_4_d1 <=  heap_bh96_w20_4;
            heap_bh96_w20_4_d2 <=  heap_bh96_w20_4_d1;
            heap_bh96_w20_4_d3 <=  heap_bh96_w20_4_d2;
            heap_bh96_w28_5_d1 <=  heap_bh96_w28_5;
            heap_bh96_w28_5_d2 <=  heap_bh96_w28_5_d1;
            heap_bh96_w28_5_d3 <=  heap_bh96_w28_5_d2;
            heap_bh96_w29_5_d1 <=  heap_bh96_w29_5;
            heap_bh96_w29_5_d2 <=  heap_bh96_w29_5_d1;
            heap_bh96_w29_5_d3 <=  heap_bh96_w29_5_d2;
            heap_bh96_w21_6_d1 <=  heap_bh96_w21_6;
            heap_bh96_w21_6_d2 <=  heap_bh96_w21_6_d1;
            heap_bh96_w21_6_d3 <=  heap_bh96_w21_6_d2;
            heap_bh96_w22_4_d1 <=  heap_bh96_w22_4;
            heap_bh96_w22_4_d2 <=  heap_bh96_w22_4_d1;
            heap_bh96_w22_4_d3 <=  heap_bh96_w22_4_d2;
            heap_bh96_w23_5_d1 <=  heap_bh96_w23_5;
            heap_bh96_w30_5_d1 <=  heap_bh96_w30_5;
            heap_bh96_w30_5_d2 <=  heap_bh96_w30_5_d1;
            heap_bh96_w30_5_d3 <=  heap_bh96_w30_5_d2;
            heap_bh96_w31_5_d1 <=  heap_bh96_w31_5;
            heap_bh96_w31_5_d2 <=  heap_bh96_w31_5_d1;
            heap_bh96_w31_5_d3 <=  heap_bh96_w31_5_d2;
            heap_bh96_w32_4_d1 <=  heap_bh96_w32_4;
            heap_bh96_w23_6_d1 <=  heap_bh96_w23_6;
            heap_bh96_w23_6_d2 <=  heap_bh96_w23_6_d1;
            heap_bh96_w24_4_d1 <=  heap_bh96_w24_4;
            heap_bh96_w24_4_d2 <=  heap_bh96_w24_4_d1;
            heap_bh96_w25_5_d1 <=  heap_bh96_w25_5;
            heap_bh96_w25_5_d2 <=  heap_bh96_w25_5_d1;
            heap_bh96_w32_5_d1 <=  heap_bh96_w32_5;
            heap_bh96_w32_5_d2 <=  heap_bh96_w32_5_d1;
            heap_bh96_w33_5_d1 <=  heap_bh96_w33_5;
            heap_bh96_w33_5_d2 <=  heap_bh96_w33_5_d1;
            heap_bh96_w34_5_d1 <=  heap_bh96_w34_5;
            heap_bh96_w34_5_d2 <=  heap_bh96_w34_5_d1;
            heap_bh96_w35_5_d1 <=  heap_bh96_w35_5;
            heap_bh96_w35_5_d2 <=  heap_bh96_w35_5_d1;
            heap_bh96_w36_5_d1 <=  heap_bh96_w36_5;
            heap_bh96_w36_5_d2 <=  heap_bh96_w36_5_d1;
            heap_bh96_w37_5_d1 <=  heap_bh96_w37_5;
            heap_bh96_w37_5_d2 <=  heap_bh96_w37_5_d1;
            heap_bh96_w38_5_d1 <=  heap_bh96_w38_5;
            heap_bh96_w38_5_d2 <=  heap_bh96_w38_5_d1;
            heap_bh96_w39_5_d1 <=  heap_bh96_w39_5;
            heap_bh96_w39_5_d2 <=  heap_bh96_w39_5_d1;
            heap_bh96_w40_4_d1 <=  heap_bh96_w40_4;
            heap_bh96_w40_5_d1 <=  heap_bh96_w40_5;
            heap_bh96_w41_5_d1 <=  heap_bh96_w41_5;
            heap_bh96_w42_3_d1 <=  heap_bh96_w42_3;
         end if;
      end process;
   XX_m98 <= Y ;
   YY_m98 <= X ;
   heap_bh96_w7_0 <= A(0); -- cycle= 0 cp= 0
   heap_bh96_w8_0 <= A(1); -- cycle= 0 cp= 0
   heap_bh96_w9_0 <= A(2); -- cycle= 0 cp= 0
   heap_bh96_w10_0 <= A(3); -- cycle= 0 cp= 0
   heap_bh96_w11_0 <= A(4); -- cycle= 0 cp= 0
   heap_bh96_w12_0 <= A(5); -- cycle= 0 cp= 0
   heap_bh96_w13_0 <= A(6); -- cycle= 0 cp= 0
   heap_bh96_w14_0 <= A(7); -- cycle= 0 cp= 0
   heap_bh96_w15_0 <= A(8); -- cycle= 0 cp= 0
   heap_bh96_w16_0 <= A(9); -- cycle= 0 cp= 0
   heap_bh96_w17_0 <= A(10); -- cycle= 0 cp= 0
   heap_bh96_w18_0 <= A(11); -- cycle= 0 cp= 0
   heap_bh96_w19_0 <= A(12); -- cycle= 0 cp= 0
   heap_bh96_w20_0 <= A(13); -- cycle= 0 cp= 0
   heap_bh96_w21_0 <= A(14); -- cycle= 0 cp= 0
   heap_bh96_w22_0 <= A(15); -- cycle= 0 cp= 0
   heap_bh96_w23_0 <= A(16); -- cycle= 0 cp= 0
   heap_bh96_w24_0 <= A(17); -- cycle= 0 cp= 0
   heap_bh96_w25_0 <= A(18); -- cycle= 0 cp= 0
   heap_bh96_w26_0 <= A(19); -- cycle= 0 cp= 0
   heap_bh96_w27_0 <= A(20); -- cycle= 0 cp= 0
   heap_bh96_w28_0 <= A(21); -- cycle= 0 cp= 0
   heap_bh96_w29_0 <= A(22); -- cycle= 0 cp= 0
   heap_bh96_w30_0 <= A(23); -- cycle= 0 cp= 0
   heap_bh96_w31_0 <= A(24); -- cycle= 0 cp= 0
   heap_bh96_w32_0 <= A(25); -- cycle= 0 cp= 0
   heap_bh96_w33_0 <= A(26); -- cycle= 0 cp= 0
   heap_bh96_w34_0 <= A(27); -- cycle= 0 cp= 0
   heap_bh96_w35_0 <= A(28); -- cycle= 0 cp= 0
   heap_bh96_w36_0 <= A(29); -- cycle= 0 cp= 0
   heap_bh96_w37_0 <= A(30); -- cycle= 0 cp= 0
   heap_bh96_w38_0 <= A(31); -- cycle= 0 cp= 0
   heap_bh96_w39_0 <= A(32); -- cycle= 0 cp= 0
   heap_bh96_w40_0 <= A(33); -- cycle= 0 cp= 0
   heap_bh96_w41_0 <= A(34); -- cycle= 0 cp= 0
   heap_bh96_w42_0 <= A(35); -- cycle= 0 cp= 0
   heap_bh96_w43_0 <= A(36); -- cycle= 0 cp= 0
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh96_ch1_0 <= std_logic_vector(signed("0" & XX_m98(9 downto 0) & "00000000000000") * signed("" & YY_m98(33 downto 16) & ""));
   heap_bh96_w17_1 <= not( DSP_bh96_ch1_0(42) ); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w16_1 <= DSP_bh96_ch1_0(41); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w15_1 <= DSP_bh96_ch1_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w14_1 <= DSP_bh96_ch1_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w13_1 <= DSP_bh96_ch1_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w12_1 <= DSP_bh96_ch1_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w11_1 <= DSP_bh96_ch1_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w10_1 <= DSP_bh96_ch1_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w9_1 <= DSP_bh96_ch1_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w8_1 <= DSP_bh96_ch1_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w7_1 <= DSP_bh96_ch1_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w6_0 <= DSP_bh96_ch1_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w5_0 <= DSP_bh96_ch1_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w4_0 <= DSP_bh96_ch1_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w3_0 <= DSP_bh96_ch1_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w2_0 <= DSP_bh96_ch1_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w1_0 <= DSP_bh96_ch1_0(26); -- cycle= 0 cp= 2.387e-09
   heap_bh96_w0_0 <= DSP_bh96_ch1_0(25); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh96_ch2_0 <= std_logic_vector(signed("" & XX_m98(34 downto 10) & "") * signed("0" & YY_m98(15 downto 0) & "0"));
   DSP_bh96_root2_1 <= std_logic_vector(signed("" & XX_m98(34 downto 10) & "") * signed("" & YY_m98(33 downto 16) & ""));
   ----------------Synchro barrier, entering cycle 1----------------
   DSP_bh96_ch2_1<= std_logic_vector(signed(DSP_bh96_root2_1_d1) +  signed( DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) & DSP_bh96_ch2_0_d1(42) &   DSP_bh96_ch2_0_d1(42 downto 17) ));
   heap_bh96_w42_1 <= not( DSP_bh96_ch2_1(43) ); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w41_1 <= DSP_bh96_ch2_1(42); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w40_1 <= DSP_bh96_ch2_1(41); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w39_1 <= DSP_bh96_ch2_1(40); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w38_1 <= DSP_bh96_ch2_1(39); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w37_1 <= DSP_bh96_ch2_1(38); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w36_1 <= DSP_bh96_ch2_1(37); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w35_1 <= DSP_bh96_ch2_1(36); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w34_1 <= DSP_bh96_ch2_1(35); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w33_1 <= DSP_bh96_ch2_1(34); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w32_1 <= DSP_bh96_ch2_1(33); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w31_1 <= DSP_bh96_ch2_1(32); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w30_1 <= DSP_bh96_ch2_1(31); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w29_1 <= DSP_bh96_ch2_1(30); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w28_1 <= DSP_bh96_ch2_1(29); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w27_1 <= DSP_bh96_ch2_1(28); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w26_1 <= DSP_bh96_ch2_1(27); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w25_1 <= DSP_bh96_ch2_1(26); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w24_1 <= DSP_bh96_ch2_1(25); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w23_1 <= DSP_bh96_ch2_1(24); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w22_1 <= DSP_bh96_ch2_1(23); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w21_1 <= DSP_bh96_ch2_1(22); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w20_1 <= DSP_bh96_ch2_1(21); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w19_1 <= DSP_bh96_ch2_1(20); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w18_1 <= DSP_bh96_ch2_1(19); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w17_2 <= DSP_bh96_ch2_1(18); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w16_2 <= DSP_bh96_ch2_1(17); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w15_2 <= DSP_bh96_ch2_1(16); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w14_2 <= DSP_bh96_ch2_1(15); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w13_2 <= DSP_bh96_ch2_1(14); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w12_2 <= DSP_bh96_ch2_1(13); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w11_2 <= DSP_bh96_ch2_1(12); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w10_2 <= DSP_bh96_ch2_1(11); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w9_2 <= DSP_bh96_ch2_1(10); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w8_2 <= DSP_bh96_ch2_1(9); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w7_2 <= DSP_bh96_ch2_1(8); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w6_1 <= DSP_bh96_ch2_1(7); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w5_1 <= DSP_bh96_ch2_1(6); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w4_1 <= DSP_bh96_ch2_1(5); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w3_1 <= DSP_bh96_ch2_1(4); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w2_1 <= DSP_bh96_ch2_1(3); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w1_1 <= DSP_bh96_ch2_1(2); -- cycle= 1 cp= 2.274e-09
   heap_bh96_w0_1 <= DSP_bh96_ch2_1(1); -- cycle= 1 cp= 2.274e-09
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
   heap_bh96_w5_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w17_3 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w18_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w19_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w20_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w21_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w22_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w23_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w24_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w25_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w26_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w27_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w28_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w29_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w30_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w31_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w32_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w33_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w34_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w35_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w36_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w37_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w38_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w39_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w40_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w41_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh96_w43_1 <= '1'; -- cycle= 0 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh96_0_0 <= heap_bh96_w17_3_d1 & heap_bh96_w17_0_d1 & heap_bh96_w17_1_d1;
   CompressorIn_bh96_0_1 <= heap_bh96_w18_2_d1 & heap_bh96_w18_0_d1;
   Compressor_bh96_0: Compressor_23_3
      port map ( R => CompressorOut_bh96_0_0   ,
                 X0 => CompressorIn_bh96_0_0,
                 X1 => CompressorIn_bh96_0_1);
   heap_bh96_w17_4 <= CompressorOut_bh96_0_0(0); -- cycle= 1 cp= 0
   heap_bh96_w18_3 <= CompressorOut_bh96_0_0(1); -- cycle= 1 cp= 0
   heap_bh96_w19_3 <= CompressorOut_bh96_0_0(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh96_1_2 <= heap_bh96_w19_2_d1 & heap_bh96_w19_0_d1 & heap_bh96_w19_3;
   CompressorIn_bh96_1_3 <= heap_bh96_w20_2_d1 & heap_bh96_w20_0_d1;
   Compressor_bh96_1: Compressor_23_3
      port map ( R => CompressorOut_bh96_1_1   ,
                 X0 => CompressorIn_bh96_1_2,
                 X1 => CompressorIn_bh96_1_3);
   heap_bh96_w19_4 <= CompressorOut_bh96_1_1(0); -- cycle= 1 cp= 5.3072e-10
   heap_bh96_w20_3 <= CompressorOut_bh96_1_1(1); -- cycle= 1 cp= 5.3072e-10
   heap_bh96_w21_3 <= CompressorOut_bh96_1_1(2); -- cycle= 1 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh96_2_4 <= heap_bh96_w21_2_d1 & heap_bh96_w21_0_d1 & heap_bh96_w21_3;
   CompressorIn_bh96_2_5 <= heap_bh96_w22_2_d1 & heap_bh96_w22_0_d1;
   Compressor_bh96_2: Compressor_23_3
      port map ( R => CompressorOut_bh96_2_2   ,
                 X0 => CompressorIn_bh96_2_4,
                 X1 => CompressorIn_bh96_2_5);
   heap_bh96_w21_4 <= CompressorOut_bh96_2_2(0); -- cycle= 1 cp= 1.06144e-09
   heap_bh96_w22_3 <= CompressorOut_bh96_2_2(1); -- cycle= 1 cp= 1.06144e-09
   heap_bh96_w23_3 <= CompressorOut_bh96_2_2(2); -- cycle= 1 cp= 1.06144e-09

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh96_3_6 <= heap_bh96_w23_2_d1 & heap_bh96_w23_0_d1 & heap_bh96_w23_3;
   CompressorIn_bh96_3_7 <= heap_bh96_w24_2_d1 & heap_bh96_w24_0_d1;
   Compressor_bh96_3: Compressor_23_3
      port map ( R => CompressorOut_bh96_3_3   ,
                 X0 => CompressorIn_bh96_3_6,
                 X1 => CompressorIn_bh96_3_7);
   heap_bh96_w23_4 <= CompressorOut_bh96_3_3(0); -- cycle= 1 cp= 1.59216e-09
   heap_bh96_w24_3 <= CompressorOut_bh96_3_3(1); -- cycle= 1 cp= 1.59216e-09
   heap_bh96_w25_3 <= CompressorOut_bh96_3_3(2); -- cycle= 1 cp= 1.59216e-09

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_4_8 <= heap_bh96_w25_2_d2 & heap_bh96_w25_0_d2 & heap_bh96_w25_3_d1 & heap_bh96_w25_1_d1;
   CompressorIn_bh96_4_9(0) <= heap_bh96_w26_2_d2;
   Compressor_bh96_4: Compressor_14_3
      port map ( R => CompressorOut_bh96_4_4   ,
                 X0 => CompressorIn_bh96_4_8,
                 X1 => CompressorIn_bh96_4_9);
   heap_bh96_w25_4 <= CompressorOut_bh96_4_4(0); -- cycle= 2 cp= 0
   heap_bh96_w26_3 <= CompressorOut_bh96_4_4(1); -- cycle= 2 cp= 0
   heap_bh96_w27_3 <= CompressorOut_bh96_4_4(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_5_10 <= heap_bh96_w5_2_d2 & heap_bh96_w5_0_d2 & heap_bh96_w5_1_d1;
   CompressorIn_bh96_5_11 <= heap_bh96_w6_0_d2 & heap_bh96_w6_1_d1;
   Compressor_bh96_5: Compressor_23_3
      port map ( R => CompressorOut_bh96_5_5   ,
                 X0 => CompressorIn_bh96_5_10,
                 X1 => CompressorIn_bh96_5_11);
   heap_bh96_w5_3 <= CompressorOut_bh96_5_5(0); -- cycle= 2 cp= 0
   heap_bh96_w6_2 <= CompressorOut_bh96_5_5(1); -- cycle= 2 cp= 0
   heap_bh96_w7_3 <= CompressorOut_bh96_5_5(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_6_12 <= heap_bh96_w7_0_d2 & heap_bh96_w7_1_d2 & heap_bh96_w7_2_d1;
   CompressorIn_bh96_6_13 <= heap_bh96_w8_0_d2 & heap_bh96_w8_1_d2;
   Compressor_bh96_6: Compressor_23_3
      port map ( R => CompressorOut_bh96_6_6   ,
                 X0 => CompressorIn_bh96_6_12,
                 X1 => CompressorIn_bh96_6_13);
   heap_bh96_w7_4 <= CompressorOut_bh96_6_6(0); -- cycle= 2 cp= 0
   heap_bh96_w8_3 <= CompressorOut_bh96_6_6(1); -- cycle= 2 cp= 0
   heap_bh96_w9_3 <= CompressorOut_bh96_6_6(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_7_14 <= heap_bh96_w9_0_d2 & heap_bh96_w9_1_d2 & heap_bh96_w9_2_d1;
   CompressorIn_bh96_7_15 <= heap_bh96_w10_0_d2 & heap_bh96_w10_1_d2;
   Compressor_bh96_7: Compressor_23_3
      port map ( R => CompressorOut_bh96_7_7   ,
                 X0 => CompressorIn_bh96_7_14,
                 X1 => CompressorIn_bh96_7_15);
   heap_bh96_w9_4 <= CompressorOut_bh96_7_7(0); -- cycle= 2 cp= 0
   heap_bh96_w10_3 <= CompressorOut_bh96_7_7(1); -- cycle= 2 cp= 0
   heap_bh96_w11_3 <= CompressorOut_bh96_7_7(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_8_16 <= heap_bh96_w11_0_d2 & heap_bh96_w11_1_d2 & heap_bh96_w11_2_d1;
   CompressorIn_bh96_8_17 <= heap_bh96_w12_0_d2 & heap_bh96_w12_1_d2;
   Compressor_bh96_8: Compressor_23_3
      port map ( R => CompressorOut_bh96_8_8   ,
                 X0 => CompressorIn_bh96_8_16,
                 X1 => CompressorIn_bh96_8_17);
   heap_bh96_w11_4 <= CompressorOut_bh96_8_8(0); -- cycle= 2 cp= 0
   heap_bh96_w12_3 <= CompressorOut_bh96_8_8(1); -- cycle= 2 cp= 0
   heap_bh96_w13_3 <= CompressorOut_bh96_8_8(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_9_18 <= heap_bh96_w13_0_d2 & heap_bh96_w13_1_d2 & heap_bh96_w13_2_d1;
   CompressorIn_bh96_9_19 <= heap_bh96_w14_0_d2 & heap_bh96_w14_1_d2;
   Compressor_bh96_9: Compressor_23_3
      port map ( R => CompressorOut_bh96_9_9   ,
                 X0 => CompressorIn_bh96_9_18,
                 X1 => CompressorIn_bh96_9_19);
   heap_bh96_w13_4 <= CompressorOut_bh96_9_9(0); -- cycle= 2 cp= 0
   heap_bh96_w14_3 <= CompressorOut_bh96_9_9(1); -- cycle= 2 cp= 0
   heap_bh96_w15_3 <= CompressorOut_bh96_9_9(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_10_20 <= heap_bh96_w15_0_d2 & heap_bh96_w15_1_d2 & heap_bh96_w15_2_d1;
   CompressorIn_bh96_10_21 <= heap_bh96_w16_0_d2 & heap_bh96_w16_1_d2;
   Compressor_bh96_10: Compressor_23_3
      port map ( R => CompressorOut_bh96_10_10   ,
                 X0 => CompressorIn_bh96_10_20,
                 X1 => CompressorIn_bh96_10_21);
   heap_bh96_w15_4 <= CompressorOut_bh96_10_10(0); -- cycle= 2 cp= 0
   heap_bh96_w16_3 <= CompressorOut_bh96_10_10(1); -- cycle= 2 cp= 0
   heap_bh96_w17_5 <= CompressorOut_bh96_10_10(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_11_22 <= heap_bh96_w27_2_d2 & heap_bh96_w27_0_d2 & heap_bh96_w27_1_d1;
   CompressorIn_bh96_11_23 <= heap_bh96_w28_2_d2 & heap_bh96_w28_0_d2;
   Compressor_bh96_11: Compressor_23_3
      port map ( R => CompressorOut_bh96_11_11   ,
                 X0 => CompressorIn_bh96_11_22,
                 X1 => CompressorIn_bh96_11_23);
   heap_bh96_w27_4 <= CompressorOut_bh96_11_11(0); -- cycle= 2 cp= 0
   heap_bh96_w28_3 <= CompressorOut_bh96_11_11(1); -- cycle= 2 cp= 0
   heap_bh96_w29_3 <= CompressorOut_bh96_11_11(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_12_24 <= heap_bh96_w29_2_d2 & heap_bh96_w29_0_d2 & heap_bh96_w29_1_d1;
   CompressorIn_bh96_12_25 <= heap_bh96_w30_2_d2 & heap_bh96_w30_0_d2;
   Compressor_bh96_12: Compressor_23_3
      port map ( R => CompressorOut_bh96_12_12   ,
                 X0 => CompressorIn_bh96_12_24,
                 X1 => CompressorIn_bh96_12_25);
   heap_bh96_w29_4 <= CompressorOut_bh96_12_12(0); -- cycle= 2 cp= 0
   heap_bh96_w30_3 <= CompressorOut_bh96_12_12(1); -- cycle= 2 cp= 0
   heap_bh96_w31_3 <= CompressorOut_bh96_12_12(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_13_26 <= heap_bh96_w31_2_d2 & heap_bh96_w31_0_d2 & heap_bh96_w31_1_d1;
   CompressorIn_bh96_13_27 <= heap_bh96_w32_2_d2 & heap_bh96_w32_0_d2;
   Compressor_bh96_13: Compressor_23_3
      port map ( R => CompressorOut_bh96_13_13   ,
                 X0 => CompressorIn_bh96_13_26,
                 X1 => CompressorIn_bh96_13_27);
   heap_bh96_w31_4 <= CompressorOut_bh96_13_13(0); -- cycle= 2 cp= 0
   heap_bh96_w32_3 <= CompressorOut_bh96_13_13(1); -- cycle= 2 cp= 0
   heap_bh96_w33_3 <= CompressorOut_bh96_13_13(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_14_28 <= heap_bh96_w33_2_d2 & heap_bh96_w33_0_d2 & heap_bh96_w33_1_d1;
   CompressorIn_bh96_14_29 <= heap_bh96_w34_2_d2 & heap_bh96_w34_0_d2;
   Compressor_bh96_14: Compressor_23_3
      port map ( R => CompressorOut_bh96_14_14   ,
                 X0 => CompressorIn_bh96_14_28,
                 X1 => CompressorIn_bh96_14_29);
   heap_bh96_w33_4 <= CompressorOut_bh96_14_14(0); -- cycle= 2 cp= 0
   heap_bh96_w34_3 <= CompressorOut_bh96_14_14(1); -- cycle= 2 cp= 0
   heap_bh96_w35_3 <= CompressorOut_bh96_14_14(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_15_30 <= heap_bh96_w35_2_d2 & heap_bh96_w35_0_d2 & heap_bh96_w35_1_d1;
   CompressorIn_bh96_15_31 <= heap_bh96_w36_2_d2 & heap_bh96_w36_0_d2;
   Compressor_bh96_15: Compressor_23_3
      port map ( R => CompressorOut_bh96_15_15   ,
                 X0 => CompressorIn_bh96_15_30,
                 X1 => CompressorIn_bh96_15_31);
   heap_bh96_w35_4 <= CompressorOut_bh96_15_15(0); -- cycle= 2 cp= 0
   heap_bh96_w36_3 <= CompressorOut_bh96_15_15(1); -- cycle= 2 cp= 0
   heap_bh96_w37_3 <= CompressorOut_bh96_15_15(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_16_32 <= heap_bh96_w37_2_d2 & heap_bh96_w37_0_d2 & heap_bh96_w37_1_d1;
   CompressorIn_bh96_16_33 <= heap_bh96_w38_2_d2 & heap_bh96_w38_0_d2;
   Compressor_bh96_16: Compressor_23_3
      port map ( R => CompressorOut_bh96_16_16   ,
                 X0 => CompressorIn_bh96_16_32,
                 X1 => CompressorIn_bh96_16_33);
   heap_bh96_w37_4 <= CompressorOut_bh96_16_16(0); -- cycle= 2 cp= 0
   heap_bh96_w38_3 <= CompressorOut_bh96_16_16(1); -- cycle= 2 cp= 0
   heap_bh96_w39_3 <= CompressorOut_bh96_16_16(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_17_34 <= heap_bh96_w39_2_d2 & heap_bh96_w39_0_d2 & heap_bh96_w39_1_d1;
   CompressorIn_bh96_17_35 <= heap_bh96_w40_2_d2 & heap_bh96_w40_0_d2;
   Compressor_bh96_17: Compressor_23_3
      port map ( R => CompressorOut_bh96_17_17   ,
                 X0 => CompressorIn_bh96_17_34,
                 X1 => CompressorIn_bh96_17_35);
   heap_bh96_w39_4 <= CompressorOut_bh96_17_17(0); -- cycle= 2 cp= 0
   heap_bh96_w40_3 <= CompressorOut_bh96_17_17(1); -- cycle= 2 cp= 0
   heap_bh96_w41_3 <= CompressorOut_bh96_17_17(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_18_36 <= heap_bh96_w41_2_d2 & heap_bh96_w41_0_d2 & heap_bh96_w41_1_d1;
   CompressorIn_bh96_18_37 <= heap_bh96_w42_0_d2 & heap_bh96_w42_1_d1;
   Compressor_bh96_18: Compressor_23_3
      port map ( R => CompressorOut_bh96_18_18   ,
                 X0 => CompressorIn_bh96_18_36,
                 X1 => CompressorIn_bh96_18_37);
   heap_bh96_w41_4 <= CompressorOut_bh96_18_18(0); -- cycle= 2 cp= 0
   heap_bh96_w42_2 <= CompressorOut_bh96_18_18(1); -- cycle= 2 cp= 0
   heap_bh96_w43_2 <= CompressorOut_bh96_18_18(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_19_38 <= heap_bh96_w17_4_d1 & heap_bh96_w17_2_d1 & heap_bh96_w17_5;
   CompressorIn_bh96_19_39 <= heap_bh96_w18_3_d1 & heap_bh96_w18_1_d1;
   Compressor_bh96_19: Compressor_23_3
      port map ( R => CompressorOut_bh96_19_19   ,
                 X0 => CompressorIn_bh96_19_38,
                 X1 => CompressorIn_bh96_19_39);
   heap_bh96_w17_6 <= CompressorOut_bh96_19_19(0); -- cycle= 2 cp= 5.3072e-10
   heap_bh96_w18_4 <= CompressorOut_bh96_19_19(1); -- cycle= 2 cp= 5.3072e-10
   heap_bh96_w19_5 <= CompressorOut_bh96_19_19(2); -- cycle= 2 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_20_40 <= heap_bh96_w26_0_d2 & heap_bh96_w26_1_d1 & heap_bh96_w26_3;
   CompressorIn_bh96_20_41 <= heap_bh96_w27_4 & heap_bh96_w27_3;
   Compressor_bh96_20: Compressor_23_3
      port map ( R => CompressorOut_bh96_20_20   ,
                 X0 => CompressorIn_bh96_20_40,
                 X1 => CompressorIn_bh96_20_41);
   heap_bh96_w26_4 <= CompressorOut_bh96_20_20(0); -- cycle= 2 cp= 5.3072e-10
   heap_bh96_w27_5 <= CompressorOut_bh96_20_20(1); -- cycle= 2 cp= 5.3072e-10
   heap_bh96_w28_4 <= CompressorOut_bh96_20_20(2); -- cycle= 2 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_21_42 <= heap_bh96_w43_1_d2 & heap_bh96_w43_0_d2 & heap_bh96_w43_2;
   Compressor_bh96_21: Compressor_3_2
      port map ( R => CompressorOut_bh96_21_21   ,
                 X0 => CompressorIn_bh96_21_42);
   heap_bh96_w43_3 <= CompressorOut_bh96_21_21(0); -- cycle= 2 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_22_43 <= heap_bh96_w19_4_d1 & heap_bh96_w19_1_d1 & heap_bh96_w19_5;
   CompressorIn_bh96_22_44 <= heap_bh96_w20_3_d1 & heap_bh96_w20_1_d1;
   Compressor_bh96_22: Compressor_23_3
      port map ( R => CompressorOut_bh96_22_22   ,
                 X0 => CompressorIn_bh96_22_43,
                 X1 => CompressorIn_bh96_22_44);
   heap_bh96_w19_6 <= CompressorOut_bh96_22_22(0); -- cycle= 2 cp= 1.06144e-09
   heap_bh96_w20_4 <= CompressorOut_bh96_22_22(1); -- cycle= 2 cp= 1.06144e-09
   heap_bh96_w21_5 <= CompressorOut_bh96_22_22(2); -- cycle= 2 cp= 1.06144e-09

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_23_45 <= heap_bh96_w28_1_d1 & heap_bh96_w28_3 & heap_bh96_w28_4;
   CompressorIn_bh96_23_46 <= heap_bh96_w29_4 & heap_bh96_w29_3;
   Compressor_bh96_23: Compressor_23_3
      port map ( R => CompressorOut_bh96_23_23   ,
                 X0 => CompressorIn_bh96_23_45,
                 X1 => CompressorIn_bh96_23_46);
   heap_bh96_w28_5 <= CompressorOut_bh96_23_23(0); -- cycle= 2 cp= 1.06144e-09
   heap_bh96_w29_5 <= CompressorOut_bh96_23_23(1); -- cycle= 2 cp= 1.06144e-09
   heap_bh96_w30_4 <= CompressorOut_bh96_23_23(2); -- cycle= 2 cp= 1.06144e-09

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_24_47 <= heap_bh96_w21_4_d1 & heap_bh96_w21_1_d1 & heap_bh96_w21_5;
   CompressorIn_bh96_24_48 <= heap_bh96_w22_3_d1 & heap_bh96_w22_1_d1;
   Compressor_bh96_24: Compressor_23_3
      port map ( R => CompressorOut_bh96_24_24   ,
                 X0 => CompressorIn_bh96_24_47,
                 X1 => CompressorIn_bh96_24_48);
   heap_bh96_w21_6 <= CompressorOut_bh96_24_24(0); -- cycle= 2 cp= 1.59216e-09
   heap_bh96_w22_4 <= CompressorOut_bh96_24_24(1); -- cycle= 2 cp= 1.59216e-09
   heap_bh96_w23_5 <= CompressorOut_bh96_24_24(2); -- cycle= 2 cp= 1.59216e-09

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh96_25_49 <= heap_bh96_w30_1_d1 & heap_bh96_w30_3 & heap_bh96_w30_4;
   CompressorIn_bh96_25_50 <= heap_bh96_w31_4 & heap_bh96_w31_3;
   Compressor_bh96_25: Compressor_23_3
      port map ( R => CompressorOut_bh96_25_25   ,
                 X0 => CompressorIn_bh96_25_49,
                 X1 => CompressorIn_bh96_25_50);
   heap_bh96_w30_5 <= CompressorOut_bh96_25_25(0); -- cycle= 2 cp= 1.59216e-09
   heap_bh96_w31_5 <= CompressorOut_bh96_25_25(1); -- cycle= 2 cp= 1.59216e-09
   heap_bh96_w32_4 <= CompressorOut_bh96_25_25(2); -- cycle= 2 cp= 1.59216e-09

   ----------------Synchro barrier, entering cycle 2----------------
   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh96_26_51 <= heap_bh96_w23_4_d2 & heap_bh96_w23_1_d2 & heap_bh96_w23_5_d1;
   CompressorIn_bh96_26_52 <= heap_bh96_w24_3_d2 & heap_bh96_w24_1_d2;
   Compressor_bh96_26: Compressor_23_3
      port map ( R => CompressorOut_bh96_26_26   ,
                 X0 => CompressorIn_bh96_26_51,
                 X1 => CompressorIn_bh96_26_52);
   heap_bh96_w23_6 <= CompressorOut_bh96_26_26(0); -- cycle= 3 cp= 0
   heap_bh96_w24_4 <= CompressorOut_bh96_26_26(1); -- cycle= 3 cp= 0
   heap_bh96_w25_5 <= CompressorOut_bh96_26_26(2); -- cycle= 3 cp= 0

   ----------------Synchro barrier, entering cycle 2----------------
   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh96_27_53 <= heap_bh96_w32_1_d2 & heap_bh96_w32_3_d1 & heap_bh96_w32_4_d1;
   CompressorIn_bh96_27_54 <= heap_bh96_w33_4_d1 & heap_bh96_w33_3_d1;
   Compressor_bh96_27: Compressor_23_3
      port map ( R => CompressorOut_bh96_27_27   ,
                 X0 => CompressorIn_bh96_27_53,
                 X1 => CompressorIn_bh96_27_54);
   heap_bh96_w32_5 <= CompressorOut_bh96_27_27(0); -- cycle= 3 cp= 0
   heap_bh96_w33_5 <= CompressorOut_bh96_27_27(1); -- cycle= 3 cp= 0
   heap_bh96_w34_4 <= CompressorOut_bh96_27_27(2); -- cycle= 3 cp= 0

   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh96_28_55 <= heap_bh96_w34_1_d2 & heap_bh96_w34_3_d1 & heap_bh96_w34_4;
   CompressorIn_bh96_28_56 <= heap_bh96_w35_4_d1 & heap_bh96_w35_3_d1;
   Compressor_bh96_28: Compressor_23_3
      port map ( R => CompressorOut_bh96_28_28   ,
                 X0 => CompressorIn_bh96_28_55,
                 X1 => CompressorIn_bh96_28_56);
   heap_bh96_w34_5 <= CompressorOut_bh96_28_28(0); -- cycle= 3 cp= 5.3072e-10
   heap_bh96_w35_5 <= CompressorOut_bh96_28_28(1); -- cycle= 3 cp= 5.3072e-10
   heap_bh96_w36_4 <= CompressorOut_bh96_28_28(2); -- cycle= 3 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh96_29_57 <= heap_bh96_w36_1_d2 & heap_bh96_w36_3_d1 & heap_bh96_w36_4;
   CompressorIn_bh96_29_58 <= heap_bh96_w37_4_d1 & heap_bh96_w37_3_d1;
   Compressor_bh96_29: Compressor_23_3
      port map ( R => CompressorOut_bh96_29_29   ,
                 X0 => CompressorIn_bh96_29_57,
                 X1 => CompressorIn_bh96_29_58);
   heap_bh96_w36_5 <= CompressorOut_bh96_29_29(0); -- cycle= 3 cp= 1.06144e-09
   heap_bh96_w37_5 <= CompressorOut_bh96_29_29(1); -- cycle= 3 cp= 1.06144e-09
   heap_bh96_w38_4 <= CompressorOut_bh96_29_29(2); -- cycle= 3 cp= 1.06144e-09

   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh96_30_59 <= heap_bh96_w38_1_d2 & heap_bh96_w38_3_d1 & heap_bh96_w38_4;
   CompressorIn_bh96_30_60 <= heap_bh96_w39_4_d1 & heap_bh96_w39_3_d1;
   Compressor_bh96_30: Compressor_23_3
      port map ( R => CompressorOut_bh96_30_30   ,
                 X0 => CompressorIn_bh96_30_59,
                 X1 => CompressorIn_bh96_30_60);
   heap_bh96_w38_5 <= CompressorOut_bh96_30_30(0); -- cycle= 3 cp= 1.59216e-09
   heap_bh96_w39_5 <= CompressorOut_bh96_30_30(1); -- cycle= 3 cp= 1.59216e-09
   heap_bh96_w40_4 <= CompressorOut_bh96_30_30(2); -- cycle= 3 cp= 1.59216e-09

   ----------------Synchro barrier, entering cycle 3----------------
   ----------------Synchro barrier, entering cycle 4----------------
   CompressorIn_bh96_31_61 <= heap_bh96_w40_1_d3 & heap_bh96_w40_3_d2 & heap_bh96_w40_4_d1;
   CompressorIn_bh96_31_62 <= heap_bh96_w41_4_d2 & heap_bh96_w41_3_d2;
   Compressor_bh96_31: Compressor_23_3
      port map ( R => CompressorOut_bh96_31_31   ,
                 X0 => CompressorIn_bh96_31_61,
                 X1 => CompressorIn_bh96_31_62);
   heap_bh96_w40_5 <= CompressorOut_bh96_31_31(0); -- cycle= 4 cp= 0
   heap_bh96_w41_5 <= CompressorOut_bh96_31_31(1); -- cycle= 4 cp= 0
   heap_bh96_w42_3 <= CompressorOut_bh96_31_31(2); -- cycle= 4 cp= 0
   ----------------Synchro barrier, entering cycle 4----------------
   ----------------Synchro barrier, entering cycle 5----------------
   finalAdderIn0_bh96 <= "0" & heap_bh96_w43_3_d3 & heap_bh96_w42_2_d3 & heap_bh96_w41_5_d1 & heap_bh96_w40_5_d1 & heap_bh96_w39_5_d2 & heap_bh96_w38_5_d2 & heap_bh96_w37_5_d2 & heap_bh96_w36_5_d2 & heap_bh96_w35_5_d2 & heap_bh96_w34_5_d2 & heap_bh96_w33_5_d2 & heap_bh96_w32_5_d2 & heap_bh96_w31_5_d3 & heap_bh96_w30_5_d3 & heap_bh96_w29_5_d3 & heap_bh96_w28_5_d3 & heap_bh96_w27_5_d3 & heap_bh96_w26_4_d3 & heap_bh96_w25_4_d3 & heap_bh96_w24_4_d2 & heap_bh96_w23_6_d2 & heap_bh96_w22_4_d3 & heap_bh96_w21_6_d3 & heap_bh96_w20_4_d3 & heap_bh96_w19_6_d3 & heap_bh96_w18_4_d3 & heap_bh96_w17_6_d3 & heap_bh96_w16_2_d4 & heap_bh96_w15_4_d3 & heap_bh96_w14_2_d4 & heap_bh96_w13_4_d3 & heap_bh96_w12_2_d4 & heap_bh96_w11_4_d3 & heap_bh96_w10_2_d4 & heap_bh96_w9_4_d3 & heap_bh96_w8_2_d4 & heap_bh96_w7_4_d3 & heap_bh96_w6_2_d3 & heap_bh96_w5_3_d3 & heap_bh96_w4_0_d5 & heap_bh96_w3_0_d5 & heap_bh96_w2_0_d5 & heap_bh96_w1_0_d5 & heap_bh96_w0_0_d5;
   finalAdderIn1_bh96 <= "0" & '0' & heap_bh96_w42_3_d1 & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh96_w25_5_d2 & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh96_w16_3_d3 & heap_bh96_w15_3_d3 & heap_bh96_w14_3_d3 & heap_bh96_w13_3_d3 & heap_bh96_w12_3_d3 & heap_bh96_w11_3_d3 & heap_bh96_w10_3_d3 & heap_bh96_w9_3_d3 & heap_bh96_w8_3_d3 & heap_bh96_w7_3_d3 & '0' & '0' & heap_bh96_w4_1_d4 & heap_bh96_w3_1_d4 & heap_bh96_w2_1_d4 & heap_bh96_w1_1_d4 & heap_bh96_w0_1_d4;
   finalAdderCin_bh96 <= '0';
   Adder_final96_0: IntAdder_45_f400_uid140  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh96,
                 R => finalAdderOut_bh96   ,
                 X => finalAdderIn0_bh96,
                 Y => finalAdderIn1_bh96);
   ----------------Synchro barrier, entering cycle 6----------------
   -- concatenate all the compressed chunks
   CompressionResult96 <= finalAdderOut_bh96;
   -- End of code generated by BitHeap::generateCompressorVHDL
   R <= CompressionResult96(43 downto 6);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_48_f400_uid194
--                    (IntAdderAlternative_48_f400_uid198)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_48_f400_uid194 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(47 downto 0);
          Y : in  std_logic_vector(47 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(47 downto 0)   );
end entity;

architecture arch of IntAdder_48_f400_uid194 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(6 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(5 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(6 downto 0);
signal sum_l1_idx1 :  std_logic_vector(5 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(47 downto 42)) + ( "0" & Y(47 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(5 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(6 downto 6);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(5 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(6 downto 6);
   R <= sum_l1_idx1(5 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                      FixMultAdd_34x38p40r41signed148
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Matei Istoan, 2012-2014
--------------------------------------------------------------------------------
-- Pipeline depth: 7 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library std;
use std.textio.all;
library work;

entity FixMultAdd_34x38p40r41signed148 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(33 downto 0);
          Y : in  std_logic_vector(37 downto 0);
          A : in  std_logic_vector(39 downto 0);
          R : out  std_logic_vector(40 downto 0)   );
end entity;

architecture arch of FixMultAdd_34x38p40r41signed148 is
   component Compressor_13_3 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             X1 : in  std_logic_vector(0 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component Compressor_14_3 is
      port ( X0 : in  std_logic_vector(3 downto 0);
             X1 : in  std_logic_vector(0 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component Compressor_23_3 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             X1 : in  std_logic_vector(1 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component IntAdder_48_f400_uid194 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(47 downto 0);
             Y : in  std_logic_vector(47 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(47 downto 0)   );
   end component;

signal XX_m151 :  std_logic_vector(37 downto 0);
signal YY_m151 :  std_logic_vector(33 downto 0);
signal heap_bh149_w7_0, heap_bh149_w7_0_d1, heap_bh149_w7_0_d2 :  std_logic;
signal heap_bh149_w8_0, heap_bh149_w8_0_d1, heap_bh149_w8_0_d2 :  std_logic;
signal heap_bh149_w9_0, heap_bh149_w9_0_d1, heap_bh149_w9_0_d2 :  std_logic;
signal heap_bh149_w10_0, heap_bh149_w10_0_d1, heap_bh149_w10_0_d2 :  std_logic;
signal heap_bh149_w11_0, heap_bh149_w11_0_d1, heap_bh149_w11_0_d2 :  std_logic;
signal heap_bh149_w12_0, heap_bh149_w12_0_d1, heap_bh149_w12_0_d2 :  std_logic;
signal heap_bh149_w13_0, heap_bh149_w13_0_d1, heap_bh149_w13_0_d2 :  std_logic;
signal heap_bh149_w14_0, heap_bh149_w14_0_d1, heap_bh149_w14_0_d2 :  std_logic;
signal heap_bh149_w15_0, heap_bh149_w15_0_d1, heap_bh149_w15_0_d2 :  std_logic;
signal heap_bh149_w16_0, heap_bh149_w16_0_d1, heap_bh149_w16_0_d2 :  std_logic;
signal heap_bh149_w17_0, heap_bh149_w17_0_d1, heap_bh149_w17_0_d2 :  std_logic;
signal heap_bh149_w18_0, heap_bh149_w18_0_d1, heap_bh149_w18_0_d2 :  std_logic;
signal heap_bh149_w19_0, heap_bh149_w19_0_d1, heap_bh149_w19_0_d2 :  std_logic;
signal heap_bh149_w20_0, heap_bh149_w20_0_d1, heap_bh149_w20_0_d2 :  std_logic;
signal heap_bh149_w21_0, heap_bh149_w21_0_d1, heap_bh149_w21_0_d2 :  std_logic;
signal heap_bh149_w22_0, heap_bh149_w22_0_d1, heap_bh149_w22_0_d2 :  std_logic;
signal heap_bh149_w23_0, heap_bh149_w23_0_d1, heap_bh149_w23_0_d2 :  std_logic;
signal heap_bh149_w24_0, heap_bh149_w24_0_d1, heap_bh149_w24_0_d2 :  std_logic;
signal heap_bh149_w25_0, heap_bh149_w25_0_d1, heap_bh149_w25_0_d2 :  std_logic;
signal heap_bh149_w26_0, heap_bh149_w26_0_d1, heap_bh149_w26_0_d2 :  std_logic;
signal heap_bh149_w27_0, heap_bh149_w27_0_d1, heap_bh149_w27_0_d2 :  std_logic;
signal heap_bh149_w28_0, heap_bh149_w28_0_d1, heap_bh149_w28_0_d2 :  std_logic;
signal heap_bh149_w29_0, heap_bh149_w29_0_d1, heap_bh149_w29_0_d2 :  std_logic;
signal heap_bh149_w30_0, heap_bh149_w30_0_d1, heap_bh149_w30_0_d2 :  std_logic;
signal heap_bh149_w31_0, heap_bh149_w31_0_d1, heap_bh149_w31_0_d2 :  std_logic;
signal heap_bh149_w32_0, heap_bh149_w32_0_d1, heap_bh149_w32_0_d2 :  std_logic;
signal heap_bh149_w33_0, heap_bh149_w33_0_d1, heap_bh149_w33_0_d2 :  std_logic;
signal heap_bh149_w34_0, heap_bh149_w34_0_d1, heap_bh149_w34_0_d2 :  std_logic;
signal heap_bh149_w35_0, heap_bh149_w35_0_d1, heap_bh149_w35_0_d2 :  std_logic;
signal heap_bh149_w36_0, heap_bh149_w36_0_d1, heap_bh149_w36_0_d2 :  std_logic;
signal heap_bh149_w37_0, heap_bh149_w37_0_d1, heap_bh149_w37_0_d2 :  std_logic;
signal heap_bh149_w38_0, heap_bh149_w38_0_d1, heap_bh149_w38_0_d2 :  std_logic;
signal heap_bh149_w39_0, heap_bh149_w39_0_d1, heap_bh149_w39_0_d2 :  std_logic;
signal heap_bh149_w40_0, heap_bh149_w40_0_d1, heap_bh149_w40_0_d2 :  std_logic;
signal heap_bh149_w41_0, heap_bh149_w41_0_d1, heap_bh149_w41_0_d2 :  std_logic;
signal heap_bh149_w42_0, heap_bh149_w42_0_d1, heap_bh149_w42_0_d2 :  std_logic;
signal heap_bh149_w43_0, heap_bh149_w43_0_d1, heap_bh149_w43_0_d2 :  std_logic;
signal heap_bh149_w44_0, heap_bh149_w44_0_d1, heap_bh149_w44_0_d2 :  std_logic;
signal heap_bh149_w45_0, heap_bh149_w45_0_d1, heap_bh149_w45_0_d2 :  std_logic;
signal heap_bh149_w46_0, heap_bh149_w46_0_d1, heap_bh149_w46_0_d2 :  std_logic;
signal DSP_bh149_ch2_0, DSP_bh149_ch2_0_d1 :  std_logic_vector(42 downto 0);
signal DSP_bh149_root2_1, DSP_bh149_root2_1_d1 :  std_logic_vector(42 downto 0);
signal DSP_bh149_ch2_1 :  std_logic_vector(43 downto 0);
signal heap_bh149_w1_0, heap_bh149_w1_0_d1, heap_bh149_w1_0_d2, heap_bh149_w1_0_d3, heap_bh149_w1_0_d4, heap_bh149_w1_0_d5 :  std_logic;
signal heap_bh149_w0_0, heap_bh149_w0_0_d1, heap_bh149_w0_0_d2, heap_bh149_w0_0_d3, heap_bh149_w0_0_d4, heap_bh149_w0_0_d5 :  std_logic;
signal heap_bh149_w45_1, heap_bh149_w45_1_d1 :  std_logic;
signal heap_bh149_w44_1, heap_bh149_w44_1_d1, heap_bh149_w44_1_d2, heap_bh149_w44_1_d3, heap_bh149_w44_1_d4 :  std_logic;
signal heap_bh149_w43_1, heap_bh149_w43_1_d1 :  std_logic;
signal heap_bh149_w42_1, heap_bh149_w42_1_d1, heap_bh149_w42_1_d2, heap_bh149_w42_1_d3 :  std_logic;
signal heap_bh149_w41_1, heap_bh149_w41_1_d1 :  std_logic;
signal heap_bh149_w40_1, heap_bh149_w40_1_d1, heap_bh149_w40_1_d2, heap_bh149_w40_1_d3 :  std_logic;
signal heap_bh149_w39_1, heap_bh149_w39_1_d1 :  std_logic;
signal heap_bh149_w38_1, heap_bh149_w38_1_d1, heap_bh149_w38_1_d2, heap_bh149_w38_1_d3 :  std_logic;
signal heap_bh149_w37_1, heap_bh149_w37_1_d1 :  std_logic;
signal heap_bh149_w36_1, heap_bh149_w36_1_d1, heap_bh149_w36_1_d2, heap_bh149_w36_1_d3 :  std_logic;
signal heap_bh149_w35_1, heap_bh149_w35_1_d1 :  std_logic;
signal heap_bh149_w34_1, heap_bh149_w34_1_d1, heap_bh149_w34_1_d2 :  std_logic;
signal heap_bh149_w33_1, heap_bh149_w33_1_d1 :  std_logic;
signal heap_bh149_w32_1, heap_bh149_w32_1_d1, heap_bh149_w32_1_d2 :  std_logic;
signal heap_bh149_w31_1, heap_bh149_w31_1_d1 :  std_logic;
signal heap_bh149_w30_1, heap_bh149_w30_1_d1, heap_bh149_w30_1_d2 :  std_logic;
signal heap_bh149_w29_1, heap_bh149_w29_1_d1 :  std_logic;
signal heap_bh149_w28_1, heap_bh149_w28_1_d1, heap_bh149_w28_1_d2 :  std_logic;
signal heap_bh149_w27_1, heap_bh149_w27_1_d1 :  std_logic;
signal heap_bh149_w26_1, heap_bh149_w26_1_d1 :  std_logic;
signal heap_bh149_w25_1, heap_bh149_w25_1_d1 :  std_logic;
signal heap_bh149_w24_1, heap_bh149_w24_1_d1 :  std_logic;
signal heap_bh149_w23_1, heap_bh149_w23_1_d1 :  std_logic;
signal heap_bh149_w22_1, heap_bh149_w22_1_d1 :  std_logic;
signal heap_bh149_w21_1, heap_bh149_w21_1_d1 :  std_logic;
signal heap_bh149_w20_1, heap_bh149_w20_1_d1, heap_bh149_w20_1_d2, heap_bh149_w20_1_d3, heap_bh149_w20_1_d4, heap_bh149_w20_1_d5 :  std_logic;
signal heap_bh149_w19_1, heap_bh149_w19_1_d1 :  std_logic;
signal heap_bh149_w18_1, heap_bh149_w18_1_d1, heap_bh149_w18_1_d2, heap_bh149_w18_1_d3, heap_bh149_w18_1_d4, heap_bh149_w18_1_d5 :  std_logic;
signal heap_bh149_w17_1, heap_bh149_w17_1_d1 :  std_logic;
signal heap_bh149_w16_1, heap_bh149_w16_1_d1, heap_bh149_w16_1_d2, heap_bh149_w16_1_d3, heap_bh149_w16_1_d4, heap_bh149_w16_1_d5 :  std_logic;
signal heap_bh149_w15_1, heap_bh149_w15_1_d1 :  std_logic;
signal heap_bh149_w14_1, heap_bh149_w14_1_d1, heap_bh149_w14_1_d2, heap_bh149_w14_1_d3, heap_bh149_w14_1_d4, heap_bh149_w14_1_d5 :  std_logic;
signal heap_bh149_w13_1, heap_bh149_w13_1_d1 :  std_logic;
signal heap_bh149_w12_1, heap_bh149_w12_1_d1, heap_bh149_w12_1_d2, heap_bh149_w12_1_d3, heap_bh149_w12_1_d4, heap_bh149_w12_1_d5 :  std_logic;
signal heap_bh149_w11_1, heap_bh149_w11_1_d1 :  std_logic;
signal heap_bh149_w10_1, heap_bh149_w10_1_d1, heap_bh149_w10_1_d2, heap_bh149_w10_1_d3, heap_bh149_w10_1_d4, heap_bh149_w10_1_d5 :  std_logic;
signal heap_bh149_w9_1, heap_bh149_w9_1_d1 :  std_logic;
signal heap_bh149_w8_1, heap_bh149_w8_1_d1, heap_bh149_w8_1_d2, heap_bh149_w8_1_d3, heap_bh149_w8_1_d4, heap_bh149_w8_1_d5 :  std_logic;
signal heap_bh149_w7_1, heap_bh149_w7_1_d1 :  std_logic;
signal heap_bh149_w6_0, heap_bh149_w6_0_d1 :  std_logic;
signal heap_bh149_w5_0, heap_bh149_w5_0_d1 :  std_logic;
signal heap_bh149_w4_0, heap_bh149_w4_0_d1, heap_bh149_w4_0_d2, heap_bh149_w4_0_d3, heap_bh149_w4_0_d4, heap_bh149_w4_0_d5 :  std_logic;
signal heap_bh149_w3_0, heap_bh149_w3_0_d1, heap_bh149_w3_0_d2, heap_bh149_w3_0_d3, heap_bh149_w3_0_d4, heap_bh149_w3_0_d5 :  std_logic;
signal heap_bh149_w2_0, heap_bh149_w2_0_d1, heap_bh149_w2_0_d2, heap_bh149_w2_0_d3, heap_bh149_w2_0_d4, heap_bh149_w2_0_d5 :  std_logic;
signal DSP_bh149_ch3_0, DSP_bh149_ch3_0_d1 :  std_logic_vector(42 downto 0);
signal DSP_bh149_root3_1, DSP_bh149_root3_1_d1 :  std_logic_vector(42 downto 0);
signal DSP_bh149_ch3_1 :  std_logic_vector(43 downto 0);
signal heap_bh149_w21_2, heap_bh149_w21_2_d1 :  std_logic;
signal heap_bh149_w20_2, heap_bh149_w20_2_d1 :  std_logic;
signal heap_bh149_w19_2, heap_bh149_w19_2_d1 :  std_logic;
signal heap_bh149_w18_2, heap_bh149_w18_2_d1 :  std_logic;
signal heap_bh149_w17_2, heap_bh149_w17_2_d1 :  std_logic;
signal heap_bh149_w16_2, heap_bh149_w16_2_d1 :  std_logic;
signal heap_bh149_w15_2, heap_bh149_w15_2_d1 :  std_logic;
signal heap_bh149_w14_2, heap_bh149_w14_2_d1 :  std_logic;
signal heap_bh149_w13_2, heap_bh149_w13_2_d1 :  std_logic;
signal heap_bh149_w12_2, heap_bh149_w12_2_d1 :  std_logic;
signal heap_bh149_w11_2, heap_bh149_w11_2_d1 :  std_logic;
signal heap_bh149_w10_2, heap_bh149_w10_2_d1 :  std_logic;
signal heap_bh149_w9_2, heap_bh149_w9_2_d1 :  std_logic;
signal heap_bh149_w8_2, heap_bh149_w8_2_d1 :  std_logic;
signal heap_bh149_w7_2, heap_bh149_w7_2_d1 :  std_logic;
signal heap_bh149_w6_1, heap_bh149_w6_1_d1 :  std_logic;
signal heap_bh149_w5_1, heap_bh149_w5_1_d1 :  std_logic;
signal heap_bh149_w4_1, heap_bh149_w4_1_d1, heap_bh149_w4_1_d2, heap_bh149_w4_1_d3, heap_bh149_w4_1_d4, heap_bh149_w4_1_d5 :  std_logic;
signal heap_bh149_w3_1, heap_bh149_w3_1_d1, heap_bh149_w3_1_d2, heap_bh149_w3_1_d3, heap_bh149_w3_1_d4, heap_bh149_w3_1_d5 :  std_logic;
signal heap_bh149_w2_1, heap_bh149_w2_1_d1, heap_bh149_w2_1_d2, heap_bh149_w2_1_d3, heap_bh149_w2_1_d4, heap_bh149_w2_1_d5 :  std_logic;
signal heap_bh149_w1_1, heap_bh149_w1_1_d1, heap_bh149_w1_1_d2, heap_bh149_w1_1_d3, heap_bh149_w1_1_d4, heap_bh149_w1_1_d5 :  std_logic;
signal heap_bh149_w0_1, heap_bh149_w0_1_d1, heap_bh149_w0_1_d2, heap_bh149_w0_1_d3, heap_bh149_w0_1_d4, heap_bh149_w0_1_d5 :  std_logic;
signal heap_bh149_w5_2, heap_bh149_w5_2_d1, heap_bh149_w5_2_d2 :  std_logic;
signal heap_bh149_w21_3, heap_bh149_w21_3_d1, heap_bh149_w21_3_d2 :  std_logic;
signal heap_bh149_w22_2, heap_bh149_w22_2_d1, heap_bh149_w22_2_d2 :  std_logic;
signal heap_bh149_w23_2, heap_bh149_w23_2_d1, heap_bh149_w23_2_d2 :  std_logic;
signal heap_bh149_w24_2, heap_bh149_w24_2_d1, heap_bh149_w24_2_d2 :  std_logic;
signal heap_bh149_w25_2, heap_bh149_w25_2_d1, heap_bh149_w25_2_d2 :  std_logic;
signal heap_bh149_w26_2, heap_bh149_w26_2_d1, heap_bh149_w26_2_d2 :  std_logic;
signal heap_bh149_w27_2, heap_bh149_w27_2_d1, heap_bh149_w27_2_d2 :  std_logic;
signal heap_bh149_w28_2, heap_bh149_w28_2_d1, heap_bh149_w28_2_d2 :  std_logic;
signal heap_bh149_w29_2, heap_bh149_w29_2_d1, heap_bh149_w29_2_d2 :  std_logic;
signal heap_bh149_w30_2, heap_bh149_w30_2_d1, heap_bh149_w30_2_d2 :  std_logic;
signal heap_bh149_w31_2, heap_bh149_w31_2_d1, heap_bh149_w31_2_d2 :  std_logic;
signal heap_bh149_w32_2, heap_bh149_w32_2_d1, heap_bh149_w32_2_d2 :  std_logic;
signal heap_bh149_w33_2, heap_bh149_w33_2_d1, heap_bh149_w33_2_d2 :  std_logic;
signal heap_bh149_w34_2, heap_bh149_w34_2_d1, heap_bh149_w34_2_d2 :  std_logic;
signal heap_bh149_w35_2, heap_bh149_w35_2_d1, heap_bh149_w35_2_d2 :  std_logic;
signal heap_bh149_w36_2, heap_bh149_w36_2_d1, heap_bh149_w36_2_d2 :  std_logic;
signal heap_bh149_w37_2, heap_bh149_w37_2_d1, heap_bh149_w37_2_d2 :  std_logic;
signal heap_bh149_w38_2, heap_bh149_w38_2_d1, heap_bh149_w38_2_d2 :  std_logic;
signal heap_bh149_w39_2, heap_bh149_w39_2_d1, heap_bh149_w39_2_d2 :  std_logic;
signal heap_bh149_w40_2, heap_bh149_w40_2_d1, heap_bh149_w40_2_d2 :  std_logic;
signal heap_bh149_w41_2, heap_bh149_w41_2_d1, heap_bh149_w41_2_d2 :  std_logic;
signal heap_bh149_w42_2, heap_bh149_w42_2_d1, heap_bh149_w42_2_d2 :  std_logic;
signal heap_bh149_w43_2, heap_bh149_w43_2_d1, heap_bh149_w43_2_d2 :  std_logic;
signal heap_bh149_w44_2, heap_bh149_w44_2_d1, heap_bh149_w44_2_d2 :  std_logic;
signal heap_bh149_w46_1, heap_bh149_w46_1_d1, heap_bh149_w46_1_d2 :  std_logic;
signal CompressorIn_bh149_0_0 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh149_0_1 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh149_0_0 :  std_logic_vector(2 downto 0);
signal heap_bh149_w21_4, heap_bh149_w21_4_d1, heap_bh149_w21_4_d2, heap_bh149_w21_4_d3, heap_bh149_w21_4_d4 :  std_logic;
signal heap_bh149_w22_3 :  std_logic;
signal heap_bh149_w23_3 :  std_logic;
signal CompressorIn_bh149_1_2 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_1_3 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_1_1 :  std_logic_vector(2 downto 0);
signal heap_bh149_w5_3, heap_bh149_w5_3_d1, heap_bh149_w5_3_d2, heap_bh149_w5_3_d3, heap_bh149_w5_3_d4 :  std_logic;
signal heap_bh149_w6_2, heap_bh149_w6_2_d1, heap_bh149_w6_2_d2, heap_bh149_w6_2_d3, heap_bh149_w6_2_d4 :  std_logic;
signal heap_bh149_w7_3, heap_bh149_w7_3_d1, heap_bh149_w7_3_d2, heap_bh149_w7_3_d3, heap_bh149_w7_3_d4 :  std_logic;
signal CompressorIn_bh149_2_4 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_2_5 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_2_2 :  std_logic_vector(2 downto 0);
signal heap_bh149_w7_4, heap_bh149_w7_4_d1, heap_bh149_w7_4_d2, heap_bh149_w7_4_d3, heap_bh149_w7_4_d4 :  std_logic;
signal heap_bh149_w8_3, heap_bh149_w8_3_d1, heap_bh149_w8_3_d2, heap_bh149_w8_3_d3, heap_bh149_w8_3_d4 :  std_logic;
signal heap_bh149_w9_3, heap_bh149_w9_3_d1, heap_bh149_w9_3_d2, heap_bh149_w9_3_d3, heap_bh149_w9_3_d4 :  std_logic;
signal CompressorIn_bh149_3_6 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_3_7 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_3_3 :  std_logic_vector(2 downto 0);
signal heap_bh149_w9_4, heap_bh149_w9_4_d1, heap_bh149_w9_4_d2, heap_bh149_w9_4_d3, heap_bh149_w9_4_d4 :  std_logic;
signal heap_bh149_w10_3, heap_bh149_w10_3_d1, heap_bh149_w10_3_d2, heap_bh149_w10_3_d3, heap_bh149_w10_3_d4 :  std_logic;
signal heap_bh149_w11_3, heap_bh149_w11_3_d1, heap_bh149_w11_3_d2, heap_bh149_w11_3_d3, heap_bh149_w11_3_d4 :  std_logic;
signal CompressorIn_bh149_4_8 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_4_9 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_4_4 :  std_logic_vector(2 downto 0);
signal heap_bh149_w11_4, heap_bh149_w11_4_d1, heap_bh149_w11_4_d2, heap_bh149_w11_4_d3, heap_bh149_w11_4_d4 :  std_logic;
signal heap_bh149_w12_3, heap_bh149_w12_3_d1, heap_bh149_w12_3_d2, heap_bh149_w12_3_d3, heap_bh149_w12_3_d4 :  std_logic;
signal heap_bh149_w13_3, heap_bh149_w13_3_d1, heap_bh149_w13_3_d2, heap_bh149_w13_3_d3, heap_bh149_w13_3_d4 :  std_logic;
signal CompressorIn_bh149_5_10 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_5_11 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_5_5 :  std_logic_vector(2 downto 0);
signal heap_bh149_w13_4, heap_bh149_w13_4_d1, heap_bh149_w13_4_d2, heap_bh149_w13_4_d3, heap_bh149_w13_4_d4 :  std_logic;
signal heap_bh149_w14_3, heap_bh149_w14_3_d1, heap_bh149_w14_3_d2, heap_bh149_w14_3_d3, heap_bh149_w14_3_d4 :  std_logic;
signal heap_bh149_w15_3, heap_bh149_w15_3_d1, heap_bh149_w15_3_d2, heap_bh149_w15_3_d3, heap_bh149_w15_3_d4 :  std_logic;
signal CompressorIn_bh149_6_12 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_6_13 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_6_6 :  std_logic_vector(2 downto 0);
signal heap_bh149_w15_4, heap_bh149_w15_4_d1, heap_bh149_w15_4_d2, heap_bh149_w15_4_d3, heap_bh149_w15_4_d4 :  std_logic;
signal heap_bh149_w16_3, heap_bh149_w16_3_d1, heap_bh149_w16_3_d2, heap_bh149_w16_3_d3, heap_bh149_w16_3_d4 :  std_logic;
signal heap_bh149_w17_3, heap_bh149_w17_3_d1, heap_bh149_w17_3_d2, heap_bh149_w17_3_d3, heap_bh149_w17_3_d4 :  std_logic;
signal CompressorIn_bh149_7_14 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_7_15 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_7_7 :  std_logic_vector(2 downto 0);
signal heap_bh149_w17_4, heap_bh149_w17_4_d1, heap_bh149_w17_4_d2, heap_bh149_w17_4_d3, heap_bh149_w17_4_d4 :  std_logic;
signal heap_bh149_w18_3, heap_bh149_w18_3_d1, heap_bh149_w18_3_d2, heap_bh149_w18_3_d3, heap_bh149_w18_3_d4 :  std_logic;
signal heap_bh149_w19_3, heap_bh149_w19_3_d1, heap_bh149_w19_3_d2, heap_bh149_w19_3_d3, heap_bh149_w19_3_d4 :  std_logic;
signal CompressorIn_bh149_8_16 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_8_17 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_8_8 :  std_logic_vector(2 downto 0);
signal heap_bh149_w19_4, heap_bh149_w19_4_d1, heap_bh149_w19_4_d2, heap_bh149_w19_4_d3, heap_bh149_w19_4_d4 :  std_logic;
signal heap_bh149_w20_3, heap_bh149_w20_3_d1, heap_bh149_w20_3_d2, heap_bh149_w20_3_d3, heap_bh149_w20_3_d4 :  std_logic;
signal heap_bh149_w21_5, heap_bh149_w21_5_d1, heap_bh149_w21_5_d2, heap_bh149_w21_5_d3, heap_bh149_w21_5_d4 :  std_logic;
signal CompressorIn_bh149_9_18 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_9_19 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_9_9 :  std_logic_vector(2 downto 0);
signal heap_bh149_w23_4 :  std_logic;
signal heap_bh149_w24_3 :  std_logic;
signal heap_bh149_w25_3 :  std_logic;
signal CompressorIn_bh149_10_20 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_10_21 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_10_10 :  std_logic_vector(2 downto 0);
signal heap_bh149_w25_4 :  std_logic;
signal heap_bh149_w26_3 :  std_logic;
signal heap_bh149_w27_3 :  std_logic;
signal CompressorIn_bh149_11_22 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_11_23 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_11_11 :  std_logic_vector(2 downto 0);
signal heap_bh149_w27_4 :  std_logic;
signal heap_bh149_w28_3, heap_bh149_w28_3_d1 :  std_logic;
signal heap_bh149_w29_3, heap_bh149_w29_3_d1 :  std_logic;
signal CompressorIn_bh149_12_24 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_12_25 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_12_12 :  std_logic_vector(2 downto 0);
signal heap_bh149_w29_4, heap_bh149_w29_4_d1 :  std_logic;
signal heap_bh149_w30_3, heap_bh149_w30_3_d1 :  std_logic;
signal heap_bh149_w31_3, heap_bh149_w31_3_d1 :  std_logic;
signal CompressorIn_bh149_13_26 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_13_27 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_13_13 :  std_logic_vector(2 downto 0);
signal heap_bh149_w31_4, heap_bh149_w31_4_d1 :  std_logic;
signal heap_bh149_w32_3, heap_bh149_w32_3_d1 :  std_logic;
signal heap_bh149_w33_3, heap_bh149_w33_3_d1 :  std_logic;
signal CompressorIn_bh149_14_28 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_14_29 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_14_14 :  std_logic_vector(2 downto 0);
signal heap_bh149_w33_4, heap_bh149_w33_4_d1 :  std_logic;
signal heap_bh149_w34_3, heap_bh149_w34_3_d1 :  std_logic;
signal heap_bh149_w35_3, heap_bh149_w35_3_d1 :  std_logic;
signal CompressorIn_bh149_15_30 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_15_31 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_15_15 :  std_logic_vector(2 downto 0);
signal heap_bh149_w35_4, heap_bh149_w35_4_d1 :  std_logic;
signal heap_bh149_w36_3, heap_bh149_w36_3_d1, heap_bh149_w36_3_d2 :  std_logic;
signal heap_bh149_w37_3, heap_bh149_w37_3_d1, heap_bh149_w37_3_d2 :  std_logic;
signal CompressorIn_bh149_16_32 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_16_33 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_16_16 :  std_logic_vector(2 downto 0);
signal heap_bh149_w37_4, heap_bh149_w37_4_d1, heap_bh149_w37_4_d2 :  std_logic;
signal heap_bh149_w38_3, heap_bh149_w38_3_d1, heap_bh149_w38_3_d2 :  std_logic;
signal heap_bh149_w39_3, heap_bh149_w39_3_d1, heap_bh149_w39_3_d2 :  std_logic;
signal CompressorIn_bh149_17_34 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_17_35 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_17_17 :  std_logic_vector(2 downto 0);
signal heap_bh149_w39_4, heap_bh149_w39_4_d1, heap_bh149_w39_4_d2 :  std_logic;
signal heap_bh149_w40_3, heap_bh149_w40_3_d1, heap_bh149_w40_3_d2 :  std_logic;
signal heap_bh149_w41_3, heap_bh149_w41_3_d1, heap_bh149_w41_3_d2 :  std_logic;
signal CompressorIn_bh149_18_36 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_18_37 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_18_18 :  std_logic_vector(2 downto 0);
signal heap_bh149_w41_4, heap_bh149_w41_4_d1, heap_bh149_w41_4_d2 :  std_logic;
signal heap_bh149_w42_3, heap_bh149_w42_3_d1, heap_bh149_w42_3_d2 :  std_logic;
signal heap_bh149_w43_3, heap_bh149_w43_3_d1, heap_bh149_w43_3_d2 :  std_logic;
signal CompressorIn_bh149_19_38 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_19_39 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_19_19 :  std_logic_vector(2 downto 0);
signal heap_bh149_w43_4, heap_bh149_w43_4_d1, heap_bh149_w43_4_d2 :  std_logic;
signal heap_bh149_w44_3, heap_bh149_w44_3_d1, heap_bh149_w44_3_d2, heap_bh149_w44_3_d3 :  std_logic;
signal heap_bh149_w45_2 :  std_logic;
signal CompressorIn_bh149_20_40 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_20_41 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_20_20 :  std_logic_vector(2 downto 0);
signal heap_bh149_w22_4, heap_bh149_w22_4_d1, heap_bh149_w22_4_d2, heap_bh149_w22_4_d3, heap_bh149_w22_4_d4 :  std_logic;
signal heap_bh149_w23_5, heap_bh149_w23_5_d1, heap_bh149_w23_5_d2, heap_bh149_w23_5_d3, heap_bh149_w23_5_d4 :  std_logic;
signal heap_bh149_w24_4 :  std_logic;
signal CompressorIn_bh149_21_42 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_21_43 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_21_21 :  std_logic_vector(2 downto 0);
signal heap_bh149_w45_3, heap_bh149_w45_3_d1, heap_bh149_w45_3_d2, heap_bh149_w45_3_d3 :  std_logic;
signal heap_bh149_w46_2, heap_bh149_w46_2_d1, heap_bh149_w46_2_d2, heap_bh149_w46_2_d3, heap_bh149_w46_2_d4 :  std_logic;
signal CompressorIn_bh149_22_44 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_22_45 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_22_22 :  std_logic_vector(2 downto 0);
signal heap_bh149_w24_5, heap_bh149_w24_5_d1, heap_bh149_w24_5_d2, heap_bh149_w24_5_d3, heap_bh149_w24_5_d4 :  std_logic;
signal heap_bh149_w25_5, heap_bh149_w25_5_d1, heap_bh149_w25_5_d2, heap_bh149_w25_5_d3, heap_bh149_w25_5_d4 :  std_logic;
signal heap_bh149_w26_4 :  std_logic;
signal CompressorIn_bh149_23_46 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_23_47 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_23_23 :  std_logic_vector(2 downto 0);
signal heap_bh149_w26_5, heap_bh149_w26_5_d1, heap_bh149_w26_5_d2, heap_bh149_w26_5_d3, heap_bh149_w26_5_d4 :  std_logic;
signal heap_bh149_w27_5, heap_bh149_w27_5_d1, heap_bh149_w27_5_d2, heap_bh149_w27_5_d3, heap_bh149_w27_5_d4 :  std_logic;
signal heap_bh149_w28_4, heap_bh149_w28_4_d1 :  std_logic;
signal CompressorIn_bh149_24_48 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_24_49 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_24_24 :  std_logic_vector(2 downto 0);
signal heap_bh149_w28_5, heap_bh149_w28_5_d1, heap_bh149_w28_5_d2, heap_bh149_w28_5_d3 :  std_logic;
signal heap_bh149_w29_5, heap_bh149_w29_5_d1, heap_bh149_w29_5_d2, heap_bh149_w29_5_d3 :  std_logic;
signal heap_bh149_w30_4 :  std_logic;
signal CompressorIn_bh149_25_50 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_25_51 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_25_25 :  std_logic_vector(2 downto 0);
signal heap_bh149_w30_5, heap_bh149_w30_5_d1, heap_bh149_w30_5_d2, heap_bh149_w30_5_d3 :  std_logic;
signal heap_bh149_w31_5, heap_bh149_w31_5_d1, heap_bh149_w31_5_d2, heap_bh149_w31_5_d3 :  std_logic;
signal heap_bh149_w32_4 :  std_logic;
signal CompressorIn_bh149_26_52 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_26_53 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_26_26 :  std_logic_vector(2 downto 0);
signal heap_bh149_w32_5, heap_bh149_w32_5_d1, heap_bh149_w32_5_d2, heap_bh149_w32_5_d3 :  std_logic;
signal heap_bh149_w33_5, heap_bh149_w33_5_d1, heap_bh149_w33_5_d2, heap_bh149_w33_5_d3 :  std_logic;
signal heap_bh149_w34_4 :  std_logic;
signal CompressorIn_bh149_27_54 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_27_55 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_27_27 :  std_logic_vector(2 downto 0);
signal heap_bh149_w34_5, heap_bh149_w34_5_d1, heap_bh149_w34_5_d2, heap_bh149_w34_5_d3 :  std_logic;
signal heap_bh149_w35_5, heap_bh149_w35_5_d1, heap_bh149_w35_5_d2, heap_bh149_w35_5_d3 :  std_logic;
signal heap_bh149_w36_4, heap_bh149_w36_4_d1 :  std_logic;
signal CompressorIn_bh149_28_56 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_28_57 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_28_28 :  std_logic_vector(2 downto 0);
signal heap_bh149_w36_5, heap_bh149_w36_5_d1, heap_bh149_w36_5_d2 :  std_logic;
signal heap_bh149_w37_5, heap_bh149_w37_5_d1, heap_bh149_w37_5_d2 :  std_logic;
signal heap_bh149_w38_4 :  std_logic;
signal CompressorIn_bh149_29_58 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_29_59 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_29_29 :  std_logic_vector(2 downto 0);
signal heap_bh149_w38_5, heap_bh149_w38_5_d1, heap_bh149_w38_5_d2 :  std_logic;
signal heap_bh149_w39_5, heap_bh149_w39_5_d1, heap_bh149_w39_5_d2 :  std_logic;
signal heap_bh149_w40_4 :  std_logic;
signal CompressorIn_bh149_30_60 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_30_61 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_30_30 :  std_logic_vector(2 downto 0);
signal heap_bh149_w40_5, heap_bh149_w40_5_d1, heap_bh149_w40_5_d2 :  std_logic;
signal heap_bh149_w41_5, heap_bh149_w41_5_d1, heap_bh149_w41_5_d2 :  std_logic;
signal heap_bh149_w42_4 :  std_logic;
signal CompressorIn_bh149_31_62 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_31_63 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh149_31_31 :  std_logic_vector(2 downto 0);
signal heap_bh149_w42_5, heap_bh149_w42_5_d1, heap_bh149_w42_5_d2 :  std_logic;
signal heap_bh149_w43_5, heap_bh149_w43_5_d1, heap_bh149_w43_5_d2 :  std_logic;
signal heap_bh149_w44_4, heap_bh149_w44_4_d1 :  std_logic;
signal CompressorIn_bh149_32_64 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh149_32_65 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh149_32_32 :  std_logic_vector(2 downto 0);
signal heap_bh149_w44_5, heap_bh149_w44_5_d1 :  std_logic;
signal heap_bh149_w45_4, heap_bh149_w45_4_d1 :  std_logic;
signal heap_bh149_w46_3, heap_bh149_w46_3_d1 :  std_logic;
signal finalAdderIn0_bh149 :  std_logic_vector(47 downto 0);
signal finalAdderIn1_bh149 :  std_logic_vector(47 downto 0);
signal finalAdderCin_bh149 :  std_logic;
signal finalAdderOut_bh149 :  std_logic_vector(47 downto 0);
signal CompressionResult149 :  std_logic_vector(47 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            heap_bh149_w7_0_d1 <=  heap_bh149_w7_0;
            heap_bh149_w7_0_d2 <=  heap_bh149_w7_0_d1;
            heap_bh149_w8_0_d1 <=  heap_bh149_w8_0;
            heap_bh149_w8_0_d2 <=  heap_bh149_w8_0_d1;
            heap_bh149_w9_0_d1 <=  heap_bh149_w9_0;
            heap_bh149_w9_0_d2 <=  heap_bh149_w9_0_d1;
            heap_bh149_w10_0_d1 <=  heap_bh149_w10_0;
            heap_bh149_w10_0_d2 <=  heap_bh149_w10_0_d1;
            heap_bh149_w11_0_d1 <=  heap_bh149_w11_0;
            heap_bh149_w11_0_d2 <=  heap_bh149_w11_0_d1;
            heap_bh149_w12_0_d1 <=  heap_bh149_w12_0;
            heap_bh149_w12_0_d2 <=  heap_bh149_w12_0_d1;
            heap_bh149_w13_0_d1 <=  heap_bh149_w13_0;
            heap_bh149_w13_0_d2 <=  heap_bh149_w13_0_d1;
            heap_bh149_w14_0_d1 <=  heap_bh149_w14_0;
            heap_bh149_w14_0_d2 <=  heap_bh149_w14_0_d1;
            heap_bh149_w15_0_d1 <=  heap_bh149_w15_0;
            heap_bh149_w15_0_d2 <=  heap_bh149_w15_0_d1;
            heap_bh149_w16_0_d1 <=  heap_bh149_w16_0;
            heap_bh149_w16_0_d2 <=  heap_bh149_w16_0_d1;
            heap_bh149_w17_0_d1 <=  heap_bh149_w17_0;
            heap_bh149_w17_0_d2 <=  heap_bh149_w17_0_d1;
            heap_bh149_w18_0_d1 <=  heap_bh149_w18_0;
            heap_bh149_w18_0_d2 <=  heap_bh149_w18_0_d1;
            heap_bh149_w19_0_d1 <=  heap_bh149_w19_0;
            heap_bh149_w19_0_d2 <=  heap_bh149_w19_0_d1;
            heap_bh149_w20_0_d1 <=  heap_bh149_w20_0;
            heap_bh149_w20_0_d2 <=  heap_bh149_w20_0_d1;
            heap_bh149_w21_0_d1 <=  heap_bh149_w21_0;
            heap_bh149_w21_0_d2 <=  heap_bh149_w21_0_d1;
            heap_bh149_w22_0_d1 <=  heap_bh149_w22_0;
            heap_bh149_w22_0_d2 <=  heap_bh149_w22_0_d1;
            heap_bh149_w23_0_d1 <=  heap_bh149_w23_0;
            heap_bh149_w23_0_d2 <=  heap_bh149_w23_0_d1;
            heap_bh149_w24_0_d1 <=  heap_bh149_w24_0;
            heap_bh149_w24_0_d2 <=  heap_bh149_w24_0_d1;
            heap_bh149_w25_0_d1 <=  heap_bh149_w25_0;
            heap_bh149_w25_0_d2 <=  heap_bh149_w25_0_d1;
            heap_bh149_w26_0_d1 <=  heap_bh149_w26_0;
            heap_bh149_w26_0_d2 <=  heap_bh149_w26_0_d1;
            heap_bh149_w27_0_d1 <=  heap_bh149_w27_0;
            heap_bh149_w27_0_d2 <=  heap_bh149_w27_0_d1;
            heap_bh149_w28_0_d1 <=  heap_bh149_w28_0;
            heap_bh149_w28_0_d2 <=  heap_bh149_w28_0_d1;
            heap_bh149_w29_0_d1 <=  heap_bh149_w29_0;
            heap_bh149_w29_0_d2 <=  heap_bh149_w29_0_d1;
            heap_bh149_w30_0_d1 <=  heap_bh149_w30_0;
            heap_bh149_w30_0_d2 <=  heap_bh149_w30_0_d1;
            heap_bh149_w31_0_d1 <=  heap_bh149_w31_0;
            heap_bh149_w31_0_d2 <=  heap_bh149_w31_0_d1;
            heap_bh149_w32_0_d1 <=  heap_bh149_w32_0;
            heap_bh149_w32_0_d2 <=  heap_bh149_w32_0_d1;
            heap_bh149_w33_0_d1 <=  heap_bh149_w33_0;
            heap_bh149_w33_0_d2 <=  heap_bh149_w33_0_d1;
            heap_bh149_w34_0_d1 <=  heap_bh149_w34_0;
            heap_bh149_w34_0_d2 <=  heap_bh149_w34_0_d1;
            heap_bh149_w35_0_d1 <=  heap_bh149_w35_0;
            heap_bh149_w35_0_d2 <=  heap_bh149_w35_0_d1;
            heap_bh149_w36_0_d1 <=  heap_bh149_w36_0;
            heap_bh149_w36_0_d2 <=  heap_bh149_w36_0_d1;
            heap_bh149_w37_0_d1 <=  heap_bh149_w37_0;
            heap_bh149_w37_0_d2 <=  heap_bh149_w37_0_d1;
            heap_bh149_w38_0_d1 <=  heap_bh149_w38_0;
            heap_bh149_w38_0_d2 <=  heap_bh149_w38_0_d1;
            heap_bh149_w39_0_d1 <=  heap_bh149_w39_0;
            heap_bh149_w39_0_d2 <=  heap_bh149_w39_0_d1;
            heap_bh149_w40_0_d1 <=  heap_bh149_w40_0;
            heap_bh149_w40_0_d2 <=  heap_bh149_w40_0_d1;
            heap_bh149_w41_0_d1 <=  heap_bh149_w41_0;
            heap_bh149_w41_0_d2 <=  heap_bh149_w41_0_d1;
            heap_bh149_w42_0_d1 <=  heap_bh149_w42_0;
            heap_bh149_w42_0_d2 <=  heap_bh149_w42_0_d1;
            heap_bh149_w43_0_d1 <=  heap_bh149_w43_0;
            heap_bh149_w43_0_d2 <=  heap_bh149_w43_0_d1;
            heap_bh149_w44_0_d1 <=  heap_bh149_w44_0;
            heap_bh149_w44_0_d2 <=  heap_bh149_w44_0_d1;
            heap_bh149_w45_0_d1 <=  heap_bh149_w45_0;
            heap_bh149_w45_0_d2 <=  heap_bh149_w45_0_d1;
            heap_bh149_w46_0_d1 <=  heap_bh149_w46_0;
            heap_bh149_w46_0_d2 <=  heap_bh149_w46_0_d1;
            DSP_bh149_ch2_0_d1 <=  DSP_bh149_ch2_0;
            DSP_bh149_root2_1_d1 <=  DSP_bh149_root2_1;
            heap_bh149_w1_0_d1 <=  heap_bh149_w1_0;
            heap_bh149_w1_0_d2 <=  heap_bh149_w1_0_d1;
            heap_bh149_w1_0_d3 <=  heap_bh149_w1_0_d2;
            heap_bh149_w1_0_d4 <=  heap_bh149_w1_0_d3;
            heap_bh149_w1_0_d5 <=  heap_bh149_w1_0_d4;
            heap_bh149_w0_0_d1 <=  heap_bh149_w0_0;
            heap_bh149_w0_0_d2 <=  heap_bh149_w0_0_d1;
            heap_bh149_w0_0_d3 <=  heap_bh149_w0_0_d2;
            heap_bh149_w0_0_d4 <=  heap_bh149_w0_0_d3;
            heap_bh149_w0_0_d5 <=  heap_bh149_w0_0_d4;
            heap_bh149_w45_1_d1 <=  heap_bh149_w45_1;
            heap_bh149_w44_1_d1 <=  heap_bh149_w44_1;
            heap_bh149_w44_1_d2 <=  heap_bh149_w44_1_d1;
            heap_bh149_w44_1_d3 <=  heap_bh149_w44_1_d2;
            heap_bh149_w44_1_d4 <=  heap_bh149_w44_1_d3;
            heap_bh149_w43_1_d1 <=  heap_bh149_w43_1;
            heap_bh149_w42_1_d1 <=  heap_bh149_w42_1;
            heap_bh149_w42_1_d2 <=  heap_bh149_w42_1_d1;
            heap_bh149_w42_1_d3 <=  heap_bh149_w42_1_d2;
            heap_bh149_w41_1_d1 <=  heap_bh149_w41_1;
            heap_bh149_w40_1_d1 <=  heap_bh149_w40_1;
            heap_bh149_w40_1_d2 <=  heap_bh149_w40_1_d1;
            heap_bh149_w40_1_d3 <=  heap_bh149_w40_1_d2;
            heap_bh149_w39_1_d1 <=  heap_bh149_w39_1;
            heap_bh149_w38_1_d1 <=  heap_bh149_w38_1;
            heap_bh149_w38_1_d2 <=  heap_bh149_w38_1_d1;
            heap_bh149_w38_1_d3 <=  heap_bh149_w38_1_d2;
            heap_bh149_w37_1_d1 <=  heap_bh149_w37_1;
            heap_bh149_w36_1_d1 <=  heap_bh149_w36_1;
            heap_bh149_w36_1_d2 <=  heap_bh149_w36_1_d1;
            heap_bh149_w36_1_d3 <=  heap_bh149_w36_1_d2;
            heap_bh149_w35_1_d1 <=  heap_bh149_w35_1;
            heap_bh149_w34_1_d1 <=  heap_bh149_w34_1;
            heap_bh149_w34_1_d2 <=  heap_bh149_w34_1_d1;
            heap_bh149_w33_1_d1 <=  heap_bh149_w33_1;
            heap_bh149_w32_1_d1 <=  heap_bh149_w32_1;
            heap_bh149_w32_1_d2 <=  heap_bh149_w32_1_d1;
            heap_bh149_w31_1_d1 <=  heap_bh149_w31_1;
            heap_bh149_w30_1_d1 <=  heap_bh149_w30_1;
            heap_bh149_w30_1_d2 <=  heap_bh149_w30_1_d1;
            heap_bh149_w29_1_d1 <=  heap_bh149_w29_1;
            heap_bh149_w28_1_d1 <=  heap_bh149_w28_1;
            heap_bh149_w28_1_d2 <=  heap_bh149_w28_1_d1;
            heap_bh149_w27_1_d1 <=  heap_bh149_w27_1;
            heap_bh149_w26_1_d1 <=  heap_bh149_w26_1;
            heap_bh149_w25_1_d1 <=  heap_bh149_w25_1;
            heap_bh149_w24_1_d1 <=  heap_bh149_w24_1;
            heap_bh149_w23_1_d1 <=  heap_bh149_w23_1;
            heap_bh149_w22_1_d1 <=  heap_bh149_w22_1;
            heap_bh149_w21_1_d1 <=  heap_bh149_w21_1;
            heap_bh149_w20_1_d1 <=  heap_bh149_w20_1;
            heap_bh149_w20_1_d2 <=  heap_bh149_w20_1_d1;
            heap_bh149_w20_1_d3 <=  heap_bh149_w20_1_d2;
            heap_bh149_w20_1_d4 <=  heap_bh149_w20_1_d3;
            heap_bh149_w20_1_d5 <=  heap_bh149_w20_1_d4;
            heap_bh149_w19_1_d1 <=  heap_bh149_w19_1;
            heap_bh149_w18_1_d1 <=  heap_bh149_w18_1;
            heap_bh149_w18_1_d2 <=  heap_bh149_w18_1_d1;
            heap_bh149_w18_1_d3 <=  heap_bh149_w18_1_d2;
            heap_bh149_w18_1_d4 <=  heap_bh149_w18_1_d3;
            heap_bh149_w18_1_d5 <=  heap_bh149_w18_1_d4;
            heap_bh149_w17_1_d1 <=  heap_bh149_w17_1;
            heap_bh149_w16_1_d1 <=  heap_bh149_w16_1;
            heap_bh149_w16_1_d2 <=  heap_bh149_w16_1_d1;
            heap_bh149_w16_1_d3 <=  heap_bh149_w16_1_d2;
            heap_bh149_w16_1_d4 <=  heap_bh149_w16_1_d3;
            heap_bh149_w16_1_d5 <=  heap_bh149_w16_1_d4;
            heap_bh149_w15_1_d1 <=  heap_bh149_w15_1;
            heap_bh149_w14_1_d1 <=  heap_bh149_w14_1;
            heap_bh149_w14_1_d2 <=  heap_bh149_w14_1_d1;
            heap_bh149_w14_1_d3 <=  heap_bh149_w14_1_d2;
            heap_bh149_w14_1_d4 <=  heap_bh149_w14_1_d3;
            heap_bh149_w14_1_d5 <=  heap_bh149_w14_1_d4;
            heap_bh149_w13_1_d1 <=  heap_bh149_w13_1;
            heap_bh149_w12_1_d1 <=  heap_bh149_w12_1;
            heap_bh149_w12_1_d2 <=  heap_bh149_w12_1_d1;
            heap_bh149_w12_1_d3 <=  heap_bh149_w12_1_d2;
            heap_bh149_w12_1_d4 <=  heap_bh149_w12_1_d3;
            heap_bh149_w12_1_d5 <=  heap_bh149_w12_1_d4;
            heap_bh149_w11_1_d1 <=  heap_bh149_w11_1;
            heap_bh149_w10_1_d1 <=  heap_bh149_w10_1;
            heap_bh149_w10_1_d2 <=  heap_bh149_w10_1_d1;
            heap_bh149_w10_1_d3 <=  heap_bh149_w10_1_d2;
            heap_bh149_w10_1_d4 <=  heap_bh149_w10_1_d3;
            heap_bh149_w10_1_d5 <=  heap_bh149_w10_1_d4;
            heap_bh149_w9_1_d1 <=  heap_bh149_w9_1;
            heap_bh149_w8_1_d1 <=  heap_bh149_w8_1;
            heap_bh149_w8_1_d2 <=  heap_bh149_w8_1_d1;
            heap_bh149_w8_1_d3 <=  heap_bh149_w8_1_d2;
            heap_bh149_w8_1_d4 <=  heap_bh149_w8_1_d3;
            heap_bh149_w8_1_d5 <=  heap_bh149_w8_1_d4;
            heap_bh149_w7_1_d1 <=  heap_bh149_w7_1;
            heap_bh149_w6_0_d1 <=  heap_bh149_w6_0;
            heap_bh149_w5_0_d1 <=  heap_bh149_w5_0;
            heap_bh149_w4_0_d1 <=  heap_bh149_w4_0;
            heap_bh149_w4_0_d2 <=  heap_bh149_w4_0_d1;
            heap_bh149_w4_0_d3 <=  heap_bh149_w4_0_d2;
            heap_bh149_w4_0_d4 <=  heap_bh149_w4_0_d3;
            heap_bh149_w4_0_d5 <=  heap_bh149_w4_0_d4;
            heap_bh149_w3_0_d1 <=  heap_bh149_w3_0;
            heap_bh149_w3_0_d2 <=  heap_bh149_w3_0_d1;
            heap_bh149_w3_0_d3 <=  heap_bh149_w3_0_d2;
            heap_bh149_w3_0_d4 <=  heap_bh149_w3_0_d3;
            heap_bh149_w3_0_d5 <=  heap_bh149_w3_0_d4;
            heap_bh149_w2_0_d1 <=  heap_bh149_w2_0;
            heap_bh149_w2_0_d2 <=  heap_bh149_w2_0_d1;
            heap_bh149_w2_0_d3 <=  heap_bh149_w2_0_d2;
            heap_bh149_w2_0_d4 <=  heap_bh149_w2_0_d3;
            heap_bh149_w2_0_d5 <=  heap_bh149_w2_0_d4;
            DSP_bh149_ch3_0_d1 <=  DSP_bh149_ch3_0;
            DSP_bh149_root3_1_d1 <=  DSP_bh149_root3_1;
            heap_bh149_w21_2_d1 <=  heap_bh149_w21_2;
            heap_bh149_w20_2_d1 <=  heap_bh149_w20_2;
            heap_bh149_w19_2_d1 <=  heap_bh149_w19_2;
            heap_bh149_w18_2_d1 <=  heap_bh149_w18_2;
            heap_bh149_w17_2_d1 <=  heap_bh149_w17_2;
            heap_bh149_w16_2_d1 <=  heap_bh149_w16_2;
            heap_bh149_w15_2_d1 <=  heap_bh149_w15_2;
            heap_bh149_w14_2_d1 <=  heap_bh149_w14_2;
            heap_bh149_w13_2_d1 <=  heap_bh149_w13_2;
            heap_bh149_w12_2_d1 <=  heap_bh149_w12_2;
            heap_bh149_w11_2_d1 <=  heap_bh149_w11_2;
            heap_bh149_w10_2_d1 <=  heap_bh149_w10_2;
            heap_bh149_w9_2_d1 <=  heap_bh149_w9_2;
            heap_bh149_w8_2_d1 <=  heap_bh149_w8_2;
            heap_bh149_w7_2_d1 <=  heap_bh149_w7_2;
            heap_bh149_w6_1_d1 <=  heap_bh149_w6_1;
            heap_bh149_w5_1_d1 <=  heap_bh149_w5_1;
            heap_bh149_w4_1_d1 <=  heap_bh149_w4_1;
            heap_bh149_w4_1_d2 <=  heap_bh149_w4_1_d1;
            heap_bh149_w4_1_d3 <=  heap_bh149_w4_1_d2;
            heap_bh149_w4_1_d4 <=  heap_bh149_w4_1_d3;
            heap_bh149_w4_1_d5 <=  heap_bh149_w4_1_d4;
            heap_bh149_w3_1_d1 <=  heap_bh149_w3_1;
            heap_bh149_w3_1_d2 <=  heap_bh149_w3_1_d1;
            heap_bh149_w3_1_d3 <=  heap_bh149_w3_1_d2;
            heap_bh149_w3_1_d4 <=  heap_bh149_w3_1_d3;
            heap_bh149_w3_1_d5 <=  heap_bh149_w3_1_d4;
            heap_bh149_w2_1_d1 <=  heap_bh149_w2_1;
            heap_bh149_w2_1_d2 <=  heap_bh149_w2_1_d1;
            heap_bh149_w2_1_d3 <=  heap_bh149_w2_1_d2;
            heap_bh149_w2_1_d4 <=  heap_bh149_w2_1_d3;
            heap_bh149_w2_1_d5 <=  heap_bh149_w2_1_d4;
            heap_bh149_w1_1_d1 <=  heap_bh149_w1_1;
            heap_bh149_w1_1_d2 <=  heap_bh149_w1_1_d1;
            heap_bh149_w1_1_d3 <=  heap_bh149_w1_1_d2;
            heap_bh149_w1_1_d4 <=  heap_bh149_w1_1_d3;
            heap_bh149_w1_1_d5 <=  heap_bh149_w1_1_d4;
            heap_bh149_w0_1_d1 <=  heap_bh149_w0_1;
            heap_bh149_w0_1_d2 <=  heap_bh149_w0_1_d1;
            heap_bh149_w0_1_d3 <=  heap_bh149_w0_1_d2;
            heap_bh149_w0_1_d4 <=  heap_bh149_w0_1_d3;
            heap_bh149_w0_1_d5 <=  heap_bh149_w0_1_d4;
            heap_bh149_w5_2_d1 <=  heap_bh149_w5_2;
            heap_bh149_w5_2_d2 <=  heap_bh149_w5_2_d1;
            heap_bh149_w21_3_d1 <=  heap_bh149_w21_3;
            heap_bh149_w21_3_d2 <=  heap_bh149_w21_3_d1;
            heap_bh149_w22_2_d1 <=  heap_bh149_w22_2;
            heap_bh149_w22_2_d2 <=  heap_bh149_w22_2_d1;
            heap_bh149_w23_2_d1 <=  heap_bh149_w23_2;
            heap_bh149_w23_2_d2 <=  heap_bh149_w23_2_d1;
            heap_bh149_w24_2_d1 <=  heap_bh149_w24_2;
            heap_bh149_w24_2_d2 <=  heap_bh149_w24_2_d1;
            heap_bh149_w25_2_d1 <=  heap_bh149_w25_2;
            heap_bh149_w25_2_d2 <=  heap_bh149_w25_2_d1;
            heap_bh149_w26_2_d1 <=  heap_bh149_w26_2;
            heap_bh149_w26_2_d2 <=  heap_bh149_w26_2_d1;
            heap_bh149_w27_2_d1 <=  heap_bh149_w27_2;
            heap_bh149_w27_2_d2 <=  heap_bh149_w27_2_d1;
            heap_bh149_w28_2_d1 <=  heap_bh149_w28_2;
            heap_bh149_w28_2_d2 <=  heap_bh149_w28_2_d1;
            heap_bh149_w29_2_d1 <=  heap_bh149_w29_2;
            heap_bh149_w29_2_d2 <=  heap_bh149_w29_2_d1;
            heap_bh149_w30_2_d1 <=  heap_bh149_w30_2;
            heap_bh149_w30_2_d2 <=  heap_bh149_w30_2_d1;
            heap_bh149_w31_2_d1 <=  heap_bh149_w31_2;
            heap_bh149_w31_2_d2 <=  heap_bh149_w31_2_d1;
            heap_bh149_w32_2_d1 <=  heap_bh149_w32_2;
            heap_bh149_w32_2_d2 <=  heap_bh149_w32_2_d1;
            heap_bh149_w33_2_d1 <=  heap_bh149_w33_2;
            heap_bh149_w33_2_d2 <=  heap_bh149_w33_2_d1;
            heap_bh149_w34_2_d1 <=  heap_bh149_w34_2;
            heap_bh149_w34_2_d2 <=  heap_bh149_w34_2_d1;
            heap_bh149_w35_2_d1 <=  heap_bh149_w35_2;
            heap_bh149_w35_2_d2 <=  heap_bh149_w35_2_d1;
            heap_bh149_w36_2_d1 <=  heap_bh149_w36_2;
            heap_bh149_w36_2_d2 <=  heap_bh149_w36_2_d1;
            heap_bh149_w37_2_d1 <=  heap_bh149_w37_2;
            heap_bh149_w37_2_d2 <=  heap_bh149_w37_2_d1;
            heap_bh149_w38_2_d1 <=  heap_bh149_w38_2;
            heap_bh149_w38_2_d2 <=  heap_bh149_w38_2_d1;
            heap_bh149_w39_2_d1 <=  heap_bh149_w39_2;
            heap_bh149_w39_2_d2 <=  heap_bh149_w39_2_d1;
            heap_bh149_w40_2_d1 <=  heap_bh149_w40_2;
            heap_bh149_w40_2_d2 <=  heap_bh149_w40_2_d1;
            heap_bh149_w41_2_d1 <=  heap_bh149_w41_2;
            heap_bh149_w41_2_d2 <=  heap_bh149_w41_2_d1;
            heap_bh149_w42_2_d1 <=  heap_bh149_w42_2;
            heap_bh149_w42_2_d2 <=  heap_bh149_w42_2_d1;
            heap_bh149_w43_2_d1 <=  heap_bh149_w43_2;
            heap_bh149_w43_2_d2 <=  heap_bh149_w43_2_d1;
            heap_bh149_w44_2_d1 <=  heap_bh149_w44_2;
            heap_bh149_w44_2_d2 <=  heap_bh149_w44_2_d1;
            heap_bh149_w46_1_d1 <=  heap_bh149_w46_1;
            heap_bh149_w46_1_d2 <=  heap_bh149_w46_1_d1;
            heap_bh149_w21_4_d1 <=  heap_bh149_w21_4;
            heap_bh149_w21_4_d2 <=  heap_bh149_w21_4_d1;
            heap_bh149_w21_4_d3 <=  heap_bh149_w21_4_d2;
            heap_bh149_w21_4_d4 <=  heap_bh149_w21_4_d3;
            heap_bh149_w5_3_d1 <=  heap_bh149_w5_3;
            heap_bh149_w5_3_d2 <=  heap_bh149_w5_3_d1;
            heap_bh149_w5_3_d3 <=  heap_bh149_w5_3_d2;
            heap_bh149_w5_3_d4 <=  heap_bh149_w5_3_d3;
            heap_bh149_w6_2_d1 <=  heap_bh149_w6_2;
            heap_bh149_w6_2_d2 <=  heap_bh149_w6_2_d1;
            heap_bh149_w6_2_d3 <=  heap_bh149_w6_2_d2;
            heap_bh149_w6_2_d4 <=  heap_bh149_w6_2_d3;
            heap_bh149_w7_3_d1 <=  heap_bh149_w7_3;
            heap_bh149_w7_3_d2 <=  heap_bh149_w7_3_d1;
            heap_bh149_w7_3_d3 <=  heap_bh149_w7_3_d2;
            heap_bh149_w7_3_d4 <=  heap_bh149_w7_3_d3;
            heap_bh149_w7_4_d1 <=  heap_bh149_w7_4;
            heap_bh149_w7_4_d2 <=  heap_bh149_w7_4_d1;
            heap_bh149_w7_4_d3 <=  heap_bh149_w7_4_d2;
            heap_bh149_w7_4_d4 <=  heap_bh149_w7_4_d3;
            heap_bh149_w8_3_d1 <=  heap_bh149_w8_3;
            heap_bh149_w8_3_d2 <=  heap_bh149_w8_3_d1;
            heap_bh149_w8_3_d3 <=  heap_bh149_w8_3_d2;
            heap_bh149_w8_3_d4 <=  heap_bh149_w8_3_d3;
            heap_bh149_w9_3_d1 <=  heap_bh149_w9_3;
            heap_bh149_w9_3_d2 <=  heap_bh149_w9_3_d1;
            heap_bh149_w9_3_d3 <=  heap_bh149_w9_3_d2;
            heap_bh149_w9_3_d4 <=  heap_bh149_w9_3_d3;
            heap_bh149_w9_4_d1 <=  heap_bh149_w9_4;
            heap_bh149_w9_4_d2 <=  heap_bh149_w9_4_d1;
            heap_bh149_w9_4_d3 <=  heap_bh149_w9_4_d2;
            heap_bh149_w9_4_d4 <=  heap_bh149_w9_4_d3;
            heap_bh149_w10_3_d1 <=  heap_bh149_w10_3;
            heap_bh149_w10_3_d2 <=  heap_bh149_w10_3_d1;
            heap_bh149_w10_3_d3 <=  heap_bh149_w10_3_d2;
            heap_bh149_w10_3_d4 <=  heap_bh149_w10_3_d3;
            heap_bh149_w11_3_d1 <=  heap_bh149_w11_3;
            heap_bh149_w11_3_d2 <=  heap_bh149_w11_3_d1;
            heap_bh149_w11_3_d3 <=  heap_bh149_w11_3_d2;
            heap_bh149_w11_3_d4 <=  heap_bh149_w11_3_d3;
            heap_bh149_w11_4_d1 <=  heap_bh149_w11_4;
            heap_bh149_w11_4_d2 <=  heap_bh149_w11_4_d1;
            heap_bh149_w11_4_d3 <=  heap_bh149_w11_4_d2;
            heap_bh149_w11_4_d4 <=  heap_bh149_w11_4_d3;
            heap_bh149_w12_3_d1 <=  heap_bh149_w12_3;
            heap_bh149_w12_3_d2 <=  heap_bh149_w12_3_d1;
            heap_bh149_w12_3_d3 <=  heap_bh149_w12_3_d2;
            heap_bh149_w12_3_d4 <=  heap_bh149_w12_3_d3;
            heap_bh149_w13_3_d1 <=  heap_bh149_w13_3;
            heap_bh149_w13_3_d2 <=  heap_bh149_w13_3_d1;
            heap_bh149_w13_3_d3 <=  heap_bh149_w13_3_d2;
            heap_bh149_w13_3_d4 <=  heap_bh149_w13_3_d3;
            heap_bh149_w13_4_d1 <=  heap_bh149_w13_4;
            heap_bh149_w13_4_d2 <=  heap_bh149_w13_4_d1;
            heap_bh149_w13_4_d3 <=  heap_bh149_w13_4_d2;
            heap_bh149_w13_4_d4 <=  heap_bh149_w13_4_d3;
            heap_bh149_w14_3_d1 <=  heap_bh149_w14_3;
            heap_bh149_w14_3_d2 <=  heap_bh149_w14_3_d1;
            heap_bh149_w14_3_d3 <=  heap_bh149_w14_3_d2;
            heap_bh149_w14_3_d4 <=  heap_bh149_w14_3_d3;
            heap_bh149_w15_3_d1 <=  heap_bh149_w15_3;
            heap_bh149_w15_3_d2 <=  heap_bh149_w15_3_d1;
            heap_bh149_w15_3_d3 <=  heap_bh149_w15_3_d2;
            heap_bh149_w15_3_d4 <=  heap_bh149_w15_3_d3;
            heap_bh149_w15_4_d1 <=  heap_bh149_w15_4;
            heap_bh149_w15_4_d2 <=  heap_bh149_w15_4_d1;
            heap_bh149_w15_4_d3 <=  heap_bh149_w15_4_d2;
            heap_bh149_w15_4_d4 <=  heap_bh149_w15_4_d3;
            heap_bh149_w16_3_d1 <=  heap_bh149_w16_3;
            heap_bh149_w16_3_d2 <=  heap_bh149_w16_3_d1;
            heap_bh149_w16_3_d3 <=  heap_bh149_w16_3_d2;
            heap_bh149_w16_3_d4 <=  heap_bh149_w16_3_d3;
            heap_bh149_w17_3_d1 <=  heap_bh149_w17_3;
            heap_bh149_w17_3_d2 <=  heap_bh149_w17_3_d1;
            heap_bh149_w17_3_d3 <=  heap_bh149_w17_3_d2;
            heap_bh149_w17_3_d4 <=  heap_bh149_w17_3_d3;
            heap_bh149_w17_4_d1 <=  heap_bh149_w17_4;
            heap_bh149_w17_4_d2 <=  heap_bh149_w17_4_d1;
            heap_bh149_w17_4_d3 <=  heap_bh149_w17_4_d2;
            heap_bh149_w17_4_d4 <=  heap_bh149_w17_4_d3;
            heap_bh149_w18_3_d1 <=  heap_bh149_w18_3;
            heap_bh149_w18_3_d2 <=  heap_bh149_w18_3_d1;
            heap_bh149_w18_3_d3 <=  heap_bh149_w18_3_d2;
            heap_bh149_w18_3_d4 <=  heap_bh149_w18_3_d3;
            heap_bh149_w19_3_d1 <=  heap_bh149_w19_3;
            heap_bh149_w19_3_d2 <=  heap_bh149_w19_3_d1;
            heap_bh149_w19_3_d3 <=  heap_bh149_w19_3_d2;
            heap_bh149_w19_3_d4 <=  heap_bh149_w19_3_d3;
            heap_bh149_w19_4_d1 <=  heap_bh149_w19_4;
            heap_bh149_w19_4_d2 <=  heap_bh149_w19_4_d1;
            heap_bh149_w19_4_d3 <=  heap_bh149_w19_4_d2;
            heap_bh149_w19_4_d4 <=  heap_bh149_w19_4_d3;
            heap_bh149_w20_3_d1 <=  heap_bh149_w20_3;
            heap_bh149_w20_3_d2 <=  heap_bh149_w20_3_d1;
            heap_bh149_w20_3_d3 <=  heap_bh149_w20_3_d2;
            heap_bh149_w20_3_d4 <=  heap_bh149_w20_3_d3;
            heap_bh149_w21_5_d1 <=  heap_bh149_w21_5;
            heap_bh149_w21_5_d2 <=  heap_bh149_w21_5_d1;
            heap_bh149_w21_5_d3 <=  heap_bh149_w21_5_d2;
            heap_bh149_w21_5_d4 <=  heap_bh149_w21_5_d3;
            heap_bh149_w28_3_d1 <=  heap_bh149_w28_3;
            heap_bh149_w29_3_d1 <=  heap_bh149_w29_3;
            heap_bh149_w29_4_d1 <=  heap_bh149_w29_4;
            heap_bh149_w30_3_d1 <=  heap_bh149_w30_3;
            heap_bh149_w31_3_d1 <=  heap_bh149_w31_3;
            heap_bh149_w31_4_d1 <=  heap_bh149_w31_4;
            heap_bh149_w32_3_d1 <=  heap_bh149_w32_3;
            heap_bh149_w33_3_d1 <=  heap_bh149_w33_3;
            heap_bh149_w33_4_d1 <=  heap_bh149_w33_4;
            heap_bh149_w34_3_d1 <=  heap_bh149_w34_3;
            heap_bh149_w35_3_d1 <=  heap_bh149_w35_3;
            heap_bh149_w35_4_d1 <=  heap_bh149_w35_4;
            heap_bh149_w36_3_d1 <=  heap_bh149_w36_3;
            heap_bh149_w36_3_d2 <=  heap_bh149_w36_3_d1;
            heap_bh149_w37_3_d1 <=  heap_bh149_w37_3;
            heap_bh149_w37_3_d2 <=  heap_bh149_w37_3_d1;
            heap_bh149_w37_4_d1 <=  heap_bh149_w37_4;
            heap_bh149_w37_4_d2 <=  heap_bh149_w37_4_d1;
            heap_bh149_w38_3_d1 <=  heap_bh149_w38_3;
            heap_bh149_w38_3_d2 <=  heap_bh149_w38_3_d1;
            heap_bh149_w39_3_d1 <=  heap_bh149_w39_3;
            heap_bh149_w39_3_d2 <=  heap_bh149_w39_3_d1;
            heap_bh149_w39_4_d1 <=  heap_bh149_w39_4;
            heap_bh149_w39_4_d2 <=  heap_bh149_w39_4_d1;
            heap_bh149_w40_3_d1 <=  heap_bh149_w40_3;
            heap_bh149_w40_3_d2 <=  heap_bh149_w40_3_d1;
            heap_bh149_w41_3_d1 <=  heap_bh149_w41_3;
            heap_bh149_w41_3_d2 <=  heap_bh149_w41_3_d1;
            heap_bh149_w41_4_d1 <=  heap_bh149_w41_4;
            heap_bh149_w41_4_d2 <=  heap_bh149_w41_4_d1;
            heap_bh149_w42_3_d1 <=  heap_bh149_w42_3;
            heap_bh149_w42_3_d2 <=  heap_bh149_w42_3_d1;
            heap_bh149_w43_3_d1 <=  heap_bh149_w43_3;
            heap_bh149_w43_3_d2 <=  heap_bh149_w43_3_d1;
            heap_bh149_w43_4_d1 <=  heap_bh149_w43_4;
            heap_bh149_w43_4_d2 <=  heap_bh149_w43_4_d1;
            heap_bh149_w44_3_d1 <=  heap_bh149_w44_3;
            heap_bh149_w44_3_d2 <=  heap_bh149_w44_3_d1;
            heap_bh149_w44_3_d3 <=  heap_bh149_w44_3_d2;
            heap_bh149_w22_4_d1 <=  heap_bh149_w22_4;
            heap_bh149_w22_4_d2 <=  heap_bh149_w22_4_d1;
            heap_bh149_w22_4_d3 <=  heap_bh149_w22_4_d2;
            heap_bh149_w22_4_d4 <=  heap_bh149_w22_4_d3;
            heap_bh149_w23_5_d1 <=  heap_bh149_w23_5;
            heap_bh149_w23_5_d2 <=  heap_bh149_w23_5_d1;
            heap_bh149_w23_5_d3 <=  heap_bh149_w23_5_d2;
            heap_bh149_w23_5_d4 <=  heap_bh149_w23_5_d3;
            heap_bh149_w45_3_d1 <=  heap_bh149_w45_3;
            heap_bh149_w45_3_d2 <=  heap_bh149_w45_3_d1;
            heap_bh149_w45_3_d3 <=  heap_bh149_w45_3_d2;
            heap_bh149_w46_2_d1 <=  heap_bh149_w46_2;
            heap_bh149_w46_2_d2 <=  heap_bh149_w46_2_d1;
            heap_bh149_w46_2_d3 <=  heap_bh149_w46_2_d2;
            heap_bh149_w46_2_d4 <=  heap_bh149_w46_2_d3;
            heap_bh149_w24_5_d1 <=  heap_bh149_w24_5;
            heap_bh149_w24_5_d2 <=  heap_bh149_w24_5_d1;
            heap_bh149_w24_5_d3 <=  heap_bh149_w24_5_d2;
            heap_bh149_w24_5_d4 <=  heap_bh149_w24_5_d3;
            heap_bh149_w25_5_d1 <=  heap_bh149_w25_5;
            heap_bh149_w25_5_d2 <=  heap_bh149_w25_5_d1;
            heap_bh149_w25_5_d3 <=  heap_bh149_w25_5_d2;
            heap_bh149_w25_5_d4 <=  heap_bh149_w25_5_d3;
            heap_bh149_w26_5_d1 <=  heap_bh149_w26_5;
            heap_bh149_w26_5_d2 <=  heap_bh149_w26_5_d1;
            heap_bh149_w26_5_d3 <=  heap_bh149_w26_5_d2;
            heap_bh149_w26_5_d4 <=  heap_bh149_w26_5_d3;
            heap_bh149_w27_5_d1 <=  heap_bh149_w27_5;
            heap_bh149_w27_5_d2 <=  heap_bh149_w27_5_d1;
            heap_bh149_w27_5_d3 <=  heap_bh149_w27_5_d2;
            heap_bh149_w27_5_d4 <=  heap_bh149_w27_5_d3;
            heap_bh149_w28_4_d1 <=  heap_bh149_w28_4;
            heap_bh149_w28_5_d1 <=  heap_bh149_w28_5;
            heap_bh149_w28_5_d2 <=  heap_bh149_w28_5_d1;
            heap_bh149_w28_5_d3 <=  heap_bh149_w28_5_d2;
            heap_bh149_w29_5_d1 <=  heap_bh149_w29_5;
            heap_bh149_w29_5_d2 <=  heap_bh149_w29_5_d1;
            heap_bh149_w29_5_d3 <=  heap_bh149_w29_5_d2;
            heap_bh149_w30_5_d1 <=  heap_bh149_w30_5;
            heap_bh149_w30_5_d2 <=  heap_bh149_w30_5_d1;
            heap_bh149_w30_5_d3 <=  heap_bh149_w30_5_d2;
            heap_bh149_w31_5_d1 <=  heap_bh149_w31_5;
            heap_bh149_w31_5_d2 <=  heap_bh149_w31_5_d1;
            heap_bh149_w31_5_d3 <=  heap_bh149_w31_5_d2;
            heap_bh149_w32_5_d1 <=  heap_bh149_w32_5;
            heap_bh149_w32_5_d2 <=  heap_bh149_w32_5_d1;
            heap_bh149_w32_5_d3 <=  heap_bh149_w32_5_d2;
            heap_bh149_w33_5_d1 <=  heap_bh149_w33_5;
            heap_bh149_w33_5_d2 <=  heap_bh149_w33_5_d1;
            heap_bh149_w33_5_d3 <=  heap_bh149_w33_5_d2;
            heap_bh149_w34_5_d1 <=  heap_bh149_w34_5;
            heap_bh149_w34_5_d2 <=  heap_bh149_w34_5_d1;
            heap_bh149_w34_5_d3 <=  heap_bh149_w34_5_d2;
            heap_bh149_w35_5_d1 <=  heap_bh149_w35_5;
            heap_bh149_w35_5_d2 <=  heap_bh149_w35_5_d1;
            heap_bh149_w35_5_d3 <=  heap_bh149_w35_5_d2;
            heap_bh149_w36_4_d1 <=  heap_bh149_w36_4;
            heap_bh149_w36_5_d1 <=  heap_bh149_w36_5;
            heap_bh149_w36_5_d2 <=  heap_bh149_w36_5_d1;
            heap_bh149_w37_5_d1 <=  heap_bh149_w37_5;
            heap_bh149_w37_5_d2 <=  heap_bh149_w37_5_d1;
            heap_bh149_w38_5_d1 <=  heap_bh149_w38_5;
            heap_bh149_w38_5_d2 <=  heap_bh149_w38_5_d1;
            heap_bh149_w39_5_d1 <=  heap_bh149_w39_5;
            heap_bh149_w39_5_d2 <=  heap_bh149_w39_5_d1;
            heap_bh149_w40_5_d1 <=  heap_bh149_w40_5;
            heap_bh149_w40_5_d2 <=  heap_bh149_w40_5_d1;
            heap_bh149_w41_5_d1 <=  heap_bh149_w41_5;
            heap_bh149_w41_5_d2 <=  heap_bh149_w41_5_d1;
            heap_bh149_w42_5_d1 <=  heap_bh149_w42_5;
            heap_bh149_w42_5_d2 <=  heap_bh149_w42_5_d1;
            heap_bh149_w43_5_d1 <=  heap_bh149_w43_5;
            heap_bh149_w43_5_d2 <=  heap_bh149_w43_5_d1;
            heap_bh149_w44_4_d1 <=  heap_bh149_w44_4;
            heap_bh149_w44_5_d1 <=  heap_bh149_w44_5;
            heap_bh149_w45_4_d1 <=  heap_bh149_w45_4;
            heap_bh149_w46_3_d1 <=  heap_bh149_w46_3;
         end if;
      end process;
   XX_m151 <= Y ;
   YY_m151 <= X ;
   heap_bh149_w7_0 <= A(0); -- cycle= 0 cp= 0
   heap_bh149_w8_0 <= A(1); -- cycle= 0 cp= 0
   heap_bh149_w9_0 <= A(2); -- cycle= 0 cp= 0
   heap_bh149_w10_0 <= A(3); -- cycle= 0 cp= 0
   heap_bh149_w11_0 <= A(4); -- cycle= 0 cp= 0
   heap_bh149_w12_0 <= A(5); -- cycle= 0 cp= 0
   heap_bh149_w13_0 <= A(6); -- cycle= 0 cp= 0
   heap_bh149_w14_0 <= A(7); -- cycle= 0 cp= 0
   heap_bh149_w15_0 <= A(8); -- cycle= 0 cp= 0
   heap_bh149_w16_0 <= A(9); -- cycle= 0 cp= 0
   heap_bh149_w17_0 <= A(10); -- cycle= 0 cp= 0
   heap_bh149_w18_0 <= A(11); -- cycle= 0 cp= 0
   heap_bh149_w19_0 <= A(12); -- cycle= 0 cp= 0
   heap_bh149_w20_0 <= A(13); -- cycle= 0 cp= 0
   heap_bh149_w21_0 <= A(14); -- cycle= 0 cp= 0
   heap_bh149_w22_0 <= A(15); -- cycle= 0 cp= 0
   heap_bh149_w23_0 <= A(16); -- cycle= 0 cp= 0
   heap_bh149_w24_0 <= A(17); -- cycle= 0 cp= 0
   heap_bh149_w25_0 <= A(18); -- cycle= 0 cp= 0
   heap_bh149_w26_0 <= A(19); -- cycle= 0 cp= 0
   heap_bh149_w27_0 <= A(20); -- cycle= 0 cp= 0
   heap_bh149_w28_0 <= A(21); -- cycle= 0 cp= 0
   heap_bh149_w29_0 <= A(22); -- cycle= 0 cp= 0
   heap_bh149_w30_0 <= A(23); -- cycle= 0 cp= 0
   heap_bh149_w31_0 <= A(24); -- cycle= 0 cp= 0
   heap_bh149_w32_0 <= A(25); -- cycle= 0 cp= 0
   heap_bh149_w33_0 <= A(26); -- cycle= 0 cp= 0
   heap_bh149_w34_0 <= A(27); -- cycle= 0 cp= 0
   heap_bh149_w35_0 <= A(28); -- cycle= 0 cp= 0
   heap_bh149_w36_0 <= A(29); -- cycle= 0 cp= 0
   heap_bh149_w37_0 <= A(30); -- cycle= 0 cp= 0
   heap_bh149_w38_0 <= A(31); -- cycle= 0 cp= 0
   heap_bh149_w39_0 <= A(32); -- cycle= 0 cp= 0
   heap_bh149_w40_0 <= A(33); -- cycle= 0 cp= 0
   heap_bh149_w41_0 <= A(34); -- cycle= 0 cp= 0
   heap_bh149_w42_0 <= A(35); -- cycle= 0 cp= 0
   heap_bh149_w43_0 <= A(36); -- cycle= 0 cp= 0
   heap_bh149_w44_0 <= A(37); -- cycle= 0 cp= 0
   heap_bh149_w45_0 <= A(38); -- cycle= 0 cp= 0
   heap_bh149_w46_0 <= A(39); -- cycle= 0 cp= 0
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh149_ch2_0 <= std_logic_vector(signed("" & XX_m151(37 downto 13) & "") * signed("0" & YY_m151(15 downto 0) & "0"));
   DSP_bh149_root2_1 <= std_logic_vector(signed("" & XX_m151(37 downto 13) & "") * signed("" & YY_m151(33 downto 16) & ""));
   ----------------Synchro barrier, entering cycle 1----------------
   DSP_bh149_ch2_1<= std_logic_vector(signed(DSP_bh149_root2_1_d1) +  signed( DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) & DSP_bh149_ch2_0_d1(42) &   DSP_bh149_ch2_0_d1(42 downto 17) ));
   heap_bh149_w1_0 <= DSP_bh149_ch2_0_d1(16); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w0_0 <= DSP_bh149_ch2_0_d1(15); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w45_1 <= not( DSP_bh149_ch2_1(43) ); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w44_1 <= DSP_bh149_ch2_1(42); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w43_1 <= DSP_bh149_ch2_1(41); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w42_1 <= DSP_bh149_ch2_1(40); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w41_1 <= DSP_bh149_ch2_1(39); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w40_1 <= DSP_bh149_ch2_1(38); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w39_1 <= DSP_bh149_ch2_1(37); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w38_1 <= DSP_bh149_ch2_1(36); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w37_1 <= DSP_bh149_ch2_1(35); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w36_1 <= DSP_bh149_ch2_1(34); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w35_1 <= DSP_bh149_ch2_1(33); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w34_1 <= DSP_bh149_ch2_1(32); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w33_1 <= DSP_bh149_ch2_1(31); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w32_1 <= DSP_bh149_ch2_1(30); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w31_1 <= DSP_bh149_ch2_1(29); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w30_1 <= DSP_bh149_ch2_1(28); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w29_1 <= DSP_bh149_ch2_1(27); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w28_1 <= DSP_bh149_ch2_1(26); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w27_1 <= DSP_bh149_ch2_1(25); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w26_1 <= DSP_bh149_ch2_1(24); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w25_1 <= DSP_bh149_ch2_1(23); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w24_1 <= DSP_bh149_ch2_1(22); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w23_1 <= DSP_bh149_ch2_1(21); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w22_1 <= DSP_bh149_ch2_1(20); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w21_1 <= DSP_bh149_ch2_1(19); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w20_1 <= DSP_bh149_ch2_1(18); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w19_1 <= DSP_bh149_ch2_1(17); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w18_1 <= DSP_bh149_ch2_1(16); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w17_1 <= DSP_bh149_ch2_1(15); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w16_1 <= DSP_bh149_ch2_1(14); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w15_1 <= DSP_bh149_ch2_1(13); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w14_1 <= DSP_bh149_ch2_1(12); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w13_1 <= DSP_bh149_ch2_1(11); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w12_1 <= DSP_bh149_ch2_1(10); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w11_1 <= DSP_bh149_ch2_1(9); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w10_1 <= DSP_bh149_ch2_1(8); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w9_1 <= DSP_bh149_ch2_1(7); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w8_1 <= DSP_bh149_ch2_1(6); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w7_1 <= DSP_bh149_ch2_1(5); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w6_0 <= DSP_bh149_ch2_1(4); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w5_0 <= DSP_bh149_ch2_1(3); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w4_0 <= DSP_bh149_ch2_1(2); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w3_0 <= DSP_bh149_ch2_1(1); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w2_0 <= DSP_bh149_ch2_1(0); -- cycle= 1 cp= 2.274e-09
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh149_ch3_0 <= std_logic_vector(signed("0" & XX_m151(12 downto 0) & "00000000000") * signed("0" & YY_m151(15 downto 0) & "0"));
   DSP_bh149_root3_1 <= std_logic_vector(signed("0" & XX_m151(12 downto 0) & "00000000000") * signed("" & YY_m151(33 downto 16) & ""));
   ----------------Synchro barrier, entering cycle 1----------------
   DSP_bh149_ch3_1<= std_logic_vector(signed(DSP_bh149_root3_1_d1) +  signed( DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) & DSP_bh149_ch3_0_d1(42) &   DSP_bh149_ch3_0_d1(42 downto 17) ));
   heap_bh149_w21_2 <= not( DSP_bh149_ch3_1(43) ); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w20_2 <= DSP_bh149_ch3_1(42); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w19_2 <= DSP_bh149_ch3_1(41); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w18_2 <= DSP_bh149_ch3_1(40); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w17_2 <= DSP_bh149_ch3_1(39); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w16_2 <= DSP_bh149_ch3_1(38); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w15_2 <= DSP_bh149_ch3_1(37); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w14_2 <= DSP_bh149_ch3_1(36); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w13_2 <= DSP_bh149_ch3_1(35); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w12_2 <= DSP_bh149_ch3_1(34); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w11_2 <= DSP_bh149_ch3_1(33); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w10_2 <= DSP_bh149_ch3_1(32); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w9_2 <= DSP_bh149_ch3_1(31); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w8_2 <= DSP_bh149_ch3_1(30); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w7_2 <= DSP_bh149_ch3_1(29); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w6_1 <= DSP_bh149_ch3_1(28); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w5_1 <= DSP_bh149_ch3_1(27); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w4_1 <= DSP_bh149_ch3_1(26); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w3_1 <= DSP_bh149_ch3_1(25); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w2_1 <= DSP_bh149_ch3_1(24); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w1_1 <= DSP_bh149_ch3_1(23); -- cycle= 1 cp= 2.274e-09
   heap_bh149_w0_1 <= DSP_bh149_ch3_1(22); -- cycle= 1 cp= 2.274e-09
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
   heap_bh149_w5_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w21_3 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w22_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w23_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w24_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w25_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w26_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w27_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w28_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w29_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w30_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w31_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w32_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w33_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w34_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w35_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w36_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w37_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w38_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w39_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w40_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w41_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w42_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w43_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w44_2 <= '1'; -- cycle= 0 cp= 0
   heap_bh149_w46_1 <= '1'; -- cycle= 0 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_0_0 <= heap_bh149_w21_3_d2 & heap_bh149_w21_0_d2 & heap_bh149_w21_2_d1 & heap_bh149_w21_1_d1;
   CompressorIn_bh149_0_1(0) <= heap_bh149_w22_2_d2;
   Compressor_bh149_0: Compressor_14_3
      port map ( R => CompressorOut_bh149_0_0   ,
                 X0 => CompressorIn_bh149_0_0,
                 X1 => CompressorIn_bh149_0_1);
   heap_bh149_w21_4 <= CompressorOut_bh149_0_0(0); -- cycle= 2 cp= 0
   heap_bh149_w22_3 <= CompressorOut_bh149_0_0(1); -- cycle= 2 cp= 0
   heap_bh149_w23_3 <= CompressorOut_bh149_0_0(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_1_2 <= heap_bh149_w5_2_d2 & heap_bh149_w5_1_d1 & heap_bh149_w5_0_d1;
   CompressorIn_bh149_1_3 <= heap_bh149_w6_1_d1 & heap_bh149_w6_0_d1;
   Compressor_bh149_1: Compressor_23_3
      port map ( R => CompressorOut_bh149_1_1   ,
                 X0 => CompressorIn_bh149_1_2,
                 X1 => CompressorIn_bh149_1_3);
   heap_bh149_w5_3 <= CompressorOut_bh149_1_1(0); -- cycle= 2 cp= 0
   heap_bh149_w6_2 <= CompressorOut_bh149_1_1(1); -- cycle= 2 cp= 0
   heap_bh149_w7_3 <= CompressorOut_bh149_1_1(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_2_4 <= heap_bh149_w7_0_d2 & heap_bh149_w7_2_d1 & heap_bh149_w7_1_d1;
   CompressorIn_bh149_2_5 <= heap_bh149_w8_0_d2 & heap_bh149_w8_2_d1;
   Compressor_bh149_2: Compressor_23_3
      port map ( R => CompressorOut_bh149_2_2   ,
                 X0 => CompressorIn_bh149_2_4,
                 X1 => CompressorIn_bh149_2_5);
   heap_bh149_w7_4 <= CompressorOut_bh149_2_2(0); -- cycle= 2 cp= 0
   heap_bh149_w8_3 <= CompressorOut_bh149_2_2(1); -- cycle= 2 cp= 0
   heap_bh149_w9_3 <= CompressorOut_bh149_2_2(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_3_6 <= heap_bh149_w9_0_d2 & heap_bh149_w9_2_d1 & heap_bh149_w9_1_d1;
   CompressorIn_bh149_3_7 <= heap_bh149_w10_0_d2 & heap_bh149_w10_2_d1;
   Compressor_bh149_3: Compressor_23_3
      port map ( R => CompressorOut_bh149_3_3   ,
                 X0 => CompressorIn_bh149_3_6,
                 X1 => CompressorIn_bh149_3_7);
   heap_bh149_w9_4 <= CompressorOut_bh149_3_3(0); -- cycle= 2 cp= 0
   heap_bh149_w10_3 <= CompressorOut_bh149_3_3(1); -- cycle= 2 cp= 0
   heap_bh149_w11_3 <= CompressorOut_bh149_3_3(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_4_8 <= heap_bh149_w11_0_d2 & heap_bh149_w11_2_d1 & heap_bh149_w11_1_d1;
   CompressorIn_bh149_4_9 <= heap_bh149_w12_0_d2 & heap_bh149_w12_2_d1;
   Compressor_bh149_4: Compressor_23_3
      port map ( R => CompressorOut_bh149_4_4   ,
                 X0 => CompressorIn_bh149_4_8,
                 X1 => CompressorIn_bh149_4_9);
   heap_bh149_w11_4 <= CompressorOut_bh149_4_4(0); -- cycle= 2 cp= 0
   heap_bh149_w12_3 <= CompressorOut_bh149_4_4(1); -- cycle= 2 cp= 0
   heap_bh149_w13_3 <= CompressorOut_bh149_4_4(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_5_10 <= heap_bh149_w13_0_d2 & heap_bh149_w13_2_d1 & heap_bh149_w13_1_d1;
   CompressorIn_bh149_5_11 <= heap_bh149_w14_0_d2 & heap_bh149_w14_2_d1;
   Compressor_bh149_5: Compressor_23_3
      port map ( R => CompressorOut_bh149_5_5   ,
                 X0 => CompressorIn_bh149_5_10,
                 X1 => CompressorIn_bh149_5_11);
   heap_bh149_w13_4 <= CompressorOut_bh149_5_5(0); -- cycle= 2 cp= 0
   heap_bh149_w14_3 <= CompressorOut_bh149_5_5(1); -- cycle= 2 cp= 0
   heap_bh149_w15_3 <= CompressorOut_bh149_5_5(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_6_12 <= heap_bh149_w15_0_d2 & heap_bh149_w15_2_d1 & heap_bh149_w15_1_d1;
   CompressorIn_bh149_6_13 <= heap_bh149_w16_0_d2 & heap_bh149_w16_2_d1;
   Compressor_bh149_6: Compressor_23_3
      port map ( R => CompressorOut_bh149_6_6   ,
                 X0 => CompressorIn_bh149_6_12,
                 X1 => CompressorIn_bh149_6_13);
   heap_bh149_w15_4 <= CompressorOut_bh149_6_6(0); -- cycle= 2 cp= 0
   heap_bh149_w16_3 <= CompressorOut_bh149_6_6(1); -- cycle= 2 cp= 0
   heap_bh149_w17_3 <= CompressorOut_bh149_6_6(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_7_14 <= heap_bh149_w17_0_d2 & heap_bh149_w17_2_d1 & heap_bh149_w17_1_d1;
   CompressorIn_bh149_7_15 <= heap_bh149_w18_0_d2 & heap_bh149_w18_2_d1;
   Compressor_bh149_7: Compressor_23_3
      port map ( R => CompressorOut_bh149_7_7   ,
                 X0 => CompressorIn_bh149_7_14,
                 X1 => CompressorIn_bh149_7_15);
   heap_bh149_w17_4 <= CompressorOut_bh149_7_7(0); -- cycle= 2 cp= 0
   heap_bh149_w18_3 <= CompressorOut_bh149_7_7(1); -- cycle= 2 cp= 0
   heap_bh149_w19_3 <= CompressorOut_bh149_7_7(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_8_16 <= heap_bh149_w19_0_d2 & heap_bh149_w19_2_d1 & heap_bh149_w19_1_d1;
   CompressorIn_bh149_8_17 <= heap_bh149_w20_0_d2 & heap_bh149_w20_2_d1;
   Compressor_bh149_8: Compressor_23_3
      port map ( R => CompressorOut_bh149_8_8   ,
                 X0 => CompressorIn_bh149_8_16,
                 X1 => CompressorIn_bh149_8_17);
   heap_bh149_w19_4 <= CompressorOut_bh149_8_8(0); -- cycle= 2 cp= 0
   heap_bh149_w20_3 <= CompressorOut_bh149_8_8(1); -- cycle= 2 cp= 0
   heap_bh149_w21_5 <= CompressorOut_bh149_8_8(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_9_18 <= heap_bh149_w23_2_d2 & heap_bh149_w23_0_d2 & heap_bh149_w23_1_d1;
   CompressorIn_bh149_9_19 <= heap_bh149_w24_2_d2 & heap_bh149_w24_0_d2;
   Compressor_bh149_9: Compressor_23_3
      port map ( R => CompressorOut_bh149_9_9   ,
                 X0 => CompressorIn_bh149_9_18,
                 X1 => CompressorIn_bh149_9_19);
   heap_bh149_w23_4 <= CompressorOut_bh149_9_9(0); -- cycle= 2 cp= 0
   heap_bh149_w24_3 <= CompressorOut_bh149_9_9(1); -- cycle= 2 cp= 0
   heap_bh149_w25_3 <= CompressorOut_bh149_9_9(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_10_20 <= heap_bh149_w25_2_d2 & heap_bh149_w25_0_d2 & heap_bh149_w25_1_d1;
   CompressorIn_bh149_10_21 <= heap_bh149_w26_2_d2 & heap_bh149_w26_0_d2;
   Compressor_bh149_10: Compressor_23_3
      port map ( R => CompressorOut_bh149_10_10   ,
                 X0 => CompressorIn_bh149_10_20,
                 X1 => CompressorIn_bh149_10_21);
   heap_bh149_w25_4 <= CompressorOut_bh149_10_10(0); -- cycle= 2 cp= 0
   heap_bh149_w26_3 <= CompressorOut_bh149_10_10(1); -- cycle= 2 cp= 0
   heap_bh149_w27_3 <= CompressorOut_bh149_10_10(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_11_22 <= heap_bh149_w27_2_d2 & heap_bh149_w27_0_d2 & heap_bh149_w27_1_d1;
   CompressorIn_bh149_11_23 <= heap_bh149_w28_2_d2 & heap_bh149_w28_0_d2;
   Compressor_bh149_11: Compressor_23_3
      port map ( R => CompressorOut_bh149_11_11   ,
                 X0 => CompressorIn_bh149_11_22,
                 X1 => CompressorIn_bh149_11_23);
   heap_bh149_w27_4 <= CompressorOut_bh149_11_11(0); -- cycle= 2 cp= 0
   heap_bh149_w28_3 <= CompressorOut_bh149_11_11(1); -- cycle= 2 cp= 0
   heap_bh149_w29_3 <= CompressorOut_bh149_11_11(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_12_24 <= heap_bh149_w29_2_d2 & heap_bh149_w29_0_d2 & heap_bh149_w29_1_d1;
   CompressorIn_bh149_12_25 <= heap_bh149_w30_2_d2 & heap_bh149_w30_0_d2;
   Compressor_bh149_12: Compressor_23_3
      port map ( R => CompressorOut_bh149_12_12   ,
                 X0 => CompressorIn_bh149_12_24,
                 X1 => CompressorIn_bh149_12_25);
   heap_bh149_w29_4 <= CompressorOut_bh149_12_12(0); -- cycle= 2 cp= 0
   heap_bh149_w30_3 <= CompressorOut_bh149_12_12(1); -- cycle= 2 cp= 0
   heap_bh149_w31_3 <= CompressorOut_bh149_12_12(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_13_26 <= heap_bh149_w31_2_d2 & heap_bh149_w31_0_d2 & heap_bh149_w31_1_d1;
   CompressorIn_bh149_13_27 <= heap_bh149_w32_2_d2 & heap_bh149_w32_0_d2;
   Compressor_bh149_13: Compressor_23_3
      port map ( R => CompressorOut_bh149_13_13   ,
                 X0 => CompressorIn_bh149_13_26,
                 X1 => CompressorIn_bh149_13_27);
   heap_bh149_w31_4 <= CompressorOut_bh149_13_13(0); -- cycle= 2 cp= 0
   heap_bh149_w32_3 <= CompressorOut_bh149_13_13(1); -- cycle= 2 cp= 0
   heap_bh149_w33_3 <= CompressorOut_bh149_13_13(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_14_28 <= heap_bh149_w33_2_d2 & heap_bh149_w33_0_d2 & heap_bh149_w33_1_d1;
   CompressorIn_bh149_14_29 <= heap_bh149_w34_2_d2 & heap_bh149_w34_0_d2;
   Compressor_bh149_14: Compressor_23_3
      port map ( R => CompressorOut_bh149_14_14   ,
                 X0 => CompressorIn_bh149_14_28,
                 X1 => CompressorIn_bh149_14_29);
   heap_bh149_w33_4 <= CompressorOut_bh149_14_14(0); -- cycle= 2 cp= 0
   heap_bh149_w34_3 <= CompressorOut_bh149_14_14(1); -- cycle= 2 cp= 0
   heap_bh149_w35_3 <= CompressorOut_bh149_14_14(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_15_30 <= heap_bh149_w35_2_d2 & heap_bh149_w35_0_d2 & heap_bh149_w35_1_d1;
   CompressorIn_bh149_15_31 <= heap_bh149_w36_2_d2 & heap_bh149_w36_0_d2;
   Compressor_bh149_15: Compressor_23_3
      port map ( R => CompressorOut_bh149_15_15   ,
                 X0 => CompressorIn_bh149_15_30,
                 X1 => CompressorIn_bh149_15_31);
   heap_bh149_w35_4 <= CompressorOut_bh149_15_15(0); -- cycle= 2 cp= 0
   heap_bh149_w36_3 <= CompressorOut_bh149_15_15(1); -- cycle= 2 cp= 0
   heap_bh149_w37_3 <= CompressorOut_bh149_15_15(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_16_32 <= heap_bh149_w37_2_d2 & heap_bh149_w37_0_d2 & heap_bh149_w37_1_d1;
   CompressorIn_bh149_16_33 <= heap_bh149_w38_2_d2 & heap_bh149_w38_0_d2;
   Compressor_bh149_16: Compressor_23_3
      port map ( R => CompressorOut_bh149_16_16   ,
                 X0 => CompressorIn_bh149_16_32,
                 X1 => CompressorIn_bh149_16_33);
   heap_bh149_w37_4 <= CompressorOut_bh149_16_16(0); -- cycle= 2 cp= 0
   heap_bh149_w38_3 <= CompressorOut_bh149_16_16(1); -- cycle= 2 cp= 0
   heap_bh149_w39_3 <= CompressorOut_bh149_16_16(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_17_34 <= heap_bh149_w39_2_d2 & heap_bh149_w39_0_d2 & heap_bh149_w39_1_d1;
   CompressorIn_bh149_17_35 <= heap_bh149_w40_2_d2 & heap_bh149_w40_0_d2;
   Compressor_bh149_17: Compressor_23_3
      port map ( R => CompressorOut_bh149_17_17   ,
                 X0 => CompressorIn_bh149_17_34,
                 X1 => CompressorIn_bh149_17_35);
   heap_bh149_w39_4 <= CompressorOut_bh149_17_17(0); -- cycle= 2 cp= 0
   heap_bh149_w40_3 <= CompressorOut_bh149_17_17(1); -- cycle= 2 cp= 0
   heap_bh149_w41_3 <= CompressorOut_bh149_17_17(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_18_36 <= heap_bh149_w41_2_d2 & heap_bh149_w41_0_d2 & heap_bh149_w41_1_d1;
   CompressorIn_bh149_18_37 <= heap_bh149_w42_2_d2 & heap_bh149_w42_0_d2;
   Compressor_bh149_18: Compressor_23_3
      port map ( R => CompressorOut_bh149_18_18   ,
                 X0 => CompressorIn_bh149_18_36,
                 X1 => CompressorIn_bh149_18_37);
   heap_bh149_w41_4 <= CompressorOut_bh149_18_18(0); -- cycle= 2 cp= 0
   heap_bh149_w42_3 <= CompressorOut_bh149_18_18(1); -- cycle= 2 cp= 0
   heap_bh149_w43_3 <= CompressorOut_bh149_18_18(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_19_38 <= heap_bh149_w43_2_d2 & heap_bh149_w43_0_d2 & heap_bh149_w43_1_d1;
   CompressorIn_bh149_19_39 <= heap_bh149_w44_2_d2 & heap_bh149_w44_0_d2;
   Compressor_bh149_19: Compressor_23_3
      port map ( R => CompressorOut_bh149_19_19   ,
                 X0 => CompressorIn_bh149_19_38,
                 X1 => CompressorIn_bh149_19_39);
   heap_bh149_w43_4 <= CompressorOut_bh149_19_19(0); -- cycle= 2 cp= 0
   heap_bh149_w44_3 <= CompressorOut_bh149_19_19(1); -- cycle= 2 cp= 0
   heap_bh149_w45_2 <= CompressorOut_bh149_19_19(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_20_40 <= heap_bh149_w22_0_d2 & heap_bh149_w22_1_d1 & heap_bh149_w22_3;
   CompressorIn_bh149_20_41 <= heap_bh149_w23_4 & heap_bh149_w23_3;
   Compressor_bh149_20: Compressor_23_3
      port map ( R => CompressorOut_bh149_20_20   ,
                 X0 => CompressorIn_bh149_20_40,
                 X1 => CompressorIn_bh149_20_41);
   heap_bh149_w22_4 <= CompressorOut_bh149_20_20(0); -- cycle= 2 cp= 5.3072e-10
   heap_bh149_w23_5 <= CompressorOut_bh149_20_20(1); -- cycle= 2 cp= 5.3072e-10
   heap_bh149_w24_4 <= CompressorOut_bh149_20_20(2); -- cycle= 2 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_21_42 <= heap_bh149_w45_0_d2 & heap_bh149_w45_1_d1 & heap_bh149_w45_2;
   CompressorIn_bh149_21_43 <= heap_bh149_w46_1_d2 & heap_bh149_w46_0_d2;
   Compressor_bh149_21: Compressor_23_3
      port map ( R => CompressorOut_bh149_21_21   ,
                 X0 => CompressorIn_bh149_21_42,
                 X1 => CompressorIn_bh149_21_43);
   heap_bh149_w45_3 <= CompressorOut_bh149_21_21(0); -- cycle= 2 cp= 5.3072e-10
   heap_bh149_w46_2 <= CompressorOut_bh149_21_21(1); -- cycle= 2 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_22_44 <= heap_bh149_w24_1_d1 & heap_bh149_w24_3 & heap_bh149_w24_4;
   CompressorIn_bh149_22_45 <= heap_bh149_w25_4 & heap_bh149_w25_3;
   Compressor_bh149_22: Compressor_23_3
      port map ( R => CompressorOut_bh149_22_22   ,
                 X0 => CompressorIn_bh149_22_44,
                 X1 => CompressorIn_bh149_22_45);
   heap_bh149_w24_5 <= CompressorOut_bh149_22_22(0); -- cycle= 2 cp= 1.06144e-09
   heap_bh149_w25_5 <= CompressorOut_bh149_22_22(1); -- cycle= 2 cp= 1.06144e-09
   heap_bh149_w26_4 <= CompressorOut_bh149_22_22(2); -- cycle= 2 cp= 1.06144e-09

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh149_23_46 <= heap_bh149_w26_1_d1 & heap_bh149_w26_3 & heap_bh149_w26_4;
   CompressorIn_bh149_23_47 <= heap_bh149_w27_4 & heap_bh149_w27_3;
   Compressor_bh149_23: Compressor_23_3
      port map ( R => CompressorOut_bh149_23_23   ,
                 X0 => CompressorIn_bh149_23_46,
                 X1 => CompressorIn_bh149_23_47);
   heap_bh149_w26_5 <= CompressorOut_bh149_23_23(0); -- cycle= 2 cp= 1.59216e-09
   heap_bh149_w27_5 <= CompressorOut_bh149_23_23(1); -- cycle= 2 cp= 1.59216e-09
   heap_bh149_w28_4 <= CompressorOut_bh149_23_23(2); -- cycle= 2 cp= 1.59216e-09

   ----------------Synchro barrier, entering cycle 2----------------
   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh149_24_48 <= heap_bh149_w28_1_d2 & heap_bh149_w28_3_d1 & heap_bh149_w28_4_d1;
   CompressorIn_bh149_24_49 <= heap_bh149_w29_4_d1 & heap_bh149_w29_3_d1;
   Compressor_bh149_24: Compressor_23_3
      port map ( R => CompressorOut_bh149_24_24   ,
                 X0 => CompressorIn_bh149_24_48,
                 X1 => CompressorIn_bh149_24_49);
   heap_bh149_w28_5 <= CompressorOut_bh149_24_24(0); -- cycle= 3 cp= 0
   heap_bh149_w29_5 <= CompressorOut_bh149_24_24(1); -- cycle= 3 cp= 0
   heap_bh149_w30_4 <= CompressorOut_bh149_24_24(2); -- cycle= 3 cp= 0

   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh149_25_50 <= heap_bh149_w30_1_d2 & heap_bh149_w30_3_d1 & heap_bh149_w30_4;
   CompressorIn_bh149_25_51 <= heap_bh149_w31_4_d1 & heap_bh149_w31_3_d1;
   Compressor_bh149_25: Compressor_23_3
      port map ( R => CompressorOut_bh149_25_25   ,
                 X0 => CompressorIn_bh149_25_50,
                 X1 => CompressorIn_bh149_25_51);
   heap_bh149_w30_5 <= CompressorOut_bh149_25_25(0); -- cycle= 3 cp= 5.3072e-10
   heap_bh149_w31_5 <= CompressorOut_bh149_25_25(1); -- cycle= 3 cp= 5.3072e-10
   heap_bh149_w32_4 <= CompressorOut_bh149_25_25(2); -- cycle= 3 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh149_26_52 <= heap_bh149_w32_1_d2 & heap_bh149_w32_3_d1 & heap_bh149_w32_4;
   CompressorIn_bh149_26_53 <= heap_bh149_w33_4_d1 & heap_bh149_w33_3_d1;
   Compressor_bh149_26: Compressor_23_3
      port map ( R => CompressorOut_bh149_26_26   ,
                 X0 => CompressorIn_bh149_26_52,
                 X1 => CompressorIn_bh149_26_53);
   heap_bh149_w32_5 <= CompressorOut_bh149_26_26(0); -- cycle= 3 cp= 1.06144e-09
   heap_bh149_w33_5 <= CompressorOut_bh149_26_26(1); -- cycle= 3 cp= 1.06144e-09
   heap_bh149_w34_4 <= CompressorOut_bh149_26_26(2); -- cycle= 3 cp= 1.06144e-09

   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh149_27_54 <= heap_bh149_w34_1_d2 & heap_bh149_w34_3_d1 & heap_bh149_w34_4;
   CompressorIn_bh149_27_55 <= heap_bh149_w35_4_d1 & heap_bh149_w35_3_d1;
   Compressor_bh149_27: Compressor_23_3
      port map ( R => CompressorOut_bh149_27_27   ,
                 X0 => CompressorIn_bh149_27_54,
                 X1 => CompressorIn_bh149_27_55);
   heap_bh149_w34_5 <= CompressorOut_bh149_27_27(0); -- cycle= 3 cp= 1.59216e-09
   heap_bh149_w35_5 <= CompressorOut_bh149_27_27(1); -- cycle= 3 cp= 1.59216e-09
   heap_bh149_w36_4 <= CompressorOut_bh149_27_27(2); -- cycle= 3 cp= 1.59216e-09

   ----------------Synchro barrier, entering cycle 3----------------
   ----------------Synchro barrier, entering cycle 4----------------
   CompressorIn_bh149_28_56 <= heap_bh149_w36_1_d3 & heap_bh149_w36_3_d2 & heap_bh149_w36_4_d1;
   CompressorIn_bh149_28_57 <= heap_bh149_w37_4_d2 & heap_bh149_w37_3_d2;
   Compressor_bh149_28: Compressor_23_3
      port map ( R => CompressorOut_bh149_28_28   ,
                 X0 => CompressorIn_bh149_28_56,
                 X1 => CompressorIn_bh149_28_57);
   heap_bh149_w36_5 <= CompressorOut_bh149_28_28(0); -- cycle= 4 cp= 0
   heap_bh149_w37_5 <= CompressorOut_bh149_28_28(1); -- cycle= 4 cp= 0
   heap_bh149_w38_4 <= CompressorOut_bh149_28_28(2); -- cycle= 4 cp= 0

   ----------------Synchro barrier, entering cycle 4----------------
   CompressorIn_bh149_29_58 <= heap_bh149_w38_1_d3 & heap_bh149_w38_3_d2 & heap_bh149_w38_4;
   CompressorIn_bh149_29_59 <= heap_bh149_w39_4_d2 & heap_bh149_w39_3_d2;
   Compressor_bh149_29: Compressor_23_3
      port map ( R => CompressorOut_bh149_29_29   ,
                 X0 => CompressorIn_bh149_29_58,
                 X1 => CompressorIn_bh149_29_59);
   heap_bh149_w38_5 <= CompressorOut_bh149_29_29(0); -- cycle= 4 cp= 5.3072e-10
   heap_bh149_w39_5 <= CompressorOut_bh149_29_29(1); -- cycle= 4 cp= 5.3072e-10
   heap_bh149_w40_4 <= CompressorOut_bh149_29_29(2); -- cycle= 4 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 4----------------
   CompressorIn_bh149_30_60 <= heap_bh149_w40_1_d3 & heap_bh149_w40_3_d2 & heap_bh149_w40_4;
   CompressorIn_bh149_30_61 <= heap_bh149_w41_4_d2 & heap_bh149_w41_3_d2;
   Compressor_bh149_30: Compressor_23_3
      port map ( R => CompressorOut_bh149_30_30   ,
                 X0 => CompressorIn_bh149_30_60,
                 X1 => CompressorIn_bh149_30_61);
   heap_bh149_w40_5 <= CompressorOut_bh149_30_30(0); -- cycle= 4 cp= 1.06144e-09
   heap_bh149_w41_5 <= CompressorOut_bh149_30_30(1); -- cycle= 4 cp= 1.06144e-09
   heap_bh149_w42_4 <= CompressorOut_bh149_30_30(2); -- cycle= 4 cp= 1.06144e-09

   ----------------Synchro barrier, entering cycle 4----------------
   CompressorIn_bh149_31_62 <= heap_bh149_w42_1_d3 & heap_bh149_w42_3_d2 & heap_bh149_w42_4;
   CompressorIn_bh149_31_63 <= heap_bh149_w43_4_d2 & heap_bh149_w43_3_d2;
   Compressor_bh149_31: Compressor_23_3
      port map ( R => CompressorOut_bh149_31_31   ,
                 X0 => CompressorIn_bh149_31_62,
                 X1 => CompressorIn_bh149_31_63);
   heap_bh149_w42_5 <= CompressorOut_bh149_31_31(0); -- cycle= 4 cp= 1.59216e-09
   heap_bh149_w43_5 <= CompressorOut_bh149_31_31(1); -- cycle= 4 cp= 1.59216e-09
   heap_bh149_w44_4 <= CompressorOut_bh149_31_31(2); -- cycle= 4 cp= 1.59216e-09

   ----------------Synchro barrier, entering cycle 4----------------
   ----------------Synchro barrier, entering cycle 5----------------
   CompressorIn_bh149_32_64 <= heap_bh149_w44_1_d4 & heap_bh149_w44_3_d3 & heap_bh149_w44_4_d1;
   CompressorIn_bh149_32_65(0) <= heap_bh149_w45_3_d3;
   Compressor_bh149_32: Compressor_13_3
      port map ( R => CompressorOut_bh149_32_32   ,
                 X0 => CompressorIn_bh149_32_64,
                 X1 => CompressorIn_bh149_32_65);
   heap_bh149_w44_5 <= CompressorOut_bh149_32_32(0); -- cycle= 5 cp= 0
   heap_bh149_w45_4 <= CompressorOut_bh149_32_32(1); -- cycle= 5 cp= 0
   heap_bh149_w46_3 <= CompressorOut_bh149_32_32(2); -- cycle= 5 cp= 0
   ----------------Synchro barrier, entering cycle 5----------------
   ----------------Synchro barrier, entering cycle 6----------------
   finalAdderIn0_bh149 <= "0" & heap_bh149_w46_2_d4 & heap_bh149_w45_4_d1 & heap_bh149_w44_5_d1 & heap_bh149_w43_5_d2 & heap_bh149_w42_5_d2 & heap_bh149_w41_5_d2 & heap_bh149_w40_5_d2 & heap_bh149_w39_5_d2 & heap_bh149_w38_5_d2 & heap_bh149_w37_5_d2 & heap_bh149_w36_5_d2 & heap_bh149_w35_5_d3 & heap_bh149_w34_5_d3 & heap_bh149_w33_5_d3 & heap_bh149_w32_5_d3 & heap_bh149_w31_5_d3 & heap_bh149_w30_5_d3 & heap_bh149_w29_5_d3 & heap_bh149_w28_5_d3 & heap_bh149_w27_5_d4 & heap_bh149_w26_5_d4 & heap_bh149_w25_5_d4 & heap_bh149_w24_5_d4 & heap_bh149_w23_5_d4 & heap_bh149_w22_4_d4 & heap_bh149_w21_5_d4 & heap_bh149_w20_1_d5 & heap_bh149_w19_4_d4 & heap_bh149_w18_1_d5 & heap_bh149_w17_4_d4 & heap_bh149_w16_1_d5 & heap_bh149_w15_4_d4 & heap_bh149_w14_1_d5 & heap_bh149_w13_4_d4 & heap_bh149_w12_1_d5 & heap_bh149_w11_4_d4 & heap_bh149_w10_1_d5 & heap_bh149_w9_4_d4 & heap_bh149_w8_1_d5 & heap_bh149_w7_4_d4 & heap_bh149_w6_2_d4 & heap_bh149_w5_3_d4 & heap_bh149_w4_1_d5 & heap_bh149_w3_1_d5 & heap_bh149_w2_1_d5 & heap_bh149_w1_1_d5 & heap_bh149_w0_1_d5;
   finalAdderIn1_bh149 <= "0" & heap_bh149_w46_3_d1 & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh149_w21_4_d4 & heap_bh149_w20_3_d4 & heap_bh149_w19_3_d4 & heap_bh149_w18_3_d4 & heap_bh149_w17_3_d4 & heap_bh149_w16_3_d4 & heap_bh149_w15_3_d4 & heap_bh149_w14_3_d4 & heap_bh149_w13_3_d4 & heap_bh149_w12_3_d4 & heap_bh149_w11_3_d4 & heap_bh149_w10_3_d4 & heap_bh149_w9_3_d4 & heap_bh149_w8_3_d4 & heap_bh149_w7_3_d4 & '0' & '0' & heap_bh149_w4_0_d5 & heap_bh149_w3_0_d5 & heap_bh149_w2_0_d5 & heap_bh149_w1_0_d5 & heap_bh149_w0_0_d5;
   finalAdderCin_bh149 <= '0';
   Adder_final149_0: IntAdder_48_f400_uid194  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh149,
                 R => finalAdderOut_bh149   ,
                 X => finalAdderIn0_bh149,
                 Y => finalAdderIn1_bh149);
   ----------------Synchro barrier, entering cycle 7----------------
   -- concatenate all the compressed chunks
   CompressionResult149 <= finalAdderOut_bh149;
   -- End of code generated by BitHeap::generateCompressorVHDL
   R <= CompressionResult149(46 downto 6);
end architecture;

--------------------------------------------------------------------------------
--                       FixHornerEvaluator_F400_uid65
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: F. de Dinechin (2014)
--------------------------------------------------------------------------------
-- Pipeline depth: 20 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FixHornerEvaluator_F400_uid65 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(33 downto 0);
          A0 : in  std_logic_vector(39 downto 0);
          A1 : in  std_logic_vector(36 downto 0);
          A2 : in  std_logic_vector(33 downto 0);
          A3 : in  std_logic_vector(18 downto 0);
          R : out  std_logic_vector(36 downto 0)   );
end entity;

architecture arch of FixHornerEvaluator_F400_uid65 is
   component FixMultAdd_19x19p34r35signed67 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(18 downto 0);
             Y : in  std_logic_vector(18 downto 0);
             A : in  std_logic_vector(33 downto 0);
             R : out  std_logic_vector(34 downto 0)   );
   end component;

   component FixMultAdd_34x35p37r38signed95 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(33 downto 0);
             Y : in  std_logic_vector(34 downto 0);
             A : in  std_logic_vector(36 downto 0);
             R : out  std_logic_vector(37 downto 0)   );
   end component;

   component FixMultAdd_34x38p40r41signed148 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(33 downto 0);
             Y : in  std_logic_vector(37 downto 0);
             A : in  std_logic_vector(39 downto 0);
             R : out  std_logic_vector(40 downto 0)   );
   end component;

signal Xs, Xs_d1, Xs_d2, Xs_d3, Xs_d4, Xs_d5, Xs_d6, Xs_d7, Xs_d8, Xs_d9, Xs_d10, Xs_d11, Xs_d12 :  signed(0+33 downto 0);
signal As0, As0_d1, As0_d2, As0_d3, As0_d4, As0_d5, As0_d6, As0_d7, As0_d8, As0_d9, As0_d10, As0_d11, As0_d12 :  signed(-1+40 downto 0);
signal As1, As1_d1, As1_d2, As1_d3, As1_d4, As1_d5 :  signed(-4+40 downto 0);
signal As2 :  signed(-7+40 downto 0);
signal As3 :  signed(-22+40 downto 0);
signal Sigma3 :  signed(-22+40 downto 0);
signal XsTrunc2 :  signed(0+18 downto 0);
signal Sigma2_slv :  std_logic_vector(34 downto 0);
signal Sigma2, Sigma2_d1 :  signed(-7+41 downto 0);
signal XsTrunc1 :  signed(0+33 downto 0);
signal Sigma1_slv :  std_logic_vector(37 downto 0);
signal Sigma1, Sigma1_d1 :  signed(-4+41 downto 0);
signal XsTrunc0 :  signed(0+33 downto 0);
signal Sigma0_slv :  std_logic_vector(40 downto 0);
signal Sigma0, Sigma0_d1 :  signed(-1+41 downto 0);
signal Ys :  signed(-1+37 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            Xs_d1 <=  Xs;
            Xs_d2 <=  Xs_d1;
            Xs_d3 <=  Xs_d2;
            Xs_d4 <=  Xs_d3;
            Xs_d5 <=  Xs_d4;
            Xs_d6 <=  Xs_d5;
            Xs_d7 <=  Xs_d6;
            Xs_d8 <=  Xs_d7;
            Xs_d9 <=  Xs_d8;
            Xs_d10 <=  Xs_d9;
            Xs_d11 <=  Xs_d10;
            Xs_d12 <=  Xs_d11;
            As0_d1 <=  As0;
            As0_d2 <=  As0_d1;
            As0_d3 <=  As0_d2;
            As0_d4 <=  As0_d3;
            As0_d5 <=  As0_d4;
            As0_d6 <=  As0_d5;
            As0_d7 <=  As0_d6;
            As0_d8 <=  As0_d7;
            As0_d9 <=  As0_d8;
            As0_d10 <=  As0_d9;
            As0_d11 <=  As0_d10;
            As0_d12 <=  As0_d11;
            As1_d1 <=  As1;
            As1_d2 <=  As1_d1;
            As1_d3 <=  As1_d2;
            As1_d4 <=  As1_d3;
            As1_d5 <=  As1_d4;
            Sigma2_d1 <=  Sigma2;
            Sigma1_d1 <=  Sigma1;
            Sigma0_d1 <=  Sigma0;
         end if;
      end process;
   Xs <= signed(X);
   As0 <= signed(A0);
   As1 <= signed(A1);
   As2 <= signed(A2);
   As3 <= signed(A3);
   Sigma3 <= As3;
   XsTrunc2 <= Xs(33 downto 15); -- fix resize from (0, -33) to (0, -18)
   Step2: FixMultAdd_19x19p34r35signed67  -- pipelineDepth=4 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 A => std_logic_vector(As2),
                 R => Sigma2_slv,
                 X => std_logic_vector(XsTrunc2),
                 Y => std_logic_vector(Sigma3));
   ----------------Synchro barrier, entering cycle 4----------------
   Sigma2 <= signed(Sigma2_slv);
   ----------------Synchro barrier, entering cycle 5----------------
   XsTrunc1 <= Xs_d5(33 downto 0); -- fix resize from (0, -33) to (0, -33)
   Step1: FixMultAdd_34x35p37r38signed95  -- pipelineDepth=6 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 A => std_logic_vector(As1_d5),
                 R => Sigma1_slv,
                 X => std_logic_vector(XsTrunc1),
                 Y => std_logic_vector(Sigma2_d1));
   ----------------Synchro barrier, entering cycle 11----------------
   Sigma1 <= signed(Sigma1_slv);
   ----------------Synchro barrier, entering cycle 12----------------
   XsTrunc0 <= Xs_d12(33 downto 0); -- fix resize from (0, -33) to (0, -33)
   Step0: FixMultAdd_34x38p40r41signed148  -- pipelineDepth=7 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 A => std_logic_vector(As0_d12),
                 R => Sigma0_slv,
                 X => std_logic_vector(XsTrunc0),
                 Y => std_logic_vector(Sigma1_d1));
   ----------------Synchro barrier, entering cycle 19----------------
   Sigma0 <= signed(Sigma0_slv);
   ----------------Synchro barrier, entering cycle 20----------------
   Ys <= Sigma0_d1(40 downto 4); -- fix resize from (-1, -41) to (-1, -37)
   R <= std_logic_vector(Ys);
end architecture;

--------------------------------------------------------------------------------
--                       FixFunctionByPiecewisePoly_61
-- Evaluator for 1b19*(exp(x*1b-10)-x*1b-10-1) on [0,1) for lsbIn=-36 (wIn=36), msbout=-1, lsbOut=-37
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2014)
--------------------------------------------------------------------------------
-- Pipeline depth: 20 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library std;
use std.textio.all;
library work;

entity FixFunctionByPiecewisePoly_61 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(35 downto 0);
          Y : out  std_logic_vector(36 downto 0)   );
end entity;

architecture arch of FixFunctionByPiecewisePoly_61 is
   component FixHornerEvaluator_F400_uid65 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(33 downto 0);
             A0 : in  std_logic_vector(39 downto 0);
             A1 : in  std_logic_vector(36 downto 0);
             A2 : in  std_logic_vector(33 downto 0);
             A3 : in  std_logic_vector(18 downto 0);
             R : out  std_logic_vector(36 downto 0)   );
   end component;

   component GenericTable_2_126 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(1 downto 0);
             Y : out  std_logic_vector(125 downto 0)   );
   end component;

signal A :  std_logic_vector(1 downto 0);
signal Z :  std_logic_vector(33 downto 0);
signal Zs :  std_logic_vector(33 downto 0);
signal Coeffs :  std_logic_vector(125 downto 0);
signal A3 :  std_logic_vector(18 downto 0);
signal A2 :  std_logic_vector(33 downto 0);
signal A1 :  std_logic_vector(36 downto 0);
signal A0 :  std_logic_vector(39 downto 0);
signal Ys :  std_logic_vector(36 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   A <= X(35 downto 34);
   Z <= X(33 downto 0);
   Zs <= (not Z(33)) & Z(32 downto 0); -- centering the interval
   coeffTable: GenericTable_2_126  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => A,
                 Y => Coeffs);

   A3 <= "0" & Coeffs(17 downto 0);
   A2 <= "0" & Coeffs(50 downto 18);
   A1 <= "0" & Coeffs(86 downto 51);
   A0 <= "0" & Coeffs(125 downto 87);
   horner: FixHornerEvaluator_F400_uid65  -- pipelineDepth=20 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 A0 => A0,
                 A1 => A1,
                 A2 => A2,
                 A3 => A3,
                 R => Ys,
                 X => Zs);

   ----------------Synchro barrier, entering cycle 20----------------
   Y <= std_logic_vector(Ys);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_47_f400_uid204
--                    (IntAdderAlternative_47_f400_uid208)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_47_f400_uid204 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(46 downto 0);
          Y : in  std_logic_vector(46 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(46 downto 0)   );
end entity;

architecture arch of IntAdder_47_f400_uid204 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(5 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(4 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(5 downto 0);
signal sum_l1_idx1 :  std_logic_vector(4 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(46 downto 42)) + ( "0" & Y(46 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(4 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(5 downto 5);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(4 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(5 downto 5);
   R <= sum_l1_idx1(4 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_55_f400_uid266
--                    (IntAdderAlternative_55_f400_uid270)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_55_f400_uid266 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(54 downto 0);
          Y : in  std_logic_vector(54 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(54 downto 0)   );
end entity;

architecture arch of IntAdder_55_f400_uid266 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(13 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(12 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(13 downto 0);
signal sum_l1_idx1 :  std_logic_vector(12 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(54 downto 42)) + ( "0" & Y(54 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(12 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(13 downto 13);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(12 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(13 downto 13);
   R <= sum_l1_idx1(12 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--            IntMultiplier_UsingDSP_46_47_48_unsigned_F400_uid211
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Kinga Illyes, Bogdan Popa, Bogdan Pasca, 2012
--------------------------------------------------------------------------------
-- Pipeline depth: 4 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library std;
use std.textio.all;
library work;

entity IntMultiplier_UsingDSP_46_47_48_unsigned_F400_uid211 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(45 downto 0);
          Y : in  std_logic_vector(46 downto 0);
          R : out  std_logic_vector(47 downto 0)   );
end entity;

architecture arch of IntMultiplier_UsingDSP_46_47_48_unsigned_F400_uid211 is
   component Compressor_13_3 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             X1 : in  std_logic_vector(0 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component Compressor_14_3 is
      port ( X0 : in  std_logic_vector(3 downto 0);
             X1 : in  std_logic_vector(0 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component Compressor_23_3 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             X1 : in  std_logic_vector(1 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component Compressor_3_2 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             R : out  std_logic_vector(1 downto 0)   );
   end component;

   component IntAdder_55_f400_uid266 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(54 downto 0);
             Y : in  std_logic_vector(54 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(54 downto 0)   );
   end component;

signal XX_m212 :  std_logic_vector(46 downto 0);
signal YY_m212 :  std_logic_vector(45 downto 0);
signal DSP_bh213_ch0_0 :  std_logic_vector(40 downto 0);
signal heap_bh213_w54_0, heap_bh213_w54_0_d1, heap_bh213_w54_0_d2, heap_bh213_w54_0_d3 :  std_logic;
signal heap_bh213_w53_0, heap_bh213_w53_0_d1, heap_bh213_w53_0_d2, heap_bh213_w53_0_d3 :  std_logic;
signal heap_bh213_w52_0, heap_bh213_w52_0_d1, heap_bh213_w52_0_d2, heap_bh213_w52_0_d3 :  std_logic;
signal heap_bh213_w51_0, heap_bh213_w51_0_d1, heap_bh213_w51_0_d2, heap_bh213_w51_0_d3 :  std_logic;
signal heap_bh213_w50_0, heap_bh213_w50_0_d1, heap_bh213_w50_0_d2, heap_bh213_w50_0_d3 :  std_logic;
signal heap_bh213_w49_0, heap_bh213_w49_0_d1, heap_bh213_w49_0_d2, heap_bh213_w49_0_d3 :  std_logic;
signal heap_bh213_w48_0, heap_bh213_w48_0_d1, heap_bh213_w48_0_d2, heap_bh213_w48_0_d3 :  std_logic;
signal heap_bh213_w47_0, heap_bh213_w47_0_d1, heap_bh213_w47_0_d2, heap_bh213_w47_0_d3 :  std_logic;
signal heap_bh213_w46_0, heap_bh213_w46_0_d1, heap_bh213_w46_0_d2, heap_bh213_w46_0_d3 :  std_logic;
signal heap_bh213_w45_0, heap_bh213_w45_0_d1, heap_bh213_w45_0_d2, heap_bh213_w45_0_d3 :  std_logic;
signal heap_bh213_w44_0, heap_bh213_w44_0_d1, heap_bh213_w44_0_d2, heap_bh213_w44_0_d3 :  std_logic;
signal heap_bh213_w43_0, heap_bh213_w43_0_d1, heap_bh213_w43_0_d2, heap_bh213_w43_0_d3 :  std_logic;
signal heap_bh213_w42_0, heap_bh213_w42_0_d1, heap_bh213_w42_0_d2, heap_bh213_w42_0_d3 :  std_logic;
signal heap_bh213_w41_0, heap_bh213_w41_0_d1, heap_bh213_w41_0_d2, heap_bh213_w41_0_d3 :  std_logic;
signal heap_bh213_w40_0, heap_bh213_w40_0_d1, heap_bh213_w40_0_d2, heap_bh213_w40_0_d3 :  std_logic;
signal heap_bh213_w39_0, heap_bh213_w39_0_d1, heap_bh213_w39_0_d2, heap_bh213_w39_0_d3 :  std_logic;
signal heap_bh213_w38_0, heap_bh213_w38_0_d1, heap_bh213_w38_0_d2, heap_bh213_w38_0_d3 :  std_logic;
signal heap_bh213_w37_0, heap_bh213_w37_0_d1 :  std_logic;
signal heap_bh213_w36_0, heap_bh213_w36_0_d1 :  std_logic;
signal heap_bh213_w35_0, heap_bh213_w35_0_d1 :  std_logic;
signal heap_bh213_w34_0, heap_bh213_w34_0_d1 :  std_logic;
signal heap_bh213_w33_0, heap_bh213_w33_0_d1 :  std_logic;
signal heap_bh213_w32_0, heap_bh213_w32_0_d1 :  std_logic;
signal heap_bh213_w31_0, heap_bh213_w31_0_d1 :  std_logic;
signal heap_bh213_w30_0, heap_bh213_w30_0_d1 :  std_logic;
signal heap_bh213_w29_0, heap_bh213_w29_0_d1, heap_bh213_w29_0_d2 :  std_logic;
signal heap_bh213_w28_0, heap_bh213_w28_0_d1 :  std_logic;
signal heap_bh213_w27_0, heap_bh213_w27_0_d1, heap_bh213_w27_0_d2 :  std_logic;
signal heap_bh213_w26_0, heap_bh213_w26_0_d1 :  std_logic;
signal heap_bh213_w25_0, heap_bh213_w25_0_d1 :  std_logic;
signal heap_bh213_w24_0, heap_bh213_w24_0_d1 :  std_logic;
signal heap_bh213_w23_0, heap_bh213_w23_0_d1 :  std_logic;
signal heap_bh213_w22_0, heap_bh213_w22_0_d1 :  std_logic;
signal heap_bh213_w21_0, heap_bh213_w21_0_d1 :  std_logic;
signal heap_bh213_w20_0, heap_bh213_w20_0_d1 :  std_logic;
signal heap_bh213_w19_0, heap_bh213_w19_0_d1 :  std_logic;
signal heap_bh213_w18_0, heap_bh213_w18_0_d1 :  std_logic;
signal heap_bh213_w17_0, heap_bh213_w17_0_d1 :  std_logic;
signal heap_bh213_w16_0, heap_bh213_w16_0_d1 :  std_logic;
signal heap_bh213_w15_0, heap_bh213_w15_0_d1 :  std_logic;
signal heap_bh213_w14_0, heap_bh213_w14_0_d1 :  std_logic;
signal DSP_bh213_ch1_0 :  std_logic_vector(40 downto 0);
signal heap_bh213_w30_1, heap_bh213_w30_1_d1 :  std_logic;
signal heap_bh213_w29_1, heap_bh213_w29_1_d1 :  std_logic;
signal heap_bh213_w28_1, heap_bh213_w28_1_d1 :  std_logic;
signal heap_bh213_w27_1, heap_bh213_w27_1_d1 :  std_logic;
signal heap_bh213_w26_1, heap_bh213_w26_1_d1 :  std_logic;
signal heap_bh213_w25_1, heap_bh213_w25_1_d1 :  std_logic;
signal heap_bh213_w24_1, heap_bh213_w24_1_d1 :  std_logic;
signal heap_bh213_w23_1, heap_bh213_w23_1_d1 :  std_logic;
signal heap_bh213_w22_1, heap_bh213_w22_1_d1 :  std_logic;
signal heap_bh213_w21_1, heap_bh213_w21_1_d1 :  std_logic;
signal heap_bh213_w20_1, heap_bh213_w20_1_d1 :  std_logic;
signal heap_bh213_w19_1, heap_bh213_w19_1_d1 :  std_logic;
signal heap_bh213_w18_1, heap_bh213_w18_1_d1 :  std_logic;
signal heap_bh213_w17_1, heap_bh213_w17_1_d1 :  std_logic;
signal heap_bh213_w16_1, heap_bh213_w16_1_d1 :  std_logic;
signal heap_bh213_w15_1, heap_bh213_w15_1_d1 :  std_logic;
signal heap_bh213_w14_1, heap_bh213_w14_1_d1 :  std_logic;
signal heap_bh213_w13_0, heap_bh213_w13_0_d1 :  std_logic;
signal heap_bh213_w12_0, heap_bh213_w12_0_d1 :  std_logic;
signal heap_bh213_w11_0, heap_bh213_w11_0_d1 :  std_logic;
signal heap_bh213_w10_0, heap_bh213_w10_0_d1 :  std_logic;
signal heap_bh213_w9_0, heap_bh213_w9_0_d1 :  std_logic;
signal heap_bh213_w8_0, heap_bh213_w8_0_d1 :  std_logic;
signal heap_bh213_w7_0, heap_bh213_w7_0_d1 :  std_logic;
signal heap_bh213_w6_0, heap_bh213_w6_0_d1 :  std_logic;
signal heap_bh213_w5_0, heap_bh213_w5_0_d1 :  std_logic;
signal heap_bh213_w4_0, heap_bh213_w4_0_d1 :  std_logic;
signal heap_bh213_w3_0, heap_bh213_w3_0_d1 :  std_logic;
signal heap_bh213_w2_0, heap_bh213_w2_0_d1 :  std_logic;
signal heap_bh213_w1_0, heap_bh213_w1_0_d1 :  std_logic;
signal heap_bh213_w0_0, heap_bh213_w0_0_d1 :  std_logic;
signal DSP_bh213_ch2_0 :  std_logic_vector(40 downto 0);
signal heap_bh213_w37_1, heap_bh213_w37_1_d1 :  std_logic;
signal heap_bh213_w36_1, heap_bh213_w36_1_d1 :  std_logic;
signal heap_bh213_w35_1, heap_bh213_w35_1_d1 :  std_logic;
signal heap_bh213_w34_1, heap_bh213_w34_1_d1 :  std_logic;
signal heap_bh213_w33_1, heap_bh213_w33_1_d1 :  std_logic;
signal heap_bh213_w32_1, heap_bh213_w32_1_d1 :  std_logic;
signal heap_bh213_w31_1, heap_bh213_w31_1_d1 :  std_logic;
signal heap_bh213_w30_2, heap_bh213_w30_2_d1 :  std_logic;
signal heap_bh213_w29_2, heap_bh213_w29_2_d1 :  std_logic;
signal heap_bh213_w28_2, heap_bh213_w28_2_d1 :  std_logic;
signal heap_bh213_w27_2, heap_bh213_w27_2_d1 :  std_logic;
signal heap_bh213_w26_2, heap_bh213_w26_2_d1 :  std_logic;
signal heap_bh213_w25_2, heap_bh213_w25_2_d1 :  std_logic;
signal heap_bh213_w24_2, heap_bh213_w24_2_d1 :  std_logic;
signal heap_bh213_w23_2, heap_bh213_w23_2_d1 :  std_logic;
signal heap_bh213_w22_2, heap_bh213_w22_2_d1 :  std_logic;
signal heap_bh213_w21_2, heap_bh213_w21_2_d1 :  std_logic;
signal heap_bh213_w20_2, heap_bh213_w20_2_d1 :  std_logic;
signal heap_bh213_w19_2, heap_bh213_w19_2_d1 :  std_logic;
signal heap_bh213_w18_2, heap_bh213_w18_2_d1 :  std_logic;
signal heap_bh213_w17_2, heap_bh213_w17_2_d1 :  std_logic;
signal heap_bh213_w16_2, heap_bh213_w16_2_d1 :  std_logic;
signal heap_bh213_w15_2, heap_bh213_w15_2_d1 :  std_logic;
signal heap_bh213_w14_2, heap_bh213_w14_2_d1 :  std_logic;
signal heap_bh213_w13_1, heap_bh213_w13_1_d1 :  std_logic;
signal heap_bh213_w12_1, heap_bh213_w12_1_d1 :  std_logic;
signal heap_bh213_w11_1, heap_bh213_w11_1_d1 :  std_logic;
signal heap_bh213_w10_1, heap_bh213_w10_1_d1 :  std_logic;
signal heap_bh213_w9_1, heap_bh213_w9_1_d1 :  std_logic;
signal heap_bh213_w8_1, heap_bh213_w8_1_d1 :  std_logic;
signal heap_bh213_w7_1, heap_bh213_w7_1_d1 :  std_logic;
signal heap_bh213_w6_1, heap_bh213_w6_1_d1 :  std_logic;
signal heap_bh213_w5_1, heap_bh213_w5_1_d1 :  std_logic;
signal heap_bh213_w4_1, heap_bh213_w4_1_d1 :  std_logic;
signal heap_bh213_w3_1, heap_bh213_w3_1_d1 :  std_logic;
signal heap_bh213_w2_1, heap_bh213_w2_1_d1 :  std_logic;
signal heap_bh213_w1_1, heap_bh213_w1_1_d1 :  std_logic;
signal heap_bh213_w0_1, heap_bh213_w0_1_d1 :  std_logic;
signal DSP_bh213_ch3_0 :  std_logic_vector(40 downto 0);
signal heap_bh213_w13_2, heap_bh213_w13_2_d1 :  std_logic;
signal heap_bh213_w12_2, heap_bh213_w12_2_d1 :  std_logic;
signal heap_bh213_w11_2, heap_bh213_w11_2_d1 :  std_logic;
signal heap_bh213_w10_2, heap_bh213_w10_2_d1 :  std_logic;
signal heap_bh213_w9_2, heap_bh213_w9_2_d1 :  std_logic;
signal heap_bh213_w8_2, heap_bh213_w8_2_d1 :  std_logic;
signal heap_bh213_w7_2, heap_bh213_w7_2_d1 :  std_logic;
signal heap_bh213_w6_2, heap_bh213_w6_2_d1 :  std_logic;
signal heap_bh213_w5_2, heap_bh213_w5_2_d1 :  std_logic;
signal heap_bh213_w4_2, heap_bh213_w4_2_d1 :  std_logic;
signal heap_bh213_w3_2, heap_bh213_w3_2_d1 :  std_logic;
signal heap_bh213_w2_2, heap_bh213_w2_2_d1 :  std_logic;
signal heap_bh213_w1_2, heap_bh213_w1_2_d1 :  std_logic;
signal heap_bh213_w0_2, heap_bh213_w0_2_d1 :  std_logic;
signal DSP_bh213_ch4_0 :  std_logic_vector(40 downto 0);
signal heap_bh213_w20_3, heap_bh213_w20_3_d1 :  std_logic;
signal heap_bh213_w19_3, heap_bh213_w19_3_d1 :  std_logic;
signal heap_bh213_w18_3, heap_bh213_w18_3_d1 :  std_logic;
signal heap_bh213_w17_3, heap_bh213_w17_3_d1 :  std_logic;
signal heap_bh213_w16_3, heap_bh213_w16_3_d1 :  std_logic;
signal heap_bh213_w15_3, heap_bh213_w15_3_d1 :  std_logic;
signal heap_bh213_w14_3, heap_bh213_w14_3_d1 :  std_logic;
signal heap_bh213_w13_3, heap_bh213_w13_3_d1 :  std_logic;
signal heap_bh213_w12_3, heap_bh213_w12_3_d1 :  std_logic;
signal heap_bh213_w11_3, heap_bh213_w11_3_d1 :  std_logic;
signal heap_bh213_w10_3, heap_bh213_w10_3_d1 :  std_logic;
signal heap_bh213_w9_3, heap_bh213_w9_3_d1 :  std_logic;
signal heap_bh213_w8_3, heap_bh213_w8_3_d1 :  std_logic;
signal heap_bh213_w7_3, heap_bh213_w7_3_d1 :  std_logic;
signal heap_bh213_w6_3, heap_bh213_w6_3_d1 :  std_logic;
signal heap_bh213_w5_3, heap_bh213_w5_3_d1 :  std_logic;
signal heap_bh213_w4_3, heap_bh213_w4_3_d1 :  std_logic;
signal heap_bh213_w3_3, heap_bh213_w3_3_d1 :  std_logic;
signal heap_bh213_w2_3, heap_bh213_w2_3_d1 :  std_logic;
signal heap_bh213_w1_3, heap_bh213_w1_3_d1 :  std_logic;
signal heap_bh213_w0_3, heap_bh213_w0_3_d1 :  std_logic;
signal heap_bh213_w6_4, heap_bh213_w6_4_d1 :  std_logic;
signal CompressorIn_bh213_0_0 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh213_0_1 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh213_0_0 :  std_logic_vector(2 downto 0);
signal heap_bh213_w0_4 :  std_logic;
signal heap_bh213_w1_4, heap_bh213_w1_4_d1, heap_bh213_w1_4_d2 :  std_logic;
signal heap_bh213_w2_4 :  std_logic;
signal CompressorIn_bh213_1_2 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh213_1_3 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh213_1_1 :  std_logic_vector(2 downto 0);
signal heap_bh213_w2_5 :  std_logic;
signal heap_bh213_w3_4 :  std_logic;
signal heap_bh213_w4_4 :  std_logic;
signal CompressorIn_bh213_2_4 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh213_2_5 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh213_2_2 :  std_logic_vector(2 downto 0);
signal heap_bh213_w4_5 :  std_logic;
signal heap_bh213_w5_4 :  std_logic;
signal heap_bh213_w6_5 :  std_logic;
signal CompressorIn_bh213_3_6 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh213_3_7 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh213_3_3 :  std_logic_vector(2 downto 0);
signal heap_bh213_w6_6 :  std_logic;
signal heap_bh213_w7_4, heap_bh213_w7_4_d1, heap_bh213_w7_4_d2 :  std_logic;
signal heap_bh213_w8_4 :  std_logic;
signal CompressorIn_bh213_4_8 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh213_4_9 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh213_4_4 :  std_logic_vector(2 downto 0);
signal heap_bh213_w8_5 :  std_logic;
signal heap_bh213_w9_4 :  std_logic;
signal heap_bh213_w10_4 :  std_logic;
signal CompressorIn_bh213_5_10 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh213_5_11 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh213_5_5 :  std_logic_vector(2 downto 0);
signal heap_bh213_w10_5 :  std_logic;
signal heap_bh213_w11_4 :  std_logic;
signal heap_bh213_w12_4 :  std_logic;
signal CompressorIn_bh213_6_12 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh213_6_13 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh213_6_6 :  std_logic_vector(2 downto 0);
signal heap_bh213_w12_5 :  std_logic;
signal heap_bh213_w13_4 :  std_logic;
signal heap_bh213_w14_4 :  std_logic;
signal CompressorIn_bh213_7_14 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh213_7_15 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh213_7_7 :  std_logic_vector(2 downto 0);
signal heap_bh213_w14_5 :  std_logic;
signal heap_bh213_w15_4 :  std_logic;
signal heap_bh213_w16_4 :  std_logic;
signal CompressorIn_bh213_8_16 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh213_8_17 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh213_8_8 :  std_logic_vector(2 downto 0);
signal heap_bh213_w16_5 :  std_logic;
signal heap_bh213_w17_4 :  std_logic;
signal heap_bh213_w18_4 :  std_logic;
signal CompressorIn_bh213_9_18 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh213_9_19 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh213_9_9 :  std_logic_vector(2 downto 0);
signal heap_bh213_w18_5 :  std_logic;
signal heap_bh213_w19_4 :  std_logic;
signal heap_bh213_w20_4 :  std_logic;
signal CompressorIn_bh213_10_20 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh213_10_21 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh213_10_10 :  std_logic_vector(2 downto 0);
signal heap_bh213_w20_5 :  std_logic;
signal heap_bh213_w21_3, heap_bh213_w21_3_d1, heap_bh213_w21_3_d2 :  std_logic;
signal heap_bh213_w22_3 :  std_logic;
signal CompressorIn_bh213_11_22 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_11_23 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_11_11 :  std_logic_vector(2 downto 0);
signal heap_bh213_w22_4 :  std_logic;
signal heap_bh213_w23_3 :  std_logic;
signal heap_bh213_w24_3 :  std_logic;
signal CompressorIn_bh213_12_24 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_12_25 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_12_12 :  std_logic_vector(2 downto 0);
signal heap_bh213_w24_4 :  std_logic;
signal heap_bh213_w25_3 :  std_logic;
signal heap_bh213_w26_3, heap_bh213_w26_3_d1 :  std_logic;
signal CompressorIn_bh213_13_26 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_13_27 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_13_13 :  std_logic_vector(2 downto 0);
signal heap_bh213_w26_4, heap_bh213_w26_4_d1 :  std_logic;
signal heap_bh213_w27_3, heap_bh213_w27_3_d1 :  std_logic;
signal heap_bh213_w28_3, heap_bh213_w28_3_d1 :  std_logic;
signal CompressorIn_bh213_14_28 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_14_29 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_14_14 :  std_logic_vector(2 downto 0);
signal heap_bh213_w28_4, heap_bh213_w28_4_d1 :  std_logic;
signal heap_bh213_w29_3, heap_bh213_w29_3_d1 :  std_logic;
signal heap_bh213_w30_3, heap_bh213_w30_3_d1 :  std_logic;
signal CompressorIn_bh213_15_30 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_15_31 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_15_15 :  std_logic_vector(2 downto 0);
signal heap_bh213_w30_4, heap_bh213_w30_4_d1 :  std_logic;
signal heap_bh213_w31_2, heap_bh213_w31_2_d1 :  std_logic;
signal heap_bh213_w32_2 :  std_logic;
signal CompressorIn_bh213_16_32 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_16_33 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh213_16_16 :  std_logic_vector(2 downto 0);
signal heap_bh213_w5_5 :  std_logic;
signal heap_bh213_w6_7 :  std_logic;
signal heap_bh213_w7_5 :  std_logic;
signal CompressorIn_bh213_17_34 :  std_logic_vector(2 downto 0);
signal CompressorOut_bh213_17_17 :  std_logic_vector(1 downto 0);
signal heap_bh213_w1_5, heap_bh213_w1_5_d1, heap_bh213_w1_5_d2 :  std_logic;
signal heap_bh213_w2_6 :  std_logic;
signal CompressorIn_bh213_18_35 :  std_logic_vector(2 downto 0);
signal CompressorOut_bh213_18_18 :  std_logic_vector(1 downto 0);
signal heap_bh213_w3_5 :  std_logic;
signal heap_bh213_w4_6 :  std_logic;
signal CompressorIn_bh213_19_36 :  std_logic_vector(2 downto 0);
signal CompressorOut_bh213_19_19 :  std_logic_vector(1 downto 0);
signal heap_bh213_w7_6 :  std_logic;
signal heap_bh213_w8_6 :  std_logic;
signal CompressorIn_bh213_20_37 :  std_logic_vector(2 downto 0);
signal CompressorOut_bh213_20_20 :  std_logic_vector(1 downto 0);
signal heap_bh213_w9_5 :  std_logic;
signal heap_bh213_w10_6 :  std_logic;
signal CompressorIn_bh213_21_38 :  std_logic_vector(2 downto 0);
signal CompressorOut_bh213_21_21 :  std_logic_vector(1 downto 0);
signal heap_bh213_w11_5 :  std_logic;
signal heap_bh213_w12_6 :  std_logic;
signal CompressorIn_bh213_22_39 :  std_logic_vector(2 downto 0);
signal CompressorOut_bh213_22_22 :  std_logic_vector(1 downto 0);
signal heap_bh213_w13_5 :  std_logic;
signal heap_bh213_w14_6 :  std_logic;
signal CompressorIn_bh213_23_40 :  std_logic_vector(2 downto 0);
signal CompressorOut_bh213_23_23 :  std_logic_vector(1 downto 0);
signal heap_bh213_w15_5 :  std_logic;
signal heap_bh213_w16_6 :  std_logic;
signal CompressorIn_bh213_24_41 :  std_logic_vector(2 downto 0);
signal CompressorOut_bh213_24_24 :  std_logic_vector(1 downto 0);
signal heap_bh213_w17_5 :  std_logic;
signal heap_bh213_w18_6 :  std_logic;
signal CompressorIn_bh213_25_42 :  std_logic_vector(2 downto 0);
signal CompressorOut_bh213_25_25 :  std_logic_vector(1 downto 0);
signal heap_bh213_w19_5 :  std_logic;
signal heap_bh213_w20_6 :  std_logic;
signal tempR_bh213_0, tempR_bh213_0_d1, tempR_bh213_0_d2, tempR_bh213_0_d3 :  std_logic;
signal CompressorIn_bh213_26_43 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_26_44 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_26_26 :  std_logic_vector(2 downto 0);
signal heap_bh213_w2_7, heap_bh213_w2_7_d1, heap_bh213_w2_7_d2 :  std_logic;
signal heap_bh213_w3_6, heap_bh213_w3_6_d1, heap_bh213_w3_6_d2 :  std_logic;
signal heap_bh213_w4_7, heap_bh213_w4_7_d1, heap_bh213_w4_7_d2 :  std_logic;
signal CompressorIn_bh213_27_45 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_27_46 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_27_27 :  std_logic_vector(2 downto 0);
signal heap_bh213_w4_8, heap_bh213_w4_8_d1, heap_bh213_w4_8_d2 :  std_logic;
signal heap_bh213_w5_6, heap_bh213_w5_6_d1, heap_bh213_w5_6_d2 :  std_logic;
signal heap_bh213_w6_8, heap_bh213_w6_8_d1, heap_bh213_w6_8_d2 :  std_logic;
signal CompressorIn_bh213_28_47 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_28_48 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_28_28 :  std_logic_vector(2 downto 0);
signal heap_bh213_w6_9, heap_bh213_w6_9_d1, heap_bh213_w6_9_d2 :  std_logic;
signal heap_bh213_w7_7, heap_bh213_w7_7_d1, heap_bh213_w7_7_d2 :  std_logic;
signal heap_bh213_w8_7, heap_bh213_w8_7_d1, heap_bh213_w8_7_d2 :  std_logic;
signal CompressorIn_bh213_29_49 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_29_50 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_29_29 :  std_logic_vector(2 downto 0);
signal heap_bh213_w8_8, heap_bh213_w8_8_d1, heap_bh213_w8_8_d2 :  std_logic;
signal heap_bh213_w9_6, heap_bh213_w9_6_d1, heap_bh213_w9_6_d2 :  std_logic;
signal heap_bh213_w10_7, heap_bh213_w10_7_d1, heap_bh213_w10_7_d2 :  std_logic;
signal CompressorIn_bh213_30_51 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_30_52 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_30_30 :  std_logic_vector(2 downto 0);
signal heap_bh213_w10_8, heap_bh213_w10_8_d1, heap_bh213_w10_8_d2 :  std_logic;
signal heap_bh213_w11_6, heap_bh213_w11_6_d1, heap_bh213_w11_6_d2 :  std_logic;
signal heap_bh213_w12_7, heap_bh213_w12_7_d1, heap_bh213_w12_7_d2 :  std_logic;
signal CompressorIn_bh213_31_53 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_31_54 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_31_31 :  std_logic_vector(2 downto 0);
signal heap_bh213_w12_8, heap_bh213_w12_8_d1, heap_bh213_w12_8_d2 :  std_logic;
signal heap_bh213_w13_6, heap_bh213_w13_6_d1, heap_bh213_w13_6_d2 :  std_logic;
signal heap_bh213_w14_7, heap_bh213_w14_7_d1, heap_bh213_w14_7_d2 :  std_logic;
signal CompressorIn_bh213_32_55 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_32_56 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_32_32 :  std_logic_vector(2 downto 0);
signal heap_bh213_w14_8, heap_bh213_w14_8_d1, heap_bh213_w14_8_d2 :  std_logic;
signal heap_bh213_w15_6, heap_bh213_w15_6_d1, heap_bh213_w15_6_d2 :  std_logic;
signal heap_bh213_w16_7, heap_bh213_w16_7_d1, heap_bh213_w16_7_d2 :  std_logic;
signal CompressorIn_bh213_33_57 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_33_58 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_33_33 :  std_logic_vector(2 downto 0);
signal heap_bh213_w16_8, heap_bh213_w16_8_d1, heap_bh213_w16_8_d2 :  std_logic;
signal heap_bh213_w17_6, heap_bh213_w17_6_d1, heap_bh213_w17_6_d2 :  std_logic;
signal heap_bh213_w18_7, heap_bh213_w18_7_d1, heap_bh213_w18_7_d2 :  std_logic;
signal CompressorIn_bh213_34_59 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_34_60 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_34_34 :  std_logic_vector(2 downto 0);
signal heap_bh213_w18_8, heap_bh213_w18_8_d1, heap_bh213_w18_8_d2 :  std_logic;
signal heap_bh213_w19_6, heap_bh213_w19_6_d1, heap_bh213_w19_6_d2 :  std_logic;
signal heap_bh213_w20_7, heap_bh213_w20_7_d1, heap_bh213_w20_7_d2 :  std_logic;
signal CompressorIn_bh213_35_61 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_35_62 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_35_35 :  std_logic_vector(2 downto 0);
signal heap_bh213_w20_8, heap_bh213_w20_8_d1, heap_bh213_w20_8_d2 :  std_logic;
signal heap_bh213_w21_4, heap_bh213_w21_4_d1, heap_bh213_w21_4_d2 :  std_logic;
signal heap_bh213_w22_5 :  std_logic;
signal CompressorIn_bh213_36_63 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_36_64 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_36_36 :  std_logic_vector(2 downto 0);
signal heap_bh213_w32_3, heap_bh213_w32_3_d1, heap_bh213_w32_3_d2 :  std_logic;
signal heap_bh213_w33_2, heap_bh213_w33_2_d1, heap_bh213_w33_2_d2 :  std_logic;
signal heap_bh213_w34_2 :  std_logic;
signal CompressorIn_bh213_37_65 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_37_66 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_37_37 :  std_logic_vector(2 downto 0);
signal heap_bh213_w22_6, heap_bh213_w22_6_d1, heap_bh213_w22_6_d2 :  std_logic;
signal heap_bh213_w23_4, heap_bh213_w23_4_d1, heap_bh213_w23_4_d2 :  std_logic;
signal heap_bh213_w24_5 :  std_logic;
signal CompressorIn_bh213_38_67 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_38_68 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_38_38 :  std_logic_vector(2 downto 0);
signal heap_bh213_w34_3, heap_bh213_w34_3_d1, heap_bh213_w34_3_d2 :  std_logic;
signal heap_bh213_w35_2, heap_bh213_w35_2_d1, heap_bh213_w35_2_d2 :  std_logic;
signal heap_bh213_w36_2 :  std_logic;
signal CompressorIn_bh213_39_69 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_39_70 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_39_39 :  std_logic_vector(2 downto 0);
signal heap_bh213_w24_6, heap_bh213_w24_6_d1, heap_bh213_w24_6_d2 :  std_logic;
signal heap_bh213_w25_4, heap_bh213_w25_4_d1, heap_bh213_w25_4_d2 :  std_logic;
signal heap_bh213_w26_5, heap_bh213_w26_5_d1 :  std_logic;
signal CompressorIn_bh213_40_71 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_40_72 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_40_40 :  std_logic_vector(2 downto 0);
signal heap_bh213_w36_3, heap_bh213_w36_3_d1, heap_bh213_w36_3_d2 :  std_logic;
signal heap_bh213_w37_2, heap_bh213_w37_2_d1, heap_bh213_w37_2_d2 :  std_logic;
signal heap_bh213_w38_1, heap_bh213_w38_1_d1, heap_bh213_w38_1_d2 :  std_logic;
signal CompressorIn_bh213_41_73 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_41_74 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_41_41 :  std_logic_vector(2 downto 0);
signal heap_bh213_w26_6, heap_bh213_w26_6_d1 :  std_logic;
signal heap_bh213_w27_4, heap_bh213_w27_4_d1 :  std_logic;
signal heap_bh213_w28_5 :  std_logic;
signal CompressorIn_bh213_42_75 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_42_76 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh213_42_42 :  std_logic_vector(2 downto 0);
signal heap_bh213_w28_6, heap_bh213_w28_6_d1 :  std_logic;
signal heap_bh213_w29_4, heap_bh213_w29_4_d1 :  std_logic;
signal heap_bh213_w30_5 :  std_logic;
signal CompressorIn_bh213_43_77 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh213_43_78 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh213_43_43 :  std_logic_vector(2 downto 0);
signal heap_bh213_w30_6, heap_bh213_w30_6_d1 :  std_logic;
signal heap_bh213_w31_3, heap_bh213_w31_3_d1 :  std_logic;
signal heap_bh213_w32_4, heap_bh213_w32_4_d1 :  std_logic;
signal finalAdderIn0_bh213 :  std_logic_vector(54 downto 0);
signal finalAdderIn1_bh213 :  std_logic_vector(54 downto 0);
signal finalAdderCin_bh213 :  std_logic;
signal finalAdderOut_bh213 :  std_logic_vector(54 downto 0);
signal CompressionResult213 :  std_logic_vector(55 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            heap_bh213_w54_0_d1 <=  heap_bh213_w54_0;
            heap_bh213_w54_0_d2 <=  heap_bh213_w54_0_d1;
            heap_bh213_w54_0_d3 <=  heap_bh213_w54_0_d2;
            heap_bh213_w53_0_d1 <=  heap_bh213_w53_0;
            heap_bh213_w53_0_d2 <=  heap_bh213_w53_0_d1;
            heap_bh213_w53_0_d3 <=  heap_bh213_w53_0_d2;
            heap_bh213_w52_0_d1 <=  heap_bh213_w52_0;
            heap_bh213_w52_0_d2 <=  heap_bh213_w52_0_d1;
            heap_bh213_w52_0_d3 <=  heap_bh213_w52_0_d2;
            heap_bh213_w51_0_d1 <=  heap_bh213_w51_0;
            heap_bh213_w51_0_d2 <=  heap_bh213_w51_0_d1;
            heap_bh213_w51_0_d3 <=  heap_bh213_w51_0_d2;
            heap_bh213_w50_0_d1 <=  heap_bh213_w50_0;
            heap_bh213_w50_0_d2 <=  heap_bh213_w50_0_d1;
            heap_bh213_w50_0_d3 <=  heap_bh213_w50_0_d2;
            heap_bh213_w49_0_d1 <=  heap_bh213_w49_0;
            heap_bh213_w49_0_d2 <=  heap_bh213_w49_0_d1;
            heap_bh213_w49_0_d3 <=  heap_bh213_w49_0_d2;
            heap_bh213_w48_0_d1 <=  heap_bh213_w48_0;
            heap_bh213_w48_0_d2 <=  heap_bh213_w48_0_d1;
            heap_bh213_w48_0_d3 <=  heap_bh213_w48_0_d2;
            heap_bh213_w47_0_d1 <=  heap_bh213_w47_0;
            heap_bh213_w47_0_d2 <=  heap_bh213_w47_0_d1;
            heap_bh213_w47_0_d3 <=  heap_bh213_w47_0_d2;
            heap_bh213_w46_0_d1 <=  heap_bh213_w46_0;
            heap_bh213_w46_0_d2 <=  heap_bh213_w46_0_d1;
            heap_bh213_w46_0_d3 <=  heap_bh213_w46_0_d2;
            heap_bh213_w45_0_d1 <=  heap_bh213_w45_0;
            heap_bh213_w45_0_d2 <=  heap_bh213_w45_0_d1;
            heap_bh213_w45_0_d3 <=  heap_bh213_w45_0_d2;
            heap_bh213_w44_0_d1 <=  heap_bh213_w44_0;
            heap_bh213_w44_0_d2 <=  heap_bh213_w44_0_d1;
            heap_bh213_w44_0_d3 <=  heap_bh213_w44_0_d2;
            heap_bh213_w43_0_d1 <=  heap_bh213_w43_0;
            heap_bh213_w43_0_d2 <=  heap_bh213_w43_0_d1;
            heap_bh213_w43_0_d3 <=  heap_bh213_w43_0_d2;
            heap_bh213_w42_0_d1 <=  heap_bh213_w42_0;
            heap_bh213_w42_0_d2 <=  heap_bh213_w42_0_d1;
            heap_bh213_w42_0_d3 <=  heap_bh213_w42_0_d2;
            heap_bh213_w41_0_d1 <=  heap_bh213_w41_0;
            heap_bh213_w41_0_d2 <=  heap_bh213_w41_0_d1;
            heap_bh213_w41_0_d3 <=  heap_bh213_w41_0_d2;
            heap_bh213_w40_0_d1 <=  heap_bh213_w40_0;
            heap_bh213_w40_0_d2 <=  heap_bh213_w40_0_d1;
            heap_bh213_w40_0_d3 <=  heap_bh213_w40_0_d2;
            heap_bh213_w39_0_d1 <=  heap_bh213_w39_0;
            heap_bh213_w39_0_d2 <=  heap_bh213_w39_0_d1;
            heap_bh213_w39_0_d3 <=  heap_bh213_w39_0_d2;
            heap_bh213_w38_0_d1 <=  heap_bh213_w38_0;
            heap_bh213_w38_0_d2 <=  heap_bh213_w38_0_d1;
            heap_bh213_w38_0_d3 <=  heap_bh213_w38_0_d2;
            heap_bh213_w37_0_d1 <=  heap_bh213_w37_0;
            heap_bh213_w36_0_d1 <=  heap_bh213_w36_0;
            heap_bh213_w35_0_d1 <=  heap_bh213_w35_0;
            heap_bh213_w34_0_d1 <=  heap_bh213_w34_0;
            heap_bh213_w33_0_d1 <=  heap_bh213_w33_0;
            heap_bh213_w32_0_d1 <=  heap_bh213_w32_0;
            heap_bh213_w31_0_d1 <=  heap_bh213_w31_0;
            heap_bh213_w30_0_d1 <=  heap_bh213_w30_0;
            heap_bh213_w29_0_d1 <=  heap_bh213_w29_0;
            heap_bh213_w29_0_d2 <=  heap_bh213_w29_0_d1;
            heap_bh213_w28_0_d1 <=  heap_bh213_w28_0;
            heap_bh213_w27_0_d1 <=  heap_bh213_w27_0;
            heap_bh213_w27_0_d2 <=  heap_bh213_w27_0_d1;
            heap_bh213_w26_0_d1 <=  heap_bh213_w26_0;
            heap_bh213_w25_0_d1 <=  heap_bh213_w25_0;
            heap_bh213_w24_0_d1 <=  heap_bh213_w24_0;
            heap_bh213_w23_0_d1 <=  heap_bh213_w23_0;
            heap_bh213_w22_0_d1 <=  heap_bh213_w22_0;
            heap_bh213_w21_0_d1 <=  heap_bh213_w21_0;
            heap_bh213_w20_0_d1 <=  heap_bh213_w20_0;
            heap_bh213_w19_0_d1 <=  heap_bh213_w19_0;
            heap_bh213_w18_0_d1 <=  heap_bh213_w18_0;
            heap_bh213_w17_0_d1 <=  heap_bh213_w17_0;
            heap_bh213_w16_0_d1 <=  heap_bh213_w16_0;
            heap_bh213_w15_0_d1 <=  heap_bh213_w15_0;
            heap_bh213_w14_0_d1 <=  heap_bh213_w14_0;
            heap_bh213_w30_1_d1 <=  heap_bh213_w30_1;
            heap_bh213_w29_1_d1 <=  heap_bh213_w29_1;
            heap_bh213_w28_1_d1 <=  heap_bh213_w28_1;
            heap_bh213_w27_1_d1 <=  heap_bh213_w27_1;
            heap_bh213_w26_1_d1 <=  heap_bh213_w26_1;
            heap_bh213_w25_1_d1 <=  heap_bh213_w25_1;
            heap_bh213_w24_1_d1 <=  heap_bh213_w24_1;
            heap_bh213_w23_1_d1 <=  heap_bh213_w23_1;
            heap_bh213_w22_1_d1 <=  heap_bh213_w22_1;
            heap_bh213_w21_1_d1 <=  heap_bh213_w21_1;
            heap_bh213_w20_1_d1 <=  heap_bh213_w20_1;
            heap_bh213_w19_1_d1 <=  heap_bh213_w19_1;
            heap_bh213_w18_1_d1 <=  heap_bh213_w18_1;
            heap_bh213_w17_1_d1 <=  heap_bh213_w17_1;
            heap_bh213_w16_1_d1 <=  heap_bh213_w16_1;
            heap_bh213_w15_1_d1 <=  heap_bh213_w15_1;
            heap_bh213_w14_1_d1 <=  heap_bh213_w14_1;
            heap_bh213_w13_0_d1 <=  heap_bh213_w13_0;
            heap_bh213_w12_0_d1 <=  heap_bh213_w12_0;
            heap_bh213_w11_0_d1 <=  heap_bh213_w11_0;
            heap_bh213_w10_0_d1 <=  heap_bh213_w10_0;
            heap_bh213_w9_0_d1 <=  heap_bh213_w9_0;
            heap_bh213_w8_0_d1 <=  heap_bh213_w8_0;
            heap_bh213_w7_0_d1 <=  heap_bh213_w7_0;
            heap_bh213_w6_0_d1 <=  heap_bh213_w6_0;
            heap_bh213_w5_0_d1 <=  heap_bh213_w5_0;
            heap_bh213_w4_0_d1 <=  heap_bh213_w4_0;
            heap_bh213_w3_0_d1 <=  heap_bh213_w3_0;
            heap_bh213_w2_0_d1 <=  heap_bh213_w2_0;
            heap_bh213_w1_0_d1 <=  heap_bh213_w1_0;
            heap_bh213_w0_0_d1 <=  heap_bh213_w0_0;
            heap_bh213_w37_1_d1 <=  heap_bh213_w37_1;
            heap_bh213_w36_1_d1 <=  heap_bh213_w36_1;
            heap_bh213_w35_1_d1 <=  heap_bh213_w35_1;
            heap_bh213_w34_1_d1 <=  heap_bh213_w34_1;
            heap_bh213_w33_1_d1 <=  heap_bh213_w33_1;
            heap_bh213_w32_1_d1 <=  heap_bh213_w32_1;
            heap_bh213_w31_1_d1 <=  heap_bh213_w31_1;
            heap_bh213_w30_2_d1 <=  heap_bh213_w30_2;
            heap_bh213_w29_2_d1 <=  heap_bh213_w29_2;
            heap_bh213_w28_2_d1 <=  heap_bh213_w28_2;
            heap_bh213_w27_2_d1 <=  heap_bh213_w27_2;
            heap_bh213_w26_2_d1 <=  heap_bh213_w26_2;
            heap_bh213_w25_2_d1 <=  heap_bh213_w25_2;
            heap_bh213_w24_2_d1 <=  heap_bh213_w24_2;
            heap_bh213_w23_2_d1 <=  heap_bh213_w23_2;
            heap_bh213_w22_2_d1 <=  heap_bh213_w22_2;
            heap_bh213_w21_2_d1 <=  heap_bh213_w21_2;
            heap_bh213_w20_2_d1 <=  heap_bh213_w20_2;
            heap_bh213_w19_2_d1 <=  heap_bh213_w19_2;
            heap_bh213_w18_2_d1 <=  heap_bh213_w18_2;
            heap_bh213_w17_2_d1 <=  heap_bh213_w17_2;
            heap_bh213_w16_2_d1 <=  heap_bh213_w16_2;
            heap_bh213_w15_2_d1 <=  heap_bh213_w15_2;
            heap_bh213_w14_2_d1 <=  heap_bh213_w14_2;
            heap_bh213_w13_1_d1 <=  heap_bh213_w13_1;
            heap_bh213_w12_1_d1 <=  heap_bh213_w12_1;
            heap_bh213_w11_1_d1 <=  heap_bh213_w11_1;
            heap_bh213_w10_1_d1 <=  heap_bh213_w10_1;
            heap_bh213_w9_1_d1 <=  heap_bh213_w9_1;
            heap_bh213_w8_1_d1 <=  heap_bh213_w8_1;
            heap_bh213_w7_1_d1 <=  heap_bh213_w7_1;
            heap_bh213_w6_1_d1 <=  heap_bh213_w6_1;
            heap_bh213_w5_1_d1 <=  heap_bh213_w5_1;
            heap_bh213_w4_1_d1 <=  heap_bh213_w4_1;
            heap_bh213_w3_1_d1 <=  heap_bh213_w3_1;
            heap_bh213_w2_1_d1 <=  heap_bh213_w2_1;
            heap_bh213_w1_1_d1 <=  heap_bh213_w1_1;
            heap_bh213_w0_1_d1 <=  heap_bh213_w0_1;
            heap_bh213_w13_2_d1 <=  heap_bh213_w13_2;
            heap_bh213_w12_2_d1 <=  heap_bh213_w12_2;
            heap_bh213_w11_2_d1 <=  heap_bh213_w11_2;
            heap_bh213_w10_2_d1 <=  heap_bh213_w10_2;
            heap_bh213_w9_2_d1 <=  heap_bh213_w9_2;
            heap_bh213_w8_2_d1 <=  heap_bh213_w8_2;
            heap_bh213_w7_2_d1 <=  heap_bh213_w7_2;
            heap_bh213_w6_2_d1 <=  heap_bh213_w6_2;
            heap_bh213_w5_2_d1 <=  heap_bh213_w5_2;
            heap_bh213_w4_2_d1 <=  heap_bh213_w4_2;
            heap_bh213_w3_2_d1 <=  heap_bh213_w3_2;
            heap_bh213_w2_2_d1 <=  heap_bh213_w2_2;
            heap_bh213_w1_2_d1 <=  heap_bh213_w1_2;
            heap_bh213_w0_2_d1 <=  heap_bh213_w0_2;
            heap_bh213_w20_3_d1 <=  heap_bh213_w20_3;
            heap_bh213_w19_3_d1 <=  heap_bh213_w19_3;
            heap_bh213_w18_3_d1 <=  heap_bh213_w18_3;
            heap_bh213_w17_3_d1 <=  heap_bh213_w17_3;
            heap_bh213_w16_3_d1 <=  heap_bh213_w16_3;
            heap_bh213_w15_3_d1 <=  heap_bh213_w15_3;
            heap_bh213_w14_3_d1 <=  heap_bh213_w14_3;
            heap_bh213_w13_3_d1 <=  heap_bh213_w13_3;
            heap_bh213_w12_3_d1 <=  heap_bh213_w12_3;
            heap_bh213_w11_3_d1 <=  heap_bh213_w11_3;
            heap_bh213_w10_3_d1 <=  heap_bh213_w10_3;
            heap_bh213_w9_3_d1 <=  heap_bh213_w9_3;
            heap_bh213_w8_3_d1 <=  heap_bh213_w8_3;
            heap_bh213_w7_3_d1 <=  heap_bh213_w7_3;
            heap_bh213_w6_3_d1 <=  heap_bh213_w6_3;
            heap_bh213_w5_3_d1 <=  heap_bh213_w5_3;
            heap_bh213_w4_3_d1 <=  heap_bh213_w4_3;
            heap_bh213_w3_3_d1 <=  heap_bh213_w3_3;
            heap_bh213_w2_3_d1 <=  heap_bh213_w2_3;
            heap_bh213_w1_3_d1 <=  heap_bh213_w1_3;
            heap_bh213_w0_3_d1 <=  heap_bh213_w0_3;
            heap_bh213_w6_4_d1 <=  heap_bh213_w6_4;
            heap_bh213_w1_4_d1 <=  heap_bh213_w1_4;
            heap_bh213_w1_4_d2 <=  heap_bh213_w1_4_d1;
            heap_bh213_w7_4_d1 <=  heap_bh213_w7_4;
            heap_bh213_w7_4_d2 <=  heap_bh213_w7_4_d1;
            heap_bh213_w21_3_d1 <=  heap_bh213_w21_3;
            heap_bh213_w21_3_d2 <=  heap_bh213_w21_3_d1;
            heap_bh213_w26_3_d1 <=  heap_bh213_w26_3;
            heap_bh213_w26_4_d1 <=  heap_bh213_w26_4;
            heap_bh213_w27_3_d1 <=  heap_bh213_w27_3;
            heap_bh213_w28_3_d1 <=  heap_bh213_w28_3;
            heap_bh213_w28_4_d1 <=  heap_bh213_w28_4;
            heap_bh213_w29_3_d1 <=  heap_bh213_w29_3;
            heap_bh213_w30_3_d1 <=  heap_bh213_w30_3;
            heap_bh213_w30_4_d1 <=  heap_bh213_w30_4;
            heap_bh213_w31_2_d1 <=  heap_bh213_w31_2;
            heap_bh213_w1_5_d1 <=  heap_bh213_w1_5;
            heap_bh213_w1_5_d2 <=  heap_bh213_w1_5_d1;
            tempR_bh213_0_d1 <=  tempR_bh213_0;
            tempR_bh213_0_d2 <=  tempR_bh213_0_d1;
            tempR_bh213_0_d3 <=  tempR_bh213_0_d2;
            heap_bh213_w2_7_d1 <=  heap_bh213_w2_7;
            heap_bh213_w2_7_d2 <=  heap_bh213_w2_7_d1;
            heap_bh213_w3_6_d1 <=  heap_bh213_w3_6;
            heap_bh213_w3_6_d2 <=  heap_bh213_w3_6_d1;
            heap_bh213_w4_7_d1 <=  heap_bh213_w4_7;
            heap_bh213_w4_7_d2 <=  heap_bh213_w4_7_d1;
            heap_bh213_w4_8_d1 <=  heap_bh213_w4_8;
            heap_bh213_w4_8_d2 <=  heap_bh213_w4_8_d1;
            heap_bh213_w5_6_d1 <=  heap_bh213_w5_6;
            heap_bh213_w5_6_d2 <=  heap_bh213_w5_6_d1;
            heap_bh213_w6_8_d1 <=  heap_bh213_w6_8;
            heap_bh213_w6_8_d2 <=  heap_bh213_w6_8_d1;
            heap_bh213_w6_9_d1 <=  heap_bh213_w6_9;
            heap_bh213_w6_9_d2 <=  heap_bh213_w6_9_d1;
            heap_bh213_w7_7_d1 <=  heap_bh213_w7_7;
            heap_bh213_w7_7_d2 <=  heap_bh213_w7_7_d1;
            heap_bh213_w8_7_d1 <=  heap_bh213_w8_7;
            heap_bh213_w8_7_d2 <=  heap_bh213_w8_7_d1;
            heap_bh213_w8_8_d1 <=  heap_bh213_w8_8;
            heap_bh213_w8_8_d2 <=  heap_bh213_w8_8_d1;
            heap_bh213_w9_6_d1 <=  heap_bh213_w9_6;
            heap_bh213_w9_6_d2 <=  heap_bh213_w9_6_d1;
            heap_bh213_w10_7_d1 <=  heap_bh213_w10_7;
            heap_bh213_w10_7_d2 <=  heap_bh213_w10_7_d1;
            heap_bh213_w10_8_d1 <=  heap_bh213_w10_8;
            heap_bh213_w10_8_d2 <=  heap_bh213_w10_8_d1;
            heap_bh213_w11_6_d1 <=  heap_bh213_w11_6;
            heap_bh213_w11_6_d2 <=  heap_bh213_w11_6_d1;
            heap_bh213_w12_7_d1 <=  heap_bh213_w12_7;
            heap_bh213_w12_7_d2 <=  heap_bh213_w12_7_d1;
            heap_bh213_w12_8_d1 <=  heap_bh213_w12_8;
            heap_bh213_w12_8_d2 <=  heap_bh213_w12_8_d1;
            heap_bh213_w13_6_d1 <=  heap_bh213_w13_6;
            heap_bh213_w13_6_d2 <=  heap_bh213_w13_6_d1;
            heap_bh213_w14_7_d1 <=  heap_bh213_w14_7;
            heap_bh213_w14_7_d2 <=  heap_bh213_w14_7_d1;
            heap_bh213_w14_8_d1 <=  heap_bh213_w14_8;
            heap_bh213_w14_8_d2 <=  heap_bh213_w14_8_d1;
            heap_bh213_w15_6_d1 <=  heap_bh213_w15_6;
            heap_bh213_w15_6_d2 <=  heap_bh213_w15_6_d1;
            heap_bh213_w16_7_d1 <=  heap_bh213_w16_7;
            heap_bh213_w16_7_d2 <=  heap_bh213_w16_7_d1;
            heap_bh213_w16_8_d1 <=  heap_bh213_w16_8;
            heap_bh213_w16_8_d2 <=  heap_bh213_w16_8_d1;
            heap_bh213_w17_6_d1 <=  heap_bh213_w17_6;
            heap_bh213_w17_6_d2 <=  heap_bh213_w17_6_d1;
            heap_bh213_w18_7_d1 <=  heap_bh213_w18_7;
            heap_bh213_w18_7_d2 <=  heap_bh213_w18_7_d1;
            heap_bh213_w18_8_d1 <=  heap_bh213_w18_8;
            heap_bh213_w18_8_d2 <=  heap_bh213_w18_8_d1;
            heap_bh213_w19_6_d1 <=  heap_bh213_w19_6;
            heap_bh213_w19_6_d2 <=  heap_bh213_w19_6_d1;
            heap_bh213_w20_7_d1 <=  heap_bh213_w20_7;
            heap_bh213_w20_7_d2 <=  heap_bh213_w20_7_d1;
            heap_bh213_w20_8_d1 <=  heap_bh213_w20_8;
            heap_bh213_w20_8_d2 <=  heap_bh213_w20_8_d1;
            heap_bh213_w21_4_d1 <=  heap_bh213_w21_4;
            heap_bh213_w21_4_d2 <=  heap_bh213_w21_4_d1;
            heap_bh213_w32_3_d1 <=  heap_bh213_w32_3;
            heap_bh213_w32_3_d2 <=  heap_bh213_w32_3_d1;
            heap_bh213_w33_2_d1 <=  heap_bh213_w33_2;
            heap_bh213_w33_2_d2 <=  heap_bh213_w33_2_d1;
            heap_bh213_w22_6_d1 <=  heap_bh213_w22_6;
            heap_bh213_w22_6_d2 <=  heap_bh213_w22_6_d1;
            heap_bh213_w23_4_d1 <=  heap_bh213_w23_4;
            heap_bh213_w23_4_d2 <=  heap_bh213_w23_4_d1;
            heap_bh213_w34_3_d1 <=  heap_bh213_w34_3;
            heap_bh213_w34_3_d2 <=  heap_bh213_w34_3_d1;
            heap_bh213_w35_2_d1 <=  heap_bh213_w35_2;
            heap_bh213_w35_2_d2 <=  heap_bh213_w35_2_d1;
            heap_bh213_w24_6_d1 <=  heap_bh213_w24_6;
            heap_bh213_w24_6_d2 <=  heap_bh213_w24_6_d1;
            heap_bh213_w25_4_d1 <=  heap_bh213_w25_4;
            heap_bh213_w25_4_d2 <=  heap_bh213_w25_4_d1;
            heap_bh213_w26_5_d1 <=  heap_bh213_w26_5;
            heap_bh213_w36_3_d1 <=  heap_bh213_w36_3;
            heap_bh213_w36_3_d2 <=  heap_bh213_w36_3_d1;
            heap_bh213_w37_2_d1 <=  heap_bh213_w37_2;
            heap_bh213_w37_2_d2 <=  heap_bh213_w37_2_d1;
            heap_bh213_w38_1_d1 <=  heap_bh213_w38_1;
            heap_bh213_w38_1_d2 <=  heap_bh213_w38_1_d1;
            heap_bh213_w26_6_d1 <=  heap_bh213_w26_6;
            heap_bh213_w27_4_d1 <=  heap_bh213_w27_4;
            heap_bh213_w28_6_d1 <=  heap_bh213_w28_6;
            heap_bh213_w29_4_d1 <=  heap_bh213_w29_4;
            heap_bh213_w30_6_d1 <=  heap_bh213_w30_6;
            heap_bh213_w31_3_d1 <=  heap_bh213_w31_3;
            heap_bh213_w32_4_d1 <=  heap_bh213_w32_4;
         end if;
      end process;
   XX_m212 <= Y ;
   YY_m212 <= X ;
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh213_ch0_0 <= std_logic_vector(unsigned("" & XX_m212(46 downto 23) & "") * unsigned("" & YY_m212(45 downto 29) & ""));
   heap_bh213_w54_0 <= DSP_bh213_ch0_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w53_0 <= DSP_bh213_ch0_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w52_0 <= DSP_bh213_ch0_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w51_0 <= DSP_bh213_ch0_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w50_0 <= DSP_bh213_ch0_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w49_0 <= DSP_bh213_ch0_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w48_0 <= DSP_bh213_ch0_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w47_0 <= DSP_bh213_ch0_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w46_0 <= DSP_bh213_ch0_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w45_0 <= DSP_bh213_ch0_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w44_0 <= DSP_bh213_ch0_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w43_0 <= DSP_bh213_ch0_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w42_0 <= DSP_bh213_ch0_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w41_0 <= DSP_bh213_ch0_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w40_0 <= DSP_bh213_ch0_0(26); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w39_0 <= DSP_bh213_ch0_0(25); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w38_0 <= DSP_bh213_ch0_0(24); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w37_0 <= DSP_bh213_ch0_0(23); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w36_0 <= DSP_bh213_ch0_0(22); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w35_0 <= DSP_bh213_ch0_0(21); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w34_0 <= DSP_bh213_ch0_0(20); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w33_0 <= DSP_bh213_ch0_0(19); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w32_0 <= DSP_bh213_ch0_0(18); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w31_0 <= DSP_bh213_ch0_0(17); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w30_0 <= DSP_bh213_ch0_0(16); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w29_0 <= DSP_bh213_ch0_0(15); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w28_0 <= DSP_bh213_ch0_0(14); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w27_0 <= DSP_bh213_ch0_0(13); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w26_0 <= DSP_bh213_ch0_0(12); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w25_0 <= DSP_bh213_ch0_0(11); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w24_0 <= DSP_bh213_ch0_0(10); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w23_0 <= DSP_bh213_ch0_0(9); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w22_0 <= DSP_bh213_ch0_0(8); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w21_0 <= DSP_bh213_ch0_0(7); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w20_0 <= DSP_bh213_ch0_0(6); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w19_0 <= DSP_bh213_ch0_0(5); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w18_0 <= DSP_bh213_ch0_0(4); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w17_0 <= DSP_bh213_ch0_0(3); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w16_0 <= DSP_bh213_ch0_0(2); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w15_0 <= DSP_bh213_ch0_0(1); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w14_0 <= DSP_bh213_ch0_0(0); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh213_ch1_0 <= std_logic_vector(unsigned("" & XX_m212(22 downto 0) & "0") * unsigned("" & YY_m212(45 downto 29) & ""));
   heap_bh213_w30_1 <= DSP_bh213_ch1_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w29_1 <= DSP_bh213_ch1_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w28_1 <= DSP_bh213_ch1_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w27_1 <= DSP_bh213_ch1_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w26_1 <= DSP_bh213_ch1_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w25_1 <= DSP_bh213_ch1_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w24_1 <= DSP_bh213_ch1_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w23_1 <= DSP_bh213_ch1_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w22_1 <= DSP_bh213_ch1_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w21_1 <= DSP_bh213_ch1_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w20_1 <= DSP_bh213_ch1_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w19_1 <= DSP_bh213_ch1_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w18_1 <= DSP_bh213_ch1_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w17_1 <= DSP_bh213_ch1_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w16_1 <= DSP_bh213_ch1_0(26); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w15_1 <= DSP_bh213_ch1_0(25); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w14_1 <= DSP_bh213_ch1_0(24); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w13_0 <= DSP_bh213_ch1_0(23); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w12_0 <= DSP_bh213_ch1_0(22); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w11_0 <= DSP_bh213_ch1_0(21); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w10_0 <= DSP_bh213_ch1_0(20); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w9_0 <= DSP_bh213_ch1_0(19); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w8_0 <= DSP_bh213_ch1_0(18); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w7_0 <= DSP_bh213_ch1_0(17); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w6_0 <= DSP_bh213_ch1_0(16); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w5_0 <= DSP_bh213_ch1_0(15); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w4_0 <= DSP_bh213_ch1_0(14); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w3_0 <= DSP_bh213_ch1_0(13); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w2_0 <= DSP_bh213_ch1_0(12); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w1_0 <= DSP_bh213_ch1_0(11); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w0_0 <= DSP_bh213_ch1_0(10); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh213_ch2_0 <= std_logic_vector(unsigned("" & XX_m212(46 downto 23) & "") * unsigned("" & YY_m212(28 downto 12) & ""));
   heap_bh213_w37_1 <= DSP_bh213_ch2_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w36_1 <= DSP_bh213_ch2_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w35_1 <= DSP_bh213_ch2_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w34_1 <= DSP_bh213_ch2_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w33_1 <= DSP_bh213_ch2_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w32_1 <= DSP_bh213_ch2_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w31_1 <= DSP_bh213_ch2_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w30_2 <= DSP_bh213_ch2_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w29_2 <= DSP_bh213_ch2_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w28_2 <= DSP_bh213_ch2_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w27_2 <= DSP_bh213_ch2_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w26_2 <= DSP_bh213_ch2_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w25_2 <= DSP_bh213_ch2_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w24_2 <= DSP_bh213_ch2_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w23_2 <= DSP_bh213_ch2_0(26); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w22_2 <= DSP_bh213_ch2_0(25); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w21_2 <= DSP_bh213_ch2_0(24); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w20_2 <= DSP_bh213_ch2_0(23); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w19_2 <= DSP_bh213_ch2_0(22); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w18_2 <= DSP_bh213_ch2_0(21); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w17_2 <= DSP_bh213_ch2_0(20); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w16_2 <= DSP_bh213_ch2_0(19); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w15_2 <= DSP_bh213_ch2_0(18); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w14_2 <= DSP_bh213_ch2_0(17); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w13_1 <= DSP_bh213_ch2_0(16); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w12_1 <= DSP_bh213_ch2_0(15); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w11_1 <= DSP_bh213_ch2_0(14); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w10_1 <= DSP_bh213_ch2_0(13); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w9_1 <= DSP_bh213_ch2_0(12); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w8_1 <= DSP_bh213_ch2_0(11); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w7_1 <= DSP_bh213_ch2_0(10); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w6_1 <= DSP_bh213_ch2_0(9); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w5_1 <= DSP_bh213_ch2_0(8); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w4_1 <= DSP_bh213_ch2_0(7); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w3_1 <= DSP_bh213_ch2_0(6); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w2_1 <= DSP_bh213_ch2_0(5); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w1_1 <= DSP_bh213_ch2_0(4); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w0_1 <= DSP_bh213_ch2_0(3); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh213_ch3_0 <= std_logic_vector(unsigned("" & XX_m212(22 downto 0) & "0") * unsigned("" & YY_m212(28 downto 12) & ""));
   heap_bh213_w13_2 <= DSP_bh213_ch3_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w12_2 <= DSP_bh213_ch3_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w11_2 <= DSP_bh213_ch3_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w10_2 <= DSP_bh213_ch3_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w9_2 <= DSP_bh213_ch3_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w8_2 <= DSP_bh213_ch3_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w7_2 <= DSP_bh213_ch3_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w6_2 <= DSP_bh213_ch3_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w5_2 <= DSP_bh213_ch3_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w4_2 <= DSP_bh213_ch3_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w3_2 <= DSP_bh213_ch3_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w2_2 <= DSP_bh213_ch3_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w1_2 <= DSP_bh213_ch3_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w0_2 <= DSP_bh213_ch3_0(27); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh213_ch4_0 <= std_logic_vector(unsigned("" & XX_m212(46 downto 23) & "") * unsigned("" & YY_m212(11 downto 0) & "00000"));
   heap_bh213_w20_3 <= DSP_bh213_ch4_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w19_3 <= DSP_bh213_ch4_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w18_3 <= DSP_bh213_ch4_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w17_3 <= DSP_bh213_ch4_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w16_3 <= DSP_bh213_ch4_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w15_3 <= DSP_bh213_ch4_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w14_3 <= DSP_bh213_ch4_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w13_3 <= DSP_bh213_ch4_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w12_3 <= DSP_bh213_ch4_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w11_3 <= DSP_bh213_ch4_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w10_3 <= DSP_bh213_ch4_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w9_3 <= DSP_bh213_ch4_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w8_3 <= DSP_bh213_ch4_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w7_3 <= DSP_bh213_ch4_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w6_3 <= DSP_bh213_ch4_0(26); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w5_3 <= DSP_bh213_ch4_0(25); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w4_3 <= DSP_bh213_ch4_0(24); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w3_3 <= DSP_bh213_ch4_0(23); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w2_3 <= DSP_bh213_ch4_0(22); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w1_3 <= DSP_bh213_ch4_0(21); -- cycle= 0 cp= 2.387e-09
   heap_bh213_w0_3 <= DSP_bh213_ch4_0(20); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
   heap_bh213_w6_4 <= '1'; -- cycle= 0 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_0_0 <= heap_bh213_w0_3_d1 & heap_bh213_w0_2_d1 & heap_bh213_w0_1_d1 & heap_bh213_w0_0_d1;
   CompressorIn_bh213_0_1(0) <= heap_bh213_w1_3_d1;
   Compressor_bh213_0: Compressor_14_3
      port map ( R => CompressorOut_bh213_0_0   ,
                 X0 => CompressorIn_bh213_0_0,
                 X1 => CompressorIn_bh213_0_1);
   heap_bh213_w0_4 <= CompressorOut_bh213_0_0(0); -- cycle= 1 cp= 0
   heap_bh213_w1_4 <= CompressorOut_bh213_0_0(1); -- cycle= 1 cp= 0
   heap_bh213_w2_4 <= CompressorOut_bh213_0_0(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_1_2 <= heap_bh213_w2_3_d1 & heap_bh213_w2_2_d1 & heap_bh213_w2_1_d1 & heap_bh213_w2_0_d1;
   CompressorIn_bh213_1_3(0) <= heap_bh213_w3_3_d1;
   Compressor_bh213_1: Compressor_14_3
      port map ( R => CompressorOut_bh213_1_1   ,
                 X0 => CompressorIn_bh213_1_2,
                 X1 => CompressorIn_bh213_1_3);
   heap_bh213_w2_5 <= CompressorOut_bh213_1_1(0); -- cycle= 1 cp= 0
   heap_bh213_w3_4 <= CompressorOut_bh213_1_1(1); -- cycle= 1 cp= 0
   heap_bh213_w4_4 <= CompressorOut_bh213_1_1(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_2_4 <= heap_bh213_w4_3_d1 & heap_bh213_w4_2_d1 & heap_bh213_w4_1_d1 & heap_bh213_w4_0_d1;
   CompressorIn_bh213_2_5(0) <= heap_bh213_w5_3_d1;
   Compressor_bh213_2: Compressor_14_3
      port map ( R => CompressorOut_bh213_2_2   ,
                 X0 => CompressorIn_bh213_2_4,
                 X1 => CompressorIn_bh213_2_5);
   heap_bh213_w4_5 <= CompressorOut_bh213_2_2(0); -- cycle= 1 cp= 0
   heap_bh213_w5_4 <= CompressorOut_bh213_2_2(1); -- cycle= 1 cp= 0
   heap_bh213_w6_5 <= CompressorOut_bh213_2_2(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_3_6 <= heap_bh213_w6_4_d1 & heap_bh213_w6_3_d1 & heap_bh213_w6_2_d1 & heap_bh213_w6_1_d1;
   CompressorIn_bh213_3_7(0) <= heap_bh213_w7_3_d1;
   Compressor_bh213_3: Compressor_14_3
      port map ( R => CompressorOut_bh213_3_3   ,
                 X0 => CompressorIn_bh213_3_6,
                 X1 => CompressorIn_bh213_3_7);
   heap_bh213_w6_6 <= CompressorOut_bh213_3_3(0); -- cycle= 1 cp= 0
   heap_bh213_w7_4 <= CompressorOut_bh213_3_3(1); -- cycle= 1 cp= 0
   heap_bh213_w8_4 <= CompressorOut_bh213_3_3(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_4_8 <= heap_bh213_w8_3_d1 & heap_bh213_w8_2_d1 & heap_bh213_w8_1_d1 & heap_bh213_w8_0_d1;
   CompressorIn_bh213_4_9(0) <= heap_bh213_w9_3_d1;
   Compressor_bh213_4: Compressor_14_3
      port map ( R => CompressorOut_bh213_4_4   ,
                 X0 => CompressorIn_bh213_4_8,
                 X1 => CompressorIn_bh213_4_9);
   heap_bh213_w8_5 <= CompressorOut_bh213_4_4(0); -- cycle= 1 cp= 0
   heap_bh213_w9_4 <= CompressorOut_bh213_4_4(1); -- cycle= 1 cp= 0
   heap_bh213_w10_4 <= CompressorOut_bh213_4_4(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_5_10 <= heap_bh213_w10_3_d1 & heap_bh213_w10_2_d1 & heap_bh213_w10_1_d1 & heap_bh213_w10_0_d1;
   CompressorIn_bh213_5_11(0) <= heap_bh213_w11_3_d1;
   Compressor_bh213_5: Compressor_14_3
      port map ( R => CompressorOut_bh213_5_5   ,
                 X0 => CompressorIn_bh213_5_10,
                 X1 => CompressorIn_bh213_5_11);
   heap_bh213_w10_5 <= CompressorOut_bh213_5_5(0); -- cycle= 1 cp= 0
   heap_bh213_w11_4 <= CompressorOut_bh213_5_5(1); -- cycle= 1 cp= 0
   heap_bh213_w12_4 <= CompressorOut_bh213_5_5(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_6_12 <= heap_bh213_w12_3_d1 & heap_bh213_w12_2_d1 & heap_bh213_w12_1_d1 & heap_bh213_w12_0_d1;
   CompressorIn_bh213_6_13(0) <= heap_bh213_w13_3_d1;
   Compressor_bh213_6: Compressor_14_3
      port map ( R => CompressorOut_bh213_6_6   ,
                 X0 => CompressorIn_bh213_6_12,
                 X1 => CompressorIn_bh213_6_13);
   heap_bh213_w12_5 <= CompressorOut_bh213_6_6(0); -- cycle= 1 cp= 0
   heap_bh213_w13_4 <= CompressorOut_bh213_6_6(1); -- cycle= 1 cp= 0
   heap_bh213_w14_4 <= CompressorOut_bh213_6_6(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_7_14 <= heap_bh213_w14_3_d1 & heap_bh213_w14_2_d1 & heap_bh213_w14_1_d1 & heap_bh213_w14_0_d1;
   CompressorIn_bh213_7_15(0) <= heap_bh213_w15_3_d1;
   Compressor_bh213_7: Compressor_14_3
      port map ( R => CompressorOut_bh213_7_7   ,
                 X0 => CompressorIn_bh213_7_14,
                 X1 => CompressorIn_bh213_7_15);
   heap_bh213_w14_5 <= CompressorOut_bh213_7_7(0); -- cycle= 1 cp= 0
   heap_bh213_w15_4 <= CompressorOut_bh213_7_7(1); -- cycle= 1 cp= 0
   heap_bh213_w16_4 <= CompressorOut_bh213_7_7(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_8_16 <= heap_bh213_w16_3_d1 & heap_bh213_w16_2_d1 & heap_bh213_w16_1_d1 & heap_bh213_w16_0_d1;
   CompressorIn_bh213_8_17(0) <= heap_bh213_w17_3_d1;
   Compressor_bh213_8: Compressor_14_3
      port map ( R => CompressorOut_bh213_8_8   ,
                 X0 => CompressorIn_bh213_8_16,
                 X1 => CompressorIn_bh213_8_17);
   heap_bh213_w16_5 <= CompressorOut_bh213_8_8(0); -- cycle= 1 cp= 0
   heap_bh213_w17_4 <= CompressorOut_bh213_8_8(1); -- cycle= 1 cp= 0
   heap_bh213_w18_4 <= CompressorOut_bh213_8_8(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_9_18 <= heap_bh213_w18_3_d1 & heap_bh213_w18_2_d1 & heap_bh213_w18_1_d1 & heap_bh213_w18_0_d1;
   CompressorIn_bh213_9_19(0) <= heap_bh213_w19_3_d1;
   Compressor_bh213_9: Compressor_14_3
      port map ( R => CompressorOut_bh213_9_9   ,
                 X0 => CompressorIn_bh213_9_18,
                 X1 => CompressorIn_bh213_9_19);
   heap_bh213_w18_5 <= CompressorOut_bh213_9_9(0); -- cycle= 1 cp= 0
   heap_bh213_w19_4 <= CompressorOut_bh213_9_9(1); -- cycle= 1 cp= 0
   heap_bh213_w20_4 <= CompressorOut_bh213_9_9(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_10_20 <= heap_bh213_w20_3_d1 & heap_bh213_w20_2_d1 & heap_bh213_w20_1_d1 & heap_bh213_w20_0_d1;
   CompressorIn_bh213_10_21(0) <= heap_bh213_w21_2_d1;
   Compressor_bh213_10: Compressor_14_3
      port map ( R => CompressorOut_bh213_10_10   ,
                 X0 => CompressorIn_bh213_10_20,
                 X1 => CompressorIn_bh213_10_21);
   heap_bh213_w20_5 <= CompressorOut_bh213_10_10(0); -- cycle= 1 cp= 0
   heap_bh213_w21_3 <= CompressorOut_bh213_10_10(1); -- cycle= 1 cp= 0
   heap_bh213_w22_3 <= CompressorOut_bh213_10_10(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_11_22 <= heap_bh213_w22_2_d1 & heap_bh213_w22_1_d1 & heap_bh213_w22_0_d1;
   CompressorIn_bh213_11_23 <= heap_bh213_w23_2_d1 & heap_bh213_w23_1_d1;
   Compressor_bh213_11: Compressor_23_3
      port map ( R => CompressorOut_bh213_11_11   ,
                 X0 => CompressorIn_bh213_11_22,
                 X1 => CompressorIn_bh213_11_23);
   heap_bh213_w22_4 <= CompressorOut_bh213_11_11(0); -- cycle= 1 cp= 0
   heap_bh213_w23_3 <= CompressorOut_bh213_11_11(1); -- cycle= 1 cp= 0
   heap_bh213_w24_3 <= CompressorOut_bh213_11_11(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_12_24 <= heap_bh213_w24_2_d1 & heap_bh213_w24_1_d1 & heap_bh213_w24_0_d1;
   CompressorIn_bh213_12_25 <= heap_bh213_w25_2_d1 & heap_bh213_w25_1_d1;
   Compressor_bh213_12: Compressor_23_3
      port map ( R => CompressorOut_bh213_12_12   ,
                 X0 => CompressorIn_bh213_12_24,
                 X1 => CompressorIn_bh213_12_25);
   heap_bh213_w24_4 <= CompressorOut_bh213_12_12(0); -- cycle= 1 cp= 0
   heap_bh213_w25_3 <= CompressorOut_bh213_12_12(1); -- cycle= 1 cp= 0
   heap_bh213_w26_3 <= CompressorOut_bh213_12_12(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_13_26 <= heap_bh213_w26_2_d1 & heap_bh213_w26_1_d1 & heap_bh213_w26_0_d1;
   CompressorIn_bh213_13_27 <= heap_bh213_w27_2_d1 & heap_bh213_w27_1_d1;
   Compressor_bh213_13: Compressor_23_3
      port map ( R => CompressorOut_bh213_13_13   ,
                 X0 => CompressorIn_bh213_13_26,
                 X1 => CompressorIn_bh213_13_27);
   heap_bh213_w26_4 <= CompressorOut_bh213_13_13(0); -- cycle= 1 cp= 0
   heap_bh213_w27_3 <= CompressorOut_bh213_13_13(1); -- cycle= 1 cp= 0
   heap_bh213_w28_3 <= CompressorOut_bh213_13_13(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_14_28 <= heap_bh213_w28_2_d1 & heap_bh213_w28_1_d1 & heap_bh213_w28_0_d1;
   CompressorIn_bh213_14_29 <= heap_bh213_w29_2_d1 & heap_bh213_w29_1_d1;
   Compressor_bh213_14: Compressor_23_3
      port map ( R => CompressorOut_bh213_14_14   ,
                 X0 => CompressorIn_bh213_14_28,
                 X1 => CompressorIn_bh213_14_29);
   heap_bh213_w28_4 <= CompressorOut_bh213_14_14(0); -- cycle= 1 cp= 0
   heap_bh213_w29_3 <= CompressorOut_bh213_14_14(1); -- cycle= 1 cp= 0
   heap_bh213_w30_3 <= CompressorOut_bh213_14_14(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_15_30 <= heap_bh213_w30_2_d1 & heap_bh213_w30_1_d1 & heap_bh213_w30_0_d1;
   CompressorIn_bh213_15_31 <= heap_bh213_w31_1_d1 & heap_bh213_w31_0_d1;
   Compressor_bh213_15: Compressor_23_3
      port map ( R => CompressorOut_bh213_15_15   ,
                 X0 => CompressorIn_bh213_15_30,
                 X1 => CompressorIn_bh213_15_31);
   heap_bh213_w30_4 <= CompressorOut_bh213_15_15(0); -- cycle= 1 cp= 0
   heap_bh213_w31_2 <= CompressorOut_bh213_15_15(1); -- cycle= 1 cp= 0
   heap_bh213_w32_2 <= CompressorOut_bh213_15_15(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_16_32 <= heap_bh213_w5_2_d1 & heap_bh213_w5_1_d1 & heap_bh213_w5_0_d1;
   CompressorIn_bh213_16_33(0) <= heap_bh213_w6_0_d1;
   Compressor_bh213_16: Compressor_13_3
      port map ( R => CompressorOut_bh213_16_16   ,
                 X0 => CompressorIn_bh213_16_32,
                 X1 => CompressorIn_bh213_16_33);
   heap_bh213_w5_5 <= CompressorOut_bh213_16_16(0); -- cycle= 1 cp= 0
   heap_bh213_w6_7 <= CompressorOut_bh213_16_16(1); -- cycle= 1 cp= 0
   heap_bh213_w7_5 <= CompressorOut_bh213_16_16(2); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_17_34 <= heap_bh213_w1_2_d1 & heap_bh213_w1_1_d1 & heap_bh213_w1_0_d1;
   Compressor_bh213_17: Compressor_3_2
      port map ( R => CompressorOut_bh213_17_17   ,
                 X0 => CompressorIn_bh213_17_34);
   heap_bh213_w1_5 <= CompressorOut_bh213_17_17(0); -- cycle= 1 cp= 0
   heap_bh213_w2_6 <= CompressorOut_bh213_17_17(1); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_18_35 <= heap_bh213_w3_2_d1 & heap_bh213_w3_1_d1 & heap_bh213_w3_0_d1;
   Compressor_bh213_18: Compressor_3_2
      port map ( R => CompressorOut_bh213_18_18   ,
                 X0 => CompressorIn_bh213_18_35);
   heap_bh213_w3_5 <= CompressorOut_bh213_18_18(0); -- cycle= 1 cp= 0
   heap_bh213_w4_6 <= CompressorOut_bh213_18_18(1); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_19_36 <= heap_bh213_w7_2_d1 & heap_bh213_w7_1_d1 & heap_bh213_w7_0_d1;
   Compressor_bh213_19: Compressor_3_2
      port map ( R => CompressorOut_bh213_19_19   ,
                 X0 => CompressorIn_bh213_19_36);
   heap_bh213_w7_6 <= CompressorOut_bh213_19_19(0); -- cycle= 1 cp= 0
   heap_bh213_w8_6 <= CompressorOut_bh213_19_19(1); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_20_37 <= heap_bh213_w9_2_d1 & heap_bh213_w9_1_d1 & heap_bh213_w9_0_d1;
   Compressor_bh213_20: Compressor_3_2
      port map ( R => CompressorOut_bh213_20_20   ,
                 X0 => CompressorIn_bh213_20_37);
   heap_bh213_w9_5 <= CompressorOut_bh213_20_20(0); -- cycle= 1 cp= 0
   heap_bh213_w10_6 <= CompressorOut_bh213_20_20(1); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_21_38 <= heap_bh213_w11_2_d1 & heap_bh213_w11_1_d1 & heap_bh213_w11_0_d1;
   Compressor_bh213_21: Compressor_3_2
      port map ( R => CompressorOut_bh213_21_21   ,
                 X0 => CompressorIn_bh213_21_38);
   heap_bh213_w11_5 <= CompressorOut_bh213_21_21(0); -- cycle= 1 cp= 0
   heap_bh213_w12_6 <= CompressorOut_bh213_21_21(1); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_22_39 <= heap_bh213_w13_2_d1 & heap_bh213_w13_1_d1 & heap_bh213_w13_0_d1;
   Compressor_bh213_22: Compressor_3_2
      port map ( R => CompressorOut_bh213_22_22   ,
                 X0 => CompressorIn_bh213_22_39);
   heap_bh213_w13_5 <= CompressorOut_bh213_22_22(0); -- cycle= 1 cp= 0
   heap_bh213_w14_6 <= CompressorOut_bh213_22_22(1); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_23_40 <= heap_bh213_w15_2_d1 & heap_bh213_w15_1_d1 & heap_bh213_w15_0_d1;
   Compressor_bh213_23: Compressor_3_2
      port map ( R => CompressorOut_bh213_23_23   ,
                 X0 => CompressorIn_bh213_23_40);
   heap_bh213_w15_5 <= CompressorOut_bh213_23_23(0); -- cycle= 1 cp= 0
   heap_bh213_w16_6 <= CompressorOut_bh213_23_23(1); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_24_41 <= heap_bh213_w17_2_d1 & heap_bh213_w17_1_d1 & heap_bh213_w17_0_d1;
   Compressor_bh213_24: Compressor_3_2
      port map ( R => CompressorOut_bh213_24_24   ,
                 X0 => CompressorIn_bh213_24_41);
   heap_bh213_w17_5 <= CompressorOut_bh213_24_24(0); -- cycle= 1 cp= 0
   heap_bh213_w18_6 <= CompressorOut_bh213_24_24(1); -- cycle= 1 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_25_42 <= heap_bh213_w19_2_d1 & heap_bh213_w19_1_d1 & heap_bh213_w19_0_d1;
   Compressor_bh213_25: Compressor_3_2
      port map ( R => CompressorOut_bh213_25_25   ,
                 X0 => CompressorIn_bh213_25_42);
   heap_bh213_w19_5 <= CompressorOut_bh213_25_25(0); -- cycle= 1 cp= 0
   heap_bh213_w20_6 <= CompressorOut_bh213_25_25(1); -- cycle= 1 cp= 0
   ----------------Synchro barrier, entering cycle 1----------------
   tempR_bh213_0 <= heap_bh213_w0_4; -- already compressed

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_26_43 <= heap_bh213_w2_6 & heap_bh213_w2_5 & heap_bh213_w2_4;
   CompressorIn_bh213_26_44 <= heap_bh213_w3_5 & heap_bh213_w3_4;
   Compressor_bh213_26: Compressor_23_3
      port map ( R => CompressorOut_bh213_26_26   ,
                 X0 => CompressorIn_bh213_26_43,
                 X1 => CompressorIn_bh213_26_44);
   heap_bh213_w2_7 <= CompressorOut_bh213_26_26(0); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w3_6 <= CompressorOut_bh213_26_26(1); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w4_7 <= CompressorOut_bh213_26_26(2); -- cycle= 1 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_27_45 <= heap_bh213_w4_6 & heap_bh213_w4_5 & heap_bh213_w4_4;
   CompressorIn_bh213_27_46 <= heap_bh213_w5_5 & heap_bh213_w5_4;
   Compressor_bh213_27: Compressor_23_3
      port map ( R => CompressorOut_bh213_27_27   ,
                 X0 => CompressorIn_bh213_27_45,
                 X1 => CompressorIn_bh213_27_46);
   heap_bh213_w4_8 <= CompressorOut_bh213_27_27(0); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w5_6 <= CompressorOut_bh213_27_27(1); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w6_8 <= CompressorOut_bh213_27_27(2); -- cycle= 1 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_28_47 <= heap_bh213_w6_7 & heap_bh213_w6_6 & heap_bh213_w6_5;
   CompressorIn_bh213_28_48 <= heap_bh213_w7_6 & heap_bh213_w7_5;
   Compressor_bh213_28: Compressor_23_3
      port map ( R => CompressorOut_bh213_28_28   ,
                 X0 => CompressorIn_bh213_28_47,
                 X1 => CompressorIn_bh213_28_48);
   heap_bh213_w6_9 <= CompressorOut_bh213_28_28(0); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w7_7 <= CompressorOut_bh213_28_28(1); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w8_7 <= CompressorOut_bh213_28_28(2); -- cycle= 1 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_29_49 <= heap_bh213_w8_6 & heap_bh213_w8_5 & heap_bh213_w8_4;
   CompressorIn_bh213_29_50 <= heap_bh213_w9_5 & heap_bh213_w9_4;
   Compressor_bh213_29: Compressor_23_3
      port map ( R => CompressorOut_bh213_29_29   ,
                 X0 => CompressorIn_bh213_29_49,
                 X1 => CompressorIn_bh213_29_50);
   heap_bh213_w8_8 <= CompressorOut_bh213_29_29(0); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w9_6 <= CompressorOut_bh213_29_29(1); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w10_7 <= CompressorOut_bh213_29_29(2); -- cycle= 1 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_30_51 <= heap_bh213_w10_6 & heap_bh213_w10_5 & heap_bh213_w10_4;
   CompressorIn_bh213_30_52 <= heap_bh213_w11_5 & heap_bh213_w11_4;
   Compressor_bh213_30: Compressor_23_3
      port map ( R => CompressorOut_bh213_30_30   ,
                 X0 => CompressorIn_bh213_30_51,
                 X1 => CompressorIn_bh213_30_52);
   heap_bh213_w10_8 <= CompressorOut_bh213_30_30(0); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w11_6 <= CompressorOut_bh213_30_30(1); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w12_7 <= CompressorOut_bh213_30_30(2); -- cycle= 1 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_31_53 <= heap_bh213_w12_6 & heap_bh213_w12_5 & heap_bh213_w12_4;
   CompressorIn_bh213_31_54 <= heap_bh213_w13_5 & heap_bh213_w13_4;
   Compressor_bh213_31: Compressor_23_3
      port map ( R => CompressorOut_bh213_31_31   ,
                 X0 => CompressorIn_bh213_31_53,
                 X1 => CompressorIn_bh213_31_54);
   heap_bh213_w12_8 <= CompressorOut_bh213_31_31(0); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w13_6 <= CompressorOut_bh213_31_31(1); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w14_7 <= CompressorOut_bh213_31_31(2); -- cycle= 1 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_32_55 <= heap_bh213_w14_6 & heap_bh213_w14_5 & heap_bh213_w14_4;
   CompressorIn_bh213_32_56 <= heap_bh213_w15_5 & heap_bh213_w15_4;
   Compressor_bh213_32: Compressor_23_3
      port map ( R => CompressorOut_bh213_32_32   ,
                 X0 => CompressorIn_bh213_32_55,
                 X1 => CompressorIn_bh213_32_56);
   heap_bh213_w14_8 <= CompressorOut_bh213_32_32(0); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w15_6 <= CompressorOut_bh213_32_32(1); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w16_7 <= CompressorOut_bh213_32_32(2); -- cycle= 1 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_33_57 <= heap_bh213_w16_6 & heap_bh213_w16_5 & heap_bh213_w16_4;
   CompressorIn_bh213_33_58 <= heap_bh213_w17_5 & heap_bh213_w17_4;
   Compressor_bh213_33: Compressor_23_3
      port map ( R => CompressorOut_bh213_33_33   ,
                 X0 => CompressorIn_bh213_33_57,
                 X1 => CompressorIn_bh213_33_58);
   heap_bh213_w16_8 <= CompressorOut_bh213_33_33(0); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w17_6 <= CompressorOut_bh213_33_33(1); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w18_7 <= CompressorOut_bh213_33_33(2); -- cycle= 1 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_34_59 <= heap_bh213_w18_6 & heap_bh213_w18_5 & heap_bh213_w18_4;
   CompressorIn_bh213_34_60 <= heap_bh213_w19_5 & heap_bh213_w19_4;
   Compressor_bh213_34: Compressor_23_3
      port map ( R => CompressorOut_bh213_34_34   ,
                 X0 => CompressorIn_bh213_34_59,
                 X1 => CompressorIn_bh213_34_60);
   heap_bh213_w18_8 <= CompressorOut_bh213_34_34(0); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w19_6 <= CompressorOut_bh213_34_34(1); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w20_7 <= CompressorOut_bh213_34_34(2); -- cycle= 1 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_35_61 <= heap_bh213_w20_6 & heap_bh213_w20_5 & heap_bh213_w20_4;
   CompressorIn_bh213_35_62 <= heap_bh213_w21_1_d1 & heap_bh213_w21_0_d1;
   Compressor_bh213_35: Compressor_23_3
      port map ( R => CompressorOut_bh213_35_35   ,
                 X0 => CompressorIn_bh213_35_61,
                 X1 => CompressorIn_bh213_35_62);
   heap_bh213_w20_8 <= CompressorOut_bh213_35_35(0); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w21_4 <= CompressorOut_bh213_35_35(1); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w22_5 <= CompressorOut_bh213_35_35(2); -- cycle= 1 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_36_63 <= heap_bh213_w32_1_d1 & heap_bh213_w32_0_d1 & heap_bh213_w32_2;
   CompressorIn_bh213_36_64 <= heap_bh213_w33_1_d1 & heap_bh213_w33_0_d1;
   Compressor_bh213_36: Compressor_23_3
      port map ( R => CompressorOut_bh213_36_36   ,
                 X0 => CompressorIn_bh213_36_63,
                 X1 => CompressorIn_bh213_36_64);
   heap_bh213_w32_3 <= CompressorOut_bh213_36_36(0); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w33_2 <= CompressorOut_bh213_36_36(1); -- cycle= 1 cp= 5.3072e-10
   heap_bh213_w34_2 <= CompressorOut_bh213_36_36(2); -- cycle= 1 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_37_65 <= heap_bh213_w22_4 & heap_bh213_w22_3 & heap_bh213_w22_5;
   CompressorIn_bh213_37_66 <= heap_bh213_w23_0_d1 & heap_bh213_w23_3;
   Compressor_bh213_37: Compressor_23_3
      port map ( R => CompressorOut_bh213_37_37   ,
                 X0 => CompressorIn_bh213_37_65,
                 X1 => CompressorIn_bh213_37_66);
   heap_bh213_w22_6 <= CompressorOut_bh213_37_37(0); -- cycle= 1 cp= 1.06144e-09
   heap_bh213_w23_4 <= CompressorOut_bh213_37_37(1); -- cycle= 1 cp= 1.06144e-09
   heap_bh213_w24_5 <= CompressorOut_bh213_37_37(2); -- cycle= 1 cp= 1.06144e-09

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_38_67 <= heap_bh213_w34_1_d1 & heap_bh213_w34_0_d1 & heap_bh213_w34_2;
   CompressorIn_bh213_38_68 <= heap_bh213_w35_1_d1 & heap_bh213_w35_0_d1;
   Compressor_bh213_38: Compressor_23_3
      port map ( R => CompressorOut_bh213_38_38   ,
                 X0 => CompressorIn_bh213_38_67,
                 X1 => CompressorIn_bh213_38_68);
   heap_bh213_w34_3 <= CompressorOut_bh213_38_38(0); -- cycle= 1 cp= 1.06144e-09
   heap_bh213_w35_2 <= CompressorOut_bh213_38_38(1); -- cycle= 1 cp= 1.06144e-09
   heap_bh213_w36_2 <= CompressorOut_bh213_38_38(2); -- cycle= 1 cp= 1.06144e-09

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_39_69 <= heap_bh213_w24_4 & heap_bh213_w24_3 & heap_bh213_w24_5;
   CompressorIn_bh213_39_70 <= heap_bh213_w25_0_d1 & heap_bh213_w25_3;
   Compressor_bh213_39: Compressor_23_3
      port map ( R => CompressorOut_bh213_39_39   ,
                 X0 => CompressorIn_bh213_39_69,
                 X1 => CompressorIn_bh213_39_70);
   heap_bh213_w24_6 <= CompressorOut_bh213_39_39(0); -- cycle= 1 cp= 1.59216e-09
   heap_bh213_w25_4 <= CompressorOut_bh213_39_39(1); -- cycle= 1 cp= 1.59216e-09
   heap_bh213_w26_5 <= CompressorOut_bh213_39_39(2); -- cycle= 1 cp= 1.59216e-09

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh213_40_71 <= heap_bh213_w36_1_d1 & heap_bh213_w36_0_d1 & heap_bh213_w36_2;
   CompressorIn_bh213_40_72 <= heap_bh213_w37_1_d1 & heap_bh213_w37_0_d1;
   Compressor_bh213_40: Compressor_23_3
      port map ( R => CompressorOut_bh213_40_40   ,
                 X0 => CompressorIn_bh213_40_71,
                 X1 => CompressorIn_bh213_40_72);
   heap_bh213_w36_3 <= CompressorOut_bh213_40_40(0); -- cycle= 1 cp= 1.59216e-09
   heap_bh213_w37_2 <= CompressorOut_bh213_40_40(1); -- cycle= 1 cp= 1.59216e-09
   heap_bh213_w38_1 <= CompressorOut_bh213_40_40(2); -- cycle= 1 cp= 1.59216e-09

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh213_41_73 <= heap_bh213_w26_4_d1 & heap_bh213_w26_3_d1 & heap_bh213_w26_5_d1;
   CompressorIn_bh213_41_74 <= heap_bh213_w27_0_d2 & heap_bh213_w27_3_d1;
   Compressor_bh213_41: Compressor_23_3
      port map ( R => CompressorOut_bh213_41_41   ,
                 X0 => CompressorIn_bh213_41_73,
                 X1 => CompressorIn_bh213_41_74);
   heap_bh213_w26_6 <= CompressorOut_bh213_41_41(0); -- cycle= 2 cp= 0
   heap_bh213_w27_4 <= CompressorOut_bh213_41_41(1); -- cycle= 2 cp= 0
   heap_bh213_w28_5 <= CompressorOut_bh213_41_41(2); -- cycle= 2 cp= 0

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh213_42_75 <= heap_bh213_w28_4_d1 & heap_bh213_w28_3_d1 & heap_bh213_w28_5;
   CompressorIn_bh213_42_76 <= heap_bh213_w29_0_d2 & heap_bh213_w29_3_d1;
   Compressor_bh213_42: Compressor_23_3
      port map ( R => CompressorOut_bh213_42_42   ,
                 X0 => CompressorIn_bh213_42_75,
                 X1 => CompressorIn_bh213_42_76);
   heap_bh213_w28_6 <= CompressorOut_bh213_42_42(0); -- cycle= 2 cp= 5.3072e-10
   heap_bh213_w29_4 <= CompressorOut_bh213_42_42(1); -- cycle= 2 cp= 5.3072e-10
   heap_bh213_w30_5 <= CompressorOut_bh213_42_42(2); -- cycle= 2 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh213_43_77 <= heap_bh213_w30_4_d1 & heap_bh213_w30_3_d1 & heap_bh213_w30_5;
   CompressorIn_bh213_43_78(0) <= heap_bh213_w31_2_d1;
   Compressor_bh213_43: Compressor_13_3
      port map ( R => CompressorOut_bh213_43_43   ,
                 X0 => CompressorIn_bh213_43_77,
                 X1 => CompressorIn_bh213_43_78);
   heap_bh213_w30_6 <= CompressorOut_bh213_43_43(0); -- cycle= 2 cp= 1.06144e-09
   heap_bh213_w31_3 <= CompressorOut_bh213_43_43(1); -- cycle= 2 cp= 1.06144e-09
   heap_bh213_w32_4 <= CompressorOut_bh213_43_43(2); -- cycle= 2 cp= 1.06144e-09
   ----------------Synchro barrier, entering cycle 2----------------
   ----------------Synchro barrier, entering cycle 3----------------
   finalAdderIn0_bh213 <= "0" & heap_bh213_w54_0_d3 & heap_bh213_w53_0_d3 & heap_bh213_w52_0_d3 & heap_bh213_w51_0_d3 & heap_bh213_w50_0_d3 & heap_bh213_w49_0_d3 & heap_bh213_w48_0_d3 & heap_bh213_w47_0_d3 & heap_bh213_w46_0_d3 & heap_bh213_w45_0_d3 & heap_bh213_w44_0_d3 & heap_bh213_w43_0_d3 & heap_bh213_w42_0_d3 & heap_bh213_w41_0_d3 & heap_bh213_w40_0_d3 & heap_bh213_w39_0_d3 & heap_bh213_w38_0_d3 & heap_bh213_w37_2_d2 & heap_bh213_w36_3_d2 & heap_bh213_w35_2_d2 & heap_bh213_w34_3_d2 & heap_bh213_w33_2_d2 & heap_bh213_w32_3_d2 & heap_bh213_w31_3_d1 & heap_bh213_w30_6_d1 & heap_bh213_w29_4_d1 & heap_bh213_w28_6_d1 & heap_bh213_w27_4_d1 & heap_bh213_w26_6_d1 & heap_bh213_w25_4_d2 & heap_bh213_w24_6_d2 & heap_bh213_w23_4_d2 & heap_bh213_w22_6_d2 & heap_bh213_w21_3_d2 & heap_bh213_w20_8_d2 & heap_bh213_w19_6_d2 & heap_bh213_w18_8_d2 & heap_bh213_w17_6_d2 & heap_bh213_w16_8_d2 & heap_bh213_w15_6_d2 & heap_bh213_w14_8_d2 & heap_bh213_w13_6_d2 & heap_bh213_w12_8_d2 & heap_bh213_w11_6_d2 & heap_bh213_w10_8_d2 & heap_bh213_w9_6_d2 & heap_bh213_w8_8_d2 & heap_bh213_w7_4_d2 & heap_bh213_w6_9_d2 & heap_bh213_w5_6_d2 & heap_bh213_w4_8_d2 & heap_bh213_w3_6_d2 & heap_bh213_w2_7_d2 & heap_bh213_w1_5_d2;
   finalAdderIn1_bh213 <= "0" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh213_w38_1_d2 & '0' & '0' & '0' & '0' & '0' & heap_bh213_w32_4_d1 & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh213_w21_4_d2 & heap_bh213_w20_7_d2 & '0' & heap_bh213_w18_7_d2 & '0' & heap_bh213_w16_7_d2 & '0' & heap_bh213_w14_7_d2 & '0' & heap_bh213_w12_7_d2 & '0' & heap_bh213_w10_7_d2 & '0' & heap_bh213_w8_7_d2 & heap_bh213_w7_7_d2 & heap_bh213_w6_8_d2 & '0' & heap_bh213_w4_7_d2 & '0' & '0' & heap_bh213_w1_4_d2;
   finalAdderCin_bh213 <= '0';
   Adder_final213_0: IntAdder_55_f400_uid266  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh213,
                 R => finalAdderOut_bh213   ,
                 X => finalAdderIn0_bh213,
                 Y => finalAdderIn1_bh213);
   ----------------Synchro barrier, entering cycle 4----------------
   -- concatenate all the compressed chunks
   CompressionResult213 <= finalAdderOut_bh213 & tempR_bh213_0_d3;
   -- End of code generated by BitHeap::generateCompressorVHDL
   R <= CompressionResult213(54 downto 7);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_57_f400_uid274
--                    (IntAdderAlternative_57_f400_uid278)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_57_f400_uid274 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(56 downto 0);
          Y : in  std_logic_vector(56 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(56 downto 0)   );
end entity;

architecture arch of IntAdder_57_f400_uid274 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(15 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(14 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(15 downto 0);
signal sum_l1_idx1 :  std_logic_vector(14 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(56 downto 42)) + ( "0" & Y(56 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(14 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(15 downto 15);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(14 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(15 downto 15);
   R <= sum_l1_idx1(14 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_65_f400_uid281
--                    (IntAdderAlternative_65_f400_uid285)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_65_f400_uid281 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(64 downto 0);
          Y : in  std_logic_vector(64 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(64 downto 0)   );
end entity;

architecture arch of IntAdder_65_f400_uid281 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(23 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(22 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(23 downto 0);
signal sum_l1_idx1 :  std_logic_vector(22 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(64 downto 42)) + ( "0" & Y(64 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(22 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(23 downto 23);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(22 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(23 downto 23);
   R <= sum_l1_idx1(22 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                              FPExp_11_52_F400
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: F. de Dinechin, Bogdan Pasca (2008-2013)
--------------------------------------------------------------------------------
-- Pipeline depth: 45 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FPExp_11_52_F400 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(11+52+2 downto 0);
          R : out  std_logic_vector(11+52+2 downto 0)   );
end entity;

architecture arch of FPExp_11_52_F400 is
   component ExpYTable_10_57 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(9 downto 0);
             Y : out  std_logic_vector(56 downto 0)   );
   end component;

   component FixFunctionByPiecewisePoly_61 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(35 downto 0);
             Y : out  std_logic_vector(36 downto 0)   );
   end component;

   component FixRealKCM_0_10_M56_log_2_unsigned is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(10 downto 0);
             R : out  std_logic_vector(66 downto 0)   );
   end component;

   component FixRealKCM_M3_9_0_1_log_2_unsigned is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(12 downto 0);
             R : out  std_logic_vector(10 downto 0)   );
   end component;

   component IntAdder_47_f400_uid204 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(46 downto 0);
             Y : in  std_logic_vector(46 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(46 downto 0)   );
   end component;

   component IntAdder_56_f484_uid52 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(55 downto 0);
             Y : in  std_logic_vector(55 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(55 downto 0)   );
   end component;

   component IntAdder_57_f400_uid274 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(56 downto 0);
             Y : in  std_logic_vector(56 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(56 downto 0)   );
   end component;

   component IntAdder_65_f400_uid281 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(64 downto 0);
             Y : in  std_logic_vector(64 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(64 downto 0)   );
   end component;

   component IntMultiplier_UsingDSP_46_47_48_unsigned_F400_uid211 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(45 downto 0);
             Y : in  std_logic_vector(46 downto 0);
             R : out  std_logic_vector(47 downto 0)   );
   end component;

   component LeftShifter_53_by_max_66_F400_uid3 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(52 downto 0);
             S : in  std_logic_vector(6 downto 0);
             R : out  std_logic_vector(118 downto 0)   );
   end component;

signal Xexn, Xexn_d1, Xexn_d2, Xexn_d3, Xexn_d4, Xexn_d5, Xexn_d6, Xexn_d7, Xexn_d8, Xexn_d9, Xexn_d10, Xexn_d11, Xexn_d12, Xexn_d13, Xexn_d14, Xexn_d15, Xexn_d16, Xexn_d17, Xexn_d18, Xexn_d19, Xexn_d20, Xexn_d21, Xexn_d22, Xexn_d23, Xexn_d24, Xexn_d25, Xexn_d26, Xexn_d27, Xexn_d28, Xexn_d29, Xexn_d30, Xexn_d31, Xexn_d32, Xexn_d33, Xexn_d34, Xexn_d35, Xexn_d36, Xexn_d37, Xexn_d38, Xexn_d39, Xexn_d40, Xexn_d41, Xexn_d42, Xexn_d43, Xexn_d44, Xexn_d45 :  std_logic_vector(1 downto 0);
signal XSign, XSign_d1, XSign_d2, XSign_d3, XSign_d4, XSign_d5, XSign_d6, XSign_d7, XSign_d8, XSign_d9, XSign_d10, XSign_d11, XSign_d12, XSign_d13, XSign_d14, XSign_d15, XSign_d16, XSign_d17, XSign_d18, XSign_d19, XSign_d20, XSign_d21, XSign_d22, XSign_d23, XSign_d24, XSign_d25, XSign_d26, XSign_d27, XSign_d28, XSign_d29, XSign_d30, XSign_d31, XSign_d32, XSign_d33, XSign_d34, XSign_d35, XSign_d36, XSign_d37, XSign_d38, XSign_d39, XSign_d40, XSign_d41, XSign_d42, XSign_d43, XSign_d44, XSign_d45 :  std_logic;
signal XexpField :  std_logic_vector(10 downto 0);
signal Xfrac :  std_logic_vector(51 downto 0);
signal e0 :  std_logic_vector(12 downto 0);
signal shiftVal, shiftVal_d1 :  std_logic_vector(12 downto 0);
signal resultWillBeOne, resultWillBeOne_d1, resultWillBeOne_d2, resultWillBeOne_d3 :  std_logic;
signal mXu :  std_logic_vector(52 downto 0);
signal oufl0, oufl0_d1, oufl0_d2, oufl0_d3, oufl0_d4, oufl0_d5, oufl0_d6, oufl0_d7, oufl0_d8, oufl0_d9, oufl0_d10, oufl0_d11, oufl0_d12, oufl0_d13, oufl0_d14, oufl0_d15, oufl0_d16, oufl0_d17, oufl0_d18, oufl0_d19, oufl0_d20, oufl0_d21, oufl0_d22, oufl0_d23, oufl0_d24, oufl0_d25, oufl0_d26, oufl0_d27, oufl0_d28, oufl0_d29, oufl0_d30, oufl0_d31, oufl0_d32, oufl0_d33, oufl0_d34, oufl0_d35, oufl0_d36, oufl0_d37, oufl0_d38, oufl0_d39, oufl0_d40, oufl0_d41, oufl0_d42, oufl0_d43, oufl0_d44 :  std_logic;
signal shiftValIn :  std_logic_vector(6 downto 0);
signal fixX0 :  std_logic_vector(118 downto 0);
signal fixX, fixX_d1, fixX_d2, fixX_d3, fixX_d4, fixX_d5, fixX_d6, fixX_d7 :  std_logic_vector(66 downto 0);
signal xMulIn :  std_logic_vector(12 downto 0);
signal absK, absK_d1 :  std_logic_vector(10 downto 0);
signal minusAbsK :  std_logic_vector(11 downto 0);
signal K, K_d1, K_d2, K_d3, K_d4, K_d5, K_d6, K_d7, K_d8, K_d9, K_d10, K_d11, K_d12, K_d13, K_d14, K_d15, K_d16, K_d17, K_d18, K_d19, K_d20, K_d21, K_d22, K_d23, K_d24, K_d25, K_d26, K_d27, K_d28, K_d29, K_d30, K_d31, K_d32, K_d33, K_d34, K_d35, K_d36, K_d37 :  std_logic_vector(11 downto 0);
signal absKLog2 :  std_logic_vector(66 downto 0);
signal subOp1 :  std_logic_vector(55 downto 0);
signal subOp2 :  std_logic_vector(55 downto 0);
signal Y :  std_logic_vector(55 downto 0);
signal Addr1 :  std_logic_vector(9 downto 0);
signal Z, Z_d1, Z_d2, Z_d3, Z_d4, Z_d5, Z_d6, Z_d7, Z_d8, Z_d9, Z_d10, Z_d11, Z_d12, Z_d13, Z_d14, Z_d15, Z_d16, Z_d17, Z_d18, Z_d19, Z_d20, Z_d21 :  std_logic_vector(45 downto 0);
signal Zhigh, Zhigh_d1 :  std_logic_vector(35 downto 0);
signal expA, expA_d1, expA_d2, expA_d3, expA_d4, expA_d5, expA_d6, expA_d7, expA_d8, expA_d9, expA_d10, expA_d11, expA_d12, expA_d13, expA_d14, expA_d15, expA_d16, expA_d17, expA_d18, expA_d19, expA_d20, expA_d21, expA_d22, expA_d23, expA_d24, expA_d25, expA_d26, expA_d27 :  std_logic_vector(56 downto 0);
signal expZmZm1 :  std_logic_vector(36 downto 0);
signal expZminus1X :  std_logic_vector(46 downto 0);
signal expZminus1Y :  std_logic_vector(46 downto 0);
signal expZminus1, expZminus1_d1 :  std_logic_vector(46 downto 0);
signal expArounded, expArounded_d1, expArounded_d2, expArounded_d3, expArounded_d4, expArounded_d5, expArounded_d6, expArounded_d7, expArounded_d8, expArounded_d9, expArounded_d10, expArounded_d11, expArounded_d12, expArounded_d13, expArounded_d14, expArounded_d15, expArounded_d16, expArounded_d17, expArounded_d18, expArounded_d19, expArounded_d20, expArounded_d21, expArounded_d22 :  std_logic_vector(45 downto 0);
signal lowerProduct, lowerProduct_d1 :  std_logic_vector(47 downto 0);
signal extendedLowerProduct :  std_logic_vector(56 downto 0);
signal expY, expY_d1 :  std_logic_vector(56 downto 0);
signal needNoNorm, needNoNorm_d1 :  std_logic;
signal preRoundBiasSig :  std_logic_vector(64 downto 0);
signal roundBit :  std_logic;
signal roundNormAddend :  std_logic_vector(64 downto 0);
signal roundedExpSigRes :  std_logic_vector(64 downto 0);
signal roundedExpSig, roundedExpSig_d1 :  std_logic_vector(64 downto 0);
signal ofl1 :  std_logic;
signal ofl2 :  std_logic;
signal ofl3 :  std_logic;
signal ofl :  std_logic;
signal ufl1 :  std_logic;
signal ufl2 :  std_logic;
signal ufl3 :  std_logic;
signal ufl :  std_logic;
signal Rexn :  std_logic_vector(1 downto 0);
constant g: positive := 4;
constant wE: positive := 11;
constant wF: positive := 52;
constant wFIn: positive := 52;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            Xexn_d1 <=  Xexn;
            Xexn_d2 <=  Xexn_d1;
            Xexn_d3 <=  Xexn_d2;
            Xexn_d4 <=  Xexn_d3;
            Xexn_d5 <=  Xexn_d4;
            Xexn_d6 <=  Xexn_d5;
            Xexn_d7 <=  Xexn_d6;
            Xexn_d8 <=  Xexn_d7;
            Xexn_d9 <=  Xexn_d8;
            Xexn_d10 <=  Xexn_d9;
            Xexn_d11 <=  Xexn_d10;
            Xexn_d12 <=  Xexn_d11;
            Xexn_d13 <=  Xexn_d12;
            Xexn_d14 <=  Xexn_d13;
            Xexn_d15 <=  Xexn_d14;
            Xexn_d16 <=  Xexn_d15;
            Xexn_d17 <=  Xexn_d16;
            Xexn_d18 <=  Xexn_d17;
            Xexn_d19 <=  Xexn_d18;
            Xexn_d20 <=  Xexn_d19;
            Xexn_d21 <=  Xexn_d20;
            Xexn_d22 <=  Xexn_d21;
            Xexn_d23 <=  Xexn_d22;
            Xexn_d24 <=  Xexn_d23;
            Xexn_d25 <=  Xexn_d24;
            Xexn_d26 <=  Xexn_d25;
            Xexn_d27 <=  Xexn_d26;
            Xexn_d28 <=  Xexn_d27;
            Xexn_d29 <=  Xexn_d28;
            Xexn_d30 <=  Xexn_d29;
            Xexn_d31 <=  Xexn_d30;
            Xexn_d32 <=  Xexn_d31;
            Xexn_d33 <=  Xexn_d32;
            Xexn_d34 <=  Xexn_d33;
            Xexn_d35 <=  Xexn_d34;
            Xexn_d36 <=  Xexn_d35;
            Xexn_d37 <=  Xexn_d36;
            Xexn_d38 <=  Xexn_d37;
            Xexn_d39 <=  Xexn_d38;
            Xexn_d40 <=  Xexn_d39;
            Xexn_d41 <=  Xexn_d40;
            Xexn_d42 <=  Xexn_d41;
            Xexn_d43 <=  Xexn_d42;
            Xexn_d44 <=  Xexn_d43;
            Xexn_d45 <=  Xexn_d44;
            XSign_d1 <=  XSign;
            XSign_d2 <=  XSign_d1;
            XSign_d3 <=  XSign_d2;
            XSign_d4 <=  XSign_d3;
            XSign_d5 <=  XSign_d4;
            XSign_d6 <=  XSign_d5;
            XSign_d7 <=  XSign_d6;
            XSign_d8 <=  XSign_d7;
            XSign_d9 <=  XSign_d8;
            XSign_d10 <=  XSign_d9;
            XSign_d11 <=  XSign_d10;
            XSign_d12 <=  XSign_d11;
            XSign_d13 <=  XSign_d12;
            XSign_d14 <=  XSign_d13;
            XSign_d15 <=  XSign_d14;
            XSign_d16 <=  XSign_d15;
            XSign_d17 <=  XSign_d16;
            XSign_d18 <=  XSign_d17;
            XSign_d19 <=  XSign_d18;
            XSign_d20 <=  XSign_d19;
            XSign_d21 <=  XSign_d20;
            XSign_d22 <=  XSign_d21;
            XSign_d23 <=  XSign_d22;
            XSign_d24 <=  XSign_d23;
            XSign_d25 <=  XSign_d24;
            XSign_d26 <=  XSign_d25;
            XSign_d27 <=  XSign_d26;
            XSign_d28 <=  XSign_d27;
            XSign_d29 <=  XSign_d28;
            XSign_d30 <=  XSign_d29;
            XSign_d31 <=  XSign_d30;
            XSign_d32 <=  XSign_d31;
            XSign_d33 <=  XSign_d32;
            XSign_d34 <=  XSign_d33;
            XSign_d35 <=  XSign_d34;
            XSign_d36 <=  XSign_d35;
            XSign_d37 <=  XSign_d36;
            XSign_d38 <=  XSign_d37;
            XSign_d39 <=  XSign_d38;
            XSign_d40 <=  XSign_d39;
            XSign_d41 <=  XSign_d40;
            XSign_d42 <=  XSign_d41;
            XSign_d43 <=  XSign_d42;
            XSign_d44 <=  XSign_d43;
            XSign_d45 <=  XSign_d44;
            shiftVal_d1 <=  shiftVal;
            resultWillBeOne_d1 <=  resultWillBeOne;
            resultWillBeOne_d2 <=  resultWillBeOne_d1;
            resultWillBeOne_d3 <=  resultWillBeOne_d2;
            oufl0_d1 <=  oufl0;
            oufl0_d2 <=  oufl0_d1;
            oufl0_d3 <=  oufl0_d2;
            oufl0_d4 <=  oufl0_d3;
            oufl0_d5 <=  oufl0_d4;
            oufl0_d6 <=  oufl0_d5;
            oufl0_d7 <=  oufl0_d6;
            oufl0_d8 <=  oufl0_d7;
            oufl0_d9 <=  oufl0_d8;
            oufl0_d10 <=  oufl0_d9;
            oufl0_d11 <=  oufl0_d10;
            oufl0_d12 <=  oufl0_d11;
            oufl0_d13 <=  oufl0_d12;
            oufl0_d14 <=  oufl0_d13;
            oufl0_d15 <=  oufl0_d14;
            oufl0_d16 <=  oufl0_d15;
            oufl0_d17 <=  oufl0_d16;
            oufl0_d18 <=  oufl0_d17;
            oufl0_d19 <=  oufl0_d18;
            oufl0_d20 <=  oufl0_d19;
            oufl0_d21 <=  oufl0_d20;
            oufl0_d22 <=  oufl0_d21;
            oufl0_d23 <=  oufl0_d22;
            oufl0_d24 <=  oufl0_d23;
            oufl0_d25 <=  oufl0_d24;
            oufl0_d26 <=  oufl0_d25;
            oufl0_d27 <=  oufl0_d26;
            oufl0_d28 <=  oufl0_d27;
            oufl0_d29 <=  oufl0_d28;
            oufl0_d30 <=  oufl0_d29;
            oufl0_d31 <=  oufl0_d30;
            oufl0_d32 <=  oufl0_d31;
            oufl0_d33 <=  oufl0_d32;
            oufl0_d34 <=  oufl0_d33;
            oufl0_d35 <=  oufl0_d34;
            oufl0_d36 <=  oufl0_d35;
            oufl0_d37 <=  oufl0_d36;
            oufl0_d38 <=  oufl0_d37;
            oufl0_d39 <=  oufl0_d38;
            oufl0_d40 <=  oufl0_d39;
            oufl0_d41 <=  oufl0_d40;
            oufl0_d42 <=  oufl0_d41;
            oufl0_d43 <=  oufl0_d42;
            oufl0_d44 <=  oufl0_d43;
            fixX_d1 <=  fixX;
            fixX_d2 <=  fixX_d1;
            fixX_d3 <=  fixX_d2;
            fixX_d4 <=  fixX_d3;
            fixX_d5 <=  fixX_d4;
            fixX_d6 <=  fixX_d5;
            fixX_d7 <=  fixX_d6;
            absK_d1 <=  absK;
            K_d1 <=  K;
            K_d2 <=  K_d1;
            K_d3 <=  K_d2;
            K_d4 <=  K_d3;
            K_d5 <=  K_d4;
            K_d6 <=  K_d5;
            K_d7 <=  K_d6;
            K_d8 <=  K_d7;
            K_d9 <=  K_d8;
            K_d10 <=  K_d9;
            K_d11 <=  K_d10;
            K_d12 <=  K_d11;
            K_d13 <=  K_d12;
            K_d14 <=  K_d13;
            K_d15 <=  K_d14;
            K_d16 <=  K_d15;
            K_d17 <=  K_d16;
            K_d18 <=  K_d17;
            K_d19 <=  K_d18;
            K_d20 <=  K_d19;
            K_d21 <=  K_d20;
            K_d22 <=  K_d21;
            K_d23 <=  K_d22;
            K_d24 <=  K_d23;
            K_d25 <=  K_d24;
            K_d26 <=  K_d25;
            K_d27 <=  K_d26;
            K_d28 <=  K_d27;
            K_d29 <=  K_d28;
            K_d30 <=  K_d29;
            K_d31 <=  K_d30;
            K_d32 <=  K_d31;
            K_d33 <=  K_d32;
            K_d34 <=  K_d33;
            K_d35 <=  K_d34;
            K_d36 <=  K_d35;
            K_d37 <=  K_d36;
            Z_d1 <=  Z;
            Z_d2 <=  Z_d1;
            Z_d3 <=  Z_d2;
            Z_d4 <=  Z_d3;
            Z_d5 <=  Z_d4;
            Z_d6 <=  Z_d5;
            Z_d7 <=  Z_d6;
            Z_d8 <=  Z_d7;
            Z_d9 <=  Z_d8;
            Z_d10 <=  Z_d9;
            Z_d11 <=  Z_d10;
            Z_d12 <=  Z_d11;
            Z_d13 <=  Z_d12;
            Z_d14 <=  Z_d13;
            Z_d15 <=  Z_d14;
            Z_d16 <=  Z_d15;
            Z_d17 <=  Z_d16;
            Z_d18 <=  Z_d17;
            Z_d19 <=  Z_d18;
            Z_d20 <=  Z_d19;
            Z_d21 <=  Z_d20;
            Zhigh_d1 <=  Zhigh;
            expA_d1 <=  expA;
            expA_d2 <=  expA_d1;
            expA_d3 <=  expA_d2;
            expA_d4 <=  expA_d3;
            expA_d5 <=  expA_d4;
            expA_d6 <=  expA_d5;
            expA_d7 <=  expA_d6;
            expA_d8 <=  expA_d7;
            expA_d9 <=  expA_d8;
            expA_d10 <=  expA_d9;
            expA_d11 <=  expA_d10;
            expA_d12 <=  expA_d11;
            expA_d13 <=  expA_d12;
            expA_d14 <=  expA_d13;
            expA_d15 <=  expA_d14;
            expA_d16 <=  expA_d15;
            expA_d17 <=  expA_d16;
            expA_d18 <=  expA_d17;
            expA_d19 <=  expA_d18;
            expA_d20 <=  expA_d19;
            expA_d21 <=  expA_d20;
            expA_d22 <=  expA_d21;
            expA_d23 <=  expA_d22;
            expA_d24 <=  expA_d23;
            expA_d25 <=  expA_d24;
            expA_d26 <=  expA_d25;
            expA_d27 <=  expA_d26;
            expZminus1_d1 <=  expZminus1;
            expArounded_d1 <=  expArounded;
            expArounded_d2 <=  expArounded_d1;
            expArounded_d3 <=  expArounded_d2;
            expArounded_d4 <=  expArounded_d3;
            expArounded_d5 <=  expArounded_d4;
            expArounded_d6 <=  expArounded_d5;
            expArounded_d7 <=  expArounded_d6;
            expArounded_d8 <=  expArounded_d7;
            expArounded_d9 <=  expArounded_d8;
            expArounded_d10 <=  expArounded_d9;
            expArounded_d11 <=  expArounded_d10;
            expArounded_d12 <=  expArounded_d11;
            expArounded_d13 <=  expArounded_d12;
            expArounded_d14 <=  expArounded_d13;
            expArounded_d15 <=  expArounded_d14;
            expArounded_d16 <=  expArounded_d15;
            expArounded_d17 <=  expArounded_d16;
            expArounded_d18 <=  expArounded_d17;
            expArounded_d19 <=  expArounded_d18;
            expArounded_d20 <=  expArounded_d19;
            expArounded_d21 <=  expArounded_d20;
            expArounded_d22 <=  expArounded_d21;
            lowerProduct_d1 <=  lowerProduct;
            expY_d1 <=  expY;
            needNoNorm_d1 <=  needNoNorm;
            roundedExpSig_d1 <=  roundedExpSig;
         end if;
      end process;
   Xexn <= X(wE+wFIn+2 downto wE+wFIn+1);
   XSign <= X(wE+wFIn);
   XexpField <= X(wE+wFIn-1 downto wFIn);
   Xfrac <= X(wFIn-1 downto 0);
   e0 <= conv_std_logic_vector(967, wE+2);  -- bias - (wF+g)
   shiftVal <= ("00" & XexpField) - e0; -- for a left shift
   -- underflow when input is shifted to zero (shiftval<0), in which case exp = 1
   resultWillBeOne <= shiftVal(wE+1);
   --  mantissa with implicit bit
   mXu <= "1" & Xfrac;
   -- Partial overflow/underflow detection
   ----------------Synchro barrier, entering cycle 1----------------
   oufl0 <= not shiftVal_d1(wE+1) when shiftVal_d1(wE downto 0) >= conv_std_logic_vector(66, wE+1) else '0';
   ---------------- cycle 0----------------
   shiftValIn <= shiftVal(6 downto 0);
   mantissa_shift: LeftShifter_53_by_max_66_F400_uid3  -- pipelineDepth=3 maxInDelay=2.7546e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => fixX0,
                 S => shiftValIn,
                 X => mXu);
   ----------------Synchro barrier, entering cycle 3----------------
   fixX <=  fixX0(118 downto 52)when resultWillBeOne_d3='0' else "0000000000000000000000000000000000000000000000000000000000000000000";
   xMulIn <=  fixX(65 downto 53); -- truncation, error 2^-3
   mulInvLog2: FixRealKCM_M3_9_0_1_log_2_unsigned  -- pipelineDepth=2 maxInDelay=1.55968e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => absK,
                 X => xMulIn);
   ----------------Synchro barrier, entering cycle 5----------------
   ----------------Synchro barrier, entering cycle 6----------------
   minusAbsK <= (11 downto 0 => '0') - ('0' & absK_d1);
   K <= minusAbsK when  XSign_d6='1'   else ('0' & absK_d1);
   ---------------- cycle 5----------------
   mulLog2: FixRealKCM_0_10_M56_log_2_unsigned  -- pipelineDepth=5 maxInDelay=2.82304e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => absKLog2,
                 X => absK);
   ----------------Synchro barrier, entering cycle 10----------------
   subOp1 <= fixX_d7(55 downto 0) when XSign_d10='0' else not (fixX_d7(55 downto 0));
   subOp2 <= absKLog2(55 downto 0) when XSign_d10='1' else not (absKLog2(55 downto 0));
   theYAdder: IntAdder_56_f484_uid52  -- pipelineDepth=3 maxInDelay=2.24144e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '1',
                 R => Y,
                 X => subOp1,
                 Y => subOp2);

   ----------------Synchro barrier, entering cycle 13----------------
   -- Now compute the exp of this fixed-point value
   Addr1 <= Y(55 downto 46);
   Z <= Y(45 downto 0);
   Zhigh <= Z(45 downto 10);
   table: ExpYTable_10_57  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Addr1,
                 Y => expA);
   ----------------Synchro barrier, entering cycle 14----------------
-- signal delay at BRAM output = 1.75e-09
   poly: FixFunctionByPiecewisePoly_61  -- pipelineDepth=20 maxInDelay=2.19472e-09
      port map ( clk  => clk,
                 rst  => rst,
                 X => Zhigh_d1,
                 Y => expZmZm1);
   ----------------Synchro barrier, entering cycle 34----------------
   -- Computing Z + (exp(Z)-1-Z)
   expZminus1X <= '0' & Z_d21;
   expZminus1Y <= (46 downto 37 => '0') & expZmZm1 ;
   Adder_expZminus1: IntAdder_47_f400_uid204  -- pipelineDepth=1 maxInDelay=4.4472e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0' ,
                 R => expZminus1,
                 X => expZminus1X,
                 Y => expZminus1Y);
   ----------------Synchro barrier, entering cycle 35----------------
   ---------------- cycle 14----------------
   -- Truncating expA to the same accuracy as expZminus1
   expArounded <= expA(56 downto 11);
   ----------------Synchro barrier, entering cycle 35----------------
   ----------------Synchro barrier, entering cycle 36----------------
   TheLowerProduct: IntMultiplier_UsingDSP_46_47_48_unsigned_F400_uid211  -- pipelineDepth=4 maxInDelay=4.36e-10
      port map ( clk  => clk,
                 rst  => rst,
                 R => lowerProduct,
                 X => expArounded_d22,
                 Y => expZminus1_d1);

   ----------------Synchro barrier, entering cycle 40----------------
   ----------------Synchro barrier, entering cycle 41----------------
   extendedLowerProduct <= ((56 downto 48 => '0') & lowerProduct_d1(47 downto 0));
   -- Final addition -- the product MSB bit weight is -k+2 = -8
   TheFinalAdder: IntAdder_57_f400_uid274  -- pipelineDepth=1 maxInDelay=4.4472e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => expY,
                 X => expA_d27,
                 Y => extendedLowerProduct);

   ----------------Synchro barrier, entering cycle 42----------------
   needNoNorm <= expY(56);
   ----------------Synchro barrier, entering cycle 43----------------
   -- Rounding: all this should consume one row of LUTs
   preRoundBiasSig <= conv_std_logic_vector(1023, wE+2)  & expY_d1(55 downto 4) when needNoNorm_d1 = '1'
      else conv_std_logic_vector(1022, wE+2)  & expY_d1(54 downto 3) ;
   roundBit <= expY_d1(3)  when needNoNorm_d1 = '1'    else expY_d1(2) ;
   roundNormAddend <= K_d37(11) & K_d37 & (51 downto 1 => '0') & roundBit;
   roundedExpSigOperandAdder: IntAdder_65_f400_uid281  -- pipelineDepth=1 maxInDelay=4.4472e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => roundedExpSigRes,
                 X => preRoundBiasSig,
                 Y => roundNormAddend);

   ----------------Synchro barrier, entering cycle 44----------------
   -- delay at adder output is 1.197e-09
   roundedExpSig <= roundedExpSigRes when Xexn_d44="01" else  "000" & (wE-2 downto 0 => '1') & (wF-1 downto 0 => '0');
   ----------------Synchro barrier, entering cycle 45----------------
   ofl1 <= not XSign_d45 and oufl0_d44 and (not Xexn_d45(1) and Xexn_d45(0)); -- input positive, normal,  very large
   ofl2 <= not XSign_d45 and (roundedExpSig_d1(wE+wF) and not roundedExpSig_d1(wE+wF+1)) and (not Xexn_d45(1) and Xexn_d45(0)); -- input positive, normal, overflowed
   ofl3 <= not XSign_d45 and Xexn_d45(1) and not Xexn_d45(0);  -- input was -infty
   ofl <= ofl1 or ofl2 or ofl3;
   ufl1 <= (roundedExpSig_d1(wE+wF) and roundedExpSig_d1(wE+wF+1))  and (not Xexn_d45(1) and Xexn_d45(0)); -- input normal
   ufl2 <= XSign_d45 and Xexn_d45(1) and not Xexn_d45(0);  -- input was -infty
   ufl3 <= XSign_d45 and oufl0_d44  and (not Xexn_d45(1) and Xexn_d45(0)); -- input negative, normal,  very large
   ufl <= ufl1 or ufl2 or ufl3;
   Rexn <= "11" when Xexn_d45 = "11"
      else "10" when ofl='1'
      else "00" when ufl='1'
      else "01";
   R <= Rexn & '0' & roundedExpSig_d1(62 downto 0);
end architecture;

