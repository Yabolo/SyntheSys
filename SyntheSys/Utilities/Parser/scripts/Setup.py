#!/usr/bin/python

###########################################################################
# ADACSYS                                                                 #
#                                                                         #
# Copyright (c), ADACSYS 2008-2014                                        #
# All Rights Reserved.                                                    #
# Licensed Materials - ADACSYS                                            #
#                                                                         #
# No part of this file may be reproduced, stored in a retrieval system,   #
# or transmitted in any form or by any means --- electronic, mechanical,  #
# photocopying, recording, or otherwise --- without prior written         #
# permission of ADACSYS.                                                  #
#                                                                         #
# WARRANTY:                                                               #
# Use all material in this file at your own risk. ADACSYS makes no claims #
# about any material contained in this file.                              #
###########################################################################


import os, sys, platform
PYTHON_PATH=os.path.abspath(os.path.normpath(os.path.join(os.path.dirname(__file__), "..", "..", "..", "..", "..", "..")))
sys.path.append(os.path.join(PYTHON_PATH, "Utilities", "trunk"))
sys.path.append(os.path.join(PYTHON_PATH, "PythonBuilder"))
import logging, ColoredLogging
import fileinput, shutil
import re
import subprocess
import argparse

import PythonExtBuilder

##############################################################
import Misc

ARCHI, OS, OS_CLASS, OS_FULL_NAME = Misc.GetSysInfo()
##############################################################
Module = "Parser"
PythonExtBuilder.BuildExt(	Module     = Module,
				HeaderFile = "GuiInterface.h",

				SWIGFile   = "{0}.i".format(Module),
				SetupFile  = "setup.py",
				SrcDir     = "../src/",

				ResultDir  = "../results/{0}/{1}bit".format(OS_CLASS, ARCHI),
				BuildDir   = "./build",
				Libraries  = ["Parser", "boost_program_options", "boost_filesystem", "boost_thread", "boost_system",] if OS=="lin" else ["Parser",],
				TestScript = "test_Parser.py")
				
##############################################################
