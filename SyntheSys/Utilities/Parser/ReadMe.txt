HDLParser python module from C++ library.

###############################################################################

DESCRIPTION:
	HDLParser is a python module made up of 2 parts: HDLParser.py and _HDLParser.so (The wrapped C++ library).
	It is automaticaly generated using scripts.
	It contains useful methods to parse VHDL files using EDBA.

###############################################################################

COMPILATION:
  
1- Install the compilation dependencies: 
  >> swig
  >> gcc
  >> g++
  >> python-setuptools
  >> python-dev

3- Run the compilation by launching:
	Execute 'BuildLib.sh' (from ./scripts/ directory).

4- Fetch the results HDLParser.py and _HDLParser.so in the results directory (or where you specified it).
 

###############################################################################


