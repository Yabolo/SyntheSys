library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

-- use IEEE.numeric_std.all;

entity calculus4bit is
port ( clk : in std_logic;
       a   : in std_logic_vector(3 downto 0); 
	   b   : in std_logic_vector(3 downto 0);
	   sel : in std_logic_vector(1 downto 0);
	   r   : out std_logic_vector(7 downto 0));
end calculus4bit;

architecture archi_calculus4bit of calculus4bit is

signal radd: std_logic_vector(7 downto 0);
signal rsub: std_logic_vector(7 downto 0);
signal rshift: std_logic_vector(7 downto 0);
signal rmux: std_logic_vector(7 downto 0);


attribute keep : boolean;
attribute keep of radd : signal is true;
attribute keep of rsub : signal is true;
attribute keep of rshift : signal is true;
attribute keep of rmux : signal is true;

component add4bit  
port ( clk : in std_logic;
       a   : in std_logic_vector(3 downto 0);
	   b   : in std_logic_vector(3 downto 0);
	   r   : out std_logic_vector(7 downto 0));
end component;

component sub4bit  
port ( clk : in std_logic;
       a   : in std_logic_vector(3 downto 0);
	   b   : in std_logic_vector(3 downto 0);
	   r   : out std_logic_vector(7 downto 0));
end component;

component mux4bit  
port ( clk : in std_logic;
       a   : in std_logic_vector(3 downto 0);
	   b   : in std_logic_vector(3 downto 0);
	   r   : out std_logic_vector(7 downto 0));
end component;

component shift4bit  
port ( clk : in std_logic;
       a   : in std_logic_vector(3 downto 0);
	   b   : in std_logic_vector(3 downto 0);
	   r   : out std_logic_vector(7 downto 0));
end component;

begin

U_add: entity work.add4bit port map(clk, a, b, radd);
U_sub: sub4bit port map(clk, a, b, rsub);
U_mux: mux4bit port map(clk, a, b, rmux);
U_shift : shift4bit port map(clk, a, b, rshift);



process(clk, sel)
begin
if clk'event and clk='1' then 
	case sel is
		when "00" => r <= radd;
		when "01" => r <= rsub;
		when "10" => r <= rmux;
		when "11" => r <= rshift;
		when others => r <= (others => '0');
	end case;
end if;
end process;

end archi_calculus4bit;

