#!/usr/bin/python

###########################################################################
# ADACSYS                                                                 #
#                                                                         #
# Copyright (c), ADACSYS 2008-2014                                        #
# All Rights Reserved.                                                    #
# Licensed Materials - ADACSYS                                            #
#                                                                         #
# No part of this file may be reproduced, stored in a retrieval system,   #
# or transmitted in any form or by any means --- electronic, mechanical,  #
# photocopying, recording, or otherwise --- without prior written         #
# permission of ADACSYS.                                                  #
#                                                                         #
# WARRANTY:                                                               #
# Use all material in this file at your own risk. ADACSYS makes no claims #
# about any material contained in this file.                              #
###########################################################################


import sys, os


if sys.maxsize==2**32/2-1: ARCHI="32" # SysArch = "32" if sys.maxsize==2**32/2-1 else "64")
else: ARCHI="64"
if sys.platform.startswith('win'): 
	OS="win";OS_CLASS="windows"
else:
	OS="lin";OS_CLASS="linux"
	
PYTHON_PATH=os.path.normpath(os.path.join(os.path.dirname(__file__), "..", "..", "..", "..", "..", "..", ".."))
sys.path.append(os.path.abspath(os.path.normpath(os.path.join(os.path.dirname(__file__), "..", "..", "results", OS_CLASS, "32bit" if OS=='win' else "{0}bit".format(ARCHI)))))
sys.path.append(os.path.abspath(os.path.normpath(os.path.join(PYTHON_PATH, "Utilities", "trunk"))))
import logging, ColoredLogging
import Parser


def Display(g, chemiName):
	res = True

	sigs = Parser.SignalVector()

	print("\t - Chemi-name '{0}'".format(chemiName))

	if not g.getPorts(chemiName, sigs):
		print("\t\t Ports : ")
		for Port in sigs:
			print("\t\t * {0} : ".format(Port.getName()))
			print("\t\t\t    io :   \t\t {0}".format(Port.getIO()))
			print("\t\t\t    type : \t\t {0}".format(Port.getType()))
			print("\t\t\t    size : \t\t {0}".format(Port.getSize()))
			print("\t\t\t    instrumentable : \t {0}".format("yes" if Port.getInstrumentable() else "no"))
	else:
		print("\t\t\t Error looking for ports of chemi-name '{0}'.".format(chemiName))
		res = False

	# ecrire signaux internes
	sigs.clear()
	if not g.getInternalSignals(chemiName, sigs):
		print("\t\t Signaux internes : ")
		for IntSig in sigs:
			print("\t\t * '{0}' : ".format(IntSig.getName()))
			print("\t\t\t    type :           \t".format(IntSig.getType()))
			print("\t\t\t    size :           \t".format(IntSig.getSize()))
			print("\t\t\t    instrumentable : \t".format("yes" if IntSig.getInstrumentable() else "no"))
	else:
		print("Error looking for internal signals of chemi-name '{0}'.".format(chemiName))
		res = False


	# ecrire instances
	Instances = Parser.ComponentVector()
	Instances.clear()
	recursiveCalls = []
	if not g.getInstances(chemiName, Instances):
		print("\t\t Instances : ")
		for ii in Instances:
			print("\t\t * {0}".format(ii.getInstanceName()))
			recursiveCalls.append(chemiName + " " + ii.getInstanceName())
	else:
		print("Error looking for instances of chemi-name '{0}.".format(chemiName))
		res = False

		for irc in recursiveCalls:
			Display(g, irc)
	return res

#===================================================================================
def ParseFiles(TopEntity, FileNames):
	"""
	"""
	g = Parser.GuiInterface()
	g.setTop(TopEntity)

	for FilePath in FileNames:
		print("----------------------------------------------------------")
		print("-----          Parse file '{0}'".format(FilePath))
		print("----------------------------------------------------------")

		if not g.singleFileParse(FilePath, "work"):	# retour 0 : parse OK
			print("\t => Parse succeeded.")
		else: print("\t => Parse failed...")

		print("----------------------------------------------------------")
	Display(g, "")







#================================================================
if __name__ == "__main__":
	# help :
	# XXX.exe entity_name file1 file2 ...

#	if len(sys.argv) < 2:
#		print("Usage : ", sys.argv[0], " top_entity file1 [file2 [file3 [...]]]")
#		sys.exit(1)

#	TopEntity = sys.argv[1]
	TopEntity = "calculus4bit"

	print("Top entity : '{0}'".format(TopEntity))

	FileList = ["./calculus4bit.vhd",]
	for Arg in sys.argv[2:]:
		FileList.append(Arg)

	ParseFiles(TopEntity, FileList)
		
		
		
		
		
		
		
		
		

