/* GuiInterface.i */
%module Parser
%{
/* Put header files here or function declarations below
or Includes the header in the wrapper code */
#include "../../../distrib/a_include/Parser.h"
%}

%{
#define SWIG_PYTHON_EXTRA_NATIVE_CONTAINERS 
#define SWIG_SHARED_PTR_SUBNAMESPACE tr1
%}

%include typemaps.i

%include std_string.i 	/*typemaps for converting C++ std::string objects*/


%include stl.i /**/
/* Parse the header file to generate wrappers */

/*#include "./li_std_string.i" */

/*%include li_std_string.i*/ /**/
%include cwstring.i
%include std_basic_string.i
%include std_wstring.i
%include cstring.i 	
%include cpointer.i 	/*wrappers around simple C pointers*/
%include carrays.i 	/*wrapping ordinary C pointers as arrays*/

%include cmalloc.i 	/*wrapping the low-level C memory allocation functions malloc(), calloc(), realloc(), and free().*/
%include cdata.i 	/*converting raw C data to and from strings in the target language.*/
%include std_deque.i 	/**/
%include std_list.i 	/**/
%include std_map.i 	/**/
%include std_pair.i 	/**/
%include std_set.i 	/**/
%include std_vector.i 	/* support for the C++ std::vector class in the STL */


/*%typemap(in,numinputs=0) std::string& Value ($*1_ltype temp)  "$1 = &temp;"*/
/*%typemap(out) std::string {
        $result = PyString_FromString($1.c_str());
    }
*/

/*%apply std::string& INOUT { std::string & }*/

namespace std {
	%template(StringVector) std::vector<std::string>;  
	%template(SignalVector) std::vector<GuiInterfaceSignal>; 
	%template(ComponentVector) std::vector<GuiInterfaceComponent>;
	%template(IntVector) vector<int>;
	%template(DoubleVector) vector<double>;
};




/*%include std_shared_ptr.i */
%include exception.i /*language-independent function for raising a run-time exception in the target language.*/

%include "../../../distrib/a_include/Parser.h"







