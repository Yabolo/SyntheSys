
import os, sys

print "LOCATION: "+os.path.abspath("./")

from distutils.core import setup, Extension

SysArch = "32" if sys.maxsize==2**32/2-1 else "64"
print "System architecture :", SysArch+"bit"

interfacemodule = Extension('_HDLParser',
		    extra_link_args=['-static'],
#		    extra_link_args = ["--enable-shared"],
		    #extra_objects = ["../a_lib/libModelGraph.a", "../a_lib/libVGuiInterface.a"],
                    define_macros = [('MAJOR_VERSION', '0'),
                                     ('MINOR_VERSION', '1')],
                    libraries = ["Parser", "boost_serialization", "boost_thread", "boost_program_options", "boost_system", "boost_filesystem"],
		    library_dirs = ["/home/payet/Bureau/ADACSYS_SVN/external/lib/linux32/boost/boost_1_43_0", "/home/payet/Bureau/ADACSYS_SVN/ADACSYS/a_lib/linux32/"],
		    include_dirs = ["/home/payet/Bureau/ADACSYS_SVN/external/include/boost/boost_1_43_0", "/home/payet/Bureau/ADACSYS_SVN/ADACSYS/a_include"],
		    sources = ["HDLParser_wrap.cxx"])

setup (name = 'HDLParser',
       version = '0.1',
       description = 'ADACSYS C++ VHDL Parsing subsystem for a_gui.exe',
       author = 'Matthieu PAYET and Sylvain DARRAS',
       author_email = 'matthieu.payet@adacsys.com',
       url = 'http://www.adacsys.com',
       long_description = '''
Module that use a C++ program to parse VHDL files.
''',
       ext_modules = [interfacemodule],
       py_modules = ["HDLParser"])
