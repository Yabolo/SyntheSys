
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_signed.all;
use IEEE.std_logic_arith.all;

package TrafficSynthesizer_pkg is

	constant DimX : natural := 2;
	constant DimY : natural := 1;
	constant FlitWidth : natural := 16;
	type FLITS is array(natural range <>) of std_logic_vector(FlitWidth-1 downto 0);
	type NATURALS is array(natural range <>) of natural;
	type DIMX_x_DIMY_NUMERIC is array(natural range <>) of natural;

end TrafficSynthesizer_pkg;

package RoutePack is


end RoutePack;

package CrossPack is


end CrossPack;