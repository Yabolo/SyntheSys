
use work.TrafficSynthesizer_pkg.all;

----------------------------------------------------------------------------------------------------
-- Actual File Name      = FifoToHandShake.vhd
-- Title & purpose       = NoC router input: Handshake protocol to FIFO converter
-- Author                = Matthieu PAYET (ADACSYS) - matthieu.payet@adacsys.com
-- Creation Date         = 2012-12-10 19:30
-- Version               = 0.1
-- Simple Description    = Convert asynchronous Handshake protocol to FIFO protocol
-- Specific issues       = 
-- Speed                 = 
-- Area estimates        = 
-- Tools (version)       = Xilinx ISE (13.1)
-- HDL standard followed = VHDL 2001 standard
-- Revisions & ECOs      = 1.0
----------------------------------------------------------------------------------------------------
	
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

-------------------------------------------------------------------------------
-- ENTITY: Handshake protocol to FIFO converter
-------------------------------------------------------------------------------
entity FifoToHandShake is
  
  generic (
    FlitWidth : natural := 16);

  port (
    Clk          : in  std_logic;
    Rst          : in  std_logic;
    
    HS_Tx        : out std_logic;
    HS_AckTx     : in  std_logic;
    HS_DataOut   : out std_logic_vector(FlitWidth-1 downto 0);
    
    FIFO_DataOut : in  std_logic_vector(FlitWidth-1 downto 0);
    FIFO_Read    : out std_logic;
    FIFO_IsEmpty : in  std_logic
    );

end FifoToHandShake;

-------------------------------------------------------------------------------
-- ARCHITECTURE: RTL, manage Handshake protocol
-------------------------------------------------------------------------------
architecture RTL of FifoToHandShake is

  type FSM_HS is (IDLE, READ_FIFO, WAIT_FOR_ACK);
  signal CurrentState, FutureState : FSM_HS := IDLE;
  signal DataToSend : std_logic_vector(FlitWidth-1 downto 0);
  
begin  -- RTL
 
  -----------------------------------------------------------------------------
  HS_DataOut <= FIFO_DataOut;
  
  -----------------------------------------------------------------------------
  FSM_Memory: process (Clk, Rst)
  begin
    if Clk'event and Clk = '1' then  -- rising clock edge
      if Rst = '1' then -- synchronous reset (active High)
        CurrentState <= IDLE;
      else
        CurrentState <= FutureState;
      end if;
      
    end if;
    
  end process FSM_Memory;
  
  -----------------------------------------------------------------------------
  FIFO2HS: process (CurrentState, HS_AckTx, FIFO_IsEmpty)
  begin  -- process FIFO2HS
    case CurrentState is
      when IDLE =>
        if FIFO_IsEmpty='1' then
          FutureState <= IDLE;
        else
          FutureState <= READ_FIFO;
        end if;
      when READ_FIFO =>
        if HS_AckTx='1' and FIFO_IsEmpty='1' then
          FutureState <= IDLE;
        else
          FutureState <= WAIT_FOR_ACK;
        end if;
      when WAIT_FOR_ACK =>
        if HS_AckTx='1'  then
          if FIFO_IsEmpty='0' then
            FutureState <= READ_FIFO;
          else
            FutureState <= IDLE;
          end if;
        else
          FutureState <= WAIT_FOR_ACK;
        end if;
      
      when others => null;
    end case;
    
  end process FIFO2HS;
  
  FIFO_Read <= '1' when CurrentState=READ_FIFO else '0';
  HS_Tx     <= '1' when CurrentState=WAIT_FOR_ACK else '0';

end RTL;


