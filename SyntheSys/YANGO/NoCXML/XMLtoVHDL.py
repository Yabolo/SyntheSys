#!/usr/bin/python

import os, sys, datetime, logging, argparse, shutil, math, random
from lxml import etree
from functools import reduce

sys.path.append("../Builder/")
import HDLEditor as HDL
HDL.Generator="NoCXML netlist generator" # For Header comments


#=======================================================================
class XmlLibManager:
	#---------------------------------------------------------------
	def __init__(self, LibPath, *ExtendedPathList):
		"""
		Save library path.
		"""
		self.LibPath  = os.path.abspath(LibPath)
		self.ExtPaths = list(ExtendedPathList)
		self.Services = []
		self.Modules  = []
		self.DTD      = {}
		self.Classification={}
		
		for Type in ["service", "module"]:
			self.DTD[Type] = etree.DTD(os.path.join(self.LibPath, "{0}.dtd".format(Type)))
		
		CurDir=os.path.abspath("./")
		
		os.chdir(self.LibPath) # For source path fetching
		self.Reload()
		os.chdir(CurDir)
		
	#---------------------------------------------------------------
	def Reload(self):
		"""
		Parse each xml file in library path and filter module/service xml elements
		"""
		del self.Services[:]
		del self.Modules[:]
		for Path in self.ExtPaths+[self.LibPath,]:
			logging.debug("[LIBRARY:'{0}'] Browsing...'{0}'".format(Path))
			for Root, SubFolders, Files in os.walk(Path):
				for FileName in Files:
					if FileName.endswith(".xml"):
						FilePath = os.path.join(Root, FileName)
						RootElmt = etree.parse(FilePath)
						logging.debug("[LIBRARY:'{0}'] Looking for modules in '{1}'...".format(os.path.basename(self.LibPath), FileName))
						for Elmt in RootElmt.iter("module"):  
							if self.Verify("module", Elmt):	 self.Modules.append(Module(Elmt, FilePath)) 
						logging.debug("[LIBRARY:'{0}'] Looking for services in '{1}'...".format(os.path.basename(self.LibPath), FileName))
						for Elmt in RootElmt.iter("service"):   
							if self.Verify("service", Elmt): self.Services.append(Service(Elmt))
		logging.debug("[LIBRARY:'{0}'] Link all modules/services...".format(os.path.basename(self.LibPath)))
		# Link all modules to services each other
		for Mod in self.Modules:
			NewDict = {}
			# Replace ID by a Service object
			for ID, ServReq in Mod.ReqServ.items():
				SName, SType, SVersion, UniqueName = ID.split('#')
				for S in self.Services:
					# Find the corresponding service in library
					if S.Name==SName and S.Type==SType and S.Version==SVersion:	
						ServReq[0]=S
							
			
		logging.debug("[LIBRARY:'{0}'] Link all services to modules...".format(os.path.basename(self.LibPath)))
		for Serv in self.Services:
			# Classify the service
			if not Serv.Category: self.ClassifyService(Serv, TreePath=None)
			else: 
				TPath=Serv.Category.split('.')
				TPath.reverse()
				self.ClassifyService(Serv, TreePath=TPath)
			# Link to modules
			for Mod in self.Modules: 
				if Mod.Offer(Serv.Name): 
					Serv.AddModule(Mod)
		logging.debug("[LIBRARY:'{0}'] Library loaded.".format(os.path.basename(self.LibPath)))
		
	#---------------------------------------------------------------
	def ClassifyService(self, Serv, TreePath=None, Cat=None):
		"""
		Insert the service into the category classification.
		"""
		if not TreePath:
			if not list(self.Classification.keys()).count("Unclassified"): self.Classification["Unclassified"]={}
			self.Classification["Unclassified"]['{0}({1})[v{2}]'.format(Serv.Name, Serv.Type, Serv.Version)]=Serv
		else:
			if Cat==None: Cat=self.Classification
			
			Leaf = TreePath.pop()
			if len(TreePath)>0:
				if not list(Cat.keys()).count(Leaf): Cat[Leaf]={}
				logging.info("Classify Service class {0}.".format(Leaf))
				self.ClassifyService(Serv, TreePath, Cat=Cat[Leaf])
			else: # in the end: add service
				if not list(Cat.keys()).count(Leaf): Cat[Leaf]={}
				if list(Cat[Leaf].keys()).count('{0}({1})[v{2}]'.format(Serv.Name, Serv.Type, Serv.Version)): 
					logging.error("Service {0} already classified: ignored.".format('{0}({1})[v{2}]'.format(Serv.Name, Serv.Type, Serv.Version)))
					return
				logging.info("Classify Service {0}.".format(Leaf))
				Cat[Leaf]['{0}({1})[v{2}]'.format(Serv.Name, Serv.Type, Serv.Version)]=Serv
			
			
	#---------------------------------------------------------------
	def Verify(self, Type, Elmt):
		"""
		Parse each xml file in library path and filter module/service xml elements
		"""
		if list(self.DTD.keys()).count(Type):  
			Valid = self.DTD[Type].validate(Elmt)
			if Valid: return True
			else:
				logging.error(self.DTD[Type].error_log.filter_from_errors()[0])
				sys.exit(1)
				return False
		else: 
			logging.error("No such type ({0}) for this library.".format(Type))
			sys.exit(1)
			return False
								
	#---------------------------------------------------------------
	def Display(self, ServiceTree=False):
		"""
		Print to stdout the list of modules and services or Service tree classification.
		"""			
		#---------------------------------------------------------------
		def DisplayTreeServ(Tree, Indent=""):
			"""
			Print to stdout recursively subtree item with indentation.
			"""
			for Item in sorted(Tree.keys()):
				if isinstance(Tree[Item], dict):
					print(Indent+"-->"+Item)
					DisplayTreeServ(Tree[Item], Indent=Indent+"  ")
				else:
					print(Indent+"* "+Item)
		#---------------------------------------------------------------
		if ServiceTree:
			print("SERVICE LIBRARY:")
			DisplayTreeServ(self.Classification)
		
		else:
			for Serv in self.Services:
				Serv.Display()
			for Mod in self.Modules:
				Mod.Display()
			
			print("")
			print(("#"*55))
			print(("# SUMMARY: {0} Services and {1} Modules saved in library.".format(len(self.Services), len(self.Modules))))
			print(("#"*55))
			
		
	#---------------------------------------------------------------
	def Service(self, SName, SType=None, SVersion=None):
		"""
		return the service object with specified name/type/version if it exists, None otherwise.
		"""
		for Serv in self.Services:
			if Serv.Name==SName:
				if SType:
					if Serv.Type==SType:
						if SVersion:
							if Serv.Version==SVersion: return Serv
						else: return Serv
				else: return Serv
		return None
		
	#---------------------------------------------------------------
	def Module(self, MName, MType=None, MVersion=None):
		"""
		return the module object with specified name/type/version if it exists, None otherwise.
		"""
		for Mod in self.Modules:
			if Mod.Name.lower()==MName.lower():
				if MType:
					if Mod.Type==MType:
						if MVersion:
							if Mod.Version==MVersion: return Mod
						else: return Mod
				else: return Mod	
		return None
		
#=======================================================================
class PackageBuilder:
	#---------------------------------------------------------------
	def __init__(self):
		"""
		Gather constants and types so as to build a HDL package synthesizable library elements.
		"""
		self.Package={"Constants":{},"Types":{},"TypeImport":{}, "EmptyPkg":[]}
		self.PkgName="NoNamePkg"

	#---------------------------------------------------------------
	def GenPackage(self, Name, OutputDir, Vars={}):
		"""
		Create a file named 'Name' into 'OutputDir' and write package information into it.
		"""
		if len(self.Package):
			self.PkgName=Name+"_pkg"
			with open(os.path.join(OutputDir, "{0}.vhd".format(self.PkgName)), 'w+') as PKGFILE:
				PKGFILE.write(HDL.Libraries(["IEEE",]))
				PKGFILE.write(HDL.Packages(["IEEE.std_logic_1164",],))
				PKGFILE.write(HDL.Packages(["IEEE.std_logic_signed",],))
				PKGFILE.write(HDL.Packages(["IEEE.std_logic_arith",],))
				PKGFILE.write("\n")
				ConstantsList=[x.Declare(Constant=True) for x in list(self.Package["Constants"].values())]
				TypesList=[]
				EmptyPkg=[]
				for NewType, TypeDesc in self.Package["TypeImport"].items():
					Size, SubType, Pkg = TypeDesc
					TypesList.append(HDL.ArrayType(NewType, Size, SubType))
					
				TypesList+=[HDL.ArrayType(TName, eval(TSize, Vars.copy()), TType) for TName, TSize, TType in list(self.Package["Types"].values())]
			
				PKGFILE.write(HDL.PkgDeclaration(self.PkgName, ''.join(ConstantsList+TypesList)))
				EmptyPkg=[]
				# Declare empty package (prevent importation error)
				for EmptyPkgName in self.Package["EmptyPkg"]:
					EmptyPkg.append(HDL.PkgDeclaration(EmptyPkgName, []))
				for PkgDeclaration in EmptyPkg:
					PKGFILE.write('\n')
					PKGFILE.write(PkgDeclaration)
					
				#PKGFILE.write(HDL.PkgBody(self.PkgName, Body=""))

			return self.PkgName, os.path.join(OutputDir, "{0}.vhd".format(self.PkgName))
		else:
			return None, 

	#---------------------------------------------------------------
	def CollectPkg(self, PkgObject):
		"""
		Update PkgObject package dictionary with this package.
		"""
		self.Package["Constants"].update(PkgObject.Package["Constants"])
		self.Package["Types"].update(PkgObject.Package["Types"])
		self.Package["TypeImport"].update(PkgObject.Package["TypeImport"])
		self.Package["EmptyPkg"]+=PkgObject.Package["EmptyPkg"]
		self.Package["EmptyPkg"]=list(set(self.Package["EmptyPkg"]))
	
			
#=======================================================================
class Service(PackageBuilder):
	#---------------------------------------------------------------
	def __init__(self, XMLElmt):
		"""
		Set global service definition parameters from an XML Element.
		"""
		PackageBuilder.__init__(self)
		self.Name    = ""
		self.Type    = ""
		self.Version = ""
		self.Category  = ""
		self.Params  = {} # Dictionary of service parameters
		self.Vars = {}
		self.Ports   = {} # Dictionary of service ports interface
		self.ModList = [] # List of modules offering this service
		self.XMLElmt = XMLElmt
		
		if not self.GetFromXML(XMLElmt): logging.error("Unable to parse service XML content.")
		
	#---------------------------------------------------------------
	def Copy(self):
		"""
		Return a copy of the object.
		"""
		return Service(self.XMLElmt)
		
	#---------------------------------------------------------------
	def GetFromXML(self, XMLElmt):
		"""
		Parse XML content and extract service information.
		"""
		if XMLElmt.tag!="service":
			logging.error("XML content do not describe a service.") 
			sys.exit(1)
			return False
		# Build a service object from Service name, Type, Version
		self.Name    = XMLElmt.attrib.get("name")
		self.Type    = XMLElmt.attrib.get("type")
		self.Version = XMLElmt.attrib.get("version")
		self.Category  = XMLElmt.attrib.get("category")
		logging.info("Found service: '{0}'".format(self.Name))
		# Get each parameter
		for ParamElmt in XMLElmt.iterchildren("parameter"):
			Attr = ParamElmt.attrib
			self.AddParameter(Attr.get("name"), Attr.get("type"), Attr.get("typeimport"), Attr.get("size"), Attr.get("default"))
		# Get each input
		for InputElmt in XMLElmt.iterchildren("input"):
			Attr = InputElmt.attrib
			self.AddPort(Attr.get("name"), "IN", Attr.get("type"), Attr.get("typeimport"), Attr.get("size"), Attr.get("default"), Attr.get("modifier"), Attr.get("share"))
		# Get each output
		for OutputElmt in XMLElmt.iterchildren("output"):
			Attr = OutputElmt.attrib
			self.AddPort(Attr.get("name"), "OUT", Attr.get("type"), Attr.get("typeimport"), Attr.get("size"), Attr.get("default"), Attr.get("modifier"), Attr.get("share"))
		return True
		
	#---------------------------------------------------------------
	def AddParameter(self, PName, PType, PTypeImport, PSize, PDefault, IdxDict={}):
		"""
		Add a parameter (defined by Name/type/TypeImport/size/default_value) to this service.
		"""
		self.Params[PName] = Signal(Name=PName, Type=PType, PTypeImport=PTypeImport, Size=PSize, Default=PDefault)
		Variables=self.Vars.copy()
		Variables.update(IdxDict)
		self.Vars[PName] = self.Params[PName].integers(Base=10, Variables=Variables)
	
	#---------------------------------------------------------------
	def AddPort(self, PName, PDir, PType, PTypeImport, PSize, PDefault, PModifier=None, PShare=False):
		"""
		Add a port (defined by Name/type/TypeImport/size/default_value) to this service.
		"""
		self.Ports[PName] = Signal(Name=PName, Type=PType, PTypeImport=PTypeImport, Size=PSize, Default=PDefault, Dir=PDir, Modifier=PModifier, share=bool(PShare))
		
	#---------------------------------------------------------------
	def AddModule(self, Mod):
		"""
		Add a module implementing this service.
		"""
		self.ModList.append(Mod)
	
	#---------------------------------------------------------------
	def GetModule(self):
		"""
		return a module implementing this service.
		"""
		if len(self.ModList): return self.ModList[0]
		else: return None
		
	#---------------------------------------------------------------
	def GenSrc(self, Synthesizable=True, OutputDir="./Output", IsTop=True):
		"""
		Generate source files or copy sources to output directory.
		Return True if success.
		"""
		if not os.path.isdir(OutputDir): os.makedirs(OutputDir)
		Mod = self.GetModule()
		if Mod: return Mod.GenSrc(Synthesizable=Synthesizable, OutputDir=OutputDir, IsTop=IsTop)
		else: 
			logging.error("[Service '{0}'] No module implementation for this service.".format(self))
			return []
			
		
	#---------------------------------------------------------------
	def IsOrthogonal(self):
		"""
		return True if service is of type 'orthogonal' else False.
		"""
		return self.Type=="orthogonal"
		
	#---------------------------------------------------------------
	def CollectPkg(self):
		"""
		Update package dictionary with children package.
		"""
		for Child in list(self.Params.values())+list(self.Ports.values())+[self.GetModule(),]:
			if Child!=None: PackageBuilder.CollectPkg(self, Child)
		for Const in list(self.Package["Constants"].keys()):
			if list(self.Params.keys()).count(Const):
				self.Package["Constants"][Const]=self.Params[Const].HDLFormat(self.Vars.copy())
		
	#---------------------------------------------------------------
	def Display(self):
		"""
		Print service parameters to stdout.
		"""
		print(("SERVICE: \n\tname='{0}', Type='{1}', Version='{2}'".format(self.Name, self.Type, self.Version)))
		print(("\tParameters =", list(self.Params.keys())))
		print(("\tPorts      =", list(self.Ports.keys())))
		print(("\tModules that implements it =", [x.Name for x in self.ModList]))
		
	#---------------------------------------------------------------
	def __str__(self):
		return '{0}({1})[v{2}]'.format(self.Name, self.Type, self.Version)

#=======================================================================
class Module(PackageBuilder):
	#---------------------------------------------------------------
	def __init__(self, XMLElmt, FilePath=None):
		"""
		Set global Module definition parameters.
		"""
		PackageBuilder.__init__(self)
		self.Name      = ""
		self.Version   = ""
		self.Title     = ""
		self.Purpose   = ""
		self.Desc      = ""
		self.Issues    = ""
		self.Speed     = ""
		self.Area      = ""
		self.Tool      = ""
		self.Params    = {}
		self.Const     = {}
		self.Vars      = {}
		self.Ports     = {} # Dictionary of port signals
		self.OrthoPorts= {}
		self.Sources   = {"RTL":[], "Behavioral":[]} # two lists of sources
		self.Resources = {} # Dictionary of devices dictionaries of resources
		self.ReqServ   = {}
		self.ServAlias = {}
		self.OfferedServ = {}
		self.IO        = []
		self.FilePath  = FilePath
		
		if not self.GetFromXML(XMLElmt): logging.error("Unable to parse module XML content.")
		
	#---------------------------------------------------------------
	def GetFromXML(self, XMLElmt):
		"""
		Parse XML content and extract module information.
		"""
		# Build a module object from module name, Type, Version
		self.Name      = XMLElmt.attrib.get("name")
		self.Version   = XMLElmt.attrib.get("version")
		self.Title     = XMLElmt.attrib.get("title")
		self.Purpose   = XMLElmt.attrib.get("purpose")
		self.Desc      = XMLElmt.attrib.get("description")
		self.Issues    = XMLElmt.attrib.get("issues")
		self.Speed     = XMLElmt.attrib.get("speed")
		self.Area      = XMLElmt.attrib.get("area")
		self.Tool      = XMLElmt.attrib.get("tool")
		logging.info("Found module: '{0}'".format(self.Name))
		
		# Get each parameter
		for ParamElmt in XMLElmt.iterchildren("parameter"):
			Attr = ParamElmt.attrib
			self.AddParameter(PName=Attr.get("name"), PType=Attr.get("type"), PTypeImport=Attr.get("typeimport"), PSize=Attr.get("size"), PDefault=Attr.get("default"))
		# Get each input
		for InputElmt in XMLElmt.iterchildren("input"):
			Attr = InputElmt.attrib
			self.AddPort(PName=Attr.get("name"), PDir="IN", PType=Attr.get("type"), PTypeImport=Attr.get("typeimport"), PSize=Attr.get("size"), PDefault=Attr.get("default"))
		# Get each output
		for OutputElmt in XMLElmt.iterchildren("output"):
			Attr = OutputElmt.attrib
			self.AddPort(PName=Attr.get("name"), PDir="OUT", PType=Attr.get("type"), PTypeImport=Attr.get("typeimport"), PSize=Attr.get("size"), PDefault=Attr.get("default"))
		# Get each source
		for CoreElmt in XMLElmt.iterchildren("core"):
			for BehavElmt in CoreElmt.iter("behavioral"): # Iterate on all children
				self.AddSource(BehavElmt.attrib.get("path"), Synthesizable=False)
			for RTLElmt in CoreElmt.iter("rtl"): # Iterate on all children
				self.AddSource(RTLElmt.attrib.get("path"), Synthesizable=True)
		# Get resource usage
		for RscElmt in XMLElmt.iterchildren("resources"):
			for DevElmt in RscElmt.iter(): # Iterate on all children
				Attr = DevElmt.attrib
				for RName, Rused in Attr.items():
					self.AddResource(DevElmt.tag, RName, Rused)
		# Get each service
		for ServElmt in XMLElmt.iterchildren("services"):
			for ReqElmt in ServElmt.iter("required"): # Iterate on all children
				ReqMapping={}
				ReqName=ReqElmt.get("name")
				ReqID=ReqName.split(':')
				if len(ReqID)>1: RVars=ReqID[1:]
				else: RVars=[]
				ReqMapping={}
				for MapElmt in ReqElmt.iter("map"):
					VDict=self.Vars.copy()
					if MapElmt.get("formal"):
						Formal=MapElmt.get("formal")
						ActualList=SigName(MapElmt.get("actual"), RVars, VDict)
						Cond=eval(str(MapElmt.get("when")), VDict)
						if MapElmt.get("when")==None: Cond=True
						ReqMapping[Formal]=[ActualList, Cond, {}]
					else:
						self.IO.append(MapElmt.get("actual"))
				for V in RVars:	V=eval(V, VDict)
				Alias = ReqElmt.get("alias")
				if not Alias: Alias=ReqName
				self.AddReqServ(ReqID[0], ReqElmt.attrib.get("type"), ReqElmt.attrib.get("version"), ReqMapping, UniqueName=ServName(Alias))
			for OffElmt in ServElmt.iterchildren("offered"): # Iterate on all children
				OffMapping={}
				for MapElmt in OffElmt.iter("map"):
					OffMapping[MapElmt.get("formal")]=MapElmt.get("actual")
				self.AddServ(OffElmt.attrib.get("name"), OffElmt.attrib.get("alias"), OffMapping)
		# Get each looped parameters
		for LoopElmt in XMLElmt.iterchildren("loop"):
			self.ParseLoops(LoopElmt)
		logging.debug("[MODULE={0}] Parse completed.".format(self.Name))
		return True
		
	#---------------------------------------------------------------
	def ParseLoops(self, LoopElmt, Indexes=[], IdxDict={}):
		"""
		Parse each service requirement in XML loop element.
		"""
		Var, InitVal, LastVal = self.ParseIndex(LoopElmt.get("index"))
		if Var: 
			if not Indexes.count(Var): Indexes.append(Var)
			for Index in range(InitVal, LastVal+1):
				LocalParams={}
				IdxDict[Var]=Index
				# Get each constant (format: Name_v0val0_v1val1...)
				for ParamElmt in LoopElmt.iterchildren("parameter"):
					Attr = ParamElmt.attrib		
					BaseName=Attr.get("name")			
					CName=BaseName+'_'+"".join([x+str(IdxDict[x]) for x in Indexes])
					CType=Attr.get("type")
					CTypeImport=Attr.get("typeimport")
					CSize=Attr.get("size")
					CDefault=Attr.get("default")
					Constant=Signal(CName, CType, CTypeImport, CSize, CDefault, Vars=IdxDict.copy()).HDLFormat(self.Vars.copy())
					if not list(self.Const.keys()).count(BaseName): self.Const[BaseName]={}
					self.Const[BaseName][CName]=Constant
					LocalParams[BaseName]=CName
				
				# Get each service
				for ServElmt in LoopElmt.iterchildren("services"):
					for ReqElmt in ServElmt.iter("required"): # Iterate on all children
						ReqName=ReqElmt.get("name")
						ReqID=ReqName.split(':')
						if len(ReqID)>1: RVars=ReqID[1:]
						else: RVars=[]
						ReqMapping={}
						for MapElmt in ReqElmt.iter("map"):
							VDict=self.Vars.copy()
							VDict.update(IdxDict.copy())
							Formal=MapElmt.get("formal")
							ActualList=SigName(MapElmt.get("actual"), RVars, VDict, LocalParams=LocalParams) 
							#------Index Value replacements----------
							for Actual in ActualList:
								Original=Actual[1][:]
								Ref=Actual[1].replace('*','_').replace('+','_').replace('-','_').replace('/','_').replace('(','_').replace(')','_')
								OpList=[] # list element without operators
								for A0 in Actual[1].split('*'):
									for A1 in Actual[1].split('+'):
										for A2 in Actual[1].split('-'):
											for A3 in Actual[1].split('/'):
												for A4 in Actual[1].split('('):
													for A5 in Actual[1].split(')'):
														OpList.append(A5)
								# Replace each index by its value
								for i, Operand in enumerate(OpList):
									if Operand in IdxDict:
										OpList[i]=str(IdxDict[Operand])
								# re-insert the operator at proper place
								Actual[1]=""
								Cnt=0#Counter of operand
								i=0#Counter for character
								while(len(Actual[1])!=len(Ref)):
									if Original[i]!=Ref[i]:
										Actual[1]+=Ref[i]
										Cnt+=1
										i+=1
									else:
										Actual[1]+=OpList[Cnt]
										i+=len(OpList[Cnt])
										Cnt+=1
							#----------------------------------------
							Cond=eval(str(MapElmt.get("when")), VDict)
							if MapElmt.get("when")==None: Cond=True
							ReqMapping[Formal]=[ActualList, Cond, IdxDict.copy()]
						for V in RVars:	V=eval(V, VDict)
						Alias = ReqElmt.get("alias")
						if not Alias: Alias=ReqName
						else: Alias+=":{0}".format(Var)
						self.AddReqServ(ReqID[0], ReqElmt.attrib.get("type"), ReqElmt.attrib.get("version"), ReqMapping, UniqueName=ServName(Alias, Vars=IdxDict.copy()))
				# Get each looped children
				for SubLoopElmt in LoopElmt.iterchildren("loop"):
					self.ParseLoops(SubLoopElmt, Indexes[:], IdxDict.copy())
		else: logging.error("[Module '{0}'] Can't find index name in loop.".format(self))
		
	#---------------------------------------------------------------
	def ParseIndex(self, String):
		"""
		Parse index string in loop. Ex: "r=[0,rows[" <=> for r in range(0,rows,1)
		"""
		if String:
			try: Var, Range  = String.split('=')
			except: logging.error("[Module '{0}'] It should be one (and only one) '=' symbol in index description '{1}'.".format(self, String))
			try: Left, Right = Range.split(',')
			except: logging.error("[Module '{0}'] It should be one (and only one) ',' symbol in index description '{1}'.".format(self, String))

			InitVal=eval(Left[1:], self.Vars.copy())
			if Left.startswith('['): pass
			elif Left.startswith(']'): InitVal+=1
			else: logging.error("[Module '{0}'] Wrong range description '{1}'. Expected brakets range format (ex: '[0,rows[').".format(self, Range))

			LastVal=eval(Right[:-1], self.Vars.copy())
			if Right.endswith('['): LastVal-=1
			elif Left.endswith(']'): pass
			else: logging.error("[Module '{0}'] Wrong range description '{1}'. Expected brakets range format (ex: '[0,rows[').".format(self, Range))
		
			return Var, InitVal, LastVal
		else:
			return None, 0, 0
		
	#---------------------------------------------------------------
	def AddReqServ(self, SName, SType, SVersion, mapping={}, UniqueName=""):
		"""
		Add a required service (defined by SName, SType, SVersion) to this module.
		"""
		# Find a unique name for the service in that module
#		Index=0
#		while('#'.join([SName, SType, SVersion, "{0}_{1}".format(UniqueName, Index)]) in self.ReqServ): Index+=1
		
		# Create ID and store in dictionary
		ID='#'.join([SName, SType, SVersion, UniqueName])
		self.ReqServ[ID]=[None, mapping]
		
	#---------------------------------------------------------------
	def AddServ(self, SName, SAlias, mapping):
		"""
		Add a service (defined by name and alias) to this module.
		"""
		self.ServAlias[SAlias]  = SName
		self.OfferedServ[SName] = mapping
		
	#---------------------------------------------------------------
	def AddParameter(self, PName, PType, PTypeImport, PSize, PDefault, IdxDict={}):
		"""
		Add a parameter (defined by Name/type/size/default value) to this module.
		"""
		self.Params[PName] = Signal(PName, PType, PTypeImport, PSize, PDefault, Vars=IdxDict.copy())
		# Set dictionary of parameters values.
		Variables=self.Vars.copy()
		Variables.update(IdxDict.copy())
		self.Vars[PName] = self.Params[PName].integers(Base=10, Variables=Variables)
		
		self.DependencyPkg(PTypeImport)
			
	#---------------------------------------------------------------
	def AddPort(self, PName, PDir, PType, PTypeImport, PSize, PDefault, PModifier=None, PShare=False, IdxDict={}):
		"""
		Add a port (defined by Name/type/size/default value) to this module.
		"""
		self.Ports[PName] = Signal(PName, PType, PTypeImport, PSize, PDefault, PDir, PModifier, bool(PShare), Vars=IdxDict.copy())
		
		self.DependencyPkg(PTypeImport)
	
	#---------------------------------------------------------------
	def AddSource(self, SrcPath, Synthesizable=False):
		"""
		Add a source file to this module.
		"""
		CurDir = os.path.abspath('./')
		os.chdir(os.path.dirname(self.FilePath))
		if Synthesizable: self.Sources["RTL"].append(os.path.abspath(SrcPath))
		else:             self.Sources["Behavioral"].append(os.path.abspath(SrcPath))
		os.chdir(CurDir)
		
	#---------------------------------------------------------------
	def AddResource(self, Dev, RName, Rused=0):
		"""
		Add a resource information for a device to this module.
		"""
		if not list(self.Resources.keys()).count(Dev): self.Resources[Dev]={}
		self.Resources[Dev][RName]=Rused
		
	#---------------------------------------------------------------
	def IsAbstract(self):
		"""
		Return False if RequiredServices list is empty, True otherwise.
		"""
		return len(self.Sources["RTL"]+self.Sources["Behavioral"])==0
		
	#---------------------------------------------------------------
	def GetSources(self, Synthesizable=True):
		"""
		Return the list of source files of this module and its dependencies.
		"""
		SrcList=[]
		if Synthesizable: TYPE = "RTL"
		else: TYPE = "Behavioral"
		for Src in self.Sources[TYPE]:
			SrcList.append(Src)
		for Serv, Map in list(self.ReqServ.values()):
			if Serv:
				SubMod = Serv.GetModule()
				if SubMod: SrcList+=SubMod.GetSources(Synthesizable)
			else:
				logging.error("[Module {0}] Required service missing: source fetch skipped.".format(self))
			
		return SrcList
		
	#---------------------------------------------------------------
	def Offer(self, SName):
		"""
		Return True if this module implement the specified service (identified by its name).
		"""
		if list(self.OfferedServ.keys()).count(SName): return True
		else: return False
		
	#---------------------------------------------------------------
	def GenSrc(self, Synthesizable=True, OutputDir="./Output", TestBench=False, IsTop=True):
		"""
		Generate source files or copy sources to output directory.
		Return True if success.
		"""
		if not os.path.isdir(OutputDir): os.makedirs(OutputDir)
		#----TestBench variables----
		FileList = []
		#---------------------------
		if Synthesizable: TYPE = "RTL"
		else: TYPE = "Behavioral"
		ArchName=TYPE
		self.IntSignals={}
			
		if self.IsAbstract():
			logging.debug("[Module '{0}'] Abstract module : generate source.".format(self))
			ArchName = "RTL"
			# Build a top and Instantiate children
			TopPath = os.path.join(OutputDir, self.Name+HDL.ExtensionDict["VHDL"])
			with open(TopPath, 'w+') as TopFile:
				# Write VHDL Header
				TopFile.write(HDL.Header(self.Name, self.Title, self.Purpose, self.Desc, self.Issues, self.Speed, self.Area, self.Tool, self.Version))
				# Write Library call
				TopFile.write(HDL.Libraries(["IEEE",]))
				TopFile.write(HDL.Packages(["IEEE.std_logic_1164",]))
				TopFile.write(HDL.Packages(["IEEE.std_logic_signed",]))
				TopFile.write(HDL.Packages(["IEEE.std_logic_arith",]))
				# Define architecture
				Content=""
				Declarations=""
		
				# Declare all constants -----------------------------------------
				for BaseName, CDict in self.Const.items():
					for C in sorted(CDict.keys()):
						Declarations+=CDict[C].Declare(Constant=True)
				
				for ServID in sorted(self.ReqServ.keys()): # Service connection
					Serv, ModMap = self.ReqServ[ServID]
					if Serv:						
						# Propagate orthogonal signals upward through the hierarchy
						self.AddOrtho(self, Serv, ModMap)
						for ParameterName, Parameter in Serv.Params.items(): # For each service parameter
							if list(ModMap.keys()).count(ParameterName): # if mapped to module
								# Change its value
								Actual=ModMap[ParameterName] # Actual=[Instance, SignalName, Index]
								Val=eval(str(Actual[1]), self.Vars.copy())
								Parameter.Value=str(Val)
						FileList += Serv.GenSrc(Synthesizable, OutputDir, IsTop=False)
						SubMod = Serv.GetModule()
						if not SubMod: 
							logging.warning("Service {0} ignored due to absence of implementation module.".format(Serv))
						else: 
							# For Python 2.7 and more :
							#self.OrthoPorts.update({v.Name:v for k,v in SubMod.OrthoPorts.items()})
							
							# For older version of python :
							for Key, Val in list(SubMod.OrthoPorts.items()):
								self.OrthoPorts[Key]=Val
							
							# Switch from Service mapping to Module Mapping
							SubServMap = SubMod.OfferedServ[Serv.Name]
							# First map services
							PortDict, GenericDict, Cont, Dec = self.BuildMap(Serv, ModMap, SubMod, SubServMap, ServID.split('#')[-1])	
							# Instantiate sub-modules
							Content+=HDL.Instantiate(ServID.split('#')[-1], SubMod.Name, TYPE, PortDict, GenericDict, Comment=str(SubMod.Title))
							Content      += Cont+'\n'+"-"*65
							Declarations += Dec
							# Declare internal signals
	#						for Actual, Formal in PortDict.iteritems():
	#							if self.Params.keys().count(Formal):
	#								S=self.Params[Formal].HDLFormat(self.Vars.copy())
	#								Declarations+=S.Declare()
					else: logging.warning("[Module {0}] no service associated with {1}.".format(self, ServID))
				# Declare all internal signals-----------------------------------
				for IntSigName in sorted(self.IntSignals.keys()):
					Declarations+=self.IntSignals[IntSigName].HDLFormat().Declare()
					
				Generics=[x.HDLFormat(self.Vars.copy()) for x in list(self.Params.values())]				
				list(map(lambda x: self.Vars.update({x.Name:x.InitVal}), Generics))
				OrthoPorts = RemoveDuplicated(list(self.OrthoPorts.values()))
				Ports=[x.HDLFormat(self.Vars.copy()) for x in list(self.Ports.values())+OrthoPorts]
				# Generate package file
				self.CollectPkg()
				if IsTop==True: Pkg, PkgPath = self.GenPackage(self.Name, OutputDir, Vars=self.Vars.copy())
				else: Pkg=None
				if Pkg: TopFile.write(HDL.Packages(["work."+Pkg,]))
				# Write entity---------------------------------------------------
				TopFile.write(HDL.Entity(self.Name, Generics, Ports, self.Purpose))
				# Write Architecture---------------------------------------------
				TopFile.write(HDL.Architecture(ArchName, self.Name, Declarations, Content, self.Desc))	
				if Pkg: FileList.insert(0, PkgPath)
				FileList.append(TopPath)
				
		else: 
			logging.info("[Module '{0}'] Simple module : copy sources.".format(self))
			for Src in self.GetSources(Synthesizable):
				if not os.path.isfile(Src):
					logging.error("Source not reachable: '{0}'".format(Src))
					sys.exit(1)
				else:
					shutil.copy(Src, OutputDir)
					NewSource=os.path.join(OutputDir, os.path.basename(Src))
							
					FileList.append(NewSource)
					
			for Serv, ModMap in list(self.ReqServ.values()): # Service connection
				if Serv:
					self.AddOrtho(self, Serv, ModMap)
					SubMod = Serv.GetModule()
					if SubMod: self.OrthoPorts.update(SubMod.OrthoPorts)
					FileList += Serv.GenSrc(Synthesizable, OutputDir, IsTop=False)
			
		# TESTBENCH GENERATION-------------	
		if TestBench: self.GenTB(FileList, Pkg, TYPE, OutputDir, self.Vars.copy())
		
		FileList+=[os.path.join(OutputDir, os.path.basename(x)) for x in self.Sources[TYPE]]
		logging.debug("{0} is top: {1}".format(self.Name, IsTop))
		if IsTop==True:
			AlreadyModified=[]
			# Add use common package line to all sources
			for SrcPath in FileList[1:]:
				if not AlreadyModified.count(SrcPath):
					AlreadyModified.append(SrcPath)	
					with open(SrcPath,"r") as SrcFile:
						Code = SrcFile.read()
					with open(SrcPath,"w+") as SrcFile:
						SrcFile.write(HDL.Packages(["work."+Pkg,]))
						SrcFile.write('\n'+Code)
			return AlreadyModified
		return FileList
									
	#---------------------------------------------------------------
	def GenTB(self, FileList, Pkg, TYPE, OutputDir, Vars={}):
		"""
		Generate TestBench files 'module_tb.vhd', 'module_simu.do', 'FileList.txt'
		and 'module_tb_io.txt'.
		"""
		# FILE:'module_tb.vhd'-------------------------------------
		TB_FilePath = os.path.join(OutputDir, "{0}_tb.vhd".format(self.Name))
		FileList.append(TB_FilePath)
		with open(TB_FilePath, 'w+') as TB_File:
			# Write TB VHDL Header
			TB_File.write(HDL.Header("{0}_tb".format(self.Name), 
						"Generate {0}'s stimuli".format(self.Name), 
						"Perform testbench", 
						"Use TextIO module to get stimuli.", 
						"no known issues", 
						"No speed information", 
						"No area information", 
						"Modelsim 6.5", 
						self.Version))
			# Write TB Library call
			TB_File.write(HDL.Libraries(["IEEE",]))
			TB_File.write(HDL.Packages(["IEEE.std_logic_1164",]))
			TB_File.write(HDL.Packages(["IEEE.std_logic_signed",]))
			TB_File.write(HDL.Packages(["IEEE.std_logic_arith",]))
			TB_File.write(HDL.Packages(["std.textio",]))
			if Pkg: TB_File.write(HDL.Packages(["work."+Pkg,]))
			# Write TB entity---------------------------------------------------
			Generics=[]
			Ports=[]
			TB_File.write(HDL.Entity("{0}_tb".format(self.Name), Generics, Ports, "{0}'s testbench module".format(self.Name)))
			# Define TB architecture--------------------------------------------
			Content="\n"
			
			Params  = list(self.Params.values())
			Signals = RemoveDuplicated(list(self.OrthoPorts.values()))+list(self.Ports.values())
			Aliases = []
			PortDict={}
			GenericDict={}
			for Par in Params : GenericDict[Par.Name]=Par.HDLFormat(Vars.copy())
			for Sig in Signals: PortDict[Sig.Name]=Sig.HDLFormat(Vars.copy())
			Content+=HDL.Instantiate('DUT_'+self.Name, self.Name, TYPE, PortDict, GenericDict, Comment=str("Instantiate DUT module"))# Instantiate DUT module
	
			Declarations="\n"
			Stim = Signal("Stimuli", Type="logic", Size=99999)
			
			for P in Params : 
				Declarations+=P.HDLFormat(Vars.copy()).Declare(Constant=True)
			Declarations+='\n'
			CurIdx=0
			for S in Signals : 
				if S.Dir.upper()=="OUT":
					Declarations+=S.HDLFormat(Vars.copy()).Declare()
				elif S.Type.lower()=="logic":
					# Declare alias
					Declarations+=S.HDLFormat(Vars.copy()).AliasOf(Stim.HDLFormat(Vars.copy())[CurIdx:eval(str(S.FullSize), Vars)+CurIdx])
					Aliases.append(S)
					CurIdx+=eval(S.FullSize, Vars)
				elif S.Type.lower()=="numeric":
					Declarations+=S.HDLFormat(Vars.copy()).Declare()
					# Declare alias
					S_bits=S.Copy()
					S_bits.Name="TO_INTEGER({0})".format(S.Name+"_bits")
					Content+=S.HDLFormat(Vars.copy()).Connect(S_bits.HDLFormat(Vars.copy()))
					S_bits.Name=S.Name+"_bits"
					S_bits.Type="logic"
					Declarations+=S_bits.HDLFormat(Vars.copy()).AliasOf(Stim.HDLFormat(Vars.copy())[CurIdx:eval(str(S.FullSize), Vars)+CurIdx])
					Aliases.append(S_bits)
					CurIdx+=eval(S_bits.FullSize, Vars)
				else:
					# Declare alias
					S.ComputeType()
					Declarations+=S.HDLFormat(Vars.copy()).Declare()
					S_bits=S.Copy()
					S_bits.TypeImport=None
					#logging.debug("Vars={0}".format(Vars))
					S_bits.Size=eval(str(S.Size)+'*'+S.Type.split('*')[0], Vars)
					S_bits.FullSize=S_bits.Size
					S_bits.Type="logic"
					S_bits.Name+="_bits"
					S_bits2=S_bits.Copy()
					S_bits2.Name+="_link"
					S_bits2_bis=S_bits2.Copy()
					S_bits2_bis.Name+="((i+1)*{0}-1 downto i*{0})".format(S.Type.split('*')[0])
					Content+='\n'+HDL.For(
							Name="ArrayToSig_{0}".format(S.Name), 
							Var='i', 
							Start=0, 
							Stop=str(S.FullSize)+"-1", 
							Content=S.HDLFormat(Vars.copy())['i'].Connect(S_bits2_bis.HDLFormat(Vars.copy())), 
							Comments="TB array to signal conversion")
					Declarations+=S_bits.HDLFormat(Vars.copy()).AliasOf(Stim.HDLFormat(Vars.copy())[CurIdx:S_bits.Size+CurIdx])
					HDL_S_bits2=S_bits2.HDLFormat(Vars.copy())
					Declarations+=HDL_S_bits2.Declare()
					Content+='\n'+S_bits2.HDLFormat(Vars.copy()).Connect(S_bits.HDLFormat(Vars.copy()))
					Aliases.append(S_bits)
					CurIdx+=S_bits.Size
										
			Stim.Size=CurIdx
			Stim.FullSize=CurIdx	
			StimSize = Signal("NbStimuli", Type="numeric", Default=CurIdx)
			Declarations=StimSize.HDLFormat(Vars.copy()).Declare(Constant=True)+Declarations
			Declarations=Stim.HDLFormat(Vars.copy()).Declare()+Declarations
			Content+=GetTBProcess(self.Name)
			TB_File.write(HDL.Architecture("TestIO_TB", "{0}_tb".format(self.Name), Declarations, Content, "Use TextIO module to get stimuli."))
			
		# 'module_simu.do'---------------------------------------
		DO_FilePath = os.path.join(OutputDir, '{0}_simu.do'.format(self.Name))
		with open(DO_FilePath, 'w+') as DO_File:
			DO_File.write("vlib work\n")
			DO_File.write("vmap work\n")
			DO_File.write("vcom -work work -f FileList.txt\n")
			DO_File.write('vsim -voptargs="+acc" +notimingchecks -L work -t "1ps" work.{0}_tb\n'.format(self.Name))
			DO_File.write("do wave.do\n")
			DO_File.write("run -all\n\n")
			
		# 'FileList.txt'-----------------------------------------
		LIST_FilePath = os.path.join(OutputDir, "FileList.txt")
		AlreadyWritten=[]
		with open(LIST_FilePath, 'w+') as LIST_File:
			for FilePath in FileList:
				FileName=os.path.basename(FilePath)	
				if not 	AlreadyWritten.count(FileName):
					LIST_File.write(FileName+'\n')
					AlreadyWritten.append(FileName)
		
		# 'module_tb_io.txt'-------------------------------------
		IO_FilePath = os.path.join(OutputDir, '{0}_tb_io.txt'.format(self.Name))
		Aliases.reverse()
		Aliases_names = [x.Name for x in Aliases]
		with open(IO_FilePath, 'w+') as IO_File:
			# Write header of textIO file
			IO_File.write("# Test vectors for {0} ports\n".format(self.Name))
			IO_File.write("# 0 means force 0\n")
			IO_File.write("# 1 means force 1\n")
			IO_File.write("# L means expect 0\n")
			IO_File.write("# H means expect 1\n")
			IO_File.write("# X means don't care\n")
			IO_File.write("#"*50+'\n')
			NTemplates=[]
			Names=[]
			for i, Alias in enumerate(Aliases):
				NTemplates.append("{0:"+str(eval(str(Alias.FullSize), Vars))+"}")
				Names.append(NTemplates[-1].format(Aliases_names[i]))
			IO_File.write("#Time   "+(' '*4).join(Names))
			# Create template for line formating
			Template=""
			for n, S in enumerate(Aliases):	
				SSize = eval(str(S.FullSize), Vars)
				Template+="{"+"{0}:0".format(n+1)+str(SSize)+"b}"+' '*4
				if len(S.Name)>SSize: Template+=" "*(len(S.Name)-SSize)
			Template='\n'+"{0:3}"+" "*5+Template
			# Write every stimuli line of textIO file
			MaxTime=300
			TimeStep=5
			for t in range(0, MaxTime, TimeStep):
				Aliases_val=[t,]
				for n, S in enumerate(Aliases):	
					MaxVal = math.pow(2,eval(str(S.FullSize), Vars))-1
					Aliases_val.append(random.randrange(0, MaxVal))
				IO_File.write(Template.format(*Aliases_val))
		return 
			
	#---------------------------------------------------------------
	def BuildMap(self, Service, ModMap, SubMod, SubServMap, InstanceName):
		"""
		Fill PortDict, GenericDict, declaration and content for interconnections.
		"""	
		Connections={} # Dictionary of connections to perform in content area
		# Sort Generics and ports
		GenericDict={}
		PortDict={}
		OrthoMap={}
		for Actual, FormalSig in SubMod.OrthoPorts.items():
			OrthoMap[Actual]=FormalSig.Name
		PortDict.update(OrthoMap)
		
		# Sort so as to begin by parameters before ports (to save generic values)
		ModuleMap=[]
		for Formal in list(ModMap.keys()):
			if list(SubMod.Params.keys()).count(SubServMap[Formal.split(':')[0]]):
				ModuleMap.insert(0, [Formal, ModMap[Formal]])
			else:
				ModuleMap.append([Formal, ModMap[Formal]])
		#---------------START MAPPING--------------------------------------
		for Formal, ActualCond in ModuleMap:
			Actuals, Condition, Vars = ActualCond	
			
			FormalSig=None
			if Formal:
				# If formal is a slice: remember for signal link.
				SplitFormal = Formal.split(':')	
				if SplitFormal[0]=='': SplitFormal[0]=Formal
				try: FormalSig = SubServMap[SplitFormal[0]]
				except: # Service parameter not used in this module
					FormalSig=None
					logging.warning("[Module {0}] Service parameter '{1}' not used in this module".format(self, SplitFormal[0]))
					
			if FormalSig:
				#---------------PROCESS THE ACTUAL EXPRESSION-----------------------
				CopyVars=self.Vars.copy()
				CopyVars.update(Vars)
				# if condition is not realized actual is default value, else
				if eval(str(Condition), CopyVars.copy()):
					ConcatList=[] # List of signals to concatenate
					for InstName, SigName, Index in Actuals:
						# Get actual name
						try: SName=eval(SigName) # actual is a constant number
						except: SName=""
						if isinstance(SName, int): SName=str(SName)
						elif InstName!='': SName='_'.join([InstName, SigName]);
						else: 
							if not list(self.Ports.keys()).count(SigName) and not list(self.Params.keys()).count(SigName): 
								SName='_'.join([InstanceName, SigName])
							else: SName=SigName
							
						# Create signal object for actual with formal format
						if list(SubMod.Ports.keys()).count(FormalSig):
							# Case: it's a port signal
							S_HDL=SubMod.Ports[FormalSig].HDLFormat(SubMod.Vars.copy())
							S2=SubMod.Ports[FormalSig].Copy()
						else:
							# Case: it's a parameter signal
							S_HDL=SubMod.Params[FormalSig].HDLFormat(SubMod.Vars.copy())
							S2=SubMod.Params[FormalSig].Copy()
							# Update parameter dictionary for this instance
							if Index!=None: SubMod.Vars[FormalSig]=eval(SName+"[{0}]".format(Index), CopyVars.copy())
							else: SubMod.Vars[FormalSig]=eval(SName, CopyVars.copy()) 
						S_HDL.Name=SName
						S2.Name=SName
						# If it's an internal connection : declare new
						if not list(self.IntSignals.keys()).count(SName) and not list(self.Ports.keys()).count(SigName) and not list(self.Params.keys()).count(SigName):# and Index==None:
							try: eval(SName) # actual is a constant number: don't declare
							except: # actual is a signal name
								S2.Vars.update(SubMod.Vars)
								self.IntSignals[SName]=S2
								self.Vars.update(SubMod.Vars.copy())
							
						NewSig = S_HDL.Copy()
						NewSig.SetIndex(Index)
						ConcatList.append(NewSig)
					# Build a signal resulting from concatenation of indexed signals
					Actual=reduce(lambda x,y: x+y, ConcatList)
				else:
					# Set None to later fetch default value of formal
					Actual=None
								
				# For Python 2.7 and more :
				#ReverseServMap = {v:k for k,v in SubServMap.items()}
				
				# For older version of python :
				ReverseServMap={}
				for k,v in list(SubServMap.items()):
					ReverseServMap[v]=k
				#---------------PROCESS THE FORMAL EXPRESSION-----------------------
				if len(SplitFormal)>1: # Formal signal is indexed: Create intermediate sig
					Index = SplitFormal[1]
					NewSig = InstanceName+'_'+ReverseServMap[FormalSig]
					# Create New internal signal
					S_HDL=SubMod.Ports[FormalSig].HDLFormat(SubMod.Vars.copy())
					S_HDL.Name=NewSig
					# If it's an internal connection : declare signal
					if not list(self.IntSignals.keys()).count(SName):
						self.IntSignals[NewSig]=SubMod.Ports[FormalSig].Copy()
						self.IntSignals[NewSig].Vars.update(SubMod.Vars)
						self.IntSignals[NewSig].Name=NewSig
						
					Linked=SubMod.Ports[FormalSig].Copy()
					Linked.Name=NewSig
					# Add or update connection dictionary----
					ConnectionDict=None
					for SigToConnect in list(Connections.keys()):
						if SigToConnect.Name==Linked.Name:
							ConnectionDict=Connections[SigToConnect]
					if ConnectionDict==None: 
						Connections[Linked]={}
						ConnectionDict=Connections[Linked]
					
					if Actual==None: # Set default value if no actual signal
						Actual=S_HDL.Copy()
						Actual.SetIndex(Index)
						Actual.Name=Actual.GetValue(Vars=CopyVars.copy())
						Actual.SetIndex(None)
						Actual=S_HDL
					else: ConnectionDict[eval(Index, CopyVars.copy())]=Actual
					# Declare this new signal
					
					# Add as port map (if 'formal' is a sub module port)
					if list(SubMod.Ports.keys()).count(FormalSig):
						PortDict[FormalSig]=S_HDL
					# or as generic map (if 'formal' is a sub module parameter)
					elif list(SubMod.Params.keys()).count(FormalSig):
						GenericDict[FormalSig]=S_HDL
					else:
						logging.error("No such formal {1} signal mapped to submodule {0}".format(SubMod, FormalSig))
				else:
					# Add as portmap if 'formal' is a sub module port
					if list(SubMod.Ports.keys()).count(FormalSig):
						if Actual!=None: 
							# Add as Port signal
							PortDict[FormalSig]=Actual
						else: PortDict[FormalSig]=SubMod.Ports[FormalSig].HDLFormat(SubMod.Vars.copy()).GetValue()
					# Add as generic if 'formal' is a sub module parameter
					elif list(SubMod.Params.keys()).count(FormalSig):
						if Actual!=None: 
							# Add as generic parameter
							GenericDict[FormalSig]=Actual
						else: GenericDict[FormalSig]=SubMod.Params[FormalSig].HDLFormat(SubMod.Vars.copy()).GetValue()
					else:
						logging.error("No such formal signal mapped to submodule {0}".format(SubMod))
						sys.exit(1)
						
					
			else:
				pass # IO connections	
				
		Declarations="" # Declaration of all linking signals
		Content="" # Content of Architecture <=> Signal linking and instances port mapping
		
		# Now connect signals---------------------------------------------------------------
		for FormalSig in sorted(Connections.keys()):
			ADict=Connections[FormalSig]
			Zeros=self.GetMissing(ADict, FormalSig, SubMod.Vars.copy())
			#ConcatList=map(lambda x: ADict[x], sorted(ADict.keys()))
			#ActualSig=reduce(lambda x,y: x+y, ConcatList)
			try: FSig=FormalSig.HDLFormat(SubMod.Vars.copy())
			except: FSig=FormalSig.HDLFormat(SubMod.Vars.copy())
			#if len(SplitFormal[1:]): FSig.SetIndex(*SplitFormal[1:])
			#FSig.Name=FormalSig
		
			FullConnections=ADict.copy()
			FullConnections.update(Zeros)
			for Index in sorted(FullConnections.keys()):
				ActualSig=FullConnections[Index]
				FSig.SetIndex(Index)
#				if Index in Zeros: UseValue=True
#				else: UseValue=False
				if Index not in Zeros:
					if ActualSig.Direction.upper()=="OUT":
						Content+=ActualSig.Connect(FSig, False)
					else:
						Content+=FSig.Connect(ActualSig, False)
					
					
		return PortDict, GenericDict, Content, Declarations
				
	#---------------------------------------------------------------
	def AddOrtho(self, Mod, Serv, ReqMap):
		"""
		Propagate orthogonal signals upward through the hierarchy
		"""
		if Serv.IsOrthogonal():
			for PortName, P in Serv.Ports.items():
				# Determine formal ortho port name
				if P.Modifier!=None and P.Modifier!="": 
					# if modifier specified: append its value
					if list(ReqMap.keys()).count(P.Modifier):
						Suffix=GetActualName(ReqMap[P.Modifier], Vars=self.Vars.copy())
					else:
					# if modifier is not specified: append default value
						if Serv.Params[P.Modifier].Default:
							Suffix=Serv.Params[P.Modifier].Default
					# else do not append anything
						else: Suffix=None
					# Rename port according to modifier
					if Suffix: PName=P.Name+'_'+Suffix
					else: PName=P.Name
				else:
					PName=PortName
					
				# create signal for ortho port 
				Port = P.Copy()
				Port.Name = PName
				# Set opposite direction for formal ortho port 
				if Port.Dir=="IN": Port.Dir="OUT"
				else: Port.Dir="IN"
				# If Orthogonal signal is mapped to submodule actual signal : 
#				if ReqMap.keys().count(PortName):
#					Mod.Ports[PName]=Port # Add it to module port
#				# If Orthogonal signal is not mapped to actual signal
#				else:
#					# Add P to module port if not already
#					if not Mod.Ports.keys().count(PName):
#						Mod.Ports[PName]=Port
				ActualList, Cond, IdxDict = ReqMap[PortName]
				InstName, SigName, Index = ActualList[0] # TODO Maybe use condition
				Mod.OrthoPorts[SigName]=Port
#		else:
#			logging.debug("[Service {0}] Service not orthogonal.".format(Serv))
		# 
	
	#---------------------------------------------------------------
	def GetMissing(self, Connections, Sig, Vars={}):
		"""
		Return a dictionary for connection to default of signals missing in Connections.
		Connections={Index0: ActualSig0, Index1: ActualSig1 (...) }
		"""
		Zeros={}
		for i in range(eval(str(Sig.Size), Vars)):
			if not list(Connections.keys()).count(i):
				Zero=Sig.Copy().HDLFormat(Vars.copy())
				Zero.SetIndex(i)
				Zeros[i]=Zero
		return Zeros
		
	#---------------------------------------------------------------
	def CollectPkg(self):
		"""
		Update PkgObject package dictionary with this package.
		"""
		for Child in list(self.Params.values())+list(self.Ports.values())+[x[0] for x in list(self.ReqServ.values())]+list(self.IntSignals.values()):
			if Child!=None:
				Child.CollectPkg()
				PackageBuilder.CollectPkg(self, Child)
		for Const in list(self.Package["Constants"].keys()):
			if list(self.Params.keys()).count(Const):
				self.Package["Constants"][Const]=self.Params[Const].HDLFormat(self.Vars.copy())
		
	#---------------------------------------------------------------
	def DependencyPkg(self, TypeImport):
		"""
		Add dependency package to empty package list. 
		They will be declared empty in top package. 
		The top package declare all type/constants.
		This will prevent error when calling old packages.
		"""
		if TypeImport!=None:
			try:
				Pkg, NewType = TypeImport.split('.')
				self.Package["EmptyPkg"].append(Pkg)
			except:
				logging.debug("[{0}] TypeImport format '{1}' don't specify a package source.".format(self,TypeImport))
		
	#---------------------------------------------------------------
	def Display(self):
		"""
		Display service parameters in log.
		"""
		logging.info("MODULE:")
		logging.info("\tname='{0}', Version='{1}'".format(self.Name, self.Version))
		logging.info("\tTitle   ="+self.Title+"(Speed={0}, Area={1}, Tool={2}, Issues={3})".format(self.Speed, self.Area, self.Tool, self.Issues))
		logging.info("\tPurpose ="+self.Purpose)
		logging.info("\tDesc    ="+self.Desc)
		logging.info("\tParameters ="+str(list(self.Params.keys())))
		logging.info("\tPorts      ="+str(list(self.Ports.keys())))
		logging.info("\tServices implemented ="+str(list(self.OfferedServ.keys())))
		logging.info("\tServices required    ="+str(list(self.ReqServ.keys())))
		logging.info("\tSources:")
		for Type in list(self.Sources.keys()):
			logging.info("\t\t*{0}".format(Type))
			for S in self.Sources[Type]:
				logging.info("\t\t\t{0}".format(S))
		logging.info("\tResources ="+str(self.Resources))
		
	#---------------------------------------------------------------
	def __str__(self):
		return self.Name
		
#=======================================================================
class Signal(PackageBuilder):
	#---------------------------------------------------------------
	def __init__(self, Name, Type="logic", PTypeImport=None, Size=None, Default=0, Dir=None, Modifier=None, share=False, Vars={}):
		"""
		Gather signal parameters.
		"""
		#logging.debug("New Signal: Name={0}, Type={1}, Size={2}, Default={3}".format(Name, Type, Size, Default))
		PackageBuilder.__init__(self)
		self.Name     = Name
		self.Dir      = Dir
		if Size=="": self.Size=None
		else:        self.Size=Size
		self.FullSize = Size
		self.Type     = Type
		self.TypeImport=PTypeImport
		self.Default  = Default
		self.Modifier = Modifier
		self.share    = share
		self.Vars     = Vars.copy()
		if self.Default: self.Value = self.Default
		else: self.Value = 0
#		if not self.Size: 
#			try: 
#				if self.Value:
#					self.Size=int(math.log(float(self.Value), 2))+1
#				else: self.Size=1
#			except: 
#				self.Size=self.Value

	#---------------------------------------------------------------
	def __len__(self):
		return self.FullSize
			
	#---------------------------------------------------------------
	def CollectPkg(self):
		"""
		Nothing to collect: pass.
		"""
		pass
				
	#---------------------------------------------------------------
	def ComputeType(self):
		"""
		Analyze Type content and generate a synthesizable type format from it.
		When necessary, add type and constant and packages to package object.
		"""
		Type=self.Type.strip('(').strip(')')
		if self.TypeImport!=None and self.TypeImport!="":
			# Keep specified type
			try: 
				Pkg, NewType = self.TypeImport.split('.')
				S, T = ParseType(Type)
				self.Package["TypeImport"][NewType] = [S, T, Pkg]
				# Eventually add a constant declaration to package
				if S!=None:
					try: 
						self.FullSize=self.Size*int(S)
					except: 
						# Build signal for constant declaration
						for C0 in S.split('*'):
							for C1 in C0.split('+'):
								for C2 in C1.split('-'):
									for C3 in C2.split('/'):
										self.Package["Constants"][C3]=Signal(C3).HDLFormat(self.Vars.copy())
						self.FullSize=str(self.Size)+'*'+str(S)
				NewType=NewType.replace('*',"_x_").replace( '+',"_plus_").replace('-',"_minus_").replace('/',"_divby_").replace('(',"").replace(')',"")	
				return NewType
			except: return self.TypeImport
			
		if Type=="logic" or Type=="numeric": return Type				
		else:
			S, T = ParseType(Type)
			if S!=None: # If type is complex (has an array subtype)
				# Create new type from size * subtype
				NewType="{0}_{1}".format(str(S).upper(), T.upper())
				
				NewType=NewType.replace('*',"_x_").replace( '+',"_plus_").replace('-',"_minus_").replace('/',"_divby_").replace('(',"").replace(')',"")	
				# Create a whole new type
				self.Package["Types"][NewType]=[NewType, S, T]
				# Update FullSize value
				try: 
					self.FullSize=self.Size*int(S)
				except: 
					for C0 in S.split('*'):
						for C1 in C0.split('+'):
							for C2 in C1.split('-'):
								for C3 in C2.split('/'):
									self.Package["Constants"][C3]=Signal(C3).HDLFormat(self.Vars.copy())
					self.FullSize=str(self.Size)+'*'+str(S)
				 
				return NewType	
			else:
				return Type
		
	#---------------------------------------------------------------
	def HDLFormat(self, Vars={}):
		Variables = Vars.copy()
		Variables.update(self.Vars)
		
		if self.Default!="open" and self.Default:
			if isinstance(self.Default, str):
				Default=[]
				for Elmt in self.Default.split(';'):
					Default.append(eval(str(Elmt), Variables))
				if len(Default)==1:
					Default=Default[0]
			else: Default=self.Default
				
		else: Default=0
		
		if self.Size!="" and self.Size!=None:
			try: Size=int(self.Size, 10)
			except: 
				Size=eval(str(self.Size), Variables)
#				try: int(str(Size), 10)
#				except: 
#					if Default==None: Size=2
#					else: 
#						if isinstance(Default, list): Size=
#						Size=int(math.log(float(str(self.Default)), 2))
		else:
			Size=None
		
		return HDL.Signal(Item=self.Name, Direction=self.Dir, Size=Size, Type=self.ComputeType(), InitVal=Default)
		
	#---------------------------------------------------------------
	def Copy(self):
		return Signal(Name=self.Name, Type=self.Type, PTypeImport=self.TypeImport, Size=self.Size, Default=self.Default, Dir=self.Dir, Modifier=self.Modifier, share=self.share)
		
	#---------------------------------------------------------------
	def integers(self, Base=10, Variables={}):
		if isinstance(self.Value, list): return [int(str(eval(str(x), Variables)), Base) for x in self.Value]
		elif isinstance(self.Value, str): 
			if len(self.Value.split(';'))>1:
				return [int(str(eval(str(x), Variables)), Base) for x in self.Value.split(';')]
			else: return int(str(eval(str(self.Value.split(';')[0]), Variables)), Base)
		else: return int(str(eval(str(self.Value), Variables)), Base)
	
	#---------------------------------------------------------------
	def __repr__(self):
		return "{0}[Dir={1}]{2}bit({3})={4}#MOD({5})#Share({6} - CUSTOMTYPE={7})".format(self.Name, self.Dir, self.Size, self.Type, self.Default, self.Modifier, self.share, self.TypeImport)
	
	#---------------------------------------------------------------
	def __str__(self):
		return "{0}[Dir={1}]{2}bit({3})={4}#MOD({5})#Share({6} - CUSTOMTYPE={7})".format(self.Name, self.Dir, self.Size, self.Type, self.Default, self.Modifier, self.share, self.TypeImport)
		
#=======================================================================
def SigName(Code, OrderedIndexes=[], Vars={}, LocalParams={}):
	"""
	Parse signal name and return normalized name format.
	"""
	if Code.startswith('{') and Code.endswith('}'):
		ToConcatList = [S.strip('{').strip('}') for S in Code.split(',')]
	else:
		ToConcatList = [Code,]
	IndexedSigList = []
	#logging.debug("Code={0}".format(Code))
	for ToConcatSig in ToConcatList:
		try: Inst, Sig = ToConcatSig.split('.') # Separate signal name from instance name
		except: Inst, Sig = "", ToConcatSig
		# split instance name and indexes
		InstID = Inst.split(':')
		if InstID[0]=='': InstID[0]=Inst 
		#logging.debug("InstID={0}".format(InstID))
		
		for i in range(1, len(InstID)):
			try: InstID[i] = OrderedIndexes[i-1]+str(eval(InstID[i], Vars))
			except: logging.critical('Failed to get instance name for "{0}" in a loop. Make sur your indexed it (name="instance:var")'.format(InstID))
			
		if len(InstID)>1: InstName = "_".join([InstID[0], "".join(InstID[1:])])
		else: InstName = InstID[0]
		
		# Fetch local constants
		if list(LocalParams.keys()).count(Sig): Sig=LocalParams[Sig]
		# split signal name and indexes
		SigID = Sig.split(':')
		SigName = SigID[0]
		Index=None
		if len(SigID)>1:
			Range = SigID[1].split('~')
			Min=eval(Range[0], Vars)
			if len(Range)>1: 
				Max=eval(Range[1], Vars)
				Index=[Min, Max]
			else:
				Index=Min
		else:
			Index=None
		for i in range(1, len(SigID)):
			SigID[i] = eval(SigID[i], Vars)
		
		IndexedSigList.append([InstName, SigName, Index])
		if SigName=='': sys.exit(1)
	return IndexedSigList
		
#=======================================================================
def ServName(Code, Vars={}):
	"""
	Parse required service name and return normalized name format.
	"""
	ServID = Code.split(':')
	if ServID[0]=='': ServID[0] = Code
	if len(ServID)>1:
		for i in range(1, len(ServID)):
			ServID[i]=ServID[i]+str(eval(ServID[i], Vars))
		InstanceName = "_".join([ServID[0], "".join(ServID[1:])])
	else: InstanceName = ServID[0]
	return InstanceName
		
		
##============================================================================
#def UniqueName(SigName, SigList):
#	"""
#	Return a unique name for a signal by appending '_' + a number.
#	The returned name is garantied not to be in SigList. 
#	"""
#	Index = 0
#	while SigList.count(SigName+'_'+str(Index)):
#		Index+=1
#	return SigName+'_'+str(Index)
#		
#============================================================================
def GetActualName(ActualList, Vars={}):
	"""
	Return a value (string format) of actual signal described by a list.
	"""
	ConcatList, Cond, LocalVars = ActualList
	Variables=Vars.copy()
	Variables.update(Vars)
	if eval(str(Cond), Variables):
		for Inst, SigVal, Index in ConcatList:
			if len(Inst)<1:
				if Index==None:
					return str(SigVal)
				else:
					S=Signal(SigVal)
					S.SetIndex(Index)
					return S.Name
			else:
				if Index==None:
					return str(Inst)+'_'+str(SigVal)
				else:
					S=Signal(str(Inst)+'_'+str(SigVal))
					S.SetIndex(Index)
					return S.Name
					
	else:
		return None
		
#============================================================================
def RemoveDuplicated(SignalList):
	"""
	Build a new list and fill it with a set of unique signals from argument list.
	"""
	SignalSet=[]
	SignalNameSet=[]
	for Sig in SignalList:
		if not SignalNameSet.count(Sig.Name):
			SignalSet.append(Sig)
			SignalNameSet.append(Sig.Name)
	
	return SignalSet

#============================================================================
def GetTBProcess(DUT):
	"""
	Return VHDL code for the read of textio stimuli file.
	"""
	return """
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
TEST: process
  file TestFile : text is in "./{0}_tb_io.txt";
  variable L           : line;
  variable TimeVector  : time;
  variable R           : real;
  variable good_number : boolean;
  variable index       : integer;

begin  -- process Test
    
  --WRITE_STRING (OUTPUT, "*** {0} test ***");
  write(output, "*** Start {0} test ***");

  while not endfile(TestFile) loop
    readline(TestFile, L);
    --write(output, L);
    
    read(L, R, GOOD => good_number);-- read the time from the beginning of the line
    next when not good_number;-- skip the line if it doesn't start with a number
    
    TimeVector := natural(R) * 1 ns; -- convert real number to time
    if (now < TimeVector) then -- wait until vector time
      wait for TimeVector - now;
    end if;
    index := NbStimuli-1;
    
    --For each caracter in line:
    for i in L'range loop
      case L(i) is
        when '0' => -- Drive 0
          Stimuli(index) <= '0';
        when '1' => -- Drive 1
          Stimuli(index) <= '1';
        when 'H' => -- Test for 1
          assert Stimuli(index) = '1';
        when 'L' => -- Test for 0
          assert Stimuli(index) = '0';
        when 'X' => -- Don't care
          null;
        when ' '
          | HT => -- Skip white space
          next;
        when others =>
          -- Illegal character
          assert false report "Illegal char in vector file: " & L(i);
          exit;
      end case;
      index := index-1;
    end loop;                         -- end of line
 
  end loop;                           -- end of file
  
  assert false report "*** Test complete ***";
  wait;
    
end process TEST;""".format(DUT)

#============================================================================
def ParseType(Type):
	"""
	Return subsize and subtype of type according to XML syntaxe.
	Size returned is None when it's not a subtype.
	"""
	Index = Type.rfind('*')
	if Index!=-1: # If type is complex (has subtype)
		# Find subtype size			
		SubSize = Type[:Index].strip('(').strip(')')
#		# Create new type from size * subtype
#		NewType="{0}_{1}".format(SubSize.upper(), Type[Index+1:].upper())
				 
		return SubSize, Type[Index+1:]
	else:
		return None, Type
	
	
# ====================  START OF THE HDLGenerator APPLICATION  =====================
if (__name__ == "__main__"):

	if sys.argv.count("--debug"): LoggingMode = logging.DEBUG # logging.INFO
	elif sys.argv.count("-v") or sys.argv.count("--verbose"): LoggingMode = logging.INFO
	else: LoggingMode = logging.ERROR
	
	logging.basicConfig(format='%(asctime)s | %(levelname)s: %(message)s', level=LoggingMode)

	Lib = XmlLibManager("./lib", [os.path.abspath("./NoC"),])
#------------------------------------------------------------------------
#	logging.info("*** Generate example design from XML description ***")
#	Lib.Service("serviceC").GenSrc(True, "./Test_ServiceC")	
#	logging.info("*** 'serviceC' generated successfully ***")
#	logging.info("*** Generate Hermes NoC design from XML description ***")
#	Lib.Module("NoC").GenSrc(True, "./Test_NoC")	
	#Lib.Module("NoC").Display()
#------------------------------------------------------------------------
#	logging.info("*** Generate Router from XML description ***")
#	Router = Lib.Module("Router")
#	if Router: 
#		Router.GenSrc(Synthesizable=True, OutputDir="./Test_Router", TestBench=True)
#	else: 
#		logging.error("Module 'Router' not found")
#		sys.exit(1)
#	logging.info("*** 'Router' generated successfully ***")
#------------------------------------------------------------------------
#	AdOCNet = Lib.Module("AdOCNet")
#	AdOCNet.Display()
#	if AdOCNet: 
#		AdOCNet.GenSrc(Synthesizable=True, OutputDir="./Test_AdOCNet", TestBench=True)
#	else: 
#		logging.error("Module 'AdOCNet' not found")
#		sys.exit(1)
#	logging.info("*** 'NoC' generated successfully ***")
#	Lib.Display()
#------------------------------------------------------------------------
	Lib.Display(ServiceTree=True)
	sys.exit(0)
	
	
	

