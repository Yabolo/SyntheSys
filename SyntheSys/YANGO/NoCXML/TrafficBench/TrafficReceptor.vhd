----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:15:10 12/09/2009 
-- Design Name: 
-- Module Name:    TrafficReceptor - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.02 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.std_logic_textio.all;
USE STD.TEXTIO.all;

--use work.HermesPackage.all;
--use work.EmulationPackage.all;

-------------------------------------------------------------------------------
-- ENTITY: Traffic receptor for traffic generation (NoC Emulation)
-------------------------------------------------------------------------------
Entity TrafficReceptor is
  generic (
    FlitWidth       : natural :=16;  
    pkt_number      : natural  -- number of packets received in this router.
    );
  port (
    clock  	    : IN  std_logic;
    reset	    : IN  std_logic;
    data_in         : IN  std_logic_vector(FlitWidth-1  downto 0);
    ack_rx          : OUT std_logic;
    rx              : IN  std_logic;
    latency_total   : out integer;   -- Total latency for all the data transmission.
    total_time      : out natural;   -- Total time used for all the data transmission. 
    ReceivedPackets : out natural
    );
END TrafficReceptor;

-------------------------------------------------------------------------------
-- ARCHITECTURE: Version 0
-------------------------------------------------------------------------------
Architecture RTL OF TrafficReceptor IS
  
  signal ReceivedPackets_int : natural :=0;
  
BEGIN

  ReceivedPackets <= ReceivedPackets_int;
  
  -----------------------------------------------------------------------------
  p1:process(reset, clock)
    variable mesure_time : integer:=0;
    variable latency_int : integer:=0;
    variable mesure_time_logic : std_logic_vector(FlitWidth*2-1  downto 0);
    variable count_flit  : integer:=0;
    variable nbre_flit   : integer:=0;
    variable nber_packet : integer:=0;
    variable compteur    : integer;
    
  begin 
    if reset='1' then
      compteur:=0; 
      latency_total<= 0;
      total_time<=0; 
      ReceivedPackets_int<=0;
    else
      if clock='1' and clock'event  then 
        compteur:= compteur + 1;
        
        if rx ='1' then 
          
          if count_flit=1 then
            nbre_flit:= conv_integer(data_in);  -- receive the information for the number of the packets. 
          end if;
          -- receive the information for the latency
          if count_flit=3 then
            mesure_time_logic(FlitWidth-1 downto 0) := data_in;
          elsif count_flit=4 then
            mesure_time_logic(FlitWidth*2-1 downto FlitWidth) := data_in;	
          end if;
          -- count the flit received
          count_flit:= count_flit +1; 
          
          if count_flit= nbre_flit+2 then
            mesure_time := conv_integer(mesure_time_logic);
            latency_int:=latency_int + (compteur - mesure_time); 
            latency_total <= latency_int;
            ReceivedPackets_int<=ReceivedPackets_int+1;
            count_flit:=0;
            nber_packet:=nber_packet+1;
            if nber_packet = pkt_number then
              total_time<= compteur;
            end if;
          end if;
        end if;	
      end if;

    end if;     
  end process p1;
  -----------------------------------------------------------------------------
  
--  p2:process(reset, clock)
--  begin 
--    if rising_edge(clock) then
--      if reset='1' then 
--        ack_rx <= '0';
--      else
--        if rx='1' then
--          ack_rx <= '1'; 
--        else
--    	  ack_rx <= '0'; 
--        end if;
--      end if;
--    end if;
--  end process p2;
  ack_rx <= rx;
  
end architecture RTL;

