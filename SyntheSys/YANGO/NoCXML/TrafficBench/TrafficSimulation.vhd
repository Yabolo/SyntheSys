----------------------------------------------------------------------------------
-- Company:         ADACSYS
-- Engineer:        Matthieu PAYET
-- 
-- Create Date:     25/02/13 
-- Design Name:     TrafficSimulation
-- Module Name:     TrafficSimulation - Behavioral 
-- Project Name:    NoC simulation
-- Target Devices:  
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
Library IEEE;
Use IEEE.std_logic_1164.all;
Use IEEE.std_logic_unsigned.all;
Use IEEE.std_logic_arith.all;
--USE ieee.numeric_std.ALL;
use std.textio.all;
use ieee.std_logic_textio.all;
use work.ROCNet_pkg.all;

-------------------------------------------------------------------------------
-- ENTITY: Traffic generator for one destination simulation
-------------------------------------------------------------------------------
Entity TrafficSimulation IS
end TrafficSimulation;

-------------------------------------------------------------------------------
-- ARCHITECTURE: Behavioral with TEXTIO, Version 0
-------------------------------------------------------------------------------
architecture Behavioral of TrafficSimulation is

  constant DimX : natural :=3;
  constant DimY : natural :=3;
  constant FlitWidth : natural :=16;
  
  signal Clk     : std_logic := '0';
  signal Rst     : std_logic;
  signal Rx      : std_logic_vector(DimX*DimY-1 downto 0);
  signal DataIn  : FLITS(DimX*DimY-1 downto 0);
  signal AckRx   : std_logic_vector(DimX*DimY-1 downto 0);
  signal Tx      : std_logic_vector(DimX*DimY-1 downto 0);
  signal DataOut : FLITS(DimX*DimY-1 downto 0);
  signal AckTx   : std_logic_vector(DimX*DimY-1 downto 0);
  
  
begin

  Clk <= not Clk after 5 ns;
  Rst <= '1', '0' after 10ns;
  -----------------------------------------------------------------------------
  -- ROCNet 3x3 - HandShake
  -----------------------------------------------------------------------------
  ROCNet_1: entity work.ROCNet(RTL)
    generic map (
      FlitWidth => FlitWidth,
      DimX      => DimX,
      DimY      => DimY)
    port map (
      AckTx    => AckTx,
      AckRx    => AckRx,
      Tx       => Tx,
      DataIn   => DataIn,
      Rx       => Rx,
      DataOut  => DataOut,
      reset_5  => Rst,
      clock_50 => Clk);
  
  -----------------------------------------------------------------------------
  RouterConnections: for i in 0 to (DimX*DimY-1) generate

    ---------------------------------------------------------------------------
    TrafficGenerator_1: entity work.TrafficGenerator(Behavioral)
      generic map (
        InputFileName => "./Scenario/In/in"&integer'image(INTEGER(i))&".txt",
        FlitWidth     => FlitWidth,
        LocalAddr     => conv_std_logic_vector(i-(i/DimX)*DimY,FlitWidth/4)&conv_std_logic_vector(i/DimX,FlitWidth/4), --XXYY
        DestAddr_x    => i/DimX,
        DestAddr_y    => i-(i/DimX)*DimY,
        PacketSize    => 16,
        NbPackets     => 8,
        IdleTime      => 30)
      port map (
        Clk    => Clk,
        Rst    => Rst,
        Rx     => Rx(i),
        AckRx  => AckRx(i),
        DataIn => DataIn(i));

    ---------------------------------------------------------------------------
    TrafficReceptor_1: entity work.TrafficReceptor
      generic map (
        FlitWidth     => FlitWidth,
        pkt_number    => 8)
      port map (
        clock         => Clk,
        reset         => Rst,
        data_in       => DataOut(i),
        ack_rx        => AckTx(i),
        rx            => Tx(i),
        latency_total => open,
        total_time    => open);
    
  end generate RouterConnections;
  
end architecture Behavioral;


