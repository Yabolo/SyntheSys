onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {Receptor x0y0}
add wave -noupdate -format Logic /trafficsimulation/routerconnections__0/trafficreceptor_1/clock
add wave -noupdate -format Logic /trafficsimulation/routerconnections__0/trafficreceptor_1/reset
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__0/trafficreceptor_1/data_in
add wave -noupdate -format Logic /trafficsimulation/routerconnections__0/trafficreceptor_1/rx
add wave -noupdate -format Literal /trafficsimulation/routerconnections__0/trafficreceptor_1/latency_total
add wave -noupdate -format Literal /trafficsimulation/routerconnections__0/trafficreceptor_1/total_time
add wave -noupdate -format Literal /trafficsimulation/routerconnections__0/trafficreceptor_1/receivedpackets
add wave -noupdate -divider {Receptor x1y0}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__1/trafficreceptor_1/data_in
add wave -noupdate -format Logic /trafficsimulation/routerconnections__1/trafficreceptor_1/rx
add wave -noupdate -format Literal /trafficsimulation/routerconnections__1/trafficreceptor_1/latency_total
add wave -noupdate -format Literal /trafficsimulation/routerconnections__1/trafficreceptor_1/total_time
add wave -noupdate -format Literal /trafficsimulation/routerconnections__1/trafficreceptor_1/receivedpackets
add wave -noupdate -divider {Receptor x2y0}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__2/trafficreceptor_1/data_in
add wave -noupdate -format Logic /trafficsimulation/routerconnections__2/trafficreceptor_1/rx
add wave -noupdate -format Literal /trafficsimulation/routerconnections__2/trafficreceptor_1/latency_total
add wave -noupdate -format Literal /trafficsimulation/routerconnections__2/trafficreceptor_1/total_time
add wave -noupdate -format Literal /trafficsimulation/routerconnections__2/trafficreceptor_1/receivedpackets
add wave -noupdate -divider {Receptor x0y1}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__3/trafficreceptor_1/data_in
add wave -noupdate -format Logic /trafficsimulation/routerconnections__3/trafficreceptor_1/rx
add wave -noupdate -format Literal /trafficsimulation/routerconnections__3/trafficreceptor_1/latency_total
add wave -noupdate -format Literal /trafficsimulation/routerconnections__3/trafficreceptor_1/total_time
add wave -noupdate -format Literal /trafficsimulation/routerconnections__3/trafficreceptor_1/receivedpackets
add wave -noupdate -divider {Receptor x1y1}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__4/trafficreceptor_1/data_in
add wave -noupdate -format Logic /trafficsimulation/routerconnections__4/trafficreceptor_1/rx
add wave -noupdate -format Literal /trafficsimulation/routerconnections__4/trafficreceptor_1/latency_total
add wave -noupdate -format Literal /trafficsimulation/routerconnections__4/trafficreceptor_1/total_time
add wave -noupdate -format Literal /trafficsimulation/routerconnections__4/trafficreceptor_1/receivedpackets
add wave -noupdate -divider {Receptor x2y1}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__5/trafficreceptor_1/data_in
add wave -noupdate -format Logic /trafficsimulation/routerconnections__5/trafficreceptor_1/rx
add wave -noupdate -format Literal /trafficsimulation/routerconnections__5/trafficreceptor_1/latency_total
add wave -noupdate -format Literal /trafficsimulation/routerconnections__5/trafficreceptor_1/total_time
add wave -noupdate -format Literal /trafficsimulation/routerconnections__5/trafficreceptor_1/receivedpackets
add wave -noupdate -divider {Receptor x0y2}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__6/trafficreceptor_1/data_in
add wave -noupdate -format Logic /trafficsimulation/routerconnections__6/trafficreceptor_1/rx
add wave -noupdate -format Literal /trafficsimulation/routerconnections__6/trafficreceptor_1/latency_total
add wave -noupdate -format Literal /trafficsimulation/routerconnections__6/trafficreceptor_1/total_time
add wave -noupdate -format Literal /trafficsimulation/routerconnections__6/trafficreceptor_1/receivedpackets
add wave -noupdate -divider {Receptor x1y2}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__7/trafficreceptor_1/data_in
add wave -noupdate -format Logic /trafficsimulation/routerconnections__7/trafficreceptor_1/rx
add wave -noupdate -format Literal /trafficsimulation/routerconnections__7/trafficreceptor_1/latency_total
add wave -noupdate -format Literal /trafficsimulation/routerconnections__7/trafficreceptor_1/total_time
add wave -noupdate -format Literal /trafficsimulation/routerconnections__7/trafficreceptor_1/receivedpackets
add wave -noupdate -divider {Receptor x2y2}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__8/trafficreceptor_1/data_in
add wave -noupdate -format Logic /trafficsimulation/routerconnections__8/trafficreceptor_1/rx
add wave -noupdate -format Literal /trafficsimulation/routerconnections__8/trafficreceptor_1/latency_total
add wave -noupdate -format Literal /trafficsimulation/routerconnections__8/trafficreceptor_1/total_time
add wave -noupdate -format Literal /trafficsimulation/routerconnections__8/trafficreceptor_1/receivedpackets
add wave -noupdate -divider <NULL>
add wave -noupdate -divider {RoutingTable x1y1}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y1/routingtable/clk
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y1/routingtable/routingready
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y1/routingtable/requests
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y1/routingtable/connected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y1/routingtable/outputport
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y1/routingtable/selectedinput
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y1/routingtable/outputconnections_table
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y1/routingtable/inputconnections_table
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y1/routingtable/selected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y1/routingtable/selectedout
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y1/routingtable/outconnection
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y1/routingtable/inputconnections
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y1/routingtable/outputconnections
add wave -noupdate -divider {TrafficGenerator x1y1}
add wave -noupdate -format Logic /trafficsimulation/routerconnections__4/trafficgenerator_1/clk
add wave -noupdate -format Logic /trafficsimulation/routerconnections__4/trafficgenerator_1/rst
add wave -noupdate -format Logic /trafficsimulation/routerconnections__4/trafficgenerator_1/rx
add wave -noupdate -format Logic /trafficsimulation/routerconnections__4/trafficgenerator_1/ackrx
add wave -noupdate -format Literal /trafficsimulation/routerconnections__4/trafficgenerator_1/datain
add wave -noupdate -divider {TransfertCtrl_4 x1y1}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y1/transfertcontroller_i4/clk
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y1/transfertcontroller_i4/rst
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y1/transfertcontroller_i4/fifoin_isempty
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y1/transfertcontroller_i4/fifoin_dataout
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y1/transfertcontroller_i4/fifoin_read
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y1/transfertcontroller_i4/fifoout_isfull
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y1/transfertcontroller_i4/fifoout_write
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y1/transfertcontroller_i4/fifoout_datain
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y1/transfertcontroller_i4/inputrequests
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y1/transfertcontroller_i4/inputconnected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y1/transfertcontroller_i4/currentstate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y1/transfertcontroller_i4/futurestate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y1/transfertcontroller_i4/cnt
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y1/transfertcontroller_i4/fifoin_read_int
add wave -noupdate -divider {RoutingTable x2y0}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/routingtable/clk
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/routingtable/routingready
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/requests
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/connected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/outputport
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/selectedinput
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/inputconnections
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/outputconnections
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/selected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/selectedout
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/outconnection
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/inputconnections_table
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/outputconnections_table
add wave -noupdate -divider {TransfertCtrl_4 x2y0}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/fifoin_dataout
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/fifoin_read
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/fifoin_isempty
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/fifoout_datain
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/fifoout_write
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/fifoout_isfull
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/inputrequests
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/inputconnected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/currentstate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/cnt
add wave -noupdate -divider {FifoOut_3 x2y0}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o3/data_in
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o3/wr
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o3/isfull
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o3/data_out
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o3/rd
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o3/isempty
add wave -noupdate -divider {FifoIn_2 x1y0}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/fifo_in_i2/data_in
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/fifo_in_i2/wr
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/fifo_in_i2/isfull
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/fifo_in_i2/data_out
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/fifo_in_i2/rd
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/fifo_in_i2/isempty
add wave -noupdate -divider {TransfertCtrl_2 x1y0}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/inputrequests
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/fifoin_dataout
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/fifoin_read
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/fifoin_isempty
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/fifoout_datain
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/fifoout_write
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/fifoout_isfull
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/inputconnected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/currentstate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/cnt
add wave -noupdate -divider {RoutingTable x1y0}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/routingtable/clk
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/routingtable/routingready
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/requests
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/connected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/outputport
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/selectedinput
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/selected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/selectedout
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/outconnection
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/inputconnections
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/outputconnections
add wave -noupdate -divider {FifoOut_3 x1y0}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/fifo_out_o3/data_in
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/fifo_out_o3/wr
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/fifo_out_o3/isfull
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/fifo_out_o3/data_out
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/fifo_out_o3/rd
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/fifo_out_o3/isempty
add wave -noupdate -divider {FifoIn_2 x0y0}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y0/fifo_in_i2/data_in
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/fifo_in_i2/wr
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/fifo_in_i2/isempty
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y0/fifo_in_i2/data_out
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/fifo_in_i2/rd
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/fifo_in_i2/isfull
add wave -noupdate -divider {TransfertCtrl_2 x0y0}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i2/inputrequests
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i2/inputconnected
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i2/fifoin_dataout
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i2/fifoin_read
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i2/fifoin_isempty
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i2/fifoout_datain
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i2/fifoout_write
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i2/fifoout_isfull
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i2/currentstate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i2/cnt
add wave -noupdate -divider {FifoOut_0 x0y0}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o0/data_in
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o0/wr
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o0/isfull
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o0/rd
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o0/data_out
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o0/isempty
add wave -noupdate -divider {FifoIn_1 x0y1}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y1/fifo_in_i1/data_in
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y1/fifo_in_i1/wr
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y1/fifo_in_i1/isfull
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y1/fifo_in_i1/data_out
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y1/fifo_in_i1/rd
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y1/fifo_in_i1/isempty
add wave -noupdate -divider {TransfertCtrl_1 x0y1}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y1/transfertcontroller_i1/inputrequests
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y1/transfertcontroller_i1/inputconnected
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y1/transfertcontroller_i1/clk
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y1/transfertcontroller_i1/rst
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y1/transfertcontroller_i1/fifoin_dataout
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y1/transfertcontroller_i1/fifoin_read
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y1/transfertcontroller_i1/fifoin_isempty
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y1/transfertcontroller_i1/fifoout_datain
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y1/transfertcontroller_i1/fifoout_write
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y1/transfertcontroller_i1/fifoout_isfull
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y1/transfertcontroller_i1/currentstate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y1/transfertcontroller_i1/cnt
add wave -noupdate -divider {RoutingTable x0y1}
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y1/routingtable/requests
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y1/routingtable/connected
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y1/routingtable/clk
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y1/routingtable/routingready
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y1/routingtable/outputport
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y1/routingtable/selectedinput
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y1/routingtable/outputconnections_table
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y1/routingtable/inputconnections_table
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y1/routingtable/outconnection
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y1/routingtable/selected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y1/routingtable/selectedout
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y1/routingtable/inputconnections
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y1/routingtable/outputconnections
add wave -noupdate -divider {TransfertCtrl_1 x0y2}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y2/transfertcontroller_i1/inputrequests
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y2/transfertcontroller_i1/inputconnected
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y2/transfertcontroller_i1/fifoout_datain
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y2/transfertcontroller_i1/fifoin_read
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y2/transfertcontroller_i1/fifoin_isempty
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y2/transfertcontroller_i1/fifoin_dataout
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y2/transfertcontroller_i1/fifoout_write
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y2/transfertcontroller_i1/fifoout_isfull
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y2/transfertcontroller_i1/currentstate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y2/transfertcontroller_i1/cnt
add wave -noupdate -divider {RoutingTable x0y2}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y2/routingtable/routingready
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y2/routingtable/requests
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y2/routingtable/connected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y2/routingtable/outputport
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y2/routingtable/selectedinput
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y2/routingtable/outputconnections_table
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y2/routingtable/inputconnections_table
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y2/routingtable/outconnection
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y2/routingtable/selected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y2/routingtable/selectedout
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y2/routingtable/inputconnections
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y2/routingtable/outputconnections
add wave -noupdate -divider {FifoOut_4 x0y2}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y2/fifo_out_o4/data_in
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y2/fifo_out_o4/wr
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y2/fifo_out_o4/isfull
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y2/fifo_out_o4/data_out
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y2/fifo_out_o4/rd
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y2/fifo_out_o4/isempty
add wave -noupdate -divider {New Divider}
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3563830 ps} 0}
configure wave -namecolwidth 457
configure wave -valuecolwidth 74
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {3142850 ps} {3770220 ps}
