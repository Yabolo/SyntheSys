onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider NoC
add wave -noupdate -format Logic -label clk /trafficsimulation/clk
add wave -noupdate -format Logic -label rst /trafficsimulation/rst
add wave -noupdate -format Literal -label rx /trafficsimulation/rx
add wave -noupdate -format Literal -label datain -radix hexadecimal /trafficsimulation/datain
add wave -noupdate -format Literal -label ackrx /trafficsimulation/ackrx
add wave -noupdate -format Literal -label tx /trafficsimulation/tx
add wave -noupdate -format Literal -label dataout -radix hexadecimal /trafficsimulation/dataout
add wave -noupdate -format Literal -label acktx /trafficsimulation/acktx
add wave -noupdate -divider {TrafficGenerator x0y0}
add wave -noupdate -format Logic -label rx /trafficsimulation/routerconnections__0/trafficgenerator_1/rx
add wave -noupdate -format Logic -label ackrx /trafficsimulation/routerconnections__0/trafficgenerator_1/ackrx
add wave -noupdate -format Literal -label datain -radix hexadecimal /trafficsimulation/routerconnections__0/trafficgenerator_1/datain
add wave -noupdate -divider {TrafficReceptor x2y2}
add wave -noupdate -format Logic -label rx /trafficsimulation/routerconnections__8/trafficreceptor_1/rx
add wave -noupdate -format Logic -label ack_rx /trafficsimulation/routerconnections__8/trafficreceptor_1/ack_rx
add wave -noupdate -format Literal -label data_in -radix hexadecimal /trafficsimulation/routerconnections__8/trafficreceptor_1/data_in
add wave -noupdate -format Literal -label latency_total /trafficsimulation/routerconnections__8/trafficreceptor_1/latency_total
add wave -noupdate -format Literal -label total_time /trafficsimulation/routerconnections__8/trafficreceptor_1/total_time
add wave -noupdate -divider {TrafficGenerator x2y0}
add wave -noupdate -format Logic -label rx /trafficsimulation/routerconnections__2/trafficgenerator_1/rx
add wave -noupdate -format Logic -label ackrx /trafficsimulation/routerconnections__2/trafficgenerator_1/ackrx
add wave -noupdate -format Literal -label datain -radix hexadecimal /trafficsimulation/routerconnections__2/trafficgenerator_1/datain
add wave -noupdate -divider {TrafficReceptor x0y2}
add wave -noupdate -format Logic -label rx /trafficsimulation/routerconnections__6/trafficreceptor_1/rx
add wave -noupdate -format Logic -label ack_rx /trafficsimulation/routerconnections__6/trafficreceptor_1/ack_rx
add wave -noupdate -format Literal -label data_in -radix hexadecimal /trafficsimulation/routerconnections__6/trafficreceptor_1/data_in
add wave -noupdate -format Literal -label latency_total /trafficsimulation/routerconnections__6/trafficreceptor_1/latency_total
add wave -noupdate -format Literal -label total_time /trafficsimulation/routerconnections__6/trafficreceptor_1/total_time
add wave -noupdate -divider {Routing Table x0y0}
add wave -noupdate -format Literal -label selected /trafficsimulation/rocnet_1/router_x0y0/routingtable/selected
add wave -noupdate -format Literal -label requests /trafficsimulation/rocnet_1/router_x0y0/routingtable/requests
add wave -noupdate -format Literal -label inputconnections /trafficsimulation/rocnet_1/router_x0y0/routingtable/inputconnections
add wave -noupdate -format Literal -label outputconnections /trafficsimulation/rocnet_1/router_x0y0/routingtable/outputconnections
add wave -noupdate -divider {TransfertCtrl_4 x0y0}
add wave -noupdate -format Logic -label clk /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i4/clk
add wave -noupdate -format Literal -label fifoout_datain -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i4/fifoout_datain
add wave -noupdate -format Logic -label fifoin_read /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i4/fifoin_read
add wave -noupdate -format Logic -label fifoin_isempty /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i4/fifoin_isempty
add wave -noupdate -format Literal -label fifoin_dataout -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i4/fifoin_dataout
add wave -noupdate -format Logic -label fifoout_write /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i4/fifoout_write
add wave -noupdate -format Logic -label fifoout_isfull /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i4/fifoout_isfull
add wave -noupdate -format Logic -label inputrequests /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i4/inputrequests
add wave -noupdate -format Logic -label inputconnected /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i4/inputconnected
add wave -noupdate -format Literal -label currentstate /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i4/currentstate
add wave -noupdate -format Literal -label cnt /trafficsimulation/rocnet_1/router_x0y0/transfertcontroller_i4/cnt
add wave -noupdate -divider {FifoOut_2 x0y0}
add wave -noupdate -format Logic -label clock_in /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o2/clock_in
add wave -noupdate -format Logic -label clock_out /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o2/clock_out
add wave -noupdate -format Literal -label data_in -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o2/data_in
add wave -noupdate -format Logic -label wr /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o2/wr
add wave -noupdate -format Logic -label ack_wr /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o2/ack_wr
add wave -noupdate -format Logic -label isfull /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o2/isfull
add wave -noupdate -format Literal -label data_out -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o2/data_out
add wave -noupdate -format Logic -label rd /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o2/rd
add wave -noupdate -format Logic -label ack_rd /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o2/ack_rd
add wave -noupdate -format Logic -label isempty /trafficsimulation/rocnet_1/router_x0y0/fifo_out_o2/isempty
add wave -noupdate -divider {FifoIn_4 x0y0}
add wave -noupdate -format Literal -label data_in -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y0/fifo_in_i4/data_in
add wave -noupdate -format Logic -label rd /trafficsimulation/rocnet_1/router_x0y0/fifo_in_i4/rd
add wave -noupdate -format Logic -label ack_rd /trafficsimulation/rocnet_1/router_x0y0/fifo_in_i4/ack_rd
add wave -noupdate -format Logic -label isempty /trafficsimulation/rocnet_1/router_x0y0/fifo_in_i4/isempty
add wave -noupdate -format Literal -label data_out -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y0/fifo_in_i4/data_out
add wave -noupdate -format Logic -label wr /trafficsimulation/rocnet_1/router_x0y0/fifo_in_i4/wr
add wave -noupdate -format Logic -label ack_wr /trafficsimulation/rocnet_1/router_x0y0/fifo_in_i4/ack_wr
add wave -noupdate -format Logic -label isfull /trafficsimulation/rocnet_1/router_x0y0/fifo_in_i4/isfull
add wave -noupdate -divider {Fifo to HS_2 x0y0}
add wave -noupdate -format Literal -label hs_dataout -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y0/fifotohandshake_o2/hs_dataout
add wave -noupdate -format Literal -label fifo_dataout -radix hexadecimal /trafficsimulation/rocnet_1/router_x0y0/fifotohandshake_o2/fifo_dataout
add wave -noupdate -format Logic -label fifo_read /trafficsimulation/rocnet_1/router_x0y0/fifotohandshake_o2/fifo_read
add wave -noupdate -format Literal -label currentstate /trafficsimulation/rocnet_1/router_x0y0/fifotohandshake_o2/currentstate
add wave -noupdate -format Logic -label fifo_isempty /trafficsimulation/rocnet_1/router_x0y0/fifotohandshake_o2/fifo_isempty
add wave -noupdate -format Logic -label hs_acktx /trafficsimulation/rocnet_1/router_x0y0/fifotohandshake_o2/hs_acktx
add wave -noupdate -format Logic -label hs_tx /trafficsimulation/rocnet_1/router_x0y0/fifotohandshake_o2/hs_tx
add wave -noupdate -divider {HS to Fifo_3 x1y0}
add wave -noupdate -format Literal -label hs_datain -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/handshaketofifo_i3/hs_datain
add wave -noupdate -format Literal -label fifo_datain -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/handshaketofifo_i3/fifo_datain
add wave -noupdate -format Logic -label fifo_write /trafficsimulation/rocnet_1/router_x1y0/handshaketofifo_i3/fifo_write
add wave -noupdate -format Literal -label currentstate /trafficsimulation/rocnet_1/router_x1y0/handshaketofifo_i3/currentstate
add wave -noupdate -format Literal -label datatosend -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/handshaketofifo_i3/datatosend
add wave -noupdate -format Logic -label fifo_isfull /trafficsimulation/rocnet_1/router_x1y0/handshaketofifo_i3/fifo_isfull
add wave -noupdate -format Logic -label hs_ackrx /trafficsimulation/rocnet_1/router_x1y0/handshaketofifo_i3/hs_ackrx
add wave -noupdate -format Logic -label hs_rx /trafficsimulation/rocnet_1/router_x1y0/handshaketofifo_i3/hs_rx
add wave -noupdate -divider {FifoIn_3 x1y0}
add wave -noupdate -format Logic -label isfull /trafficsimulation/rocnet_1/router_x1y0/fifo_in_i3/isfull
add wave -noupdate -format Logic -label isempty /trafficsimulation/rocnet_1/router_x1y0/fifo_in_i3/isempty
add wave -noupdate -format Literal -label data_in -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/fifo_in_i3/data_in
add wave -noupdate -format Logic -label wr /trafficsimulation/rocnet_1/router_x1y0/fifo_in_i3/wr
add wave -noupdate -format Logic -label rd /trafficsimulation/rocnet_1/router_x1y0/fifo_in_i3/rd
add wave -noupdate -format Logic -label ack_rd /trafficsimulation/rocnet_1/router_x1y0/fifo_in_i3/ack_rd
add wave -noupdate -format Logic -label ack_wr /trafficsimulation/rocnet_1/router_x1y0/fifo_in_i3/ack_wr
add wave -noupdate -format Literal -label data_out -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/fifo_in_i3/data_out
add wave -noupdate -divider {TransfertCtrl_3 x1y0}
add wave -noupdate -format Literal -label fifoout_datain -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i3/fifoout_datain
add wave -noupdate -format Logic -label fifoin_read /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i3/fifoin_read
add wave -noupdate -format Logic -label fifoin_isempty /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i3/fifoin_isempty
add wave -noupdate -format Literal -label fifoin_dataout -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i3/fifoin_dataout
add wave -noupdate -format Logic -label fifoout_write /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i3/fifoout_write
add wave -noupdate -format Logic -label fifoout_isfull /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i3/fifoout_isfull
add wave -noupdate -format Logic -label inputrequests /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i3/inputrequests
add wave -noupdate -format Logic -label inputconnected /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i3/inputconnected
add wave -noupdate -format Literal -label currentstate /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i3/currentstate
add wave -noupdate -format Literal -label cnt /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i3/cnt
add wave -noupdate -divider {FifoOut_2 x1y0}
add wave -noupdate -format Literal -label data_in -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/fifo_out_o2/data_in
add wave -noupdate -format Logic -label wr /trafficsimulation/rocnet_1/router_x1y0/fifo_out_o2/wr
add wave -noupdate -format Logic -label ack_wr /trafficsimulation/rocnet_1/router_x1y0/fifo_out_o2/ack_wr
add wave -noupdate -format Logic -label isfull /trafficsimulation/rocnet_1/router_x1y0/fifo_out_o2/isfull
add wave -noupdate -format Literal -label data_out -radix hexadecimal /trafficsimulation/rocnet_1/router_x1y0/fifo_out_o2/data_out
add wave -noupdate -format Logic -label rd /trafficsimulation/rocnet_1/router_x1y0/fifo_out_o2/rd
add wave -noupdate -format Logic -label ack_rd /trafficsimulation/rocnet_1/router_x1y0/fifo_out_o2/ack_rd
add wave -noupdate -format Logic -label isempty /trafficsimulation/rocnet_1/router_x1y0/fifo_out_o2/isempty
add wave -noupdate -divider {New Divider}
add wave -noupdate -format Logic -label clk /trafficsimulation/clk
add wave -noupdate -format Literal -label fifoin_dataout -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/fifoin_dataout
add wave -noupdate -format Logic -label fifoin_read /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/fifoin_read
add wave -noupdate -format Logic -label fifoin_isempty /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/fifoin_isempty
add wave -noupdate -format Literal -label fifoout_datain -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/fifoout_datain
add wave -noupdate -format Logic -label fifoout_write /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/fifoout_write
add wave -noupdate -format Logic -label fifoout_isfull /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/fifoout_isfull
add wave -noupdate -format Literal -label currentstate /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/currentstate
add wave -noupdate -format Literal -label cnt /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/cnt
add wave -noupdate -format Logic -label inputconnected /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/inputconnected
add wave -noupdate -format Logic -label inputrequests /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/inputrequests
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {245000 ps} 0}
configure wave -namecolwidth 143
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {191860 ps} {608830 ps}
