onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider NoC
add wave -noupdate -format Logic -label clk /trafficsimulation/clk
add wave -noupdate -format Logic -label rst /trafficsimulation/rst
add wave -noupdate -format Literal -label rx /trafficsimulation/rx
add wave -noupdate -format Literal -label datain -radix hexadecimal /trafficsimulation/datain
add wave -noupdate -format Literal -label ackrx /trafficsimulation/ackrx
add wave -noupdate -format Literal -label tx /trafficsimulation/tx
add wave -noupdate -format Literal -label dataout -radix hexadecimal /trafficsimulation/dataout
add wave -noupdate -format Literal -label acktx /trafficsimulation/acktx
add wave -noupdate -divider {TrafficGenerator x0y0}
add wave -noupdate -format Logic -label rx /trafficsimulation/routerconnections__0/trafficgenerator_1/rx
add wave -noupdate -format Logic -label ackrx /trafficsimulation/routerconnections__0/trafficgenerator_1/ackrx
add wave -noupdate -format Literal -label datain -radix hexadecimal /trafficsimulation/routerconnections__0/trafficgenerator_1/datain
add wave -noupdate -divider {TrafficReceptor x2y2}
add wave -noupdate -format Logic -label rx /trafficsimulation/routerconnections__8/trafficreceptor_1/rx
add wave -noupdate -format Logic -label ack_rx /trafficsimulation/routerconnections__8/trafficreceptor_1/ack_rx
add wave -noupdate -format Literal -label data_in -radix hexadecimal /trafficsimulation/routerconnections__8/trafficreceptor_1/data_in
add wave -noupdate -format Literal -label latency_total /trafficsimulation/routerconnections__8/trafficreceptor_1/latency_total
add wave -noupdate -format Literal -label total_time /trafficsimulation/routerconnections__8/trafficreceptor_1/total_time
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {28000 ps} 0}
configure wave -namecolwidth 157
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {315 ns}
