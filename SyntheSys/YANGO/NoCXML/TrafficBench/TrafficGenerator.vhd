----------------------------------------------------------------------------------
-- Company:         ADACSYS
-- Engineer:        Matthieu PAYET
-- 
-- Create Date:     25/02/13 
-- Design Name:     TrafficGenerator
-- Module Name:     TrafficGenerator - Behavioral 
-- Project Name:    NoC Hermes simulation
-- Target Devices:  
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
Library IEEE;
Use IEEE.std_logic_1164.all;
Use IEEE.std_logic_unsigned.all;
--Use IEEE.std_logic_arith.all;
USE ieee.numeric_std.ALL;
use ieee.std_logic_textio.all;
use std.textio.all;
-------------------------------------------------------------------------------
-- ENTITY: Traffic generator for one destination simulation
-------------------------------------------------------------------------------
Entity TrafficGenerator IS
  generic (  
    InputFileName   : string :="./Scenario/In/inX.txt";
    FlitWidth       : natural :=16;
    LocalAddr       : std_logic_vector:="0000";	-- LocalAddr of initiator
    DestAddr_x      : natural:=2;               -- LocalAddr of destination X axis
    DestAddr_y      : natural:=3;               -- LocalAddr of destination Y axis
    PacketSize      : natural:=16;              -- LocalAddr and number of flits is included.
    NbPackets       : natural:=8;	        -- number of the packets.
    IdleTime        : natural:=30               -- idle time.
    );
  port ( 
    Clk, Rst  : IN  std_logic;  
    Rx	      : OUT std_logic:='0'; -- signals face to the Local ports of the NoC
    AckRx     : IN  std_logic; -- signals face to the Local ports of the NoC
    DataIn    : OUT std_logic_vector(FlitWidth-1 downto 0)    -- signals face to the Local ports of the NoC   
    );
end TrafficGenerator;

-------------------------------------------------------------------------------
-- ARCHITECTURE: Behavioral with TEXTIO, Version 0
-------------------------------------------------------------------------------
architecture Behavioral of TrafficGenerator is

begin
  
  -----------------------------------------------------------------------------
  -----------------------------------------------------------------------------
  TEST: process
    file TestFile : text;
    variable L           : line;
    variable TimeVector  : time;
    variable R           : real := 0.0;
    variable GoodNumber  : boolean;
    variable index       : integer;
    variable Val         : std_logic_vector(FlitWidth-1 downto 0);

  begin  -- process Test
    
    file_open(TestFile, InputFileName, READ_MODE);  
    write(output, LF&"-========- Start NoC traffic generator -========-");

    while not endfile(TestFile) loop
      readline(TestFile, L);
      --write(output, L);
      
      --TimeStamp, Target, Size, Source, timestamp of node output, number of sequence, timestamp of network input, payload
      index := 0;
      Rx<='0';
      
      wait for 1 ns;
      
      hread(L, Val, GOOD => GoodNumber);-- read the time from the beginning of the line
      next when not GoodNumber;-- skip the line if it doesn't start with a number
      R := Real(to_integer(unsigned(Val)));
      --For each value in line:
      while true loop
        
        case index is
            --------------------------------------------------
          when 0 =>  -- Value="Timestamp" : send Header (target address) at time
            --------------------------------------------------
            TimeVector := R*1 ns; -- convert real number to time
            if (now < TimeVector) then -- wait until vector time
              wait for TimeVector - now;
            end if;
            write(output, LF&"Send Header (target address) at time : "&integer'image(INTEGER(R))&"ns.");
            --DataIn<=X"00"&STD_LOGIC_VECTOR(CONV_SIGNED(INTEGER(DestAddr_y), FlitWidth/4))&STD_LOGIC_VECTOR(CONV_SIGNED(INTEGER(DestAddr_x), FlitWidth/4)); -- Send target address (first flit)
            hread(L, Val, GOOD => GoodNumber); -- read the hexa value from the beginning of the line
            DataIn<=Val;
            Rx<='1';
            wait until AckRx='1';
            wait for 5 ns;
            Rx<='0';
            wait until AckRx='0';
            wait for 5 ns;
            --------------------------------------------------
          when 1 =>  -- Value="packet size"
            --------------------------------------------------
            write(output, LF&"Send packet size flit.");
            DataIn<=Val; -- Send package size address (second flit)
            Rx<='1';
            wait until AckRx='1';
            wait for 5 ns;
            Rx<='0';
            wait until AckRx='0';
            wait for 5 ns;
            --------------------------------------------------
          when 2 =>   -- Value="Source address"
            --------------------------------------------------
            write(output, LF&"Send source address flit.");
            DataIn<=Val; -- Send source address (third flit)
            Rx<='1';
            wait until AckRx='1';
            wait for 5 ns;
            Rx<='0';
            wait until AckRx='0';
            wait for 5 ns;
            --------------------------------------------------
          when 3|4|5|6 =>   -- Value="timestamp of node output"
            --------------------------------------------------
            write(output, LF&"Send timestamp.");
            DataIn<=Val; -- fourth to seventh flits
            Rx<='1';
            wait until AckRx='1';
            wait for 5 ns;
            Rx<='0';
            wait until AckRx='0';
            wait for 5 ns;
            --------------------------------------------------
          when 7 =>   -- Value="number of sequence"
            --------------------------------------------------
            write(output, LF&"Send number of sequence.");
            DataIn<=Val; 
            Rx<='1';
            wait until AckRx='1';
            wait for 5 ns;
            Rx<='0';
            wait until AckRx='0';
            wait for 5 ns;
          when others => 
            --------------------------------------------------
            write(output, LF&"Send payload:"&integer'image(INTEGER(index-8)));
            DataIn<=Val; -- Send payload (rest of the flits)
            Rx<='1';
            wait until AckRx='1';
            wait for 5 ns;
            Rx<='0';
            wait until AckRx='0';
            wait for 5 ns;
            --------------------------------------------------
        end case;
        
        hread(L, Val, GOOD => GoodNumber); -- read the hexa value from the beginning of the line
        --write(output, LF&"hread:"&integer'image(CONV_INTEGER(unsigned(Val))));
        exit when not GoodNumber; -- skip the line if it doesn't start with a number
        --Val <= STD_LOGIC_VECTOR(CONV_SIGNED(INTEGER(R), FlitWidth));
        
        index:=index+1;
      end loop;                         -- end of line
      
    end loop;                         -- end of file
    
    assert false report LF&"-========- Test complete -========-";
    wait;
    
  end process TEST;
  -----------------------------------------------------------------------------
  
end architecture Behavioral;




-------------------------------------------------------------------------------
-- ARCHITECTURE: Version 0
-------------------------------------------------------------------------------
architecture RTL of TrafficGenerator is
  signal data_enter : std_logic_vector(FlitWidth-1 downto 0);
  signal routerrx   : std_logic :='0';
  signal etat       : integer :=0;  --count the number of cycles between 2 packets (measure of idle)
  signal DestAddr   : std_logic_vector(FlitWidth/2-1 downto 0);

begin

  -----------------------------------------------------------------------------
  P: process (Clk, Rst)
    variable nber  : integer :=0 ; -- count the nber of packets ("measure" of the nber of packets)
    VARIABLE state : INTEGER:=0;   -- use to send ths data in the table.
    VARIABLE sum   : INTEGER:=0;   -- count the nber of cycles between 2 packets ("measure" of idle time)
    variable Cnt, mesure_initial : integer:=0;
    variable mesure_initial_logic : std_logic_vector( FlitWidth*2-1 downto 0);
    
  begin
    
    DestAddr<=std_logic_vector(to_unsigned(DestAddr_x,FlitWidth/4))&std_logic_vector(to_unsigned(DestAddr_y,FlitWidth/4));
    if Rst ='1' THEN
      routerrx <= '0';
      DataIn <= (others=>'0');
      state:=0;
    else
      if Clk='1' and Clk'event then
        Cnt:=Cnt+1;
        if sum=0 or IdleTime=0 then
          if (LocalAddr /= DestAddr and nber< NbPackets and routerrx ='0') then
            if state =0 then
              mesure_initial := Cnt;
              mesure_initial_logic:= std_logic_vector(to_unsigned(mesure_initial, FlitWidth*2));
            end if;
            if state =3 then	
              DataIn <= mesure_initial_logic(FlitWidth-1 downto 0);
            elsif state =4 then
              DataIn <= mesure_initial_logic(FlitWidth*2-1 downto FlitWidth);
            else
              DataIn <= data_enter;
            end if;	
            routerrx <='1'; 
            state:=state+1;
            If (state=PacketSize) then -- fin du paquet
              state := 0 ; 
              nber := nber+1;
              sum :=1;
            end if;
          end if;
          if AckRx='1' then
            routerrx<='0';
          end if;
        else
          sum:=sum+1;
          IF sum=IdleTime+2 THEN
            sum:=0;
          END IF;
          IF AckRx='1' THEN
            routerrx<='0';
          END IF;
          
        END IF;
        
        etat<=state;

      end if;
      
    end if;
  end process P;
  -----------------------------------------------------------------------------
  
  Rx<=routerrx;
  
  --donnes d'entree selon les routeurs
  data_enter<="00000000" & LocalAddr WHEN etat=0 ELSE
               STD_LOGIC_VECTOR(to_unsigned(PacketSize-2, FlitWidth)) WHEN etat=1 ELSE
               "00000000" & DestAddr WHEN etat=2 ELSE
               "0000000000000000" WHEN etat=3 ELSE
               "0000000000000000" WHEN etat=4 ELSE
               "1111111111111110" WHEN etat=5 ELSE
               "1111111111111100" WHEN etat=6 ELSE
               "1111111111111000" WHEN etat=7 ELSE
               "1111111111110000" WHEN etat=8 ELSE
               "1111111111100000" WHEN etat>8;

end architecture RTL;


