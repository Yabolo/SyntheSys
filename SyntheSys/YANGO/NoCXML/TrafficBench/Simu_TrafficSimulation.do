vlib work
vmap work work

vcom -work work -93 -explicit ../Test_ROCNet/ROCNet_pkg.vhd
vcom -work work -93 -explicit ../Test_ROCNet/Arbiter.vhd
vcom -work work -93 -explicit ../Test_ROCNet/reset.vhd
vcom -work work -93 -explicit ../Test_ROCNet/CM.vhd
vcom -work work -93 -explicit ../Test_ROCNet/CrossBar.vhd
vcom -work work -93 -explicit ../Test_ROCNet/Fifo.vhd
vcom -work work -93 -explicit ../Test_ROCNet/FifoToHandShake.vhd
vcom -work work -93 -explicit ../Test_ROCNet/HandShakeToFifo.vhd
vcom -work work -93 -explicit ../Test_ROCNet/RoutingControl.vhd
vcom -work work -93 -explicit ../Test_ROCNet/RoutingTable.vhd
vcom -work work -93 -explicit ../Test_ROCNet/TransfertControl.vhd
vcom -work work -93 -explicit ../Test_ROCNet/Router.vhd
vcom -work work -93 -explicit ../Test_ROCNet/ROCNet.vhd

vcom -work work -93 -explicit ./TrafficGenerator.vhd
vcom -work work -93 -explicit ./TrafficReceptor.vhd
vcom -work work -93 -explicit ./TrafficSimulation.vhd

vsim -t 10ps work.TrafficSimulation
do wave.do

run 2000ns
