onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {Generator x0y2}
add wave -noupdate -format Logic /trafficsimulation/routerconnections__6/trafficgenerator_1/clk
add wave -noupdate -format Logic /trafficsimulation/routerconnections__6/trafficgenerator_1/rst
add wave -noupdate -format Logic /trafficsimulation/routerconnections__6/trafficgenerator_1/rx
add wave -noupdate -format Logic /trafficsimulation/routerconnections__6/trafficgenerator_1/ackrx
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__6/trafficgenerator_1/datain
add wave -noupdate -divider {Receptor x2y0}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__2/trafficreceptor_1/data_in
add wave -noupdate -format Literal /trafficsimulation/routerconnections__2/trafficreceptor_1/latency_total
add wave -noupdate -format Literal /trafficsimulation/routerconnections__2/trafficreceptor_1/total_time
add wave -noupdate -format Logic /trafficsimulation/routerconnections__2/trafficreceptor_1/rx
add wave -noupdate -format Logic /trafficsimulation/routerconnections__2/trafficreceptor_1/ack_rx
add wave -noupdate -format Logic /trafficsimulation/routerconnections__2/trafficreceptor_1/reset
add wave -noupdate -format Logic /trafficsimulation/routerconnections__2/trafficreceptor_1/clock
add wave -noupdate -divider {Generator x2y0}
add wave -noupdate -format Logic /trafficsimulation/routerconnections__2/trafficgenerator_1/clk
add wave -noupdate -format Logic /trafficsimulation/routerconnections__2/trafficgenerator_1/rst
add wave -noupdate -format Logic /trafficsimulation/routerconnections__2/trafficgenerator_1/rx
add wave -noupdate -format Logic /trafficsimulation/routerconnections__2/trafficgenerator_1/ackrx
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__2/trafficgenerator_1/datain
add wave -noupdate -divider <NULL>
add wave -noupdate -divider {RoutingCtrl x2y0}
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingcontrol/startrouting
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingcontrol/fifo_dataout_list
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingcontrol/selectedinput
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingcontrol/outputport
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/routingcontrol/routingready
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingcontrol/x_dest
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingcontrol/y_dest
add wave -noupdate -divider {RoutingTable x2y0}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/routingtable/routingready
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/requests
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/connected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/outputport
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/selectedinput
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/selected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/selectedout
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/inputconnections
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/routingtable/outputconnections
add wave -noupdate -divider {RoutingTable x1y0}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/routingtable/routingready
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/requests
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/connected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/outputport
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/selectedinput
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/inputconnections
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/outputconnections
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/selected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/routingtable/selectedout
add wave -noupdate -divider {RoutingTable x0y0}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x0y0/routingtable/routingready
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y0/routingtable/requests
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y0/routingtable/connected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y0/routingtable/outputport
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y0/routingtable/selectedinput
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y0/routingtable/inputconnections
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y0/routingtable/outputconnections
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y0/routingtable/selected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x0y0/routingtable/selectedout
add wave -noupdate -divider {TransfertCtrl_4 x2y0}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/clk
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/rst
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/fifoin_isempty
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/fifoin_dataout
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/fifoin_read
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/fifoout_isfull
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/fifoout_write
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/fifoout_datain
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/inputrequests
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/inputconnected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/currentstate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/futurestate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/cnt
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i4/fifoin_read_int
add wave -noupdate -divider {TransfertCtrl_2 x1y0}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/clk
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/rst
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/fifoin_isempty
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/fifoin_dataout
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/fifoin_read
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/fifoout_isfull
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/fifoout_write
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/fifoout_datain
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/inputrequests
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/inputconnected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/currentstate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/futurestate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/cnt
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x1y0/transfertcontroller_i2/fifoin_read_int
add wave -noupdate -divider {TransfertCtrl_3 x2y0}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/clk
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/rst
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/fifoin_dataout
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/fifoin_read
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/fifoin_isempty
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/fifoout_datain
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/fifoout_write
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/fifoout_isfull
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/inputrequests
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/inputconnected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/currentstate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/transfertcontroller_i3/cnt
add wave -noupdate -divider {FifoOut_0 x2y0}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o0/clock_out
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o0/data_in
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o0/wr
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o0/ack_wr
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o0/isfull
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o0/data_out
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o0/rd
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o0/ack_rd
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifo_out_o0/isempty
add wave -noupdate -divider {Fifo to HS_0 x2y0}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y0/fifotohandshake_o0/hs_dataout
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y0/fifotohandshake_o0/fifo_dataout
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifotohandshake_o0/fifo_read
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/fifotohandshake_o0/currentstate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y0/fifotohandshake_o0/futurestate
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifotohandshake_o0/fifo_isempty
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifotohandshake_o0/hs_acktx
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifotohandshake_o0/hs_tx
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifotohandshake_o0/rst
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y0/fifotohandshake_o0/clk
add wave -noupdate -divider {HS to Fifo_1 x2y1}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y1/handshaketofifo_i1/hs_datain
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y1/handshaketofifo_i1/fifo_datain
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y1/handshaketofifo_i1/fifo_write
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y1/handshaketofifo_i1/currentstate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y1/handshaketofifo_i1/futurestate
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y1/handshaketofifo_i1/datatosend
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y1/handshaketofifo_i1/fifo_isfull
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y1/handshaketofifo_i1/hs_ackrx
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y1/handshaketofifo_i1/hs_rx
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y1/handshaketofifo_i1/rst
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y1/handshaketofifo_i1/clk
add wave -noupdate -divider {TransfertCtrl_1 x2y1}
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y1/transfertcontroller_i1/clk
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y1/transfertcontroller_i1/rst
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y1/transfertcontroller_i1/fifoin_dataout
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y1/transfertcontroller_i1/fifoin_read
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y1/transfertcontroller_i1/fifoin_isempty
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/rocnet_1/router_x2y1/transfertcontroller_i1/fifoout_datain
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y1/transfertcontroller_i1/fifoout_write
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y1/transfertcontroller_i1/fifoout_isfull
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y1/transfertcontroller_i1/inputrequests
add wave -noupdate -format Logic /trafficsimulation/rocnet_1/router_x2y1/transfertcontroller_i1/inputconnected
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y1/transfertcontroller_i1/currentstate
add wave -noupdate -format Literal /trafficsimulation/rocnet_1/router_x2y1/transfertcontroller_i1/cnt
add wave -noupdate -divider <NULL>
add wave -noupdate -divider {Receptor x0y2}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__6/trafficreceptor_1/data_in
add wave -noupdate -format Literal /trafficsimulation/routerconnections__6/trafficreceptor_1/latency_total
add wave -noupdate -format Literal /trafficsimulation/routerconnections__6/trafficreceptor_1/total_time
add wave -noupdate -format Logic /trafficsimulation/routerconnections__6/trafficreceptor_1/rx
add wave -noupdate -format Logic /trafficsimulation/routerconnections__6/trafficreceptor_1/ack_rx
add wave -noupdate -format Logic /trafficsimulation/routerconnections__6/trafficreceptor_1/reset
add wave -noupdate -format Logic /trafficsimulation/routerconnections__6/trafficreceptor_1/clock
add wave -noupdate -divider {Generator x0y0}
add wave -noupdate -format Logic /trafficsimulation/routerconnections__0/trafficgenerator_1/clk
add wave -noupdate -format Logic /trafficsimulation/routerconnections__0/trafficgenerator_1/rst
add wave -noupdate -format Logic /trafficsimulation/routerconnections__0/trafficgenerator_1/rx
add wave -noupdate -format Logic /trafficsimulation/routerconnections__0/trafficgenerator_1/ackrx
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__0/trafficgenerator_1/datain
add wave -noupdate -divider {Receptor x2y2}
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__8/trafficreceptor_1/data_in
add wave -noupdate -format Literal /trafficsimulation/routerconnections__8/trafficreceptor_1/latency_total
add wave -noupdate -format Literal /trafficsimulation/routerconnections__8/trafficreceptor_1/total_time
add wave -noupdate -format Logic /trafficsimulation/routerconnections__8/trafficreceptor_1/rx
add wave -noupdate -format Logic /trafficsimulation/routerconnections__8/trafficreceptor_1/ack_rx
add wave -noupdate -format Logic /trafficsimulation/routerconnections__8/trafficreceptor_1/reset
add wave -noupdate -format Logic /trafficsimulation/routerconnections__8/trafficreceptor_1/clock
add wave -noupdate -divider {Generator x2y2}
add wave -noupdate -format Logic /trafficsimulation/routerconnections__8/trafficgenerator_1/clk
add wave -noupdate -format Logic /trafficsimulation/routerconnections__8/trafficgenerator_1/rst
add wave -noupdate -format Logic /trafficsimulation/routerconnections__8/trafficgenerator_1/rx
add wave -noupdate -format Logic /trafficsimulation/routerconnections__8/trafficgenerator_1/ackrx
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__8/trafficgenerator_1/datain
add wave -noupdate -divider {Receptor x0y0}
add wave -noupdate -format Logic /trafficsimulation/routerconnections__0/trafficreceptor_1/clock
add wave -noupdate -format Logic /trafficsimulation/routerconnections__0/trafficreceptor_1/reset
add wave -noupdate -format Literal -radix hexadecimal /trafficsimulation/routerconnections__0/trafficreceptor_1/data_in
add wave -noupdate -format Logic /trafficsimulation/routerconnections__0/trafficreceptor_1/ack_rx
add wave -noupdate -format Logic /trafficsimulation/routerconnections__0/trafficreceptor_1/rx
add wave -noupdate -format Literal /trafficsimulation/routerconnections__0/trafficreceptor_1/latency_total
add wave -noupdate -format Literal /trafficsimulation/routerconnections__0/trafficreceptor_1/total_time
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {50950 ps} 0}
configure wave -namecolwidth 457
configure wave -valuecolwidth 74
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {464650 ps}
