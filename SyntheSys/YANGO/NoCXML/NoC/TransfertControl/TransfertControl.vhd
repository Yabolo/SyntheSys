library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

-------------------------------------------------------------------------------
-- ENTITY: TransfertControl that 
-------------------------------------------------------------------------------
entity TransfertControl is
  generic (
    FlitWidth  : natural := 16);
  port (
    Clk, Rst          : in  std_logic;
    
    FifoIn_IsEmpty    : in  std_logic;
    FifoIn_DataOut    : in  std_logic_vector(FlitWidth-1 downto 0);
    FifoIn_Read       : out std_logic;
    
    FifoOut_IsFull    : in  std_logic;
    FifoOut_Write     : out std_logic;
    FifoOut_DataIn    : out std_logic_vector(FlitWidth-1 downto 0);
    
    InputRequests     : out std_logic;
    InputConnected    : out std_logic
    );

end TransfertControl;

-------------------------------------------------------------------------------
-- ARCHITECTURE: RTL, update request table on inputs events
-------------------------------------------------------------------------------
architecture RTL of TransfertControl is

  type FSM_STATE is (IDLE, HEADER, PAYLOAD, CONNECTED, ENDCONNECTION);
  signal CurrentState, FutureState : FSM_STATE := IDLE;
  signal Cnt : natural := 0;
  
  signal FifoIn_Read_int : std_logic :='0';
  
begin  -- RTL

  FifoIn_Read    <= FifoIn_Read_int;
  FifoOut_DataIn <= FifoIn_DataOut;
  -----------------------------------------------------------------------------
  TransfertFSM: process (Clk, Rst)
  begin  -- process Transfert
    if rising_edge(Clk) then  --  clock edge -- and Clk = '1' rising
      if Rst = '1' then                   -- synchronous reset (active high)
        CurrentState <= IDLE;
      else
        CurrentState <= FutureState;
      end if;
    end if;
  end process TransfertFSM;

  -----------------------------------------------------------------------------
  STATE_OUTPUTS: process (CurrentState, FifoIn_IsEmpty, FifoOut_IsFull, FifoIn_DataOut, Cnt)
  begin  -- process STATE_ASSIGNEMENT
    case CurrentState is
      when IDLE =>
  	InputConnected    <= '0';
  	InputRequests     <= '0';
        FifoOut_Write     <= '0';
        if FifoIn_IsEmpty='0' then
          FifoIn_Read_int <= '1'; -- First read !
          FutureState     <= HEADER;
        else
          FifoIn_Read_int <= '0';
          FutureState     <= IDLE;
        end if;
      ------------------------------------------------  
      when HEADER =>
  	InputConnected    <= '0';
  	InputRequests     <= '1';
        if FifoIn_IsEmpty='0' and FifoOut_IsFull='0' then
          FifoIn_Read_int       <= '1';
          FifoOut_Write     <= '1';
          FutureState       <= PAYLOAD;
        else
          FifoIn_Read_int       <= '0';
          FifoOut_Write     <= '0';
          FutureState       <= HEADER;
        end if;
      ------------------------------------------------  
--      when SENDHEADER =>
--  	InputConnected    <= '1';
--        if FifoIn_IsEmpty='0' and FifoOut_IsFull='0' then
--          FifoIn_Read_int       <= '0';
--          FifoOut_Write     <= '1';
--          FutureState       <= PAYLOAD;
--        else
--          FifoIn_Read_int       <= '0';
--          FifoOut_Write     <= '0';
--          FutureState       <= SENDHEADER;
--        end if;
      ------------------------------------------------  
      when PAYLOAD =>
  	InputConnected    <= '1';
  	InputRequests     <= '1';
        if FifoIn_IsEmpty='0' and FifoOut_IsFull='0' then
          FifoIn_Read_int     <= '1';
          FifoOut_Write   <= '1';
          FutureState     <= CONNECTED;
        else
          FifoIn_Read_int     <= '0';
          FifoOut_Write   <= '0';
          FutureState     <= PAYLOAD;
        end if;
      ------------------------------------------------  
      when CONNECTED => 
  	InputConnected    <= '1';
  	InputRequests     <= '1';
        if FifoIn_IsEmpty='0' and FifoOut_IsFull='0' then
          FifoIn_Read_int     <= '1';
          FifoOut_Write   <= '1';
        else
          FifoIn_Read_int     <= '0';
          FifoOut_Write   <= '0';
        end if;
        if Cnt<=1 then
          FutureState     <= ENDCONNECTION;
        else
          FutureState     <= CONNECTED;
        end if;
      ------------------------------------------------  
      when ENDCONNECTION => 
  	InputConnected    <= '1';
  	InputRequests     <= '1';
        FifoIn_Read_int   <= '0';
        if FifoOut_IsFull='0' then
          FifoOut_Write     <= '1'; -- Last write !
          FutureState       <= IDLE;
        else
          FifoOut_Write     <= '0';
          FutureState       <= ENDCONNECTION;
        end if;
      when others => null;
    end case;
  end process STATE_OUTPUTS;

  -----------------------------------------------------------------------------
  Counter: process (Clk, Rst)
  begin  -- process Counter
    if falling_edge(Clk) then  -- clock rising_edge
      if Rst = '1' then              --synchronous reset (active high)
        Cnt <= 0;
      else
        if CurrentState=PAYLOAD then
          Cnt <= CONV_INTEGER(FifoIn_DataOut);
        else
          if Cnt>1 and FifoOut_IsFull='0' and FifoIn_IsEmpty='0' then
            Cnt <= Cnt-1;
          else
            Cnt <= Cnt;
            
          end if;
        end if;
      end if;
    end if;
  end process Counter;

  -----------------------------------------------------------------------------
  
  
end RTL;


