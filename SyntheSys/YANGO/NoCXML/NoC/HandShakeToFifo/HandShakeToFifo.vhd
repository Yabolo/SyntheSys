
----------------------------------------------------------------------------------------------------
-- Actual File Name      = HandshakeToFifo.vhd
-- Title & purpose       = NoC router input: Handshake protocol to FIFO converter
-- Author                = Matthieu PAYET (ADACSYS) - matthieu.payet@adacsys.com
-- Creation Date         = 2012-12-10 19:30
-- Version               = 0.1
-- Simple Description    = Convert asynchronous Handshake protocol to FIFO protocol
-- Specific issues       = 
-- Speed                 = 
-- Area estimates        = 
-- Tools (version)       = Xilinx ISE (13.1)
-- HDL standard followed = VHDL 2001 standard
-- Revisions & ECOs      = 1.0
----------------------------------------------------------------------------------------------------
	
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

-------------------------------------------------------------------------------
-- ENTITY: Handshake protocol to FIFO converter
-------------------------------------------------------------------------------
entity HandShakeToFifo is
  
  generic (
    FlitWidth : natural := 16);

  port (
    Clk         : in  std_logic;
    Rst         : in std_logic;
    
    HS_Rx       : in  std_logic;
    HS_AckRx    : out std_logic;
    HS_DataIn   : in  std_logic_vector(FlitWidth-1 downto 0);
    
    FIFO_DataIn : out std_logic_vector(FlitWidth-1 downto 0);
    FIFO_Write  : out std_logic;
    FIFO_IsFull : in  std_logic
    );

end HandShakeToFifo;

-------------------------------------------------------------------------------
-- ARCHITECTURE: RTL, manage Handshake protocol
-------------------------------------------------------------------------------
architecture RTL of HandShakeToFifo is

  type FSM_HS is (IDLE, WRITE_FIFO, WAIT_FOR_ACK);
  signal CurrentState, FutureState : FSM_HS := IDLE;
  signal DataToSend : std_logic_vector(FlitWidth-1 downto 0);
  
begin  -- RTL

  -----------------------------------------------------------------------------
  FIFO_DataIn <= DataToSend;
  DataToSend  <= DataToSend when CurrentState=WRITE_FIFO else HS_DataIn;
  HS_AckRx    <= '1' when CurrentState/=IDLE and HS_Rx='1' else '0';
  FIFO_Write  <= '1' when CurrentState=WRITE_FIFO else '0';
  
  -----------------------------------------------------------------------------
  FSM_Memory: process (Clk, Rst)
  begin
    if Clk'event and Clk = '1' then  -- rising clock edge
      if Rst = '1' then -- synchronous reset (active High)
        CurrentState <= IDLE;
      else
        CurrentState <= FutureState;
      end if;
      
    end if;
    
  end process FSM_Memory;
  
  -----------------------------------------------------------------------------
  HS2FIFO: process (CurrentState, HS_Rx, FIFO_IsFull)
  begin  -- process HS2FIFO
    case CurrentState is
      when IDLE =>
        if HS_Rx='1' and not FIFO_IsFull='1' then
          FutureState<=WRITE_FIFO;
        else
          FutureState<=IDLE;
        end if;
      when WRITE_FIFO =>
        if HS_Rx='1' then
          FutureState<=WAIT_FOR_ACK;
        else
          FutureState<=IDLE;
        end if;
      when WAIT_FOR_ACK =>
        if HS_Rx='1' then
          FutureState<=WAIT_FOR_ACK;
        else
          FutureState<=IDLE;
        end if;
      
      when others => null;
    end case;
    
  end process HS2FIFO;
  

end RTL;


