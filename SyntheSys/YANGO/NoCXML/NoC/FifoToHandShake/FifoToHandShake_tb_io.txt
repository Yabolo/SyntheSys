# Test vectors for FifoToHandShake ports
# 0 means force 0
# 1 means force 1
# L means expect 0
# H means expect 1
# X means don’t care
########################
## Clock cycle = 10ns ##
########################
#time Rst   Clk   HS_AckTx  FIFO_DataOut(15:0)  FIFO_IsEmpty 
0     1     0     0         0000000000000000    1
5     1     1     0         0000000000000000    1
10    0     0     0         0000000000000000    1
15    0     1     0         0000000000000000    1
20    0     0     0         0000000000000000    1
25    0     1     0         0000000000000000    1
30    0     0     0         0000000000000000    0
35    0     1     0         0000000000000000    0
# First Data
40    0     0     0         0000000000000001    0
45    0     1     0         0000000000000001    0
50    0     0     1         0000000000000001    0
55    0     1     0         0000000000000001    0
60    0     0     0         0000000000000011    0
65    0     1     0         0000000000000011    0
# Second Data
70    0     0     0         0000000000000011    0
75    0     1     0         0000000000000011    0
80    0     0     1         0000000000000011    0
85    0     1     1         0000000000000011    0
90    0     0     0         0000000000000101    0
95    0     1     0         0000000000000101    0
100   0     0     1         0000000000011111    0
105   0     1     1         0000000000011111    0
110   0     0     0         0000000000011111    0
115   0     1     0         0000000000011111    0
120   0     0     0         0000000000000000    0
125   0     1     0         0000000000000000    0
130   0     0     1         0000000000000000    0
135   0     1     1         0000000000000000    0
# Third Data with a FIFO full
140   0     0     0         0000000000000111    0
145   0     1     1         0000000000000111    0
150   0     0     1         0000000000000111    0
155   0     1     0         0000000000000111    0
160   0     0     1         0000000000010101    0
165   0     1     1         0000000000010101    0
170   0     0     0         1111111111111111    1
175   0     1     0         1111111111111111    1
180   0     0     1         0000000000000000    1
185   0     1     1         0000000000000000    1
190   0     0     0         0000000000000000    1
195   0     1     0         0000000000000000    1
200   0     0     0         0000000000000000    1
205   0     1     0         0000000000000000    1

210   1     0     0         0000000000000000    1
