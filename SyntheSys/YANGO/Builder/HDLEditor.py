# VHDL EDITOR PACKAGE
import sys, os, logging, datetime, math, unicodedata

#sys.path.append(os.path.abspath("./DesignTree"))
#import DesignTree

#from myhdl import *
Generator="YANGO"
ExtensionDict = {"VHDL": ".vhd", "Verilog":".v"}

#======================================================================
def Header(Module, Title, Purpose, Desc, Issues="", Speed="", Area="", Tool="Xilinx ISE (13.1)", Rev=""):
	"""
	Return string commented header.
	"""
	Head = """
----------------------------------------------------------------------------------------------------
-- Actual File Name      = {MODULE}.vhd
-- Title & purpose       = {TITLE} - {PURPOSE}
-- Author                = Automaticaly generated by {GENERATOR} - made by Matthieu PAYET (ADACSYS) - matthieu.payet@adacsys.com
-- Creation Date         = {TIME}
-- Version               = 0.1
-- Simple Description    = {DESCRIPTION}
-- Specific issues       = {ISSUES}
-- Speed                 = {SPEED}
-- Area estimates        = {AREA}
-- Tools (version)       = {TOOL}
-- HDL standard followed = VHDL 2001 standard
-- Revisions & ECOs      = {REV}
----------------------------------------------------------------------------------------------------
	"""
	return Head.format(
			GENERATOR=Generator,
			MODULE=Module, 
			TITLE=Title, 
			PURPOSE=Purpose, 
			DESCRIPTION=Desc, 
			ISSUES=Issues, 
			SPEED=Speed, 
			AREA=Area, 
			TOOL=Tool, 
			REV=Rev,
			TIME=datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))


#======================================================================
def Libraries(LibList):
	Headers=""
	for Lib in list(set(LibList)):
		Headers+="\nlibrary {0};".format(Lib)
	return Headers

#======================================================================
def Packages(PackageList, All=True):
	Code=""
	for PackageName in PackageList:
		Code+=Package(PackageName, All)
	return Code
	
#======================================================================
def Package(PackageName, All=True):
	if All: return "\nuse {0}.all;".format(PackageName)
	else: return "\nuse {0};".format(PackageName) 

#======================================================================
def Process(Name, SensitivList, Declarations, Content, Comments=""):
	"""
	Return code for process declaration and insert specified content/declaration List.
	"""
	Text = "--"*30
	Text = Comment("Process "+Name+" : "+Comments)
	Text+= "process("+",".join(SensitivList)+")\n"
	Text+= Indent( Declarations )
	Text+= "\nbegin\n"
	Text+= Indent( Content )
	Text+= "\nend process;\n"
	Text = "--"*30
	
	return Text

#======================================================================
def SyncCondition(ClockName, EveryEdge=False, Rising=True):
	"""
	Return code for clock synchronous condition.
	"""
	if EveryEdge:
		return "{0}'event".format(ClockName)
	else:
		if Rising:
			return "rising_edge({0})".format(ClockName)
		else:
			return "falling_edge({0})".format(ClockName)

#======================================================================
def Entity(Name, Generics=[], Ports=[], Comments="Empty entity"):
	Text="\n"+"--"*40
	Text+="\n-- ENTITY: {0} - {1}\n".format(Name, Comments)
	Text+="--"*40
	PortDeclaration=""
	GenericDeclaration=""
	if len(Generics):
		GenericDeclaration="\ngeneric("
		while Generics.count([]): Generics.remove([])
		for G in Generics:
			GenericDeclaration+=Indent(Generic(G.GetName(), G.GetType(), G.GetValue())+";")
		GenericDeclaration=GenericDeclaration[:-1]+");"
	if len(Ports):
		PortDeclaration="\nport("
		for P in Ports:
			PortDeclaration+=Indent(Port(P.GetName(), P.Direction, P.GetType()))+";"
		PortDeclaration=PortDeclaration[:-1]+"\n);"
	return Text+"\nentity {0} is {1}{2}\nend entity {0};\n".format(Name, Indent(GenericDeclaration), Indent(PortDeclaration))
	
#======================================================================
def Port(PortName, IO, PortType):
	return "\n{0} : {1} {2}".format(PortName, IO, PortType)
	
#======================================================================
def Generic(GenericName, GenericType, GenericValue):
	if GenericType.find("integer")!=-1:
		Type="integer"
	else: Type=GenericType
	return "\n{0} : {1} := {2}".format(GenericName, Type, GenericValue)
		
#======================================================================
def Architecture(Name, Entity, Declarations="", Content="", Comments="Version 0"):
	"""
	Return code for the architecture of a module in VHDL.
	An instance is made up of 5 data (in a list) : InstanceName, Module, SignalDict, GenericDict, comment.
	A signal is described by its SignalName, SignalType and InitialValue (in a list). 
	"""
	Text="\n"+"--"*40
	Text+="\n-- ARCHITECTURE: {0} - {1}\n".format(Name, Comments)
	Text+="--"*40
	return Text+"\narchitecture {0} of {1} is\n{2}\n\nbegin\n{3}\n\nend architecture {0};\n\n".format(Name, Entity, Indent(Declarations), Indent(Content))
	
#======================================================================
def Constant(ConstName, ConstType, Value, Size=1):
	return "\nconstant {0} : {1} := {2};".format(ConstName, NormalizeType(ConstType), Value)
	

#======================================================================
#def Type(TypeName, ValueList, InitialValue=None):
#	"""
#	Return code for type declaration.
#	"""
#	if InitialValue:
#		return "type {0} is ("+",".join(ValueList)+");"
#	else:
#		return "type {0} is ("+",".join(ValueList)+") := {0};".format(InitialValue)
#
#======================================================================
def Instantiate(InstanceName, Module, Architecture=None, SignalDict={}, GenericDict={}, Comment=""):
	"""
	Generate code for instanciation (VHDL)
	"""
	Text=""
	if Comment!="": Text+="\n-- "+Comment
	if(Architecture!=None and Architecture!=""):
		Text +="\n{0}: entity work.{1}({2})".format(InstanceName, Module, Architecture)
	else: 
		Text +="\n{0}: entity work.{1}".format(InstanceName, Module)
	
	if len(GenericDict)>0:
		Text+="\n\tgeneric map(\n\t\t"
		Links=[]
		for GenericValue, Actual in GenericDict.items():
			if isinstance(Actual, Signal):
				Links.append(str(GenericValue)+' => '+str(Actual.GetName()))
			else: 
				Links.append(str(GenericValue)+' => '+str(Actual))
		Text+=",\n\t\t".join(Links)+"\n\t\t)"
	
	if len(SignalDict)>0:
		Text+="\n\tport map(\n\t\t"
		Links=[]
		for SignalName, Actual in SignalDict.items():
			if isinstance(Actual, Signal):
				Links.append(str(SignalName)+' => '+str(Actual.GetName()))
			else: 
				Links.append(str(SignalName)+' => '+str(Actual))
		Text+=",\n\t\t".join(Links)+"\n\t\t);"
		
	return Text

#======================================================================
def Case(SignalName, AllocationDict, Comments=""):
	"""
	Return code for process declaration and insert specified content/declaration List.
	"""
	Text = "\ncase {0} is ".format(SignalName)+Comment(Comments)
	for Condition in list(AllocationDict.keys()):
		Text+= Indent( "\nwhen {0} =>".format(Condition) )
		Text+= Indent( "\n{0}".format(AllocationDict[Condition]) ,2)
		
	if not list(AllocationDict.keys()).count("others"):
		Text+= Indent( "\nwhen others => null;" )
		
	Text+= "\nend case;\n"
	
	return 

#======================================================================
def If(AllocationList, Comments=""):
	"""
	Return code for if/elsif/else declaration according to Condition/Action pairs in Allocation List.
	Else item is represented by a condition='None'.
	"""
	Text=""
	if len(AllocationList):
		ElseItem = None
		Condition, Action = AllocationList.pop(0)
		Text+="if {0} then\n\t{1};".format(Condition, Action)
		for Condition, Action in AllocationList:
			if Condition:
				Text+="\nelsif {0} then\n\t{1}".format(Condition, Action)
			else:
				ElseItem=(Condition, Action)
		if ElseItem:
			Text+="else \n\t{1}".format(ElseItem[0], ElseItem[1])
	return Text+"\nend if;"

#======================================================================
def SyncCondition(SignalName):
	return "rising_edge({0})".format(SignalName)

#======================================================================
def For(Name, Var, Start, Stop, Content, Comments=""):
	"""
	Return code for 'for' declaration of a content.
	"""
	Text="\n{0}: for {1} in {2} to {3} generate {4}\nend generate {0};".format(Name, Var, Start, Stop, Indent(Content))
	return "\n--"+Comments+Text
		
#======================================================================
def LogicValue(Integer, BitLength):
	if int(BitLength)<2:
		if int(Integer)>1:
			logging.error("Bit value is higher than 1: set 0 instead.")
			return "'{0:b}'".format(0)
		else:
			return "'{0:b}'".format(int(Integer))
	else:   
		return str('"{0:0'+str(BitLength)+'b}"').format(int(Integer))

#======================================================================
def Equals(ValueA, ValueB=None, Size=2):
	if ValueB:
		if isinstance(ValueB, int): 
			return "({0})".format(Associate(ValueA, LogicValue(ValueB, Size), "="))
		else:
			return "({0})".format(Associate(ValueA, ValueB, "="))
	else:
		if Size>1: return "({0})".format(Associate(ValueA, "(others=>'0')", "="))
		else: return "({0})".format(Associate(ValueA, "'0'", "="))
		
#======================================================================
def NotEquals(ValueA, ValueB=None, Size=2):
	if ValueB:
		if isinstance(ValueB, int): 
			return "({0})".format(Associate(ValueA, LogicValue(ValueB, Size), "/="))
		else:
			return "({0})".format(Associate(ValueA, ValueB, "/="))
	else:
		if Size>1: return "({0})".format(Associate(ValueA, "(others=>'0')", "/="))
		else: return "({0})".format(Associate(ValueA, "'0'", "/="))

#======================================================================
def Associate(ValueA, ValueB, Symbol='='):
	return "{0} {1} {2}".format(ValueA, Symbol, ValueB)

#======================================================================
def Indent(Text, TabNB=1):
	"""
	Add specified number of tabulation to each line of the text and return it.
	"""
	if Text: return str(Text).replace("\n", "\n"+"\t"*TabNB)
	else: return ""

#======================================================================
def Comment(Text):
	if Text: 
		CommmentedText = str(Text).replace("\n", "\n"+"-- ")
		if not CommmentedText.startswith('--'): CommmentedText= "-- "+CommmentedText
		return CommmentedText
	else: return ""
	

#======================================================================
def NormalizeType(SignalType, Size, IsArray=False, Direction=None):
	if SignalType == "logic": 
		try: Max=int(Size)-1
		except: Max=str(Size)+"-1"
		try: 
			S = int(Size)
			if S==1: return "std_logic"
			elif S>1: return "std_logic_vector({0} downto {1})".format(str(Max), 0)
			else: raise NameError("Unable to recognize size format '{0}': aborted.".format(S))
		except: return "std_logic_vector({0} downto {1})".format(Max, 0)
			
	elif  SignalType == "numeric": 
		#if Size==None: return "natural"
		#else: return "natural range 0 to {0}".format(int(math.pow(2, float(Size))))
		return "natural"
	else: 
		try: Max=int(Size)-1
		except: Max=str(Size)+"-1"
		if Size>1: return SignalType+"({0} downto {1})".format(str(Max), 0)
		else: 
			#print "#\n#SignalType:", SignalType,"(size", Size,") IsArray:", IsArray,"\n#"
			if IsArray and Direction != None: return SignalType+"(0 downto 0)" 
			else:  return SignalType
		#raise NameError("Unable to recognize size format '{0}': aborted.".format(Size))

#======================================================================
def NewName(BaseName, NameList):
	"""
	Return a Name that is unique to the list, based on the pattern Base-Name + number
	"""
	NameList=list(NameList)
	NB = 0
	while NameList.count("{0}_{1}".format(BaseName, NB)): NB+=1
	return 	"{0}_{1}".format(BaseName, NB)
	
#======================================================================
def PkgDeclaration(Name, Declaration=''):
	"""
	Return code for Package declaration.
	"""
	return "\npackage {0} is\n{1}\n\nend {0};".format(Name, Indent(Declaration))

#======================================================================
def PkgBody(Name, Body=""):
	"""
	Return code for Package body.
	"""
	return "\npackage body {0} is\n{1}\n\nend {0};".format(Name, Indent(Body))

#======================================================================
def Type(TName, TType):
	"""
	Return code for type declaration.
	"""
	return "\ntype {0} is {1};".format(TName, TType)

#======================================================================
def SubType(TName, TType):
	"""
	Return code for type declaration.
	"""
	return "\nsubtype {0} is {1};".format(TName, TType)
	
#======================================================================
def ArrayType(Name, Size, SubType, ArraySize=None):
	"""
	Return code for array type declaration.
	"""
	if ArraySize!=None: return "\ntype {0} is array(natural range 0 to {1}) of {2};".format(Name, ArraySize, NormalizeType(SubType, Size))
	else: return "\ntype {0} is array(natural range <>) of {1};".format(Name, NormalizeType(SubType, Size))
	
#======================================================================
def Access(NewType, Accessed):
	"""
	Return code for VHDL Access declaration of type pointer.
	"""
	return "\ntype {0} is access {1};".format(NewType, Accessed)

#==================================================================
# Signal object
#==================================================================
class Signal:
	"""
	Object that contains basic parameters for a signal definition.
	"""
	#----------------------------------------------------------------------
	def __init__(self, Item, Direction="IN", Size=1, Type=None, InitVal=0, IsArray=False):
		if isinstance(Item, Signal):
			self.Name      = Item.Name
			self.Direction = Item.Direction
			self.Size      = Item.Size # an integer or a string  
			self.Type      = Item.Type
			self.InitVal   = Item.InitVal # string or integer or list
			self.Index     = Item.Index
			self.IsArray   = Item.IsArray
		else:
			self.Name      = Item              # a string
			if Direction: self.Direction = Direction.upper() # "IN" or "OUT"
			else: self.Direction = None # Should be None
			self.Size      = Size         # an integer        
			self.InitVal   = InitVal           # string or integer
			self.Index     = None
			self.Type      = Type
			self.IsArray   = IsArray
		
		if self.IsArray and self.Size==1:
			self.SetIndex(0)
					
	#----------------------------------------------------------------------
	def GetName(self):
		if self.Index!=None or (self.IsArray and self.Size==1):
			if isinstance(self.Index, list):
				start, stop = self.Index
				return self.Name+"({0} downto {1})".format(str(stop), start) 
			return self.Name+"({0})".format(self.Index)
		else: 
			return self.Name.replace(".", "_")
		
	#----------------------------------------------------------------------
	def GetSize(self, Full=False):
		if Full:
			if isinstance(self.GetType(), str):
				return self.Size
			else:
				return self.Size*self.Type.GetSize()
		else:
			return self.Size
			
	#----------------------------------------------------------------------
	def GetType(self):
		if self.Type: 
			if isinstance(self.Type, Signal):
				if self.Size!=None: 
					return "ARRAY{0}_{1}".format(self.Size, self.Type.Type)
				else:
					logging.error("Signal array '{0}' has no size.".format(self.Name))
					return "ARRAY{0}_{1}".format(1, self.Type.Type)
			else: 
				return NormalizeType(self.Type, self.Size, IsArray=self.IsArray, Direction=self.Direction)
		else: 
			return NormalizeType("logic", self.Size, IsArray=False, Direction=self.Direction)
				
	#----------------------------------------------------------------------
	def GetValue(self, Val=None, WriteBits=False, Vars={}):
		# Get size of signal----------------------------------
		if isinstance(self.Index, list): 
			Size=self.Index[1]-self.Index[0]
		elif isinstance(self.Index, int): 
			Size=1
		else: 
			if self.Size==None: Size=32 # Temporary ! TODO: resolve None issue
			else: Size=self.Size
			
		# Get base type of signal----------------------------------
		BaseType=self.GetType().split('(')[0].split()[0]
		# if not standard type, check for array else return empty string
		if (not BaseType.lower().startswith("std_logic")) and not (BaseType in ["integer", "natural"]):
			if isinstance(Val, list):
				return '('+','.join([str(x) for x in Val])+')'
			elif isinstance(self.InitVal, list):
				return '('+','.join([str(x) for x in self.InitVal])+')'
			else: return ""
		# If it's a standard type :
		if Val==None:
			if str(self.InitVal).lower()!="open":
				if not BaseType.lower().startswith("std_logic"):
					if isinstance(self.InitVal, list): return self.InitVal[0]
					else: return int(self.InitVal)
				else:
					if isinstance(self.InitVal, list): LogicValue(self.InitVal[0], Size)
					else: return LogicValue(self.InitVal, Size)
			else:
				if not BaseType.lower().startswith("std_logic"): return 0
				else: 
					if int(Size)<2: return "'0'"
					else: 
						if WriteBits:
							return '"{0}"'.format("0"*int(Size))
						else:
							return "(others=>'0')"
		if Val==0:
			if not BaseType.lower().startswith("std_logic"): return 0
			else: 
				if int(Size)<2: return "'0'"
				else: 
					if WriteBits:
						return '"{0}"'.format("0"*int(Size))
					else:
						return "(others=>'0')"
		else:
			if not BaseType.lower().startswith("std_logic"): return Val
			else: return LogicValue(Val, Size)  
			
		
	#----------------------------------------------------------------------
	def SetIndex(self, Min, Max=None):
		"""
		Change signal name with it's VHDL indexation representation.
		"""
		#if Max!=None: 
		if Min!=None:
			if Max==None: self.Index=Min
			else: self.Index=[Min, Max]
		else: 
			self.Index=None
			if self.IsArray and self.Size==1: # To appear as indexed signal ([Array 0 downto 0] must be indexed to 0)
				self.SetIndex(0)
		return self
	
#	#----------------------------------------------------------------------
#	def Connect(self, OtherSignal, UseValue=False):
#		if self.Direction == OtherSignal.Direction:
#			logging.error("Try to connect same signal's IO directions ({0}): skipped.".format(self.Direction))
#			return ""
#		else:
#			if self.Size==OtherSignal.Size:
#				if UseValue:
#					if self.Direction == "IN": 
#						return Connect(self.GetName(), OtherSignal.GetValue())
#					else: return Connect(OtherSignal.GetName(), self.GetValue())
#					
#				else:
#					if self.Direction == "IN": 
#						return Connect(self.GetName(), OtherSignal.GetName())
#					else: return Connect(OtherSignal.GetName(), self.GetName())
#			else:
#				logging.error("Try to connect signals of different size: skipped.")
#				return ""
	
	#----------------------------------------------------------------------
	def Connect(self, SignalX=None, UseValue=False, ValCondPairs=[]):
		"""
		Generate code for 2 signals connection (VHDL)
		"""
		if len(ValCondPairs):
			Drivers = []
			for Val, Cond in ValCondPairs:
				Drivers.append("{0} when {1}".format(Val, Cond))
			Sep = "\n"+(" "*(len(SignalA)+4)) # Align all 'when'
			Driver = Sep.join(Drivers)
		else:
			if SignalX!=None: 
				if UseValue: Driver = SignalX.GetValue()
				else:        Driver = SignalX.GetName()
			else: Driver = LogicValue(0, self.Size)
		return '\n'+Associate(self.GetName(), Driver, "<=")+';'
			
				
	#----------------------------------------------------------------------
	def Parameters(self):
		"""
		return a list with parameters according to HDLEditor signal representation.
		"""
		return [self.GetName(), self.GetType(), self.InitVal, self.Size]
		
	#----------------------------------------------------------------------
	def Driven(self, Value):
		"""
		Signal connection: Example:"S0.Driven(S1+S2)"
		"""
		return Connect(self.Name, str(Value))
		
	#----------------------------------------------------------------------
	def Declare(self, Constant=False):
		"""
		return Signal declaration code.
		"""
		if Constant: Text="\nconstant "
		else: Text="\nsignal "
		Name = self.Name
		if self.InitVal!=None:
			if str(self.InitVal).lower() == "open" or self.GetValue(self.InitVal)=="":
				Text+="{0} : {1};".format(Name, self.GetType())
			else:
				Text+="{0} : {1} := {2};".format(Name, self.GetType(), self.GetValue(self.InitVal))
		else:
			Text+="{0} : {1};".format(Name, self.GetType())
		return Text
		
	#----------------------------------------------------------------------
	def AliasOf(self, Sig):
		"""
		Declare Signal as alias of another signal.
		"""
		return "\nalias {0} is {1};".format(self.Name, Sig.GetName())
		
	#----------------------------------------------------------------------
	def __getitem__(self, index):
		if isinstance(index, int) or isinstance(index, str):
			Sig = self.Copy().SetIndex(index)
			return Signal(Sig.GetName(), self.Direction, 1, self.GetType(), self.InitVal)
		elif isinstance(index, slice):
			start, stop, step = index.indices(len(self))    # index is a slice
			# process slice
			Sig = Signal(self.GetName(), self.Direction, stop-start, Type=None, InitVal=0)
			if start<(stop-1): Sig.SetIndex(start, stop-1)
			else: Sig.SetIndex(start)
			return Sig
		else:
			raise TypeError("index must be int or slice")
		
	#----------------------------------------------------------------------
	def __add__(self, SignalName):
		"""
		Signal concatenation(if logic) or addition(if numeric): "S0 + S1"
		"""
		return self.Concatenate(SignalName)
		
	#----------------------------------------------------------------------
	def Concatenate(self, Sig):
		"""
		Return a new signal object which name is the VHDL representation
		of signals concatenation.
		"""
		if isinstance(Sig, Signal): SignalName = Sig.GetName()
		elif isinstance(Sig, str): SignalName = Sig
		else: 
			raise TypeError("Signal '{0}' is not a string or a Signal object.")
			return None
		NewSig = self.Copy()
		NewSig.Name = "{0} & {1}".format(self.GetName(), SignalName)
		return NewSig
		
	#---------------------------------------------------------------
	def Copy(self):
		return Signal(self)	
		
	#----------------------------------------------------------------------
	def __str__(self):
		"""
		Return string representation.
		"""
		return self.GetName()
		
	#----------------------------------------------------------------------
	def __repr__(self):
		"""
		Return object representation.
		"""
		return '<HDL Signal({0}={1})'.format(self.GetName(), self.GetValue())+'>'
		
	#----------------------------------------------------------------------
	def __len__(self):
		"""
		Return size of signal.
		"""
		return self.Size
	
#======================================================================
		
def Connect(SignalA, SignalB=None, ValCondPairs=[], Size=1):
	"""
	Generate code for 2 signals connection (VHDL)
	"""
	if len(ValCondPairs):
		Drivers = []
		for Val, Cond in ValCondPairs:
			Drivers.append("{0} when {1}".format(Val, Cond))
		Sep = "\n"+(" "*(len(SignalA)+4)) # Align all 'when'
		Driver = Sep.join(Drivers)
	else:
		if SignalB: Driver = SignalB
		else: Driver = LogicValue(0, Size)
	return '\n'+Associate(SignalA, Driver, "<=")+';'
	

#======================================================================
def NormalizeText(text):
	"""
	Return a normalized format for the input text.
	"""
	return unicodedata.normalize('NFKD', str(text)).encode('ASCII', 'ignore')
	
#======================================================================

# ====================     START OF MODULE TEST  =====================
if (__name__ == "__main__"):

	HDLTopFile=os.path.abspath("./HDLEditor/NOC.vhd")
	toVHDL.name="NOC"
	toVHDL(*HDL2myHDL(HDLTopFile))


















