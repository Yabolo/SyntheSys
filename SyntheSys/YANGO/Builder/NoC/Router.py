
import myhdl as HDL
import random as RDM
#====================================================================
def ClkDriver(Clk, Period=20):
	"""
	Clock Generator
	"""
	LowTime  = int(Period/2)
	HighTime = Period - LowTime

	@HDL.instance
	def DriveClk():
		while True:
			yield HDL.delay(LowTime)
			Clk.next = 1
			yield HDL.delay(HighTime)
			Clk.next = 0
	return DriveClk

#====================================================================
def HERMES_Switch_tb(Duration):
	"""
	Print Hello world !
	"""
	ModuleName="Switch"
		
	Rst = HDL.Signal(HDL.intbv(0))
	HalfPeriod = HDL.delay(5)
	Clk = HDL.Signal(HDL.intbv(0))
	DataWidth = 32
	NbInputs  = 5
	NbOutputs = 5
	Inputs  = [HDL.Signal(HDL.intbv(0, min=0, max=DataWidth)) for k in range(0, NbInputs)]
	Outputs = [HDL.Signal(HDL.intbv(0, min=0, max=DataWidth)) for k in range(0, NbOutputs)]
	Cmd = HDL.Signal(HDL.intbv(0, min=0, max=NbOutputs))
	
	#-ClockGenerator-------------------
	@HDL.always(HalfPeriod)
	def ClkDriver():
		Clk.next = not Clk
		
	#-StimuliGenerator-----------------
	@HDL.instance
	def Stimuli():
		#-INIT---------------------
		Rst.next = 1
		yield Clk.negedge
		Rst.next = 0
		#-START--------------------
		for i in range(Duration):
			for Index in range(0, len(Inputs)):
				Inputs[Index].next = RDM.randrange(len(Inputs))
			yield Clk.negedge
		raise HDL.StopSimulation
		
	#-Monitor--------------------------
	@HDL.instance
	def monitor():
		yield Rst.posedge
		while 1:
			yield Clk.posedge
			yield HDL.delay(1)
			print("[{0}]: Inputs={1}, Outputs={2}".format(HDL.now(), Inputs, Outputs))
			
	#-LaunchSimulation-----------------
	TestedModule = HERMES_Switch(Cmd, Inputs, Outputs)
	
	print("Start of '{0}' simulation.".format(ModuleName))
	Simu = HDL.Simulation(HDL.instances())
	Simu.run(Duration)

	return True
	

#====================================================================
def HERMES_Switch(Cmd, Inputs, Outputs):
	"""
	Connect inputs to specified outputs according to Cmd.
	Cmd is a vector of output length of input index.
	Inputs and outputs are arrays of bit vector.
	"""
	@HDL.always_comb
	def LinkInToOut():
		for I in range(0, len(Inputs)):
			for O in range(0, len(Cmd)):
				if Cmd[O]==I:
					Outputs[O]=Inputs[I]
		
	return LinkInToOut
	
	
######################################################################################
if __name__ == '__main__':

	HERMES_Switch_tb(50)


