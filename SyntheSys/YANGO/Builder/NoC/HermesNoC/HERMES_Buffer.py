
import subprocess, datetime, math
import myhdl as HDL
import random as RDM
	
#====================================================================
# HEADER PARAMETERS
#====================================================================
ModuleName = "HERMES_Buffer"
Title      = "HERMES NoC input queue"
Purpose    = "Buffer the input of a router port" 
Desc       = "?"
Issues     = ""
Speed      = ""
Area       = ""
Tool       = "Xilinx ISE (13.1)"
Rev        = "0.12"
#====================================================================
# MODULE PARAMETERS
#====================================================================
NbPort      = 6
BufferDepth = 6
FlitWidth   = 16
PtrWidth    = int(math.ceil(math.log(BufferDepth, 2)))
	
#====================================================================
def Module(clock, reset, data_in, rx, ack_rx, h, ack_h, data_av, data, data_ack, sender):
	"""
	Schedule input connection order with a FSM.
	SWCmd is a vector with length of output port number of inputs index.
	InputTarget is a vector with length of input port number of outputs index.
	SOPList, EOPList are bit vector (one bit per input).
	"""
	SType=HDL.enum("S_INIT", "S_PAYLOAD", "S_SENDHEADER", "S_HEADER", "S_END", "S_END2")
	Ea = HDL.Signal(SType.S_INIT)

	first = HDL.Signal(HDL.intbv(0)[PtrWidth:])
	
	QHasSpace    = HDL.Signal(bool(0))
	last  = HDL.Signal(HDL.intbv(0)[PtrWidth:])
	#----------------------------------
	@HDL.always(clock.posedge, reset.posedge)
	def SpaceInBuffer():
		if reset==1: QHasSpace.next = 1 
		else:
			if (first==0 and last==BufferDepth - 1) or (first==last+1):
				QHasSpace.next = 0 
			else:
				QHasSpace.next = 1
		
	#----------------------------------
	@HDL.always_comb
	def DriveAck_rx():
		auxack_rx = 0
		if (QHasSpace==1 and rx==1) or (auxack_rx==1 and rx==1):
			auxack_rx = 1	
		else:   auxack_rx = 0
		ack_rx.next = auxack_rx
		
	buf   = [HDL.Signal(HDL.intbv(0)[FlitWidth:]) for i in range(0, BufferDepth)]
	#----------------------------------
	@HDL.always(clock.posedge, reset.posedge)
	def BufferFilling():
		if reset==1: last.next = 0
		else:
			if QHasSpace==1 and rx==1:
				buf[last.val].next = data_in
				# increment the last
				if last == (BufferDepth - 1): last.next = 0
				else: last.next = last + 1
				
	#----------------------------------
	@HDL.always_comb
	def DataFirst():
		data.next = buf[first]

	counter_flit = HDL.Signal(HDL.intbv(0)[FlitWidth:])
	#----------------------------------
	@HDL.always(clock.posedge, reset.posedge)
	def Main():
		if reset==1:
			counter_flit.next = 0
			h.next       = 0
			data_av.next = 0
			sender.next  = 0
			first.next   = 0
			Ea.next      = SType.S_INIT
		else:
			if Ea.__next__ == SType.S_INIT:  
				counter_flit.next = 0
				h.next = 0                 
				data_av.next = 0
				if first != last: # detectou dado na fila
					h.next = 1 # pede roteamento
					Ea.next = SType.S_HEADER
				else:
					Ea.next = SType.S_INIT
					
			elif Ea.__next__ == SType.S_HEADER:
				if ack_h==1:
					# depois de rotear envia o pacote 
					Ea.next      = SType.S_SENDHEADER 
					h.next       = 0
					data_av.next = 1
					sender.next  = 1
				else: Ea.next = SType.S_HEADER
						
			elif Ea == SType.S_SENDHEADER: 
				if data_ack==1: # mantehm este dado ateh a resposta
					Ea.next = SType.S_PAYLOAD
					data_av.next = 0   

					# retira um dado do buffer
					if (first == BufferDepth -1): first.next = 0
					else: first.next = first+1

				elif first != last:
					data_av.next = 1
					Ea.next = SType.S_SENDHEADER
				else:
					data_av.next = 0
					Ea.next = SType.S_SENDHEADER

			elif Ea == SType.S_PAYLOAD :
				if data_ack == 1 and counter_flit != 1: 
					# confirmacao do envio de um dado que nao eh o tail
					# se counter_flit eh zero indica recepcao do size do payload
					if counter_flit == 0: counter_flit.next = buf[first] 
					else: counter_flit.next = counter_flit - 1 

					# retira um dado do buffer
					if (first == BufferDepth -1): first.next = 0
					else: first.next = first+1

					data_av.next = 0 
					Ea.next = SType.S_PAYLOAD
			
				elif data_ack == 1 and counter_flit == 1: 
					# confirmacao do envio do tail
					# retira um dado do buffer
					if first==(BufferDepth-1): first.next = 0
					else: first.next = first+1

					data_av.next = 0 
					sender.next  = 0
					Ea.next = SType.S_END

				elif first != last :
					data_av.next = 1
					Ea.next = SType.S_PAYLOAD
				else:
					data_av.next = 0
					Ea.next = SType.S_PAYLOAD
					
			elif Ea == SType.S_END : 
				data_av.next = 0
				Ea.next = SType.S_END2
			elif Ea == SType.S_END2 : # estado necessario para permitir a liberacao da porta antes da solicitacao de novo envio
				data_av.next = 0
				Ea.next = SType.S_INIT
				
	return HDL.instances()
		
#====================================================================
def TB():
	"""
	Send/receive value to/from Switch module.
	Return myhdl simulation object. 
	"""
	#-Signals------------------------------------------
	clock    = HDL.Signal(bool(0)) # IN 
	reset    = HDL.Signal(bool(0)) # IN 
	data_in  = HDL.Signal(HDL.intbv(0)[FlitWidth:]) # IN 
	rx       = HDL.Signal(bool(0)) # IN 
	ack_rx   = HDL.Signal(bool(0)) # OUT 
	h        = HDL.Signal(bool(0)) # OUT
	ack_h    = HDL.Signal(bool(0)) # IN 
	data_av  = HDL.Signal(bool(0)) # OUT
	data     = HDL.Signal(HDL.intbv(0)[FlitWidth:])
	data_ack = HDL.Signal(bool(0)) # IN 
	sender   = HDL.Signal(bool(0)) # OUT
	
	#-Top Module---------------------------------------
	TestedModule = Module(clock, reset, data_in, rx, ack_rx, h, ack_h, data_av, data, data_ack, sender)
		
	#-ClockGenerator-----------------------------------
	HalfPeriod = HDL.delay(5)
	@HDL.always(HalfPeriod)
	def ClkDriver():
		clock.next = not clock
	
	#-StimuliGenerator---------------------------------
	@HDL.instance
	def Stimuli():
		#-INIT-------------------------------------
		reset.next = 1
		yield HDL.delay(10)
		yield clock.negedge
		reset.next = 0
		#-START------------------------------------
		while True:
			for Sig in [data_in, rx, ack_h, data_ack]: 
				Sig.next = RDM.randrange(0, max(Sig.max, 2))
				
			yield HDL.delay(20)
		raise HDL.StopSimulation
	
	#-Monitor------------------------------------------
	@HDL.instance
	def monitor():
		print("Start of '{0}' simulation.".format(ModuleName))
		yield reset.posedge
		while True:
			yield clock.posedge
			print("Step[{0}]".format(HDL.now())+"-"*50)
			print("INPUTS: data_in={0}\trx={1}\tack_h={2}\tdata_ack={3}".format(data_in, rx, ack_h, data_ack))
			print("OUTPUTS: ack_rx={0}\th={1}\tdata_av={2}\tsender={3}".format(ack_rx, h, data_av, sender))
			yield HDL.delay(20)
		
	HDL.traceSignals.name = ModuleName
	return HDL.instances()
	
#====================================================================
def GenHDL():

	#-Signals------------------------------------------
	clock    = HDL.Signal(bool(0)) # IN 
	reset    = HDL.Signal(bool(0)) # IN 
	data_in  = HDL.Signal(HDL.intbv(0)[FlitWidth:]) # IN 
	rx       = HDL.Signal(bool(0)) # IN 
	ack_rx   = HDL.Signal(bool(0)) # OUT 
	h        = HDL.Signal(bool(0)) # OUT
	ack_h    = HDL.Signal(bool(0)) # IN 
	data_av  = HDL.Signal(bool(0)) # OUT
	data     = HDL.Signal(HDL.intbv(0)[FlitWidth:])
	data_ack = HDL.Signal(bool(0)) # IN 
	sender   = HDL.Signal(bool(0)) # OUT*
	
	# Verify for convertion ------------------------------
	#HDL.conversion.analyze(HERMES_Buffer, clock, reset, data_in, rx, ack_rx, h, ack_h, data_av, data, data_ack, sender)
	
	# Convert to Verilog-----------------------------------
	HDL.toVerilog.no_myhdl_header = True
	HDL.toVerilog.header = Header(Lang="Verilog")
	HDL.toVerilog.name = ModuleName
	HDL.toVerilog(Module, clock, reset, data_in, rx, ack_rx, h, ack_h, data_av, data, data_ack, sender)
	
	# Convert to VHDL-----------------------------------
	HDL.toVHDL.no_myhdl_header = True
	HDL.toVHDL.header = Header(Lang="VHDL")
	HDL.toVHDL.name   = ModuleName
	inc_inst = HDL.toVHDL(Module, clock, reset, data_in, rx, ack_rx, h, ack_h, data_av, data, data_ack, sender)

#======================================================================
def Header(Lang):
	"""
	Return string commented header.
	"""
	if Lang=="VHDL":      Token = '--'	
	elif Lang=="Verilog": Token = '//'

	Head = Token*50+"""
{TOK} Actual File Name      = '{MODULE}.vhd'
{TOK} Title & purpose       = {TITLE} - {PURPOSE}
{TOK} Author                = Automaticaly generated by YANGO - made by Matthieu PAYET (ADACSYS)
{TOK} Creation Date         = {TIME}
{TOK} Version               = 0.1
{TOK} Simple Description    = {DESCRIPTION}
{TOK} Specific issues       = {ISSUES}
{TOK} Speed                 = {SPEED}
{TOK} Area estimates        = {AREA}
{TOK} Tools (version)       = {TOOL}
{TOK} HDL standard followed = VHDL 2001 standard
{TOK} Revisions & ECOs      = {REV}
"""+Token*50
	return Head.format(
			TOK = Token,
			MODULE=ModuleName, 
			TITLE=Title, 
			PURPOSE=Purpose, 
			DESCRIPTION=Desc, 
			ISSUES=Issues, 
			SPEED=Speed, 
			AREA=Area, 
			TOOL=Tool, 
			REV=Rev,
			TIME=datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
	
######################################################################################
if __name__ == '__main__':
	
	HDL.Simulation(HDL.traceSignals(TB)).run(500)
	
	GenHDL()
	
	# Display waveforms
	subprocess.call(["gtkwave", "TB.vcd"])



