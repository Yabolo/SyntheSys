
import myhdl as HDL
import random as RDM
import subprocess, datetime	

#====================================================================
# HEADER PARAMETERS
#====================================================================
ModuleName = "HERMES_SwitchCtrl"
Title      = "HERMES NoC input queue"
Purpose    = "Buffer the input of a router port" 
Desc       = "?"
Issues     = ""
Speed      = ""
Area       = ""
Tool       = "Xilinx ISE (13.1)"
Rev        = "0.12"
#====================================================================
# MODULE PARAMETERS
#====================================================================
NbPort     = 5
FlitWidth  = 8
METADEFLIT = FlitWidth/2
QUARTOFLIT = METADEFLIT/2
LOCAL      = 4
EAST       = 1
WEST       = 2
NORTH      = 3
SOUTH      = 4
	
#====================================================================
def ModulePort():
	"""
	Return module signal dictionary.
	"""
	#-Signals------------------------------------------
	clock   = HDL.Signal(bool(0)) # IN  
	reset   = HDL.Signal(bool(0)) # IN   
	address = HDL.Signal(HDL.intbv(0)[METADEFLIT:]) # IN
	h       = HDL.Signal(HDL.intbv(0)[NbPort:])
	ack_h   = HDL.Signal(HDL.intbv(0)[NbPort:])
	data    = HDL.Signal(HDL.intbv(0)[FlitWidth*NbPort:])
	sender  = HDL.Signal(HDL.intbv(0)[NbPort:])
	free    = HDL.Signal(HDL.intbv(0)[NbPort:])

	mux_in  = HDL.Signal(HDL.intbv(0)[3*NbPort:])
	mux_out = HDL.Signal(HDL.intbv(0)[3*NbPort:])

		
	return [clock, reset, h, ack_h, address, data, sender, free, mux_in, mux_out]

#====================================================================
def Module(clock, reset, h, ack_h, address, data, sender, free, mux_in, mux_out):
	"""
	Connect inputs to specified outputs according to Cmd.
	Cmd is a vector of output length of input index.
	Inputs and outputs are arrays of bit vector.
	"""			
	State = HDL.enum("S0","S1","S2","S3","S4","S5","S6","S7")
	ES    = HDL.Signal(State.S0)

	# Arbiter signals
	ask       = HDL.Signal(bool(0))
	sel, prox = [HDL.Signal(HDL.intbv(0, min=0, max=NbPort)) for i in range(2)]
	
	# Control signals
	dirx, diry = [HDL.Signal(HDL.intbv(0, min=0, max=NbPort)) for i in range(2)]
	lx, ly, tx, ty = [HDL.Signal(HDL.intbv(0)[QUARTOFLIT:]) for i in range(4)]
	auxfree    = HDL.Signal(HDL.intbv(0)[NbPort:])
	auxfree_i  = [HDL.Signal(bool(0)) for i in range(NbPort)]
	source     = HDL.Signal(HDL.intbv(0)[3*NbPort:])

	sender_ant = HDL.Signal(HDL.intbv(0)[NbPort:])

	incoming  = HDL.Signal(HDL.intbv(0)[3:])
	header    = HDL.Signal(HDL.intbv(0)[FlitWidth:])

	#------------------------------------
	data_i    = [HDL.Signal(HDL.intbv(0)[FlitWidth:]) for i in range(NbPort)]
	data_ii   = [HDL.Signal(HDL.intbv(0)[FlitWidth:]) for i in range(NbPort)]
	mux_in_i  = [HDL.Signal(HDL.intbv(0)[3:]) for i in range(NbPort)]
	free_i    = [HDL.Signal(bool(0)) for i in range(NbPort)]
	ack_h_i   = [HDL.Signal(bool(0)) for i in range(NbPort)]
	mux_out_i = [HDL.Signal(HDL.intbv(0)[3:]) for i in range(NbPort)]
	sender_ant_i = [HDL.Signal(bool(0)) for i in range(NbPort)]
	source_i   = [HDL.Signal(HDL.intbv(0)[3:]) for i in range(NbPort)]

	#--------------------------
	ack_h_ii    = HDL.ConcatSignal(*ack_h_i)
	mux_out_ii  = HDL.ConcatSignal(*mux_out_i)
	free_ii     = HDL.ConcatSignal(*free_i)
	mux_in_ii   = HDL.ConcatSignal(*mux_in_i)
	auxfree_ii  = HDL.ConcatSignal(*auxfree_i)
	sender_ant_ii = HDL.ConcatSignal(*sender_ant_i)
	
	for P in range(NbPort):
		data_i[P].next = data(incoming*FlitWidth+1, incoming)
	
	tx_i = header(METADEFLIT-1, QUARTOFLIT)
	ty_i = header(QUARTOFLIT-1, 0)
		
	#--------------------------
	@HDL.always_comb
	def DataShadowing():
		for P in range(NbPort):
			data_ii[P].next = data_i[P]
	#--------------------------
	@HDL.always_comb
	def Shadowing():
		ack_h.next    = ack_h_ii
		mux_out.next  = mux_out_ii
		free.next     = free_ii
		mux_in.next   = mux_in_ii
		auxfree.next  = auxfree_ii
		sender_ant.next = sender_ant_ii
		

	#------------------------------------
	@HDL.always_comb
	def SetHeader():
		header.next = data_ii[incoming]

	#--------------------------
	# Slice Address and header
	lx.next = address(METADEFLIT-1, QUARTOFLIT)
	ly.next = address(QUARTOFLIT-1, 0)

	#------------------------------------
	@HDL.always_comb
	def TargetCoordinates():
		tx.next = tx_i
		ty.next = ty_i
	#--------------------------
	@HDL.always_comb
	def DriveAsk():
		if h[LOCAL]==1 or h[EAST]==1 or h[WEST]==1 or h[NORTH]==1 or h[SOUTH]==1:
			ask.next = 1
		else:   ask.next = 0
	
	#--------------------------
	@HDL.always_comb
	def SetIncoming():
		incoming.next = sel	

	#--------------------------
	@HDL.always_comb
	def SetProx():
		if sel==LOCAL:
			if h[EAST]==1:    prox.next=EAST
			elif h[WEST]==1:  prox.next=WEST
			elif h[NORTH]==1: prox.next=NORTH
			elif h[SOUTH]==1: prox.next=SOUTH
			else:             prox.next=LOCAL
		elif sel==EAST:
			if h[WEST]==1:    prox.next=WEST
			elif h[NORTH]==1: prox.next=NORTH
			elif h[SOUTH]==1: prox.next=SOUTH
			elif h[LOCAL]==1: prox.next=LOCAL
			else:             prox.next=EAST
		elif sel==WEST:
			if h[NORTH]==1:   prox.next=NORTH
			elif h[SOUTH]==1: prox.next=SOUTH
			elif h[LOCAL]==1: prox.next=LOCAL
			elif h[EAST]==1:  prox.next=EAST
			else:             prox.next=WEST
		elif sel==NORTH:
			if h[SOUTH]==1:   prox.next=SOUTH
			elif h[LOCAL]==1: prox.next=LOCAL
			elif h[EAST]==1:  prox.next=EAST
			elif h[WEST]==1:  prox.next=WEST
			else:             prox.next=NORTH
		elif sel==SOUTH:
			if h[LOCAL]==1:   prox.next=LOCAL
			elif h[EAST]==1:  prox.next=EAST
			elif h[WEST]==1:  prox.next=WEST
			elif h[NORTH]==1: prox.next=NORTH
			else:             prox.next=SOUTH
			

	#--------------------------
	@HDL.always_comb
	def PathFinder():
		if lx > tx: dirx.next = WEST
		else:       dirx.next = EAST
		
		if ly < ty: diry.next = NORTH
		else:       dirx.next = SOUTH
		
	#--------------------------
	@HDL.always(clock.posedge)
	def SenderAllocation():
		sender_ant_i[LOCAL].next = sender[LOCAL]
		sender_ant_i[EAST].next  = sender[EAST]
		sender_ant_i[WEST].next  = sender[WEST]
		sender_ant_i[NORTH].next = sender[NORTH]
		sender_ant_i[SOUTH].next = sender[SOUTH]
		
	#--------------------------
	@HDL.always(clock.posedge)
	def FreeCheck():
		if sender[LOCAL]==0 and sender_ant[LOCAL]==1: 
			auxfree_i[source[LOCAL]]=1
		if sender[EAST] ==0 and sender_ant[EAST]==1 : 
			auxfree_i[source[EAST]] =1
		if sender[WEST] ==0 and sender_ant[WEST]==1 : 
			auxfree_i[source[WEST]] =1
		if sender[NORTH]==0 and sender_ant[NORTH]==1: 
			auxfree_i[source[NORTH]]=1
		if sender[SOUTH]==0 and sender_ant[SOUTH]==1: 
			auxfree_i[source[SOUTH]]=1
	

	#--------------------------
	@HDL.always_comb
	def DataFlow():
		for P in range(NbPort):
			mux_in_i[P].next = source[P]
			free_i[P].next   = auxfree[P]
	
	#--------------------------
	@HDL.always(clock.posedge, reset.posedge)
	def SetState():
		if reset == 1:     
			ES.next = State.S0
		else:
			# Initialization
			if ES==State.S0: 
				ES.next = State.S1
				sel.next = 0
				#auxfree.next = int('1'*len(auxfree), 2)
				for P in range(0, NbPort):
					ack_h_i[P].next      = 0
					#sender_ant_i[P] = 0
					mux_out_i[P].next    = 0
					source_i[P].next     = 0
			# a Header arrives
			elif ES==State.S1: 
				for P in range(0, NbPort):
					ack_h_i[P].next = 0
				if ask==1: 
					ES.next = State.S2
				else:      
					ES.next = State.S1
			# Selects who will be entitled to request routing
			elif ES==State.S2: 
				sel.next = prox
				ES.next = State.S3
			elif ES==State.S3: 
				if lx==tx and ly==ty and auxfree[LOCAL]==1:  
					ES.next=State.S4
				elif lx!=tx and auxfree[dirx]==1:            
					ES.next=State.S5
				elif lx==tx and ly!=ty and auxfree[diry]==1: 
					ES.next=State.S6
				else: ES.next=State.S1
			# Establishes a connection to the port LOCAL         
			elif ES==State.S4: 
				source_i[incoming].next = LOCAL
				mux_out_i[LOCAL].next = incoming
				#auxfree[LOCAL]   = 0
				ack_h_i[sel].next     = 1
				ES.next=State.S7
			# Establishes a connection to the port or EAST WEST
			elif ES==State.S5:
				source_i[incoming].next = dirx
				mux_out_i[dirx].next  = incoming
				#auxfree[dirx]    = 0 
				ack_h_i[sel].next     = 1 
				ES.next=State.S7 
			# Establishes a connection to the NORTH or SOUTH port
			elif ES==State.S6: 
				source_i[incoming].next = diry
				mux_out_i[diry].next  = incoming
				#auxfree[diry]   = 0 
				ack_h_i[sel].next     = 1
				ES.next=State.S7 
			elif ES==State.S7: 
				ES.next=State.S1
			else: 
				ack_h_i[sel].next     = 0
				ES.next = State.S0

		
	return HDL.instances()
	
#====================================================================
def TB():
	"""
	Send/receive value to/from Switch module.
	Return myhdl simulation object. 
	"""
	#-Top Module---------------------------------------
	Port      = ModulePort()
	TestedModule = Module(*Port)
	clock, reset, h, ack_h, address, data, sender, free, mux_in, mux_out = Port
	
	#-ClockGenerator-----------------------------------
	HalfPeriod=HDL.delay(50)
	@HDL.always(HalfPeriod)
	def ClkDriver():
		clock.next = not clock
		
	#-StimuliGenerator---------------------------------
	@HDL.instance
	def Stimuli():
		#-INIT-------------------------------------
		reset.next = 1
		yield HDL.delay(10)
		yield clock.negedge
		reset.next = 0
		#-START------------------------------------
		while True:
			for Sig in [h, address, data, sender]: 
				Sig.next = RDM.randrange(0, len(Sig))
				
			yield HDL.delay(20)
		raise HDL.StopSimulation
	
	#-Monitor------------------------------------------
	@HDL.instance
	def monitor():
		print("Start of '{0}' simulation.".format(ModuleName))
		yield reset.posedge
		while True:
			yield clock.posedge
			print("Step[{0}]".format(HDL.now())+"-"*50)
			print("INPUTS: ", end=' ')
			for Sig in Port:
				print("{0} ".format(Sig), end=' ')
			print("")
			yield HDL.delay(20)
		
	HDL.traceSignals.name = ModuleName
	return HDL.instances()
	
	
#====================================================================
def GenHDL():
	clock, reset, h, ack_h, address, data, sender, free, mux_in, mux_out = ModulePort()
	# Convert to VHDL-----------------------------------
	HDL.toVHDL.no_myhdl_header = True
	HDL.toVHDL.header          = Header(Lang="VHDL")
	HDL.toVHDL.name            = ModuleName
	inc_inst = HDL.toVHDL(Module, clock, reset, h, ack_h, address, data, sender, free, mux_in, mux_out)

	# Convert to Verilog-----------------------------------
	HDL.toVerilog.no_myhdl_header = True
	HDL.toVerilog.header          = Header(Lang="Verilog")
	HDL.toVerilog.name            = ModuleName
	HDL.toVerilog(Module, clock, reset, h, ack_h, address, data, sender, free, mux_in, mux_out)
	

#======================================================================
def Header(Lang):
	"""
	Return string commented header.
	"""
	if Lang=="VHDL":      Token = '--'	
	elif Lang=="Verilog": Token = '//'

	Head = Token*50+"""
{TOK} Actual File Name      = {MODULE}.vhd
{TOK} Title & purpose       = {TITLE} - {PURPOSE}
{TOK} Author                = Automaticaly generated by YANGO - made by Matthieu PAYET (ADACSYS)
{TOK} Creation Date         = {TIME}
{TOK} Version               = 0.1
{TOK} Simple Description    = {DESCRIPTION}
{TOK} Specific issues       = {ISSUES}
{TOK} Speed                 = {SPEED}
{TOK} Area estimates        = {AREA}
{TOK} Tools (version)       = {TOOL}
{TOK} HDL standard followed = VHDL 2001 standard
{TOK} Revisions & ECOs      = {REV}
"""+Token*50
	return Head.format(
			TOK = Token,
			MODULE=ModuleName, 
			TITLE=Title, 
			PURPOSE=Purpose, 
			DESCRIPTION=Desc, 
			ISSUES=Issues, 
			SPEED=Speed, 
			AREA=Area, 
			TOOL=Tool, 
			REV=Rev,
			TIME=datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
	
######################################################################################
if __name__ == '__main__':
	
	HDL.Simulation(HDL.traceSignals(TB)).run(500)
	
	# Display waveforms
	subprocess.call(["gtkwave", "TB.vcd"])
	
	Ret = "?"
	while(Ret!='O' and Ret!='n'): Ret = input("Generate VHDL and Verilog files (O/n) ? ")
	if Ret=='O': GenHDL()
	


