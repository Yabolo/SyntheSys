
import myhdl as HDL
import random as RDM
import datetime, subprocess
	
import HERMES_Buffer
import HERMES_SwitchCtrl

#====================================================================
# HEADER PARAMETERS
#====================================================================
ModuleName = "HERMES_Router"
Title      = "HERMES NoC router"
Purpose    = "Route packets through the network" 
Desc       = "?"
Issues     = ""
Speed      = ""
Area       = ""
Tool       = "Xilinx ISE (13.1)"
Rev        = "0.12"
#====================================================================
# MODULE PARAMETERS
#====================================================================

NbPort      = 6
FlitWidth   = 8
BufferDepth = 8
Address     = 0
RouterType  ="CC"

HERMES_SwitchCtrl.NbPort      = NbPort
HERMES_SwitchCtrl.FlitWidth   = FlitWidth
HERMES_Buffer.FlitWidth       = FlitWidth
HERMES_Buffer.BufferDepth     = BufferDepth

#====================================================================
def Module(reset, clock, data_in, rx, ack_rx, data_out, tx, ack_tx):
	"""
	Connect inputs to specified outputs according to Cmd.
	Cmd is a vector of output length of input index.
	Inputs and outputs are arrays of bit vector.
	"""
	h, ack_h, data_av, sender, data_ack = [HDL.Signal(HDL.intbv(0)[NbPort:]) for i in range(0,5)]
	data    = HDL.Signal(HDL.intbv(0)[NbPort*FlitWidth:])
	mux_in  = HDL.Signal(HDL.intbv(0)[NbPort*3:])
	mux_out = HDL.Signal(HDL.intbv(0)[NbPort*3:])
	mux_out = HDL.Signal(HDL.intbv(0)[3*NbPort:])
	free    = HDL.Signal(HDL.intbv(0)[NbPort:])
	address = HDL.Signal(HDL.intbv(Address)[HERMES_SwitchCtrl.METADEFLIT:])
	#------------------------------------
	data_in_i  = [HDL.Signal(HDL.intbv(0)[FlitWidth:]) for i in range(NbPort)]
	for P in range(NbPort):
		data_in_i = data_in(P*FlitWidth+1,P)
	data_out_i = [HDL.Signal(HDL.intbv(0)[FlitWidth:]) for i in range(NbPort)]
	data_out.next   = HDL.ConcatSignal(*data_out_i)

	tx_i    = [HDL.Signal(bool(0)) for i in range(NbPort)]
	tx.next = HDL.ConcatSignal(*tx_i)

	#------------------------------------
	@HDL.always_comb
	def MUXS():
		for i in range(NbPort):
			if free[i]==0:
				data_out_i[i] <= data[mux_out[i]]
				tx_i[i] <= data_av[mux_out[i]]
			else:
				data_out_i[i] <= 0
				tx_i[i] <= 0
		
			if sender[i]==0: data_ack[i] <= ack_tx[mux_in[i]]
			else:            data_ack[i] <= 0
	
	#------------------------------------
	# Buffer instantiation
	Buffer     = HERMES_Buffer.Module(clock, reset, data_in, rx, ack_rx, h, ack_h, data_av, data, data_ack, sender)
	#------------------------------------
	# Switch control instantiation
	SwitchCtrl = HERMES_SwitchCtrl.Module(clock, reset, h, ack_h, address, data, sender, free, mux_in, mux_out)
	

	return HDL.instances()
	
#====================================================================
def ModulePort():
	"""
	Return module signal list.
	"""
	
	#-Signals------------------------------------------
	reset    = HDL.Signal(HDL.intbv(0)[1:])
	clock    = HDL.Signal(HDL.intbv(0)[1:])
	data_in  = HDL.Signal(HDL.intbv(0)[FlitWidth*NbPort:])
	rx       = HDL.Signal(HDL.intbv(0)[NbPort:])
	ack_rx   = HDL.Signal(HDL.intbv(0)[NbPort:])
	data_out = HDL.Signal(HDL.intbv(0)[FlitWidth*NbPort:])
	tx       = HDL.Signal(HDL.intbv(0)[NbPort:])
	ack_tx   = HDL.Signal(HDL.intbv(0)[NbPort:])
	
	return [reset, clock, data_in, rx, ack_rx, data_out, tx, ack_tx]
	
#====================================================================
def TB():
	"""
	Send/receive value to/from Switch module.
	Return myhdl simulation object. 
	"""
	#-Top Module---------------------------------------
	Port      = ModulePort()
	TestedModule = Module(*Port)
	reset, clock, data_in, rx, ack_rx, data_out, tx, ack_tx = Port
	
	
	#-ClockGenerator-----------------------------------
	HalfPeriod=HDL.delay(50)
	@HDL.always(HalfPeriod)
	def ClkDriver():
		clock.next = not clock
		
	#-StimuliGenerator---------------------------------
	@HDL.instance
	def Stimuli():
		#-INIT-------------------------------------
		reset.next = 1
		yield HDL.delay(10)
		yield clock.negedge
		reset.next = 0
		#-START------------------------------------
		while True:
			for Sig in [data_in, rx, ack_tx]: 
				Sig.next = RDM.randrange(0, len(Sig))
				
			yield HDL.delay(20)
		raise HDL.StopSimulation
	
	#-Monitor------------------------------------------
	@HDL.instance
	def monitor():
		print("Start of '{0}' simulation.".format(ModuleName))
		yield reset.posedge
		while True:
			yield clock.posedge
			print("Step[{0}]".format(HDL.now())+"-"*50)
			print("SIGNALS: ", end=' ')
			for Sig in Port:
				print("{0} ".format(Sig), end=' ')
			print("")
			yield HDL.delay(20)
		
	HDL.traceSignals.name = ModuleName
	return HDL.instances()
	
	
#====================================================================
def GenHDL():
	# Convert to VHDL-----------------------------------
	HDL.toVHDL.no_myhdl_header = True
	HDL.toVHDL.header          = Header(Lang="VHDL")
	HDL.toVHDL.name            = ModuleName
	inc_inst = HDL.toVHDL(Module, *ModulePort())

	# Convert to Verilog-----------------------------------
	HDL.toVerilog.no_myhdl_header = True
	HDL.toVerilog.header          = Header(Lang="Verilog")
	HDL.toVerilog.name            = ModuleName
	HDL.toVerilog(Module, *ModulePort())
	

#======================================================================
def Header(Lang):
	"""
	Return string commented header.
	"""
	if Lang=="VHDL":      Token = '--'	
	elif Lang=="Verilog": Token = '//'

	Head = Token*50+"""
{TOK} Actual File Name      = {MODULE}.vhd
{TOK} Title & purpose       = {TITLE} - {PURPOSE}
{TOK} Author                = Automaticaly generated by YANGO - made by Matthieu PAYET (ADACSYS)
{TOK} Creation Date         = {TIME}
{TOK} Version               = 0.1
{TOK} Simple Description    = {DESCRIPTION}
{TOK} Specific issues       = {ISSUES}
{TOK} Speed                 = {SPEED}
{TOK} Area estimates        = {AREA}
{TOK} Tools (version)       = {TOOL}
{TOK} HDL standard followed = VHDL 2001 standard
{TOK} Revisions & ECOs      = {REV}
"""+Token*50
	return Head.format(
			TOK = Token,
			MODULE=ModuleName, 
			TITLE=Title, 
			PURPOSE=Purpose, 
			DESCRIPTION=Desc, 
			ISSUES=Issues, 
			SPEED=Speed, 
			AREA=Area, 
			TOOL=Tool, 
			REV=Rev,
			TIME=datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
	
######################################################################################
if __name__ == '__main__':
	
	HDL.Simulation(HDL.traceSignals(TB)).run(500)
	
	# Display waveforms
	subprocess.call(["gtkwave", "TB.vcd"])
	
	Ret = "?"
	while(Ret!='O' and Ret!='n'): Ret = input("Generate VHDL and Verilog files (O/n) ? ")
	if Ret=='O': GenHDL()
	
	


