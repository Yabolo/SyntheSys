
import myhdl as HDL
import random as RDM
import datetime	
	
import HERMES_Router

#====================================================================
# HEADER PARAMETERS
#====================================================================
ModuleName = "HERMES_NoC"
Title      = "HERMES on chip network"
Purpose    = "Make it easy for designers to link IP each others." 
Desc       = "?"
Issues     = ""
Speed      = ""
Area       = ""
Tool       = "Xilinx ISE (13.1)"
Rev        = "0.12"
#====================================================================
# MODULE PARAMETERS
#====================================================================

NbPort      = 6
FlitWidth   = 8
BufferDepth = 8
Dimensions  = "2x2"
# Get integers for xDim and yDim
xDim, yDim = map(int, Dimensions.lower().split('x'))
 # Dictionary of connections
ConnectDict={
	"BL":[0,2,4], 
	"BC":[0,1,2,4], 
	"BR":[1,2,4], 
	"CL":[0,2,3,4], 
	"CC":[0,1,2,3,4], 
	"CR":[1,2,3,4], 
	"TL":[0,3,4], 
	"TC":[0,1,3,4], 
	"TR":[1,3,4]}


HERMES_Router.NbPort      = NbPort
HERMES_Router.FlitWidth   = FlitWidth
HERMES_Router.BufferDepth = BufferDepth

#====================================================================
def Module(clock, reset, data_in, rx, ack_rx, data_out, tx, ack_tx):
	"""
	Connect inputs to specified outputs according to Cmd.
	Cmd is a vector of output length of input index.
	Inputs and outputs are arrays of bit vector.
	"""
	#-------------------------------------------------
	
	#-------------------------------------------------
	RMatrix=[]
	for y in range(yDim): RMatrix.append([None in range(xDim)])
	# Generate the list of routers
	AddressTemplate = "{0:0"+str(FlitWidth/4)+"b}{1:0"+str(FlitWidth/4)+"b}"
	for y in range(0, yDim):
		for x in range(0, xDim):
			# Get router Type
			if x==0:
				if y==0:          HERMES_Router.RouterType="BL" 
				elif y==(yDim-1): HERMES_Router.RouterType="TL"
				else:             HERMES_Router.RouterType="CL"
			elif x==(xDim-1):
				if y==0:          HERMES_Router.RouterType="BR"
				elif y==(yDim-1): HERMES_Router.RouterType="TR"
				else:             HERMES_Router.RouterType="CR"
			else:
				if y==0:          HERMES_Router.RouterType="BC"
				elif y==(yDim-1): HERMES_Router.RouterType="TC"
				else:             HERMES_Router.RouterType="CC"
			# Router signals
			Rst, Clk, data_in, rx, ack_rx, data_out, tx, ack_tx = HERMES_Router.ModulePort()
			# LOCAL Address
			HERMES_Router.Address = int(AddressTemplate.format(x, y), 2)
			# Instantiate router module (into the matrix)
			RMatrix[y][x]=[HERMES_Router.RouterType, HERMES_Router.Module(reset, clock, data_in, rx, ack_rx, data_out, tx, ack_tx), [data_in, rx, ack_rx, data_out, tx, ack_tx]]
			
	Links+=Connections(RMatrix)
			
	return reduce(lambda a, b: a+b[1], RMatrix) + Links
	
#====================================================================
def Connections(RMatrix, Locals):
	"""
	Connect unused pin to ground.
	"""
	Links=[]
	for y in range(yDim):
		for x in range(xDim):
			for P in range(NbPort):
				if ConnectDict[RMatrix[y][x][0]].count(P):
					if P==0: Links+=Connect(P,RMatrix[y][x][2],RMatrix[y][x+1][2])
					elif P==1: Links+=Connect(P,RMatrix[y][x][2],RMatrix[y+1][x][2])
					elif P==2: Links+=Connect(P,RMatrix[y][x][2],RMatrix[y][x-1][2])
					elif P==3: Links+=Connect(P,RMatrix[y][x][2],RMatrix[y-1][x][2])
					elif P==4: Links+=Connect(P,RMatrix[y][x][2],Locals)
				else: Links+=Connect(P, RMatrix[y][x][2], None)
	
	return Links
	
	
#====================================================================
def Connect(Port, SigList0, SigList1):
	"""
	Port connection between routers.
	"""
	data_in0, rx0, ack_rx0, data_out0, tx0, ack_tx0 = SigList0
	
	if SigList1==None:
		data_in1, rx1, ack_rx1, data_out1, tx1, ack_tx1 = SigList1
		@HDL.always_comb
		def RouterLink():
			data_in0[Port].next  = 
			rx0[Port].next       = 
			ack_rx0[Port].next   =  
			data_out0[Port].next = 
			tx0[Port].next       = 
			ack_tx0[Port].next   = 
	
		return [RouterLink,]
	else:
		@HDL.always_comb
		def ZeroLink():
			data_in0[Port].next  = 0
			rx0[Port].next       = 0
			ack_rx0[Port].next   = 
			data_out0[Port].next = 
			tx0[Port].next       = 
			ack_tx0[Port].next   = 
		
		return [ZeroLink,]

#====================================================================
def ModulePort():
	"""
	Return module signal list.
	"""
	
	#-Signals------------------------------------------
	reset    = HDL.Signal(HDL.intbv(0)[1:])
	clock    = HDL.Signal(HDL.intbv(0)[1:])
	data_in  = [HDL.Signal(HDL.intbv(0)[FlitWidth:]) for i in range(0, NbPort)]
	rx       = HDL.Signal(HDL.intbv(0)[NbPort:])
	ack_rx   = HDL.Signal(HDL.intbv(0)[NbPort:])
	data_out = [HDL.Signal(HDL.intbv(0)[FlitWidth:]) for i in range(0, NbPort)]
	tx       = HDL.Signal(HDL.intbv(0)[NbPort:])
	ack_tx   = HDL.Signal(HDL.intbv(0)[NbPort:])
	
	return [reset, clock, data_in, rx, ack_rx, data_out, tx, ack_tx]
	
#====================================================================
def ModuleStimuli():
	"""
	Return module signal list.
	"""
	#-Signals------------------------------------------
	DataIn  = [HDL.Signal(HDL.intbv(0)[FlitWidth:]) for i in range(NbPort)]
	Rx      = HDL.Signal(HDL.intbv(0)[NbPort:])
	AckTx   = HDL.Signal(HDL.intbv(0)[NbPort:])
	
	return [DataIn, Rx, AckTx]
	
#====================================================================
def TB():
	"""
	Send/receive value to/from Switch module.
	Return myhdl simulation object. 
	"""
	#-Top Module---------------------------------------
	Port         = ModulePort()
	TestedModule = Module(*Port)
	reset        = Port[0]
	clock        = Port[1]
	
	#-ClockGenerator-----------------------------------
	HalfPeriod=HDL.delay(50)
	@HDL.always(HalfPeriod)
	def ClkDriver():
		clock.next = not clock
		
	#-StimuliGenerator---------------------------------
	@HDL.instance
	def Stimuli():
		#-INIT-------------------------------------
		reset.next = 1
		yield HDL.delay(10)
		yield clock.negedge
		reset.next = 0
		#-START------------------------------------
		while True:
			for Sig in ModuleStimuli(): 
				if isinstance(Sig, list): 
					for i in range(0, len(Sig)):
						Sig[i].next = RDM.randrange(0, len(Sig[i]))
				else: Sig.next = RDM.randrange(0, max(Sig.max, 2))
				
			yield HDL.delay(20)
		raise HDL.StopSimulation
	
	#-Monitor------------------------------------------
	@HDL.instance
	def monitor():
		clock, reset, DataIn, Rx, AckRx, DataOut, Tx, AckTx = ModulePort()
		print "Start of '{0}' simulation.".format(ModuleName)
		yield reset.posedge
		while True:
			yield clock.posedge
			print "Step[{0}]".format(HDL.now())+"-"*50
			print "INPUTS: h={0} data={1} sender={2} address={3}".format(h, data, sender, address)
			print "OUTPUTS: ack_h={0} free={1} mux_in={2} mux_out={3}".format(ack_h, free, mux_in, mux_out)
			yield HDL.delay(20)
		
	HDL.traceSignals.name = ModuleName
	return HDL.instances()
	
	
#====================================================================
def GenHDL():
	# Convert to VHDL-----------------------------------
	HDL.toVHDL.no_myhdl_header = True
	HDL.toVHDL.header          = Header(Lang="VHDL")
	HDL.toVHDL.name            = ModuleName
	inc_inst = HDL.toVHDL(Module, *ModulePort())

	# Convert to Verilog-----------------------------------
	HDL.toVerilog.no_myhdl_header = True
	HDL.toVerilog.header          = Header(Lang="Verilog")
	HDL.toVerilog.name            = ModuleName
	HDL.toVerilog(Module, *ModulePort())
	

#======================================================================
def Header(Lang):
	"""
	Return string commented header.
	"""
	if Lang=="VHDL":      Token = '--'	
	elif Lang=="Verilog": Token = '//'

	Head = Token*50+"""
{TOK} Actual File Name      = {MODULE}.vhd
{TOK} Title & purpose       = {TITLE} - {PURPOSE}
{TOK} Author                = Automaticaly generated by YANGO - made by Matthieu PAYET (ADACSYS)
{TOK} Creation Date         = {TIME}
{TOK} Version               = 0.1
{TOK} Simple Description    = {DESCRIPTION}
{TOK} Specific issues       = {ISSUES}
{TOK} Speed                 = {SPEED}
{TOK} Area estimates        = {AREA}
{TOK} Tools (version)       = {TOOL}
{TOK} HDL standard followed = VHDL 2001 standard
{TOK} Revisions & ECOs      = {REV}
"""+Token*50
	return Head.format(
			TOK = Token,
			MODULE=ModuleName, 
			TITLE=Title, 
			PURPOSE=Purpose, 
			DESCRIPTION=Desc, 
			ISSUES=Issues, 
			SPEED=Speed, 
			AREA=Area, 
			TOOL=Tool, 
			REV=Rev,
			TIME=datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
	
######################################################################################
if __name__ == '__main__':
	
	HDL.Simulation(HDL.traceSignals(TB)).run(500)
	
	GenHDL()
	
	# Display waveforms
	subprocess.call(["gtkwave", "TB.vcd"])
	


