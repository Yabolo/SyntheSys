
import myhdl as HDL
import random as RDM
	
def ModuleBuilder(NbIn, NbOut):
	"""
	Builder for Arbiter.
	"""
	# Parameters
	#====================================================================
	def Module(Rst_n, Clk, SOPList, EOPList, InputTarget, SWCmd):
		"""
		Schedule input connection order with a FSM.
		SWCmd is a vector with length of output port number of inputs index.
		InputTarget is a vector with length of input port number of outputs index.
		SOPList, EOPList are bit vector (one bit per input).
		"""
		def Arbiter():
			InputTarget=[HDL.Signal(HDL.intbv(0, min=0, max=NbOut-1)) for i in range(0,len(NbIn))]
			OutTab=[HDL.Signal(HDL.intbv(0, min=0, max=NbIn-1)) for i in range(0,len(NbOut))]
			InTab=[HDL.Signal(HDL.intbv(0, min=0, max=NbOut-1)) for i in range(0,len(NbIn))]
			FreeTable=HDL.Signal(HDL.intbv(0, min=0, max=NbOut-1))
			ConflictTable=HDL.Signal(HDL.intbv(0, min=0, max=NbOut-1))
			
			SWCmd=OutTab
			#-RoutingTable-------------------
			@HDL.always_comb
			def RoutingTable():
				for InPort in range(0, NbIn):
					if SOPList[InPort]==1: # Connection request
						# If OutPort free and no conflict => Connect
						if FreeTable[InputTarget[InPort]]==1:
							OutTab[InputTarget[InPort]].next=InPort
							InTab[InPort].next=InputTarget[InPort]
								
						else: # do not connect: keep state.
							OutTab[OutPort].next=OutTab[OutPort]
							InTab[InPort].next=InputTarget[InPort]
		
			#-FreeTableSetup-------------------
			@HDL.always_comb
			def FreeTableSetup():
				for InPort in range(0, NbIn): 	
					if EOPList[InPort] != 0: # Close connection request
						SWCmd[InputTarget[InPort]]
						FreeTable[OutPort]=0
					else:
						FreeTable[OutPort]=1
				
			return RoutingTable, FreeTableSetup, ConflictDetection
			
		return Arbiter
	return Module

#====================================================================
def TB(Duration):
	"""
	Send/receive value to/from Arbiter module.
	"""
	ModuleName="Arbiter"
		
	Rst = HDL.Signal(HDL.intbv(0))
	HalfPeriod = HDL.delay(5)
	Clk = HDL.Signal(HDL.intbv(0))
	DataWidth = 32
	NbInputs  = 5
	NbOutputs = 5
	Inputs  = [HDL.Signal(HDL.intbv(0, min=0, max=DataWidth)) for k in range(0, NbInputs)]
	Outputs = [HDL.Signal(HDL.intbv(0, min=0, max=DataWidth)) for k in range(0, NbOutputs)]
	Cmd = HDL.Signal(HDL.intbv(0, min=0, max=NbOutputs))
	
	#-ClockGenerator-------------------
	@HDL.always(HalfPeriod)
	def ClkDriver():
		Clk.next = not Clk
		
	#-StimuliGenerator-----------------
	@HDL.instance
	def Stimuli():
		#-INIT---------------------
		Rst.next = 1
		yield Clk.negedge
		Rst.next = 0
		#-START--------------------
		for i in range(Duration):
			for Index in range(0, len(Inputs)):
				Inputs[Index].next = RDM.randrange(len(Inputs))
			yield Clk.negedge
		raise HDL.StopSimulation
		
	#-Monitor--------------------------
	@HDL.instance
	def monitor():
		yield Rst.posedge
		while True:
			yield Clk.posedge
			yield HDL.delay(1)
			print("[{0}]: Inputs={1}, Outputs={2}".format(HDL.now(), Inputs, Outputs))
			
	#-LaunchSimulation-----------------
	TestedModule = Arbiter(Cmd, Inputs, Outputs)
	
	print("Start of '{0}' simulation.".format(ModuleName))
	Simu = HDL.Simulation(HDL.instances())
	Simu.run(Duration)

	return True
	
######################################################################################
if __name__ == '__main__':

	Arbiter_tb(50)


