#! /usr/bin/python

import os, sys, re, logging
import math

sys.path.append(os.path.abspath("./HDL"))
import HDLEditor as HDL

from distutils.file_util import copy_file as CopyFile

ExtensionDict = HDL.ExtensionDict

#==================================================================
# NoC parameters avalability
#==================================================================
def AvailableValues(Parameter):
	"""
	Return a list of available values for this parameter.
	"""
	AvailableList=[]
	if Parameter == "Type": AvailableList = ["HERMES",]
	#elif Parameter == "Dimensions": 
	#	for x in range(0,100): 
	#		for y in range(0,100): 
	#			AvailableList.append("{0}x{1}".format(x,y))
	elif Parameter == "FlitWidth":  AvailableList = ["8", "16", "32", "64"]
	elif Parameter == "Buffers":    AvailableList = ["4", "8", "16", "32"]
	elif Parameter == "Lanes":      AvailableList = ["1", "2", "4"]
	elif Parameter == "Routing":    AvailableList = ["XY",]
	elif Parameter == "Scheduling": AvailableList = ["RoundRobin", "Priority",]
	
	return {Parameter:AvailableList}
	
#==================================================================
# NoC packages
#==================================================================
def GetRouterNI(Type, FluxCtrl, VC, NbPorts):
	"""
	Return a dictionary of signals for specified NoC port depending on specified flux control. 
	"""
	SigDict={"InFlux":[],"OutFlux":[],}
	if Type=="HERMES":
		if FluxCtrl=="HandShake": 
			SigDict["Clocks"]=[
				["clock", "IN", 1, "std_logic"],
				]
			SigDict["Resets"]=[
				["reset", "IN", 1, "std_logic"],
				]
			SigDict["InFlux"]=[
				["data_in", "IN", NbPorts, "arrayNport_regflit"],
				["rx", "IN", NbPorts, "regNport"],
				["ack_rx", "OUT", NbPorts, "regNport"],
				]
			SigDict["OutFlux"]=[
				["data_out", "OUT", NbPorts, "arrayNport_regflit"],
				["tx", "OUT", NbPorts, "regNport"],
				["ack_tx", "IN", NbPorts, "regNport"],
				]
			SigDict["NetworkInterface"]=SigDict["InFlux"]+SigDict["OutFlux"]
			
		elif  FluxCtrl=="CreditBased":
			SigDict["Clocks"]=[
				["clock", "IN", 1, "std_logic"],
				]
			SigDict["Resets"]=[
				["reset", "IN", 1, "std_logic"],
				]
			SigDict["InFlux"]=[
				["clock_rx", "IN", NbPorts, "regNport"],
				["rx", "IN", NbPorts, "regNport"],
				["data_in", "IN", NbPorts, "arrayNport_regflit"],
				]
			if VC>1:
				SigDict["OutFlux"].append(["credit_i","IN",NbPorts,"arrayNport_regNlane"])
				SigDict["OutFlux"].append(["lane_tx","IN",NbPorts,"arrayNport_regNlane"])
			else:
				SigDict["OutFlux"].append(["credit_i", "IN", NbPorts, "regNport"])
				
			SigDict["OutFlux"]=[
				["clock_tx", "IN", NbPorts, "regNport"],
				["tx", "IN", NbPorts, "regNport"],
				["data_out", "IN", NbPorts, "arrayNport_regflit"], 
				]
				
			if VC>1:
				SigDict["InFlux"].append(["credit_o","OUT",NbPorts,"arrayNport_regNlane"])
				SigDict["InFlux"].append(["lane_rx","IN",NbPorts,"arrayNport_regNlane"])
			else:
				SigDict["InFlux"].append(["credit_o", "OUT", NbPorts, "regNport"])
				
			SigDict["NetworkInterface"]=SigDict["InFlux"]+SigDict["OutFlux"]
	return SigDict
	
#==================================================================
# NoC packages
#==================================================================
def GetPackages(Type="HERMES"):
	"""
	Return a list of needed package for specified NoC. 
	"""
	if Type=="HERMES": return ["IEEE.std_logic_1164.all", "IEEE.std_logic_unsigned.all", "work.HermesPackage.all"]
	else: return []
	
#==================================================================
# NoC libraries
#==================================================================
def GetLibraries(Type="HERMES"):
	"""
	Return a list of needed package for specified NoC. 
	"""
	if Type=="HERMES": return ["IEEE",]
	else: return []

#==================================================================
# Return NoC IOs
#==================================================================
def GetIO(Type="HERMES", FlitWidth=16, FluxCtrl="HandShake", Dimensions="4x4", VC=1):
	NbRouters = GetNbRouters(Dimensions)
	SigDict={"Resets":[], "Clocks":[], "NetworkInterface":[], "Ports": [], "InFlux":[],"OutFlux":[],}
	if Type=="HERMES":	
		if FluxCtrl=="HandShake":
			SigDict["Clocks"] = [	
				["Clock", "in", 1, "std_logic"],
				]
			SigDict["Resets"] = [
				["Reset", "in", 1, "std_logic"],
				]
			SigDict["InFlux"]  = [
				["DataInLocal", "in", NbRouters, "arrayNrot_regflit"],
				["RxLocal", "in", NbRouters, "regNrot"],
				["AckRxLocal", "out", NbRouters, "regNrot"],
				]
			SigDict["OutFlux"]  = [
				["DataOutLocal", "out", NbRouters, "arrayNrot_regflit"],
				["TxLocal", "out", NbRouters, "regNrot"],
				["AckTxLocal", "in", NbRouters, "regNrot"],
				]
			SigDict["NetworkInterface"]  = SigDict["InFlux"]+SigDict["OutFlux"]
			SigDict["Ports"] = [
				]
		elif FluxCtrl=="CreditBased":
			SigDict["Clocks"] = [	
				["Clock", "in", NbRouters, "regNrot"],
				]
			SigDict["Resets"] = [
				["Reset", "in", 1, "std_logic"],
				]
			SigDict["InFlux"]  = [
				["ClockRxLocal", "in", NbRouters, "regNrot"],
				["RxLocal", "in", NbRouters, "regNrot"],
				["DataInLocal", "in", NbRouters, "arrayNrot_regflit"],
				["CreditOutLocal", "out", NbRouters, "arrayNrot_regNlane"],
				]
			SigDict["OutFlux"]  = [
				["ClockTxLocal", "out", NbRouters, "regNrot"],
				["TxLocal", "out", NbRouters, "regNrot"],
				["DataOutLocal", "out", NbRouters, "arrayNrot_regflit"],
				["CreditInLocal", "in", NbRouters, "arrayNrot_regNlane"],
				]
			SigDict["Ports"] = [
				]
			if VC>1:
				SigDict["InFlux"]+=[
					["LaneRxLocal", "in", NbRouters, "arrayNrot_regNlane"],
					]
				SigDict["OutFlux"]+=[
					["LaneTxLocal", "in", NbRouters, "arrayNrot_regNlane"],
					]
			SigDict["NetworkInterface"]  = SigDict["InFlux"]+SigDict["OutFlux"] 

	return SigDict

#==================================================================
# Return the number of router given dimensions
#==================================================================
def GetNbRouters(Dimensions):
	"""
	Return the number of router given dimensions.
	"""
	# Get integers for xDim and yDim
	xDim, yDim = list(map(int, Dimensions.lower().split('x')))
	return int(xDim*yDim)

#==================================================================
# NoC HDL File generation
#==================================================================
def GenHDL(Type="HERMES", Config="Mesh", Dimensions="3x3", FlitWidth=16, BufferDepth=32, FluxCtrl="HandShake", VC=1, Routing="XY", Scheduling="Priority", Language="VHDL", OutputPath="./NoC", LibPath="./"):
	"""
	Generate HDL files (VHDL or Verilog) for a NoC of specified type and for specified parameters.
		Type         = "HERMES"
		Config       = "MESH" or "MESH_SR"(Source routing) or "MESH_CRC" or
		               "TORUS_TU"(Torus unidirectionnal) or "TORUS_TB"(Torus bidirectionnal),
		Dimension    = "NxM" (where N and M are x dimension and y Dimension respectively)
		FlitWidth    = Size of flit in bits
		BufferDepth  = Number of registers for input buffer (per channel)
		FluxCtrl     = "HandShake" or "CreditBased"
		VC           = Number of virtual channels (natural)
		Language     = "VHDL" or "Verilog"
		OutputPath   = Output directory path
	"""
	SourceList=[]
	# Check arguments-----------------------------------------------------------
	# Define sources subdirectory
	if not os.path.isdir(LibPath):
		logging.error("Library Directory '{0}' does not exist: NoC generation aborted.".format(LibPath))
	
	if VC>1:
		if FluxCtrl=="HandShake": 
			logging.error("Handshake control flux do not support virtual channel.")
			return
		SrcFolder=os.path.join(LibPath, Type.upper(),"VirtualChannel", Scheduling) # Only Credit Based flux control
	else: SrcFolder=os.path.join(LibPath, "IP", "Management", "NOC", Type.upper(), FluxCtrl) # CreditBased or HandShake
	
	if not os.path.isdir(SrcFolder):
		logging.error("Library Directory '{0}' does not exist: NoC generation aborted.".format(SrcFolder))
	# Create the subdirectory for the NoC
	if not os.path.isdir(OutputPath):# First create tree if it doesn't already exist'
		try: os.makedirs(OutputPath)# Recreate the whole directory tree
		except: 
			logging.error("Directory '{0}' cannot be created: NoC generation aborted.".format(OutputPath))
			return SourceList # Failure
		logging.info("Directory '{0}' did not already exist : successfully created.".format(OutputPath))
	if not ExtensionDict[Language]:
		logging.error("Unrecognized HDL language '{0}': NoC Generation aborted.".format(Language))
		return SourceList # Failure
	# Check library-------------------------------------------------------------
	#---------------------------------------------------------------------------
	
	# Create the package file------------------------------------------------------
	Pack=CommonPackage(SrcFolder, OutputPath, Type, Dimensions, FlitWidth, BufferDepth, VC, "VHDL")
	SourceList.append(Pack)
		
	# Copy common source files
	SourceList+=SearchAndCopy(SrcFolder, OutputPath, ["buffer",]) # Avoid hidden files
	
	# Copy CB source files
	if FluxCtrl=="CreditBased":
		SourceList+=SearchAndCopy(SrcFolder, OutputPath, ["crossbar",])
		
	
	# Create the router file for each router type----------------------------------
	RouterList = Routers(Dimensions, FlitWidth)
	RouterTypes = list(set([x[-1] for x in RouterList]))# list of types with removed duplicates
	PortDict={0:"EAST", 1:"WEST", 2:"NORTH", 3:"SOUTH", 4:"LOCAL"}
	ConnectDict={"BL":[0,2,4], "BC":[0,1,2,4], "BR":[1,2,4], "CL":[0,2,3,4], "CC":[0,1,2,3,4], "CR":[1,2,3,4], "TL":[0,3,4], "TC":[0,1,3,4], "TR":[1,3,4]} # Dictionnaire des connections
	for RType in RouterTypes:
		# Instanciate FIFOs in routers
		CodeFIFOs=""
		CodeNullConnection=""
		for PortNum in range(0,5):
		 	# Router generics :
			GenericDict={}
			# Instances list :
			#-------------------------------------------------------------------------
			if FluxCtrl=="HandShake": 
				if ConnectDict[RType].count(PortNum):
				 	# FIFOs signals :
					SignalDict={
						"clock"   : "clock",
						"reset"   : "reset",
						"data_in" : "data_in({0})".format(PortNum),
						"rx"      : "rx({0})".format(PortNum),
						"ack_rx"  : "ack_rx({0})".format(PortNum),
						"h"       : "h({0})".format(PortNum),
						"ack_h"   : "ack_h({0})".format(PortNum),
						"data_av" : "data_av({0})".format(PortNum),
						"data"    : "data({0})".format(PortNum),
						"data_ack": "data_ack({0})".format(PortNum),
						"sender"  : "sender({0})".format(PortNum),
						}
					# Instancie le 'filas' ou FIFO du port
					CodeFIFOs+=HDL.Instanciate("F_"+PortDict[PortNum], "Fila", None, SignalDict, GenericDict, "")
				else:
					# Code for non connected FIFOs
					CodeNullConnection = HDL.Indent(
						HDL.Comment("Drive input buffer signals to ground.")+
						HDL.Connect("h({0})".format(PortNum), None, Size=1)+
						HDL.Connect("data_av({0})".format(PortNum), None, Size=1)+
						HDL.Connect("data({0})".format(PortNum), None, Size=5)+
						HDL.Connect("sender({0})".format(PortNum), None, Size=1))
				Dictionary={
					"zeros": CodeNullConnection,
					}
			#-------------------------------------------------------------------------
			elif FluxCtrl=="CreditBased": # Instancie le 'hermes_buffer' pour ce port
				if ConnectDict[RType].count(PortNum):
				 	# Buffers signals :
					SignalDict={
						"clock"   : "clock",
						"reset"   : "reset",
						"data_in" : "data_in({0})".format(PortNum),
						"rx"      : "rx({0})".format(PortNum),
						"h"       : "h({0})".format(PortNum),
						"ack_h"   : "ack_h({0})".format(PortNum),
						"data_av" : "data_av({0})".format(PortNum),
						"data"    : "data({0})".format(PortNum),
						"sender"  : "sender({0})".format(PortNum),
						"clock_rx": "clock_rx({0})".format(PortNum),
						"data_ack": "data_ack({0})".format(PortNum),
						"credit_o": "credit_o({0})".format(PortNum),
						}
					Dictionary={
						"inports":  HDL.Indent( RoutersInport(RType)  ),
						"outports": HDL.Indent( RoutersOutport(RType) ),
						}
					CodeFIFOs+= HDL.Instanciate("F_"+PortDict[PortNum], "Hermes_buffer", None, SignalDict, GenericDict, "")
				else:
				 	# Buffers output signals :
					SignalList=[
						"h({0})".format(PortNum), 
						"data_av({0})".format(PortNum), 
						"sender({0})".format(PortNum), 
						"credit_o({0})".format(PortNum),]
					for Sig in SignalList:
						CodeFIFOs+= HDL.Connect(Sig, None, Size=1)
					CodeFIFOs+= HDL.Connect("data({0})".format(PortNum), None, Size=5) # Warning: Size to adapt if not null
			#-------------------------------------------------------------------------
			else:
				logging.error("Flux control '{0}' not supported: NoC HDL generation aborted").format(FluxCtrl)
				return
		#-------------------------------------------------------------------------
		# General tags (flux control independent)
		Dictionary.update({
			"Chave": "Router"+RType,
			"algorithm": "algorithm"+Routing,
			"filas": CodeFIFOs,
			})
		TemplateEdit(
			os.path.join(SrcFolder, "Hermes_router"+ExtensionDict[Language]), 
			os.path.join(OutputPath, "Router"+RType+ExtensionDict[Language]), 
			Dictionary
			)
		SourceList.append(os.path.join(OutputPath, "Router"+RType+ExtensionDict[Language]))
				
	# Create the virtual channels inport file ----------------------------------------
	if VC>1:
		Dictionary={
			"channelsLocal": HDL.Indent( LocalVC(int(VC))  ),
			"rxSignals":     HDL.Indent( RxVC(int(VC))     ),
			"rxChannels":    HDL.Indent( RxTestVC(int(VC)) ),
			"bufChannels":   HDL.Indent( InstBufVC(int(VC))),
			}
		TemplateEdit(
			os.path.join(SrcFolder, "Hermes_inport"+ExtensionDict[Language]), 
			os.path.join(OutputPath, "Hermes_inport"+ExtensionDict[Language]), 
			Dictionary
			)
		SourceList.append(os.path.join(OutputPath, "Hermes_inport"+ExtensionDict[Language]))
			
		
	# Create the switch control file -------------------------------------------
	
		
		
	Dictionary={
		"state": HDL.Indent( HDL.Type("state",[("S"+str(i)) for i in range(0,VC)]) ),
		"ask":   HDL.Indent( HDL.Connect("ask", AffectAsk(int(VC))) ),		
		"localSel": HDL.Indent( AffectProx(VC, "LOCAL"), 4),
		"eastSel":  HDL.Indent( AffectProx(VC, "EAST"),  4),
		"westSel":  HDL.Indent( AffectProx(VC, "WEST"),  4),
		"northSel": HDL.Indent( AffectProx(VC, "NORTH"), 4),
		"southSel": HDL.Indent( AffectProx(VC, "SOUTH"), 4),
		"statesConditions": HDL.Indent( StateCondition(VC, Routing), 4),
		"statesFinal":      HDL.Indent(FinalStateCondition(VC, Routing) , 3) ,
		"state_s2":         HDL.Indent(S2StateCondition(VC), 4),
		"statesImplements": HDL.Indent(StatesImplements(VC, Routing), 3),
		"sender_ant":       HDL.Indent(SenderAnt(VC), 2),
		"senderConditions": HDL.Indent(SenderConditions(VC), 2),
		}
	TemplateEdit(
		os.path.join(SrcFolder, "Hermes_switchcontrol"+ExtensionDict[Language]), 
		os.path.join(OutputPath, "Hermes_switchcontrol"+ExtensionDict[Language]), 
		Dictionary
		)
	SourceList.append(os.path.join(OutputPath, "Hermes_switchcontrol"+ExtensionDict[Language]))
				
	# Create the virtual channels outport file ----------------------------------------
	if VC>1:
		Dictionary={
			"signal": HDL.Signals(["c{0}".format(i) for i in range(0,VC)], "std_logic", 0)+';',
			"cs_local": HDL.Indent( CTest(VC,"LOCAL") ,3),
			"cs_east":  HDL.Indent( CTest(VC,"EAST")  ,3),
			"cs_west":  HDL.Indent( CTest(VC,"WEST")  ,3),
			"cs_north": HDL.Indent( CTest(VC,"NORTH") ,3),
			"cs_south": HDL.Indent( CTest(VC,"SOUTH") ,3),
			"tx":       HDL.Indent( TxTest(VC) )+';',
			"data_out_local": HDL.Indent( DataOutTest(VC, "EAST") ),
			"data_out_east":  HDL.Indent( DataOutTest(VC, "EAST") ),
			"data_out_west":  HDL.Indent( DataOutTest(VC, "WEST") ),
			"data_out_north": HDL.Indent( DataOutTest(VC, "NORTH")),
			"data_out_south": HDL.Indent( DataOutTest(VC, "SOUTH")),
			"data_ack_local": HDL.Indent( DataAckTest(VC, "LOCAL")),
			"data_ack_east":  HDL.Indent( DataAckTest(VC, "EAST")),
			"data_ack_west":  HDL.Indent( DataAckTest(VC, "WEST")),
			"data_ack_north": HDL.Indent( DataAckTest(VC, "NORTH")),
			"data_ack_south": HDL.Indent( DataAckTest(VC, "SOUTH")),
			"process":        HDL.Indent( DataOutProcess(VC) ,2),
			}
		TemplateEdit(
			os.path.join(SrcFolder, "Hermes_outport"+ExtensionDict[Language]), 
			os.path.join(OutputPath, "Hermes_outport"+ExtensionDict[Language]), 
			Dictionary
			)
		SourceList.append(os.path.join(OutputPath, "Hermes_outport"+ExtensionDict[Language]))
	
	# Create the NoC Top -------------------------------------------------------
	NoCFileName = os.path.join(OutputPath, "NoC"+ExtensionDict[Language])
	with open(NoCFileName, "w+") as HDLFile:
		HDLFile.write(GenerateTop(Type, FluxCtrl, Dimensions, FlitWidth, VC, "VHDL"))
	SourceList.append(NoCFileName)
	
	return SourceList # Success
	
#==================================================================
# Return the input port map for given router and port.
#==================================================================
def RoutersInport(RouterType):
	"""
	Return the input port map for each router and port.
	"""
	PortDict={"EAST":0, "WEST":1, "NORTH":2, "SOUTH":3, "LOCAL":4}
	ConnectDict={"BL":[0,2], "BC":[0,1,2], "BR":[1,2], "CL":[0,2,3], "CC":[0,1,2,3], "CR":[1,2,3], "TL":[0,3], "TC":[0,1,3], "TR":[1,3]} # Dictionnaire des connections
	Code=""
	for Port in ["EAST", "WEST", "NORTH", "SOUTH", "LOCAL",]:
		if ConnectDict[RouterType].count(PortDict[Port]):
			SignalDict={
				"clock":	"clock",
				"reset":	"reset",
				"clock_rx":	"clock_rx({0})".format(PortDict[Port]),
				"rx":		"rx({0})".format(PortDict[Port]),
				"lane_rx":	"lane_rx({0})".format(PortDict[Port]),
				"data_in":	"data_in({0})".format(PortDict[Port]),
				"credit_o":	"credit_o({0})".format(PortDict[Port]),
				"h":		"h({0})".format(PortDict[Port]), 
				"ack_h":	"ack_h({0})".format(PortDict[Port]),
				"data_av":	"data_av({0})".format(PortDict[Port]), 
				"data":		"data({0})".format(PortDict[Port]), 
				"data_ack":	"data_ack({0})".format(PortDict[Port]),
				"sender":	"sender({0})".format(PortDict[Port]), 
				}
			Code+= HDL.Instanciate(
				"Router_"+RouterType, 
				"Hermes_inport", # Entity name
				"Hermes_inport", # Architecture name
				SignalDict, 
				GenericDict={}, 
				Comment="input port map for "+RouterType)
		else:
			Code+='\n'+HDL.Comment("Connecting zeros to the removed input port".format(PortDict[Port]))
			for Signal in ["h", "data_av", "data", "sender", "credit_o", ]:
				Code+=HDL.Connect(Signal+"({0})".format(PortDict[Port]), 0, 5)
	return Code
	
#==================================================================
# Write the output port map for a specific router.
#==================================================================
def RoutersOutport(RouterType):
	"""
	Return the output port map for each router.
	"""
	PortDict={"EAST":0, "WEST":1, "NORTH":2, "SOUTH":3, "LOCAL":4}
	ConnectDict={"BL":[0,2], "BC":[0,1,2], "BR":[1,2], "CL":[0,2,3], "CC":[0,1,2,3], "CR":[1,2,3], "TL":[0,3], "TC":[0,1,3], "TR":[1,3]} # Dictionnaire des connections
	Code=""
	for Port in ["EAST", "WEST", "NORTH", "SOUTH", "LOCAL",]:
		if ConnectDict[RouterType].count(PortDict[Port]):
			SignalDict={
				"clock":	"clock",
				"reset":	"reset",
				"data_av":	"data_av", 
				"data":		"data", 
				"data_ack":	"data_ack({0})".format(PortDict[Port]),
				"free": 	"free({0})".format(PortDict[Port]),
				"all_lane_tx":	"all_lane_tx",
				"tableIn": 	"tableIn({0})".format(PortDict[Port]),
				"tableOut": 	"tableOut({0})".format(PortDict[Port]),
				"clock_tx": 	"clock_tx({0})".format(PortDict[Port]),
				"tx": 		"tx({0})".format(PortDict[Port]),
				"lane_tx": 	"lane_tx({0})".format(PortDict[Port]),
				"data_out": 	"data_out({0})".format(PortDict[Port]),
				"credit_i":	"credit_i({0})".format(PortDict[Port]),
				}
			Code+= HDL.Instanciate(
				"OP_"+RouterType, 
				"Hermes_outport",  # Entity name
				"Hermes_outport_"+RouterType, # Architecture name
				SignalDict, 
				GenericDict={}, 
				Comment="output port map for "+RouterType)
		else:
			Code+='\n'+HDL.Comment("Connecting zeros to the removed output port".format(PortDict[Port]))
			for Signal in ["data_ack", "aux_lane_tx", "data_out", ]:
				Code+=HDL.Connect(Signal+"({0})".format(PortDict[Port]), 0, 5)
			Code+=HDL.Connect("clock_tx({0})".format(PortDict[Port]), "clock")
			Code+=HDL.Connect("tx({0})".format(PortDict[Port]), 0, 1)
	return Code
	
#==================================================================
# Write output process.
#==================================================================
def DataOutProcess(VC=2):
	if VC<2:
		logging.error("") 
		return ""
	SensitivList = ["c{0}".format(i+1) for i in range(0,VC)]
	AllocationDict = {}
	for i in range(0, VC): # Write case last_lane_tx signal.
		if i != (VC-1): Condition = HDL.LogicValue(int(math.pow(2,i)), VC)
		
		IfList=[]
		for k in range(i+1, VC):
			Condition=HDL.Equals("c{0}".format(k+1),HDL.LogicValue(1, 1))
			Action= "; ".join([
				HDL.Connect("aux_lane_tx",HDL.LogicValue(int(math.pow(2,k)),VC)),
				HDL.Connect("indice", "tableOut({0})".format(k+1))
				])
			IfList.append([Condition,Action])
		IfList.append([None, HDL.Connect("aux_lane_tx",HDL.LogicValue(0, VC))])
		AllocationDict[Condition]=HDL.If(IfList)
	Content = HDL.Case("last_lane_tx", AllocationDict, Comments="")
	return HDL.Process("DataOutProcess", SensitivList, "", Content, Comments="Write case last_lane_tx signal.")
	
#==================================================================
# Write test all_lane_tx signal for DataAck assignment.
#==================================================================
def DataAckTest(VC=1, PortName="LOCAL"):
	PortDict={"EAST":0, "WEST":1, "NORTH":2, "SOUTH":3, "LOCAL":4}
	if   PortName=="LOCAL": PortList=["EAST", "WEST", "NORTH", "SOUTH",]
	elif PortName=="EAST":  PortList=["WEST", "NORTH", "SOUTH", "LOCAL"]
	elif PortName=="WEST":  PortList=["EAST", "NORTH", "SOUTH", "LOCAL"]
	elif PortName=="NORTH": PortList=["SOUTH", "LOCAL"]
	elif PortName=="SOUTH": PortList=["NORTH", "LOCAL"]
	Lines=[]
	for i in range(0,VC):
		for Port in PortList:
			Text=""
			for j in range(0, VC):
				if not(Port==PortList[0] and i==0): # Does not write if it is a initial port and first virtual channel 
					Text+=HDL.Indent("\n",2)
				Text+="all_lane_tx({0})(L{1}) when tableIn(L{2})=x\"{3}{4}\" and data_av({5})(L{2})='1' else ".format(Port, j+1, i+1, PortDict[Port], j, PortName)
			#Text+=") else"
			Lines.append(HDL.Connect("data_ack(L{0})".format(i+1), Text+"\t\t'0'"))
		
	return "\n".join(Lines)
	
#==================================================================
# Write test if there is a packet in a specific channel of a specific port.
#==================================================================
def DataOutTest(VC=1, PortName="LOCAL"):
	PortDict={"EAST":0, "WEST":1, "NORTH":2, "SOUTH":3, "LOCAL":4}
	if   PortName=="LOCAL": PortList=["EAST", "WEST", "NORTH", "SOUTH",]
	elif PortName=="EAST":  PortList=["WEST", "LOCAL"]
	elif PortName=="WEST":  PortList=["EAST", "LOCAL"]
	elif PortName=="NORTH": PortList=["EAST", "WEST", "SOUTH", "LOCAL"]
	elif PortName=="SOUTH": PortList=["EAST", "WEST", "NORTH", "LOCAL"]
	Lines=[]
	for Port in PortList:
		for i in range(0,VC):
			Text=""
			if not(Port==PortList[0] and i==0): # Does not write if it is a initial port and first virtual channel 
				Text+=HDL.Indent("\n",2)
			Text+='data({0})(L{1}) when indice=x"{2}{3}" and ('.format(Port, i+1, PortDict[Port], i)
			for j in range(0, VC):
				if j==(VC-1): Text+="c{0}='1'".format(j+1)
				else: Text+="c{0}='1' or ".format(j+1)
			Text+=") else"
		Lines.append(Text)
		
	
	return HDL.Connect("data_out", "\n".join(Lines)+"\t\t(others=>'0')")
			
#==================================================================
# Write test for TX.
#==================================================================
def TxTest(VC=1):
	Text="tx <= '1' when ("
	for i in range(0, VC):
		if i != VC-1: Text+="c{0}='1' or ".format(i+1)
		else: Text+="c{0}='1') else '0';\n".format(i+1)
	return Text
	
#==================================================================
# Write test C signal in a specifed port.
#==================================================================
def CTest(VC=1, PortName="LOCAL"):
	Lines=[]
	PortDict={"EAST":0, "WEST":1, "NORTH":2, "SOUTH":3, "LOCAL":4}
	if   PortName=="LOCAL": PortList=["EAST", "WEST", "NORTH", "SOUTH",]
	elif PortName=="EAST":  PortList=["WEST", "LOCAL"]
	elif PortName=="WEST":  PortList=["EAST", "LOCAL"]
	elif PortName=="NORTH": PortList=["EAST", "WEST", "SOUTH", "LOCAL"]
	elif PortName=="SOUTH": PortList=["EAST", "WEST", "NORTH", "LOCAL"]
	Lines.append(HDL.Comment("C signal test for '{0}' port:".format(PortName)))
	for i in range(0,VC): # The number of virtual channel of tableOut signal.
		Lines.append("c{0} <= '1' when free(L{0})='0' and credit_i(L{0})='1' and".format(i+1))
		for Port in PortList:
		# Write test if there is a packet in a specific channel j of the port 'Port'.
			Text=""		
			for j in range(0,VC): # The number of virtual channel for data_av signal.
				if Port == PortList[0] and j==0: Text+="("
				else: Text+=" "
				Text+="(tableOut(L{0})=x\"{1}{2}\" and data_av({3})(L{4})='1')".format(i+1, PortDict[Port], j, Port, j+2)
				if Port == PortList[-1] and j==(VC-1): Text+=") else\n"
				else: Text+=" or"
			Lines.append(Text)
	return "\n".join(Lines)+";"
				
#==================================================================
# Write sender conditions in Hermes_switchcontrol file.
#==================================================================
def SenderConditions(VC=1):
	Lines=[]
	for Port in ["LOCAL", "EAST", "WEST", "NORTH", "SOUTH"]:
		Lines+=["if sender({0})(L{1})='0' and sender_ant({0})(L{1})='1' then auxfree(CONV_INTEGER(source({0})(L{1})(7 downto 4)))(CONV_INTEGER(source({0})(L{1})(3 downto 0))) <='1'; end if;".format(Port, i+1) for i in range(0,VC)]
	
	return "\n".join(Lines)
	
#==================================================================
# Write sender_ant signal for Hermes_switchcontrol file.
#==================================================================
def SenderAnt(VC=1):
	Lines=[]
	for Port in ["LOCAL", "EAST", "WEST", "NORTH", "SOUTH"]:
		Lines.append(HDL.Comment("'{0}' port sender ant:".format(Port)))
		Lines+=([HDL.Connect("sender_ant({0})(L{1})".format(Port, i+1), "sender({0})(L{1})".format(Port, i+1)) for i in range(0, VC)] )
		
	return "\n".join(Lines)
			
#==================================================================
# Write implement condition (XY or WFM) for Hermes_switchcontrol file.
#==================================================================
def StatesImplements(VC=1, Routing="XY"):
	Lines=[]
	if   Routing=="XY" : PortList = ["LOCAL", "dirx", "diry"]
	elif Routing=="WFM": PortList = ["LOCAL", "WEST", "EAST", "NORTH", "SOUTH"]
	else: 
		logging.error("Routing algorithm '{0}' not supported: Hermes_switchcontrol file not edited properly.".format(Routing))
		return ""
	for Port in PortList:
		for i in range(0, VC):
			if   Port=="dirx": 
				State = i+4+VC
				Comment="Connection EAST or WEST port - Channel L{0}".format(i+1)
			elif Port=="diry": 
				State = i+4+2*VC
				Comment="Connection NORTH or SOUTH port - Channel L{0}".format(i+1)
			else:
				if   Port=="WEST":  State = i+4+VC
				elif Port=="EAST":  State = i+4+2*VC
				elif Port=="NORTH": State = i+4+3*VC
				elif Port=="SOUTH": State = i+4+4*VC
				else: State = i+4
				Comment = "Connection "+Port+" port - Channel L{0}".format(i+1)
				
			Lines.append(HDL.Comment(Comment))
			Lines.append("when S{0} =>".format(State))
			Lines.append("source(sel)(sel_lane) <= CONV_STD_LOGIC_VECTOR({0},4) & CONV_STD_LOGIC_VECTOR(L{1},4);".format(Port, i+1))
			Lines.append("mux_out({0})(L{1}) <= CONV_STD_LOGIC_VECTOR(sel,4) & CONV_STD_LOGIC_VECTOR(sel_lane,4);".format(Port, i+1))
			Lines.append("auxfree({0})(L{1}) <= '0';".format(Port, i+1))
			Lines.append("ack_h(sel)(sel_lane) <= '1';")
	return "\n".join(Lines)
			
			
#==================================================================
# Write S2 state condition for Hermes_switchcontrol file.
#==================================================================
def S2StateCondition(VC=1):
	Lines=[]
	for i in range(0,VC):
		if i== 0: Lines.append("if h(prox)(L{0})='1' then sel_lane <= L{0};".format(i+1))
		elif i<(VC-1): Lines.append("elsif h(prox)(L{0})='1' then sel_lane <= L{0};".format(i+1))
		else: Lines.append("else sel_lane <= L{0}; end if;\n".format(i+1))
	
	return "\n"+"\n".join(Lines)
	
#==================================================================
# Write Final state condition for Hermes_switchcontrol file.
#==================================================================
def FinalStateCondition(VC=1, Routing="XY"):
	if   Routing=="XY":  State = 4+3*VC
	elif Routing=="WFM": State = 5+4*VC
	else: State = 0
	
	Lines=[	"when S{0} => PES<=S1;".format(State),
		"when others => PES<=S{0};".format(State),]
		
	return "\n"+"\n".join(Lines)
	
#==================================================================
# Write state condition for Hermes_switchcontrol file.
#==================================================================
def StateCondition(VC=1, Routing="XY"):
	Lines=[]
	if Routing=="XY":
		for i in range(VC):
			if i==0: Lines.append("if lx = tx and ly = ty and auxfree(LOCAL)(L{0})='1' then PES<=S{1};".format(i+1,i+4))
			else: Lines.append("elsif lx = tx and ly = ty and auxfree(LOCAL)(L{0})='1' then PES<=S{1};".format(i+1,i+4))
		
		Lines+= (["elsif lx /= tx and auxfree(dirx)(L{0})='1' then PES<=S{1};".format(i+1,i+4+VC) for i in range(VC)])
		Lines+= (["elsif lx = tx and ly /= ty and auxfree(diry)(L{0})='1' then PES<=S{1};".format(i+1,i+4+2*VC) for i in range(VC)])
	
	return "\n".join(Lines)
	
#==================================================================
# Write prox signal in the LOCAL port.
#==================================================================
def AffectProx(VC=1, PortName="LOCAL"):
	PortList = ["EAST", "WEST", "NORTH", "SOUTH", "LOCAL",]
	while PortList[-1]!=PortName: PortList.append(PortList.pop(0))
	IfList=[]
	for Port in PortList:
		if PortList[-1] == Port: IfList.append([None, HDL.Connect("prox", Port)])
		else:			
			Condition = HDL.NotEquals("h({0})".format(Port),HDL.LogicValue(0,VC))
			Action    = HDL.Connect("prox", Port)
			IfList.append([Condition,Action])
			
	return HDL.If(IfList) # Return allocation in if/elsif/else format (for process)
	
#==================================================================
# Write ask signal of a specific port.
#==================================================================
def AffectAsk(VC=1):
	Text=""
	for Port in ["LOCAL", "EAST", "WEST", "NORTH", "SOUTH"]:
		if not Port=="SOUTH":
			Text+=" or \n".join(" or ".join(["h({0})(L{1})='1'".format(Port, i) for i in range(0, VC)]))
		else:	
			Text+=" else '0'"
			break
		
	return "'1' when " + Text
	
#==================================================================
# Return connections for local channel (which doesn't exist).
#==================================================================
def LocalVC(VC=1):
	"""
	Return code for signal connection of local virtual channel.
	"""
	Text=""
	for i in range(0, VC):
		Text+= HDL.Connect("credit_o(L{0})".format(i+1), None, Size=1)
		Text+= HDL.Connect("h(L{0})".format(i+1), None, Size=1)
		Text+= HDL.Connect("data_av(L{0})".format(i+1), None, Size=1)
		Text+= HDL.Connect("data(L{0})".format(i+1), None, Size=5)
		Text+= HDL.Connect("sender(L{0})".format(i+1), None, Size=1)
	return Text
	
#==================================================================
# Return Rx signal declaration for all Virtual channels
#==================================================================
def RxVC(VC=1):
	"""
	Return code for rxL signal declaration of all virtual channels.
	"""
	SignalNames = [("rxL"+str(i+1)) for i in range(0, VC)]
	return HDL.Signals(SignalNames, "std_logic", "'0'")
	
#==================================================================
# Return test of Rx signal in all virtual channels.
#==================================================================
def RxTestVC(VC=1):
	"""
	Return code for signal affectation of rxL in all virtual channels.
	"""
	Template="'1' when rx='1' and lane_rx(L{0})='1' else '0';"
	return "".join([HDL.Connect("rxL"+str(i+1), Template.format(i+1)) for i in range(0, VC)])
	
#==================================================================
# Return the buffer instanciation for all virtual channels.
#==================================================================
def InstBufVC(VC=1):
	"""
	Return code for signal connection of local virtual channel.
	"""
	Comment="buffer port maps for virtual channels {0}"
	GenericDict={}
	
	return "".join([HDL.Instanciate("BUFL"+str(i+1), "Hermes_buffer", None, {
			"clock":     "clock", 
			"reset":     "reset", 
			"clock_rx":  "clock_rx", 
			"rx":        "rxL"+str(i+1), 
			"data_in":   "data_in", 
			"credit_o":  "credit_o(L{0})".format(i+1), 
			"h":         "h(L{0})".format(i+1), 
			"ack_h":     "ack_h(L{0})".format(i+1), 
			"data_av":   "data_av(L{0})".format(i+1), 
			"data":      "data(L{0})".format(i+1), 
			"data_ack":  "data_ack(L{0})".format(i+1), 
			"sender":    "sender(L{0})".format(i+1), 
			}, GenericDict, Comment.format(i)) for i in range(0, VC)])
	
#==================================================================
# Generate [name, Address, Type] list for all routers.
#==================================================================
def Routers(Dimensions="3x3", FlitWidth=16):
	"""
	Return a list of list of router name, Address, Type corresponding to a specified size.
	Router name is made up of 3 parts: 'N' + x coordinate(hexa) + y coordinate(hexa)
	For example, a dimensions 2x2 will produce routers N0000, N0100, N0001, N0101.
	"""
	# Get integers for xDim and yDim
	xDim, yDim = list(map(int, Dimensions.lower().split('x')))
	
	# Generate the list
	RouterList=[]	
	NumberTemplate=("{0:0"+str(FlitWidth/8)+"X}{1:0"+str(FlitWidth/8)+"X}")
	AddressTemplate="{0:0"+str(FlitWidth/4)+"b}{1:0"+str(FlitWidth/4)+"b}"
	for y in range(0, yDim):
		for x in range(0, xDim):
			# Get router name
			RouterNum = NumberTemplate.format(x, y)
			# Get router address
			RouterAddr = AddressTemplate.format(x, y)
			# Get router Type
			if x==0:
				if y==0:          RouterType="BL"
				elif y==(yDim-1): RouterType="TL"
				else:             RouterType="CL"
			elif x==(xDim-1):
				if y==0:          RouterType="BR"
				elif y==(yDim-1): RouterType="TR"
				else:             RouterType="CR"
			else:
				if y==0:          RouterType="BC"
				elif y==(yDim-1): RouterType="TC"
				else:             RouterType="CC"
			RouterList.append([RouterNum, RouterAddr, RouterType,])
	return RouterList
	
#==================================================================
# Generate NoC top HDL file content.
#==================================================================
def GenerateTop(Type, FluxCtrl, Dimensions, FlitWidth, VC, Language="VHDL"):
	"""
	Return the code for specified NoC top.
	"""
	Code=""
	# Generate the libraries----------------------------------------------------------------
	Code+=HDL.Libraries(GetLibraries(Type))
	# Generate the Packages-----------------------------------------------------------------
	Code+=HDL.Packages(GetPackages(Type))
	# Generate the NoC entity---------------------------------------------------------------
	Comments="NoC {0}, Dimensions:{1}, Flit width:{2}, Virtual Channels: {3}".format(Type, Dimensions, FlitWidth, VC)

	NoCSigDict = GetIO(Type, FlitWidth, FluxCtrl, Dimensions)
	Code+=HDL.Entity("NoC", NoCSigDict["Resets"]+NoCSigDict["Clocks"]+NoCSigDict["NetworkInterface"]+NoCSigDict["Ports"], Comments)

	# Generate the architecture--------------------------------------------------------------
	NumberTemplate=("{0:0"+str(FlitWidth/8)+"X}{1:0"+str(FlitWidth/8)+"X}") # Router number
	# Get integers for xDim and yDim
	xDim, yDim = list(map(int, Dimensions.lower().split('x')))
	# List signals to declare in architecture (Network interface signals => One for each router)
#	ConnectorLists=[]
#	
#	for SName, SIO, SSize, SType in NoCSigDict["NetworkInterface"]:
#		ConnectorList=[]
#		for y in range(0, yDim): # First y (for each line)
#			for x in range(0, xDim):
#				ID = "N"+NumberTemplate.format(x,y)
#				# For each router define these signals :
#				SignalList.append(SName+ID)
#		# Build the signals list for declaration
#		RouterType=SType.replace("regNrot", "regNport").replace("arrayNrot", "arrayNport")
#		ConnectorLists.append([SignalList, RouterType, None,])
			
	# List instances to add to the architecture
	Instances = []
	ConnectorList=[]
	# Get the routers to instanciate
	RouterList = Routers(Dimensions, FlitWidth)
	RouterSigDict = GetRouterNI(Type, FluxCtrl, VC, 5)
	for RouterNum, RouterAddr, RouterType in RouterList:
		SignalDict={}
		# Port map association for Routers
		for Index in range(0, len(NoCSigDict["Clocks"])):
	 		SignalDict[RouterSigDict["Clocks"][Index][0]] = NoCSigDict["Clocks"][Index][0]
	 		
		for Index in range(0, len(NoCSigDict["Resets"])):
	 		SignalDict[RouterSigDict["Resets"][Index][0]] = NoCSigDict["Resets"][Index][0]
			
		for Index in range(0, len(NoCSigDict["NetworkInterface"])):
			Connector = NoCSigDict["NetworkInterface"][Index][0]+'N'+RouterNum
	 		SignalDict[RouterSigDict["NetworkInterface"][Index][0]] = Connector
			ConnectorList.append([Connector, RouterSigDict["NetworkInterface"][Index][3], None])			
	 	# Router generics :
		GenericDict={"address": "ADDRESSN"+RouterNum,}
	 	# Router comment :
		Comment="Router : Number={0}, Addr={1}, Type={2}, VC={3}, FlitW={4}".format(RouterNum, RouterAddr, RouterType, VC, FlitWidth) 
		# Instance code :
		Instances.append(["Router"+RouterNum, "Router"+RouterType, None, SignalDict, GenericDict, Comment])
	# Get the code for signals to connect
	Content=ConnectRouters(Type, FluxCtrl, VC, 5, FlitWidth, Dimensions, RouterList) # Return the code for routers connections
	# Insert it into the architecture code :
	Code+=HDL.Architecture("v0", "NoC", ConnectorList, Instances, Content, Comments="Version 0 - built by YANGO from ADACSYS.")
	
	return Code
		
#==================================================================
# Generate the code for routers connections
#==================================================================
def ConnectRouters(Type, FluxCtrl, VC, NbPorts, FlitWidth, Dimensions, RouterList):
	"""
	Return the code for routers connections
	"""
	Code=""
	xDim, xDim = list(map(int, Dimensions.split('x')))
	NRouter  = xDim*xDim	
	NoCSigDict = GetIO(Type, FlitWidth, FluxCtrl, Dimensions, VC)
	for RouterNum, RouterAddr, RouterType in RouterList:
		Code+="\n-- ROUTER "+RouterNum+" ({0})".format(RouterType)
		PortDict={"EAST":0, "WEST":1, "NORTH":2, "SOUTH":3, "LOCAL":4}
		Rules={1:0, 0:1, 2:3, 3:2} # Port association
		ConnectDict={"BL":[0,2], "BC":[0,1,2], "BR":[1,2], "CL":[0,2,3], "CC":[0,1,2,3], "CR":[1,2,3], "TL":[0,3], "TC":[0,1,3], "TR":[1,3]} # Dictionnaire des connections
		
		for RouterPort in list(PortDict.keys()):
			Code+="\n-- {0} port (Addr:{1}, type:{2})".format(RouterPort, RouterAddr, RouterType)
			if ConnectDict[RouterType].count(PortDict[RouterPort]) and RouterNum<(NRouter-1):
				# For each router signal of the input control flux
				print("Router '{0}', Port:'{1}', Influx:{2}, NetworkInterface:{3}".format(RouterType, RouterPort, len(NoCSigDict["InFlux"]), len(NoCSigDict["NetworkInterface"])))
				for Sig, IO, Size, Type in NoCSigDict["InFlux"]:
					# Router1 => Router2
					Code+=HDL.Connect(
						"{0}N{1}({2})".format(Sig,RouterNum,PortDict[RouterPort]),
						"{0}N".format(Sig)+GetNextRouter(RouterNum, x=True, y=False)+"({0})".format(Rules[PortDict[RouterPort]]))
			elif PortDict[RouterPort]==4: # LOCAL
				# For each router signal
				for Sig, IO, Size, Type in NoCSigDict["NetworkInterface"]:
					# IP => Router and Router => IP
					if IO.upper()=="IN": Code+=HDL.Connect("{0}N{1}({2})".format(Sig,RouterNum,4), "{0}(N{1})".format(Sig,RouterNum))
					else: Code+=HDL.Connect("{0}(N{1})".format(Sig,RouterNum), "{0}N{1}({2})".format(Sig,RouterNum,4))
			else: # no connection
				# For each router signal of the input control flux
				for Sig, IO, Size, Type in NoCSigDict["InFlux"]:
					if Type.startswith("array"): Length=5 # Length > 1
					else: Length=1
					Code+=HDL.Connect(
						"{0}N{1}({2})".format(Sig,RouterNum,PortDict[RouterPort]), None, Length)
	return Code
						
#==================================================================
# Get next router name in x or y direction from actual router name
#==================================================================
def GetNextRouter(RouterName, x=False, y=False):
	"""
	Get next router name on x or y direction.
	"""	
	# Find current x, y from name
	NbChar = len(RouterName)/2
	Template="{0:0"+str(NbChar)+"X}"
	if x and not y: return Template.format(int(RouterName[:NbChar], 16)+1)+RouterName[NbChar:]
	elif y and not x: return RouterName[:NbChar]+Template.format(int(RouterName[NbChar:], 16)+1)
	elif x and y:  return Template.format(int(RouterName[:NbChar], 16)+1)+Template.format(int(RouterName[NbChar:], 16)+1)
	else: return RouterName

#==================================================================
# Generate common NoC package content.
#==================================================================
def CommonPackage(LibPath, OutputPath, Type="HERMES", Dimensions="3x3", FlitWidth=16, BufferDepth=32, VC=1, Language="VHDL"):
	"""
	Return the code of a package.
	Router name is made up of 3 parts: 'N' + x coordinate(hexa) + y coordinate(hexa)
	For example, a dimension 2x2 will produce routers N0000, N0100, N0001 and N0101.
	"""	
	if Language=="VHDL":
		# Define constants
		ConstCode=""
		#AddressTemplate="{0:0"+str(FlitWidth/4)+"b}{1:0"+str(FlitWidth/4)+"b}"
		RouterList = Routers(Dimensions=Dimensions, FlitWidth=FlitWidth)
		# First declare the constants (two for each router: Number and address)
		for RouterNum, RouterAddr, RouterType in RouterList:
			x, y = int(RouterNum[-FlitWidth/4:-FlitWidth/8], 16), int(RouterNum[-FlitWidth/8:], 16)
			ConstCode+=HDL.Indent(HDL.Constant('N'+RouterNum, "integer", RouterList.index([RouterNum, RouterAddr, RouterType])))
			ConstCode+=HDL.Indent(HDL.Constant("ADDRESSN"+RouterNum, "std_logic_vector({0} downto 0)".format(FlitWidth/2-1), '"{0}"'.format(RouterAddr)))
		ConstCode+='\n'
		
		# Then declare the lanes for virtual channels
		VCCode=""
		for Lane in range(0, VC):
			VCCode+=HDL.Indent(HDL.Constant("L"+str(Lane+1), "integer", Lane))
			# TODO: Ajout condition pour isSR4 (Source Routing - HERMES_SR)
	else:
		ConstCode = ""
		VCCode = ""
	# Build the dictionary for value replacement in template
	# Get integers for xDim and yDim
	xDim, yDim = list(map(int, Dimensions.lower().split('x')))
	Dictionary={
		"n_nodos": ConstCode,
		"n_lanes": VCCode,
		"n_port": str(5),
		"tam_line": str(int(FlitWidth/4)),
		"flit_size": str(FlitWidth),
		"buff_depth": str(BufferDepth),
		"pointer_size": str(int(math.ceil(math.log(BufferDepth, 2)))),
		"n_rot": str(xDim*yDim),
		"max_X": str(xDim-1),
		"max_Y": str(yDim-1),
		"n_lane": str(VC),
		"buff1":"--",
		"buff2":"--",
		}
	# Get template and add constant + subsitute key by value
	TemplatePath = os.path.join(LibPath, "Hermes_package"+ExtensionDict[Language])
	OutputPath   = os.path.join(OutputPath, Type.capitalize()+"_package"+ExtensionDict[Language])
	TemplateEdit(TemplatePath, OutputPath, Dictionary)
	return OutputPath

#==================================================================
# Create a new file which replace each template token by its value given in 'Dictionary'.
#==================================================================
def TemplateEdit(InFilePath, OutFilePath, Dictionary):
	"""
	Create a new file 'OutFilePath' (replace it if it already exist) 
	which replace each token in 'InFilePath' by its value given in 'Dictionary'.
	"""	
	# Check arguments------------------------------------
	if not os.path.isfile(InFilePath):
		logging.error("Input '{0}' is not a correct file: Template edition aborted.".format(InFilePath))
		return None
	if not os.path.isdir(os.path.dirname(OutFilePath)):
		logging.error("Output '{0}' directory does not exist: Template edition aborted.".format(os.path.dirname(OutFilePath)))
		return None
	if not isinstance(Dictionary, dict):
		logging.error("Dictionary argument is not a 'dict' object : Template edition aborted.")
		return None
	# Start editing template-----------------------------
	Text=""
	with open(InFilePath, 'r') as TEMPLATE:
		TemplateText = TEMPLATE.read()
		for Key in list(Dictionary.keys()):
			# Substitue key by its value
			#TemplateText=re.sub("${0}$".format(Key), Dictionary[Key], TemplateText, re.IGNORECASE)
			TemplateText=TemplateText.replace("${0}$".format(Key), Dictionary[Key])
	# Now write the final text to OutFilePath------------
	with open(OutFilePath, 'w+') as OUTFILE:
		OUTFILE.write(TemplateText)
	return TemplateText
		
#==================================================================
# Search in directory tree for specified file, and copy it to output folder.
#==================================================================
def SearchAndCopy(InputDirectory, OutputFolder, KeyWords):
	"""
	Search recursively into InputDirectory folders.
	Then copy the file that match key words in 'KeyWords' to OutputFolder.
	"""	
	SourceList=[]	
	for Root, Dirs, Files in os.walk(InputDirectory):
		for FileName in [x for x in Files if not x.startswith('.')]:
			for KeyWord in KeyWords:
				# Copy source files
				if re.match(r'.*{0}.*'.format(KeyWord), FileName.lower()):
					if FileName.endswith(".vhd") or FileName.endswith(".v"):
						CopyFile(os.path.join(Root, FileName), OutputFolder)
						SourceList.append(os.path.join(OutputFolder, FileName))
						
						logging.info("File '{0}' copied to '{1}'.".format(FileName, OutputFolder))	
	return SourceList
	
#==================================================================
# Main tests.
#==================================================================
if __name__ == "__main__":
	print(("NoC 3x3 router list:", Routers(NoCDimension="3x3")))
	
	print(("Next router name of 0000 in x dir:", GetNextRouter("0000", x=True, y=False)))
	print(("Next router name of 0305 in x dir:", GetNextRouter("0305", x=True, y=False)))
	print(("Next router name of 0206 in y dir:", GetNextRouter("0206", x=False, y=True)))
	print(("Next router name of 0709 in y dir:", GetNextRouter("0709", x=False, y=True)))
	print(("Next router name of 0E0E in x and y dir:", GetNextRouter("0E0E", x=True, y=True)))
	
	
	
	
	
	
	
	
	
