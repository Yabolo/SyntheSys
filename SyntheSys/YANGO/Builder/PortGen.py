#! /usr/bin/python

import os, sys, re, logging
import math

sys.path.append(os.path.abspath("./HDLEditor"))
import Constraints

from distutils.file_util import copy_file as CopyFile

ExtensionDict = HDLEditor.ExtensionDict

#==================================================================
# Ports  avalability
#==================================================================
def AvailablePorts(ConstraintsFile):
	"""
	Generate a list of available ports for an architecture by reading the constraints file.
	it return a Dictionnary with 
	"""
	return Constraints.IOPins(ConstraintsFile)
	
#==================================================================
# Return IP IOs and module name 
#==================================================================
def GenConstraintsFile(PortDict):
	"""
	Generate a constraint file according to Port dictionary.
	Port dictionary keys are the port names.
	Its values are dictionaries of connected signals. (with IO port map).
	"""
	return

#==================================================================
# Main tests.
#==================================================================
if __name__ == "__main__":
	
	print("IPGen module test.")
	
	
	
	
	
	
	
