#! /usr/bin/python

import os, sys, re, logging
import math
from functools import reduce

sys.path.append(os.path.abspath("./HDLEditor"))
import HDLEditor as HDL

from distutils.file_util import copy_file as CopyFile

#--------------------------------------
LibPath="./"
Language="VHDL"
Extension=HDL.ExtensionDict[Language]
#--------------------------------------
def SetHDL(Lang):
	Language=Lang
	Extension=HDL.ExtensionDict[Language]
	
	
#==================================================================
# Package class
#==================================================================
class Package:
	def __init__(self, Name="NoCPkg", Dimensions="3x3", FlitWidth=16, BufferDepth=32, VC=1):
		self.Name        = Name
		self.Dimensions  = Dimensions
		self.FlitWidth   = FlitWidth
		self.BufferDepth = BufferDepth
		self.VC          = VC
		FuncDict  = {}
		ConstDict = {}
		TypeDict  = {}
		logging.info("Create object 'Package' named '{0}'.".format(Name))
		
	#------------------------------------------------------------------------
	def AddConst(self, ConstName, ConstValue):
		"""
		Add a constant to package list. I already in the list: replace it.
		"""
		ConstDict[ConstName]=ConstValue
		
	#------------------------------------------------------------------------
	def AddConst(self, TypeName, TypeValue):
		"""
		Add a constant to package list. I already in the list: replace it.
		"""
		TypeDict[ConstName]=TypeValue
		
	#------------------------------------------------------------------------
	def Declare(self):
		"""
		Return HDL code for package declaration in a design src file.
		"""
		return HDL.Package(self.Name)
		
	#------------------------------------------------------------------------
	def GenHDL(self, OutputPath):
		"""
		Return the code of a package.
		Router name is made up of 3 parts: 'N' + x coordinate(hexa) + y coordinate(hexa)
		For example, a dimension 2x2 will produce routers N0000, N0100, N0001 and N0101.
		"""	
		# Define constants
		ConstCode=""
		#AddressTemplate="{0:0"+str(FlitWidth/4)+"b}{1:0"+str(FlitWidth/4)+"b}"
		RouterList = Routers(Dimensions=self.Dimensions, FlitWidth=self.FlitWidth)
		# First declare the constants (two for each router: Number and address)
		for RouterNum, RouterAddr, RouterType in RouterList:
			x, y = int(RouterNum[-FlitWidth/4:-FlitWidth/8], 16), int(RouterNum[-FlitWidth/8:], 16)
			ConstCode+=HDL.Indent(HDL.Constant('N'+RouterNum, "integer", RouterList.index([RouterNum, RouterAddr, RouterType])))
			ConstCode+=HDL.Indent(HDL.Constant("ADDRESSN"+RouterNum, "std_logic_vector({0} downto 0)".format(FlitWidth/2-1), '"{0}"'.format(RouterAddr)))
		ConstCode+='\n'
	
		# Then declare the lanes for virtual channels
		VCCode=""
		for Lane in range(0, self.VC):
			VCCode+=HDL.Indent(HDL.Constant("L"+str(Lane+1), "integer", Lane))
			# TODO: Ajout condition pour isSR4 (Source Routing - HERMES_SR)
				
		# Build the dictionary for value replacement in template
		# Get integers for xDim and yDim
		xDim, yDim = list(map(int, self.Dimensions.lower().split('x')))
		Dictionary={
			"n_nodos": ConstCode,
			"n_lanes": VCCode,
			"n_port": str(5),
			"tam_line": str(int(self.FlitWidth/4)),
			"flit_size": str(self.FlitWidth),
			"buff_depth": str(self.BufferDepth),
			"pointer_size": str(int(math.ceil(math.log(self.BufferDepth, 2)))),
			"n_rot": str(xDim*yDim),
			"max_X": str(xDim-1),
			"max_Y": str(yDim-1),
			"n_lane": str(self.VC),
			"buff1":"--",
			"buff2":"--",
			}
		# Get template and add constant + subsitute key by value
		TemplatePath = os.path.join(LibPath, "Hermes_package"+Extension)
		OutputPath   = os.path.join(OutputPath, "HERMES_package"+Extension)
		TemplateEdit(TemplatePath, OutputPath, Dictionary)
		return OutputPath
	#------------------------------------------------------------------------
		
#==================================================================
Pack = Package() # Global variable
#==================================================================

#==================================================================
# NoC parameters avalability
#==================================================================
def AvailableValues(Parameter):
	"""
	Return a list of available values for this parameter.
	"""
	AvailableList=[]
	if Parameter == "Type": AvailableList = ["HERMES","CUSTOM"]
	elif Parameter == "FlitWidth":  AvailableList = ["8", "16", "32", "64", "128"]
	elif Parameter == "Buffers":    AvailableList = ["4", "8", "16", "32"]
	elif Parameter == "Lanes":      AvailableList = ["1", "2", "4"]
	elif Parameter == "Routing":    AvailableList = ["XY", "WFM"]
	elif Parameter == "Scheduling": AvailableList = ["RoundRobin", "Priority",]
	
	return {Parameter:AvailableList}

#==================================================================
# Abstract Component class
#==================================================================
class Comp:
	"""
	Object with basic methods for component HDL generation.
	"""
	#----------------------------------------------------------------------
	def __init__(self, InstanceName, EntityName):
		self.Name   = InstanceName
		self.Module = EntityName
		# Build IO dictionary
		self.IO={"Resets":[], "Clocks":[], "In":[],"Out":[],}
		# Build Generics list
		self.GenericList=[]
		# Internal signal dictionary
		self.IntSig=[]
		logging.info("Create component object named '{0}({1})'.".format(self.Name, self.Module))
		
	#----------------------------------------------------------------------
	def Instanciate(self):
		"""
		Return Code for router instanciation.
		"""
		SignalDict={}
		for Sig in reduce(lambda x, y: x+y, list(self.IO.values())): 
			SignalDict[Sig.Name]="{0}_{1}_i".format(Sig.Name, self.Name)
		GenericDict = {}
		for GName, Type, GVal in self.GenericList:
			GenericDict[GName]=GVal
		Comment = str(self)
		return HDL.Instanciate(self.Name,self.Module,None,SignalDict,GenericDict,Comment)
				
	#----------------------------------------------------------------------
	def IntSigDeclare(self):
		"""
		Return component internal signals declarations.
		"""
		IntSigs=[]
		for Sig in self.TempIODict["Reset"]+self.TempIODict["Clocks"]+self.TempIODict["NetworkInterface"]:
			IntSig=HDL.Signal(Sig)
			IntSig.Name+="_{0}_i".format(self.Name)			
			IntSigs.append(IntSig)	
		
		return "".join([x.Declare() for x in IntSigs])
			
	#----------------------------------------------------------------------
	def Entity(self, Comments=""):
		"""
		Return HDL code for component entity declaration.
		"""
		IOs = reduce(lambda x, y: x+y, list(self.IO.values()))
		return HDL.Entity(	Name     = self.Name, 
					Generics = self.GenericList, 
					Ports    = IOs, 
					Comments = Comments)
					
	#----------------------------------------------------------------------
	def Archi(self, Declarations, Content, Comments="Version 0"):
		"""
		Return HDL code for router architecture body.
		"""
		# Return architecture code
		return HDL.Architecture(
				"Behavioral", 
				self.Module, 
				Declarations, 
				Content, 
				Comments)
		
	#----------------------------------------------------------------------
	def IntSigNew(self, Name, Size, Type, InitVal):
		"""
		Add a new signal to internal signal list and return a signal object.
		"""
		Sig = HDL.Signal(Name, "IN", Size, Type, InitVal)
		self.IntSig.append(Sig)
		return Sig
		
	#----------------------------------------------------------------------
	def IntSigDeclare(self):
		"""
		Return NoC internal signals declarations.
		"""
		IntSigs=[]
		for Sig in self.IntSig:
			IntSig=HDL.Signal(Sig)
			IntSig.Name+="_{0}_i".format(self.Name)			
			IntSigs.append(IntSig)
		
		return "".join([x.Declare() for x in IntSigs])
				
	#----------------------------------------------------------------------
	def __getitem__(self, Index):
		if isinstance(Index, str):
			for Sig in reduce(lambda x, y: x+y, list(self.IO.values()))+self.IntSig:
				if Sig.Name == Index:
					return Sig
			if list(self.IO.keys()).count(Index): return self.IO[Index]
				
		elif isinstance(Index, slice):
			start, stop, step = Index.indices(len(self))    # Index is a slice
			SigList = reduce(lambda x, y: x+y, list(self.IO.values()))
			return 
		else:
			raise NameError("Wrong signal name")
	#----------------------------------------------------------------------
	def WriteFile(self, FileName, Declarations, Content):
		with open(FileName, "w+") as HDLFile:
			HDLFile.write(HDL.Header(Module=self.Name, 
						Title=self.Module+" "+self.Name, 
						Purpose=self.Purpose, 
						Desc=self.Description, 
						Issues=self.Issues, 
						Speed=self.Speed,
						Area=self.Area, 
						Tool="Xilinx ISE (13.1)", 
						Rev="0.12")) # TODO
			HDLFile.write(Pack.Declare())
			Ports = reduce(lambda x, y: x+y, list(self.IO.values()))
			HDLFile.write(self.Entity())
			HDLFile.write(self.Archi(Declarations, Content))
	#----------------------------------------------------------------------
	def __len__(self):
		return len(reduce(lambda x, y: x+y, list(self.IO.values())))
		
	#----------------------------------------------------------------------
	def __repr__(self):
		return self.Name+"("+self.Module+")"

#==================================================================
# NoC generator object
#==================================================================
class NoC(Comp):
	"""
	Object that can build a entire NoC HDL code.
	"""
	#----------------------------------------------------------------------
	def __init__(self, Topology="Mesh", Dimensions="3x3", FluxCtrl="HandShake", FlitWidth=16, IPDict={}, VC=1, Routing="XY", Scheduling="Priority", MultiFPGA=False, InQDepth=8, OutQDepth=8):
		"""
		Initialize NoC parameters.
		"""
		Pack=Package("Hermes_package", Dimensions, FlitWidth, InQDepth, VC)
		Comp.__init__(self, "NoCiNoC", "NoC")
		self.PackageDict = {}
		self.Topology    = Topology
		xDim, yDim       = list(map(int, Dimensions.lower().split('x')))
		self.Dim         = {'x':xDim, 'y':yDim}
		self.FlitWidth   = FlitWidth
		self.IPDict      = IPDict
		self.VC          = VC
		self.Routing     = Routing
		self.Scheduling  = Scheduling
		self.Topology    = Topology
		self.FluxCtrl    = FluxCtrl
		
		self.MultiFPGA   = MultiFPGA
		self.InputQDepth = InQDepth
		self.OutputQDepth= OutQDepth
		
		# Build Generics list
		self.GenericList.append(["FlitWidth", "unsigned", self.FlitWidth])
		# Build IO dictionary
		self.GetIO()
		
		# Information
		self.Title       = "Network on Chip" 
		self.Purpose     = "Interconnect IP and provide communication services"
		self.Description = "See manual"
		self.Issues      = "Not working yet"
		self.Speed       = "?"
		self.Area        = "?"
		
	#----------------------------------------------------------------------
	def GetIO(self):
		"""
		Return signal dictionary of port interface for this NoC.
		"""
		NbRouters = self.Dim['x']*self.Dim['y']
		SigDict={"Resets":[], "Clocks":[], "NetworkInterface":[], "Ports": [], "InFlux":[],"OutFlux":[],}
		FLIT  = HDL.Signal("Data",  "IN", self.FlitWidth, Type="logic", InitVal=0)
		LANES = HDL.Signal("Lanes", "IN", self.VC,        Type="logic", InitVal=0)
			
		if self.FluxCtrl=="HandShake":
			SigDict["Clocks"] = [HDL.Signal("Clock", "IN", 1, "logic", InitVal=0),]
			SigDict["Resets"] = [HDL.Signal("Reset", "IN", 1, "logic", InitVal=1),]
			SigDict["InFlux"] = [
				HDL.Signal("DataIn", "IN", NbRouters, FLIT, InitVal=0),
				HDL.Signal("Rx", "IN", NbRouters, "logic", InitVal=0),
				HDL.Signal("AckRx", "OUT", NbRouters, "logic", InitVal=0),
				]
			SigDict["OutFlux"]  = [
				HDL.Signal("DataOut", "OUT", NbRouters, FLIT, InitVal=0),
				HDL.Signal("Tx", "OUT", NbRouters, "logic", InitVal=0),
				HDL.Signal("AckTx", "IN", NbRouters, "logic", InitVal=0),
				]
			SigDict["NetworkInterface"]  = SigDict["InFlux"]+SigDict["OutFlux"]
			SigDict["Ports"] = []
		elif self.FluxCtrl=="CreditBased":
			SigDict["Clocks"] = [HDL.Signal("Clock", "in", NbRouters, "logic", InitVal=0),]
			SigDict["Resets"] = [HDL.Signal("Reset", "in", 1, "logic", InitVal=1),]
			SigDict["InFlux"]  = [
				HDL.Signal("ClockRx", "in", NbRouters, "logic", InitVal=0),
				HDL.Signal("Rx", "in", NbRouters, "logic", InitVal=0),
				HDL.Signal("DataIn", "in", NbRouters, FLIT, InitVal=0),
				HDL.Signal("CreditOut", "out", NbRouters, LANES, InitVal=0),
				]
			SigDict["OutFlux"]  = [
				HDL.Signal("ClockTx", "out", NbRouters, "logic", InitVal=0),
				HDL.Signal("Tx", "out", NbRouters, "logic", InitVal=0),
				HDL.Signal("DataOut", "out", NbRouters, FLIT, InitVal=0),
				HDL.Signal("CreditIn", "in", NbRouters, LANES, InitVal=0),
				]
			SigDict["Ports"] = []
			if VC>1:
				SigDict["InFlux"] +=[HDL.Signal("LaneRx", "in", NbRouters, LANES, InitVal=0),]
				SigDict["OutFlux"]+=[HDL.Signal("LaneTx", "in", NbRouters, LANES, InitVal=0),]
			SigDict["NetworkInterface"] = SigDict["InFlux"]+SigDict["OutFlux"] 

		return SigDict
		
	#----------------------------------------------------------------------
	def RoutersMap(self):
		"""
		Return a dictionary that represents routers connections.
		"""
		Mapping = {}
		if self.Topology=="Mesh":
			for y in range(0, self.Dim['y']): 
				for x in range(0, self.Dim['x']):
					RName = "Router{0}{1}".format(x, y) # Base Name
					# Establish the router port dictionary
					PortDict = {}
					if x==0:
						if y==0: # "BL"
							PortDict[0]="EAST" 
							PortDict[1]="NORTH" 
							RName+="_BottomLeft"
						elif y==(self.Dim['y']-1):  # "TL"
							PortDict[0]="EAST" 
							PortDict[1]="SOUTH"  
							RName+="_TopLeft"
						else: # "CL"       
							PortDict[0]="EAST" 
							PortDict[1]="NORTH"  
							PortDict[2]="SOUTH"        
							RName+="_CenterLeft" 
					elif x==(self.Dim['x']-1):
						if y==0: # "BR"         
							PortDict[0]="NORTH"
							PortDict[1]="WEST"   
							RName+="_BottomRight"
						elif y==(self.Dim['y']-1): # "TR" 
							PortDict[0]="WEST"
							PortDict[1]="SOUTH"
							RName+="_TopRight"
						else: # "CR"   
							PortDict[0]="NORTH" 
							PortDict[1]="WEST"
							PortDict[2]="SOUTH"  
							RName+="_CenterRight"
					else:
						if y==0: # "BC"
							PortDict[0]="EAST" 
							PortDict[1]="NORTH" 
							PortDict[2]="WEST"
							RName+="_BottomCenter"
						elif y==(self.Dim['y']-1):
							PortDict[0]="EAST" 
							PortDict[1]="WEST"
							PortDict[2]="SOUTH" 
							del PortDict[1] # port NORTH 
							RName+="_TopCenter"
						else:
							PortDict[0]="EAST"
							PortDict[1]="NORTH"
							PortDict[2]="WEST"
							PortDict[3]="SOUTH"
							RName+="_CenterCenter" # "CC"
					
					Pos   = "{0}:{1}".format(x, y)
					try:
						if not isinstance(self.IPDict[Pos], list):
							self.IPDict[Pos]=[]
					except: self.IPDict[Pos]=[]
					Mapping[Pos]=Router(	RName, 
								{'x':x, 'y':y}, # Coordinate dictionary
								PortDict,
								self.FlitWidth, 
								self.FluxCtrl, 
								self.IPDict[Pos], # IP features
								self.VC, 
								self.Routing, 
								self.Scheduling,
								self.InputQDepth,
								self.OutputQDepth
								)
					
		return Mapping
		
	#----------------------------------------------------------------------
	def GenHDL(self, OutputDir):
		"""
		Generate HDL files (VHDL or Verilog) for a NoC with object parameters.
		"""
		SrcList  = []
		# Create the subdirectory for the NoC -------------------------------------------
		if not os.path.isdir(OutputDir):# First create tree if it doesn't already exist
			try: os.makedirs(OutputDir)# Recreate the whole directory tree
			except: 
				logging.error("Directory '{0}' cannot be created: NoC generation aborted.".format(OutputDir))
				return [] # Failure
			logging.info("Directory '{0}' did not already exist : successfully created.".format(OutputDir))
					
		# Map routers -------------------------------------------------------------------
		RMap = self.RoutersMap() # Dictionary of routers
		Content  = ""
		Declarations = ""
		PreviousR = None
		for Pos in sorted(RMap.keys()):
			R=RMap[Pos]
			# Generate HDL files for each type of router-----------------------------
			SrcList.append(R.GenHDL(OutputDir))
			# Prepare instanciation of router component in NoC top ------------------
			Content+=R.Instanciate() # Get code of router instanciation
			Content+=R.Connect(PreviousR) # Connect to other routers and IP(s)
			Declarations+=R.IntSigDeclare()	
			PreviousR = R
			
		# Create the NoC Top ------------------------------------------------------------
		FileName = os.path.join(OutputDir, self.Module+Extension)
		self.WriteFile(FileName, Declarations, Content)
		
		SrcList.append(FileName)
		return SrcList # Success
			
	#----------------------------------------------------------------------
	def __str__(self):
		return "NoC {0}, Dim:{1}, FlitWidth:{2}, VC: {3}".format(Type, Dimensions, FlitWidth, VC)

#==================================================================
# Router Object
#==================================================================
class Router(Comp):
	"""
	Object that can build a entire Router HDL code.
	"""
	#----------------------------------------------------------------------
	def __init__(self, Name, Dims, PortDict, FlitWidth, FluxCtrl, IPList, VC, Routing, Scheduling, InputQDepth, OutputQDepth):
		"""
		Initialize router parameters.
		"""
		Comp.__init__(self, Name, Name)
		self.Dims       = Dims
		self.PortDict   = PortDict
		self.FlitWidth  = FlitWidth
		self.FluxCtrl   = FluxCtrl
		self.VC         = VC
		self.Routing    = Routing
		self.Scheduling = Scheduling
		NbPorts = len(self.PortDict)
		self.NbInputs   = NbPorts
		self.NbOutputs  = NbPorts
		self.InputQDepth  = InputQDepth
		self.OutputQDepth = OutputQDepth
		self.IPList = IPList
		for IP in IPList: # Multi local port
			NbPorts+=1
			self.PortDict[NbPorts]="LOCAL"+str(NbPorts)
		# Get router number
		NumberTemplate=("{0:0"+str(self.FlitWidth/8)+"X}{1:0"+str(self.FlitWidth/8)+"X}")
		self.Num = NumberTemplate.format(self.Dims["x"], self.Dims["y"])
		# Get router address
		AddressTemplate="{0:0"+str(self.FlitWidth/4)+"b}{1:0"+str(self.FlitWidth/4)+"b}"
		self.Addr = AddressTemplate.format(self.Dims["x"], self.Dims["y"])
		# Build Generics list
		self.GenericList.append(["RouterAddr", "unsigned", '"{0}"'.format(self.Addr)])
		self.GenericList.append(["FlitWidth", "unsigned", self.FlitWidth])
		self.GenericList.append(["NbPorts", "unsigned", NbPorts])
		# Build IO dictionary
		self.GetIO()
		
		# Information
		self.Title       = "NoC router ({0})".format(self.Name) 
		self.Purpose     = "Provide communication services."
		self.Description = "See manual"
		self.Issues      = "Not working yet"
		self.Speed       = "?"
		self.Area        = "?"
		
	#----------------------------------------------------------------------
	def GetIO(self):
		"""
		Return signal dictionary of port interface for this router.
		"""
		SigDict=self.IO
		FLIT  = HDL.Signal("Data",  "IN", self.FlitWidth, Type="logic", InitVal=0)
		LANES = HDL.Signal("Lanes", "IN", self.VC,        Type="logic", InitVal=0)
			
		#----------------------
		if self.FluxCtrl=="HandShake":
			self.IO["Clocks"] = [HDL.Signal("Clock", "IN", 1, "logic", InitVal=0),]
			self.IO["Resets"] = [HDL.Signal("Reset", "IN", 1, "logic", InitVal=0),]
			self.IO["InFlux"] = [
				HDL.Signal("DataIn", "IN", self.FlitWidth, "logic", InitVal=0),
				HDL.Signal("Rx", "IN", 1, "logic", InitVal=0),
				HDL.Signal("AckRx", "OUT", 1, "logic", InitVal=0),
				]
			self.IO["OutFlux"]  = [
				HDL.Signal("DataOut", "OUT", self.FlitWidth, "logic", InitVal=0),
				HDL.Signal("Tx", "OUT", 1, "logic", InitVal=0),
				HDL.Signal("AckTx", "IN", 1, "logic", InitVal=0),
				]
			self.IO["NetworkInterface"]  = self.IO["InFlux"]+self.IO["OutFlux"]
		#----------------------
		elif self.FluxCtrl=="CreditBased":
			self.IO["Clocks"] = [HDL.Signal("Clock", "in", 1, "logic"),]
			self.IO["Resets"] = [HDL.Signal("Reset", "in", 1, "logic"),]
			self.IO["InFlux"]  = [
				HDL.Signal("ClockRx", "in", 1, "logic"),
				HDL.Signal("Rx", "in", 1, "logic"),
				HDL.Signal("DataIn", "in", self.FlitWidth, FLIT),
				HDL.Signal("CreditOut", "out", self.VC, "logic"),
				]
			self.IO["OutFlux"]  = [
				HDL.Signal("ClockTx", "out", 1, "logic"),
				HDL.Signal("Tx", "out", 1, "logic"),
				HDL.Signal("DataOut", "out", self.FlitWidth, "logic"),
				HDL.Signal("CreditIn", "in", self.VC, "logic"),
				]
			self.IO["Ports"] = []
			if VC>1:
				self.IO["InFlux"] +=[HDL.Signal("LaneRx","in",self.VC, "logic"),]
				self.IO["OutFlux"]+=[HDL.Signal("LaneTx","in",self.VC, "logic"),]
			self.IO["NetworkInterface"] = self.IO["InFlux"]+self.IO["OutFlux"] 

		return self.IO
		
	#----------------------------------------------------------------------
	def GenHDL(self, OutputDir):
		"""
		Create source files in HDL code for this router in specified directory.
		return package data used (as a dictionary).
		"""	
		SrcList = []
		Declarations = ""
		Content      = ""
		ComponentList = []
		# Instanciate InputQ, Mux, OutputQ and generate HDL--------------------------
		for I in range(0, self.NbInputs):
			InQ = InputSide(Queue(	"InputQ_{0}".format(I), 
						self.FlitWidth, 
						self.InputQDepth, 
						self.FluxCtrl, 
						self.NbInputs, 
						self.VC))
			ComponentList.append(InQ)
		for O in range(0, self.NbOutputs):
			OutQ = OutputSide(Queue("OutputQ_{0}".format(O), 
						self.FlitWidth, 
						self.OutputQDepth, 
						self.FluxCtrl, 
						self.NbOutputs, 
						self.VC))
			ComponentList.append(OutQ)
		ComponentList.append(Scheduler("Scheduler", self.Routing, self.NbInputs, self.NbOutputs))
		ComponentList.append(Switch("Switch", self.FlitWidth, self.NbInputs, self.NbOutputs))
		# Instanciate Input/Output queue, Scheduler, Switch (Cross bar) + generate HDL
		for Component in ComponentList:
			SrcList.append(Component.GenHDL(OutputDir))
			Content+=Component.Instanciate()
			Content+=self.Connect(Component)
			Declarations+=Component.IntSigDeclare()
			
		# Create the Router Top file -----------------------------------------------
		FileName = os.path.join(OutputDir, self.Module+Extension)
		self.WriteFile(FileName, Declarations, Content)
		
		SrcList.append(FileName)
			
		return SrcList
		
	#----------------------------------------------------------------------
	def Connect(self, Component):
		return ""
			
	#----------------------------------------------------------------------
	def __str__(self):
		Ports = ", ".join([self.PortDict[x] for x in sorted(self.PortDict.keys())])
		Ports += ", ".join([x.Name for x in self.IPList])
		Dims  = " ".join(["{0}={1}".format(x,self.Dims[x]) for x in sorted(self.Dims.keys())])
		return "Router: {0} Ports({1})".format(Dims, Ports)
		
		
#==================================================================
# Scheduler Object
#==================================================================
class Scheduler(Comp):
	"""
	Object that can build a entire Routing Scheduler HDL code.
	"""
	#----------------------------------------------------------------------
	def __init__(self, Name, Routing, NbInputs, NbOutputs):
		"""
		Initialize Scheduler parameters.
		"""
		Comp.__init__(self, Name, "Scheduler")
		self.Name      = Name
		self.Routing   = Routing
		self.NbInputs  = NbInputs
		self.NbOutputs = NbOutputs
		# Build Generics list
		self.GenericList.append(["NbInputs",  "unsigned", NbInputs])
		self.GenericList.append(["NbOutputs", "unsigned", NbOutputs])
		# Build ports dictionary
		self.GetIO()
		
		# Information
		self.Title       = "Router Scheduler" 
		self.Purpose     = "Process the routing algorithm."
		self.Description = "See manual"
		self.Issues      = "Not working yet"
		self.Speed       = "?"
		self.Area        = "?"
		
	#----------------------------------------------------------------------
	def GetIO(self):
		"""
		Return signal list of port interface for this Scheduler.
		"""		
		TargetPort  = HDL.Signal("TargetPort","IN", self.NbOutputs, "numeric", 0)
		self.IO["Resets"] = [HDL.Signal("RST", "IN", 1),]
		self.IO["Clocks"] = [HDL.Signal("CLK", "IN", 1),]
		self.IO["In"] = [
				HDL.Signal("SOP", "IN", self.NbInputs, "logic", 0),
				HDL.Signal("EOP", "IN", self.NbInputs, "logic", 0),
				HDL.Signal("TargetPorts","IN",  self.NbInputs, TargetPort, 0), ]
		self.IO["Out"] = [
				HDL.Signal("InputSel",   "OUT", self.NbInputs), 
				HDL.Signal("OutputSel",  "OUT", self.NbOutputs), ]
		
		return self.IO
		
	#----------------------------------------------------------------------
	def GenHDL(self, OutputDir):
		"""
		Create source files in HDL code for this Scheduler in specified directory.
		return package data used (as a dictionary).
		"""	
		SrcList  = []
		# Create the Scheduler Top file -----------------------------------------------
		SchedFileName = os.path.join(OutputDir, "Scheduler"+Extension)
		Comments=""
		# generate code in HDL file ---------------------------------------------------
		SignalList = []
		Declarations = ""
		Content = ""
		# All signals (Name, Size, Type, InitVal)
		SOP         = self["SOP"]
		EOP         = self["EOP"]
		InputSel    = self["InputSel"]
		OutputSel   = self["OutputSel"]
		TargetPorts = self["TargetPorts"]
		Pointer     = self.IntSigNew("InPointer_i", self.NbInputs, "numeric", 0) 
		# InPointer_i, InputSel_i driver and OutputSel_i driver
		Drivers    = ""
		#for Num in range(0, self.NbInputs):
			#PortDrivers.append("TargetPorts({0})".format(Num), HDL.Equals("Pointer_i",str(Num)))
		#PortDrivers.append(HDL.LogicValue(0, Size=self.PortList[3].Size), None) # Default
		Content+=Pointer.Connect(Drivers)
		
		# Requester counter process ------------------------------------------------------
		IfOverflow = HDL.If([	[HDL.Equals("Pointer_i", str(self.NbInputs-1)), 
						Pointer.Connect(str(0))],  
					[None, 
						Pointer.Connect("Pointer_i+1")]],
					Comments="")
		IfResetSync = HDL.If([	[HDL.Equals("RST", HDL.LogicValue(1, BitLength=1)), 
						Pointer.Connect(Pointer.Val(0))],	
					[HDL.SyncCondition("CLK"), 
						IfOverflow]],
					Comments="")
		
		Content+=HDL.Process("ReqCnt", ["RST","CLK"], [], IfResetSync, Comments="Requester counter process")
		# Connection table ----------------------------------------------------------------
		# Ins  = HDL.Signal("InConnected_i",  None, self.NbInputs,  "logic", 0)
		# Outs = HDL.Signal("OutConnected_i", None, self.NbOutputs, "logic", 0)
		
		# SignalList.append(Ins.Parameters()) 
		# SignalList.append(Outs.Parameters()) 
		
		# for i in range(0, self.NbInputs):
		# 	Content+=HDL.Connect(	Ins[i],
		# 				[Ins[i].Val(0), self["EOP"]==self["EOP"].Val(1)],
		# 				[Ins[i].Val(1), self["EOP"]==self["EOP"].Val(0)],)
		# for i in range(0, self.NbOutputs):
		# 	Content+=HDL.Connect(Outs[i], [Outs[i].Val(0), ])
		
		# InputSel and OutputSel connection -----------------------------------------------
		# Content+=HDL.Connect("InputSel",  "InConnected_i")
		# Content+=HDL.Connect("OutputSel", "OutConnected_i")
		FileName = os.path.join(OutputDir, self.Module+Extension)
		self.WriteFile(FileName, Declarations, Content)
		
		SrcList.append(FileName)
						
		
	#----------------------------------------------------------------------
	def Connect(self, Component):
		if isinstance(Component, int): raise TypeError("euh...")
		return ""
		
	#----------------------------------------------------------------------
	def __str__(self):
		return self.Name
		
		
#==================================================================
# Switch Object 
#==================================================================
class Switch(Comp):
	"""
	Object that can build a entire Switch HDL code.
	!!! Crossbar must be replaced by MUX for large design !!!
	"""
	#----------------------------------------------------------------------
	def __init__(self, Name, FlitWidth, NbInputs, NbOutputs):
		"""
		Initialize Switch parameters.
		"""
		Comp.__init__(self, Name, "Switch")
		self.FlitWidth = FlitWidth
		self.NbInputs  = NbInputs
		self.NbOutputs = NbOutputs
		# Build Generics list
		self.GenericList=[]
		# Build ports dictionary
		self.GetIO()
		
		# Information
		self.Title       = "Switch for routers" 
		self.Purpose     = "Connect inputs to specified outputs."
		self.Description = "See manual"
		self.Issues      = "Not working yet"
		self.Speed       = "?"
		self.Area        = "?"
		
	#----------------------------------------------------------------------
	def GetIO(self):
		"""
		Return signal list of port interface for this Scheduler.
		"""
		Data = HDL.Signal("Data",  "IN", self.FlitWidth, "logic", 0)
		self.IO["In"]= [HDL.Signal("DataIn",  "IN",  self.NbInputs, Data),  
				HDL.Signal("Connections",  "IN", math.ceil(math.log(self.NbInputs, 2))),]
		self.IO["Out"]= [
				HDL.Signal("DataOut", "OUT", self.NbOutputs, Data),]
		
		return self.IO
		
	#----------------------------------------------------------------------
	def GenHDL(self, OutputDir):
		"""
		Create source files in HDL code for this Switch in specified directory.
		return package data used (as a dictionary).
		"""
		SrcList  = []
		Content=""
		Declarations=""	
		
		# Instanciate Muxs and generate HDL------------------------------------------
		Content += HDL.For("Mux", 'i', 0, "DataOut'length-1", self["DataOut"]['i'].Connect("DataIn(to_integer(Connections(i)))"), Comments="Connect input if corresponding connection pin is set.")
		
		# Create the OutputQ Top file -----------------------------------------------
		self.WriteFile(os.path.join(OutputDir, self.Module+Extension), Declarations, Content)
		
	#----------------------------------------------------------------------
	def Connect(self, Component):
		if isinstance(Component, int): raise TypeError("euh...")
		return ""
						
	#----------------------------------------------------------------------
	def __str__(self):
		return self.Name	
						
#==================================================================
# Queue Object
#==================================================================
class Queue(Comp):
	"""
	Object that can build a entire Queue HDL code.
	"""
	#----------------------------------------------------------------------
	def __init__(self, Name, FlitWidth, QDepth, FluxCtrl, NbInputs, VC):
		"""
		Initialize InQueue parameters.
		"""
		Comp.__init__(self, Name, "InputQ")
		self.FlitWidth = FlitWidth
		self.QDepth    = QDepth
		self.FlitWidth = FlitWidth
		self.NbInputs  = NbInputs
		self.FluxCtrl  = FluxCtrl
		self.VC        = VC
		# Build Generics list
		self.GenericList.append(["FlitWidth", "unsigned", self.FlitWidth])
		self.GenericList.append(["QDepth",    "unsigned", self.QDepth])
		self.GenericList.append(["NbInputs",  "unsigned", self.NbInputs])
		# Build ports dictionary
		self.GetIO()
		
		# Information
		self.Title       = self.Name+" for router buffering" 
		self.Purpose     = "Provide buffering to input/output."
		self.Description = "See manual"
		self.Issues      = "Not working yet"
		self.Speed       = "?"
		self.Area        = "?"
		
	#----------------------------------------------------------------------
	def GetIO(self):
		"""
		Return signal list of port interface for this input queue.
		"""
		self.IO = FluxCtrlSignals(self.FluxCtrl, self.VC, self.NbInputs)
		return self.IO
		
	#----------------------------------------------------------------------
	def GenHDL(self, OutputDir):
		"""
		Create source files in HDL code for this InputQ in specified directory.
		return package data used (as a dictionary).
		"""
		SrcList  = []	
		
		Content=""
		Declarations=""
		# Instanciate input Network Adapter + generate HDL
		NetAdapt = NA(self.Name, self.FlitWidth, self.FluxCtrl, self.VC)
		ComponentList = [InputNA(NetAdapt), FIFO("FIFO", self.FlitWidth, self.QDepth, self.FluxCtrl, self.VC)]
		for Component in ComponentList:
			Component.GenHDL(OutputDir)
			Content+=Component.Instanciate()
			Declarations+=Component.IntSigDeclare()
		
		# Create the InputQ Top file -----------------------------------------------
		Comments="InputQ - Flit width:{0}, Virtual Channels: {1}".format(self.FlitWidth, self.VC)
		FileName = os.path.join(OutputDir, self.Module+Extension)
		self.WriteFile(FileName, Declarations, Content)
		
		SrcList.append(FileName)
		
	#----------------------------------------------------------------------
	def Connect(self, Component):
		if isinstance(Component, int): raise TypeError("euh...")
		return ""
			
	#----------------------------------------------------------------------
	def __str__(self):
		return self.Name
		
#==================================================================
# InputSide Object
#==================================================================
class InputSide(Queue):
	"""
	Pattern decorator object for InputQ HDL code.
	"""
	#----------------------------------------------------------------------
	def __init__(self, Q):
		Queue.__init__(self, Q.Name, Q.FlitWidth, Q.QDepth, Q.FluxCtrl, Q.NbInputs, Q.VC)
		pass
#==================================================================
# OutputSide Object
#==================================================================
class OutputSide(Queue):
	"""
	Pattern decorator object for OutputQ HDL code.
	"""
	#----------------------------------------------------------------------
	def __init__(self, Q):
		Queue.__init__(self, Q.Name, Q.FlitWidth, Q.QDepth, Q.FluxCtrl, Q.NbInputs, Q.VC)
		pass
		
		
#==================================================================
# Network adapter Object
#==================================================================
class NA(Comp):
	"""
	Object that can build a entire Network adapter HDL code.
	"""
	#----------------------------------------------------------------------
	def __init__(self, Name, FlitWidth, FluxCtrl, VC):
		"""
		Initialize Network adapter parameters.
		"""
		Comp.__init__(self, Name, "NA")
		self.Name         = Name
		self.FlitWidth    = FlitWidth
		self.FluxCtrl     = FluxCtrl
		self.VC           = VC
		# Build Generics list
		self.GenericList.append(["FlitWidth", "unsigned", self.FlitWidth])
		# Build ports dictionary
		self.GetIO()
		
		# Information
		self.Title       = "Network Adapter" 
		self.Purpose     = "Provide asynchronous interface for communication."
		self.Description = "See manual"
		self.Issues      = "Not working yet"
		self.Speed       = "?"
		self.Area        = "?"
		
	#----------------------------------------------------------------------
	def GetIO(self):
		"""
		Return signal list of port interface for this Network adapter.
		"""
		return self.IO
		
	#----------------------------------------------------------------------
	def GenHDL(self, OutputDir):
		"""
		Create source files in HDL code for this Network adapter in specified directory.
		return package data used (as a dictionary).
		"""
		SrcList = []			
		Content=""
		Declarations=""
		# Instanciate Output Network Adapter + generate HDL
		ComponentList = [] # TODO
		for Component in ComponentList:
			Component.GenHDL(OutputDir)
			Content+=Component.Instanciate()
			Declarations+=Component.IntSigDeclare()
			
		# Create the OutputQ Top file -----------------------------------------------
		FileName = os.path.join(OutputDir, self.Module+Extension)
		self.WriteFile(FileName, Declarations, Content)
		
		SrcList.append(FileName)
		
	#----------------------------------------------------------------------
	def Connect(self, Component):
		if isinstance(Component, int): raise TypeError("euh...")
		return ""
			
	#----------------------------------------------------------------------
	def __str__(self):
		return self.Name
		
#==================================================================
# FIFO Object
#==================================================================
class InputNA(NA):
	"""
	Pattern decorator object for InputNA HDL code.
	"""
	#----------------------------------------------------------------------
	def __init__(self, NetworkA):
		NA.__init__(self, NetworkA.Name, NetworkA.FlitWidth, NetworkA.FluxCtrl, NetworkA.VC)
		pass
		
#==================================================================
# FIFO Object
#==================================================================
class OutputNA(NA):
	"""
	Pattern decorator object for OutputNA HDL code.
	"""
	#----------------------------------------------------------------------
	def __init__(self, NetworkAdapter):
		NA.__init__(self, NetworkA.Name, NetworkA.FlitWidth, NetworkA.FluxCtrl, NetworkA.VC)
		pass
		
	
#==================================================================
# FIFO Object
#==================================================================
class FIFO(Comp):
	"""
	Object that can build a entire FIFO HDL code.
	"""
	#----------------------------------------------------------------------
	def __init__(self, Name, FlitWidth, FluxCtrl, Depth, VC):
		"""
		Initialize FIFO parameters.
		"""
		Comp.__init__(self, Name, "NA")
		self.Name         = Name
		self.FlitWidth    = FlitWidth
		self.FluxCtrl     = FluxCtrl
		self.VC           = VC
		self.Depth        = Depth
		# Build Generics list
		self.GenericList.append(["FlitWidth", "unsigned", self.FlitWidth])
		self.GenericList.append(["Depth", "unsigned", self.Depth])
		# Build ports dictionary
		self.GetIO()
		
		# Information
		self.Title       = "FIFO" 
		self.Purpose     = "Provide memory buffers for router queues."
		self.Description = "See manual"
		self.Issues      = "Not working yet"
		self.Speed       = "?"
		self.Area        = "?"
		
	#----------------------------------------------------------------------
	def GetIO(self):
		"""
		Return signal list of port interface for this FIFO.
		"""
		return self.IO
		
	#----------------------------------------------------------------------
	def GenHDL(self, OutputDir):
		"""
		Create source files in HDL code for this FIFO in specified directory.
		return package data used (as a dictionary).
		"""
		SrcList  = []	
		Content=""
		Declarations=""	 
		
		# Create the OutputQ Top file -----------------------------------------------
		FileName = os.path.join(OutputDir, self.Module+Extension)
		self.WriteFile(FileName, Declarations, Content)
		
		SrcList.append(FileName)
			
	#----------------------------------------------------------------------
	def __str__(self):
		return self.Name
	
#==================================================================
# NoC Signals
#==================================================================
def FluxCtrlSignals(FluxCtrl, VC, NbPorts):
	"""
	Return a dictionary of signals for specified NoC port depending on specified flux control. 
	"""
	SigDict={"InFlux":[],"OutFlux":[],}
	if FluxCtrl=="HandShake": 
		# Base signals----------
		DATA = HDL.Signal("data", "IN", 1, "logic", 0)
		CTRL = HDL.Signal("ctrl", "IN", 1, "logic", 0)
		#-----------------------
		SigDict["Clocks"]=[HDL.Signal("clock", "IN", 1, "logic", 0),]
		SigDict["Resets"]=[HDL.Signal("reset", "IN", 1, "logic", 0),]
		SigDict["InFlux"]=[
			HDL.Signal("data_in", "IN", NbPorts, "arrayNport_regflit"),
			HDL.Signal("rx", "IN", NbPorts, "regNport"),
			HDL.Signal("ack_rx", "OUT", NbPorts, "regNport"),
			]
		SigDict["OutFlux"]=[
			HDL.Signal("data_out", "OUT", NbPorts, "arrayNport_regflit"),
			HDL.Signal("tx", "OUT", NbPorts, "regNport"),
			HDL.Signal("ack_tx", "IN", NbPorts, "regNport"),
			]
		SigDict["NetworkInterface"]=SigDict["InFlux"]+SigDict["OutFlux"]
		#-----------------------
		
	elif  FluxCtrl=="CreditBased":
		#-----------------------
		SigDict["Clocks"]=[HDL.Signal("clock", "IN", 1, "logic", 0),]
		SigDict["Resets"]=[HDL.Signal("reset", "IN", 1, "logic", 0),]
		SigDict["InFlux"]=[
			HDL.Signal("clock_rx", "IN", NbPorts, "regNport"),
			HDL.Signal("rx", "IN", NbPorts, "regNport"),
			HDL.Signal("data_in", "IN", NbPorts, "arrayNport_regflit"),
			]
		if VC>1:
			SigDict["OutFlux"].append(
					HDL.Signal("credit_i","IN",NbPorts,"arrayNport_regNlane"))
			SigDict["OutFlux"].append(
					HDL.Signal("lane_tx","IN",NbPorts,"arrayNport_regNlane"))
		else:
			SigDict["OutFlux"].append(HDL.Signal("credit_i", "IN", NbPorts, "regNport"))
			
		SigDict["OutFlux"]=[
			HDL.Signal("clock_tx", "IN", NbPorts, "regNport"),
			HDL.Signal("tx", "IN", NbPorts, "regNport"),
			HDL.Signal("data_out", "IN", NbPorts, "arrayNport_regflit"), 
			]
			
		if VC>1:
			SigDict["InFlux"].append(
					HDL.Signal("credit_o","OUT",NbPorts,"arrayNport_regNlane"))
			SigDict["InFlux"].append(
					HDL.Signal("lane_rx","IN",NbPorts,"arrayNport_regNlane"))
		else:
			SigDict["InFlux"].append(HDL.Signal("credit_o", "OUT", NbPorts, "regNport"))
			
		SigDict["NetworkInterface"]=SigDict["InFlux"]+SigDict["OutFlux"]
		#-----------------------
			
	return SigDict
	
#==================================================================
# Return NoC IOs
#==================================================================
def GetIO(Type="HERMES", FlitWidth=16, FluxCtrl="HandShake", Dimensions="4x4", VC=1):
	NbRouters = GetNbRouters(Dimensions)
	SigDict={"Resets":[], "Clocks":[], "NetworkInterface":[], "Ports": [], "InFlux":[],"OutFlux":[],}
	if Type=="HERMES":	
		if FluxCtrl=="HandShake":
			SigDict["Clocks"] = [	
				["Clock", "in", 1, "std_logic"],
				]
			SigDict["Resets"] = [
				["Reset", "in", 1, "std_logic"],
				]
			SigDict["InFlux"]  = [
				["DataIn", "in", NbRouters, "arrayNrot_regflit"],
				["Rx", "in", NbRouters, "regNrot"],
				["AckRx", "out", NbRouters, "regNrot"],
				]
			SigDict["OutFlux"]  = [
				["DataOut", "out", NbRouters, "arrayNrot_regflit"],
				["Tx", "out", NbRouters, "regNrot"],
				["AckTx", "in", NbRouters, "regNrot"],
				]
			SigDict["NetworkInterface"]  = SigDict["InFlux"]+SigDict["OutFlux"]
			SigDict["Ports"] = [
				]
		elif FluxCtrl=="CreditBased":
			SigDict["Clocks"] = [	
				["Clock", "in", NbRouters, "regNrot"],
				]
			SigDict["Resets"] = [
				["Reset", "in", 1, "std_logic"],
				]
			SigDict["InFlux"]  = [
				["ClockRx", "in", NbRouters, "regNrot"],
				["Rx", "in", NbRouters, "regNrot"],
				["DataIn", "in", NbRouters, "arrayNrot_regflit"],
				["CreditOut", "out", NbRouters, "arrayNrot_regNlane"],
				]
			SigDict["OutFlux"]  = [
				["ClockTx", "out", NbRouters, "regNrot"],
				["Tx", "out", NbRouters, "regNrot"],
				["DataOut", "out", NbRouters, "arrayNrot_regflit"],
				["CreditIn", "in", NbRouters, "arrayNrot_regNlane"],
				]
			SigDict["Ports"] = [
				]
			if VC>1:
				SigDict["InFlux"]+=[
					["LaneRx", "in", NbRouters, "arrayNrot_regNlane"],
					]
				SigDict["OutFlux"]+=[
					["LaneTx", "in", NbRouters, "arrayNrot_regNlane"],
					]
			SigDict["NetworkInterface"]  = SigDict["InFlux"]+SigDict["OutFlux"] 

	return SigDict
#==================================================================
# Generate NoC top HDL file content.
#==================================================================
def GenerateTop(Type, FluxCtrl, Dimensions, FlitWidth, VC):
	"""
	Return the code for specified NoC top.
	"""
	Code=""
	# Generate the libraries----------------------------------------------------------------
	Code+=HDL.Libraries(GetLibraries(Type))
	# Generate the Packages-----------------------------------------------------------------
	Code+=HDL.Packages(GetPackages(Type))
	# Generate the NoC entity---------------------------------------------------------------
	Comments="NoC {0}, Dimensions:{1}, Flit width:{2}, Virtual Channels: {3}".format(Type, Dimensions, FlitWidth, VC)

	NoCSigDict = GetIO(Type, FlitWidth, FluxCtrl, Dimensions)
	Code+=HDL.Entity("NoC", NoCSigDict["Resets"]+NoCSigDict["Clocks"]+NoCSigDict["NetworkInterface"]+NoCSigDict["Ports"], Comments)

	# Generate the architecture--------------------------------------------------------------
	NumberTemplate=("{0:0"+str(FlitWidth/8)+"X}{1:0"+str(FlitWidth/8)+"X}") # Router number
	# Get integers for xDim and yDim
	xDim, yDim = list(map(int, Dimensions.lower().split('x')))
	# List signals to declare in architecture (Network interface signals => One for each router)
#	ConnectorLists=[]
#	
#	for SName, SIO, SSize, SType in NoCSigDict["NetworkInterface"]:
#		ConnectorList=[]
#		for y in range(0, yDim): # First y (for each line)
#			for x in range(0, xDim):
#				ID = "N"+NumberTemplate.format(x,y)
#				# For each router define these signals :
#				SignalList.append(SName+ID)
#		# Build the signals list for declaration
#		RouterType=SType.replace("regNrot", "regNport").replace("arrayNrot", "arrayNport")
#		ConnectorLists.append([SignalList, RouterType, None,])
			
	# List instances to add to the architecture
	Instances = []
	ConnectorList=[]
	# Get the routers to instanciate
	RouterList = Routers(Dimensions, FlitWidth)
	RouterSigDict = GetRouterNI(Type, FluxCtrl, VC, 5)
	for RouterNum, RouterAddr, RouterType in RouterList:
		SignalDict={}
		# Port map association for Routers
		for Index in range(0, len(NoCSigDict["Clocks"])):
	 		SignalDict[RouterSigDict["Clocks"][Index][0]] = NoCSigDict["Clocks"][Index][0]
	 		
		for Index in range(0, len(NoCSigDict["Resets"])):
	 		SignalDict[RouterSigDict["Resets"][Index][0]] = NoCSigDict["Resets"][Index][0]
			
		for Index in range(0, len(NoCSigDict["NetworkInterface"])):
			Connector = NoCSigDict["NetworkInterface"][Index][0]+'N'+RouterNum
	 		SignalDict[RouterSigDict["NetworkInterface"][Index][0]] = Connector
			ConnectorList.append([Connector, RouterSigDict["NetworkInterface"][Index][3], None])
 		
			
	 	# Router generics :
		GenericDict={"address": "ADDRESSN"+RouterNum,}
	 	# Router comment :
		Comment="Router : Number={0}, Addr={1}, Type={2}, VC={3}, FlitW={4}".format(RouterNum, RouterAddr, RouterType, VC, FlitWidth) 
		# Instance code :
		Instances.append(["Router"+RouterNum, "Router"+RouterType, None, SignalDict, GenericDict, Comment])
	# Get the code for signals to connect
	Content=ConnectRouters(Type, FluxCtrl, VC, 5, FlitWidth, Dimensions, RouterList) # Return the code for routers connections
	# Insert it into the architecture code :
	Code+=HDL.Architecture("v0", "NoC", ConnectorList, Instances, Content, Comments="Version 0 - built by YANGO from ADACSYS.")
	
	return Code
		
#==================================================================
# Generate the code for routers connections
#==================================================================
def ConnectRouters(Type, FluxCtrl, VC, NbPorts, FlitWidth, Dimensions, RouterList):
	"""
	Return the code for routers connections
	"""
	Code=""
	xDim, xDim = list(map(int, Dimensions.split('x')))
	NRouter  = xDim*xDim	
	NoCSigDict = GetIO(Type, FlitWidth, FluxCtrl, Dimensions, VC)
	for RouterNum, RouterAddr, RouterType in RouterList:
		Code+="\n-- ROUTER "+RouterNum+" ({0})".format(RouterType)
		PortDict={"EAST":0, "WEST":1, "NORTH":2, "SOUTH":3, "LOCAL":4}
		Rules={1:0, 0:1, 2:3, 3:2} # Port association
		ConnectDict={"BL":[0,2], "BC":[0,1,2], "BR":[1,2], "CL":[0,2,3], "CC":[0,1,2,3], "CR":[1,2,3], "TL":[0,3], "TC":[0,1,3], "TR":[1,3]} # Dictionnaire des connections
		
		for RouterPort in list(PortDict.keys()):
			Code+="\n-- {0} port (Addr:{1}, type:{2})".format(RouterPort, RouterAddr, RouterType)
			if ConnectDict[RouterType].count(PortDict[RouterPort]) and RouterNum<(NRouter-1):
				# For each router signal of the input control flux
				print("Router '{0}', Port:'{1}', Influx:{2}, NetworkInterface:{3}".format(RouterType, RouterPort, len(NoCSigDict["InFlux"]), len(NoCSigDict["NetworkInterface"])))
				for Sig, IO, Size, Type in NoCSigDict["InFlux"]:
					# Router1 => Router2
					Code+=HDL.Connect(
						"{0}N{1}({2})".format(Sig,RouterNum,PortDict[RouterPort]),
						"{0}N".format(Sig)+GetNextRouter(RouterNum, x=True, y=False)+"({0})".format(Rules[PortDict[RouterPort]]))
			elif PortDict[RouterPort]==4: # LOCAL
				# For each router signal
				for Sig, IO, Size, Type in NoCSigDict["NetworkInterface"]:
					# IP => Router and Router => IP
					if IO.upper()=="IN": Code+=HDL.Connect("{0}N{1}({2})".format(Sig,RouterNum,4), "{0}(N{1})".format(Sig,RouterNum))
					else: Code+=HDL.Connect("{0}(N{1})".format(Sig,RouterNum), "{0}N{1}({2})".format(Sig,RouterNum,4))
			else: # no connection
				# For each router signal of the input control flux
				for Sig, IO, Size, Type in NoCSigDict["InFlux"]:
					if Type.startswith("array"): Length=5 # Length > 1
					else: Length=1
					Code+=HDL.Connect(
						"{0}N{1}({2})".format(Sig,RouterNum,PortDict[RouterPort]), None, Length)
	return Code
						
#==================================================================
# Get next router name in x or y direction from actual router name
#==================================================================
def GetNextRouter(RouterName, x=False, y=False):
	"""
	Get next router name on x or y direction.
	"""	
	# Find current x, y from name
	NbChar = len(RouterName)/2
	Template="{0:0"+str(NbChar)+"X}"
	if x and not y: return Template.format(int(RouterName[:NbChar], 16)+1)+RouterName[NbChar:]
	elif y and not x: return RouterName[:NbChar]+Template.format(int(RouterName[NbChar:], 16)+1)
	elif x and y:  return Template.format(int(RouterName[:NbChar], 16)+1)+Template.format(int(RouterName[NbChar:], 16)+1)
	else: return RouterName

#==================================================================
# Generate common NoC package content.
#==================================================================
def CommonPackage(LibPath, OutputPath, Type="HERMES", Dimensions="3x3", FlitWidth=16, BufferDepth=32, VC=1):
	"""
	Return the code of a package.
	Router name is made up of 3 parts: 'N' + x coordinate(hexa) + y coordinate(hexa)
	For example, a dimension 2x2 will produce routers N0000, N0100, N0001 and N0101.
	"""	
	if Language=="VHDL":
		# Define constants
		ConstCode=""
		#AddressTemplate="{0:0"+str(FlitWidth/4)+"b}{1:0"+str(FlitWidth/4)+"b}"
		RouterList = Routers(Dimensions=Dimensions, FlitWidth=FlitWidth)
		# First declare the constants (two for each router: Number and address)
		for RouterNum, RouterAddr, RouterType in RouterList:
			x, y = int(RouterNum[-FlitWidth/4:-FlitWidth/8], 16), int(RouterNum[-FlitWidth/8:], 16)
			ConstCode+=HDL.Indent(HDL.Constant('N'+RouterNum, "integer", RouterList.index([RouterNum, RouterAddr, RouterType])))
			ConstCode+=HDL.Indent(HDL.Constant("ADDRESSN"+RouterNum, "std_logic_vector({0} downto 0)".format(FlitWidth/2-1), '"{0}"'.format(RouterAddr)))
		ConstCode+='\n'
		
		# Then declare the lanes for virtual channels
		VCCode=""
		for Lane in range(0, VC):
			VCCode+=HDL.Indent(HDL.Constant("L"+str(Lane+1), "integer", Lane))
			# TODO: Ajout condition pour isSR4 (Source Routing - HERMES_SR)
	else:
		ConstCode = ""
		VCCode = ""
	# Build the dictionary for value replacement in template
	# Get integers for xDim and yDim
	xDim, yDim = list(map(int, Dimensions.lower().split('x')))
	Dictionary={
		"n_nodos": ConstCode,
		"n_lanes": VCCode,
		"n_port": str(5),
		"tam_line": str(int(FlitWidth/4)),
		"flit_size": str(FlitWidth),
		"buff_depth": str(BufferDepth),
		"pointer_size": str(int(math.ceil(math.log(BufferDepth, 2)))),
		"n_rot": str(xDim*yDim),
		"max_X": str(xDim-1),
		"max_Y": str(yDim-1),
		"n_lane": str(VC),
		"buff1":"--",
		"buff2":"--",
		}
	# Get template and add constant + subsitute key by value
	TemplatePath = os.path.join(LibPath, "Hermes_package"+ExtensionDict[Language])
	OutputPath   = os.path.join(OutputPath, Type.capitalize()+"_package"+ExtensionDict[Language])
	TemplateEdit(TemplatePath, OutputPath, Dictionary)
	return OutputPath

#==================================================================
# Create a new file which replace each template token by its value given in 'Dictionary'.
#==================================================================
def TemplateEdit(InFilePath, OutFilePath, Dictionary):
	"""
	Create a new file 'OutFilePath' (replace it if it already exist) 
	which replace each token in 'InFilePath' by its value given in 'Dictionary'.
	"""	
	# Check arguments------------------------------------
	if not os.path.isfile(InFilePath):
		logging.error("Input '{0}' is not a correct file: Template edition aborted.".format(InFilePath))
		return None
	if not os.path.isdir(os.path.dirname(OutFilePath)):
		logging.error("Output '{0}' directory does not exist: Template edition aborted.".format(os.path.dirname(OutFilePath)))
		return None
	if not isinstance(Dictionary, dict):
		logging.error("Dictionary argument is not a 'dict' object : Template edition aborted.")
		return None
	# Start editing template-----------------------------
	Text=""
	with open(InFilePath, 'r') as TEMPLATE:
		TemplateText = TEMPLATE.read()
		for Key in list(Dictionary.keys()):
			# Substitue key by its value
			#TemplateText=re.sub("${0}$".format(Key), Dictionary[Key], TemplateText, re.IGNORECASE)
			TemplateText=TemplateText.replace("${0}$".format(Key), Dictionary[Key])
	# Now write the final text to OutFilePath------------
	with open(OutFilePath, 'w+') as OUTFILE:
		OUTFILE.write(TemplateText)
	return TemplateText
		
#==================================================================
# Search in directory tree for specified file, and copy it to output folder.
#==================================================================
def SearchAndCopy(InputDirectory, OutputFolder, KeyWords):
	"""
	Search recursively into InputDirectory folders.
	Then copy the file that match key words in 'KeyWords' to OutputFolder.
	"""	
	SourceList=[]	
	for Root, Dirs, Files in os.walk(InputDirectory):
		for FileName in [x for x in Files if not x.startswith('.')]:
			for KeyWord in KeyWords:
				# Copy source files
				if re.match(r'.*{0}.*'.format(KeyWord), FileName.lower()):
					if FileName.endswith(".vhd") or FileName.endswith(".v"):
						CopyFile(os.path.join(Root, FileName), OutputFolder)
						SourceList.append(os.path.join(OutputFolder, FileName))
						
						logging.info("File '{0}' copied to '{1}'.".format(FileName, OutputFolder))	
	return SourceList
	
#==================================================================
# Main tests.
#==================================================================
if __name__ == "__main__":
#	print("NoC 3x3 router list:", Routers(NoCDimension="3x3"))
#	
#	print("Next router name of 0000 in x dir:", GetNextRouter("0000", x=True, y=False))
#	print("Next router name of 0305 in x dir:", GetNextRouter("0305", x=True, y=False))
#	print("Next router name of 0206 in y dir:", GetNextRouter("0206", x=False, y=True))
#	print("Next router name of 0709 in y dir:", GetNextRouter("0709", x=False, y=True))
#	print("Next router name of 0E0E in x and y dir:", GetNextRouter("0E0E", x=True, y=True))
	
	MyNoC = NoC()
	MyNoC.GenHDL("./NoC_Test")
	
	
	
	
	
	
