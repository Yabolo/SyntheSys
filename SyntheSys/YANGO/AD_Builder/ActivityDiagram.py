#!/usr/bin/env python

import logging, sys, os
import gtk
import gnomecanvas
from random import random

sys.path.append(os.path.abspath("../../"))
from pyInterCo import LibManager

IcoLib, IcoServ = "./Lib.png", "./Service.png"

WidgetLibrary={
	"InitialNode":{"Color":"red", "Shape":"GnomeCanvasEllipse", "Dimensions":[0,30,0,30]},
	"FinalNode":{"Color":"black", "Shape":"GnomeCanvasEllipse", "Dimensions":[0,30,0,30]},
	"Activity":{"Color":"green", "Shape":"GnomeCanvasRect", "Dimensions":[0,100,0,50]},
	"FlowEdge":{"Color":"black", "Shape":"GnomeCanvasLine", "Points":(50,50, 150,50, 150,150, 50,150)},
	"Fork":{"Color":"black", "Shape":"GnomeCanvasRect", "Dimensions":[0,150,0,5]},
	"Join":{"Color":"black", "Shape":"GnomeCanvasRect", "Dimensions":[0,150,0,5]},
	"Condition":{"Color":"black", "Shape":"GnomeCanvasText", "Text":"Text"},
	"Decision":{"Color":"blue", "Shape":"GnomeCanvasPolygon", "Points":(0,10, 10,20, 20,10, 10,0 , 0,10)},
	"Merge":{"Color":"blue", "Shape":"GnomeCanvasRect", "Dimensions":[30,40,30,40]},
	"Partition":{"Color":"blue", "Shape":"GnomeCanvasRect", "Dimensions":[30,40,30,40]},
	"SubActivityIndic":{"Color":"blue", "Shape":"GnomeCanvasRect", "Dimensions":[30,40,30,40]},
	"FlowFinal":{"Color":"blue", "Shape":"GnomeCanvasLine", "Points":(50,50, 150,50, 150,150, 50,150,)},
	}

#=========================================================================
class Canvas(gtk.HBox):
	"""
	Canvas object to place in a container.
	"""
	#-----------------------------------------------------------------
	def __init__(self, InitWidth=800, InitHeight=400, Libs=[]):
		"""
		Initialize empty widget list, Canvas dimensions.
		"""
		logging.info("Create new Canvas")
		self.Libraries=Libs
		self.Widgets={}
		self.Buttons={}
		self.ActiveWidget=None
		self.SelectedWidget=None
		self.WLine=None
		self.ConnectID=None
		
		gtk.HBox.__init__(self)
		
		# Create buttons.
		self.RemoveButton=None # Delete button
		
		
		self.CanvasWidth  = InitWidth
		self.CanvasHeight = InitHeight
		
		self.Canvas = gnomecanvas.Canvas(aa=True)
#		self.Canvas.connect("event", self.on_canvas_event)
#		self.Zoom=1.0
#		self.Canvas.set_pixels_per_unit(self.Zoom)
		self.Canvas.show()
		EB=gtk.EventBox()
		EB.connect("event", self.on_canvas_event)
		EB.add(self.Canvas)
		self.pack_start(EB)
		self.pack_start(gtk.VSeparator(), False, False)
		self.BuildButtons()
		self.Canvas.set_size_request(self.CanvasWidth, self.CanvasHeight)
		self.NewWidget("InitialNode", x=self.CanvasWidth/2, y=20)
		self.show_all()
		self.CanvasWidth, self.CanvasHeight = self.Canvas.get_size_request()
		self.Canvas.set_scroll_region(0,0, self.CanvasWidth, self.CanvasHeight)
		
		#self.connect("size-allocate", self.NewSize)
		self.remember_x = 0
		self.remember_y = 0
		
		self.TCGHandler= lambda : logging.warning("No packet dependency graph handler.")

#-----------------------------------------------------------------
	def NewSize(self, HboxSelf, Rectangle):
		"""
		Request new size for canvas to avoid coordinate errors.
		"""
		self.Canvas.set_size_request(Rectangle.width, Rectangle.height)
		
	#-----------------------------------------------------------------
	def NewWidget(self, WidgetName=None, x=200, y=200):
		"""
		Add a new widget to canvas corresponding to WidgetName at x, y coordinate.
		"""
		#x, y = self.Canvas.world_to_window(x, y) # worldx, worldy, winx, winy
		#x_off, y_off = self.Canvas.get_scroll_offsets()
		if WidgetName:
			logging.info("Create new {0}".format(WidgetName))
			if list(WidgetLibrary.keys()).count(WidgetName):
				# DRAW TEXT	
				if WidgetLibrary[WidgetName]["Shape"] == "GnomeCanvasText":
					W = self.Canvas.root().add(WidgetLibrary[WidgetName]["Shape"], 
						text=WidgetLibrary[WidgetName]["Text"], 
						fill_color=WidgetLibrary[WidgetName]["Color"],
						x=x,
						y=y)
				# DRAW LINE (never enter there)	
				elif WidgetLibrary[WidgetName]["Shape"] == "GnomeCanvasLine":
					W = self.Canvas.root().add(WidgetLibrary[WidgetName]["Shape"], 
						points=WidgetLibrary[WidgetName]["Points"], 
						fill_color="red", 
						width_pixels=6,
						join_style="GDK_JOIN_ROUND",
						first_arrowhead=False,
						last_arrowhead=True,
						arrow_shape_a=7,  # a = length of the arrow from base
						arrow_shape_b=10, # b = Length of the arrow from edges
						arrow_shape_c=5,  # c = width of the arrow between edges
						smooth=False)
					W.move(x, y)
				# DRAW POLYGON	
				elif WidgetLibrary[WidgetName]["Shape"] == "GnomeCanvasPolygon":
					W = self.Canvas.root().add(WidgetLibrary[WidgetName]["Shape"], 
								points=WidgetLibrary[WidgetName]["Points"], 
								fill_color=WidgetLibrary[WidgetName]["Color"], 
								outline_color='black',
								width_units=1.0)
					W.move(x, y)
				# DRAW RECTANGLE or ELLISPE				
				else:
					x1, x2, y1, y2 = [float(x) for x in WidgetLibrary[WidgetName]["Dimensions"]]
					W = self.Canvas.root().add(WidgetLibrary[WidgetName]["Shape"], 
								x1=x1, y1=y1, x2=x2, y2=y2, 
								fill_color=WidgetLibrary[WidgetName]["Color"], 
								outline_color='black',
								width_units=1.0)
					W.move(x - x1, y - y1)
					
				W.connect("event", self.on_widget_event)
				W.show()
				W.raise_to_top()
				Lx, Ly = GetCenter(W)
				if WidgetName == "Activity":
					NewWLabel=self.Canvas.root().add(
									"GnomeCanvasText", 
									text=WidgetName, 
									fill_color="black",
									x=Lx,
									y=Ly)
					NewWLabel.connect("event", self.on_label_event)
					Xmove=(max(x2,x1)-min(x2,x1))/2.0
					Ymove=(max(y2,y1)-min(y2,y1))/2.0
					NewWLabel.move(Xmove, Ymove)
					NewWLabel.raise_to_top()
				elif WidgetName in ["InitialNode", "FinalNode"]:
					NewWLabel=self.Canvas.root().add(
									"GnomeCanvasText", 
									text="?", 
									fill_color="black",
									x=Lx,
									y=Ly)
					NewWLabel.connect("event", self.on_label_event)
					Xmove=(max(x2,x1)-min(x2,x1))/2.0
					Ymove=(max(y2,y1)-min(y2,y1))/2.0
					if WidgetName == "InitialNode":
						NewWLabel.move(Xmove, -Ymove)
					else:
						NewWLabel.move(Xmove, Ymove*3)
					NewWLabel.raise_to_top()
				else:
					NewWLabel=self.Canvas.root().add(
									"GnomeCanvasText", 
									text="", 
									fill_color="black",
									x=Lx,
									y=Ly)
				self.Widgets[W]={"Targets":[], "InputLinks":{}, "OutputLinks":{}, "Label":[WidgetName, NewWLabel], "Service": None, "Type": WidgetName} # Widget is connected to an empty list of items
				
	#-----------------------------------------------------------------
	def on_button_toggled(self, Button, WidgetName=None):
		"""
		Toggle out all other buttons and set active widget.
		"""
		if Button.get_active(): 
			for W, B in self.Buttons.items():
				if W!=WidgetName: B.set_active(False)
			logging.debug("Select {0}".format(WidgetName))
			self.ActiveWidget=WidgetName
		else:
			self.ActiveWidget=None
			
		self.Canvas.grab_focus()
		self.UpdateActivity()
		
	#-----------------------------------------------------------------
	def on_ButtonBox_clicked(self, BB, Event):
		"""
		And 
		"""
		if Event.type == gtk.gdk.BUTTON_PRESS:
			logging.debug("Selection mode")
			self.UnselectAllButtons()
		return False
		
	#-----------------------------------------------------------------
	def BuildButtons(self):
		"""
		Add a list of widget buttons to Canvas HBox.
		"""
		BB = gtk.VButtonBox()
		BB.connect("event", self.on_ButtonBox_clicked)
		BB.set_layout(gtk.BUTTONBOX_START)
		self.pack_start(BB, False, False)
		for WidgetName in sorted(WidgetLibrary.keys()):
			B = gtk.ToggleButton(WidgetName)
			B.connect("toggled", self.on_button_toggled, WidgetName)
			B.set_relief(gtk.RELIEF_NONE)
			self.Buttons[WidgetName]=B
			BB.pack_start(B)
			
		self.RemoveButton = gtk.Button("Remove")
		self.BuildTCGButton = gtk.Button("Build TCG")
		BB.pack_start(self.RemoveButton)
		BB.pack_start(self.BuildTCGButton)
		BB.set_child_secondary(self.RemoveButton, True)
		BB.set_child_secondary(self.BuildTCGButton, True)
		self.RemoveButton.connect("clicked", self.on_RemoveButton_clicked)
		self.BuildTCGButton.connect("clicked", self.on_BuildTCGButton_clicked)
		self.BuildTCGButton.hide()
		self.Select(None)
		
	#-----------------------------------------------------------------
	def on_BuildTCGButton_clicked(self, BuildTCGButton):
		"""
		Perform action corresponding to event.
		"""
		self.TCGHandler(self.GetTree())
		
	#-----------------------------------------------------------------
	def UnselectAllButtons(self):
		"""
		Toggle out every buttons.
		"""
		for W, B in self.Buttons.items():
			B.set_active(False)
		
	#-----------------------------------------------------------------
	def UpdateActivity(self):
		"""
		Change drawing status.
		"""
		pass
		
	#-----------------------------------------------------------------
	def on_canvas_event(self, W, event):
		"""
		Perform action corresponding to event.
		"""
		if self.ActiveWidget:
			if event.type == gtk.gdk.BUTTON_PRESS:
				if event.button == 1:
					if WidgetLibrary[self.ActiveWidget]["Shape"]=="GnomeCanvasLine": return False
					self.NewWidget(self.ActiveWidget, x=event.x, y=event.y)
				elif event.button == 3: 
					self.UnselectAllButtons()
					self.ActiveWidget=None
#					self.Select(None)
				
		if event.type == gtk.gdk.SCROLL:
			if event.direction == gtk.gdk.SCROLL_UP:
				logging.info("Zoom out")
				self.Zoom+=0.1
				self.Canvas.set_pixels_per_unit(self.Zoom)
			if event.direction == gtk.gdk.SCROLL_DOWN:
				logging.info("Zoom in")
				self.Zoom-=0.1
				self.Canvas.set_pixels_per_unit(self.Zoom)
				
		return False
		
	#-----------------------------------------------------------------
	def on_widget_event(self, W, event):
		"""
		Perform action corresponding to event.
		"""
		if event.type == gtk.gdk.BUTTON_PRESS:
			self.Select(W)
			if event.button == 1:
				# Remember starting position.
				self.remember_x = event.x
				self.remember_y = event.y
				# DRAW LINE
				if not self.ActiveWidget: return False
				if WidgetLibrary[self.ActiveWidget]["Shape"]=="GnomeCanvasLine":
					self.EnterLineMode(W)
					return True
					
				return False
#			elif event.button == 2: 
#				if self.SelectedWidget: self.SelectedWidget.destroy()

		elif event.type == gtk.gdk._2BUTTON_PRESS:
			# Change the item's text content.
			
			logging.info("Open Service Library Manager.")
			if isinstance(self.SelectedWidget, gnomecanvas.CanvasLine):
				return False
			elif self.Widgets[self.SelectedWidget]["Label"][0] == "Activity":
				# Open dialog to hold canvas.
				Parent=self.get_parent() # Fetch parent window
				while not isinstance(Parent, gtk.Window): Parent=Parent.get_parent()
				LibManager.IcoLib=IcoLib
				LibManager.IcoServ=IcoServ
				Serv=LibManager.SelectService(Parent, self.Libraries)
				if Serv!=None: 
					self.ChangeLabel(W, Serv.Name)
					self.SetService(W, Serv)
			else:
				self.AddLabel(W) # Default way to edit widget label
			return True
            
		elif event.type == gtk.gdk.MOTION_NOTIFY:
			if event.state & gtk.gdk.BUTTON1_MASK:
				# If moving instead of drawing line: stop drawing line
				if self.ConnectID!=None: 
					logging.debug("")
					self.LeaveLineMode()
				# Get the new position and move by the difference
				new_x = event.x
				new_y = event.y

				if W in self.Widgets:
					W.raise_to_top()
					W.move(new_x - self.remember_x, new_y - self.remember_y)
					
					if self.Widgets[W]["Label"][0] is not None:
						Label, WLabel = self.Widgets[W]["Label"]
						WLabel.move(new_x - self.remember_x, new_y - self.remember_y)
						WLabel.raise_to_top()

				self.Link()
#					for Target in self.Widgets[W]:
#						self.Link(Target)

				self.remember_x = new_x
				self.remember_y = new_y
					
				return False
				
		elif event.type == gtk.gdk.ENTER_NOTIFY:
			# Make the outline wide.
			if W in self.Widgets:
				try: W.set(width_units=4)
				except: pass # For text items
			else:
				try: W.set(width_units=6)
				except: pass # For text items
				
			return False

		elif event.type == gtk.gdk.LEAVE_NOTIFY:
			# Make the outline thin.
			if W in self.Widgets:
				try: W.set(width_units=1)
				except: pass # For text items
			else:
				try: W.set(width_units=4)
				except: pass # For text items
				
			self.Link()
			return False
		
	#-----------------------------------------------------------------
	def on_RemoveButton_clicked(self, RemoveButton):
		"""
		Perform action corresponding to event.
		"""
		if self.SelectedWidget: 
			# If it's not a line, remove from others link dictionaries
			if self.SelectedWidget in self.Widgets:
				# Clean outputs
				for Source in self.Widgets:
					WOutputs=self.Widgets[Source]["OutputLinks"]
					if self.SelectedWidget in WOutputs:
						# If a link to selected is found: remove reference
						logging.info("Remove selected widget output link")
						L=WOutputs[self.SelectedWidget]
						del WOutputs[self.SelectedWidget]
						self.Widgets[Source]["Targets"].remove(self.SelectedWidget)
						
						# Remove duplicated input reference from selected
						WInputs=self.Widgets[self.SelectedWidget]["InputLinks"]
						del WInputs[Source]
						L.destroy() # En destroy link
						
				# Clean inputs
				for Target in self.Widgets:
					WInputs=self.Widgets[Target]["InputLinks"]
					
					if self.SelectedWidget in WInputs:
						logging.info("Remove selected widget input link")
						L=WInputs[self.SelectedWidget]
						del WInputs[self.SelectedWidget]
						L.destroy() # Already referenced
					
				# Clean Label
				self.Widgets[self.SelectedWidget]["Label"][1].destroy()
				# Clean reference to widget
				del self.Widgets[self.SelectedWidget]
				logging.info("Remove selected widget")
						
			else: # If it's a line
				logging.info("Remove selected link")
				for W in self.Widgets:
					for T, L in self.Widgets[W]["InputLinks"].items():
						if L is self.SelectedWidget:
							del self.Widgets[W]["InputLinks"][T]
							self.Widgets[T]["Targets"].remove(W)
							logging.info("-> as input link")
							break
					for T, L in self.Widgets[W]["OutputLinks"].items():
						if L is self.SelectedWidget:
							del self.Widgets[W]["OutputLinks"][T]
							logging.info("-> as output link")
							break
			self.SelectedWidget.destroy()
			self.Select(None)
		
	#-----------------------------------------------------------------
	def Select(self, W):
		"""
		Remember selected widget.
		"""	
		if isinstance(W, gnomecanvas.CanvasText):
			# Fine widget of this label
			for Widget in self.Widgets:
				if "Label" in self.Widgets[Widget]:
					if self.Widgets[W]["Label"][1] is W:
						break
			W=Widget
		if not W: self.SelectedWidget=W # Select None
		for Widget in list(self.Widgets.keys()):
			if Widget is W:
				logging.debug("Select widget")
				self.SelectedWidget=W
				try: Widget.set(outline_color="red")
				except: Widget.set(fill_color="red")
			else:
				try: Widget.set(outline_color="black")
				except: Widget.set(fill_color="black")
				
			for L in list(self.Widgets[Widget]["OutputLinks"].values()):
				if W is L:
					logging.debug("Select link")
					self.SelectedWidget=L
					L.set(fill_color="red")
				else:   
					L.set(fill_color="black")
					
		if self.SelectedWidget: self.RemoveButton.set_sensitive(True)
		else: self.RemoveButton.set_sensitive(False)
		
	#-----------------------------------------------------------------
	def UpdateLine(self, C, event, x, y):
		"""
		Refresh line draw between origin and mouse.
		"""	
		if event.type == gtk.gdk.BUTTON_PRESS:
			if event.button == 1:
				if self.WLine:  self.WLine.destroy()
				Target=self.Canvas.get_item_at(event.x, event.y)
				if Target!=None:
					if isinstance(Target, gnomecanvas.CanvasText):
						# Fine widget of this label
						for W in self.Widgets:
							if self.Widgets[W]["Label"][0] not in ["InitialNode","FinalNode"]:
								if self.Widgets[W]["Label"][1] is Target:
									Target=W
									break
						
					Source=self.Canvas.get_item_at(x, y)
					if isinstance(Source, gnomecanvas.CanvasText):
						# Fine widget of this label
						for W in self.Widgets:
							if self.Widgets[W]["Label"][0] not in ["InitialNode","FinalNode"]:
								if self.Widgets[W]["Label"][1] is Source:
									Source=W
									break
					if not self.Widgets[Source]["Targets"].count(Target) and Target in self.Widgets:
						self.Widgets[Source]["Targets"].append(Target)
						
					self.LeaveLineMode()
				
			if event.button == 3:
				self.LeaveLineMode()
				
		if event.type == gtk.gdk.MOTION_NOTIFY:
			# Get the new position and move by the difference
			try:
				if self.WLine: self.WLine.destroy()
				if not self.ActiveWidget: return False
				self.WLine = self.Canvas.root().add(WidgetLibrary[self.ActiveWidget]["Shape"], 
					points=(x,y , event.x,event.y), 
					fill_color="black", 
					width_pixels=4,
					join_style="GDK_JOIN_ROUND",
					first_arrowhead=False,
					last_arrowhead=True,
					arrow_shape_a=5,
					arrow_shape_b=10,
					arrow_shape_c=5,
					smooth=False,
					line_style="GDK_LINE_ON_OFF_DASH")
				self.WLine.lower_to_bottom()
				if self.Widgets[self.SelectedWidget]["Label"][0] not in ["InitialNode","FinalNode"]:
					self.Widgets[self.SelectedWidget]["Label"][1].raise_to_top()
#			self.ConnectID=self.Canvas.connect("event", self.UpdateLine, x, y)
			except:
				logging.warning("Disconnect line drawing")
				self.LeaveLineMode()
			return True

	#-----------------------------------------------------------------
	def EnterLineMode(self, W):
		"""
		Process entering line drawing mode steps.
		"""	
		if W not in self.Widgets: return True
		logging.debug("EnterLineMode !")
		LineOriX, LineOriY = GetCenter(W)
		self.ConnectID=self.Canvas.connect("event", self.UpdateLine, LineOriX, LineOriY)
		W.raise_to_top()
		self.Widgets[W]["Label"][1].raise_to_top()
		
	#-----------------------------------------------------------------
	def LeaveLineMode(self):
		"""
		Process leaving line drawing mode steps.
		"""	
		logging.debug("LeaveLineMode !")
		self.UnselectAllButtons()
		if self.ConnectID!=None: 
			self.Canvas.disconnect(self.ConnectID)
			self.ConnectID=None
		if self.WLine: self.WLine.destroy()
		self.Link()
		
	#-----------------------------------------------------------------
	def Link(self):
		"""
		Refresh line drawn between a target widget(s) and all its predecessors.
		"""
		for W in list(self.Widgets.keys()):
			TargetList=self.Widgets[W]["Targets"]
			for Target in TargetList:
				Sx,Sy = GetCenter(W) # Get Source coordinates
				Tx,Ty = GetAnchor(Target, Sy)
				InputLinks=self.Widgets[Target]["InputLinks"]
				OutputLinks=self.Widgets[W]["OutputLinks"]
				if not list(OutputLinks.keys()).count(Target): # Draw for first time
					Line = self.Canvas.root().add("GnomeCanvasLine", 
							points=(Sx,Sy , Tx,Ty), 
							fill_color="black", 
							width_pixels=4,
							join_style="GDK_JOIN_ROUND",
							first_arrowhead=False,
							last_arrowhead=True,
							arrow_shape_a=5,
							arrow_shape_b=10,
							arrow_shape_c=5,
							smooth=False)
					Line.lower_to_bottom()
					InputLinks[W]=Line
					OutputLinks[Target]=Line
					Line.connect("event", self.on_widget_event)
				else:
					# Change arrow target
					OutputLinks[Target].set(points=(Sx,Sy , Tx,Ty))
					OutputLinks[Target].lower_to_bottom()
				
	#-----------------------------------------------------------------
	def ChangeLabel(self, W, Text):
		"""
		Change label of an activity widget.
		"""
		if not W in self.Widgets: return False
		if not "Label" in self.Widgets[W]: return False
		logging.debug("Service selected is '{0}'".format(Text))
		self.Widgets[W]["Label"][1].set_property("text", Text)
				#-----------------------------------------------------------------
	def SetService(self, W, Serv):
		"""
		Change service of an activity widget.
		"""
		if not W in self.Widgets: return False
		if not "Service" in self.Widgets[W]: return False
		logging.debug("Set Service '{0}'".format(Serv))
		self.Widgets[W]["Service"]=Serv
				
	#-----------------------------------------------------------------
	def AddLabel(self, W):
		"""
		Add new label to activity widget. Open a dialog
		"""
		if not W in self.Widgets: return False
		if not "Label" in self.Widgets[W]: return False
		Parent=self.get_parent() # Fetch parent window
		while not isinstance(Parent, gtk.Window): Parent=Parent.get_parent()
		DialogInput = gtk.Dialog("Label Editor", Parent, gtk.DIALOG_MODAL|gtk.MESSAGE_INFO, (gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE))
		DialogInput.add_button(gtk.STOCK_OK, gtk.RESPONSE_OK) # Add the OK button
		DialogInput.set_title("Enter label")
		DialogInput.get_content_area().pack_start(gtk.Label("Enter label:"))
		InputEntry=gtk.Entry()
		InputEntry.show()
		InputEntry.connect("activate", self.on_InputEntry_activate, DialogInput)
		DialogInput.get_content_area().pack_start(InputEntry)
		InputEntry.set_text(self.Widgets[W]["Label"][1].get_property("text"))
		Response = DialogInput.run()
		Label=InputEntry.get_text()
		DialogInput.destroy()
		logging.debug("New label set to '{0}'".format(Label))
		if Response==gtk.RESPONSE_OK:
			self.Widgets[W]["Label"][1].set_property("text", Label)
#			self.Widgets[W]["Label"][1].destroy()
#			x, y = GetCenter(W)
#			L = self.Canvas.root().add("GnomeCanvasText", 
#				text=Label, 
#				fill_color="black",
#				x=x,
#				y=y)
#			self.Widgets[W]["Label"]=[Label, L]
#			L.connect("event", self.on_label_event)
			
	#-----------------------------------------------------------------
	def on_InputEntry_activate(self, InputEntry, DialogInput):
		"""
		Perform action corresponding to event.
		"""
		Button_OK = DialogInput.get_widget_for_response(gtk.RESPONSE_OK)
		Button_OK.clicked()
		
	#-----------------------------------------------------------------
	def on_label_event(self, L, event):
		"""
		Perform action corresponding to event.
		"""
		if event.type == gtk.gdk.BUTTON_PRESS:
			if event.button == 1:
				# Remember starting position.
				self.remember_x = event.x
				self.remember_y = event.y
				# Fine widget of this label
				for W in self.Widgets:
					if len(self.Widgets[W]["Label"])>1:
						if self.Widgets[W]["Label"][1] is L:
							self.Select(W)
							break
				# DRAW LINE
				if not self.ActiveWidget: return False
				if WidgetLibrary[self.ActiveWidget]["Shape"]=="GnomeCanvasLine":
					self.EnterLineMode(W)
				return False
				
		elif event.type == gtk.gdk._2BUTTON_PRESS:
			logging.info("Label edition request")
			# Fine widget of this label
			for W in self.Widgets:
				if self.Widgets[W]["Label"][0] not in ["InitialNode","FinalNode"]:
					if self.Widgets[W]["Label"][1] is L:
						break
			self.AddLabel(W)
			return False
			
		elif event.type == gtk.gdk.MOTION_NOTIFY:
			if event.state & gtk.gdk.BUTTON1_MASK:
				# Fine widget of this label
				for W in self.Widgets:
					if self.Widgets[W]["Label"][0] not in ["InitialNode","FinalNode"]:
						if self.Widgets[W]["Label"][1] is L:
							break
				new_x = event.x
				new_y = event.y
				L.move(new_x - self.remember_x, new_y - self.remember_y)
				W.move(new_x - self.remember_x, new_y - self.remember_y)
				
				self.remember_x = new_x
				self.remember_y = new_y

				self.Link()
				return False
		
	#-----------------------------------------------------------------
	def GetTree(self):
		"""
		Convert widget dictionary to exploitable ActivityDiagram object.
		"""
		return ActivityDiagram(self.Widgets).ActivTree
		
	#-----------------------------------------------------------------
	def SetTCGHandler(self, Handler):
		"""
		Set which fonction has to be called with the ActivityTree as argument.
		"""
		self.TCGHandler=Handler
		
#=========================================================================
class ActivityService:
	"""
	ActivityService object build from activity information.
	"""
	#-----------------------------------------------------------------
	def __init__(self, Name, Type, Serv=None):
		"""
		Build an service object from activity information.
		"""
		logging.debug("New item in activity diagram : {0}({1})".format(Name, Type))
		self.Name    = Name # Service name
		self.Type    = Type
		self.Service = Serv # Service object
		
	#-----------------------------------------------------------------
	def __str__(self):
		"""
		Display the ActivityService name in string format.
		"""
		return str(self.Service)
		
		
		
#=========================================================================
class ActivityDiagram:
	"""
	Diagram informations.
	"""
	#-----------------------------------------------------------------
	def __init__(self, Widgets):
		"""
		Read widget dictionnary and keep useful informations in a tree-dictionnary.
		"""
		self.ActivTree={}
		WidgetsDictionnary = Widgets.copy()
		InitWidgets=[]
		# Find the init widgets-----------------------------------
		logging.debug("Find the init widgets")
		for InitW, ParamList in WidgetsDictionnary.items():
			for TestW, TestParamList in WidgetsDictionnary.items():
				if InitW is not TestW:
					if InitW in TestParamList["Targets"]: 
						if InitW in InitWidgets:
							InitWidgets.remove(InitW)
						break
					else: 
						if InitW not in InitWidgets: 
							InitWidgets.append(InitW)
					
		if InitWidgets==[]: return
		
		self.AlreadyConverted={}
		for InitWidget in InitWidgets: # Build a tree from diagram
			#print "* INIT WIDGET *", WidgetsDictionnary[InitWidget]["Label"][1].get_property("text"), '('+str(WidgetsDictionnary[InitWidget]["Label"][0])+')'
			self.AddActivity(self.ActivTree, InitWidget, WidgetsDictionnary, Init=True, Final=False)
		
		# Then remove redondency in tree -------------------------
		#	(gather same services after each "join")
		
	#-----------------------------------------------------------------
	def AddActivity(self, ActivTree, Widget, WidgetsDictionnary, Init=False, Final=False):
		"""
		Add service object to Service tree: ActivTree.
		"""
		StopBuilding=False
		WLabel=WidgetsDictionnary[Widget]["Label"][1].get_property("text")
		WServ = WidgetsDictionnary[Widget]["Service"]
		if WidgetsDictionnary[Widget]["Type"]=="Activity": Type="Activity"
		elif Init==True: Type="Input"
		elif Final==True: Type="Output"
		else:
			logging.error("Unrocognized activity type : add of activity aborted.")
			return
			
		AServ = ActivityService(WLabel, Type, Serv=WServ);
		Params={"Name":AServ.Name, "Type": AServ.Type}
		
		Children={}
		if Widget in list(self.AlreadyConverted.keys()):
			# It's a joined activity : add corresponding service and quit
			logging.debug("Already referenced service: {0}({1})".format(WLabel, Type))
			ActivTree[AServ]=self.AlreadyConverted[Widget]
			StopBuilding=True
			return
		else:# Remember already converted
			if Widget!="Join":
				self.AlreadyConverted[Widget]=[AServ, Params, Children]
				# Build new tree branch
				ActivTree[AServ]=[AServ, Params, Children]
				logging.debug("Add new reference of activity service: {0}({1})".format(WLabel, Type))
				
		if StopBuilding==True: return # After a join (if branch already handled)
	
		for T in WidgetsDictionnary[Widget]["Targets"]:
			if len(WidgetsDictionnary[T]["Targets"])==0:
				self.AddActivity(Children, T, WidgetsDictionnary, Init=False, Final=True)
			else: 
				self.AddActivity(Children, T, WidgetsDictionnary, Init=False, Final=False)
		
		
	#-----------------------------------------------------------------
	def DisplayTreeBranch(self, AServ, Parameters, ChildTree, IndentLevel=0):
		"""
		Display the activity diagram in text format recursively.
		"""
		Text=""
		Text+= '\n' +"  "*IndentLevel + "-->"+ str(AServ.Name) + "["+str(Parameters)+"] => " 
		Text+= '[' + ", ".join([str(x) for x in list(ChildTree.keys())]) +']'
		for Child in list(ChildTree.keys()):
			AServ, Params, Children = ChildTree[Child]
			Text+=self.DisplayTreeBranch(AServ, Params, Children, IndentLevel=IndentLevel+2)
		return Text
		
	#-----------------------------------------------------------------
	def __str__(self):
		"""
		Display the activity diagram in text format.
		"""
		Text="ACTIVITY DIAGRAM:"
		Dict=self.ActivTree.copy()
		
		for A in list(Dict.keys()):
			AServ, Params, Children = Dict[A]
			Text+=self.DisplayTreeBranch(AServ, Params, Children, IndentLevel=2)
		
		return Text
		
		
#====================================================================
def GetCenter(Widget):
	"""
	Get center coordinate of the widget.
	"""
	x1, y1, x2, y2 = Widget.get_bounds()
	LineOriX=(max(x1, x2)-min(x1, x2))/2.0+min(x1, x2)
	LineOriY=(max(y1, y2)-min(y1, y2))/2.0+min(y1, y2)
	return LineOriX, LineOriY
		
#====================================================================
def GetAnchor(Widget, Sy):
	"""
	Get center coordinate of the widget.
	"""
	Tx,Ty = GetCenter(Widget) # Get Target coordinates
	x1, y1, x2, y2 = Widget.get_bounds()
	if Sy>Ty: dy=(max(y1, y2)-min(y1, y2))/2.0
	else: dy=-(max(y1, y2)-min(y1, y2))/2.0
	
	return Tx,Ty+dy
		
"""
Initial node. The filled in circle is the starting point of the diagram.  An initial node isn't required although it does make it significantly easier to read the diagram.

Activity final node. The filled circle with a border is the ending point.  An activity diagram can have zero or more activity final nodes.

Activity.   The rounded rectangles represent activities that occur. An activity may be physical, such as Inspect Forms, or electronic, such as Display Create Student Screen.

Flow/edge.  The arrows on the diagram.  Although there is a subtle difference between flows and edges I have never seen a practical purpose for the difference although I have no doubt one exists.  I'll use the term flow. 

Fork.  A black bar with one flow going into it and several leaving it.  This denotes the beginning of parallel activity.

Join.  A black bar with several flows entering it and one leaving it.  All flows going into the join must reach it before processing may continue.  This denotes the end of parallel processing.

Condition.  Text such as [Incorrect Form] on a flow, defining a guard which must evaluate to true in order to traverse the node.

Decision. A diamond with one flow entering and several leaving.  The flows leaving include conditions although some modelers will not indicate the conditions if it is obvious. 

Merge.  A diamond with several flows entering and one leaving.  The implication is that one or more incoming flows must reach this point until processing continues, based on any guards on the outgoing flow.
Partition. Figure 2 is organized into three partitions, also called swimlanes, indicating who/what is performing the activities (either the Applicant, Registrar, or System).
Sub-activity indicator.  The rake in the bottom corner of an activity, such as in the Apply to University activity, indicates that the activity is described by a more finely detailed activity diagram.  In Figure 2 the Enroll In Seminar activity includes this symbol.

Flow final.  The circle with the X through it.  This indicates that the process stops at this point. 

Note. Figure 2 includes a standard UML note to indicate that the merges does not require all three flows to arrive before processing can continue.  An alternative way to model this would have been with an OR constraint between the no match and applicant not on match list flows.  I prefer notes because stakeholders find them easier to understand.

Use case.  In Figure 1 I indicated that the Enroll in Seminar use case is invoked as one of the activities.  This is a visual cheat that I use to indicate that an included use case is being invoked.  To tell you the truth I'm not sure if this is official allowed by the UML but clearly it should be.  Another way to depict this is shown in Figure 2 via the use of a normal activity although I don't think this is as obvious as using a use case.
"""

def MainQuit(*args):
	gtk.main_quit()
    
if __name__ == '__main__':


	LoggingMode = logging.DEBUG

	logging.basicConfig(format='%(asctime)s | %(levelname)s: %(message)s', level=LoggingMode, filemode='w+') # Change to ERROR for releases

	logging.info("Test of the Activity graph canvas.")
        # Open window to hold canvas.
        win = gtk.Window()
        win.connect('destroy', MainQuit)
        win.set_title('Canvas Example')
        C = Canvas()
        win.add(C)
	win.show_all()
    	gtk.main()
    	print((C.GetADTree()))
        
        
        
        
        
        

