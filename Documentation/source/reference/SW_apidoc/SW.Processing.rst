SW.Processing package
=====================

.. automodule:: SW.Processing
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    SW.Processing.Finance
    SW.Processing.ImageProcessing
    SW.Processing.Standard

Submodules
----------

.. toctree::

   SW.Processing.TestNode

