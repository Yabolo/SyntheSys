SW package
==========

.. automodule:: SW
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    SW.Communication
    SW.Processing

