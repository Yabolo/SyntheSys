*****************
SyntheSys library
*****************




The *SyntheSys library* if made up of three parts : 
	* **The software library** : Python functions linked to HDL modules internally. These functions must be called by the user algorithm.
	* **The HDL library** : HDL modules (accelerators)
	* **The hardware library** : logic synthesis scripts, configuration files for FPGA boards, FPGA board connectivity

.. toctree::
   :maxdepth: 2

   SyntheSys_Library_SW
   SyntheSys_Library_HDL
   SyntheSys_Library_HW

.. toctree::
   :hidden:










