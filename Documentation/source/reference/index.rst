.. _reference:

Reference
*********

   :Release: |release|
   :Date: |today|


.. toctree::
   :maxdepth: 2

   SyntheSys_API
   SyntheSys_Library
   license

.. toctree::
   :hidden:

