**********
Installing
**********

Installing with pip
===================
Try to install it with

::

   pip install SyntheSys

and an attempt will be made to find and install an appropriate version
that matches your operating system and Python version.

You can also get SyntheSys from the Python Package Index manually
at http://pypi.python.org/pypi/SyntheSys
To use pip, you need to have `setuptools <https://pypi.python.org/pypi/setuptools>`_ installed.

You can install the development version (at gitlab.com) with

::

  pip install git://gitlab.com/Yabolo/SyntheSys.git#wheel=SyntheSys




Installing from source
======================

You can install from source by downloading a source archive file
(tar.gz or zip) or by checking out the source files from the
Git source code repository.

SyntheSys is a pure Python package; you don't need a compiler to build
or install it.

Source archive file
-------------------

  1. Download the source (tar.gz or zip file) from
     https://pypi.python.org/pypi/SyntheSys/
     or get the latest development version from
     https://gitlab.com/SyntheSys/SyntheSys/

  2. Unpack and change directory to the source directory
     (it should have the files README.txt and setup.py).

  3. Run :samp:`python setup.py install` to build and install

  4. (Optional) Run :samp:`nosetests` to execute the tests if you have
     `nose <https://pypi.python.org/pypi/nose>`_ installed.


Gitlab
------

  1. Clone the SyntheSys repository
     (see https://gitlab.com/Yabolo/SyntheSys/ for options)
     ::

       git clone https://gitlab.com/Yabolo/SyntheSys.git


  2. Change directory to :samp:`SyntheSys`

  3. Run :samp:`python setup.py install` to build and install


If you don't have permission to install software on your
system, you can install into another directory using
the :samp:`--user`, :samp:`--prefix`, or :samp:`--home` flags to setup.py.

For example

::

    python setup.py install --prefix=/home/username/python

or

::

    python setup.py install --home=~

or

::

    python setup.py install --user

If you didn't install in the standard Python site-packages directory
you will need to set your PYTHONPATH variable to the alternate location.
See http://docs.python.org/3/install/index.html#search-path for further details.


Requirements
============

Python
------

To use SyntheSys you need Python 3.3 or later.


Optional packages
=================

The following are optional packages that SyntheSys can use to
provide additional functions.


GraphViz
--------

In conjunction with 

      - PyGraphviz:  http://pygraphviz.gitlab.io/

provides graph drawing and graph layout algorithms.

  - Download: http://graphviz.org/


Other software
---------------

These are extra software you may consider using with SyntheSys.

For HDL simulation :
      - Modelsim or Questasim (Menthor Graphics)
      - gHDL
      - isim (Xilinx ISE)
      
For FPGA logic synthesis :
      - Vivado (Xilinx)
      - Quartus (Altera)
      

Communication drivers
---------------------

The HDL library provided comes with PCIe Riffa module that needs a specific driver to be used.
  - Download: http://riffa.ucsd.edu/

      
      
      
      
      
      
      
      
