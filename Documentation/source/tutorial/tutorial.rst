..  -*- coding: utf-8 -*-

.. currentmodule:: SyntheSys

Start here to begin working with SyntheSys.

Generate your first FPGA design from Python
-------------------------------------------

Create a new python module MyAlgo.py.

1. First import SyntheSys decorators and functions : 

>>> from SyntheSys import Algorithm, Testbench, Synthesize
    
2. Then import the library functions you need from SyntheSys library : 

>>> from SyntheSys.Library.SW.Processing.Math import Square

3. Decorate the function you need to accelerate like this : 

>>> @Algorithm() # Mean that this method will be HighLevelModule algorithm to be synthesized.
>>> def MyAlgorithm(a,b,c):
>>> 	Delta = Square(b)-4*a*c
>>> 	return Delta

4. Decorate the function used to generate stimuli like this : 

>>> @MyTestbench() # Mean that this method will be HighLevelModule algorithm to be synthesized.
>>> def MyAlgorithm(a,b,c):
>>> 	Delta = Square(b)-4*a*c
>>> 	return Delta

5. To synthesize the specified algorithm, call the 'Synthesize' function like this : 

>>> Synthesize(MyAlgorithm, MyTestbench, OutputPath="./MyOutputDirectory", a=float,b=float,c=float)

6. Finally execute MyAlgo.py script with SyntheSys arguments : 

>>> python3 MyAlgo.py simulate -a VC707 --simulator=modelsim

Use the option "--help" for more details about the needed options.

To go further :

- Add new HDL modules to the library : :doc:`/tutorial/HdlModules`

- Configure the simulation and synthesis environment: :doc:`/tutorial/SyntheSysEnv`

