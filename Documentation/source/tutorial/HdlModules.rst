..  -*- coding: utf-8 -*-


**************************
Add modules to HDL library
**************************

HDL Library Structure
---------------------

Add processing module
---------------------

Add communication module
------------------------


User clock from communication module
-------------------------------------------------
Communication module can provided a user clock (like the Riffa PCIe module In this case, the user design should not be connected directly to the system clock but to this communication synchronized clock.
To do that, an orthogonal service named "userclock" is provided in SyntheSys library. 
The communication module should require this service and map the user clock to the "userclock" signal.

User reset from communication module
-------------------------------------------------
User reset signal from communication module can replace system reset by using the requiring the orthogonal service named "userreset" the same way as the user clock (see previous paragraph). 



