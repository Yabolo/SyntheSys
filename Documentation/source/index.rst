.. SyntheSys documentation master file, created by
   sphinx-quickstart on Tue Oct  4 12:44:57 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SyntheSys's documentation!
=====================================

.. only:: html

    :Release: |version|
    :Date: |today|
    
Contents:

.. toctree::
   :maxdepth: 2

   overview
   install
   tutorial/index
   reference/index
   examples/index

#.. only:: html


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
