..  -*- coding: utf-8 -*-

Overview
========

SyntheSys is a Python language software package for the synthesis of NoC based FPGA applications from Python programs.  

With SyntheSys you can generate VHDL, simulate with the supported simulators and synthesize the resulting design for any FPGA referenced in the extensible hardware library.
SyntheSys can also be used to analyse the potential parallelism that can be extracted from a python program.



Who uses SyntheSys?
-------------------

The potential audience for SyntheSys includes FPGA designers, computer scientists, physicists, biologists, and any algorithm designers that could benefit from accelerating programs with the help of FPGAs.



Goals
-----
SyntheSys is intended to provide

- a graphical analysis of the data flow of a program 

- a programming interface that does not require any architectural knowledge to use an FPGA

- a rapid prototyping environment for reconfigurable computing

- an extensible hardware library for FPGA synthesis and simulation

- an extensible HDL library (IPs) that can be instanciated as NoC nodes

- optimisation tools for NoC design

- a transparent way to use FPGA locally or remotly (cloud FPGA)


Free software
-------------

SyntheSys is free software; you can redistribute it and/or
modify it under the terms of the :doc:`GPLv3 License </reference/license>`.
We welcome contributions from the community.  Information on
SyntheSys development is found at the SyntheSys Developer Zone at Gitlab
https://gitlab.com/Yabolo/SyntheSys


History
-------

SyntheSys was born in September 2016. 
The original version was designed and written by Matthieu Payet during his Ph.D (Lyon, France) in 2012.
The first public release was in October 2016.

Publications
------------
SyntheSys has been used in the following publications: 

* ? TODO
* ? TODO


What Next
^^^^^^^^^

 - :doc:`A Brief Tour </tutorial/tutorial>`

 - :doc:`Installing </install>`

 - :doc:`Reference </reference/index>`

 - :doc:`Examples </examples/index>`
