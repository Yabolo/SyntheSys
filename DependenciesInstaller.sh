#! /bin/bash

# To be executed by Ubuntu / Debian OS
sudo apt-get install python3-setuptools
sudo apt-get install python3-dev
sudo apt-get install libgraphviz-dev graphviz
sudo apt-get install gfortran libopenblas-dev liblapack-dev
sudo apt-get install pkg-config
sudo apt-get install libpng++-dev

sudo easy_install3 pip
sudo -H pip3 install gi
sudo -H pip3 install cairo
sudo -H pip3 install numpy
sudo -H pip3 install scipy
sudo -H pip3 install sklearn
sudo -H pip3 install lxml
sudo -H pip3 install pygraphviz
#sudo easy_install3 -U pygraphviz
# Matplotlib----------------
# sudo pip3 install tornado pytz cycler pyparsing libagg freetype-dev ft2build qhull
# sudo pip3 install matplotlib 
sudo -H pip3 networkx==1.9
sudo -H pip3 install python-dateutil
sudo -H pip3 install xlrd
sudo -H pip3 install bitstring
sudo -H pip3 install openpyxl
#sudo pip3 install tornado


#9. SWIG (module for C/C++ to python interfacing)
#	> on windows : see the tar.gz
#	> on linux   : use apt-get

#11. pyparsing
#	> on windows : use easy_install
#	> on linux   : use easy_install

#12. tornado
#	> on windows : use easy_install
#	> on linux   : use easy_install

#13. pyinstaller (module for turning python into executable) 
#	> on windows : use easy_install
#	> on linux   : use easy_install

#14. nuitka (module for turning python into C then into executable) 
#	> on windows : use easy_install
#	> on linux   : use easy_install

#15. pip3 (module for installing python modules) 
#	> on windows : use easy_install
#	> on linux   : use easy_install

#18. sh (module for calling bash commands like methods) 
#	> on windows : use easy_install
#	> on linux   : use easy_install
	

