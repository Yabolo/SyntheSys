**********
Installing
**********

Requirements
============

On Ubuntu / Debian OS :
	sudo apt-get install python3-setuptools
	sudo apt-get install python3-dev
	sudo apt-get install libgraphviz-dev graphviz
	sudo apt-get install gfortran libopenblas-dev liblapack-dev
	sudo apt-get install pkg-config
	sudo apt-get install libpng++-dev


Quick Install
=============

Get SyntheSys from the Python Package Index at
https://gitlab.com/Yabolo/SyntheSys

or install it with::

	pip install SyntheSys

and an attempt will be made to find and install an appropriate version
that matches your operating system and Python version.

You can install the development version (at github.com) with::

	pip install https://gitlab.com/Yabolo/SyntheSys.git#egg=SyntheSys


Get SyntheSys from the Python Package Index at
http://pypi.python.org/pypi/SyntheSys

or install it with::

	easy_install SyntheSys

and an attempt will be made to find and install an appropriate version
that matches your operating system and Python version. 


Installing from Source
======================

You can install from source by downloading a source archive file
(tar.gz or zip) or by checking out the source files from the
Subversion repository.

Source Archive File
-------------------

  1. Download the source (tar.gz or zip file).

  2. Unpack and change directory to SyntheSys-"version" 

  3. Run "python setup.py install" to build and install 



Github
------

  1. Clone the SyntheSys repository

       git clone https://gitlab.com/Yabolo/SyntheSys.git

  (see https://gitlab.com/Yabolo/SyntheSys/ for other options)

  2. Change directory to "SyntheSys"

  3.  Run "python setup.py install" to build and install

  4. (optional) Run "python setup_egg.py nosetests" to execute the tests


If you don't have permission to install software on your
system, you can install into another directory using
the --user, --prefix, or --home flags to setup.py.

For example

::

    python setup.py install --prefix=/home/username/python
    or
    python setup.py install --home=~
    or
    python setup.py install --user

If you didn't install in the standard Python site-packages directory
you will need to set your PYTHONPATH variable to the alternate location.
Seehttp://docs.python.org/2/install/index.html#search-path for further details.


Requirements
============

Python3
------

To use SyntheSys you need Python version > 3.2.x 
Python2 or older are not supported.

The easiest way to get Python and most optional packages is to install

There are several other distributions that contain the key packages you need for scientific computing.  See the following link for a list: http://scipy.org/install.html

